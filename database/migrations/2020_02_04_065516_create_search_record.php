<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSearchRecord extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Schema::create('search_record_view', function (Blueprint $table) {
        //     $table->bigIncrements('id');
        //     $table->timestamps();
        // });

        \DB::statement("
            CREATE VIEW search_records 
            AS
            SELECT
                mst_domain_url.domain_url, 
                mst_domain_url.domain_id, 
                mst_domain_url.domain_website, 
                mst_domain_url.domain_status,
                mst_domain_url.domain_approval_status,
                mst_domain_url.cost_price,
                mst_domain_url.backlink_type,
                mst_domain_url.title,
                mst_domain_url.description,
                mst_domain_url.p, 
                mst_domain_url.a, 
                mst_domain_url.h1, 
                mst_domain_url.h2, 
                mst_domain_url.h3, 
                mst_domain_url.h4, 
                mst_domain_url.h5, 
                mst_domain_url.h6,
                mst_domain_url.all_seo_data,
                
                mst_domain_url.domain_approval_by,
                mst_domain_url.domain_fk_user_id,
                mst_seo_data.sd_title, 
                mst_seo_data.sd_descr, 
                mst_seo_data.sd_title_n, 
                mst_seo_data.sd_descr_n,
                mst_seo_data.sd_global_rank, 
                mst_seo_data.sd_country_rank,
                mst_seo_data.sd_category_rank,
                mst_seo_data.sd_traffic_main_value,
                mst_seo_data.sd_traffic_main_percent,
                mst_seo_data.sd_page_views_avg,
                mst_seo_data.sd_time_on_site_avg,
                mst_da_pa.dp_da, 
                mst_da_pa.dp_pa,  
                mst_da_pa.dp_page_likes,  
                mst_da_pa.dp_followers,  
                mst_da_pa.dp_dropped,  
                mst_seo_data.sd_visits, 
                mst_seo_data.sd_bounce_rate, 
                mst_seo_data.sd_percent, 
                mst_search_url.surl_url,
                add_to_carts.mst_payment_id,
                assigned_websites.user_id AS assigned_user_id,
               ( SELECT COUNT(add_to_carts.fk_domain_url) FROM add_to_carts WHERE add_to_carts.fk_domain_url = mst_domain_url.domain_url AND is_billed = 0 ) as orders
            FROM
                mst_domain_url
                LEFT JOIN mst_seo_data ON mst_seo_data.sd_fk_domain = mst_domain_url.domain_id
                LEFT JOIN mst_search_url ON mst_search_url.surl_fk_domain = mst_domain_url.domain_id
                LEFT JOIN mst_da_pa ON mst_da_pa.dp_fk_domain = mst_domain_url.domain_id
                LEFT JOIN add_to_carts ON add_to_carts.domain_id = mst_domain_url.domain_id
                JOIN assigned_websites ON assigned_websites.mst_domain_url_id = mst_domain_url.domain_id
            WHERE mst_domain_url.cost_price > 0 AND mst_da_pa.dp_da >= 20 AND mst_domain_url.trash = 0
            
        ");

    }
//LEFT JOIN add_to_carts ON add_to_carts.domain_id = mst_domain_url.domain_id
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('search_records');
    }
}
