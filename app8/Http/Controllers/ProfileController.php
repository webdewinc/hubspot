<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Support\Facades\Validator;
use Auth;
use Crypt;
use Redirect;
use Hash;
use DB;
use App\User;
use App\Credential;

class ProfileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application profile.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function edit_profile()
    {
        $user_id = Auth::user()->id;
        $user_data = DB::table('users')
                    ->where('id',$user_id)
                    ->first();

        if(empty( $user_data )){
            $user_data = DB::table('users')
                    ->where('id',$user_id)
                    ->first();
         }
        return view('profile.edit-profile',compact('user_data'));
    }


    public function store_profile(Request $request)
    {
        $args   = [
            'user_fname'        =>  $request->first_name,
            'user_lname'        =>  $request->last_name,
            'user_mob_no'       =>  $request->contact_number
        ];
        if(request()->file('image')){
            $args['image'] = request()->file('image')->store('public/images');
        }        
        //update profile db
        User::where('id', Auth::user()->id)->update($args);
        return redirect()->back()->with(['message'=> 'Profile has been Updated Successfully!!', 'alert' => 'success','current_index'=> 1]);
    }

    /**
     * Show the application change password.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function change_password()
    {
        return view('customer.change-password');
    }


    public function store_change_password(Request $request)
    {
        $rules = array(
            'current_password'      => 'required|string|max:20',
            'new_password'      => 'required|string|max:20',
            'verify_password' => 'required|same:new_password'
        );

        $validatedData = $request->all();
        $validator = Validator::make($validatedData, $rules);
        if ($validator->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->with(['message'=> 'All fields are required', 'alert' => 'danger', 'current_index'=> 3])
                        ->withInput();
        } else {
            $input = $request->all();
            $user = User::find(Auth::user()->id);
            if(Hash::check($input['current_password'], $user->password)){
                try {
                    unset($validatedData['_token']);
                    unset($validatedData['current_password']);
                    unset($validatedData['verify_password']);
                    $validatedData['password']        = bcrypt(array_get($validatedData, 'new_password'));
                    unset($validatedData['new_password']);
                    $password                        =  User::where('id',Auth::user()->id)->update($validatedData);
                } catch (\Exception $exception) {
                    logger()->error($exception);
                    return redirect()->back()->with(['message'=> 'Unable to change password.', 'alert' => 'danger', 'current_index'=> 3]);
                    
                }

            } else if(md5($input['current_password']) == $user->password){
                
                try {
                    unset($validatedData['_token']);
                    unset($validatedData['current_password']);
                    unset($validatedData['verify_password']);
                    $validatedData['password']        = bcrypt(array_get($validatedData, 'new_password'));
                    unset($validatedData['new_password']);
                    $password                        =  User::where('id',Auth::user()->id)->update($validatedData);
                } catch (\Exception $exception) {
                    logger()->error($exception);
                    return redirect()->back()->with(['message'=> 'Unable to change password.', 'alert' => 'danger', 'current_index'=> 3]);
                    
                }

            } else {
            return redirect()->back()->with(['message'=> 'Current password not match. please try again', 'alert' => 'danger', 'current_index'=> 3]);
            
            }            
            return redirect()->back()->with(['message'=> 'Password has been changed.', 'alert' => 'success', 'current_index'=> 3]);
        }    
    }

    

}
