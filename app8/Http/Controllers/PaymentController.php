<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Support\Facades\Validator;
use Auth;
use Crypt;
use Redirect;
use Hash;
use DB;
use App\User;
use App\Credential;
use Stripe\Error\Card;
use Stripe;
use Session;

class PaymentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * handling payment with POST
     */
    public function handlePost(Request $request)
    {
     
        try {


            $app_type = $request->type;
            $app_id = $request->app_id;
            $amount = DB::table('apps')->where('id',$app_id)->value('amount');
            $app_name = DB::table('apps')->where('id',$app_id)->value('name');
            if($amount > 0) {
                Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
                $response= Stripe\Charge::create ([
                    "amount" => $amount * 100,
                    "currency" => "usd",
                    "source" => $request->stripeToken,
                    "description" => $app_name. " Plugin Charges" 
                ]);
             
                $status = $response->status;
                if($status == 'succeeded') {
             
                    $id = $response->id;
                    $amount = $response->amount / 100;
                    $currency = $response->currency;
                    $customer_name = $request->request->get('cardholder');
                    $status = $response->status;
                    $email  = $response->billing_details->name;
                    $res = json_encode($response);
                    $payment_status = $response->paid;
                    $receipt_url = @$response->receipt_url;

                    $auth_email = Auth::user()->email;

                    $payment_id = DB::table('payments')->insertGetId([
                                                            'app_id' => $app_id,
                                                            'app_type' => $app_type,
                                                            'stripe_id' => $id,
                                                            'user_id' => Auth::user()->id,
                                                            'amount' => $amount,
                                                            'currency_code' => $currency,
                                                            'status' => $status,
                                                            'all_response' => serialize($res) ,
                                                            'receipt_url' => $receipt_url
                                                    ]);


                    // check subscription 

                        $args['url'] = 'https://api.hubapi.com/crm/v3/objects/subscriptions/search?hapikey='.env('HUBSPOT_API_KEY');
                        $args['data'] =  json_encode([
                                            "filterGroups" => [
                                              [
                                                "filters" => [
                                                  [
                                                    "propertyName" => "email",
                                                    "operator" => "EQ",
                                                    "value" => Auth::user()->email
                                                  ]
                                                ]
                                              ]
                                            ]
                                          ]);

                        $result = curlAccess('POST',$args);
                    
                    if(!isset($result->status)){
                        
                        // insert subscription
                        $args['url'] = 'https://api.hubapi.com/crm/v3/objects/'.env('subscription_key').'/?hapikey='.env('HUBSPOT_API_KEY');
                        
                        if(!empty(Auth::user()->firstname) || !empty(Auth::user()->lastname)){
                            $fullname = @Auth::user()->firstname .' '.Auth::user()->lastname;
                        } else {
                            $fullname = @Auth::user()->email;
                        }
                        // if(env('APP_ENV') == 'live'){
                        //     if($app_id == 1){
                        //         $app_val = 'All in one SMS App';
                        //     } else if($app_id == 2){
                        //         $app_val = 'WHMCS HubSpot App';
                        //     } else if($app_id == 3){
                        //         $app_val = 'HubSpot eCommerce App';
                        //     }
                        //     $args['data']= json_encode([
                        //           "properties" => [
                        //               "email" => Auth::user()->email,
                        //               "name" => $fullname,
                        //               "app_type" => $app_val,
                        //               "subscription_id" => $payment_id
                        //           ]
                        //     ]);
                        //     // live
                        // } else {
                        //stage
                            $args['data']= json_encode([
                                  "properties" => [
                                      "email" => Auth::user()->email,
                                      "name" => $fullname,
                                      "app_type" => $app_id,
                                      "amount" => $amount,
                                      "subscription_id" => $payment_id
                                  ]
                            ]);
                        
                        //}
                        $result = curlAccess('POST',$args);

                        DB::table('payments')->where('id',$payment_id)->update(['subscription_id' => $result->id ]);
                        // insert subscription
                    }
                    // check subscription

                    // search contact
                    $args['url'] = 'https://api.hubapi.com/crm/v3/objects/contacts/search?hapikey='.env('HUBSPOT_API_KEY');
                    $args['data'] =  json_encode([
                                        "filterGroups"=>[
                                          [
                                            "filters"=>[
                                              [
                                                "propertyName" => "email",
                                                "operator" => "EQ",
                                                "value" => Auth::user()->email
                                              ]
                                            ]
                                          ]
                                        ]
                                      ]);

                    $result = curlAccess('POST',$args);
                    // search contact
                    $vid = 0;
                    if(!isset($result->status)){
                        if($result->total > 0){
                            $vid = $result->results[0]->id;
                        } else {
                            // insert contacts
                            $args['url'] = 'https://api.hubapi.com/contacts/v1/contact/?hapikey='.env('HUBSPOT_API_KEY');
                            
                            $args['data']= json_encode([
                                  "properties" => [
                                    [
                                      "property" => "email",
                                      "value" => Auth::user()->email
                                    ],
                                    [
                                      "property" => "firstname",
                                      "value" => Auth::user()->firstname
                                    ],
                                    [
                                      "property" => "lastname",
                                      "value" => Auth::user()->lastname
                                    ]
                                  ]
                            ]);
                       
                            $result  = curlAccess('POST',$args);
                            if(!isset($result->status)){
                                $vid = $result->vid;
                            }
                            // insert contact
                        }

                        // // create deals
                        // $timestamp = time() *1000;
              
                        // $args['url'] = 'https://api.hubapi.com/deals/v1/deal?hapikey='.env('HUBSPOT_API_KEY');
                        
                        // $args['data']= '{
                        //       "associations": {
                        //         "associatedCompanyIds": [],
                        //         "associatedVids": [
                        //           '.$vid.'
                        //         ]
                        //       },
                        //       "properties": [
                        //         {
                        //           "value": "'.$app_name.' Deal",
                        //           "name": "dealname"
                        //         },
                        //         {
                        //           "value": "appointmentscheduled",
                        //           "name": "dealstage"
                        //         },
                        //         {
                        //           "value": "default",
                        //           "name": "pipeline"
                        //         },
                        //         {
                        //           "value": "51771313",
                        //           "name": "hubspot_owner_id"
                        //         },
                        //         {
                        //           "value": '.$timestamp.',
                        //           "name": "closedate"
                        //         },
                        //         {
                        //           "value": "'.$amount.'",
                        //           "name": "amount"
                        //         },
                        //         {
                        //           "value": "newbusiness",
                        //           "name": "dealtype"
                        //         }
                        //       ]
                        //     }';
                   
                        // curlAccess('POST',$args);
                        // create deals
                        Session::flash('success', 'Payment has been successfully processed.');
                        return Redirect::to($app_type."/auth2-callback");

                    }
                    
                    
                } else {
                    Session::flash('transaction-failed','transaction failed.please try again.');
                    return view('all-app',compact("posts")); 
                }
            } else {
                DB::table('payments')->insert([
                                                'app_id' => $app_id,
                                                'app_type' => $app_type,
                                                'stripe_id' => 0,
                                                'user_id' => Auth::user()->id,
                                                'amount' => $amount,
                                                'currency_code' => 'usd',
                                                'status' => 'succeeded',
                                                'all_response' => NULL,
                                                'receipt_url' => NULL
                                        ]);
            }
            return back();
        } catch(\throwable $e) {
            echo 'Message: ' .$e->getMessage();
        }
    }


    public function whmcs_automation_POST(Request $request)
    {

        try {
            if(isset($request->daily)){
                $type = $request->daily;
            }
            if(isset($request->hour)){
                $type = $request->hour;
            }

            if(isset($request->hour_sync_time)){
                $sync_time = $request->hour_sync_time;
            }
            if(isset($request->daily_sync_time)){
                $sync_time = $request->daliy_sync_time;
            }
            $amount = $request->amount;
            if($amount > 0) {
                Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
                $response= Stripe\Charge::create ([
                    "amount" => $amount * 100,
                    "currency" => "usd",
                    "source" => $request->stripeToken,
                    "description" => "Automation charges." 
                ]);
                
                $status = $response->status;
                if($status == 'succeeded') {
             
                    $id = $response->id;
                    $amount = $response->amount / 100;
                    $currency = $response->currency;
                    $customer_name = $request->request->get('cardholder');
                    $status = $response->status;
                    $email  = $response->billing_details->name;
                    $res = json_encode($response);
                    $payment_status = $response->paid;
                    $receipt_url = @$response->receipt_url;


                    $start_date = date('Y-m-d');
                    if($type == 'daily_month'){
                        $end_date = date('Y-m-d', strtotime('+30 days'));    
                    } else if ($type == 'daily_year'){
                        $end_date = date('Y-m-d', strtotime('+365 days'));
                    } else if ($type == 'hour_month'){
                        $end_date = date('Y-m-d', strtotime('+30 days'));
                    } else if ($type == 'hour_year'){
                        $end_date = date('Y-m-d', strtotime('+365 days'));
                    }

                    $auth_email = Auth::user()->email;
                    
                    if(!isset($result->status)){
                        $payment_id = DB::table('whmcs_payments')->insertGetId([
                                                            'app_id' => 2,
                                                            'app_type' => $type,
                                                            'stripe_id' => $id,
                                                            'user_id' => Auth::user()->id,
                                                            'amount' => $amount,
                                                            'currency_code' => $currency,
                                                            'status' => $status,
                                                            'all_response' => serialize($res),
                                                            'start_date' => $start_date,
                                                            'end_date' => $end_date,
                                                            'receipt_url' => $receipt_url,
                                                            'subscription_id' => 0,
                                                            'sync_time' => $sync_time,
                                                            'active_status' => 1
                                                    ]);
                        $credentials = unserialize(Credential::where('user_id',Auth::user()->id)->where('fk_app_id', 2)->value('data'));
                        if(!empty($credentials['hapi_key'] )) {
                            $timing = time();
                            $time = $sync_time * 3600000;

                            //$domain = httpRemove(url('whmcs-workflow-trigger'));
                            $domain = 'https://localhost/projects/laravel/HSAPP/hubspot/whmcs-workflow-trigger';

                            $args['url'] = 'https://api.hubapi.com/automation/v3/workflows?hapikey='.$credentials['hapi_key'];
                            $args['data'] = '{
                                    "name": "WHMCS Automation '. $sync_time . '-'. $timing .'",
                                    "type": "DRIP_DELAY",
                                    "segmentCriteria": [
                                        [
                                            {
                                                "operator": "IS_NOT_EMPTY",
                                                "property": "email",
                                                "type": "string"
                                            }
                                        ]
                                    ],
                                    "onlyEnrollsManually": true,
                                    "actions": [                                        
                                        {
                                            "type": "DELAY",
                                            "delayMillis": '.$time.'
                                        },
                                        {
                                            "type": "WEBHOOK",
                                            "url": "'.$domain.'",
                                            "method": "POST"
                                        }
                                    ]
                                }';

                                
                            $result = curlAccessAll('POST',$args);
                            
                            if(isset($result->id)){
                                DB::table('whmcs_payments')
                                                    ->where('id', $payment_id)
                                                    ->update([
                                                        'workflow_id' => $result->id,
                                                    ]);
                                if(isset($result->migrationStatus)){
                                    if($result->migrationStatus->portalId && $result->migrationStatus->flowId){
                                        $url = 'https://app.hubspot.com/workflows/'.$result->migrationStatus->portalId.'/platform/flow/'.$result->migrationStatus->flowId.'/edit';
                                        return redirect()->to($url);
                                    }    
                                }
                            }
                        }
                        return redirect()->to('/whmcs-automation')->with(['message'=> 'Payment has been successfully processed.', 'alert' => 'success']);
                    }
                    
                } else {
                    // Session::flash('transaction-failed','transaction failed.please try again.');
                    // return view('whmcs.whmcs-automation'); 
                    return redirect()->to('/whmcs-automation')->with(['message'=> 'transaction failed, please try again.', 'alert' => 'danger']);
                }
            } else {
                // DB::table('payments')->insert([
                //                                 'app_id' => $app_id,
                //                                 'app_type' => $app_type,
                //                                 'stripe_id' => 0,
                //                                 'user_id' => Auth::user()->id,
                //                                 'amount' => $amount,
                //                                 'currency_code' => 'usd',
                //                                 'status' => 'succeeded',
                //                                 'all_response' => NULL,
                //                                 'receipt_url' => NULL
                //                         ]);
                // Session::flash('transaction-failed','Payment amount should be greater than zero.');
                // return view('whmcs-automation'); 
                return redirect()->to('/whmcs-automation')->with(['message'=> 'Payment amount should be greater than zero.', 'alert' => 'danger']);
            }
            return back();
        } catch(\throwable $e) {
            //echo 'Message: ' .$e->getMessage();
            // Session::flash('transaction-failed',$e->getMessage());
            // return view('whmcs.whmcs-automation');
            return redirect()->to('/whmcs-automation')->with(['message'=> $e->getMessage(), 'alert' => 'danger']);
        }
    }
}
