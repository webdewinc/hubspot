<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * Indicates whether the XSRF-TOKEN cookie should be set on the response.
     *
     * @var bool
     */
    protected $addHttpCookie = true;

    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'smsdew/workflowSent','smsdew/settings-hub','smsdew/send-sms','smsdew/send-sms-app', 'whmcs/workflowSent','whmcs/settings-hub','ecommerce/workflowSent','ecommerce/settings-hub','get-product-list-hub','flock/workflowSent','smsdew/check','smsdew/testing','smsdew/definations','stripe-payment','payment','check','smsdew/checking','checking','workflowsent',
        'smsdew/stripe-payment','whmcs-ecommerce','whmcs-ecommerce-admin'
    ];
}
