@extends('layout.app')
@php
$totalTitle = 'Data Import';
@endphp
@section('title', $totalTitle)
@section('title-description')

<meta name="description" content="Data Import">
    
@endsection
@section('content')
@section('head')
@parent    
@show
<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor bg-white mt-5" id="kt_content">

        <!-- begin:: Content -->
        <div class="kt-container kt-grid__item kt-grid__item--fluid home-container">
           
            <div class="row">
                <!-- body content start -->
                <div class="col-12">
                    <div class="kt_blog-post  pb-5">
                        
                        <div class="row justify-content-center">
                            <div class="col-xl-8 col-lg-8 col-md-12">
                                <span id="alert-span"></span>
                                @if (!empty($alert))
                                <div class="alert alert-{{$alert}}">
                                    {{ $message }}
                                </div>
                                @endif
                                @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif

                                <div class="row">
                                    <div class="col">
                                        <div class="card">
                                            <div class="card-header">
                                                <label>Import Data</label>
                                            </div>
                                            <div class="card-body">
                                                <div class="container">

                                                    <style type="text/css">
                                                        #stepProgressBar  {
                                                            display: flex;
                                                            justify-content: space-between;
                                                            align-items: flex-end;
                                                            width: 300px;
                                                            margin: 0 auto;
                                                            margin-bottom: 40px;
                                                        }
                                                        .step  {
                                                            text-align:  center;
                                                        }
                                                        /*.step-text  {
                                                            margin-bottom:  10px;
                                                            color:  #ff7a59;
                                                        }*/
                                                        /*.bullet {
                                                            border: 1px solid #ff7a59;
                                                            height: 20px;
                                                            width: 20px;
                                                            border-radius: 100%;
                                                            color: #ff7a59;
                                                            display: inline-block;
                                                            position: relative;
                                                            transition: background-color 500ms;
                                                            line-height:20px;
                                                        }*/
                                                        .step-text  {
                                                            margin-bottom:  10px;
                                                            color:  #000;
                                                        }
                                                        .bullet {
                                                            border: 1px solid #000;
                                                            height: 20px;
                                                            width: 20px;
                                                            border-radius: 100%;
                                                            color: #000;
                                                            display: inline-block;
                                                            position: relative;
                                                            transition: background-color 500ms;
                                                            line-height:20px;
                                                        }
                                                        .bullet.completed  {
                                                            color:  white;
                                                            background-color:  #ff7a59;
                                                        }
                                                        .bullet.completed::after {
                                                            content: '';
                                                            position: absolute;
                                                            right: -60px;
                                                            bottom: 10px;
                                                            height: 1px;
                                                            width: 10%;
                                                            background-color: #ff7a59;
                                                        }
                                                        /* Base styles and helper stuff */
                                                        .hidden  {
                                                            display:  none;
                                                        }
                                                        .text-center  {
                                                            text-align:  center;
                                                        }
                                                        .container  {
                                                            max-width: 400px;
                                                            margin: 0 auto;
                                                            margin-top:  20px;
                                                            padding:  40px;
                                                        }
                                                    </style>

                                                    <div id="stepProgressBar">
                                                        <div class="step">
                                                            <p class="step-text">Products</p>
                                                            <div class="bullet">1</div>
                                                        </div>
                                                        <div class="step">
                                                            <p class="step-text">Contacts</p>
                                                            <div class="bullet">2</div>
                                                        </div>
                                                        <div class="step">
                                                            <p class="step-text">Orders</p>
                                                            <div class="bullet">3</div>
                                                        </div>
                                                        <div class="step">
                                                            <p class="step-text">Finish</p>
                                                            <div class="bullet">4</div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="card-body">
                                                    <span id="vendor-append">
                                                        <button id="nextBtn" class="btn btn-label-brand btn-bold sync" rel="products" data-step="0" data-url="{{url('whmcs-products')}}">Data Import</button>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>                                  
                    </div>
                </div>
                <!-- body content end -->
            </div>
        </div>
    </div>                  
</div>

@section('scripts')
@parent

  <!--begin::Page Scripts(used by this page) -->
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
        <script type="text/javascript">
            "use strict";

            // Class definition
            var KTWizard2 = function () {
                // Base elements
                var wizardEl;
                var formEl;
                var validator;
                var wizard;

                var initValidation = function() {
                    validator = formEl.validate({
                        // Validate only visible fields
                        ignore: ":hidden",

                        // Validation rules
                        rules: {
                           
                            // routes :{
                            //     required: true,
                            //     maxlength: 3,
                            //     digits:true
                            // },
                            // country_code :{
                            //     required: true,
                            //     maxlength: 3,
                            //     digits:true
                            // },
                            // sender :{
                            //     required: true,
                            //     maxlength: 10,
                            // },
                            // MSG91 :{
                            //     required: true,
                            // }
                        },

                        // Display error
                        invalidHandler: function(event, validator) {
                            KTUtil.scrollTop();
                            swal.fire({
                                "title": "",
                                "text": "There are some errors in your submission. Please correct them.",
                                "type": "error",
                                "confirmButtonClass": "btn btn-secondary"
                            });
                        },

                        // Submit valid form
                        // submitHandler: function (form) {

                        // }
                    });
                }

                var initSubmit = function() {
                    var btn = formEl.find('[data-ktwizard-type="action-submit"]');

                    btn.on('click', function(e) {
                        e.preventDefault();

                        if (validator.form()) {
                            $('#kt_form').submit();     
                        }
                    });
                }

                return {
                    // public functions
                    init: function() {
                        wizardEl = KTUtil.get('kt_wizard_v2');
                        formEl = $('#kt_form');
                        initValidation();
                        initSubmit();
                    }
                };
            }();

            jQuery(document).ready(function() {
                KTWizard2.init();
                /** validation for guest example not to match with domain url **/
                jQuery.validator.addMethod("notEqual", function(value, element, param) {
                  return this.optional(element) || value != param;
                }, "Please specify a different value");
                /** end here code **/
                $.validator.addMethod('lessThanEqual', function(value, element, param) {
                    return this.optional(element) || parseInt(value) <= parseInt($(param).val());
                }, "The content cost must be less than number words");
            });

        function loadData(url, ds){
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                beforeSend: function() {
                    // setting a timeout
                    //$(".loader").fadeIn("slow");
                },
                url  : url,
                type : 'GET',
                async: true,
                //dataType: 'json',
                enctype: 'multipart/form-data',
                cache: false,                      
                success: function(response){
                    if(response.status === true){
                        const  bullets =  [...document.querySelectorAll('.bullet')];
                        if(response.name == 'product') {
                            bullets[0].classList.add('completed');    
                        }
                        if(response.name == 'contact') {
                            bullets[1].classList.add('completed');    
                        }
                        
                        $('#vendor-append').html(response.html);
                        $('#alert-span').html('<div class="alert alert-success">'+response.message+'</div>');
                        if(response.name == 'finish') {
                            bullets[2].classList.add('completed');
                            bullets[3].classList.add('completed');
                            $(".loader").fadeOut("slow");
                        } else {
                            var url = $('.sync').attr('data-url');
                            var ds = $('.sync').attr('data-step');
                            loadData(url, ds);
                        }
                        
                    } else {
                        $('#alert-span').html('<div class="alert alert-danger">'+response.message+'</div>');
                        $(".loader").fadeOut("slow");
                    }
                    
                },
                error: function(){

                }
            });
        }
        // jQuery(document).on('click','.finish',function(e){
        //     const  bullets  =  [...document.querySelectorAll('.bullet')];
        //     bullets[3].classList.add('completed');
        // });   
        jQuery(document).on('click','.sync',function(e) {
            $(".loader").fadeIn("slow");
            //e.preventDefault();
            var url = $(this).attr('data-url');
            var ds = $(this).attr('data-step');
            $('#alert-span').html('');
            loadData(url, ds);
            // e.stopImmediatePropagation();
            // return false;
        });

        </script>
@show
@endsection