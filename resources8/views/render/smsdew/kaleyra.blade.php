<?php
$vendor = vendor_select('kaleyra');
?>
<p><a href="javascript:void(0);" title="Webdew Groups" ><strong id="select-vendor-append">{{$vendor['name']}}</strong></a>
<a href="https://apidocs-sms.kaleyra.com/" target="_blank"  hidden style="float:right;">Api Docs</a>

</p>             
@csrf
<div class="kt-wizard-v2__form">
        
    <?php
        if(empty($data)){
            $button = 'Save';
        } else {
            $button = 'Update';
        }
    ?>
    <div class="form-row">
        <div class="form-group col-md-6 col-12">
            <label class="required">{{$vendor['label']['s_id']}}</label>
            <div class="input-group">
                <input class="form-control" type="text" id="s_id" name="s_id" value="{{@$data['s_id']}}" required="" autocomplete="off">
            </div>
        </div>
		<div class="form-group col-md-6 col-12">
            <label class="required">{{$vendor['label']['api_key']}}</label>
            <div class="input-group">
                <input class="form-control" type="text" id="api_key" name="api_key" value="{{@$data['api_key']}}"  required="" autocomplete="off">
            </div>
        </div>
    </div>
       <div class="form-row">
        <div class="form-group col-md-6 col-12">
            <label class="required">{{$vendor['label']['sender_id']}}</label>
            <div class="input-group">
                <input class="form-control" type="text" id="sender_id" name="sender_id" value="{{@$data['sender_id']}}" required="" placeholder="KLRHXA" autocomplete="off">
            </div>
        </div>
	 <div class="form-group col-md-6 col-12">
            <label class="required">{{$vendor['label']['t_hapi_key']}}</label>
            <div class="input-group">
                <input class="form-control" type="text" id="t_hapi_key" name="t_hapi_key" value="{{@$data['t_hapi_key']}}" required="" minlength="10"  autocomplete="off">
            </div>
        </div>
    </div>
   
 
		 <div class="form-group col-md-6 col-12">            
            <div class="input-group">
                
				<label class="kt-checkbox">
				   <input type="checkbox" id="default" name="default" value="kaleyra" @if(@$type == 'kaleyra'){{'checked'}}@endif> Set as default
					<span></span>
				</label>
            </div>
        </div>
   
   
    <div class="kt-form__actions">
        <button class="btn btn-label-brand btn-bold"  type="submit" data-ktwizard-type="action-submit">
            {{$button}}
        </button>
    </div>
</div>