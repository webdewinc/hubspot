<?php
namespace App\Http\Controllers\WHMCS;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Support\Facades\Validator;
use Auth;
use Crypt;
use Redirect;
use Hash;
use DB;
use App\User;
use Session;
use Twilio\Rest\Client;
use App\Credential;
use App\WhmcsImport;

class WHMCSController extends Controller
{

    public function __construct()
    {
        //$this->middleware('auth');
    }
    // old and working
    // public function auth2_callback(Request $request) 
    // {
       
    //     if(env('APP_ENV') == 'live'){
    //         $provider = new \Flipbox\OAuth2\Client\Provider\HubSpot([
    //             'clientId'          => config('services.live_whmcs.clientId'),
    //             'clientSecret'      => config('services.live_whmcs.clientSecret'),
    //             'redirectUri'       => url('/whmcs/auth2-callback')
    //         ]);
    //     } else {
    //         $provider = new \Flipbox\OAuth2\Client\Provider\HubSpot([
    //             'clientId'          => config('services.stage_whmcs.clientId'),
    //             'clientSecret'      => config('services.stage_whmcs.clientSecret'),
    //             'redirectUri'       => url('/whmcs/auth2-callback')
    //         ]);
    //     }

    //     if ( !isset($_GET['code']) ) {
            
    //         $options = [
    //             'scope' => ['contacts','timeline'] // array or string
    //         ];

    //         // If we don't have an authorization code then get one
    //         $authUrl = $provider->getAuthorizationUrl($options);
    //         header('Location: '.$authUrl);
    //         exit;
    //         // Check given state against previously stored one to mitigate CSRF attack

    //     } 
    //     // elseif (empty($_GET['state']) || ($_GET['state'] !== Auth::user()->oauth2state)) {

    //     //     exit('Invalid state');

    //     // }
    //     else {

    //         $result = [];
    //         // Try to get an access token (using the authorization code grant)
    //         $token = $provider->getAccessToken('authorization_code', [
    //             'code' => $_GET['code']
    //         ]);
    //         // Optional: Now you have a token you can look up a users profile data
    //         try {
    //             // We got an access token, let's now get the user's details
    //             $user = $provider->getResourceOwner($token);                
                
    //             $result['portal_id'] = $user->getHubId();
    //             $result['email'] = $user->getEmail();
    //             $result['app_id'] = @$user->getAppId();
    //             $result['refresh_token'] = @$token->getRefreshToken();
    //             $result['expires_at'] = @$token->getExpires();
    //             $result['user_expires_at'] = @$user->getExpires();
    //             $result['code'] = $_GET['code'];
    //             $result['expires_at'] = time() + $result['expires_at'] * 0.95;
    //             $result['auth_token'] = $token->getToken();
                
    //             $u = User::whereEmail($result['email'])->first();
    //             if(empty($u->id)) {
    //                 $validatedData['user_type']       = 'users';
    //                 $validatedData['activation_code'] = str_random(30).time();
    //                 $validatedData['user_email_verified'] = 1;
    //                 $validatedData['user_fname'] = 'New';
    //                 $validatedData['user_lname'] = 'User';
    //                 $validatedData['email'] = $result['email'] ;
    //                 $validatedData['user_status'] = 1;
    //                 $validatedData['vid'] = 0;
    //                 app(User::class)->create($validatedData);
    //                 $u = User::whereEmail($result['email'])->first();
    //             } 
    //             Auth::login($u);

    //             $mapped_app_id = DB::table('mapped_apps')->where('user_id',Auth::user()->id)->where('fk_app_id',2)->value('mapped_app_id');
    //             if(empty($mapped_app_id)){
    //                 DB::table('mapped_apps')->insert(['fk_app_id'=> 2,'user_id'=> Auth::user()->id]);
    //             }

    //             $scopes = @serialize($user->getScopes());
    //               // update
    //                 $users = User::find(Auth::user()->id);
    //                 $users->portal_id = $result['portal_id'];
    //                 $users->hub_email = $user->getEmail();
    //                 $users->scopes =  $scopes;
    //                 $users->app_id = @$user->getAppId();
    //                 $users->refresh_token = @$token->getRefreshToken();
    //                 $users->expires_at = @$token->getExpires();
    //                 $users->code = $_GET['code'];
    //                 $users->auth_token = $token->getToken();
    //                 $users->save();
    //               // update
                
    //             // redirect to hubspot 
    //             $hubspotUrl = 'https://app.hubspot.com/';
    //             if(!empty($result['portal_id']) && isset($result['portal_id'])){
    //                 //$hubspotUrl = 'https://app.hubspot.com/home?portalID='.$result['portal_id'];
    //                 $hubspotUrl = 'https://app.hubspot.com/integrations-settings/'.$result['portal_id'].'/installed';
    //             }
    //             header('Location: '.$hubspotUrl);
    //             exit;
    //             // redirect to hubspot 

    //             // $args['data'] = '{
    //             //       "properties": [
    //             //         {
    //             //           "property": "portal_id",
    //             //           "value": "'.$result['portal_id'].'"
    //             //         },
    //             //         {
    //             //           "property": "hub_email",
    //             //           "value": "'.$result['email'].'"
    //             //         },
    //             //         {
    //             //           "property": "refresh_token",
    //             //           "value": "'.$result['refresh_token'].'"
    //             //         },
    //             //         {
    //             //           "property": "token",
    //             //           "value": "'.$result['auth_token'].'"
    //             //         },
    //             //         {
    //             //           "property": "code",
    //             //           "value": "'.$result['code'].'"
    //             //         },
    //             //         {
    //             //           "property": "app_id",
    //             //           "value": "'.$result['app_id'].'"
    //             //         }
    //             //       ]
    //             //     }';
    //             // ;
    //             // $args['url'] = 'https://api.hubapi.com/contacts/v1/contact/email/'.$result['email'].'/profile?hapikey='.env('HUBSPOT_API_KEY');
    //             // $store = $this->curlAccess( 'POST', $args );                
    //             // Use these details to create a new profile

    //         } catch (Exception $e) {
    //             // Failed to get user details
    //             exit('Oh dear...');
    //         }
    //         // Use this to interact with an API on the users behalf
    //         return redirect('home');
    //     }
    // }
    // old and working

    public function auth2_callback(Request $request)
    {
        // $paymentCheck = DB::table('payments')->where('user_id',Auth::user()->id)->where('app_id',1)->value('id');
        // if ($paymentCheck > 0) { //payment is done

            if(env('APP_ENV') == 'live'){
                $provider = new \Flipbox\OAuth2\Client\Provider\HubSpot([
                    'clientId'          => config('services.live_whmcs.clientId'),
                    'clientSecret'      => config('services.live_whmcs.clientSecret'),
                    'redirectUri'       => url('/whmcs/auth2-callback')
                ]);
            } else {
                $provider = new \Flipbox\OAuth2\Client\Provider\HubSpot([
                    'clientId'          => config('services.stage_whmcs.clientId'),
                    'clientSecret'      => config('services.stage_whmcs.clientSecret'),
                    'redirectUri'       => url('/whmcs/auth2-callback')
                ]);
            }

            if (!isset($_GET['code']))
            {

                $options = ['scope' => ['contacts', 'timeline', 'e-commerce', 'integration-sync', 'oauth'] // array or string
                ];
                // If we don't have an authorization code then get one
                $authUrl = $provider->getAuthorizationUrl($options);

                // update
                header('Location: ' . $authUrl);
                exit;
                // Check given state against previously stored one to mitigate CSRF attack
                
            }
            else
            {

                $result = [];
                // Try to get an access token (using the authorization code grant)
                $token = $provider->getAccessToken('authorization_code', ['code' => $_GET['code']]);

                //            $_SESSION['token']=$token;
                // Optional: Now you have a token you can look up a users profile data
                try
                {

                    // We got an access token, let's now get the user's details
                    $user = $provider->getResourceOwner($token);

                    $result['portal_id'] = $user->getHubId();
                    $result['email'] = $user->getEmail();
                    $result['app_id'] = @$user->getAppId();
                    $result['refresh_token'] = @$token->getRefreshToken();
                    $result['expires_at'] = @$token->getExpires();
                    $result['user_expires_at'] = @$user->getExpires();
                    $result['code'] = $_GET['code'];
                    $result['expires_at'] = time() + $result['expires_at'] * 0.95;
                    $result['auth_token'] = $token->getToken();

                    $u = User::whereEmail($result['email'])->first();

                    /*===============
                        |insert code end
                        |================
                        */
                        if (empty($u->id))
                        {
                            $validatedData['user_type'] = 'users';
                            $validatedData['activation_code'] = str_random(30) . time();
                            $validatedData['user_email_verified'] = 1;
                            $validatedData['user_fname'] = 'New';
                            $validatedData['user_lname'] = 'User';
                            $validatedData['email'] = $result['email'];
                            $validatedData['user_status'] = 1;
                            $validatedData['vid'] = 0;
                            app(User::class)->create($validatedData);
                            $u = User::whereEmail($result['email'])->first();
                        }
                        Auth::login($u);

                        $mapped_app_id = DB::table('mapped_apps')->where('user_id', Auth::user()
                            ->id)
                            ->where('fk_app_id', 2)
                            ->value('mapped_app_id');
                        if (empty($mapped_app_id))
                        {
                            DB::table('mapped_apps')->insert(['fk_app_id' => 2, 'user_id' => Auth::user()->id]);
                        }

                        $scopes = @serialize($user->getScopes());
                        // update
                        $users = User::find(Auth::user()->id);
                        $users->portal_id = $result['portal_id'];
                        $users->hub_email = $user->getEmail();
                        $users->scopes = $scopes;
                        $users->app_id = @$user->getAppId();
                        $users->refresh_token = @$token->getRefreshToken();
                        $users->expires_at = @$token->getExpires();
                        $users->code = $_GET['code'];
                        $users->auth_token = $token->getToken();
                        $users->save();
                        // update
                        

                    //$user_id = $u->id;
                    $user_id = Auth::user()->id;
                    $portal_id = $result['portal_id'];
                    $refresh_token = $result['refresh_token'];
                    $expiry_time = $result['expires_at'];
                    

                    //$this->setting_store_token($token->getToken());

                    // $args['url'] = 'https://api.hubapi.com/extensions/ecomm/v2/stores/ecommercebridge-whmcs-store';
                    // $args['token'] = $token->getToken();;
                    // $bridgeR = $this->curlAccessH('GET',$args);
                    // if(!empty($bridgeR)){
                    //     if(isset($bridgeR->id)){
                    //         if($bridgeR->id == ''){
                                // $args['url'] = 'https://api.hubapi.com/extensions/ecomm/v2/stores/';
                                // $args['data'] = '{
                                //           "id": "ecommercebridge-whmcs-web-store",
                                //           "label": "WHMCS Ecommerce Bridge Store",
                                //           "adminUri": "https://apps.webdew.com/hubspot/whmcs-ecommerce-admin"
                                //         }';
                                // $args['token'] = $token->getToken();
                                // $g = $this->curlAccessH('PUT',$args);

                                // $args['url'] = 'https://api.hubapi.com/extensions/ecomm/v2/stores/ecommercebridge-whmcs-web-store';
                                // $args['token'] = $token->getToken();
                                // $g = $this->curlAccessH('DELETE',$args);

                    //         }
                    //     }
                    // }

                    /*===============
                    |implementing payment check
                    |================
                    */
                    $email = $user->getEmail();

                    $paymentCheck = DB::table('payments')->where('user_id',Auth::user()->id)->where('app_id',2)->value('id');
                    
                    if ($paymentCheck > 0) { //payment is done

                        /*===============
                        |Select query to check if application was already intalled in the same poratl or not
                        |================
                        */

                        $record_check = DB::table('application_records')->where('user_id',Auth::user()->id)->where('app_id',2)->where('portal_id',$portal_id)->value('id');

                        if ($record_check > 0)
                        {
                            DB::table('application_records')->where('app_id',2)->where('user_id',Auth::user()->id)->where('portal_id',$portal_id)->update([
                                        'refresh_token' => $refresh_token,
                                        'expiry_time' => $expiry_time
                                    ]);
                        } else {

                            /*===============
                            |insert code in new table for storing refresh token for different portals
                            |================
                            */

                            $insert = DB::table('application_records')->insertGetId([
                                        'user_id' => $user_id,
                                        'portal_id' => $portal_id,
                                        'client_id' => '',
                                        'secret_id' => '',
                                        'refresh_token' => $refresh_token,
                                        'expiry_time' => $expiry_time,
                                        'app_id' => 2
                                    ]);

                        }

                        // redirect to hubspot
                        $hubspotUrl = 'https://app.hubspot.com/';
                        if (!empty($result['portal_id']) && isset($result['portal_id']))
                        {
                            
                            $hubspotUrl = 'https://app.hubspot.com/integrations-settings/'.$result['portal_id'].'/installed';
                            //$hubspotUrl = 'https://app.hubspot.com/home?portalID=' . $result['portal_id'];
                        }
                        header('Location: ' . $hubspotUrl);
                        exit;
                        // redirect to hubspot
                        
                        
                    } else { 

                        $paymentCheck = DB::table('payments')->insertGetId([
                                                                'app_id' => 2 ,
                                                                'user_id' => Auth::user()->id,
                                                                'stripe_id' => 'manual',
                                                                'amount' => 0,
                                                                'currency_code' => 'usd',
                                                                'status' => 'succeeded',
                                                                'app_type' => 'whmcs',
                                                                'subscription_id' => 0
                                                            ]);

                        /*===============
                        |Select query to check if application was already intalled in the same poratl or not
                        |================
                        */

                        $record_check = DB::table('application_records')->where('user_id',Auth::user()->id)->where('portal_id',$portal_id)->where('app_id',2)->value('id');

                        if ($record_check > 0)
                        {
                            DB::table('application_records')->where('app_id',2)->where('user_id',Auth::user()->id)->where('portal_id',$portal_id)->update([
                                        'refresh_token' => $refresh_token,
                                        'expiry_time' => $expiry_time
                                    ]);
                        } else {

                            /*===============
                            |insert code in new table for storing refresh token for different portals
                            |================
                            */

                            $insert = DB::table('application_records')->insertGetId([
                                        'user_id' => $user_id,
                                        'portal_id' => $portal_id,
                                        'client_id' => '',
                                        'secret_id' => '',
                                        'refresh_token' => $refresh_token,
                                        'expiry_time' => $expiry_time,
                                        'app_id' => 2
                                    ]);

                        }

                        // redirect to hubspot
                        $hubspotUrl = 'https://app.hubspot.com/';
                        if (!empty($result['portal_id']) && isset($result['portal_id']))
                        {
                            
                            $hubspotUrl = 'https://app.hubspot.com/integrations-settings/'.$result['portal_id'].'/installed';
                            //$hubspotUrl = 'https://app.hubspot.com/home?portalID=' . $result['portal_id'];
                        }
                        header('Location: ' . $hubspotUrl);
                        exit;
                        // redirect to hubspot

                        //return redirect('all-in-one-sms');
                        //exit;
                    }
                }
                catch(Exception $e)
                {
                    // Failed to get user details
                    exit('Oh dear...');
                }
                // Use this to interact with an API on the users behalf
                return redirect('/');
            }
        // } else {
        //     return redirect('all-in-one-sms');
        // }
    }
    public function fetch_view(){
        Auth::logout();
        if(!empty($_GET['userEmail'])){
            $id = User::whereEmail($_GET['userEmail'])->value('id');
            if(!empty($id)) {
                $auth_token = User::whereEmail($_GET['userEmail'])->value('auth_token');

                // DB::table('workflow_results')->insertGetId([
                //                     'data' => serialize($_GET),
                //                     'response' => serialize($_GET)
                //                 ]);
                if(!empty($auth_token)) {
                    
                    $results = array("results" => array(),
                                "primaryAction"=> array(
                                    "type"=> "IFRAME",
                                    "width"=> 890,
                                    "height"=> 748,
                                    "uri"=> url("whmcs/settings-hub/".$id."?vid=" . @$_GET['associatedObjectId'] . "&portal_id=" . @$GET['portalId'] . "&email=" .@$_GET['userEmail'] . "&phone=" . @$_GET['phone']. "&auth_id=" . @$id),
                                    "label"=> "Settings"
                                )
                    );  
                    
                } else {
                    $results = array("results" => array( 0 => array(
                                    "objectId"=> -1,
                                    "title" => "Please login and authorize your account with hubspot",
                                    "link" => env('APP_URL'),
                                    "priority" => "HIGH",
                                    "project" => "API"
                                )
                            )
                        );
                }

            } else {
                $results = array("results" => array( 0 => array(
                            "objectId"=> -1,
                            "title" => "You have to register your account.",
                            "link" => url('/'),
                            "priority" => env('APP_URL'),
                            "project" => "API"
                        )
                    )
                );
            }

        } else {
            $results = array("results" => array( 0 => array(
                        "objectId"=> -1,
                        "title" => "something were wrong.",
                        "link" => env('APP_URL'),
                        "priority" => "HIGH",
                        "project" => "API"
                    )
                )
            );
        }
        return json_encode($results);
    }
    public function refresh_token(){

        $refresh_token = Auth::user()->refresh_token;
        $tokens = "";
        if($refresh_token){            
            if(!empty($refresh_token)){
                
                //if(time() > Auth::user()->expires_at) {
                    
                    $curl = curl_init();
                    curl_setopt_array($curl, array(
                        CURLOPT_URL => 'https://api.hubapi.com/oauth/v1/token',
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => "",
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 30,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => 'POST',
                        CURLOPT_POSTFIELDS => 'grant_type=refresh_token&client_id='.env('HUBSPOT_CLIENT_ID').'&client_secret='.env('HUBSPOT_CLIENT_SECRET').'&refresh_token='.$refresh_token,
                        //CURLOPT_POSTFIELDS => 'grant_type=refresh_token&client_id='.env('HUBSPOT_CLIENT_ID').'&client_secret='.env('HUBSPOT_CLIENT_SECRET').'&scopes=e-commercere&fresh_token='.$refresh_token,
                        CURLOPT_SSL_VERIFYHOST => 0,
                        CURLOPT_SSL_VERIFYPEER => 0,
                        CURLOPT_HTTPHEADER => array(
                            //"authkey: ".Auth::user()->auth_token,
                            "content-type: application/x-www-form-urlencoded;charset=utf-8"
                        ),
                    ));

                    $result = curl_exec($curl);
                    $err = curl_error($curl);
                    curl_close($curl);
                    if ($err) {
                      echo "cURL Error #:" . $err;
                    }
                    $result = json_decode($result);
                    if(isset($result->access_token)){
                        $tokens = $result->access_token;
                        $refresh_token = $result->refresh_token;
                        
                        // updated
                            $users  = User::find(Auth::user()->id);
                            $users->expires_at = time() + $result->expires_in * 0.95;
                            $users->refresh_token = $refresh_token;
                            $users->auth_token = $tokens;
                            //$users->save();
                        // refresh session
                    }
                    
                //}
            }
        } 
        
        return $tokens;
    }
    private function auth_access($auth_id){
        $user = User::whereId($auth_id)->first();
        Auth::login($user);
    }
    public function curlGet($url) {
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => $url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'GET',
          CURLOPT_HTTPHEADER => array(
            'Cookie: __cfduid=d15f14d1984c6b921f0319eb10ff9cce51606378583'
          ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return $response;
        
    }
    public function setting_store($hapi_key){

        // settings
        $args['url'] = 'https://api.hubapi.com/extensions/ecomm/v2/settings?hapikey='.$hapi_key;
        $args['data'] = '{
                            "enabled": true,
                            "webhookUri": "https://apps.webdew.com/hubspot/whmcs-ecommerce",
                            "mappings": {
                                "CONTACT": {
                                    "properties": [
                                        {
                                            "externalPropertyName": "firstname",
                                            "hubspotPropertyName": "firstname",
                                            "dataType": "STRING"
                                        },
                                        {
                                            "externalPropertyName": "familyname",
                                            "hubspotPropertyName": "lastname",
                                            "dataType": "STRING"
                                        },
                                        {
                                            "externalPropertyName": "customer_email",
                                            "hubspotPropertyName": "email",
                                            "dataType": "STRING"
                                        },
                                        {
                                            "externalPropertyName": "phone_number",
                                            "hubspotPropertyName": "mobilephone",
                                            "dataType": "STRING"
                                        }
                                    ]
                                },
                                "DEAL": {
                                    "properties": [
                                        {
                                            "externalPropertyName": "purchase_date",
                                            "hubspotPropertyName": "closedate",
                                            "dataType": "STRING"
                                        },
                                        {
                                            "externalPropertyName": "name",
                                            "hubspotPropertyName": "dealname",
                                            "dataType": "STRING"
                                        },
                                        {
                                            "externalPropertyName": "stage",
                                            "hubspotPropertyName": "dealstage",
                                            "dataType": "STRING"
                                        },
                                        {
                                            "externalPropertyName": "abandoned_cart_url",
                                            "hubspotPropertyName": "ip__ecomm_bride__abandoned_cart_url",
                                            "dataType": "STRING"
                                        }
                                    ]
                                },
                                "PRODUCT": {
                                    "properties": [
                                        {
                                            "externalPropertyName": "product_description",
                                            "hubspotPropertyName": "description",
                                            "dataType": "STRING"
                                        },
                                        {
                                            "externalPropertyName": "price",
                                            "hubspotPropertyName": "price",
                                            "dataType": "NUMBER"
                                        },
                                        {
                                            "externalPropertyName": "product_name",
                                            "hubspotPropertyName": "name",
                                            "dataType": "STRING"
                                        }
                                    ]
                                },
                                "LINE_ITEM": {
                                    "properties": [
                                        {
                                            "externalPropertyName": "tax_amount",
                                            "hubspotPropertyName": "tax",
                                            "dataType": "NUMBER"
                                        },
                                        {
                                            "externalPropertyName": "num_items",
                                            "hubspotPropertyName": "quantity",
                                            "dataType": "NUMBER"
                                        },
                                        {
                                            "externalPropertyName": "price",
                                            "hubspotPropertyName": "price",
                                            "dataType": "NUMBER"
                                        },
                                        {
                                            "externalPropertyName": "discount_amount",
                                            "hubspotPropertyName": "discount",
                                            "dataType": "NUMBER"
                                        }
                                    ]
                                }
                            }
                        }';
        $g = $this->curlAccess1('PUT', $args);

        $args['url'] = 'https://api.hubapi.com/extensions/ecomm/v2/stores?hapikey='.$hapi_key;
        $args['data'] = '{
                  "id": "ecommercebridge-whmcs-web-store",
                  "label": "WHMCS Ecommerce Bridge",
                  "adminUri": "https://apps.webdew.com/hubspot/whmcs-ecommerce-admin"
                }';
        $g = $this->curlAccess1('PUT',$args);


        // settings 
        
    }
    public function whmcs_products_hub() {

        $this->auth_access($_GET['auth_id']);
        $users = User::where('id', Auth::user()->id)->get();
        if(!empty($users)){
            foreach($users as $k => $v) {
               
                //$credential_all = unserialize($v->credentials->data);
                $credential_all = unserialize(DB::table('credentials')->where('type','whmcs')->whereUserId(Auth::user()->id)->value('data'));
               
                //getting portal id fro profile 
                $url='https://api.hubapi.com/integrations/v1/me?hapikey='.$credential_all['hapi_key'];
                $profile=$this->curlGet($url);
                $profile_data = json_decode($profile);
                $portal_id = $profile_data->portalId;
            
              
               //getting credentials from view.
                $array['validation'] =  [
                    'username' => 'required|min:3|max:50',
                    'password' => 'required|min:10|max:200',
                    'hapi_key' => 'required|min:10|max:200',
                ];
                
                /* call function*/
                $v = \Validator::make($credential_all, $array['validation']);
                if ($v->fails()) {
                    $errors = $v->errors();
                    $res = ['status' => false, 'message' => 'Credentials should be updated.'];
                    return $res;
                } 
                $username = $credential_all['username'];
                $password = $credential_all['password'];
                $hapi_key = $credential_all['hapi_key'];

                // $refresh_token_auth = DB::table('application_records')->where('portal_id',$portal_id)->where('user_id',Auth::user()->id)->value('refresh_token');
                // if($refresh_token_auth){
                //     $args['token'] = $this->refresh_token_m($refresh_token_auth);
                // }
                // print_r($args);
                // die;
                // setting store
                $this->setting_store($hapi_key);
                // setting store

                // Get products start
                $orderfields = array(
                    'action' => 'GetProducts',
                    'username' => $username,
                    'password' => $password,
                    'responsetype' => 'json'
                );
                $args['data'] = $orderfields;
                $result = $this->curlAccessWHMCS('POST', $args);


                if(!empty($result)){
                    // Get products end
                    if($result->result != 'error'){
                         
                        foreach($result->products->product as $key => $val) {
                          
                            $pricing_list = json_decode(json_encode($val->pricing), true);
                            
                            $pid = $val->pid;
                            $gid = $val->gid;
                            $name = $val->name;
                            $payType = $val->paytype;
                            $description = $val->description;
                            $currency = 'INR';
                            foreach($pricing_list as $p => $pv){
                                $currency = $p;
                                $monthly_price = $pv['monthly'];
                                $quarterly_price = $pv['quarterly'];
                                $semiannually_price = $pv['semiannually'];
                                $annually_price = $pv['annually'];
                                $biennially_price = $pv['biennially'];
                                $triennially_price = $pv['triennially'];
                            }

                            if($monthly_price < 0){
                                $monthly_price = 0;
                            }
      
                            // $id = DB::table('whmcs_sync_products')->where('whmcs_id',$pid)->where('app_user_id', Auth::user()->id)->where('portal_id',$portal_id)->value('id');
                            $id = DB::table('whmcs_sync_products')->where('whmcs_id',$pid)->where('portal_id',$portal_id)->value('id');
                         
                            if(empty($id) ){
                          
                                    // $product_id = DB::table('whmcs_sync_products')->insertGetId(
                                    //     ['whmcs_id' => $pid , 'hub_id' => 0, 'product_name' => $name, 'app_user_id' => Auth::user()->id,'portal_id' => $portal_id]
                                    // );
                                    $product_id = DB::table('whmcs_sync_products')->insertGetId(
                                        ['whmcs_id' => $pid , 'hub_id' => 0, 'product_name' => $name ,'portal_id' => $portal_id,'synced_user_id' => Auth::user()->id]
                                    );

                                    $args['url'] = "https://api.hubapi.com/crm-objects/v1/objects/products?hapikey=".$hapi_key;

                                    $args['data'] = '[
                                                          {
                                                            "name": "name",
                                                            "value": "'.$name.'"
                                                          },
                                                          {
                                                            "name": "description",
                                                            "value": "'.$description.'"
                                                          },
                                                          {
                                                            "name": "price",
                                                            "value": "'.$monthly_price.'"
                                                          },
                                                          {
                                                            "name": "recurringbillingfrequency",
                                                            "value": "monthly"
                                                          }
                                                        ]';
                                    $result = $this->curlAccessHS('POST', $args);

                                if(!empty($result)){
                                    if(isset($result->status) == 'error'){
                                        $res = ['status' => 'false', 'message' => $result->message ];
                                        return $res;
                                    } else {
                                        $success = DB::table('whmcs_sync_products')->where('id',$product_id)->update(['hub_id' => $result->objectId]);

                                        // insert product into ecommerce bridge

                                            $args1['url'] = 'https://api.hubapi.com/extensions/ecomm/v2/sync/messages?hapikey='.$hapi_key;

                                            $args1['data'] = '{
                                                      "storeId": "ecommercebridge-whmcs-web-store",
                                                      "objectType": "PRODUCT",
                                                      "messages": [
                                                        {
                                                          "action": "UPSERT",
                                                          "externalObjectId": "'.$result->objectId.'",
                                                          "properties": {
                                                            "price": "'.$monthly_price.'",
                                                            "product_name": "'.$name.'",
                                                            "product_description": "'.$description.'"
                                                          }
                                                        }
                                                      ]
                                                    }';
                                            $result1 = $this->curlAccess1('PUT', $args1);

                                        // insert product into ecommerce bridge

                                    }
                                } else {
                                    $res = ['status' => 'false', 'message' => 'Product not updated properly' ];
                                    return $res;
                                }
                                    
                            } else {
                                
                                // $object_id = DB::table('whmcs_sync_products')->where('whmcs_id',$pid)->where('app_user_id', Auth::user()->id)->where('portal_id',$portal_id)->value('hub_id');

                                $object_id = DB::table('whmcs_sync_products')->where('whmcs_id',$pid)->where('portal_id',$portal_id)->value('hub_id');

                                //getting hub id from whmcs product sync table where whmcs_id=product_id and app_user_id = hub_id 
                   
                                $args['url'] = "https://api.hubapi.com/crm-objects/v1/objects/products/".$object_id."?hapikey=".$hapi_key;

                                $args['data'] = '[
                                                    {
                                                        "name": "name",
                                                        "value": "'.$name.'"
                                                    },
                                                    {
                                                        "name": "description",
                                                        "value": "'.$description.'"
                                                    },
                                                    {
                                                        "name": "price",
                                                        "value": "'.$monthly_price.'"
                                                    },
                                                    {
                                                        "name": "recurringbillingfrequency",
                                                        "value": "monthly"
                                                    }
                                                ]';
                            
                                $result = $this->curlAccessHS('PUT', $args);

                                // insert product into ecommerce bridge

                                    $args1['url'] = 'https://api.hubapi.com/extensions/ecomm/v2/sync/messages?hapikey='.$hapi_key;

                                    $args1['data'] = '{
                                              "storeId": "ecommercebridge-whmcs-web-store",
                                              "objectType": "PRODUCT",
                                              "messages": [
                                                {
                                                  "action": "UPSERT",
                                                  "externalObjectId": "'.$object_id.'",
                                                  "properties": {
                                                    "price": "'.$monthly_price.'",
                                                    "product_name": "'.$name.'",
                                                    "product_description": "'.$description.'"
                                                  }
                                                }
                                              ]
                                            }';
                                    $result1 = $this->curlAccess1('PUT', $args1);
                                // insert product into ecommerce bridge

                                if(!empty($result)){
                                    if(isset($result->status) == 'error'){
                                        $res = ['status' => 'false', 'message' => $result->message ];
                                        return $res;
                                    } else {

                                        $success = DB::table('whmcs_sync_products')
                                                    ->where('id',$id)
                                                    ->update([
                                                        'hub_id' => @$result->objectId,
                                                        'product_name' => @$name,
                                                        'synced_user_id' => Auth::user()->id
                                                    ]);
                                    }
                                } else {
                                    $res = ['status' => 'false', 'message' => 'Product not updated properly' ];
                                    return $res;
                                }
                            }
                        }
                    } else {
                        $res = ['status' => 'false', 'message' => $result->message ];
                        return $res;
                    }
                } else {
                    $res = ['status' => 'false', 'message' => 'No data found.' ];
                    return $res;
                }
            }
            $u = url('whmcs-contacts-hub');
            $html = '<button data-step="1"  class="btn btn-label-brand btn-bold sync" rel="contacts" data-url="'.$u.'">Import Contacts</button>';
            $res = ['status' => true, 'message' => 'Products has been import.','html' => $html];
            return $res;
        } else {
            $res = ['status' => 'false', 'message' => 'User not available.'];
            return $res;
        }
    }

    public function whmcs_contacts_hub(){
        $this->auth_access($_GET['auth_id']);
        $users = User::where('id', Auth::user()->id)->get();
       
        if(!empty($users)){
            foreach($users as $k => $v){
                //$credential_all = unserialize($v->credentials->data);
                $credential_all = unserialize(DB::table('credentials')->where('type','whmcs')->whereUserId(Auth::user()->id)->value('data'));

                $url='https://api.hubapi.com/integrations/v1/me?hapikey='.$credential_all['hapi_key'];
                $profile=$this->curlGet($url);
                $profile_data = json_decode($profile);
                $portal_id = $profile_data->portalId;
            

                
                $array['validation'] =  [
                    'username' => 'required|min:3|max:50',
                    'password' => 'required|min:10|max:200',
                    'hapi_key' => 'required|min:10|max:200',
                ];
                /* call function*/
                $v = \Validator::make($credential_all, $array['validation']);
                if ($v->fails()) {
                    $errors = $v->errors();
                    $res = ['status' => false, 'message' => 'Credentials should be updated.'];
                    return $res;
                } 
                $username = $credential_all['username'];
                $password = $credential_all['password'];
                $hapi_key = $credential_all['hapi_key'];
        
                // Get contacts start
                $orderfields = array(
                    'action' => 'GetClients',
                    'username' => $username,
                    'password' => $password,
                    'responsetype' => 'json',
                );
                $args['data'] = $orderfields;
                $result = $this->curlAccessWHMCS('POST', $args);        
                
                if(!empty($result)){
                    // Get products end
                    if($result->result != 'error'){
                        foreach($result->clients->client as $key => $val) {
                            
                            $cid = $val->id;
                            $email = $val->email;
                            $firstname = $val->firstname;
                            $lastname = $val->lastname;

                            // $id = DB::table('whmcs_contacts')->where('whmcs_id',$cid)->where('app_user_id', Auth::user()->id)->where('portal_id', $portal_id)->value('id');

                            $id = DB::table('whmcs_contacts')->where('whmcs_id',$cid)->where('portal_id', $portal_id)->value('id');

                            if(empty($id)){
                               
                                    $args['url'] = "https://api.hubapi.com/contacts/v1/contact/?hapikey=".$hapi_key;
                                    $args['data'] = '{
                                                        "properties": [
                                                            {
                                                                "property": "email",
                                                                "value": "'.$email.'"
                                                            },
                                                            {
                                                                "property": "firstname",
                                                                "value": "'.$firstname.'"
                                                            },
                                                            {
                                                                "property": "lastname",
                                                                "value": "'.$lastname.'"
                                                            }
                                                        ]
                                                    }';
                                    $result = $this->curlAccessHS('POST', $args);
                                        
                               
                                    $vid = 0;
                                    if(!empty($result->vid)){
                                        $vid = $result->vid;

                                        // contacts 
                                            $args1['url'] = 'https://api.hubapi.com/extensions/ecomm/v2/sync/messages?hapikey='.$hapi_key;
                                            $timestamp = time() * 1000;
                                            $args1['data'] = '{
                                                  "storeId": "ecommercebridge-whmcs-web-store",
                                                  "objectType": "CONTACT",
                                                  "messages": [
                                                    {
                                                      "action": "UPSERT",
                                                      "changedAt": "'. $timestamp .'",
                                                      "externalObjectId": "'. $vid .'",
                                                      "properties": {
                                                        "firstname": "' . $firstname . '",
                                                        "lastname": "' . $lastname . '",
                                                        "customer_email": "' .$email. '"
                                                      }
                                                    }
                                                  ]
                                                }';
                                            $result = $this->curlAccessHS('PUT', $args1);
                                        // contacts

                                    }
                                    $contact_id = DB::table('whmcs_contacts')->insertGetId(
                                        ['whmcs_id' => $cid , 'hub_id' => $vid, 'email' => $email, 'app_user_id' => Auth::user()->id,'portal_id' =>$portal_id ]
                                    );
                            } else {
                                    // $vid = DB::table('whmcs_contacts')->where('whmcs_id',$cid)->where('app_user_id', Auth::user()->id)->value('hub_id');

                                    $vid = DB::table('whmcs_contacts')->where('whmcs_id',$cid)->where('portal_id', $portal_id)->value('hub_id');

                                    $args['url'] = "https://api.hubapi.com/contacts/v1/contact/vid/".$vid."/profile?hapikey=".$hapi_key;
                                    $args['data'] = '{
                                                        "properties": [
                                                            {
                                                                "property": "email",
                                                                "value": "'.$email.'"
                                                            },
                                                            {
                                                                "property": "firstname",
                                                                "value": "'.$firstname.'"
                                                            },
                                                            {
                                                                "property": "lastname",
                                                                "value": "'.$lastname.'"
                                                            }
                                                        ]
                                                    }';
                                
                                    $result = $this->curlAccessHS('PUT', $args);
                                    $contact_id = DB::table('whmcs_contacts')->where('id',$id)->update(
                                        ['whmcs_id' => $cid , 'hub_id' => $vid, 'email' => $email, 'app_user_id' => Auth::user()->id,'portal_id' =>$portal_id]
                                    );

                                    // contacts 
                                        $args1['url'] = 'https://api.hubapi.com/extensions/ecomm/v2/sync/messages?hapikey='.$hapi_key;
                                            $timestamp = time() * 1000;
                                            $args1['data'] = '{
                                                  "storeId": "ecommercebridge-whmcs-web-store",
                                                  "objectType": "CONTACT",
                                                  "messages": [
                                                    {
                                                      "action": "UPSERT",
                                                      "changedAt": "'. $timestamp .'",
                                                      "externalObjectId": "'. $vid .'",
                                                      "properties": {
                                                        "firstname": "' . $firstname . '",
                                                        "lastname": "' . $lastname . '",
                                                        "customer_email": "' .$email. '"
                                                      }
                                                    }
                                                  ]
                                                }';
                                            $result = $this->curlAccessHS('PUT', $args1);
                                    // contacts
                            }
                        }
                    } else {
                        $res = ['status' => false, 'message' => $result->message ];
                        return $res;
                    }

                } else {
                    $res = ['status' => false, 'message' => 'No data found.' ];
                    return $res;
                }
            }
            $u    = url('whmcs-orders-hub');
            $html = '<button data-step="2"  class="btn btn-label-brand btn-bold sync" rel="orders" data-url="'.$u.'">Import Orders</button>';
            $res  = ['status' => true, 'message' => 'Contacts has been import','html' => $html];
            return $res;
        } else {
            $res = ['status' => false, 'message' => 'User not available.'];
            return $res;
        }
    }

    function deal_pipeline($hapikey) {

        $args['url'] = "https://api.hubapi.com/deals/v1/pipelines?hapikey=".$hapikey;
        $result = curlAccess( 'GET', $args );
        $pipeline_Id = 'default';
        foreach ($result as $key => $value) {
            if($value->label == 'Ecommerce Pipeline'){
                $pipeline_Id = $value->pipelineId;
            }    
        }
        return $pipeline_Id;
    }

    function deal_pipeline_DB($status) {
        $list = DB::table('whmcs_sync_pipelines')->where('whmcs_pipeline',$status)->where('fk_user_id',Auth::user()->id)->first();
        $list = json_decode(json_encode($list), true);
        if($list){
            $res['hubspot_pipeline'] = $list['hubspot_pipeline'];
            $res['hubspot_parent_pipeline'] = $list['hubspot_parent_pipeline'];
        }
        return $res;
    }

    public function whmcs_orders_hub(){
        $this->auth_access($_GET['auth_id']);
        $users = User::where('id', Auth::user()->id)->get();
        if(!empty($users)){
            foreach($users as $k => $v) {
                //$credential_all = unserialize($v->credentials->data);
                $credential_all = unserialize(DB::table('credentials')->where('type','whmcs')->whereUserId(Auth::user()->id)->value('data'));

                $url='https://api.hubapi.com/integrations/v1/me?hapikey='.$credential_all['hapi_key'];
                $profile=$this->curlGet($url);
                $profile_data = json_decode($profile);
                $portal_id = $profile_data->portalId;
                
                $array['validation'] =  [
                    'username' => 'required|min:3|max:50',
                    'password' => 'required|min:10|max:200',
                    'hapi_key' => 'required|min:10|max:200',
                ];
                /* call function*/
                $v = \Validator::make($credential_all, $array['validation']);
                if ($v->fails()) {
                    $errors = $v->errors();
                    $res = ['status' => false, 'message' => 'Credentials should be updated.'];
                    return $res;
                } 
                $username = $credential_all['username'];
                $password = $credential_all['password'];
                $hapi_key = $credential_all['hapi_key'];
        
        
                // Get orders start
                $orderfields = array(
                    'action' => 'GetOrders',
                    'username' => $username,
                    'password' => $password,
                    'responsetype' => 'json',
                );
                // Get orders stop
                $args['data'] = $orderfields;
                $result = $this->curlAccessWHMCS('POST', $args);
                
                if(!empty($result)){
                    if($result->result != 'error') {
                        foreach($result->orders->order as $key => $val) {
                            $user_id = $val->userid;
                            $amount = $val->amount;

                            $deal_pipeline_id = $this->deal_pipeline($hapi_key);
                            if($deal_pipeline_id == 'default'){
                                $deal_stage = 'appointmentscheduled';
                            } else {
                                if($val->status == 'Pending'){
                                    $deal_stage = 'checkout_pending';
                                } else {
                                    $deal_stage = 'checkout_completed';
                                }
                            }

                            
                            //$productid = DB::connection('mysql2')->table('tblhosting')->where('orderid',$val->id)->value('packageid');
                            if(!empty($val->lineitems->lineitem)){
                                foreach($val->lineitems->lineitem as $key2 => $val2) {
                                    $array = explode("-",$val2->product);
                                    
                                    // $productid = DB::table('whmcs_sync_products')->where('product_name', 'LIKE' ,'%'.trim($array[1]).'%')->where('app_user_id', Auth::user()->id)->where('portal_id',$portal_id)->value('whmcs_id');

                                    $productid = DB::table('whmcs_sync_products')->where('product_name', 'LIKE' ,'%'.trim($array[1]).'%')->where('portal_id',$portal_id)->value('whmcs_id');
                                    
                                    // $id = DB::table('whmcs_sync_order_products')->where('product_id', $productid)->where('user_id', $user_id)->where('app_user_id', Auth::user()->id)->where('order_id', $val->id)->where('portal_id',$portal_id)->value('id');

                                    $id = DB::table('whmcs_sync_order_products')->where('product_id', $productid)->where('user_id', $user_id)->where('order_id', $val->id)->where('portal_id',$portal_id)->value('id');
                                    
                                    // $hub_product_id = DB::table('whmcs_sync_products')->where('whmcs_id', $productid)->where('app_user_id', Auth::user()->id)->where('portal_id',$portal_id)->value('hub_id');

                                    $hub_product_id = DB::table('whmcs_sync_products')->where('whmcs_id', $productid)->where('portal_id',$portal_id)->value('hub_id');

                                    
                                    // $product = DB::table('whmcs_sync_products')->where('whmcs_id', $productid)->where('app_user_id', Auth::user()->id)->where('portal_id',$portal_id)->value('product_name');

                                    $product = DB::table('whmcs_sync_products')->where('whmcs_id', $productid)->where('portal_id',$portal_id)->value('product_name');
                                    
                                    // $vid = DB::table('whmcs_contacts')->where('whmcs_id', $user_id)->where('app_user_id', Auth::user()->id)->where('portal_id',$portal_id)->value('hub_id');

                                    $vid = DB::table('whmcs_contacts')->where('whmcs_id', $user_id)->where('portal_id',$portal_id)->value('hub_id');
                                    
                                    $timestamp = time() * 1000;
                                    
                                    if(empty($id)){
                                        //appointmentscheduled

                                        // add to cart
                                        $args['url'] = "https://api.hubapi.com/deals/v1/deal?hapikey=".$hapi_key;
                                        $args['data'] = '{
                                            "associations": {
                                            "associatedCompanyIds": [],
                                            "associatedVids": [
                                                '.$vid.'
                                            ]
                                            },
                                            "properties": [
                                            {
                                                "value": "'.$product.'",
                                                "name": "dealname"
                                            },
                                            {
                                                "value": "'.$deal_stage.'",
                                                "name": "dealstage"
                                            },
                                            {
                                                "value": "'.$deal_pipeline_id.'",
                                                "name": "pipeline"
                                            },
                                            {
                                                "value": '.$timestamp.',
                                                "name": "closedate"
                                            },
                                            {
                                                "value": "'.$amount.'",
                                                "name": "amount"
                                            },
                                            {
                                                "value": "newbusiness",
                                                "name": "dealtype"
                                            }
                                            ]
                                        }';

                                        $result2 = $this->curlAccessHS('POST', $args);
                                       
                                        
                                        $deal_id = $result2->dealId;


                                        // deals
                                            $args1['url'] = 'https://api.hubapi.com/extensions/ecomm/v2/sync/messages?hapikey='.$hapi_key;
                                            $args1['data'] = '{
                                                  "storeId": "ecommercebridge-whmcs-web-store",
                                                  "objectType": "DEAL",
                                                  "messages": [
                                                    {
                                                      "action": "UPSERT",
                                                      "changedAt": "'. $timestamp .'",
                                                      "externalObjectId": "'.$deal_id.'",
                                                      "properties": {
                                                        "name": "'.$product.'",
                                                        "stage": "'.$deal_stage.'",
                                                        "purchase_date": "'. $timestamp .'"
                                                      },
                                                      "associations": {
                                                        "CONTACT": [
                                                          "'.$vid.'"
                                                        ]
                                                      }
                                                    }
                                                  ]
                                                }';
                                            $result2 = $this->curlAccessHS('PUT', $args1);
                                        // deals

                                        // line items
                                        $args['url'] = "https://api.hubapi.com/crm-objects/v1/objects/line_items?hapikey=".$hapi_key;
                                        $args['data'] = '[
                                                            {
                                                                "name": "hs_product_id",
                                                                "value": "'.$hub_product_id.'"
                                                            },
                                                            {
                                                                "name": "quantity",
                                                                "value": "1"
                                                            },
                                                            {
                                                                "name": "price",
                                                                "value": "'.$amount.'"
                                                            },
                                                            {
                                                                "name": "name",
                                                                "value": "'.$product.'"
                                                            }
                                                        ]';
                                        $result2 = $this->curlAccessHS('POST', $args);
                                        $line_item_id = $result2->objectId;


                                        // line items
                                            $args1['url'] = 'https://api.hubapi.com/extensions/ecomm/v2/sync/messages?hapikey='.$hapi_key;
                                            $args1['data'] = '{
                                                  "storeId": "ecommercebridge-whmcs-web-store",
                                                  "objectType": "LINE_ITEM",
                                                  "messages": [
                                                    {
                                                      "action": "UPSERT",
                                                      "changedAt": "' . $timestamp . '",
                                                      "externalObjectId": "'. $line_item_id .'",
                                                      "properties": {
                                                        "price": "'.$amount.'",
                                                        "num_items": "1",
                                                        "tax_amount": "0"
                                                      },
                                                      "associations": {
                                                        "DEAL": [
                                                          "'.$deal_id.'"
                                                        ],
                                                        "PRODUCT": [
                                                          "'.$productid.'"
                                                        ]
                                                      }
                                                    }
                                                  ]
                                                }';
                                            $result2 = $this->curlAccessHS('PUT', $args1);
                                        // line items
                                        
                                        
                                        $args['url'] = 'https://api.hubapi.com/crm-associations/v1/associations?hapikey='.$hapi_key;
                                        $args['data'] = '{
                                                            "fromObjectId":'.$deal_id.',
                                                            "toObjectId":'. $line_item_id.',
                                                            "category" : "HUBSPOT_DEFINED",
                                                            "definitionId": 19
                                                        }';
                                                        
                                        $result2 = $this->curlAccessHS('PUT', $args);
                                        
                                        $order_product_id = DB::table('whmcs_sync_order_products')->insertGetId(
                                            ['product_id' => $productid , 'order_id' => $val->id, 'user_id' => $user_id, 'hub_add_to_cart_id' => $deal_id, 'hub_lineitem_id' => $line_item_id, 'app_user_id' => Auth::user()->id,'portal_id' => $portal_id]
                                        );

                                    } else {

                                        $p_id = DB::table('whmcs_sync_order_products')->where('product_id', $productid)->where('user_id', $user_id)->where('order_id', $val->id)->where('portal_id',$portal_id)->value('product_id');

                                        if($p_id > 0) {

                                            $deal_id = DB::table('whmcs_sync_order_products')->where('product_id', $productid)->where('user_id', $user_id)->where('order_id', $val->id)->where('portal_id',$portal_id)->value('hub_add_to_cart_id');

                                            $line_item_id = DB::table('whmcs_sync_order_products')->where('product_id', $productid)->where('user_id', $user_id)->where('order_id', $val->id)->where('portal_id',$portal_id)->value('hub_lineitem_id');

                                            // add to cart
                                            $args['url'] = "https://api.hubapi.com/deals/v1/deal/".$deal_id."?hapikey=".$hapi_key;
                                            $args['data'] = '{
                                                "associations": {
                                                "associatedCompanyIds": [],
                                                "associatedVids": [
                                                    '.$vid.'
                                                ]
                                                },
                                                "properties": [
                                                {
                                                    "value": "'.$product.'",
                                                    "name": "dealname"
                                                },
                                                {
                                                    "value": "'.$deal_stage.'",
                                                    "name": "dealstage"
                                                },
                                                {
                                                    "value": "'.$deal_pipeline_id.'",
                                                    "name": "pipeline"
                                                },
                                                {
                                                    "value": '.$timestamp.',
                                                    "name": "closedate"
                                                },
                                                {
                                                    "value": "'.$amount.'",
                                                    "name": "amount"
                                                },
                                                {
                                                    "value": "newbusiness",
                                                    "name": "dealtype"
                                                }
                                                ]
                                            }';

                                            $result2 = $this->curlAccessHS('PUT', $args);
                                            

                                            // deals
                                                $args1['url'] = 'https://api.hubapi.com/extensions/ecomm/v2/sync/messages?hapikey='.$hapi_key;
                                                $args1['data'] = '{
                                                      "storeId": "ecommercebridge-whmcs-web-store",
                                                      "objectType": "DEAL",
                                                      "messages": [
                                                        {
                                                          "action": "UPSERT",
                                                          "changedAt": "'. $timestamp .'",
                                                          "externalObjectId": "'.$deal_id.'",
                                                          "properties": {
                                                            "name": "'.$product.'",
                                                            "stage": "'.$deal_stage.'",
                                                            "purchase_date": "'. $timestamp .'"
                                                          },
                                                          "associations": {
                                                            "CONTACT": [
                                                              "'.$vid.'"
                                                            ]
                                                          }
                                                        }
                                                      ]
                                                    }';
                                                $result2 = $this->curlAccessHS('PUT', $args1);
                                            // deals

                                            // line items
                                            $args['url'] = "https://api.hubapi.com/crm-objects/v1/objects/line_items/".$line_item_id."?hapikey=".$hapi_key;
                                            $args['data'] = '[
                                                                {
                                                                    "name": "hs_product_id",
                                                                    "value": "'.$hub_product_id.'"
                                                                },
                                                                {
                                                                    "name": "quantity",
                                                                    "value": "1"
                                                                },
                                                                {
                                                                    "name": "price",
                                                                    "value": "'.$amount.'"
                                                                },
                                                                {
                                                                    "name": "name",
                                                                    "value": "'.$product.'"
                                                                }
                                                            ]';
                                            $result2 = $this->curlAccessHS('PUT', $args);
                                            


                                            // line items
                                                $args1['url'] = 'https://api.hubapi.com/extensions/ecomm/v2/sync/messages?hapikey='.$hapi_key;
                                                $args1['data'] = '{
                                                      "storeId": "ecommercebridge-whmcs-web-store",
                                                      "objectType": "LINE_ITEM",
                                                      "messages": [
                                                        {
                                                          "action": "UPSERT",
                                                          "changedAt": "' . $timestamp . '",
                                                          "externalObjectId": "'. $line_item_id .'",
                                                          "properties": {
                                                            "price": "'.$amount.'",
                                                            "num_items": "1",
                                                            "tax_amount": "0"
                                                          },
                                                          "associations": {
                                                            "DEAL": [
                                                              "'.$deal_id.'"
                                                            ],
                                                            "PRODUCT": [
                                                              "'.$p_id.'"
                                                            ]
                                                          }
                                                        }
                                                      ]
                                                    }';
                                                $result2 = $this->curlAccessHS('PUT', $args1);
                                            // line items
                                            
                                            
                                            $args['url'] = 'https://api.hubapi.com/crm-associations/v1/associations?hapikey='.$hapi_key;
                                            $args['data'] = '{
                                                                "fromObjectId":"'.$deal_id.'",
                                                                "toObjectId":"'. $line_item_id.'",
                                                                "category" : "HUBSPOT_DEFINED",
                                                                "definitionId": 19
                                                            }';
                                                            
                                            $results = $this->curlAccessHS('PUT', $args);
                                            
                                            $order_product_id = DB::table('whmcs_sync_order_products')->where('id',$id)->update(
                                                ['product_id' => $productid , 'order_id' => $val->id, 'user_id' => $user_id, 'hub_add_to_cart_id' => $deal_id, 'hub_lineitem_id' => $line_item_id, 'app_user_id' => Auth::user()->id,'portal_id' => $portal_id]
                                            );

                                        }

                                        
                                    }

                                } 
                            } else {
                                // $hub_product_id = DB::table('whmcs_sync_products')->where('whmcs_id', $productid)->where('app_user_id', Auth::user()->id)->where('portal_id',$portal_id)->value('hub_id');


                                $hub_product_id = DB::table('whmcs_sync_products')->where('whmcs_id', $productid)->where('portal_id',$portal_id)->value('hub_id');

                                // $id = DB::table('whmcs_sync_order_products')->where('user_id', $user_id)->where('app_user_id', Auth::user()->id)->where('order_id', $val->id)->where('portal_id',$portal_id)->value('id');

                                $id = DB::table('whmcs_sync_order_products')->where('user_id', $user_id)->where('order_id', $val->id)->where('portal_id',$portal_id)->value('id');

                                if(empty($id)){
                                    $order_product_id = DB::table('whmcs_sync_order_products')->insertGetId(
                                        ['product_id' => 0 , 'order_id' => $val->id, 'user_id' => $user_id, 'app_user_id'=> Auth::user()->id,'portal_id' => $portal_id]
                                    );
                                }
                            }   
                        }
                    } else {
                        $res = ['status' => false, 'message' => $result->message ];
                        return $res;
                    }

                } else {
                    $res = ['status' => false, 'message' => 'No data found.' ];
                    return $res;
                }
            }
            
            $html = '<button data-step="3" class="btn btn-label-brand btn-bold finish">Success</button>';
            $res = ['status' => true, 'message' => 'Orders has been import.','html' => $html];
            return $res;
        } else {
            
            $res = ['status' => false, 'message' => 'User not available.'];
            return $res;
        }
    }
    public function import_status($time, $pipeline){
        $id = WhmcsImport::where('fk_user_id', Auth::user()->id)->value('id');
        if($id > 0){
            WhmcsImport::where('id', $id)->update([
                    'pipeline' => $pipeline,
                    'fk_user_id' => Auth::user()->id,
                    'status'     => 1
                ]);
        } else {
            WhmcsImport::create([
                    'pipeline' => $pipeline,
                    'fk_user_id' => Auth::user()->id,
                    'status'     => 1
                ]);
        }
    }
    public function whmcs_products() {

        $users = User::where('id', Auth::user()->id)->get();
        if(!empty($users)){
            foreach($users as $k => $v) {
               
                //$credential_all = unserialize($v->credentials->data);
                $credential_all = unserialize(DB::table('credentials')->where('type','whmcs')->whereUserId(Auth::user()->id)->value('data'));
               
                //getting portal id fro profile 
                $url='https://api.hubapi.com/integrations/v1/me?hapikey='.$credential_all['hapi_key'];
                $profile=$this->curlGet($url);
                $profile_data = json_decode($profile);
                $portal_id = $profile_data->portalId;
            
              
               //getting credentials from view.
                $array['validation'] =  [
                    'username' => 'required|min:3|max:50',
                    'password' => 'required|min:10|max:200',
                    'hapi_key' => 'required|min:10|max:200',
                ];
                
                /* call function*/
                $v = \Validator::make($credential_all, $array['validation']);
                if ($v->fails()) {
                    $errors = $v->errors();
                    $res = ['status' => false, 'message' => 'Credentials should be updated.'];
                    return $res;
                } 
                $username = $credential_all['username'];
                $password = $credential_all['password'];
                $hapi_key = $credential_all['hapi_key'];

                // $refresh_token_auth = DB::table('application_records')->where('portal_id',$portal_id)->where('user_id',Auth::user()->id)->value('refresh_token');
                // if($refresh_token_auth){
                //     $args['token'] = $this->refresh_token_m($refresh_token_auth);
                // }
                // print_r($args);
                // die;
                // setting store
                $this->setting_store($hapi_key);
                // setting store

                // Get products start
                $orderfields = array(
                    'action' => 'GetProducts',
                    'username' => $username,
                    'password' => $password,
                    'responsetype' => 'json'
                );
                $args['data'] = $orderfields;
                $result = $this->curlAccessWHMCS('POST', $args);

                if(!empty($result)) {
                    // Get products end
                    if($result->result != 'error') {

                        foreach($result->products->product as $key => $val) {
                          
                            $pricing_list = json_decode(json_encode($val->pricing), true);
                            
                            $pid = $val->pid;
                            $gid = $val->gid;
                            $name = $val->name;
                            $payType = $val->paytype;
                            $description = $val->description;
                            $currency = 'INR';
                            foreach($pricing_list as $p => $pv){
                                $currency = $p;
                                $monthly_price = $pv['monthly'];
                                $quarterly_price = $pv['quarterly'];
                                $semiannually_price = $pv['semiannually'];
                                $annually_price = $pv['annually'];
                                $biennially_price = $pv['biennially'];
                                $triennially_price = $pv['triennially'];
                            }

                            if($monthly_price < 0){
                                $monthly_price = 0;
                            }
      
                            // $id = DB::table('whmcs_sync_products')->where('whmcs_id',$pid)->where('app_user_id', Auth::user()->id)->where('portal_id',$portal_id)->value('id');
                            $id = DB::table('whmcs_sync_products')->where('whmcs_id',$pid)->where('portal_id',$portal_id)->value('id');
                         
                            if(empty($id) ){
                          
                                    // $product_id = DB::table('whmcs_sync_products')->insertGetId(
                                    //     ['whmcs_id' => $pid , 'hub_id' => 0, 'product_name' => $name, 'app_user_id' => Auth::user()->id,'portal_id' => $portal_id]
                                    // );
                                    $product_id = DB::table('whmcs_sync_products')->insertGetId(
                                        ['whmcs_id' => $pid , 'hub_id' => 0, 'product_name' => $name ,'portal_id' => $portal_id,'synced_user_id' => Auth::user()->id]
                                    );

                                    $args['url'] = "https://api.hubapi.com/crm-objects/v1/objects/products?hapikey=".$hapi_key;

                                    $args['data'] = '[
                                                          {
                                                            "name": "name",
                                                            "value": "'.$name.'"
                                                          },
                                                          {
                                                            "name": "description",
                                                            "value": "'.$description.'"
                                                          },
                                                          {
                                                            "name": "price",
                                                            "value": "'.$monthly_price.'"
                                                          },
                                                          {
                                                            "name": "recurringbillingfrequency",
                                                            "value": "monthly"
                                                          }
                                                        ]';
                                    $result = $this->curlAccessHS('POST', $args);

                                if(!empty($result)){
                                    if(isset($result->status) == 'error'){
                                        $res = ['status' => 'false', 'message' => $result->message ];
                                        return $res;
                                    } else {
                                        $success = DB::table('whmcs_sync_products')->where('id',$product_id)->update(['hub_id' => $result->objectId]);

                                        // insert product into ecommerce bridge

                                            $args1['url'] = 'https://api.hubapi.com/extensions/ecomm/v2/sync/messages?hapikey='.$hapi_key;

                                            $args1['data'] = '{
                                                      "storeId": "ecommercebridge-whmcs-web-store",
                                                      "objectType": "PRODUCT",
                                                      "messages": [
                                                        {
                                                          "action": "UPSERT",
                                                          "externalObjectId": "'.$result->objectId.'",
                                                          "properties": {
                                                            "price": "'.$monthly_price.'",
                                                            "product_name": "'.$name.'",
                                                            "product_description": "'.$description.'"
                                                          }
                                                        }
                                                      ]
                                                    }';
                                            $result1 = $this->curlAccess1('PUT', $args1);

                                        // insert product into ecommerce bridge

                                    }
                                } else {
                                    $res = ['status' => 'false', 'message' => 'Product not updated properly' ];
                                    return $res;
                                }
                                    
                            } else {
                                
                                // $object_id = DB::table('whmcs_sync_products')->where('whmcs_id',$pid)->where('app_user_id', Auth::user()->id)->where('portal_id',$portal_id)->value('hub_id');

                                $object_id = DB::table('whmcs_sync_products')->where('whmcs_id',$pid)->where('portal_id',$portal_id)->value('hub_id');

                                //getting hub id from whmcs product sync table where whmcs_id=product_id and app_user_id = hub_id 
                   
                                $args['url'] = "https://api.hubapi.com/crm-objects/v1/objects/products/".$object_id."?hapikey=".$hapi_key;

                                $args['data'] = '[
                                                    {
                                                        "name": "name",
                                                        "value": "'.$name.'"
                                                    },
                                                    {
                                                        "name": "description",
                                                        "value": "'.$description.'"
                                                    },
                                                    {
                                                        "name": "price",
                                                        "value": "'.$monthly_price.'"
                                                    },
                                                    {
                                                        "name": "recurringbillingfrequency",
                                                        "value": "monthly"
                                                    }
                                                ]';
                            
                                $result = $this->curlAccessHS('PUT', $args);

                                // insert product into ecommerce bridge

                                    $args1['url'] = 'https://api.hubapi.com/extensions/ecomm/v2/sync/messages?hapikey='.$hapi_key;

                                    $args1['data'] = '{
                                              "storeId": "ecommercebridge-whmcs-web-store",
                                              "objectType": "PRODUCT",
                                              "messages": [
                                                {
                                                  "action": "UPSERT",
                                                  "externalObjectId": "'.$object_id.'",
                                                  "properties": {
                                                    "price": "'.$monthly_price.'",
                                                    "product_name": "'.$name.'",
                                                    "product_description": "'.$description.'"
                                                  }
                                                }
                                              ]
                                            }';
                                    $result1 = $this->curlAccess1('PUT', $args1);
                                // insert product into ecommerce bridge

                                if(!empty($result)){
                                    if(isset($result->status) == 'error'){
                                        $res = ['status' => 'false', 'message' => $result->message ];
                                        return $res;
                                    } else {

                                        $success = DB::table('whmcs_sync_products')
                                                    ->where('id',$id)
                                                    ->update([
                                                        'hub_id' => @$result->objectId,
                                                        'product_name' => @$name,
                                                        'synced_user_id' => Auth::user()->id
                                                    ]);
                                    }
                                } else {
                                    $res = ['status' => 'false', 'message' => 'Product not updated properly' ];
                                    return $res;
                                }
                            }
                        }

                        $this->import_status(time(), 1);
                    } else {
                        $res = ['status' => 'false', 'message' => $result->message ];
                        return $res;
                    }
                } else {
                    $res = ['status' => 'false', 'message' => 'No data found.' ];
                    return $res;
                }
            }
            $u = url('whmcs-contacts');
            $html = '<button data-step="1" class="btn btn-label-brand btn-bold sync" rel="contacts" data-url="'.$u.'">Import Contacts</button>';
            $res = ['status' => true, 'name' => 'product', 'message' => 'Products has been import.','html' => $html];
            return $res;
        } else {
            $res = ['status' => 'false', 'name' => 'product', 'message' => 'User not available.'];
            return $res;
        }
    }
    public function whmcs_contacts(){
        $users = User::where('id', Auth::user()->id)->get();
       
        if(!empty($users)){
            foreach($users as $k => $v){
                //$credential_all = unserialize($v->credentials->data);
                $credential_all = unserialize(DB::table('credentials')->where('type','whmcs')->whereUserId(Auth::user()->id)->value('data'));

                $url='https://api.hubapi.com/integrations/v1/me?hapikey='.$credential_all['hapi_key'];
                $profile=$this->curlGet($url);
                $profile_data = json_decode($profile);
                $portal_id = $profile_data->portalId;
            

                
                $array['validation'] =  [
                    'username' => 'required|min:3|max:50',
                    'password' => 'required|min:10|max:200',
                    'hapi_key' => 'required|min:10|max:200',
                ];
                /* call function*/
                $v = \Validator::make($credential_all, $array['validation']);
                if ($v->fails()) {
                    $errors = $v->errors();
                    $res = ['status' => false, 'message' => 'Credentials should be updated.'];
                    return $res;
                } 
                $username = $credential_all['username'];
                $password = $credential_all['password'];
                $hapi_key = $credential_all['hapi_key'];
        
                // Get contacts start
                $orderfields = array(
                    'action' => 'GetClients',
                    'username' => $username,
                    'password' => $password,
                    'responsetype' => 'json',
                );
                $args['data'] = $orderfields;
                $result = $this->curlAccessWHMCS('POST', $args);        
                
                if(!empty($result)){
                    // Get products end
                    if($result->result != 'error'){
                        foreach($result->clients->client as $key => $val) {
                            
                            $cid = $val->id;
                            $email = $val->email;
                            $firstname = $val->firstname;
                            $lastname = $val->lastname;

                            // $id = DB::table('whmcs_contacts')->where('whmcs_id',$cid)->where('app_user_id', Auth::user()->id)->where('portal_id', $portal_id)->value('id');

                            $id = DB::table('whmcs_contacts')->where('whmcs_id',$cid)->where('portal_id', $portal_id)->value('id');

                            if(empty($id)){
                               
                                    $args['url'] = "https://api.hubapi.com/contacts/v1/contact/?hapikey=".$hapi_key;
                                    $args['data'] = '{
                                                        "properties": [
                                                            {
                                                                "property": "email",
                                                                "value": "'.$email.'"
                                                            },
                                                            {
                                                                "property": "firstname",
                                                                "value": "'.$firstname.'"
                                                            },
                                                            {
                                                                "property": "lastname",
                                                                "value": "'.$lastname.'"
                                                            }
                                                        ]
                                                    }';
                                    $result = $this->curlAccessHS('POST', $args);
                                        
                               
                                    $vid = 0;
                                    if(!empty($result->vid)){
                                        $vid = $result->vid;

                                        // contacts 
                                            $args1['url'] = 'https://api.hubapi.com/extensions/ecomm/v2/sync/messages?hapikey='.$hapi_key;
                                            $timestamp = time() * 1000;
                                            $args1['data'] = '{
                                                  "storeId": "ecommercebridge-whmcs-web-store",
                                                  "objectType": "CONTACT",
                                                  "messages": [
                                                    {
                                                      "action": "UPSERT",
                                                      "changedAt": "'. $timestamp .'",
                                                      "externalObjectId": "'. $vid .'",
                                                      "properties": {
                                                        "firstname": "' . $firstname . '",
                                                        "lastname": "' . $lastname . '",
                                                        "customer_email": "' .$email. '"
                                                      }
                                                    }
                                                  ]
                                                }';
                                            $result = $this->curlAccessHS('PUT', $args1);
                                        // contacts

                                    }
                                    $contact_id = DB::table('whmcs_contacts')->insertGetId(
                                        ['whmcs_id' => $cid , 'hub_id' => $vid, 'email' => $email, 'app_user_id' => Auth::user()->id,'portal_id' =>$portal_id ]
                                    );
                            } else {
                                    // $vid = DB::table('whmcs_contacts')->where('whmcs_id',$cid)->where('app_user_id', Auth::user()->id)->value('hub_id');

                                    $vid = DB::table('whmcs_contacts')->where('whmcs_id',$cid)->where('portal_id', $portal_id)->value('hub_id');

                                    $args['url'] = "https://api.hubapi.com/contacts/v1/contact/vid/".$vid."/profile?hapikey=".$hapi_key;
                                    $args['data'] = '{
                                                        "properties": [
                                                            {
                                                                "property": "email",
                                                                "value": "'.$email.'"
                                                            },
                                                            {
                                                                "property": "firstname",
                                                                "value": "'.$firstname.'"
                                                            },
                                                            {
                                                                "property": "lastname",
                                                                "value": "'.$lastname.'"
                                                            }
                                                        ]
                                                    }';
                                
                                    $result = $this->curlAccessHS('PUT', $args);
                                    $contact_id = DB::table('whmcs_contacts')->where('id',$id)->update(
                                        ['whmcs_id' => $cid , 'hub_id' => $vid, 'email' => $email, 'app_user_id' => Auth::user()->id,'portal_id' =>$portal_id]
                                    );

                                    // contacts 
                                        $args1['url'] = 'https://api.hubapi.com/extensions/ecomm/v2/sync/messages?hapikey='.$hapi_key;
                                            $timestamp = time() * 1000;
                                            $args1['data'] = '{
                                                  "storeId": "ecommercebridge-whmcs-web-store",
                                                  "objectType": "CONTACT",
                                                  "messages": [
                                                    {
                                                      "action": "UPSERT",
                                                      "changedAt": "'. $timestamp .'",
                                                      "externalObjectId": "'. $vid .'",
                                                      "properties": {
                                                        "firstname": "' . $firstname . '",
                                                        "lastname": "' . $lastname . '",
                                                        "customer_email": "' .$email. '"
                                                      }
                                                    }
                                                  ]
                                                }';
                                            $result = $this->curlAccessHS('PUT', $args1);
                                    // contacts
                            }
                        }

                        $this->import_status(time(), 2);

                    } else {
                        $res = ['status' => false, 'name' => 'orders', 'message' => $result->message ];
                        return $res;
                    }

                } else {
                    $res = ['status' => false, 'name' => 'orders', 'message' => 'No data found.' ];
                    return $res;
                }
            }
            $u    = url('whmcs-orders');
            $html = '<button data-step="2"  class="btn btn-label-brand btn-bold sync" rel="orders" data-url="'.$u.'">Import Orders</button>';
            $res  = ['status' => true, 'name' => 'contact', 'message' => 'Contacts has been Import','html' => $html];
            return $res;
        } else {
            $res = ['status' => false, 'name' => 'contact', 'message' => 'User not available.'];
            return $res;
        }
    }
    public function whmcs_orders() {
        
        $users = User::where('id', Auth::user()->id)->get();
        if(!empty($users)){
            foreach($users as $k => $v) {
                //$credential_all = unserialize($v->credentials->data);
                $credential_all = unserialize(DB::table('credentials')->where('type','whmcs')->whereUserId(Auth::user()->id)->value('data'));

                $url = 'https://api.hubapi.com/integrations/v1/me?hapikey='.$credential_all['hapi_key'];
                $profile=$this->curlGet($url);
                $profile_data = json_decode($profile);
                $portal_id = $profile_data->portalId;
                
                $array['validation'] =  [
                    'username' => 'required|min:3|max:50',
                    'password' => 'required|min:10|max:200',
                    'hapi_key' => 'required|min:10|max:200',
                ];
                /* call function*/
                $v = \Validator::make($credential_all, $array['validation']);
                if ($v->fails()) {
                    $errors = $v->errors();
                    $res = ['status' => false, 'message' => 'Credentials should be updated.'];
                    return $res;
                } 
                $username = $credential_all['username'];
                $password = $credential_all['password'];
                $hapi_key = $credential_all['hapi_key'];
                // Get orders start
                $orderfields = array(
                    'action' => 'GetOrders',
                    'username' => $username,
                    'password' => $password,
                    'responsetype' => 'json',
                );
                // Get orders stop
                $args['data'] = $orderfields;
                $result = $this->curlAccessWHMCS('POST', $args);
                
                if(!empty($result)){
                    if($result->result != 'error') {
                        foreach($result->orders->order as $key => $val) {
                            $user_id = $val->userid;
                            $amount = $val->amount;


                            // $port = DB::table('application_records')->where('user_id',Auth::user()->id)->where('app_id',2)->value('portal_id');
                            // if($port > 0){
                            //     $res = $this->deal_pipeline_DB($val->status);
                            //     if($res){
                            //         $deal_stage = @$res['hubspot_pipeline'];
                            //         $deal_pipeline_id = @$res['hubspot_parent_pipeline'];    
                            //     } else {
                            //         $deal_pipeline_id = $this->deal_pipeline($hapi_key);
                            //         if($deal_pipeline_id == 'default'){
                            //             $deal_stage = 'appointmentscheduled';
                            //         } else {
                            //             if($val->status == 'Pending'){
                            //                 $deal_stage = 'checkout_pending';
                            //             } else {
                            //                 $deal_stage = 'checkout_completed';
                            //             }
                            //         }
                            //     }
                            // } else {
                                $deal_pipeline_id = $this->deal_pipeline($hapi_key);
                                if($deal_pipeline_id == 'default'){
                                    $deal_stage = 'appointmentscheduled';
                                } else {
                                    if($val->status == 'Pending'){
                                        $deal_stage = 'checkout_pending';
                                    } else {
                                        $deal_stage = 'checkout_completed';
                                    }
                                }
                            //}

                            
                            //$productid = DB::connection('mysql2')->table('tblhosting')->where('orderid',$val->id)->value('packageid');
                            if(!empty($val->lineitems->lineitem)){
                                foreach($val->lineitems->lineitem as $key2 => $val2) {
                                    $array = explode("-",$val2->product);
                                    
                                    // $productid = DB::table('whmcs_sync_products')->where('product_name', 'LIKE' ,'%'.trim($array[1]).'%')->where('app_user_id', Auth::user()->id)->where('portal_id',$portal_id)->value('whmcs_id');

                                    $productid = DB::table('whmcs_sync_products')->where('product_name', 'LIKE' ,'%'.trim($array[1]).'%')->where('portal_id',$portal_id)->value('whmcs_id');
                                    
                                    // $id = DB::table('whmcs_sync_order_products')->where('product_id', $productid)->where('user_id', $user_id)->where('app_user_id', Auth::user()->id)->where('order_id', $val->id)->where('portal_id',$portal_id)->value('id');

                                    $id = DB::table('whmcs_sync_order_products')->where('product_id', $productid)->where('user_id', $user_id)->where('order_id', $val->id)->where('portal_id',$portal_id)->value('id');
                                    
                                    // $hub_product_id = DB::table('whmcs_sync_products')->where('whmcs_id', $productid)->where('app_user_id', Auth::user()->id)->where('portal_id',$portal_id)->value('hub_id');

                                    $hub_product_id = DB::table('whmcs_sync_products')->where('whmcs_id', $productid)->where('portal_id',$portal_id)->value('hub_id');

                                    
                                    // $product = DB::table('whmcs_sync_products')->where('whmcs_id', $productid)->where('app_user_id', Auth::user()->id)->where('portal_id',$portal_id)->value('product_name');

                                    $product = DB::table('whmcs_sync_products')->where('whmcs_id', $productid)->where('portal_id',$portal_id)->value('product_name');
                                    
                                    // $vid = DB::table('whmcs_contacts')->where('whmcs_id', $user_id)->where('app_user_id', Auth::user()->id)->where('portal_id',$portal_id)->value('hub_id');

                                    $vid = DB::table('whmcs_contacts')->where('whmcs_id', $user_id)->where('portal_id',$portal_id)->value('hub_id');
                                    
                                    $timestamp = time() * 1000;
                                    
                                    if(empty($id)) {

                                        // add to cart
                                        $args['url'] = "https://api.hubapi.com/deals/v1/deal?hapikey=".$hapi_key;
                                        $args['data'] = '{
                                            "associations": {
                                            "associatedCompanyIds": [],
                                            "associatedVids": [
                                                '.$vid.'
                                            ]
                                            },
                                            "properties": [
                                            {
                                                "value": "'.$product.'",
                                                "name": "dealname"
                                            },
                                            {
                                                "value": "'.$deal_stage.'",
                                                "name": "dealstage"
                                            },
                                            {
                                                "value": "'.$deal_pipeline_id.'",
                                                "name": "pipeline"
                                            },
                                            {
                                                "value": '.$timestamp.',
                                                "name": "closedate"
                                            },
                                            {
                                                "value": "'.$amount.'",
                                                "name": "amount"
                                            },
                                            {
                                                "value": "newbusiness",
                                                "name": "dealtype"
                                            }
                                            ]
                                        }';

                                        $result2 = $this->curlAccessHS('POST', $args);
                                       
                                        
                                        $deal_id = $result2->dealId;


                                        // deals
                                            $args1['url'] = 'https://api.hubapi.com/extensions/ecomm/v2/sync/messages?hapikey='.$hapi_key;
                                            $args1['data'] = '{
                                                  "storeId": "ecommercebridge-whmcs-web-store",
                                                  "objectType": "DEAL",
                                                  "messages": [
                                                    {
                                                      "action": "UPSERT",
                                                      "changedAt": "'. $timestamp .'",
                                                      "externalObjectId": "'.$deal_id.'",
                                                      "properties": {
                                                        "name": "'.$product.'",
                                                        "stage": "'.$deal_stage.'",
                                                        "purchase_date": "'. $timestamp .'"
                                                      },
                                                      "associations": {
                                                        "CONTACT": [
                                                          "'.$vid.'"
                                                        ]
                                                      }
                                                    }
                                                  ]
                                                }';
                                            $result2 = $this->curlAccessHS('PUT', $args1);
                                        // deals

                                        // line items
                                        $args['url'] = "https://api.hubapi.com/crm-objects/v1/objects/line_items?hapikey=".$hapi_key;
                                        $args['data'] = '[
                                                            {
                                                                "name": "hs_product_id",
                                                                "value": "'.$hub_product_id.'"
                                                            },
                                                            {
                                                                "name": "quantity",
                                                                "value": "1"
                                                            },
                                                            {
                                                                "name": "price",
                                                                "value": "'.$amount.'"
                                                            },
                                                            {
                                                                "name": "name",
                                                                "value": "'.$product.'"
                                                            }
                                                        ]';
                                        $result2 = $this->curlAccessHS('POST', $args);
                                        $line_item_id = $result2->objectId;


                                        // line items
                                            $args1['url'] = 'https://api.hubapi.com/extensions/ecomm/v2/sync/messages?hapikey='.$hapi_key;
                                            $args1['data'] = '{
                                                  "storeId": "ecommercebridge-whmcs-web-store",
                                                  "objectType": "LINE_ITEM",
                                                  "messages": [
                                                    {
                                                      "action": "UPSERT",
                                                      "changedAt": "' . $timestamp . '",
                                                      "externalObjectId": "'. $line_item_id .'",
                                                      "properties": {
                                                        "price": "'.$amount.'",
                                                        "num_items": "1",
                                                        "tax_amount": "0"
                                                      },
                                                      "associations": {
                                                        "DEAL": [
                                                          "'.$deal_id.'"
                                                        ],
                                                        "PRODUCT": [
                                                          "'.$productid.'"
                                                        ]
                                                      }
                                                    }
                                                  ]
                                                }';
                                            $result2 = $this->curlAccessHS('PUT', $args1);
                                        // line items
                                        
                                        
                                        $args['url'] = 'https://api.hubapi.com/crm-associations/v1/associations?hapikey='.$hapi_key;
                                        $args['data'] = '{
                                                            "fromObjectId":'.$deal_id.',
                                                            "toObjectId":'. $line_item_id.',
                                                            "category" : "HUBSPOT_DEFINED",
                                                            "definitionId": 19
                                                        }';
                                                        
                                        $result2 = $this->curlAccessHS('PUT', $args);
                                        
                                        $order_product_id = DB::table('whmcs_sync_order_products')->insertGetId(
                                            ['product_id' => $productid , 'order_id' => $val->id, 'user_id' => $user_id, 'hub_add_to_cart_id' => $deal_id, 'hub_lineitem_id' => $line_item_id, 'app_user_id' => Auth::user()->id,'portal_id' => $portal_id]
                                        );

                                    } else {

                                        $p_id = DB::table('whmcs_sync_order_products')->where('product_id', $productid)->where('user_id', $user_id)->where('order_id', $val->id)->where('portal_id',$portal_id)->value('product_id');

                                        if($p_id > 0) {

                                            $deal_id = DB::table('whmcs_sync_order_products')->where('product_id', $productid)->where('user_id', $user_id)->where('order_id', $val->id)->where('portal_id',$portal_id)->value('hub_add_to_cart_id');

                                            $line_item_id = DB::table('whmcs_sync_order_products')->where('product_id', $productid)->where('user_id', $user_id)->where('order_id', $val->id)->where('portal_id',$portal_id)->value('hub_lineitem_id');

                                            // add to cart
                                            $args['url'] = "https://api.hubapi.com/deals/v1/deal/".$deal_id."?hapikey=".$hapi_key;
                                            $args['data'] = '{
                                                "associations": {
                                                "associatedCompanyIds": [],
                                                "associatedVids": [
                                                    '.$vid.'
                                                ]
                                                },
                                                "properties": [
                                                {
                                                    "value": "'.$product.'",
                                                    "name": "dealname"
                                                },
                                                {
                                                    "value": "'.$deal_stage.'",
                                                    "name": "dealstage"
                                                },
                                                {
                                                    "value": "'.$deal_pipeline_id.'",
                                                    "name": "pipeline"
                                                },
                                                {
                                                    "value": '.$timestamp.',
                                                    "name": "closedate"
                                                },
                                                {
                                                    "value": "'.$amount.'",
                                                    "name": "amount"
                                                },
                                                {
                                                    "value": "newbusiness",
                                                    "name": "dealtype"
                                                }
                                                ]
                                            }';

                                            $result2 = $this->curlAccessHS('PUT', $args);
                                            

                                            // deals
                                                $args1['url'] = 'https://api.hubapi.com/extensions/ecomm/v2/sync/messages?hapikey='.$hapi_key;
                                                $args1['data'] = '{
                                                      "storeId": "ecommercebridge-whmcs-web-store",
                                                      "objectType": "DEAL",
                                                      "messages": [
                                                        {
                                                          "action": "UPSERT",
                                                          "changedAt": "'. $timestamp .'",
                                                          "externalObjectId": "'.$deal_id.'",
                                                          "properties": {
                                                            "name": "'.$product.'",
                                                            "stage": "'.$deal_stage.'",
                                                            "purchase_date": "'. $timestamp .'"
                                                          },
                                                          "associations": {
                                                            "CONTACT": [
                                                              "'.$vid.'"
                                                            ]
                                                          }
                                                        }
                                                      ]
                                                    }';
                                                $result2 = $this->curlAccessHS('PUT', $args1);
                                            // deals

                                            // line items
                                            $args['url'] = "https://api.hubapi.com/crm-objects/v1/objects/line_items/".$line_item_id."?hapikey=".$hapi_key;
                                            $args['data'] = '[
                                                                {
                                                                    "name": "hs_product_id",
                                                                    "value": "'.$hub_product_id.'"
                                                                },
                                                                {
                                                                    "name": "quantity",
                                                                    "value": "1"
                                                                },
                                                                {
                                                                    "name": "price",
                                                                    "value": "'.$amount.'"
                                                                },
                                                                {
                                                                    "name": "name",
                                                                    "value": "'.$product.'"
                                                                }
                                                            ]';
                                            $result2 = $this->curlAccessHS('PUT', $args);
                                            


                                            // line items
                                                $args1['url'] = 'https://api.hubapi.com/extensions/ecomm/v2/sync/messages?hapikey='.$hapi_key;
                                                $args1['data'] = '{
                                                      "storeId": "ecommercebridge-whmcs-web-store",
                                                      "objectType": "LINE_ITEM",
                                                      "messages": [
                                                        {
                                                          "action": "UPSERT",
                                                          "changedAt": "' . $timestamp . '",
                                                          "externalObjectId": "'. $line_item_id .'",
                                                          "properties": {
                                                            "price": "'.$amount.'",
                                                            "num_items": "1",
                                                            "tax_amount": "0"
                                                          },
                                                          "associations": {
                                                            "DEAL": [
                                                              "'.$deal_id.'"
                                                            ],
                                                            "PRODUCT": [
                                                              "'.$p_id.'"
                                                            ]
                                                          }
                                                        }
                                                      ]
                                                    }';
                                                $result2 = $this->curlAccessHS('PUT', $args1);
                                            // line items
                                            
                                            
                                            $args['url'] = 'https://api.hubapi.com/crm-associations/v1/associations?hapikey='.$hapi_key;
                                            $args['data'] = '{
                                                                "fromObjectId":"'.$deal_id.'",
                                                                "toObjectId":"'. $line_item_id.'",
                                                                "category" : "HUBSPOT_DEFINED",
                                                                "definitionId": 19
                                                            }';
                                                            
                                            $results = $this->curlAccessHS('PUT', $args);
                                            
                                            $order_product_id = DB::table('whmcs_sync_order_products')->where('id',$id)->update(
                                                ['product_id' => $productid , 'order_id' => $val->id, 'user_id' => $user_id, 'hub_add_to_cart_id' => $deal_id, 'hub_lineitem_id' => $line_item_id, 'app_user_id' => Auth::user()->id,'portal_id' => $portal_id]
                                            );

                                        }

                                        
                                    }

                                } 
                            } else {
                                // $hub_product_id = DB::table('whmcs_sync_products')->where('whmcs_id', $productid)->where('app_user_id', Auth::user()->id)->where('portal_id',$portal_id)->value('hub_id');


                                $hub_product_id = DB::table('whmcs_sync_products')->where('whmcs_id', $productid)->where('portal_id',$portal_id)->value('hub_id');

                                // $id = DB::table('whmcs_sync_order_products')->where('user_id', $user_id)->where('app_user_id', Auth::user()->id)->where('order_id', $val->id)->where('portal_id',$portal_id)->value('id');

                                $id = DB::table('whmcs_sync_order_products')->where('user_id', $user_id)->where('order_id', $val->id)->where('portal_id',$portal_id)->value('id');

                                if(empty($id)){
                                    $order_product_id = DB::table('whmcs_sync_order_products')->insertGetId(
                                        ['product_id' => 0 , 'order_id' => $val->id, 'user_id' => $user_id, 'app_user_id'=> Auth::user()->id,'portal_id' => $portal_id]
                                    );
                                }
                            }   
                        }
                        $this->import_status(time(), 3);
                    } else {
                        $res = ['status' => false, 'message' => $result->message ];
                        return $res;
                    }

                } else {
                    $res = ['status' => false, 'message' => 'No data found.' ];
                    return $res;
                }
            }
            $html = '<button data-step="3" class="btn btn-label-brand btn-bold finish">Success</button>';
            $res = ['status' => true,  'name' => 'finish', "data_step" => "3",  'message' => 'Orders has been import.','html' => $html];
            return $res;
        } else {
            $res = ['status' => false,  'name' => 'finish', 'message' => 'User not available.'];
            return $res;
        }
    }

    public function workflow_automation(){

        $credentials = unserialize(Credential::where('user_id',Auth::user()->id)->where('fk_app_id', 2)->value('data'));
        if(!empty($credentials['hapi_key'] )) {
            //$args['url'] = 'https://api.hubapi.com/automation/v3/workflows/20354255?hapikey='.$credentials['hapi_key'];

            //$args['url'] = 'https://api.hubapi.com/automation/v3/workflows?hapikey=e1964b7f-c16c-453e-9cc0-efcaeb1ced47';
            $args['url'] = 'https://api.hubapi.com/automationextensions/v1/definitions?hapikey=e1964b7f-c16c-453e-9cc0-efcaeb1ced47&applicationId=215155';
            // $args['data'] = '{
            //         "name": "Test Workflow",
            //         "type": "DRIP_DELAY",
            //         "onlyEnrollsManually": true,
            //         "actions": [
            //             {
            //                 "type": "DELAY",
            //                 "delayMillis": 3600000
            //             },
            //             {
            //                 "type": "WEBHOOK",
            //                 "url": "'.url('whmcs-workflow-trigger').'",
            //                 "method": "POST"
            //             }
            //         ]
            //     }';

            // {
            //     "type" : "",
            //     "extensionDefinitionId" : ,
            //     ""
            // }

            $result = $this->curlAccessH('POST',$args);
            echo '<pre>';
            print_r($result);
            die;

            // [portalId] => 7323845
            // [flowId] => 47587566
            // [workflowId] => 20351880
            //20351880
        }
        
    }

    public function whmcs_automation_GET(){

        return view('whmcs.whmcs-automation');
    }
    public function whmcs_payment_update(Request $req){
        $data = $req->all();
        if(isset($data['id'])){
            if(isset($data['hour_sync_time'])){
                $sync_time = $data['hour_sync_time'];
            }
            if(isset($data['daily_sync_time'])){
                $sync_time = $data['daily_sync_time'];
            }
            //$credentials = unserialize(Credential::where('user_id',Auth::user()->id)->where('fk_app_id', 2)->value('data'));
            //$time = $sync_time * 3600000;
            DB::table('whmcs_payments')->where('id',$data['id'])->update(['sync_time' => $sync_time]);
            // $workflow_id = DB::table('whmcs_payments')->where('id',$data['id'])->value('workflow_id');

            // $args['url'] = 'https://api.hubapi.com/automation/v3/workflows/'.$workflow_id.'?hapikey='.$credentials['hapi_key'];
            // $args['data'] = '{
            //         "actions": [                                        
            //             {
            //                 "type": "DELAY",
            //                 "delayMillis": '.$time.'
            //             }
            //         ]
            //     }';
            //$result = curlAccessAll('PUT',$args);
            return redirect()->to('/whmcs-automation')->with(['message'=> 'Data has been updated.', 'alert' => 'success']);
        } else {
            return redirect()->to('/whmcs-automation')->with(['message'=> 'Payment ID should not be empty.', 'alert' => 'danger']);
        }
        
    }
    private function whmcs_all(){
        $res = $this->whmcs_products();
        if($res['status'] == 'true') {
            $res = $this->whmcs_contacts();
            if($res['status'] == 'true') {
                $res = $this->whmcs_orders();
                if($res['status'] == 'false') {
                    return $res;
                } else {
                    return $res;
                }
            } else {
                return $res;
            }
        } else {
            return $res;
        }
    }

    /**
     * Hubspot Integration
     */
    public function GET_settings_hub($auth_id)
    {
        if(!empty($_GET)) {
            $response = $_GET;
            User::whereId($_GET['auth_id'])->update(['object_data'=> serialize($response)]);
            $user = User::where('id', $_GET['auth_id'])->first();
            Auth::login($user);
        } else {
            $this->auth_access($auth_id);
        }

        $data = Credential::whereUserId(Auth::user()->id)->where('type','whmcs')->value('data');
        
        $render = 'render.whmcs.settings';
        $data = unserialize($data);
        if(!empty($data)){
            $sync = '1';
        } else {
            $sync = '0';
        }
        $html = view($render, compact('data'));
        return view('whmcs.settings-hub',compact('data','html','sync'));
        
    }
    
    public function POST_settings_hub(Request $request)
    {   
        $array['validation'] =  [
                                    'username' => 'required|min:3|max:50',
                                    'password' => 'required|min:10|max:200',
                                    'hapi_key' => 'required|min:10|max:200',
                                ];
        /* call function*/
        $v = \Validator::make($request->all(), $array['validation']);
        
        $this->auth_access($request->auth_id);
        
        if ($v->fails()) {
            $errors = $v->errors();
            $data = Credential::whereUserId(Auth::user()->id)
                                ->where('type','whmcs')
                                ->value('data');
            $data = unserialize($data);
            $html = view('render.whmcs.settings', compact('data'));
            return view('whmcs.settings-hub',compact('errors','data','html'));
        } 
        try {
            
            $res = $request->all();
            unset($res['_token']);
            $id = Credential::whereUserId(Auth::user()->id)
                                ->where('type','whmcs')
                                ->value('id');
            $data = serialize($res);
            if(empty($id)){
                Credential::create([
                                            'data'=> $data,
                                            'type' => 'whmcs',
                                            'fk_app_id' => 2,
                                            'user_id' => Auth::user()->id,
                                    ]);
                $alert = 'success';
                $message = 'Data has been saved.';
            } else {
                Credential::where('id',$id)->update([
                                                            'data' => $data,
                                                            'type' => 'whmcs',
                                                            'fk_app_id' => 2
                                                        ]);
                $alert = 'success';
                $message = 'Data has been updated.';
            }
        } catch (Exception $e) {
            $alert = 'warning';
            $message = 'Something went wrong in code.';
        }
        
        $data = Credential::whereUserId(Auth::user()->id)
                                ->where('type','whmcs')
                                ->value('data');
        $data = unserialize($data);
        if(!empty($data)){
            $sync = '1';
        } else {
            $sync = '0';
        }
        $html = view('render.whmcs.settings', compact('data'));
        
        return view('whmcs.settings-hub',compact('alert','message','data','html','sync'));
    }

    public function curlAccess1($method, $array, $content_type = 'array' ) {
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $array['url']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        switch ($method){
          case "POST":
            curl_setopt($ch, CURLOPT_POST, 1);        
             if ($array['data'])
                curl_setopt($ch, CURLOPT_POSTFIELDS, $array['data']);
             break;
          case "PUT":
             curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
             if ($array['data'])
                curl_setopt($ch, CURLOPT_POSTFIELDS, $array['data']);
             break;
          default:
             curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
       }

        $headers = array();
        $headers[] = 'Content-Type: application/json';
        
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $result = curl_exec($ch);
        
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);

        if($content_type == 'array'){
            $result = json_decode($result); 
        }  
        return $result;
    } 

    private function curlAccess($method, $array, $content_type = 'array' ) 
    {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $array['url']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_ENCODING, "");
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);        
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        switch ($method){
          case "POST":
            curl_setopt($ch, CURLOPT_POST, 1);        
             if ($array['data'])
                curl_setopt($ch, CURLOPT_POSTFIELDS, $array['data']);
             break;
          case "PUT":
             curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
             if ($array['data'])
                curl_setopt($ch, CURLOPT_POSTFIELDS, $array['data']);
             break;
          default:
             curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        }

        curl_setopt($ch, CURLOPT_HTTPHEADER, $array['headers']);
        $result = curl_exec($ch);
        
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);

        if($content_type == 'array'){
            $result = json_decode($result); 
        }  
        return $result;

    }
    private function curlAccessH($method, $array, $content_type = 'array' ) 
    {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $array['url']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_ENCODING, "");
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);        
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        switch ($method){
          case "POST":
            curl_setopt($ch, CURLOPT_POST, 1);        
             if ($array['data'])
                curl_setopt($ch, CURLOPT_POSTFIELDS, $array['data']);
             break;
          case "PUT":
             curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
             if ($array['data'])
                curl_setopt($ch, CURLOPT_POSTFIELDS, $array['data']);
             break;
          default:
             curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        }

        $headers = array();
        $headers[] = 'Content-Type: application/json';
        if(!empty($array['token'])){
            $headers[] = 'authorization: Bearer '. $array['token'];  
        }

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $result = curl_exec($ch);
        
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);

        if($content_type == 'array'){
            $result = json_decode($result); 
        }  
        return $result;

    }
    private function curlAccessG($method, $array, $content_type = 'array' ) 
    {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $array['url']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_ENCODING, "");
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);        
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        switch ($method){
          case "POST":
            curl_setopt($ch, CURLOPT_POST, 1);        
             if ($array['data'])
                curl_setopt($ch, CURLOPT_POSTFIELDS, $array['data']);
             break;
          case "PUT":
             curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
             if ($array['data'])
                curl_setopt($ch, CURLOPT_POSTFIELDS, $array['data']);
             break;
          default:
             curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        }

        $headers = array();
        $headers[] = 'Content-Type: application/html';

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $result = curl_exec($ch);
        
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);

        if($content_type == 'array'){
            $result = json_decode($result); 
        }  
        return $result;

    }
    public function settings()
    {
        
        
        $data = Credential::whereUserId(Auth::user()->id)
                            ->where('type','whmcs')
                            ->value('data');
        $data = unserialize($data);
        if(!empty($data)){
            $sync = '1';
        } else {
            $sync = '0';
        }
        $render = 'render.whmcs.settings';
        $html = view($render, compact('data'));
        return view('whmcs.settings',compact('data','html','sync'));
    }
    public function POST_settings(Request $request)
    {   
        $array['validation'] =  [
                                    'username' => 'required|min:3|max:50',
                                    'password' => 'required|min:10|max:200',
                                    'hapi_key' => 'required|min:10|max:200',
                                ];
        /* call function*/
        $v = \Validator::make($request->all(), $array['validation']);

        if ($v->fails()) {
            $errors = $v->errors();
            $data = Credential::whereUserId(Auth::user()->id)
                                ->where('type','whmcs')
                                ->value('data');
            $data = unserialize($data);
            $html = view('render.whmcs.settings', compact('data'));
            return view('whmcs.settings',compact('errors','data','html'));
        } 
        try {
            
            $res = $request->all();
            unset($res['_token']);
            $id = Credential::whereUserId(Auth::user()->id)
                                ->where('type','whmcs')
                                ->value('id');
            $data = serialize($res);
            if(empty($id)){
                Credential::create([
                                            'data'=> $data,
                                            'fk_app_id' => 2,
                                            'type' => 'whmcs',
                                            'user_id' => Auth::user()->id,
                                    ]);
                $alert = 'success';
                $message = 'Data has been saved.';
            } else {
                Credential::where('id',$id)->update([
                                                            'data' => $data,
                                                            'type' => 'whmcs',
                                                            'fk_app_id' => 2,
                                                        ]);
                $alert = 'success';
                $message = 'Data has been updated.';
            }
        } catch (Exception $e) {
            $alert = 'warning';
            $message = 'Something went wrong in code.';
        }
        
        $data = Credential::whereUserId(Auth::user()->id)
                                ->where('type','whmcs')
                                ->value('data');
        $data = unserialize($data);
        
        $html = view('render.whmcs.settings', compact('data'));
        
        return view('whmcs.settings',compact('alert','message','data','html'));
    }
    // public function workflowSent(Request $request){

    //     $data = $request->all();
        
    //     $newData['portal_id'] = @$data['origin']['portalId'];
    //     $newData['email_notification_custom'] = @$data['fields']['email_notification_custom'];

    //     // $newData['email_notification_custom'] = 'randeep.s@webdew.com';
    //     // $newData['portal_id'] = 7323845;
    //     //$newData['actionDefinitionId'] = @$data['origin']['actionDefinitionId'];

    //     $v = \Validator::make($newData, [
    //         'portal_id' => 'required'
    //     ]);
    //     mail($newData['email_notification_custom'], 'hello subject', 'Hi');

    //     if ($v->fails())
    //     {
    //         $errors = $v->errors();
    //         $workflows = DB::table('workflows')->insert(['data'=>'validation error.']);
    //         return ['status' => false];
    //     } else {
    //         $application_users = DB::table('application_records')->where('portal_id',$newData['portal_id'])->where('app_id',2)->get()->toArray();
    //         foreach ($application_users as $key => $value) {
    //             Auth::logout();
    //             $user_id = $value->user_id;
    //             $u = User::where('id', $user_id)->first();
    //             Auth::login($u);
    //             $res = $this->whmcs_all();
    //             DB::table('workflow_results')->insertGetId([
    //                 'data' => serialize($newData['email_notification_custom']),
    //                 'response' => serialize($data)
    //             ]);
    //         }
    //     }
    //     return json_encode('true');
    // }

    public function workflowSent(){

        $now = date('Y-m-d H:i:s', strtotime('now'));
        $whmcs_payments = DB::table('whmcs_payments')->whereDate('datetime', '>=', $now)->get()->toArray();
        
        if(count($whmcs_payments) > 0){
            foreach ($whmcs_payments as $key => $value) {
                Auth::logout();
                $user_id = $value->user_id;
                $u = User::where('id', $user_id)->first();
                Auth::login($u);
                //$res = $this->whmcs_all();
                // DB::table('workflow_results')->insertGetId([
                //     'data' => serialize($newData['email_notification_custom']),
                //     'response' => serialize($data)
                // ]);
                $sync_time = $value->sync_time;
                if($sync_time == 1){
                    echo $after = date('Y-m-d H:i:s', strtotime($sync_time . ' hour'));    
                } else {
                    echo $after = date('Y-m-d H:i:s', strtotime($sync_time . ' hours'));
                }
                $whmcs_payments = DB::table('whmcs_payments')->where('id', $value->id)->update([
                                            'datetime' => $after
                                        ]);
            }
            return json_encode('true');
        } else {
            return json_encode('false');
        }
        
        
    }

    public function whmcs_pipeline_hubspot_POST(Request $request){
        $data = $request->all();
        if(!isset($data->edit)){

            foreach ($data['whmcs'] as $key => $value) {
                foreach ($value as $id => $val) {
                    $whmcs_pipeline = $val;
                    $hubspot_pipeline_both = explode("|",$data['hubspot'][$key][$id]);
                    $hubspot_pipeline = @$hubspot_pipeline_both[1];
                    $hubspot_parent_pipeline = @$hubspot_pipeline_both[0];
                    $exist_id = DB::table('whmcs_sync_pipelines')
                                ->where('id',$id)
                                ->value('id');
                    if($exist_id > 0){
                        DB::table('whmcs_sync_pipelines')->where('id',$id)->update([
                                'whmcs_pipeline' => $whmcs_pipeline,
                                'hubspot_parent_pipeline' => $hubspot_parent_pipeline,
                                'hubspot_pipeline' => $hubspot_pipeline,
                                'fk_user_id' => Auth::user()->id
                            ]);
                    } else {
                        DB::table('whmcs_sync_pipelines')->insert([
                                'whmcs_pipeline' => $whmcs_pipeline,
                                'hubspot_parent_pipeline' => $hubspot_parent_pipeline,
                                'hubspot_pipeline' => $hubspot_pipeline,
                                'fk_user_id' => Auth::user()->id
                            ]);
                    }
                }
            }    
        } else {
            foreach ($data['whmcs'] as $key => $value) {
                $whmcs_pipeline = $value;
                $hubspot_pipeline_both = explode("|",$data['hubspot'][$key]);
                $hubspot_pipeline = @$hubspot_pipeline_both[1];
                $hubspot_parent_pipeline = @$hubspot_pipeline_both[0];
                $exist_id = DB::table('whmcs_sync_pipelines')
                            ->where('whmcs_pipeline',$whmcs_pipeline)
                            ->where('hubspot_parent_pipeline',$hubspot_parent_pipeline)
                            ->where('hubspot_pipeline',$hubspot_pipeline)
                            ->where('fk_user_id', Auth::user()->id)
                            ->value('id');
                if($exist_id < 1){
                    DB::table('whmcs_sync_pipelines')->insert([
                            'whmcs_pipeline' => $whmcs_pipeline,
                            'hubspot_parent_pipeline' => $hubspot_parent_pipeline,
                            'hubspot_pipeline' => $hubspot_pipeline,
                            'fk_user_id' => Auth::user()->id
                        ]);
                }
            }
        }
        
        return redirect()->to('/whmcs-pipeline-hubspot')->with(['message'=> 'Pipeline has been synced', 'alert' => 'success']);
    }
    public function whmcs_pipeline_hubspot(){
        
        $credential_all = unserialize(DB::table('credentials')->where('type','whmcs')->whereUserId(Auth::user()->id)->value('data'));
               
        //getting portal id fro profile 
        // $url='https://api.hubapi.com/integrations/v1/me?hapikey='.$credential_all['hapi_key'];
        // $profile=$this->curlGet($url);
        // $profile_data = json_decode($profile);
        // $portal_id = $profile_data->portalId;
              
       //getting credentials from view.
        $array['validation'] =  [
            'username' => 'required|min:3|max:50',
            'password' => 'required|min:10|max:200',
            'hapi_key' => 'required|min:10|max:200',
        ];
        
        /* call function*/
        $v = \Validator::make($credential_all, $array['validation']);
        if ($v->fails()) {
            $errors = $v->errors();
            $res = ['status' => false, 'message' => 'Credentials should be updated.'];
            return $res;
        } 
        $username = $credential_all['username'];
        $password = $credential_all['password'];
        $hapi_key = $credential_all['hapi_key'];

        // Get products start
        $orderfields = array(
            'action' => 'GetOrderStatuses',
            'username' => $username,
            'password' => $password,
            'responsetype' => 'json'
        );
        $args['data'] = $orderfields;
        $pipeline = $this->curlAccessWHMCS('POST', $args);

        $args['url'] = "https://api.hubapi.com/deals/v1/pipelines?hapikey=".$hapi_key;
        $hubspot_pipeline = curlAccess( 'GET', $args );

        $exist_pipelines = DB::table('whmcs_sync_pipelines')
                        ->where('fk_user_id', Auth::user()->id)
                        ->get()->toArray();
        return view('whmcs.whmcs-pipeline-hubspot',compact('pipeline','hubspot_pipeline','exist_pipelines'));
    }
    // for creating defination
    public function definations() {

        $args['data'] = array(
                            'action' => 'GetOrderStatuses',
                            'username' => 'webdew',
                            'password' => '51f104a428f63aa23ea85240f7720fe6',
                            'responsetype' => 'json'
                        );
        $result = $this->curlAccessWHMCS('POST',$args);
        echo '<pre>';
        print_r($result);
        die;

        // $timing = time();
        // $sync_time = 3;
        //                     $time = $sync_time * 3600000;
        
        //                     $domain = env('APP_URL') . 'whmcs-workflow-trigger'; 

        //                     // $args['url'] = 'https://api.hubapi.com/automation/v3/workflows/20468670?hapikey=58723188-d3ff-473b-b9b0-24c927e10c10';
        //                     $args['url'] = 'https://api.hubapi.com/automation/v3/workflows?hapikey=58723188-d3ff-473b-b9b0-24c927e10c10';
        //                     $args['data'] = '{
        //                             "name": "WHMCS Automation '. $sync_time . '-'. $timing .'",
        //                             "type": "DRIP_DELAY",
        //                             "segmentCriteria": [
        //                                 [
        //                                     {
        //                                         "operator": "IS_NOT_EMPTY",
        //                                         "property": "email",
        //                                         "type": "string"
        //                                     }
        //                                 ]
        //                             ],
        //                             "onlyEnrollsManually": true,
        //                             "actions": [
        //                                 {
        //                                     "type": "DELAY",
        //                                     "delayMillis": '.$time.'
        //                                 },
        //                                 {
        //                                     "type": "WEBHOOK",
        //                                     "url": "'.$domain.'",
        //                                     "method": "POST"
        //                                 }
        //                             ]
        //                         }';

                                
                            // $result = curlAccessAll('POST',$args);
                            // echo '<pre>';
                            // print_r($result);
                            // die;
        // $d = DB::table('workflow_results')->where('id', 520)->value('response');
        // echo '<pre>';
        // print_r(unserialize($d));

        // die;

        $args['url'] = 'https://api.hubapi.com/automationextensions/v1/definitions/2402?hapikey=e1964b7f-c16c-453e-9cc0-efcaeb1ced47&applicationId=224529';
        $args['data'] = '{
                            "integrationAppId": 224529,
                            "extensionName": "WHMCS Notification",
                            "webhookUrl": "https://apps.webdew.com/hubspot/whmcs/workflowSent",
                            "fieldMetadata": [
                                {
                                    "label": "Email Notification",
                                    "key": "email_notification_custom",
                                    "fieldType": "TEXT",
                                    "values": [
                                        {
                                            "type": "STATIC_VALUE",
                                            "allowsMergeTags": false
                                        }
                                    ]
                                }
                            ]
                        }';

        //$args['url'] = 'https://api.hubapi.com/automationextensions/v1/definitions?hapikey=e1964b7f-c16c-453e-9cc0-efcaeb1ced47&applicationId=224529';
        // $args['data'] = '{
        //                     "integrationAppId": 224529,
        //                     "extensionName": "WHMCS Notification Stage Test",
        //                     "webhookUrl": "https://phpstack-401531-1641101.cloudwaysapps.com/hubspot/whmcs/workflowSent",
        //                     "fieldMetadata": [
        //                         {
        //                             "label": "Email Notification",
        //                             "key": "email_notification_custom",
        //                             "fieldType": "TEXT",
        //                             "values": [
        //                                 {
        //                                     "type": "STATIC_VALUE",
        //                                     "allowsMergeTags": false
        //                                 }
        //                             ]
        //                         }
        //                     ]
        //                 }';
        //                 echo '<pre>';
        //                 print_r($args);
                        //die;
        //$result = $this->curlAccessH('DELETE',$args);
        // echo '<pre>';
        // print_r($result);
        // die;
        //2406 - live
        //2405 - stage
    }

    // function call
    public function curlAccessWHMCS($method, $array, $content_type = 'array' ) {
        $ch = curl_init();
        $domainURL = 'https://phpstack-242348-1101472.cloudwaysapps.com/';
        curl_setopt($ch, CURLOPT_URL, $domainURL . '/includes/api.php');
        
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        switch ($method){
        case "POST":
            curl_setopt($ch, CURLOPT_POST, 1);
            if ($array['data'])
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query( $array['data'] ));
            break;
        case "PUT":
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
            if ($array['data'])
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query( $array['data'] ) );
            break;
        default:
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        }
        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);

        if($content_type == 'array'){
            $result = json_decode($result); 
        }  
        return $result;
    }

    public function curlAccessHS($method, $array, $content_type = 'array' ) {
      
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $array['url']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        switch ($method){
        case "POST":
            curl_setopt($ch, CURLOPT_POST, 1);
            if ($array['data'])
                curl_setopt($ch, CURLOPT_POSTFIELDS,  $array['data'] );
            break;
        case "PUT":
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
            if ($array['data'])
                curl_setopt($ch, CURLOPT_POSTFIELDS,  $array['data']  );
            break;
        default:
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        }

        $headers = array();
        $headers[] = 'Content-Type: application/json';        
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);

        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);

        if($content_type == 'array'){
            $result = json_decode($result); 
        }  
        return $result;
    }
    public function sync_data() {
        return view('whmcs.sync-data');
    }
    public function sync_data_hub($auth_id) {
        $this->auth_access($auth_id);
        return view('whmcs.sync-data-hub');
    }
    public function whmcs_ecommerce(Request $res) {
        $data = $res->all();
        DB::table('workflow_results')->insertGetId([
                                                        'data' => serialize($data),
                                                        'response' => serialize($data),
                                                    ]);
        $results = [ "results" => true ];
        echo json_encode($results);
        die;
        // $args['url'] = 'https://api.hubapi.com/extensions/ecomm/v2/stores?hapikey=0fab6b1c-02b4-49d4-ab72-15a1f13a6642';
        // $args['data'] = '{
        //           "id": "ecommercebridge-whncs-store",
        //           "label": "WHMCS Ecommerce Bridge Store",
        //           "adminUri": "https://apps.webdew.com/hubspot/whmcs-ecommerce-admin"
        //         }';
        // $result = $this->curlAccess1('POST', $args);
        return json_encode('1');
    }
    public function whmcs_ecommerce_admin(Request $req){
        $data = $res->all();
        DB::table('workflow_results')->insertGetId([
                                                        'data' => serialize($data),
                                                        'response' => serialize($_GET),
                                                    ]);
        // $args['url'] = 'https://api.hubapi.com/extensions/ecomm/v2/stores?hapikey=0fab6b1c-02b4-49d4-ab72-15a1f13a6642';
        // $args['data'] = '{
        //           "id": "ecommercebridge-whncs-store",
        //           "label": "WHMCS Ecommerce Bridge Store",
        //           "adminUri": "https://apps.webdew.com/hubspot/whmcs-ecommerce-admin"
        //         }';
        // $result = $this->curlAccess1('POST', $args);
        return json_encode('1');
    }
    public function whmcs_ecommerces(Request $res) {
        $data = $res->all();
        DB::table('workflow_results')->insertGetId([
                                                        'data' => serialize($data),
                                                        'response' => serialize($data),
                                                    ]);
        $results = [ "results" => true ];
        echo json_encode($results);
        die;
        //         $args['url'] = 'https://api.hubapi.com/extensions/ecomm/v2/stores/ecommercebridge-whmcs-web-store?hapikey=58723188-d3ff-473b-b9b0-24c927e10c10';
                
        //         $result = $this->curlAccess1('DELETE', $args);
        // die;
        // Get orders start
                // $orderfields = array(
                //     'action' => 'GetOrders',
                //     'username' => 'webdew',
                //     'password' => '51f104a428f63aa23ea85240f7720fe6',
                //     'responsetype' => 'json',
                // );
                // // Get orders stop
                // $args['data'] = $orderfields;
                // $result = $this->curlAccessWHMCS('POST', $args);
                // echo '<pre>';
                // print_r($result);
                // die;
        

        //          $args['url'] = 'https://api.hubapi.com/extensions/ecomm/v1/installs?hapikey=0fab6b1c-02b4-49d4-ab72-15a1f13a6642';

        // //         // $args['url'] = 'https://api.hubapi.com/extensions/ecomm/v2/sync/errors/app/224529?hapikey=e1964b7f-c16c-453e-9cc0-efcaeb1ced47';
        //         $args['data'] = '{
        //   "portalId": 8632780,
        //   "storeId": "224529_ecommercebridge-whncs-store",
        //   "importStartedAt": 1552678940201,
        //   "settingsToImport": [
        //     {
        //       "settingsId": 1,
        //       "objectType": "PRODUCT"
        //     },
        //     {
        //       "settingsId": 2,
        //       "objectType": "CONTACT"
        //     },
        //     {
        //       "settingsId": 3,
        //       "objectType": "LINE_ITEM"
        //     },
        //     {
        //       "settingsId": 4,
        //       "objectType": "DEAL"
        //     }
        //   ]
        // }';
        // echo 'ss';
        //         $result = $this->curlAccess1('POST', $args);
        //         echo '<pre>';
        //         print_r($result);
        //         die;

        // $args['url'] = 'https://api.hubapi.com/extensions/ecomm/v2/settings?hapikey=0fab6b1c-02b4-49d4-ab72-15a1f13a6642';
        // $re = $this->curlAccess1('GET',$args);

        // // $args['data'] = '{
        // //                     "enabled": true,
        // //                     "webhookUri": "https://apps.webdew.com/hubspot/whmcs-ecommerce",
        // //                     "mappings": {
        // //                     "CONTACT": {
        // //                         "properties": [
        // //                             {
        // //                                 "externalPropertyName": "firstname",
        // //                                 "hubspotPropertyName": "firstname",
        // //                                 "dataType": "STRING"
        // //                             },
        // //                             {
        // //                                 "externalPropertyName": "familyname",
        // //                                 "hubspotPropertyName": "lastname",
        // //                                 "dataType": "STRING"
        // //                             },
        // //                             {
        // //                                 "externalPropertyName": "customer_email",
        // //                                 "hubspotPropertyName": "email",
        // //                                 "dataType": "STRING"
        // //                             },
        // //                             {
        // //                                 "externalPropertyName": "phone_number",
        // //                                 "hubspotPropertyName": "mobilephone",
        // //                                 "dataType": "STRING"
        // //                             }
        // //                         ]
        // //                     },
        // //                     "DEAL": {
        // //                         "properties": [
        // //                             {
        // //                                 "externalPropertyName": "purchase_date",
        // //                                 "hubspotPropertyName": "closedate",
        // //                                 "dataType": "STRING"
        // //                             },
        // //                             {
        // //                                 "externalPropertyName": "name",
        // //                                 "hubspotPropertyName": "dealname",
        // //                                 "dataType": "STRING"
        // //                             },
        // //                             {
        // //                                 "externalPropertyName": "stage",
        // //                                 "hubspotPropertyName": "dealstage",
        // //                                 "dataType": "STRING"
        // //                             },
        // //                             {
        // //                                 "externalPropertyName": "abandoned_cart_url",
        // //                                 "hubspotPropertyName": "ip__ecomm_bride__abandoned_cart_url",
        // //                                 "dataType": "STRING"
        // //                             }
        // //                         ]
        // //                     },
        // //                     "PRODUCT": {
        // //                         "properties": [
        // //                             {
        // //                                 "externalPropertyName": "product_description",
        // //                                 "hubspotPropertyName": "description",
        // //                                 "dataType": "STRING"
        // //                             },
        // //                             {
        // //                                 "externalPropertyName": "price",
        // //                                 "hubspotPropertyName": "price",
        // //                                 "dataType": "NUMBER"
        // //                             },
        // //                             {
        // //                                 "externalPropertyName": "product_name",
        // //                                 "hubspotPropertyName": "name",
        // //                                 "dataType": "STRING"
        // //                             }
        // //                         ]
        // //                     },
        // //                     "LINE_ITEM": {
        // //                         "properties": [
        // //                             {
        // //                                 "externalPropertyName": "tax_amount",
        // //                                 "hubspotPropertyName": "tax",
        // //                                 "dataType": "NUMBER"
        // //                             },
        // //                             {
        // //                                 "externalPropertyName": "num_items",
        // //                                 "hubspotPropertyName": "quantity",
        // //                                 "dataType": "NUMBER"
        // //                             },
        // //                             {
        // //                                 "externalPropertyName": "price",
        // //                                 "hubspotPropertyName": "price",
        // //                                 "dataType": "NUMBER"
        // //                             },
        // //                             {
        // //                                 "externalPropertyName": "discount_amount",
        // //                                 "hubspotPropertyName": "discount",
        // //                                 "dataType": "NUMBER"
        // //                             }
        // //                         ]
        // //                     }
        // //                     }
        // //                 }';
        // // $result = $this->curlAccessHS('PUT', $args);

        // $args['url'] = 'https://api.hubapi.com/extensions/ecomm/v2/stores/ecommercebridge-whncs-store?hapikey=0fab6b1c-02b4-49d4-ab72-15a1f13a6642';
        // $re = $this->curlAccess1('DELETE',$args);
        // echo '<pre>';
        // print_r($re);
        // die;

        // $args['url'] = 'https://api.hubapi.com/extensions/ecomm/v2/stores?hapikey=0fab6b1c-02b4-49d4-ab72-15a1f13a6642';
        // $args['data'] = '{
        //           "id": "ecommercebridge-whmcs-web-store",
        //           "label": "WHMCS Ecommerce Bridge Store",
        //           "adminUri": "https://apps.webdew.com/hubspot/whmcs-ecommerce-admin"
        //         }';
        // $re = $this->curlAccess1('PUT',$args);
        // echo '<pre>';
        // print_r($re);
        // die;
        // ecommercebridge-test-store
        // $args['url'] = 'https://api.hubapi.com/extensions/ecomm/v2/sync/messages?hapikey=0fab6b1c-02b4-49d4-ab72-15a1f13a6642';

        // $args['data'] = '{
        //     "storeId": "ecommercebridge-whmcs-web-store",
        //     "objectType": "CONTACT",
        //     "messages": [
        //     {
        //         "action": "UPSERT",
        //         "changedAt": "'.time() * 1000 .'",
        //         "externalObjectId": "3051",
        //         "properties": {
        //             "firstname": "Daniel",
        //             "lastname": "Smith",
        //             "customer_email": "danielsmith@gmail.com",
        //             "phone_number": "123-456-7890"
        //         }
        //     }
        //   ]
        // }';

        // $result = $this->curlAccess1('PUT', $args);
        // echo '<pre>';
        // print_r($result);
        die;

        // create settings
        $args['url'] = 'https://api.hubapi.com/extensions/ecomm/v2/settings?appId=224529&hapikey=e1964b7f-c16c-453e-9cc0-efcaeb1ced47';
        $args['data'] = '{
                            "enabled": true,
                            "webhookUri": "https://apps.webdew.com/hubspot/whmcs-ecommerce",
                            "mappings": {
                            "CONTACT": {
                                "properties": [
                                    {
                                        "externalPropertyName": "firstname",
                                        "hubspotPropertyName": "firstname",
                                        "dataType": "STRING"
                                    },
                                    {
                                        "externalPropertyName": "familyname",
                                        "hubspotPropertyName": "lastname",
                                        "dataType": "STRING"
                                    },
                                    {
                                        "externalPropertyName": "customer_email",
                                        "hubspotPropertyName": "email",
                                        "dataType": "STRING"
                                    },
                                    {
                                        "externalPropertyName": "phone_number",
                                        "hubspotPropertyName": "mobilephone",
                                        "dataType": "STRING"
                                    }
                                ]
                            },
                            "DEAL": {
                                "properties": [
                                    {
                                        "externalPropertyName": "purchase_date",
                                        "hubspotPropertyName": "closedate",
                                        "dataType": "STRING"
                                    },
                                    {
                                        "externalPropertyName": "name",
                                        "hubspotPropertyName": "dealname",
                                        "dataType": "STRING"
                                    },
                                    {
                                        "externalPropertyName": "stage",
                                        "hubspotPropertyName": "dealstage",
                                        "dataType": "STRING"
                                    },
                                    {
                                        "externalPropertyName": "abandoned_cart_url",
                                        "hubspotPropertyName": "ip__ecomm_bride__abandoned_cart_url",
                                        "dataType": "STRING"
                                    }
                                ]
                            },
                            "PRODUCT": {
                                "properties": [
                                    {
                                        "externalPropertyName": "product_description",
                                        "hubspotPropertyName": "description",
                                        "dataType": "STRING"
                                    },
                                    {
                                        "externalPropertyName": "price",
                                        "hubspotPropertyName": "price",
                                        "dataType": "NUMBER"
                                    },
                                    {
                                        "externalPropertyName": "product_name",
                                        "hubspotPropertyName": "name",
                                        "dataType": "STRING"
                                    }
                                ]
                            },
                            "LINE_ITEM": {
                                "properties": [
                                    {
                                        "externalPropertyName": "tax_amount",
                                        "hubspotPropertyName": "tax",
                                        "dataType": "NUMBER"
                                    },
                                    {
                                        "externalPropertyName": "num_items",
                                        "hubspotPropertyName": "quantity",
                                        "dataType": "NUMBER"
                                    },
                                    {
                                        "externalPropertyName": "price",
                                        "hubspotPropertyName": "price",
                                        "dataType": "NUMBER"
                                    },
                                    {
                                        "externalPropertyName": "discount_amount",
                                        "hubspotPropertyName": "discount",
                                        "dataType": "NUMBER"
                                    }
                                ]
                            }
                            }
                        }';
        // $result = $this->curlAccessHS('PUT', $args);
        // echo '<pre>';
        // print_r($result);
        // die;
        // create settings end

        $args['url']  = 'https://api.hubapi.com/extensions/ecomm/v2/stores?hapikey=0fab6b1c-02b4-49d4-ab72-15a1f13a6642';
        //$args['url']  = 'https://api.hubapi.com/extensions/ecomm/v2/stores?hapikey=e1964b7f-c16c-453e-9cc0-efcaeb1ced47';
        // $args['data'] = '{
        //           "id": "ecommercebridge-whncs-store",
        //           "label": "Ecommerce Bridge WHMCS Store",
        //           "adminUri": "ecommercebridge-whmcs-store.myshopify.com"
        //         }';
        $result = $this->curlAccessG('GET', $args);
        echo '<pre>';
        print_r($result);
        die;

        $args['url'] = 'https://api.hubapi.com/extensions/ecomm/v2/settings?appId=224529&hapikey=e1964b7f-c16c-453e-9cc0-efcaeb1ced47';

        //$args['url'] = "https://api.hubapi.com/crm-objects/v1/objects/products/paged?hapikey=ebd3b34f-c079-42d6-9acd-ab262aa8594f&properties=name&properties=description&properties=embed_code";
        $result = $this->curlAccessHS('GET', $args);

        // $args['url']  = 'https://api.hubapi.com/extensions/ecomm/v2/stores?appId=224529&hapikey=e1964b7f-c16c-453e-9cc0-efcaeb1ced47';
        // // $args['data'] = '{
        // //                     "id"       : "ecommercebridge-test-store",
        // //                     "label"    : "Ecommerce Bridge Test Store",
        // //                     "adminUri" : "ecommercebridge-test-store.myshopify.com"
        // //                 }';
        
        // //$args['token'] = Auth::user()->auth_token;
        // $result = $this->curlAccessHS('GET', $args);

        echo '<pre>';
        // print_r($result);
        // die;
        
        // $args['url']  = 'https://api.hubapi.com/extensions/ecomm/v2/stores?appId=224529&hapikey=e1964b7f-c16c-453e-9cc0-efcaeb1ced47';
        // $args['data'] = '{
        //                     "id"       : "whmcs-ecommercebridge-store",
        //                     "label"    : "WHMCS Ecommerce Bridge",
        //                     "adminUri" : "apps.webdew.com/hubspot/whmcs/admin"
        //                 }';
        // $args['token'] = 'CM6QmbvmLhICQQEYzPOOBCC_-8MEKJHaDTIZAHKvZ25BQYopsudlpd6w3hwQu04VR9UtTDobAAoCRwAADIADAAgAAAABAAAAAAAAABjAAPMfQhkAcq9nbgAyg_Q3gGKOE2VXf0H80OIghxB9';
        // $result = $this->curlAccessH('PUT', $args);        
        //$args['token'] = Auth::user()->auth_token;
        
        // $result = $this->curlAccessH('PUT', $args);
        print_r($result);
        die;
    }
    // laravel app functionality
    public function get_product_list() {
        
        $users = User::where('id', Auth::user()->id)->first();
        $hapi_key = '';
        if(!empty(DB::table('credentials')->where('type','whmcs')->whereUserId(Auth::user()->id)->value('data'))){
            //$credential_all = unserialize($users->credentials->data);
            $credential_all = unserialize(DB::table('credentials')->where('type','whmcs')->whereUserId(Auth::user()->id)->value('data'));
            $hapi_key = $credential_all['hapi_key'];
        }
        
        if(!empty($hapi_key)){
            $args['url'] = 'https://api.hubapi.com/crm/v3/objects/products?limit=10&hapikey='.$hapi_key;
            if(isset($_GET['offset'])){
                $args['url'] = 'https://api.hubapi.com/crm/v3/objects/products?limit=10&after=' . $_GET['offset'] . '&hapikey='.$hapi_key;
            }
            
            $results = $this->curlAccessH('GET',$args);
            $data = [];
            $next = '';
            $prev = '';
            if(!empty($results->results)){
                foreach($results->results as $key => $value) {
                    $v = json_decode(json_encode($value), true);
                    $data[$key] = $v ;
                    $product_id = $v['id'];
                    $product_name = $v['properties']['name'];
                    $product_price = $v['properties']['price'];
                    $html = view('button.cart-button',compact(['product_id','hapi_key','product_price','product_name']));
                    $data[$key]['html'] = $html;
                }
                if(!empty($results->paging)){
                    $next = url('get-product-list') .'?offset='. $results->paging->next->after;
                }
            }
            return view('product-list-code',compact('data','next'));
        } else {
            echo 'Hubspot key is required.';
        }
    }
    // for creating defination
    public function terms_service() {
        return view('whmcs.websites.terms-service');
    }
    // for creating defination
    public function policy() {
        return view('whmcs.websites.policy');
    }

    public function documentation(){
        return view('whmcs.websites.documentation');
    }

    public function tagline(){
        return view('whmcs.websites.tagline');
    }
    public function home()
    {
        return view('whmcs.websites.home');
    }
}