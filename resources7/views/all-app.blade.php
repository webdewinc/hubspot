@extends('layout.app')
@php
$totalTitle = 'Home';
@endphp
@section('title', $totalTitle)
@section('title-description')

<meta name="description" content="Home">

<style type="text/css">

/* font kit start here */

@font-face {
    font-family: 'proxima_nova_blblack';
    src: url('../hubspot/public/assets/fonts/proxima_nova_black.eot');
    src: url('../hubspot/public/assets/fonts/proxima_nova_black.eot?#iefix') format('embedded-opentype'),
         url('../hubspot/public/assets/fonts/proxima_nova_black.woff2') format('woff2'),
         url('../hubspot/public/assets/fonts/proxima_nova_black.woff') format('woff'),
         url('../hubspot/public/assets/fonts/proxima_nova_black.ttf') format('truetype'),
         url('../hubspot/public/assets/fonts/proxima_nova_black.svg#proxima_nova_blblack') format('svg');
    font-weight: normal;
    font-style: normal;

}
@font-face {
    font-family: 'proxima_nova_rgbold';
    src: url('../hubspot/public/assets/fonts/proxima_nova_bold.eot');
    src: url('../hubspot/public/assets/fonts/proxima_nova_bold.eot?#iefix') format('embedded-opentype'),
         url('../hubspot/public/assets/fonts/proxima_nova_bold.woff2') format('woff2'),
         url('../hubspot/public/assets/fonts/proxima_nova_bold.woff') format('woff'),
         url('../hubspot/public/assets/fonts/proxima_nova_bold.ttf') format('truetype'),
         url('../hubspot/public/assets/fonts/proxima_nova_bold.svg#proxima_nova_rgbold') format('svg');
    font-weight: normal;
    font-style: normal;

}
@font-face {
    font-family: 'proxima_nova_thextrabold';
    src: url('../hubspot/public/assets/fonts/proxima_nova_extrabold.eot');
    src: url('../hubspot/public/assets/fonts/proxima_nova_extrabold.eot?#iefix') format('embedded-opentype'),
         url('../hubspot/public/assets/fonts/proxima_nova_extrabold.woff2') format('woff2'),
         url('../hubspot/public/assets/fonts/proxima_nova_extrabold.woff') format('woff'),
         url('../hubspot/public/assets/fonts/proxima_nova_extrabold.ttf') format('truetype'),
         url('../hubspot/public/assets/fonts/proxima_nova_extrabold.svg#proxima_nova_thextrabold') format('svg');
    font-weight: normal;
    font-style: normal;

}
@font-face {
    font-family: 'proxima_nova_ltlight';
    src: url('../hubspot/public/assets/fonts/proxima_nova_light.eot');
    src: url('../hubspot/public/assets/fonts/proxima_nova_light.eot?#iefix') format('embedded-opentype'),
         url('../hubspot/public/assets/fonts/proxima_nova_light.woff2') format('woff2'),
         url('../hubspot/public/assets/fonts/proxima_nova_light.woff') format('woff'),
         url('../hubspot/public/assets/fonts/proxima_nova_light.ttf') format('truetype'),
         url('../hubspot/public/assets/fonts/proxima_nova_light.svg#proxima_nova_ltlight') format('svg');
    font-weight: normal;
    font-style: normal;

}
@font-face {
    font-family: 'proxima_nova_rgregular';
    src: url('../hubspot/public/assets/fonts/proxima_nova_regular.eot');
    src: url('../hubspot/public/assets/fonts/proxima_nova_regular.eot?#iefix') format('embedded-opentype'),
         url('../hubspot/public/assets/fonts/proxima_nova_regular.woff2') format('woff2'),
         url('../hubspot/public/assets/fonts/proxima_nova_regular.woff') format('woff'),
         url('../hubspot/public/assets/fonts/proxima_nova_regular.ttf') format('truetype'),
         url('../hubspot/public/assets/fonts/proxima_nova_regular.svg#proxima_nova_rgregular') format('svg');
    font-weight: normal;
    font-style: normal;

}
@font-face {
    font-family: 'proxima_nova_ltsemibold';
    src: url('../hubspot/public/assets/fonts/proxima_nova_semibold.eot');
    src: url('../hubspot/public/assets/fonts/proxima_nova_semibold.eot?#iefix') format('embedded-opentype'),
         url('../hubspot/public/assets/fonts/proxima_nova_semibold.woff2') format('woff2'),
         url('../hubspot/public/assets/fonts/proxima_nova_semibold.woff') format('woff'),
         url('../hubspot/public/assets/fonts/proxima_nova_semibold.ttf') format('truetype'),
         url('../hubspot/public/assets/fonts/proxima_nova_semibold.svg#proxima_nova_ltsemibold') format('svg');
    font-weight: normal;
    font-style: normal;

}
@font-face {
    font-family: 'proxima_novathin_italic';
    src: url('../hubspot/public/assets/fonts/proxima_nova_thin_italic.eot');
    src: url('../hubspot/public/assets/fonts/proxima_nova_thin_italic.eot?#iefix') format('embedded-opentype'),
         url('../hubspot/public/assets/fonts/proxima_nova_thin_italic.woff2') format('woff2'),
         url('../hubspot/public/assets/fonts/proxima_nova_thin_italic.woff') format('woff'),
         url('../hubspot/public/assets/fonts/proxima_nova_thin_italic.ttf') format('truetype'),
         url('../hubspot/public/assets/fonts/proxima_nova_thin_italic.svg#proxima_novathin_italic') format('svg');
    font-weight: normal;
    font-style: normal;

}
@font-face {
    font-family: 'proxima_nova_ththin';
    src: url('../hubspot/public/assets/fonts/proxima_nova_thin.eot');
    src: url('../hubspot/public/assets/fonts/proxima_nova_thin.eot?#iefix') format('embedded-opentype'),
         url('../hubspot/public/assets/fonts/proxima_nova_thin.woff2') format('woff2'),
         url('../hubspot/public/assets/fonts/proxima_nova_thin.woff') format('woff'),
         url('../hubspot/public/assets/fonts/proxima_nova_thin.ttf') format('truetype'),
         url('../hubspot/public/assets/fonts/proxima_nova_thin.svg#proxima_nova_ththin') format('svg');
    font-weight: normal;
    font-style: normal;

}
/* font kit end here */

* {
    font-family: 'proxima_nova_rgregular';
    font-weight: normal;
}
h1, h2, h3, h4, h5, h6 {
    font-family: 'proxima_nova_rgbold';
    font-weight: normal;
} 
.smshub-contentBox-colInner {
    padding: 45px 25px 40px;
    border-radius: 10px;
    box-shadow: 0 0 8px rgba(0,0,0,.18);
}
.smshub-contentBox-icon {
    margin-bottom: 35px;
}
.smshub-contentBox-iconInner img {
    height: 115px;
}



</style>
    
@endsection
@section('content')
@section('head')
@parent    
@show
<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor bg-white pt-5" id="kt_content">

        <!-- begin:: Content -->
        <div class="kt-container  kt-grid__item kt-grid__item--fluid home-container">
           
            <div class="row">
                <!-- body content start -->
                <!-- <div class="col-12">
                    <div class="kt_blog-post pb-5">
                        
                        <div class="row justify-content-center">
                            <div class="col-xl-8 col-lg-8 col-md-12">
                                
                                @if (session('success'))
                                <div class="alert alert-success">
                                    {{ session('success') }}
                                </div>
                                @endif
                                @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif

                                <div class="row">
                                    <div class="col">
                                        <div class="card">
                                            <div class="card-header">
                                                
                                            </div>
                                            <div class="card-body">
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>                                  
                    </div>
                </div> -->


<div class="col-12">
    <section class="smshub-contentBox pt-5">
          <div class="container-full">
            <div class="row">
              <div class="col-12">
                <div class="smshub-contentBox-head text-center pb-5">
                  <h2>Our Apps</h2>
                  <p>From right within your HubSpot you can send and receive text messages on the go. Communicate <br> with your customers faster with two-way texting.</p>

                </div>
              </div>
            </div>
            <div class="row smshub-contentBox-row">
              <div class="col-xl-4 col-lg-6">
                <div class="card card-custom smshub-contentBox-col kt-portlet kt-iconbox kt-icon-size kt-iconbox--primary kt-iconbox--animate-slow">
                  <div class="smshub-contentBox-colInner text-center">
                    <div class="smshub-contentBox-icon">
                      <div class="smshub-contentBox-iconInner">
                        <img src="/hubspot/public/assets/media/misc/HubSpot All in One SMS App.png" alt="icon1">
                      </div>
                    </div>
                    <div class="smshub-contentBox-content">
                      <h3>All in One SMS</h3>
                      <p>Send and receive messages from any phone number in the US or Canada. Even if the person using the phone isn't!</p>
                      @if(Auth::check())
                        <?php
                        
                        $id = \DB::table('payments')->where('user_id',Auth::user()->id)->where('app_id',1)->value('id');
                        ?>
                        @if($id < 1)
                          <form role="form" action="{{url('/stripe-payment')}}" method="post"  data-cc-on-file="false" data-stripe-publishable-key="{{env('STRIPE_KEY')}}" id="payment-form">
                            @csrf
                            <input type="hidden" name="email" value="{{Auth::user()->email}}">
                            <input type="hidden" name="type" value="smsdew">
                            <input type="hidden" name="app_id" value="1">
                            <noscript>You must <a href="http://www.enable-javascript.com" target="_blank">enable JavaScript</a> in your web browser in order to pay via Stripe.</noscript>
                                <input 
                                    type="submit"
                                    class="btn btn-brand btn-bold kt-mr-10" 
                                    value="Pay Now"
                                    data-key="{{env('STRIPE_KEY')}}"
                                    data-amount="1900"
                                    data-currency="usd"
                                    data-name="stripe Payment"
                                    data-email="{{Auth::user()->email}}"
                                    id="stripe"
                                />
                                <script src="https://checkout.stripe.com/v2/checkout.js"></script>
                                <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
                                <script>
                                    $(document).ready(function() {
                                        $(document).on('click','#stripe' ,function(event) {
                                            event.preventDefault();


                                            var $button = $(this),
                                                $form = $button.parents('form');


                                            var opts = $.extend({}, $button.data(), {
                                                token: function(result) {
                                                $form.append($('<input>').attr({ type: 'hidden', name: 'stripeToken', value: result.id })).submit();
                                                }
                                            });


                                            StripeCheckout.open(opts);
                                        });
                                    });
                                </script>
                          </form>
                        @else
                            <a href="{{url('/smsdew/auth2-callback')}}" class="btn btn-brand btn-bold kt-mr-10">Install App</a>
                        @endif
                      @endif
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-xl-4 col-lg-6">
                <div class="card card-custom smshub-contentBox-col kt-portlet kt-iconbox kt-icon-size kt-iconbox--primary kt-iconbox--animate-slow">
                  <div class="smshub-contentBox-colInner text-center">
                    <div class="smshub-contentBox-icon">
                      <div class="smshub-contentBox-iconInner">
                        <img src="/hubspot/public/assets/media/misc/HubSpot All in One SMS App.png" alt="icon1">
                      </div>
                    </div>
                    <div class="smshub-contentBox-content">
                      <h3>WHMCS</h3>
                      <p>Send and receive messages from any phone number in the US or Canada. Even if the person using the phone isn't!</p>
                      <form action="javascript:void(0);">
                          <input 
                                type="button"
                                class="btn btn-brand btn-bold kt-mr-10" 
                                value="Coming soon."
                            />
                      </form>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-xl-4 col-lg-6">
                <div class="card card-custom smshub-contentBox-col kt-portlet kt-iconbox kt-icon-size kt-iconbox--primary kt-iconbox--animate-slow">
                  <div class="smshub-contentBox-colInner text-center">
                    <div class="smshub-contentBox-icon">
                      <div class="smshub-contentBox-iconInner">
                        <img src="/hubspot/public/assets/media/misc/HubSpot All in One SMS App.png" alt="icon1">
                      </div>
                    </div>
                    <div class="smshub-contentBox-content">
                      <h3>eCommerce</h3>
                      <p>Send and receive messages from any phone number in the US or Canada. Even if the person using the phone isn't!</p>
                      <form action="javascript:void(0);">
                          <input 
                                type="button"
                                class="btn btn-brand btn-bold kt-mr-10" 
                                value="Coming soon."
                            />
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
    </div>






                <!-- body content end -->
            </div>
        </div>
    </div>                  
</div>

@section('scripts')
@parent
@show
@endsection