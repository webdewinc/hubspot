@extends('layout.website')
@php
$totalTitle = 'Home';
@endphp
@section('title', $totalTitle)
@section('title-description')

<meta name="description" content="Home">
    
@endsection
@section('content')
@section('head')
@parent    
@show
<?php /* ?>
<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor bg-white pt-5" id="kt_content">

        <!-- begin:: Content -->
        <div class="kt-container  kt-grid__item kt-grid__item--fluid home-container">
           
            <div class="row">
                <!-- body content start -->
                <div class="col-12">
                    <div class="kt_blog-post pb-5">
                        
                        <div class="row justify-content-center">
                            <div class="col-xl-8 col-lg-8 col-md-12">
                                
                                @if (session('success'))
                                <div class="alert alert-success">
                                    {{ session('success') }}
                                </div>
                                @endif
                                @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif

                                <div class="row">
                                    <div class="col">
                                        <div class="card">
                                            <div class="card-header">
                                                <label>Welcome to SMS app</label>
                                                <span class="float-right">
                                                    @php
                                                        $portal_id = @Auth::user()->portal_id;
                                                        $email = @Auth::user()->email;
                                                    @endphp
                                                    <!-- <a class="btn btn-secondary" href="{{url('/contacts/'.$portal_id.'/contacts/list/view/all/')}}">Go to Contacts list</a> -->
                                                    <!-- <a class="btn btn-label-brand btn-bold kt-ml-5" href="{{url('logout')}}">Logout</a> -->
                                                </span>
                                            </div>
                                            <div class="card-body">
                                                Portal Id : {{$portal_id}}
                                                <br>
                                                Email : {{$email}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>                                  
                    </div>
                </div>
                <!-- body content end -->
            </div>
        </div>
    </div>                  
</div>
<?php */ ?>
    <!-- banner html start here -->
    
   

    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    @if (Session::has('mailexists'))
 <script>
//  alert('email already exists');
      swal('transcation failed !! user with same email already exists');
</script>
   @endif
   
       @if (Session::has('transaction-failed'))
 <script>
  swal('transaction failed');
</script>
   @endif
   
         @if (Session::has('payment_error'))
 <script>
  swal('you have to do payment first ..!!');
</script>
   @endif
    
    
    <section class="smshub_banner">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-xl-5">
            <div class="smshub_banner-content">
              <h1>SMS Texting to enrich your online business</h1>
              <p>Fast, simple, powerful business text messaging. Connecting your Twilio and HubSpot for sales, marketing, and support.</p>
              <div class="smshub_banner-actionBtn">
<!--                <a class="btn" href="https://app.hubspot.com/oauth/authorize?client_id=0f804048-64f7-4a3a-b7c6-dc08163f36a4&redirect_uri=https://phpstack-242348-1570241.cloudwaysapps.com/hubspot/smsdew/auth2-callback&scope=contacts%20timeline">Get Started</a>-->
                <form role="form" action="{{ route('stripe.payment') }}" method="post"  data-cc-on-file="false"
                                                    data-stripe-publishable-key="{{ env('STRIPE_KEY') }}"  id="payment-form">
                                                @csrf
                                                    <input id="amount" value="" name="amount" type="hidden">
                                                    <input type="hidden" id="tot_price" value="19" />
                                                    <input type="hidden" name="stripe_promo_id" id="stripe_promo_id" value="0" />
                                                    <input type="hidden" id="stripe_wallet" name="wallet" value="19">

 
                                                    <noscript>You must <a href="http://www.enable-javascript.com" target="_blank">enable JavaScript</a> in your web browser in order to pay via Stripe.</noscript>
                                                       
                                                        <!-- <i class="fa fa-credit-card"></i> -->
<!--                                                        class="btn btn-brand btn-upper btn-block"-->
                                                        <input 
                                                            type="submit"
                                                            class="btn" 
                                                            value="Pay Now"
                                                            data-key={{env('STRIPE_KEY')}}
                                                            data-amount="1900"
                                                            data-currency="usd"
                                                            data-name="stripe Payment"
                                                            data-email=""
                                                            id="stripe"
                                                            
                                                        />
                                                         
                                                        
                                                        <script src="https://checkout.stripe.com/v2/checkout.js"></script>
                                                        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
                                                        
                                                        <script>
                                                        $(document).ready(function() {
                                                            $(document).on('click','#stripe' ,function(event) {
                                                                event.preventDefault();

 
                                                                var $button = $(this),
                                                                    $form = $button.parents('form');

 
                                                                var opts = $.extend({}, $button.data(), {
                                                                    token: function(result) {
                                                                    $form.append($('<input>').attr({ type: 'hidden', name: 'stripeToken', value: result.id })).submit();
                                                                    }
                                                                });

 
                                                                StripeCheckout.open(opts);
                                                            });
                                                        });
                                                        </script>
                                                        
                                                         <a style="margin-left:20px;" class="outline-btn" href="{{URL::asset('signin')}}">Login</a>
                                                </form>
                
                
               
               
              </div>
          
            </div>
          </div>
          <div class="col-xl-7">
            <div class="smshub_banner-img">
              
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- banner html end here -->

    <!-- content box html start here -->
    <section class="smshub-contentBox">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="smshub-contentBox-head text-center">
              <h5>TWILIO CONNECT FROM WITHIN YOUR HUBSPOT</h5>
              <h2>A better way to communicate with your contacts</h2>
              <p>From right within your HubSpot you can send and receive text messages on the go. Communicate <br/> with your customers faster with two-way texting.</p>
            </div>
          </div>
        </div>
        <div class="row smshub-contentBox-row">
          <div class="col-xl-4 col-lg-6">
            <div class="smshub-contentBox-col">
              <div class="smshub-contentBox-colInner text-center">
                <div class="smshub-contentBox-icon">
                  <div class="smshub-contentBox-iconInner">
                    <img src="{{ asset('') . config('app.public_url') . '/images/icon1.svg' }}" alt="icon1">
                  </div>
                </div>
                <div class="smshub-contentBox-content">
                  <h3>Hubspot Workflows</h3>
                  <p>Send and receive messages from any phone number in the US or Canada. Even if the person using the phone isn't!</p>
                </div>
              </div>
            </div>
          </div>
          <div class="col-xl-4 col-lg-6">
            <div class="smshub-contentBox-col">
              <div class="smshub-contentBox-colInner text-center">
                <div class="smshub-contentBox-icon">
                  <div class="smshub-contentBox-iconInner">
                    <img src="{{ asset('') . config('app.public_url') . '/images/icon1.svg' }}" alt="icon1">
                  </div>
                </div>
                <div class="smshub-contentBox-content">
                  <h3>Instant Delivery</h3>
                  <p>Send and receive messages from any phone number in the US or Canada. Even if the person using the phone isn't!</p>
                </div>
              </div>
            </div>
          </div>
          <div class="col-xl-4 col-lg-6">
            <div class="smshub-contentBox-col">
              <div class="smshub-contentBox-colInner text-center">
                <div class="smshub-contentBox-icon">
                  <div class="smshub-contentBox-iconInner">
                    <img src="{{ asset('') . config('app.public_url') . '/images/icon1.svg' }}" alt="icon1">
                  </div>
                </div>
                <div class="smshub-contentBox-content">
                  <h3>Twilio Connect</h3>
                  <p>Send and receive messages from any phone number in the US or Canada. Even if the person using the phone isn't!</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- content box html end here -->

    <!-- ready dive html start here -->
    <section class="smshub_ready-drive">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-xl-6 col-lg-6">
            <div class="smshub_ready-drive-text">
              <h2>Ready to dive in? <br/> <span class="primary-color">Start for $19 today!</span></h2>
            </div>
          </div>
          <div class="col-xl-6 col-lg-6">
            <div class="smshub_ready-drive-actionBtn text-right">
<!--              <a class="btn" href="https://app.hubspot.com/oauth/authorize?client_id=0f804048-64f7-4a3a-b7c6-dc08163f36a4&redirect_uri=https://phpstack-242348-1570241.cloudwaysapps.com/hubspot/smsdew/auth2-callback&scope=contacts%20timeline">Get Started</a>-->
             
             
                   <form role="form" action="{{ route('stripe.payment') }}" method="post"  data-cc-on-file="false"
                                                    data-stripe-publishable-key="{{ env('STRIPE_KEY') }}"  id="payment-form">
                                                @csrf
                                                    <input id="amount" value="" name="amount" type="hidden">
                                                    <input type="hidden" id="tot_price" value="19" />
                                                    <input type="hidden" name="stripe_promo_id" id="stripe_promo_id" value="0" />
                                                    <input type="hidden" id="stripe_wallet" name="wallet" value="19">

 
                                                    <noscript>You must <a href="http://www.enable-javascript.com" target="_blank">enable JavaScript</a> in your web browser in order to pay via Stripe.</noscript>
                                                       
                                                        <!-- <i class="fa fa-credit-card"></i> -->
<!--                                                        class="btn btn-brand btn-upper btn-block"-->
                                                        <input 
                                                            type="submit"
                                                            class="btn" 
                                                            value="Pay Now"
                                                            data-key={{env('STRIPE_KEY')}}
                                                            data-amount="1900"
                                                            data-currency="usd"
                                                            data-name="stripe Payment"
                                                            data-email=""
                                                            id="stripe"
                                                            
                                                        />
                                                        <a style="margin-left:20px;" class="outline-btn" href="{{URL::asset('signin')}}">Login</a>
                                       
                                                </form>
             
             
             
             
             
             
             
              
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- ready dive html end here -->
@section('scripts')
@parent
@show
@endsection