<!--begin::Fonts -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700">
<!--end::Fonts -->

<!--begin::Global Theme Styles(used by all pages) -->
<link href="{{ asset('') . config('app.public_url') . '/assets/plugins/global/plugins.bundle.css' }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('') . config('app.public_url') . '/assets/css/style.bundle.css' }}" rel="stylesheet" type="text/css" />
<!--end::Global Theme Styles -->
<!--begin::Layout Skins(used by all pages) -->
<!--end::Layout Skins -->

<!-- <link rel="shortcut icon" href="{{ asset('') . config('app.public_url') . '/images/favicon.ico' }}" /> -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <!-- loader -->
<script type="text/javascript">
    // $(window).load(function() {
    //     $(".loader").fadeOut("slow");
    // });
</script>

