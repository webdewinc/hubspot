<!-- header html start here -->
    <header class="smshub_header">
      <div class="container">
        <div class="smshub_header-inner w-100 d-flex align-items-center">
          <div class="smshub_header-left">
            <div class="smshub_brand-logo">
              <a href="{{URL::asset('smsdew')}}">
                <img src="{{ asset('') . config('app.public_url') . '/images/smsDEW-final-logo.png' }}" alt="smsdew-logo">
              </a>
            </div>
          </div>
          <div class="smshub_header-right">
            <div id="smshub_mmenu"><img src="{{ asset('') . config('app.public_url') . '/images/mobile-menu-icon.svg' }}" alt="mobile-menu-icon"></div>
            <ul id="smshub_menu">
              <li><a href="{{URL::asset('smsdew/policy')}}">Privacy</a></li>
              <li><a href="{{URL::asset('smsdew/terms-service')}}">Terms of Use</a></li>
            </ul>
          </div>
        </div>
      </div>
    </header>
    <!-- header html end here -->