@extends('layout.login')
@php
$totalTitle = 'Sign up';
@endphp
@section('title', $totalTitle)

<style type="text/css">

    /* font kit start here */

@font-face {
    font-family: 'proxima_nova_blblack';
    src: url('../hubspot/public/assets/fonts/proxima_nova_black.eot');
    src: url('../hubspot/public/assets/fonts/proxima_nova_black.eot?#iefix') format('embedded-opentype'),
         url('../hubspot/public/assets/fonts/proxima_nova_black.woff2') format('woff2'),
         url('../hubspot/public/assets/fonts/proxima_nova_black.woff') format('woff'),
         url('../hubspot/public/assets/fonts/proxima_nova_black.ttf') format('truetype'),
         url('../hubspot/public/assets/fonts/proxima_nova_black.svg#proxima_nova_blblack') format('svg');
    font-weight: normal;
    font-style: normal;

}
@font-face {
    font-family: 'proxima_nova_rgbold';
    src: url('../hubspot/public/assets/fonts/proxima_nova_bold.eot');
    src: url('../hubspot/public/assets/fonts/proxima_nova_bold.eot?#iefix') format('embedded-opentype'),
         url('../hubspot/public/assets/fonts/proxima_nova_bold.woff2') format('woff2'),
         url('../hubspot/public/assets/fonts/proxima_nova_bold.woff') format('woff'),
         url('../hubspot/public/assets/fonts/proxima_nova_bold.ttf') format('truetype'),
         url('../hubspot/public/assets/fonts/proxima_nova_bold.svg#proxima_nova_rgbold') format('svg');
    font-weight: normal;
    font-style: normal;

}
@font-face {
    font-family: 'proxima_nova_thextrabold';
    src: url('../hubspot/public/assets/fonts/proxima_nova_extrabold.eot');
    src: url('../hubspot/public/assets/fonts/proxima_nova_extrabold.eot?#iefix') format('embedded-opentype'),
         url('../hubspot/public/assets/fonts/proxima_nova_extrabold.woff2') format('woff2'),
         url('../hubspot/public/assets/fonts/proxima_nova_extrabold.woff') format('woff'),
         url('../hubspot/public/assets/fonts/proxima_nova_extrabold.ttf') format('truetype'),
         url('../hubspot/public/assets/fonts/proxima_nova_extrabold.svg#proxima_nova_thextrabold') format('svg');
    font-weight: normal;
    font-style: normal;

}
@font-face {
    font-family: 'proxima_nova_ltlight';
    src: url('../hubspot/public/assets/fonts/proxima_nova_light.eot');
    src: url('../hubspot/public/assets/fonts/proxima_nova_light.eot?#iefix') format('embedded-opentype'),
         url('../hubspot/public/assets/fonts/proxima_nova_light.woff2') format('woff2'),
         url('../hubspot/public/assets/fonts/proxima_nova_light.woff') format('woff'),
         url('../hubspot/public/assets/fonts/proxima_nova_light.ttf') format('truetype'),
         url('../hubspot/public/assets/fonts/proxima_nova_light.svg#proxima_nova_ltlight') format('svg');
    font-weight: normal;
    font-style: normal;

}
@font-face {
    font-family: 'proxima_nova_rgregular';
    src: url('../hubspot/public/assets/fonts/proxima_nova_regular.eot');
    src: url('../hubspot/public/assets/fonts/proxima_nova_regular.eot?#iefix') format('embedded-opentype'),
         url('../hubspot/public/assets/fonts/proxima_nova_regular.woff2') format('woff2'),
         url('../hubspot/public/assets/fonts/proxima_nova_regular.woff') format('woff'),
         url('../hubspot/public/assets/fonts/proxima_nova_regular.ttf') format('truetype'),
         url('../hubspot/public/assets/fonts/proxima_nova_regular.svg#proxima_nova_rgregular') format('svg');
    font-weight: normal;
    font-style: normal;

}
@font-face {
    font-family: 'proxima_nova_ltsemibold';
    src: url('../hubspot/public/assets/fonts/proxima_nova_semibold.eot');
    src: url('../hubspot/public/assets/fonts/proxima_nova_semibold.eot?#iefix') format('embedded-opentype'),
         url('../hubspot/public/assets/fonts/proxima_nova_semibold.woff2') format('woff2'),
         url('../hubspot/public/assets/fonts/proxima_nova_semibold.woff') format('woff'),
         url('../hubspot/public/assets/fonts/proxima_nova_semibold.ttf') format('truetype'),
         url('../hubspot/public/assets/fonts/proxima_nova_semibold.svg#proxima_nova_ltsemibold') format('svg');
    font-weight: normal;
    font-style: normal;

}
@font-face {
    font-family: 'proxima_novathin_italic';
    src: url('../hubspot/public/assets/fonts/proxima_nova_thin_italic.eot');
    src: url('../hubspot/public/assets/fonts/proxima_nova_thin_italic.eot?#iefix') format('embedded-opentype'),
         url('../hubspot/public/assets/fonts/proxima_nova_thin_italic.woff2') format('woff2'),
         url('../hubspot/public/assets/fonts/proxima_nova_thin_italic.woff') format('woff'),
         url('../hubspot/public/assets/fonts/proxima_nova_thin_italic.ttf') format('truetype'),
         url('../hubspot/public/assets/fonts/proxima_nova_thin_italic.svg#proxima_novathin_italic') format('svg');
    font-weight: normal;
    font-style: normal;

}
@font-face {
    font-family: 'proxima_nova_ththin';
    src: url('../hubspot/public/assets/fonts/proxima_nova_thin.eot');
    src: url('../hubspot/public/assets/fonts/proxima_nova_thin.eot?#iefix') format('embedded-opentype'),
         url('../hubspot/public/assets/fonts/proxima_nova_thin.woff2') format('woff2'),
         url('../hubspot/public/assets/fonts/proxima_nova_thin.woff') format('woff'),
         url('../hubspot/public/assets/fonts/proxima_nova_thin.ttf') format('truetype'),
         url('../hubspot/public/assets/fonts/proxima_nova_thin.svg#proxima_nova_ththin') format('svg');
    font-weight: normal;
    font-style: normal;

}
/* font kit end here */
    
.kt-login__hubspot .kt-login.kt-login--v6 .kt-login__content {
    background-image: linear-gradient(45deg,#21aefa,#00a9fa);
}
.kt-login__hubspot .btn.btn-label-brand {
    background-color: rgba(0, 155, 255, 0.1) !important;
    color: #009bff;
    cursor: text !important;
    border: none;
}
.kt-login__hubspot a.btn.btn-label-brand.btn-bold {
    background-image: url(../hubspot/public/assets/media/misc/hubspot-icon.svg);
    background-repeat: no-repeat;
    background-position: 15px center;
    padding-left: 40px;
    color: #595d6e;
    font-family: 'proxima_nova_rgregular';
    font-weight: normal;
}
.kt-login__hubspot a.btn.btn-label-brand.btn-bold:hover {
    background-color: rgba(0, 155, 255, 0.2) !important;
    color: #595d6e;
}
.kt-login__block .banner-wrapper h1 {
    font-family: 'proxima_nova_ltsemibold';
    font-weight: normal;
}


</style>

@section('content')

<!-- Authentication Links --> 
@if(session()->has('message'))
    <?php
        $form = session()->get('form');
        if($form == ''){
            $form = 'signin';
            $result = 1;
            $message = 'Password has been changed.';
            $alert = 'success';
        } else {
            $result = 0;
        }
        
    ?>
@else 
    <?php
        $form = 'signin';
        $result = 0;
    ?>
@endif
<div class="kt-grid kt-grid--ver kt-grid--root kt-page kt-login__hubspot">
    <div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v6 kt-login--{{$form}}" id="kt_login">
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--desktop kt-grid--ver-desktop kt-grid--hor-tablet-and-mobile">
            <div class="kt-grid__item  kt-grid__item--order-tablet-and-mobile-2  kt-grid kt-grid--hor kt-login__aside">
                <div class="kt-login__wrapper">
                    <div class="kt-login__container">
                        <div class="kt-login__body">
                            <div class="kt-login__logo">
                                <!-- <a href="#">
                                    <img src="assets/media/company-logos/logo-2.png">
                                </a> -->
                                @include('layout.include.partials.brand')
                            </div>
                            
                            <div class="kt-login__signin">
                                <div class="kt-login__form">
                                </div>
                                <div class="kt-login__actions">
                                    <a class="btn btn-label-brand btn-bold" href="{{url('auth2-callback')}}" >Login with HubSpot</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="kt-grid__item kt-grid__item--fluid kt-grid__item--center kt-grid kt-grid--ver kt-login__content" style="/*background-image: url(assets/media/bg/bg-4.jpg);*/">
                <div class="kt-login__section">
                    <div class="kt-login__block">
                        <div class="banner-wrapper text-center">
                            @include('layout.include.partials.banner')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@section('mainscripts')
@parent
@show
@endsection


