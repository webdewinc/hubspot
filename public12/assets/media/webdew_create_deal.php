<?php
include_once("config.php");
$data = json_decode(file_get_contents('php://input'), true);
$jsonobj = json_encode($data);
// search existing deals
$JIC_obj = json_decode($jsonobj);
$student_id = $JIC_obj->StudentInfo->StudentID;
if($student_id  > 0){
  $check = checkDealMap('jic_deal_id', $JIC_obj->StudentInfo->StudentID, $hapikey);

  if($check['status'] === false) {
      $data = objManage($JIC_obj, $hapikey);
      $args['url'] = 'https://api.hubapi.com/deals/v1/deal?hapikey='.$hapikey;
      $args['data'] = json_encode($data);
      $result = curlAccess('POST', $args);
      if(isset($result->status) && $result->status == 'error'){
          echo json_encode(['status' => 'Unsuccess', 'message' => 'Something wrong in field value.', 'response' => $result ]);
      } else {
          $dealId = $result->dealId;
          echo json_encode(['status' => 'Success', 'student_id' => $student_id, 'dealId' => $dealId, 'message' => 'Deal has been created successfully.']);
      }
      die;
  }else {
      $dealId = @$check['data'][0]->id;
      echo json_encode(['status' => 'Success', 'student_id' => $student_id, 'dealId' => $dealId, 'response' => $check, 'message' => 'Deal already exist.']);
  } 
}

function objManage($JIC_obj, $hapikey) {

    
    $associate_counselor_owner_id = owner_match($JIC_obj->StudentInfo->AssociateCounselor, $hapikey);
    //$studentDOB_timestamp = strtotime($JIC_obj->StudentInfo->StudentDOB) * 1000;
    $studentDOB_timestamp = '';
    //$studentDOB_timestamp = $JIC_obj->StudentInfo->StudentDOB;
    $data = [
        "associations" => [
          "associatedCompanyIds" => [
          ],
          "associatedVids" => [
          ]
        ],
        "properties" => [
          [
            "value" => 4036770,
            "name" => "dealstage"
          ],
          [
            "value" => 4036769,
            "name" => "pipeline"
          ],
          [
            "value" => $JIC_obj->AgentInfo->AgentID,
            "name" => "agent_id"
          ],
          [
            "value" => $JIC_obj->AgentInfo->AgentName,
            "name" => "agent_name"
          ],
          [
            "value" => $JIC_obj->AgentInfo->AgentCompanyName,
            "name" => "agent_company_name"
          ],
          [
            "value" => $JIC_obj->AgentInfo->AgentEmailId,
            "name" => "agent_email_id"
          ],
          [
            "value" => $JIC_obj->AgentInfo->AgentMobileNumber,
            "name" => "agent_mobile_number"
          ],
          [
            "value" => $JIC_obj->AgentInfo->AgentCountry,
            "name" => "agent_country"
          ],
          [
            "value" => $JIC_obj->AgentInfo->AgentCity,
            "name" => "agent_city"
          ],
          [
            "value" => $JIC_obj->StudentInfo->StudentID,
            "name" => "jic_deal_id"
          ],
          [
            "value" => $JIC_obj->StudentInfo->StudentName,
            "name" => "dealname"
          ],
          [
            "value" => $JIC_obj->StudentInfo->StudentCode,
            "name" => "student_code"
          ],
          [
            "value" => $JIC_obj->StudentInfo->DealSource,
            "name" => "deal_source"
          ],
          [
            "value" => $associate_counselor_owner_id,
            "name" => "hubspot_owner_id"
          ],
          [
            "value" => $JIC_obj->StudentInfo->Gender,
            "name" => "gender"
          ],
          [
            "value" => $studentDOB_timestamp,
            "name" => "date_of_birth"
          ],
          [
            "value" => $JIC_obj->StudentInfo->StudentEmailID,
            "name" => "student_email_id"
          ],
          [
            "value" => $JIC_obj->StudentInfo->MobileNo,
            "name" => "phone_number"
          ],
          [
            "value" => $JIC_obj->StudentInfo->SkypeID,
            "name" => "skype_id"
          ],          
          [
            "value" => $JIC_obj->StudentInfo->City,
            "name" => "city"
          ],
          [
            "value" => $JIC_obj->StudentInfo->StudentCountry,
            "name" => "student_country"
          ],
          [
            "value" => $JIC_obj->EducationInfo->HighestLevelOfEducation,
            "name" => "highest_level_of_education"
          ],
          [
            "value" => $JIC_obj->EducationInfo->ExamAppeared,
            "name" => "exam_appeared"
          ],
          [
            "value" => $JIC_obj->EducationInfo->ExamScore,
            "name" => "exam_course"
          ],
          [
            "value" => $JIC_obj->EducationInfo->WorkExperience,
            "name" => "work_experience"
          ],
          [
            "value" => $JIC_obj->EducationInfo->Intake,
            "name" => "intake"
          ],
          [
            "value" => $JIC_obj->EducationInfo->IntakeYear,
            "name" => "intake_year"
          ],

          [
            "value" => $JIC_obj->EducationInfo->VisaRefusalBefore,
            "name" => "visa_refusal_in_past_if_applicable_"
          ],
          [
            "value" => $JIC_obj->EducationInfo->VisaRefusalCountry,
            "name" => "previous_visa_refusal_country"
          ],
          [
            "value" => $JIC_obj->EducationInfo->VisaRefusalMonth,
            "name" => "previous_visa_refusal_month"
          ],
          [
            "value" => $JIC_obj->EducationInfo->VisaRefusalYear,
            "name" => "previous_visa_refusal_year"
          ],



          [
            "value" => $JIC_obj->DocumentInfo->Resume,
            "name" => "resume"
          ],
          [
            "value" => $JIC_obj->DocumentInfo->StatementOfPurpose,
            "name" => "statement_of_purpose_sop_"
          ],
          [
            "value" => $JIC_obj->DocumentInfo->N2_LetterOfRecommendation,
            "name" => "n2_letter_of_recommendation"
          ],
          [
            "value" => $JIC_obj->DocumentInfo->PassportFirstPage,
            "name" => "passport_first_page"
          ],
          [
            "value" => $JIC_obj->DocumentInfo->PassportLastPage,
            "name" => "passport_last_page"
          ],
          [
            "value" => $JIC_obj->DocumentInfo->N10th_12thDiplomaMarksheets,
            "name" => "n10th_12th_diploma_marksheets"
          ],
          [
            "value" => $JIC_obj->DocumentInfo->AllBachelorMarksheets,
            "name" => "all_bachelor_mark_sheets"
          ],
          [
            "value" => $JIC_obj->DocumentInfo->DegreeCertificate,
            "name" => "degree_certificate"
          ],
          [
            "value" => $JIC_obj->DocumentInfo->Photo,
            "name" => "photo"
          ],
          [
            "value" => $JIC_obj->DocumentInfo->WorkExpCertificate,
            "name" => "work_exp_certificate"
          ],
          [
            "value" => $JIC_obj->DocumentInfo->ApplicationForm,
            "name" => "application_form"
          ],
          [
            "value" => $JIC_obj->DocumentInfo->Transcripts,
            "name" => "transcripts"
          ]
        ]
    ];
    
    $count = count($data['properties']);
    $newcount = 1;
    foreach($JIC_obj->EducationInfo->SchoolsAndCourse as $k => $v){
      
        $data['properties'][$count] = [
                    "value" => $v->Country,
                    "name" => 'country_'.$newcount
        ];
        $count++;
        $data['properties'][$count] = [
                  "value" => $v->School,
                  "name" => 'school_'.$newcount
        ];
        $count++;
        $data['properties'][$count] = [
                  "value" => $v->Course,
                  "name" => 'course_'.$newcount
        ];
        $count++;
        $newcount++;
    }
    return $data;
}
function checkDealMap($key, $id, $hapikey) {

    $args['url'] = 'https://api.hubapi.com/crm/v3/objects/deals/search?hapikey='. $hapikey;
    $args['data'] = '{
                        "filterGroups":[
                            {
                                "filters":[
                                    {
                                        "propertyName": "'.$key.'",
                                        "operator": "EQ",
                                        "value": "'.$id.'"
                                    }
                                ]
                            }
                        ]
                    }';
    $deals_get = curlAccess( 'POST', $args );    
    
    if($deals_get->status == 'error'){
        return $response = array('status' => false, 'data' => 'something went wrong' );
    } else {
        if($deals_get->total > 0){
            return $response = array('status' => true, 'data' => $deals_get->results );
        } else {
            return $response = array('status' => false, 'data' => $deals_get->results );
        }
    }
   
}
function owner_match($email, $hapikey) {

    $args['url'] = "https://api.hubapi.com/owners/v2/owners?hapikey=".$hapikey;
    $r = curlAccess( 'GET', $args );
    $owner = [];
    foreach($r as $key => $v){
        $owner[$v->email] = $v->ownerId;
    }
    if (array_key_exists($email, $owner)) {
        $ownerId = $owner[$email];
    } else {
        $ownerId = '';
    }
    return $ownerId;
}
// Curl Function Begin
function curlAccess($method, $array, $content_type = 'array' ) {
    
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $array['url']);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    switch ($method){
        case "POST":
            curl_setopt($ch, CURLOPT_POST, 1);        
            if ($array['data'])
                curl_setopt($ch, CURLOPT_POSTFIELDS, $array['data']);
            break;
        case "PUT":
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
            if ($array['data'])
                curl_setopt($ch, CURLOPT_POSTFIELDS, $array['data']);
            break;
        case "PATCH":
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
            if ($array['data'])
                curl_setopt($ch, CURLOPT_POSTFIELDS, $array['data']);
            break;                
        default:
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
    }

    $headers = array();
    $headers[] = 'Content-Type: application/json';
    
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    $result = curl_exec($ch);
    
    if (curl_errno($ch)) {
        echo 'Error:' . curl_error($ch);
    }
    curl_close($ch);

    if($content_type == 'array'){
        $result = json_decode($result); 
    }  
    return $result;
}