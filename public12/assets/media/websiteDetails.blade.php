@foreach($selectQueryWebsite as $key => $split_Query)

    @if($key == 0)
        <?php
        $domain_id = $split_Query['domain_id'];
        $description = "Publish a Guest Post Article on ".$split_Query['domain_url']." with DA:".@$split_Query['data_da_pa']['dp_da']." and boost your organic traffic on search engines."
        ?>

        @section('title-description')

        <meta name="description" content="{{$description}}">
            
        @endsection

        <!-- single website details start -->
        <div class="kt-widget kt-widget--user-profile-3">
            <div class="kt-widget__top">
                <div class="kt-widget__pic kt-widget__pic--danger kt-font-danger kt-font-boldest kt-font-light kt-hidden">
                    JM
                </div>
                <div class="kt-widget__content">
                    <div class="kt-widget__head">
                        <div class="d-flex align-items-center flex-wrap">
                        <div class="d-flex align-items-center">
                           <div class="kt-widget__media kt-hidden-">
                                <!-- <img src="https://d1gwm4cf8hecp4.cloudfront.net/images/favicons/favicon-16x16.png" alt="image"> -->
                                <!-- Display this icon when favicon unavailable -->
                                <!-- <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon favicon-svg">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <rect x="0" y="0" width="24" height="24"></rect>
                                        <circle fill="#000000" opacity="0.3" cx="12" cy="12" r="9"></circle>
                                        <path d="M11.7357634,20.9961946 C6.88740052,20.8563914 3,16.8821712 3,12 C3,11.9168367 3.00112797,11.8339369 3.00336944,11.751315 C3.66233009,11.8143341 4.85636818,11.9573854 4.91262842,12.4204038 C4.9904938,13.0609191 4.91262842,13.8615942 5.45804656,14.101772 C6.00346469,14.3419498 6.15931561,13.1409372 6.6267482,13.4612567 C7.09418079,13.7815761 8.34086797,14.0899175 8.34086797,14.6562185 C8.34086797,15.222396 8.10715168,16.1034596 8.34086797,16.2636193 C8.57458427,16.423779 9.5089688,17.54465 9.50920913,17.7048097 C9.50956962,17.8649694 9.83857487,18.6793513 9.74040201,18.9906563 C9.65905192,19.2487394 9.24857641,20.0501554 8.85059781,20.4145589 C9.75315358,20.7620621 10.7235846,20.9657742 11.7357634,20.9960544 L11.7357634,20.9961946 Z M8.28272988,3.80112099 C9.4158415,3.28656421 10.6744554,3 12,3 C15.5114513,3 18.5532143,5.01097452 20.0364482,7.94408274 C20.069657,8.72412177 20.0638332,9.39135321 20.2361262,9.6327358 C21.1131932,10.8600506 18.0995147,11.7043158 18.5573343,13.5605384 C18.7589671,14.3794892 16.5527814,14.1196773 16.0139722,14.886394 C15.4748026,15.6527403 14.1574598,15.137809 13.8520064,14.9904917 C13.546553,14.8431744 12.3766497,15.3341497 12.4789081,14.4995164 C12.5805657,13.664636 13.2922889,13.6156126 14.0555619,13.2719546 C14.8184743,12.928667 15.9189236,11.7871741 15.3781918,11.6380045 C12.8323064,10.9362407 11.963771,8.47852395 11.963771,8.47852395 C11.8110443,8.44901109 11.8493762,6.74109366 11.1883616,6.69207022 C10.5267462,6.64279981 10.170464,6.88841096 9.20435656,6.69207022 C8.23764828,6.49572949 8.44144409,5.85743687 8.2887174,4.48255778 C8.25453994,4.17415686 8.25619136,3.95717082 8.28272988,3.80112099 Z M20.9991771,11.8770357 C20.9997251,11.9179585 21,11.9589471 21,12 C21,16.9406923 17.0188468,20.9515364 12.0895088,20.9995641 C16.970233,20.9503326 20.9337111,16.888438 20.9991771,11.8770357 Z" fill="#000000" opacity="0.3"></path>
                                    </g>
                                </svg> -->

                                <img width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon favicon-svg" src="<?php echo 'https://www.google.com/s2/favicons?domain='. $split_Query['domain_url']; ?>" />
                            </div>
                            
                            <a href="{{url('search/website/')}}{{'/'.$split_Query['domain_url']}}" class="kt-widget__username">
                            {{$split_Query['domain_url']}}

                            {{domain_status($split_Query['domain_url'])}}
                            </a>&nbsp;
                            </div>
                            <span>
                                @if(!empty($split_Query['backlink_type']))

                                    @if($split_Query['backlink_type'] == 'no_follow')
                                        @if($follow > 0)
                                            @php
                                                $follow = 'Do Follow';
                                                $class = 'success';
                                            @endphp
                                        @else
                                            @php
                                                $follow = 'No Follow';
                                                $class = 'warning';
                                            @endphp
                                        @endif
                                    @elseif($split_Query['backlink_type'] == 'follow')
                                        @php
                                            $follow = 'Do Follow';
                                            $class = 'success';
                                        @endphp
                                    @endif
                                @else
                                    @if($follow > 0)
                                        @php
                                            $follow = 'Do Follow';
                                            $class = 'success';
                                        @endphp
                                    @else
                                        @php
                                            $follow = 'No Follow';
                                            $class = 'warning';
                                        @endphp
                                    @endif
                                @endif
                                <span class="kt-badge kt-badge--{{$class}} kt-badge--dot"></span>&nbsp;
                                <span class="kt-font-bold kt-font-{{$class}}">
                                   {{$follow}} 
                                </span>
                            </span>
                        </div>
                        <div class="kt-widget__action d-flex align-items-center">
                            <div class="website-price">
                                {{--$ {{buyerDomainPriceCalculate($split_Query['cost_price'], $split_Query['extra_cost'])}}--}}
                                </div>&nbsp; &nbsp;
                        </div>
                    </div>
                    <div class="kt-widget__subhead">
                        
                        @if(trim($split_Query['data_seo']['sd_title']))
                            {{ $split_Query['data_seo']['sd_title'] }}
                        @elseif(trim($split_Query['data_seo']['sd_title_n']))
                            {{ $split_Query['data_seo']['sd_title_n'] }}
                        @else
                            {{ $split_Query['title']}}
                        @endif

                    </div>
                    <div class="kt-widget__info">
                        <div class="kt-widget__desc">
                            @if(!empty($split_Query['data_seo']['sd_descr']))
                                {{ @$split_Query['data_seo']['sd_descr'] }}
                            @elseif(trim($split_Query['data_seo']['sd_descr_n']))
                                {{ @$split_Query['data_seo']['sd_descr_n'] }}
                            @else
                                {{ @$split_Query['descrition'] }}
                            @endif
                        </div>
                        
                    </div>
                </div>
            </div>
            <div class="">
                <ul class="nav nav-tabs nav-tabs-line nav-tabs-line-primary nav-tabs-line-2x kt-mt-20" role="tablist" style="border-top: 2px solid rgba(0, 155, 255, 0.1);">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#kt_portlet_base_demo_1_1_tab_content" role="tab" aria-selected="true">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <rect x="0" y="0" width="24" height="24" />
                                    <path d="M3,12 C3,12 5.45454545,6 12,6 C16.9090909,6 21,12 21,12 C21,12 16.9090909,18 12,18 C5.45454545,18 3,12 3,12 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
                                    <path d="M12,15 C10.3431458,15 9,13.6568542 9,12 C9,10.3431458 10.3431458,9 12,9 C13.6568542,9 15,10.3431458 15,12 C15,13.6568542 13.6568542,15 12,15 Z" fill="#000000" opacity="0.3" />
                                </g>
                            </svg>
                            </i>Overview
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#kt_portlet_base_demo_1_2_tab_content" role="tab" aria-selected="false">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <rect x="0" y="0" width="24" height="24" />
                                    <path d="M4.00246329,12.2004927 L13,14 L13,4.06189375 C16.9463116,4.55399184 20,7.92038235 20,12 C20,16.418278 16.418278,20 12,20 C7.64874861,20 4.10886412,16.5261253 4.00246329,12.2004927 Z" fill="#000000" opacity="0.3" />
                                    <path d="M3.0603968,10.0120794 C3.54712466,6.05992157 6.91622084,3 11,3 L11,11.6 L3.0603968,10.0120794 Z" fill="#000000" />
                                </g>
                            </svg>
                            Traffic
                        </a>
                    </li>
                    
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#kt_portlet_base_demo_1_3_tab_content" role="tab" aria-selected="false">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <rect x="0" y="0" width="24" height="24"></rect>
                                    <path d="M14.2928932,16.7071068 C13.9023689,16.3165825 13.9023689,15.6834175 14.2928932,15.2928932 C14.6834175,14.9023689 15.3165825,14.9023689 15.7071068,15.2928932 L19.7071068,19.2928932 C20.0976311,19.6834175 20.0976311,20.3165825 19.7071068,20.7071068 C19.3165825,21.0976311 18.6834175,21.0976311 18.2928932,20.7071068 L14.2928932,16.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"></path>
                                    <path d="M11,16 C13.7614237,16 16,13.7614237 16,11 C16,8.23857625 13.7614237,6 11,6 C8.23857625,6 6,8.23857625 6,11 C6,13.7614237 8.23857625,16 11,16 Z M11,18 C7.13400675,18 4,14.8659932 4,11 C4,7.13400675 7.13400675,4 11,4 C14.8659932,4 18,7.13400675 18,11 C18,14.8659932 14.8659932,18 11,18 Z" fill="#000000" fill-rule="nonzero"></path>
                                </g>
                            </svg>
                            Searches
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#kt_portlet_base_demo_1_4_tab_content" role="tab" aria-selected="false">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <polygon points="0 0 24 0 24 24 0 24"></polygon>
                                    <path d="M12,11 C9.790861,11 8,9.209139 8,7 C8,4.790861 9.790861,3 12,3 C14.209139,3 16,4.790861 16,7 C16,9.209139 14.209139,11 12,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"></path>
                                    <path d="M3.00065168,20.1992055 C3.38825852,15.4265159 7.26191235,13 11.9833413,13 C16.7712164,13 20.7048837,15.2931929 20.9979143,20.2 C21.0095879,20.3954741 20.9979143,21 20.2466999,21 C16.541124,21 11.0347247,21 3.72750223,21 C3.47671215,21 2.97953825,20.45918 3.00065168,20.1992055 Z" fill="#000000" fill-rule="nonzero"></path>
                                </g>
                            </svg>
                            Referrals
                        </a>
                    </li>
                    
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#kt_portlet_base_demo_1_5_tab_content" role="tab" aria-selected="false">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <rect x="0" y="0" width="24" height="24" />
                                    <rect fill="#000000" opacity="0.3" x="2" y="2" width="20" height="20" rx="10" />
                                    <path d="M6.16794971,14.5547002 C5.86159725,14.0951715 5.98577112,13.4743022 6.4452998,13.1679497 C6.90482849,12.8615972 7.52569784,12.9857711 7.83205029,13.4452998 C8.9890854,15.1808525 10.3543313,16 12,16 C13.6456687,16 15.0109146,15.1808525 16.1679497,13.4452998 C16.4743022,12.9857711 17.0951715,12.8615972 17.5547002,13.1679497 C18.0142289,13.4743022 18.1384028,14.0951715 17.8320503,14.5547002 C16.3224187,16.8191475 14.3543313,18 12,18 C9.64566871,18 7.67758127,16.8191475 6.16794971,14.5547002 Z" fill="#000000" />
                                </g>
                            </svg>
                            Social
                        </a>
                    </li>
                    
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="kt_portlet_base_demo_1_1_tab_content" role="tabpanel">
                        <div class="kt-widget17 kt-mt-20">
                            <div class="kt-widget17__stats w-100 kt-margin-0">
                                <div class="kt-widget17__items kt-mb-25 flex-wrap">
                                    <div class="row w-100">
                                        <div class="kt-widget17__item d-flex col-12 col-md-6">
                                            <span class="kt-widget17__icon">
                                                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 280.6 280.3" style="enable-background:new 0 0 280.6 280.3;width:40px;" xml:space="preserve">
                                                    <path class="st0" d="M140.2,280.3C66.1,282.2-0.6,217.8,0,138.9C0.6,64.8,64.4-2.1,144.5,0c71.7,1.9,136.8,63.8,136.1,141.7
                                                        C280,218.2,214.6,282.3,140.2,280.3z M185.1,139.7c0.4,0.1,0.7,0.3,1.1,0.4c0,13.1,0.1,26.2-0.1,39.3c-0.1,4.7,2.1,6.7,6.6,6.6
                                                        c6.3-0.1,12.7-0.1,19,0c2.5,0,3.6-0.7,3.5-3.4c-0.1-28.3-0.1-56.6,0-85c0-2.8-1.2-3.3-3.6-3.3c-5.2,0.1-10.3,0.1-15.5,0
                                                        c-6.6-0.1-11.8,2.3-16.2,7.3c-12,13.6-24.3,27-36.3,40.6c-2.6,2.9-4,3.3-6.8,0.1c-12.1-13.8-24.5-27.3-36.6-41
                                                        c-3.6-4.1-7.9-6.7-13.3-6.9c-6.2-0.3-12.3,0-18.5-0.1c-2.4,0-3.2,0.8-3.2,3.2c0.1,28.3,0.1,56.6,0,85c0,2.6,0.8,3.5,3.4,3.5
                                                        c6.3-0.1,12.7,0,19-0.1c5.2-0.1,6.6-1.7,6.6-7.1c0-11.8,0-23.7,0.1-35.5c0-1.2-0.8-2.6,1-3.7c12.9,14.4,25.8,28.7,38.6,43.1
                                                        c4.2,4.7,8,4.8,12.4-0.1C159.4,168.4,172.3,154.1,185.1,139.7z"></path>
                                                    <path class="st1" d="M185.1,139.7c-12.9,14.4-25.8,28.7-38.7,43.1c-4.3,4.8-8.2,4.8-12.4,0.1c-12.9-14.4-25.8-28.7-38.6-43.1
                                                        c-1.8,1-1,2.5-1,3.7c-0.1,11.8,0,23.7-0.1,35.5c0,5.4-1.4,7-6.6,7.1c-6.3,0.1-12.7-0.1-19,0.1c-2.6,0.1-3.4-0.9-3.4-3.5
                                                        c0.1-28.3,0.1-56.6,0-85c0-2.4,0.8-3.2,3.2-3.2c6.2,0.1,12.3-0.2,18.5,0.1c5.4,0.2,9.7,2.8,13.3,6.9c12.2,13.7,24.6,27.2,36.6,41
                                                        c2.8,3.2,4.2,2.8,6.8-0.1c12-13.6,24.3-27,36.3-40.6c4.4-5,9.7-7.4,16.2-7.3c5.2,0.1,10.3,0.1,15.5,0c2.4-0.1,3.6,0.4,3.6,3.3
                                                        c-0.1,28.3-0.1,56.6,0,85c0,2.7-1.1,3.4-3.5,3.4c-6.3-0.1-12.7-0.1-19,0c-4.6,0.1-6.7-1.9-6.6-6.6c0.1-13.1,0.1-26.2,0.1-39.3
                                                        C185.9,140,185.5,139.8,185.1,139.7z"></path>
                                                </svg>
                                            </span>
                                            <div>
                                                <span class="kt-widget17__subtitle">
                                                    <div class="d-flex"><span class="kt-widget__value">{{@$split_Query['data_da_pa']['dp_da']}}</span>
                                                        <span class="d-flex align-items-center" style="
                                                            font-size: 1rem;
                                                            ">/100</span>
                                                    </div>
                                                </span>
                                                <span class="kt-widget17__desc"><span>Domain Authority </span><br><span style="
                                                    color: #009bff;
                                                    font-weight: 100;
                                                    "> By MOZ</span></span>
                                            </div>
                                        </div>
                                        <div class="kt-widget17__item d-flex col-12 col-md-6">
                                            <span class="kt-widget17__icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="34px" height="34px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                        <path d="M13,18.9450712 L13,20 L14,20 C15.1045695,20 16,20.8954305 16,22 L8,22 C8,20.8954305 8.8954305,20 10,20 L11,20 L11,18.9448245 C9.02872877,18.7261967 7.20827378,17.866394 5.79372555,16.5182701 L4.73856106,17.6741866 C4.36621808,18.0820826 3.73370941,18.110904 3.32581341,17.7385611 C2.9179174,17.3662181 2.88909597,16.7337094 3.26143894,16.3258134 L5.04940685,14.367122 C5.46150313,13.9156769 6.17860937,13.9363085 6.56406875,14.4106998 C7.88623094,16.037907 9.86320756,17 12,17 C15.8659932,17 19,13.8659932 19,10 C19,7.73468744 17.9175842,5.65198725 16.1214335,4.34123851 C15.6753081,4.01567657 15.5775721,3.39010038 15.903134,2.94397499 C16.228696,2.49784959 16.8542722,2.4001136 17.3003976,2.72567554 C19.6071362,4.40902808 21,7.08906798 21,10 C21,14.6325537 17.4999505,18.4476269 13,18.9450712 Z" fill="#000000" fill-rule="nonzero"></path>
                                                        <circle fill="#000000" opacity="0.3" cx="12" cy="10" r="6"></circle>
                                                    </g>
                                                </svg>
                                            </span>
                                            <div>
                                                <span class="kt-widget17__subtitle">
                                                    @php
                                                        $global_rank = 0;
                                                    @endphp
                                                    @if(@$split_Query['data_seo']['sd_global_rank'])
                                                        @php
                                                            $global_rank = @unserialize(@$split_Query['data_seo']['sd_global_rank']);
                                                            $global_rank = @$global_rank['rank'];
                                                        @endphp
                                                    @endif
                                                    {{$global_rank}}
                                                </span>
                                                <span class="kt-widget17__desc"><span>Global Rank </span><br><span style="
                                                    color: #009bff;
                                                    font-weight: 100;
                                                    "> Worldwide</span></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row w-100">
                                        <div class="kt-widget17__item d-flex col-12 col-md-6">
                                            <span class="kt-widget17__icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                        <circle fill="#000000" opacity="0.3" cx="12" cy="12" r="9"></circle>
                                                        <path d="M11.7357634,20.9961946 C6.88740052,20.8563914 3,16.8821712 3,12 C3,11.9168367 3.00112797,11.8339369 3.00336944,11.751315 C3.66233009,11.8143341 4.85636818,11.9573854 4.91262842,12.4204038 C4.9904938,13.0609191 4.91262842,13.8615942 5.45804656,14.101772 C6.00346469,14.3419498 6.15931561,13.1409372 6.6267482,13.4612567 C7.09418079,13.7815761 8.34086797,14.0899175 8.34086797,14.6562185 C8.34086797,15.222396 8.10715168,16.1034596 8.34086797,16.2636193 C8.57458427,16.423779 9.5089688,17.54465 9.50920913,17.7048097 C9.50956962,17.8649694 9.83857487,18.6793513 9.74040201,18.9906563 C9.65905192,19.2487394 9.24857641,20.0501554 8.85059781,20.4145589 C9.75315358,20.7620621 10.7235846,20.9657742 11.7357634,20.9960544 L11.7357634,20.9961946 Z M8.28272988,3.80112099 C9.4158415,3.28656421 10.6744554,3 12,3 C15.5114513,3 18.5532143,5.01097452 20.0364482,7.94408274 C20.069657,8.72412177 20.0638332,9.39135321 20.2361262,9.6327358 C21.1131932,10.8600506 18.0995147,11.7043158 18.5573343,13.5605384 C18.7589671,14.3794892 16.5527814,14.1196773 16.0139722,14.886394 C15.4748026,15.6527403 14.1574598,15.137809 13.8520064,14.9904917 C13.546553,14.8431744 12.3766497,15.3341497 12.4789081,14.4995164 C12.5805657,13.664636 13.2922889,13.6156126 14.0555619,13.2719546 C14.8184743,12.928667 15.9189236,11.7871741 15.3781918,11.6380045 C12.8323064,10.9362407 11.963771,8.47852395 11.963771,8.47852395 C11.8110443,8.44901109 11.8493762,6.74109366 11.1883616,6.69207022 C10.5267462,6.64279981 10.170464,6.88841096 9.20435656,6.69207022 C8.23764828,6.49572949 8.44144409,5.85743687 8.2887174,4.48255778 C8.25453994,4.17415686 8.25619136,3.95717082 8.28272988,3.80112099 Z M20.9991771,11.8770357 C20.9997251,11.9179585 21,11.9589471 21,12 C21,16.9406923 17.0188468,20.9515364 12.0895088,20.9995641 C16.970233,20.9503326 20.9337111,16.888438 20.9991771,11.8770357 Z" fill="#000000" opacity="0.7"></path>
                                                    </g>
                                                </svg>
                                            </span>
                                            <div>
                                            @php
                                                $country_rank = 0;
                                                $country_name ='NA';
                                            @endphp

                                            @if(!empty(@unserialize(@$split_Query['data_seo']['sd_country_rank'])))
                                                @php
                                                    $sd_country_rank = @unserialize($split_Query['data_seo']['sd_country_rank']);
                                                    $country_rank = @$sd_country_rank['rank'];
                                                    $country_name = @$sd_country_rank['country'];
                                                @endphp
                                            @endif
                                                <span class="kt-widget17__subtitle">
                                                {{$country_rank}}
                                                </span>
                                                <span class="kt-widget17__desc"><span>Country Rank </span><br><span style="
                                                    color: #009bff;
                                                    font-weight: 100;
                                                    "> {{$country_name}}</span></span>
                                            </div>
                                        </div>
                                        <div class="kt-widget17__item d-flex col-12 col-md-6">
                                            <span class="kt-widget17__icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                        <path d="M4.00246329,12.2004927 L13,14 L13,4.06189375 C16.9463116,4.55399184 20,7.92038235 20,12 C20,16.418278 16.418278,20 12,20 C7.64874861,20 4.10886412,16.5261253 4.00246329,12.2004927 Z" fill="#000000" opacity="0.3"></path>
                                                        <path d="M3.0603968,10.0120794 C3.54712466,6.05992157 6.91622084,3 11,3 L11,11.6 L3.0603968,10.0120794 Z" fill="#000000"></path>
                                                    </g>
                                                </svg>
                                            </span>
                                            <div>
                                                <span class="kt-widget17__subtitle">
                                                    @php
                                                        $category_rank = 0;
                                                        $category_name ='NA';
                                                    @endphp

                                                    @if(!empty(@unserialize(@$split_Query['data_seo']['sd_category_rank'])))
                                                        @php
                                                            $sd_category_rank = @unserialize($split_Query['data_seo']['sd_category_rank']);
                                                            $category_rank = @$sd_category_rank['rank'];
                                                            $category_name = @$sd_category_rank['category'];
                                                        @endphp
                                                    @endif
                                                {{$category_rank}}
                                                </span>
                                                <span class="kt-widget17__desc"><span>Category Rank </span><br><span style="color: #009bff;font-weight: 100;word-break: break-all;">{{$category_name}}</span></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="kt_portlet_base_demo_1_2_tab_content" role="tabpanel">
                        <div class="kt-widget17 kt-mt-20">
                            <div class="kt-widget17__stats w-100 kt-margin-0">
                                <div class="kt-widget17__items kt-mb-25 flex-wrap">
                                    <div class="row w-100">
                                        <div class="kt-widget17__item d-flex col-12 col-md-6">
                                            <span class="kt-widget17__icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                        <path d="M4.00246329,12.2004927 L13,14 L13,4.06189375 C16.9463116,4.55399184 20,7.92038235 20,12 C20,16.418278 16.418278,20 12,20 C7.64874861,20 4.10886412,16.5261253 4.00246329,12.2004927 Z" fill="#000000" opacity="0.3"></path>
                                                        <path d="M3.0603968,10.0120794 C3.54712466,6.05992157 6.91622084,3 11,3 L11,11.6 L3.0603968,10.0120794 Z" fill="#000000"></path>
                                                    </g>
                                                </svg>
                                            </span>
                                            <div>
                                                <span class="kt-widget17__subtitle">{{@$split_Query['data_seo']['sd_visits']}}</span>
                                                <span class="kt-widget17__desc"><span>Total Traffic</span></span>
                                            </div>
                                        </div>
                                        <div class="kt-widget17__item d-flex col-12 col-md-6">
                                            <span class="kt-widget17__icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                        <path d="M12,22 C7.02943725,22 3,17.9705627 3,13 C3,8.02943725 7.02943725,4 12,4 C16.9705627,4 21,8.02943725 21,13 C21,17.9705627 16.9705627,22 12,22 Z" fill="#000000" opacity="0.3"></path>
                                                        <path d="M11.9630156,7.5 L12.0475062,7.5 C12.3043819,7.5 12.5194647,7.69464724 12.5450248,7.95024814 L13,12.5 L16.2480695,14.3560397 C16.403857,14.4450611 16.5,14.6107328 16.5,14.7901613 L16.5,15 C16.5,15.2109164 16.3290185,15.3818979 16.1181021,15.3818979 C16.0841582,15.3818979 16.0503659,15.3773725 16.0176181,15.3684413 L11.3986612,14.1087258 C11.1672824,14.0456225 11.0132986,13.8271186 11.0316926,13.5879956 L11.4644883,7.96165175 C11.4845267,7.70115317 11.7017474,7.5 11.9630156,7.5 Z" fill="#000000"></path>
                                                    </g>
                                                </svg>
                                            </span>
                                            <div>
                                                <span class="kt-widget17__subtitle">{{@$split_Query['data_seo']['sd_time_on_site_avg']}}</span>
                                                <span class="kt-widget17__desc"><span>Avgerage time on site</span></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row w-100">
                                        <div class="kt-widget17__item d-flex col-12 col-md-6">
                                            <span class="kt-widget17__icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                        <path d="M3,12 C3,12 5.45454545,6 12,6 C16.9090909,6 21,12 21,12 C21,12 16.9090909,18 12,18 C5.45454545,18 3,12 3,12 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"></path>
                                                        <path d="M12,15 C10.3431458,15 9,13.6568542 9,12 C9,10.3431458 10.3431458,9 12,9 C13.6568542,9 15,10.3431458 15,12 C15,13.6568542 13.6568542,15 12,15 Z" fill="#000000" opacity="0.8"></path>
                                                    </g>
                                                </svg>
                                            </span>
                                            <div>
                                                <span class="kt-widget17__subtitle">{{@$split_Query['data_seo']['sd_page_views_avg']}}</span>
                                                <span class="kt-widget17__desc"><span>Average Page views</span></span>
                                            </div>
                                        </div>
                                        <div class="kt-widget17__item d-flex col-12 col-md-6">
                                            <span class="kt-widget17__icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon" style="width:40px;">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                        <path d="M10.9,2 C11.4522847,2 11.9,2.44771525 11.9,3 C11.9,3.55228475 11.4522847,4 10.9,4 L6,4 C4.8954305,4 4,4.8954305 4,6 L4,18 C4,19.1045695 4.8954305,20 6,20 L18,20 C19.1045695,20 20,19.1045695 20,18 L20,16 C20,15.4477153 20.4477153,15 21,15 C21.5522847,15 22,15.4477153 22,16 L22,18 C22,20.209139 20.209139,22 18,22 L6,22 C3.790861,22 2,20.209139 2,18 L2,6 C2,3.790861 3.790861,2 6,2 L10.9,2 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"></path>
                                                        <path d="M24.0690576,13.8973499 C24.0690576,13.1346331 24.2324969,10.1246259 21.8580869,7.73659596 C20.2600137,6.12944276 17.8683518,5.85068794 15.0081639,5.72356847 L15.0081639,1.83791555 C15.0081639,1.42370199 14.6723775,1.08791555 14.2581639,1.08791555 C14.0718537,1.08791555 13.892213,1.15726043 13.7542266,1.28244533 L7.24606818,7.18681951 C6.93929045,7.46513642 6.9162184,7.93944934 7.1945353,8.24622707 C7.20914339,8.26232899 7.22444472,8.27778811 7.24039592,8.29256062 L13.7485543,14.3198102 C14.0524605,14.6012598 14.5269852,14.5830551 14.8084348,14.2791489 C14.9368329,14.140506 15.0081639,13.9585047 15.0081639,13.7695393 L15.0081639,9.90761477 C16.8241562,9.95755456 18.1177196,10.0730665 19.2929978,10.4469645 C20.9778605,10.9829796 22.2816185,12.4994368 23.2042718,14.996336 L23.2043032,14.9963244 C23.313119,15.2908036 23.5938372,15.4863432 23.9077781,15.4863432 L24.0735976,15.4863432 C24.0735976,15.0278051 24.0690576,14.3014082 24.0690576,13.8973499 Z" fill="#000000" fill-rule="nonzero" transform="translate(15.536799, 8.287129) scale(-1, 1) translate(-15.536799, -8.287129) "></path>
                                                    </g>
                                                </svg>
                                            </span>
                                            <div>
                                                <span class="kt-widget17__subtitle">{{@$split_Query['data_seo']['sd_bounce_rate']}}</span>
                                                <span class="kt-widget17__desc"><span>Bounce Rate</span></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        @if($array['cnf'])
                        <div class="kt-portlet kt-portlet--height-fluid">
                            <div class="kt-widget14">
                                <div class="kt-widget14__header text-center">
                                    <h3 class="kt-widget14__title">Traffic by Countries</h3>
                                </div>
                                <div class="kt-widget14__content">
                                    <div class="kt-widget14__chart flex-grow-1 d-flex">
                                        <div id="kt_chart_revenue_change" style="height: 200px; width: 200px;margin: auto;"></div>
                                    </div>
                                    <div class="kt-widget14__legends">
                                        @php
                                            $arrayName = array(0 => 'kt-bg-success', 1 => 'kt-bg-danger', 2 => 'kt-bg-light', 3 => 'kt-bg-warning', 4 => 'kt-bg-dark', 5 => 'kt-bg-primary');
                                            $count = 0;
                                        @endphp
                                        @foreach($array['cnf'] as $h => $hv)
                                            @php
                                                if($count == 5):
                                                    $count = 0;
                                                endif;
                                            @endphp
                                            <div class="kt-widget14__legend">
                                                <span class="kt-widget14__bullet {{$arrayName[$h]}}"></span>
                                                <span class="kt-widget14__stats">{{$hv['label']}}</span>
                                            </div>
                                            @php
                                                $count++;
                                            @endphp
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                        <div class="kt-portlet kt-portlet--height-fluid">
                            <div class="kt-widget14">
                                <div class="kt-widget14__header text-center">
                                    <h3 class="kt-widget14__title">Traffic by Sources</h3>
                                </div>
                                <div class="kt-widget14__content">
                                    <div class="kt-widget14__chart flex-grow-1 d-flex">
                                        <div id="kt_chart_revenue_change1" style="height: 200px; width: 200px;margin: auto;"></div>
                                    </div>
                                    <div class="kt-widget14__legends">
                                        @php
                                            $arrayName = array(0 => 'kt-bg-primary', 1 => 'kt-bg-danger', 2 => 'kt-bg-light', 3 => 'kt-bg-warning', 4 => 'kt-bg-dark', 5 => 'kt-bg-success');
                                            $count = 0;
                                        @endphp
                                        @foreach($array['org'] as $h => $hv)

                                            @if(!empty($hv['value']))
                                                @php
                                                    if($count == 5):
                                                        $count = 0;
                                                    endif;
                                                @endphp                                            
                                                <div class="kt-widget14__legend">
                                                    <span class="kt-widget14__bullet {{$arrayName[$count]}}"></span>
                                                    <span class="kt-widget14__stats">{{$hv['label']}}</span>
                                                </div>
                                                @php
                                                    $count++;
                                                @endphp
                                            @endif
                                        @endforeach                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    
                    <div class="tab-pane" id="kt_portlet_base_demo_1_3_tab_content" role="tabpanel">
                        @if($array['org_search'])
                        <?php
                        // print_r($array['org_search']);
                        // die;
                        ?>
                        <div class="kt-widget14">
                            <div class="kt-widget14__header text-center">
                                <h3 class="kt-widget14__title">Traffic by Searches</h3>
                            </div>


                            <div class="kt-widget14__content justify-content-center">
                                <div class="kt-widget14__chart">
                                    <div id="kt_chart_revenue_change2" style="height: 200px; width: 200px;"></div>
                                </div>
                            </div>
                        </div>
                        @endif
                        <div class="kt-portlet__body">
                            <div class="kt-scroll ps ps--active-y" data-scroll="true" data-height="380" data-mobile-height="300" style="height: 300px; overflow: hidden;">
                                <div class="d-flex traffic-searches-container">
                                    <div class="flex-grow-1 col-6 organic-section kt-padding-20">
                                        <h3 class="kt-font-primary">Organic</h3>
                                        @if(!empty($split_Query['data_organic_search']))
                                            @foreach($split_Query['data_organic_search'] as $okey => $ova)
                                            <div class="kt-padding-20 position-relative">
                                                <span class="keyword-item">{{@$ova['org_keyword']}}
                                                    @if(!empty($ova['org_percent']))
                                                        <strong>{{$ova['org_percent']}}%</strong>
                                                    @else 
                                                        <strong>{{'0'}}%</strong>
                                                    @endif                                                    
                                                </span>
                                                <i class="fa fa-genderless kt-font-primary"></i>
                                            </div>
                                            @endforeach
                                        @endif
                                        <!-- <div class="kt-padding-20 position-relative">
                                            <span class="keyword-item">asana login<br><strong>2.74%</strong></span>
                                            <i class="fa fa-genderless kt-font-primary"></i>
                                        </div>
                                        <div class="kt-padding-20 position-relative">
                                            <span class="keyword-item">asana pricing<br><strong>1.10%</strong></span>
                                            <i class="fa fa-genderless kt-font-primary"></i>
                                        </div>
                                        <div class="kt-padding-20 position-relative">
                                            <span class="keyword-item">asana careers<br><strong>0.50%</strong></span>
                                            <i class="fa fa-genderless kt-font-primary"></i>
                                        </div>
                                        <div class="kt-padding-20 position-relative">
                                            <span class="keyword-item">asana app<br><strong>0.44%</strong></span>
                                            <i class="fa fa-genderless kt-font-primary"></i>
                                        </div> -->
                                    </div>
                                    <div class="flex-grow-1 col-6 paid-section kt-padding-20 text-center">
                                        <h3 class="kt-font-primary">Paid</h3>
                                        @if(!empty($split_Query['data_extra_field']['org_search_ad']))
                                            @php
                                                $data_add = @unserialize($split_Query['data_extra_field']['org_search_ad']);
                                            @endphp
                                            @if($data_add)
                                                @foreach($data_add['top_keywords'] as $okey => $ova)
                                                    <div class="kt-padding-20 position-relative">
                                                        <span class="keyword-item">{{@$ova['keyword']}}<br>
                                                            @if(!empty($ova['percent']))
                                                                <strong>{{$ova['percent']}}%</strong>
                                                            @else 
                                                                <strong>{{'0'}}%</strong>
                                                            @endif
                                                        </span>
                                                    </div>
                                                @endforeach
                                            @endif
                                        @endif
                                        
                                        <!-- <div class="kt-padding-20 position-relative">
                                            <span class="keyword-item">trello<br><strong>0.15%</strong></span>
                                        </div>
                                        <div class="kt-padding-20 position-relative">
                                            <span class="keyword-item">asana pricing<br><strong>0.12%</strong></span>
                                        </div>
                                        <div class="kt-padding-20 position-relative">
                                            <span class="keyword-item">gantt chart<br><strong>0.11%</strong></span>
                                        </div>
                                        <div class="kt-padding-20 position-relative">
                                            <span class="keyword-item">to do list<br><strong>0.07%</strong></span>
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="kt_portlet_base_demo_1_4_tab_content" role="tabpanel">
                        <div class="kt-widget14">
                            <div class="kt-widget14__header text-center">
                                <h3 class="kt-widget14__title">Traffic by Referrals</h3>
                            </div>
                            <div class="kt-widget14__content justify-content-center">
                                <div class="kt-widget14__chart">
                                    <div id="referralchart" style="height: 200px; width: 200px;"></div>
                                </div>
                            </div>
                        </div>
                        <div class="kt-portlet__body">
                            <div class="kt-scroll ps ps--active-y" data-scroll="true" data-height="380" data-mobile-height="300" style="height: 300px; overflow: hidden;">
                                <div class="d-flex">
                                    <div class="flex-grow-1 col-6 organic-section kt-padding-20">
                                        <h3 class="kt-font-primary">Organic</h3>
                                        @if(!empty($split_Query['data_extra_field']['org_referral']))
                                            @php
                                                $data_add = @unserialize($split_Query['data_extra_field']['org_referral']);
                                            @endphp
                                            @if(!empty($data_add['top_referrals']))
                                                @foreach($data_add['top_referrals'] as $okey => $ova)
                                                    <div class="kt-padding-20 position-relative">
                                                        <span class="keyword-item">{{@$ova['keyword']}}
                                                            @if(!empty($ova['percent']))
                                                                <strong>{{$ova['percent']}}%</strong>
                                                            @else 
                                                                <strong>{{'0'}}%</strong>
                                                            @endif
                                                        </span>
                                                    </div>
                                                @endforeach
                                             @else
                                                <div class="kt-padding-20 position-relative">
                                                    <span class="keyword-item">Not Available
                                                        
                                                    </span>
                                                </div>
                                                
                                            @endif
                                        @endif
                                       

                                    </div>
                                    <div class="flex-grow-1 col-6 paid-section kt-padding-20 text-center">
                                        <h3 class="kt-font-primary">Paid</h3>
                                       @if(!empty($split_Query['data_extra_field']['org_referral_ad']))
                                            @php
                                                $data_add = @unserialize($split_Query['data_extra_field']['org_referral_ad']);
                                            @endphp
                                            @if(!empty($data_add['top_referrals']))
                                                @foreach($data_add['top_referrals'] as $okey => $ova)
                                                    <div class="kt-padding-20 position-relative">
                                                        <span class="keyword-item">{{@$ova['keyword']}}
                                                            @if(!empty($ova['percent']))
                                                                <strong>{{$ova['percent']}}%</strong>
                                                            @else 
                                                                <strong>{{'0'}}%</strong>
                                                            @endif
                                                        </span>
                                                    </div>
                                                @endforeach
                                            @else
                                                <div class="kt-padding-20 position-relative">
                                                    <span class="keyword-item">Not Available
                                                        
                                                    </span>
                                                </div>
                                            @endif
                                        @endif
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="tab-pane" id="kt_portlet_base_demo_1_5_tab_content" role="tabpanel">
                        <div class="kt-widget14">
                            <div class="kt-widget14__content">
                                <div class="kt-widget14__chart flex-grow-1 d-flex">
                                    <div id="socialchart" style="height: 200px; width: 200px;margin: auto;"></div>
                                </div>
                                <div class="kt-widget14__legends">
                                        @php
                                            $arrayName = array('Facebook' => 'kt-bg-facebook', 'Youtube' => 'kt-bg-youtube', 'WhatsApp' => 'kt-bg-whatsapp', 'Twitter' => 'kt-bg-twitter', 'Linkedin' => 'kt-bg-linkedin', 'Pinterest' => 'kt-bg-pinterest');
                                            $count = 0;
                                            $array_color = [];
                                            
                                        @endphp
                                        @if(!empty($array['org_social']))
                                            @foreach($array['org_social'] as $h => $hv)
                                                @php
                                                    if($count == 5):
                                                        $count = 0;
                                                    endif;
                                                    $array_color[$h] = $hv['label'];
                                                @endphp
                                                
                                                <div class="kt-widget14__legend">
                                                    <span class="kt-widget14__bullet {{@$arrayName[$hv['label']]}}"></span>
                                                    <span class="kt-widget14__stats">{{$hv['label']}}</span>
                                                </div>
                                                @php
                                                    $count++;
                                                @endphp
                                            @endforeach
                                        @endif
                                    <!-- <div class="kt-widget14__legend">
                                        <span class="kt-widget14__bullet kt-bg-facebook"></span>
                                        <span class="kt-widget14__stats">Facebook</span>
                                    </div>
                                    <div class="kt-widget14__legend">
                                        <span class="kt-widget14__bullet kt-bg-youtube"></span>
                                        <span class="kt-widget14__stats">Youtube</span>
                                    </div>
                                    <div class="kt-widget14__legend">
                                        <span class="kt-widget14__bullet kt-bg-whatsapp"></span>
                                        <span class="kt-widget14__stats">WhatsApp</span>
                                    </div>
                                    <div class="kt-widget14__legend">
                                        <span class="kt-widget14__bullet kt-bg-twitter"></span>
                                        <span class="kt-widget14__stats">Twitter</span>
                                    </div>
                                    <div class="kt-widget14__legend">
                                        <span class="kt-widget14__bullet kt-bg-linkedin"></span>
                                        <span class="kt-widget14__stats">Linkedin</span>
                                    </div> -->
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    
                </div>
            </div>
        </div>
        <!-- single website details end-->
        <?php break;?>
    @endif
@endforeach