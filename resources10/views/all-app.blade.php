@extends('layout.app')
@php
$totalTitle = 'Home';
@endphp
@section('title', $totalTitle)
@section('title-description')

<meta name="description" content="Home">

<style type="text/css">

.smshub-contentBox-colInner {
    padding: 45px 25px 40px;
    border-radius: 10px;
}
.smshub-contentBox-icon {
    margin-bottom: 35px;
}
.smshub-contentBox-iconInner img {
    height: 115px;
}



</style>
    
@endsection
@section('content')
@section('head')
@parent    
@show
<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor bg-white pt-5" id="kt_content">

        <!-- begin:: Content -->
        <div class="kt-container  kt-grid__item kt-grid__item--fluid home-container">
           
            <div class="row">
                <!-- body content start -->
                <!-- <div class="col-12">
                    <div class="kt_blog-post pb-5">
                        
                        <div class="row justify-content-center">
                            <div class="col-xl-8 col-lg-8 col-md-12">
                                
                                @if (session('success'))
                                <div class="alert alert-success">
                                    {{ session('success') }}
                                </div>
                                @endif
                                @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif

                                <div class="row">
                                    <div class="col">
                                        <div class="card">
                                            <div class="card-header">
                                                
                                            </div>
                                            <div class="card-body">
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>                                  
                    </div>
                </div> -->


<div class="col-12">
    <section class="smshub-contentBox pt-5">
          <div class="container-fluid">
            <div class="row">
              <div class="col-12">
                <div class="smshub-contentBox-head text-center pb-5">
                  <h2>Our Apps</h2>
                  <p>From right within your HubSpot you can send and receive text messages on the go with All-In-One Sms App.<br>Synce your Whmcs Data with WHMCS app.<br>Shop as you like with E-commerce app.</p>

                </div>
              </div>
            </div>
            <div class="row smshub-contentBox-row">
              <div class="col-xl-4 col-lg-6">
                <div class="card card-custom smshub-contentBox-col kt-portlet kt-iconbox kt-icon-size kt-iconbox--primary kt-iconbox--animate-slow">
                  <div class="smshub-contentBox-colInner text-center">
                    <div class="smshub-contentBox-icon">
                      <div class="smshub-contentBox-iconInner">
                        <img src="/hubspot/public/assets/media/misc/HubSpot All in One SMS App.png" alt="icon1">
                      </div>
                    </div>
                    <div class="smshub-contentBox-content">
                      <h3>All in One SMS</h3>
                      <p>Send and receive messages from any phone number in the US or Canada. Even if the person using the phone isn't! 
                       Pay only <b>$19</b> to  use the app. </p>
                      @if(Auth::check())
                        <?php
                        //check if the app is installed for the same portal by this user or not 
                        $portal_id =\DB::table('users')->where('id',Auth::user()->id)->value('portal_id');
                        $id = \DB::table('payments')->where('user_id',Auth::user()->id)->where('app_id',1)->where('portal_id',$portal_id)->value('id');
                        ?>
                        @if($id < 1)
                         
                          <form role="form" action="{{url('/stripe-payment')}}" method="post"  data-cc-on-file="false" data-stripe-publishable-key="{{env('STRIPE_KEY')}}" id="payment-form">
                            @csrf
                            <input type="hidden" name="email" value="{{Auth::user()->email}}">
                            <input type="hidden" name="type" value="smsdew">
                            <input type="hidden" name="app_id" value="1">
                            <noscript>You must <a href="http://www.enable-javascript.com" target="_blank">enable JavaScript</a> in your web browser in order to pay via Stripe.</noscript>
                                <input 
                                    type="submit"
                                    class="btn btn-brand btn-bold kt-mr-10" 
                                    value="Pay $19"
                                    data-key="{{env('STRIPE_KEY')}}"
                                    data-amount="1900"
                                    data-currency="usd"
                                    data-name="stripe Payment"
                                    data-email="{{Auth::user()->email}}"
                                    id="stripe"
                                />
                                <script src="https://checkout.stripe.com/v2/checkout.js"></script>
                                <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
                                <script>
                                    $(document).ready(function() {
                                        $(document).on('click','#stripe' ,function(event) {
                                            event.preventDefault();


                                            var $button = $(this),
                                                $form = $button.parents('form');


                                            var opts = $.extend({}, $button.data(), {
                                                token: function(result) {
                                                $form.append($('<input>').attr({ type: 'hidden', name: 'stripeToken', value: result.id })).submit();
                                                }
                                            });


                                            StripeCheckout.open(opts);
                                        });
                                    });
                                </script>
                          </form>
                        @else
                        <!--                            <a href="{{env('APP_URL').'/smsdew/auth2-callback'}}" class="btn btn-brand btn-bold kt-mr-10" >Install</a>-->
                        <button class="btn btn-brand btn-bold kt-mr-10">Installed</button>
                        @endif
                      @endif
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-xl-4 col-lg-6">
                <div class="card card-custom smshub-contentBox-col kt-portlet kt-iconbox kt-icon-size kt-iconbox--primary kt-iconbox--animate-slow">
                  <div class="smshub-contentBox-colInner text-center">
                    <div class="smshub-contentBox-icon">
                      <div class="smshub-contentBox-iconInner">
                        <!-- <img src="/hubspot/public/assets/media/misc/HubSpot All in One SMS App.png" alt="icon1"> -->
                        <img src="/hubspot/public/assets/media/misc/376_Whmcs_logo-512.png" alt="image" >
                      </div>
                    </div>
                    <div class="smshub-contentBox-content">
                      <h3>WHMCS</h3>
                        <p>Sync your Whmcs <b>Products,Contacts and Orders</b> to your Hubspot account with just one click!</p>
                      <!-- <form action="javascript:void(0);">
                          <input 
                                type="button"
                                class="btn btn-brand btn-bold kt-mr-10" 
                                value="Coming soon."
                            />
                      </form> -->
                      <?php
                        $portal_id = \Session::get('portal_id');
                        $exi = DB::table('application_records')->where('portal_id',$portal_id)->where('app_id', 2)->value('id');
                      ?>
                      @if($exi < 1)
                      <a href="{{env('APP_URL').'whmcs/auth2-callback'}}" class="btn btn-brand btn-bold kt-mr-10">Install App</a>
                      @else 
                      <a href="{{env('APP_URL').'apps/whmcs/automation-pricing'}}" class="btn btn-brand btn-bold kt-mr-10">Automation</a>
                      @endif
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-xl-4 col-lg-6">
                <div class="card card-custom smshub-contentBox-col kt-portlet kt-iconbox kt-icon-size kt-iconbox--primary kt-iconbox--animate-slow">
                  <div class="smshub-contentBox-colInner text-center">
                    <div class="smshub-contentBox-icon">
                      <div class="smshub-contentBox-iconInner">
                        <img src="/hubspot/public/assets/media/misc/HubSpot All in One SMS App.png" alt="icon1">
                      </div>
                    </div>
                    <div class="smshub-contentBox-content">
                      <h3>E-Commerce</h3>
                      <p>Create your own checkout button and shop anything with this amazing app.</p>
                      <?php
                        $portal_id = \Session::get('portal_id');
                        $exi = DB::table('application_records')->where('portal_id',$portal_id)->where('app_id', 3)->value('id');
                      ?>
                      @if($exi < 1)
                      <a href="{{env('APP_URL').'ecommerce/auth2-callback'}}" class="btn btn-brand btn-bold kt-mr-10">Install App</a>
                      @else 
                      <a href="{{env('APP_URL').'apps/ecommerce/settings'}}" class="btn btn-brand btn-bold kt-mr-10">Installed</a>
                      @endif
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
    </div>






                <!-- body content end -->
            </div>
        </div>
    </div>                  
</div>

@section('scripts')
@parent
@show
@endsection