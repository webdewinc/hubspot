<!--begin::Fonts -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700">
<!--end::Fonts -->

<!--begin::Global Theme Styles(used by all pages) -->
<link href="{{ asset('') . config('app.public_url') . '/assets/plugins/global/plugins.bundle.css' }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('') . config('app.public_url') . '/assets/css/style.bundle.css' }}" rel="stylesheet" type="text/css" />
<!--end::Global Theme Styles -->
<!--begin::Layout Skins(used by all pages) -->
<!--end::Layout Skins -->

<!-- <link rel="shortcut icon" href="{{ asset('') . config('app.public_url') . '/images/favicon.ico' }}" /> -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <!-- loader -->
<script type="text/javascript">
    // $(window).load(function() {
    //     $(".loader").fadeOut("slow");
    // });
</script>

<style type="text/css">
	
/*28-12-20*/    
#kt_content{
  background: #fff;
      padding-bottom: 80px;
}
.pannel-image .whmcs-logo{
    height: 100%;
    display: flex;
    align-items: center;
    justify-content: center;
    background: #f8f8f8;
}
.setting-page .kt_blog-post{
    margin: 0 auto;
    padding:0 15px;
    width: 1024px;
    max-width: 100%;
}
.setting-page .card{
  border: 0;
}
.setting-page .card-header{
    background: transparent;
    padding-left: 0;
    padding-right: 0;
    padding-bottom: 30px;
}
.setting-page .card-header h2 label{
    font-size: 30px;
    margin-bottom: 30px;
  }
.setting-page .card-header a{
  font-size: 16px;
}
.setting-page .card-body{
    padding-left: 0;
    padding-right: 0;
}
.setting-page .form-row .col-md-6{
    flex: 0 0 100%;
    max-width: 100%;
}
.setting-page .card-body .form-control{
  background: #fff;
}
.import-page .card-body .container{
  max-width: 100% !important;
  padding-left: 20px;
  padding-right: 20px;
}
.import-page .card-body #stepProgressBar {
    position: relative;
    width: 100% !important;
}
.import-page .card-body #stepProgressBar::before {
    position: absolute;
    content: '';
    width: 95%;
    height: 2px;
    background: #f5f8fa;
    top: 75%;
    transform: translateY(-50%);
    left: 0;
}
.import-page .card-body .step-text {
    font-size: 20px;
    font-weight: 500;
}
.import-page .card-body .bullet {
    height: 40px;
    width: 40px;
    line-height: 20px;
    font-size: 24px;
    display: flex;
    align-items: center;
    justify-content: center;
    background: #fff;
    border-color: #1dc9b7 !important;
}
.import-page .card-body .bullet.completed {
    background-color: #1dc9b7 !important;
}

.bullet.completed::before {
    content: none !important;
}
.bullet.completed::after {
    content: '';
    position: absolute;
    right: -225px !important;
    top: 50%;
    height: 1px !important;
    width: 600% !important;
    background-color: #1dc9b7 !important;
    transform: translateY(-50%);
}
.import-page .card-body #stepProgressBar .step:first-child .bullet.completed:before,
.import-page .card-body #stepProgressBar .step:last-child .bullet.completed:after{
    content: none !important;
}
.automate-page .card-body p{
  color: #1dc9b7;
}
.automate-page .card-body h2{
      font-size: 20px;
    font-weight: 500;
}
.automate-page .card-body h2 span {
    background: #1dc9b7;
    color: #fff;
    border-radius: 3px;
    padding: 5px;
}
.automate-page .card-body .card-body h3{
    margin-bottom: 15px;
    margin-top: 30px;
        font-size: 18px;
    font-weight: 500;
}
.automate-page .card-body h4 label {
    font-size: 16px;
    margin-bottom: 0px;
}
.automate-page .card-body label{
  font-size: 16px;
  margin-bottom: 15px;
}


[type="radio"]:checked,
[type="radio"]:not(:checked) {
    position: absolute;
    left: -9999px;
}
[type="radio"]:checked + label,
[type="radio"]:not(:checked) + label
{
    position: relative;
    padding-left: 28px;
    cursor: pointer;
    line-height: 20px;
    display: inline-block;
    color: #666;
}
[type="radio"]:checked + label:before,
[type="radio"]:not(:checked) + label:before {
    content: '';
    position: absolute;
    left: 0;
    top: 0;
    width: 18px;
    height: 18px;
    border: 1px solid #ddd;
    border-radius: 100%;
    background: #fff;
}
[type="radio"]:checked + label:after,
[type="radio"]:not(:checked) + label:after {
    content: '';
    width: 12px;
    height: 12px;
    background: #1dc9b7;
    position: absolute;
    top: 3px;
    left: 3px;
    border-radius: 100%;
    -webkit-transition: all 0.2s ease;
    transition: all 0.2s ease;
}
[type="radio"]:not(:checked) + label:after {
    opacity: 0;
    -webkit-transform: scale(0);
    transform: scale(0);
}
[type="radio"]:checked + label:after {
    opacity: 1;
    -webkit-transform: scale(1);
    transform: scale(1);
}

.card-body button.btn{
  width: 180px;
}

.pipline-page form select {
    word-wrap: normal;
    width: 100%;
    margin: 10px 0 20px 0;
    padding: 8px;
    border-color: #ccc; 
    border-radius: 6px
}

.pipline-page form select:focus {
  border-color: #0091ae;
  outline: none;
}

.pipline-page form .invalid-feedback{
  margin-top: -15px;
  position: absolute;
}

@media(max-width: 767px){
  .pannel-image .whmcs-logo {
    height: 200px;
}
  .setting-page .card-header {
    margin-top: 50px;
  }
  .pannel-login{
    order: 2;
  }
  .pannel-image{
    order: 1
  }
}
.swal2-styled.swal2-confirm{
  color : #fff !important;
}
</style>