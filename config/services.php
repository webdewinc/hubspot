<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, SparkPost and others. This file provides a sane default
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stage_login' => [
        'clientId'          => '69f9985e-7d73-4550-a68a-c85f840b0ac2',
        'clientSecret'      => '3ad14c53-5e8d-4248-9c38-bc1873509922',
    ],
    'login' => [
        'clientId'          => '40a31f75-4c61-4eef-997f-aff176f37d09',
        'clientSecret'      => '22b71210-54e9-4ddf-bfad-8be3ac384fff',
        // 'clientId'          => '69f9985e-7d73-4550-a68a-c85f840b0ac2',
        // 'clientSecret'      => '3ad14c53-5e8d-4248-9c38-bc1873509922',
    ],
    'stage_smsdew' => [
        'clientId'          => '51b676ca-2179-4565-a354-2e51052da948',
        'clientSecret'      => 'b2dfbaf6-0f9f-4d2b-b2a7-86f5a1f75b30'
    ],
    // 'stage_smsdew' => [
    //     'clientId'          => '51b676ca-2179-4565-a354-2e51052da948',
    //     'clientSecret'      => 'b2dfbaf6-0f9f-4d2b-b2a7-86f5a1f75b30'
    // ],
    'stage_whmcs' => [
        'clientId'          => '91b45883-e8b9-447d-a7be-cef72e39d89b',
        'clientSecret'      => '02eef7c8-4c6d-4904-a68b-cec58c8c9a35'
    ],
    'stage_ecommerceCartButton' => [
        'clientId'          => '0ed03bb8-dc81-43ba-9529-2ce3d43a84a4',
        'clientSecret'      => '630b21e5-e117-4095-94af-83cb94e725a3'
    ],
    'stage_flock' => [
        'clientId'          => '1086e92a-64a1-4d2f-98dd-74053a617697',
        'clientSecret'      => '39061cf2-c3ef-4179-a10a-2f4ac841ef0c'
    ],
    'live_smsdew' => [
        // 'clientId'          => 'd52f5464-d029-4391-b16e-13fa929a5406',
        // 'clientSecret'      => 'b53771b8-794b-4046-a867-736c9c9a6d19'
        'clientId'          => '27045765-d704-4ca3-b3a5-2a936f4ab4c0',
        'clientSecret'      => '18b262aa-01a7-43d4-8ba4-11ac1662cddb'
    ],

    'live_whmcs' => [
        'clientId'          => '69e5f3e5-1cfb-4683-8aea-cc833d955837',
        'clientSecret'      => '1abcf255-dc2c-46e3-9f76-5c208db81238'
    ],
    'live_ecommerceCartButton' => [
        'clientId'          => 'ffd134a7-2027-400b-a85f-d7223e6ccde0',
        'clientSecret'      => 'e70dfdda-2aa3-4d9a-9ea7-65f50d08e207'
    ],
    'live_flock' => [
        'clientId'          => '1086e92a-64a1-4d2f-98dd-74053a617697',
        'clientSecret'      => '39061cf2-c3ef-4179-a10a-2f4ac841ef0c'
    ],
    'stage_stripe' => [
        'pk'          => 'pk_test_51HIGy1Et32QR8DEin6O2X1MaUaQeRLUmdx49imFUDG2A5sdTZ94Sl7Mqy0cGikPOnxwpWRDjB9pU4oBeQ2VWuE4O002IlB5paY',
        'sk'      => 'sk_test_51HIGy1Et32QR8DEiqTN6PgD4ZdimL9RhR8AWLu1p819MvEiGrh4YmugVMRBd53i6zKzr0L91danoDExZThFKzaKx00ePcQNTDI'
    ],
    'live_stripe' => [
        'pk'          => 'pk_live_51HIGy1Et32QR8DEiqHLUJkoooDvREEx2pCmxY2N6D4IoBooH07VI7npE1tfrSObZjOXpG2trG3dmV8hrOIgFJHv600L2ZZRNuK',
        'sk'      => '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'
    ],
    'stage_hapikey' => [
        'hapikey'          => '58723188-d3ff-473b-b9b0-24c927e10c10',
        'subscription_key'      => '2-1170154'
    ],
    'live_hapikey' => [
        'hapikey'          => '58723188-d3ff-473b-b9b0-24c927e10c10',
        'subscription_key'      => '2-1170154'
    ]
];
