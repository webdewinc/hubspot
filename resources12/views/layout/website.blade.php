<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>

    <title>@yield('title')</title>
     
    <!-- begin::Head -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!--begin::Page Vendors Styles(used by this page) -->
    
    @yield('title-description')

    @section('head')
        @include('layout.include.website.head')
    @show
    <!-- end::Head -->
    

    @yield('customCSS')
    

</head>
    <?php 
        if(Auth::check()){
            $class =  'publisher-wrapper ';
        } else {
            $class = '';
        }
    ?>
    <body class="{{$class}}kt-page--loading-enabled kt-page--loading kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header--minimize-menu kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-aside--enabled kt-aside--left kt-aside--fixed kt-page--loading">

        @include('layout.include.website.header')

        @yield('content')

        <!-- end:: Page -->

        <!-- begin::Scrolltop -->
        <div id="kt_scrolltop" class="kt-scrolltop">
            <i class="fa fa-arrow-up"></i>
        </div>
        @section('scripts')
            @include('layout.include.website.scripts')
        @show

        @yield('customScript')
        <!-- end::Scrolltop -->
        @include('layout.include.website.footer')

