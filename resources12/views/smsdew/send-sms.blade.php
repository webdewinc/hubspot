@extends('layout.app')
@php
$totalTitle = 'Send SMS';
@endphp
@section('title', $totalTitle)
@section('title-description')
<meta name="description" content="Send SMS">
@endsection
@section('content')
@section('head')
@parent    
@show
<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor bg-white" id="kt_content">

        <!-- begin:: Content -->
        <div class="kt-container  kt-grid__item kt-grid__item--fluid home-container">
           
            <div class="row">
                <!-- body content start -->
                <div class="col-12">
                    <div class="kt_blog-post">
                        
                        <div class="row justify-content-center">
                            <div class="col-xl-8 col-lg-8 col-md-12">
                                
                                    @if(!empty($alert))
                                    <div class="alert alert-{{$alert}}">
                                        {{ $message }}
                                    </div>
                                    @endif
                                    @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    @endif

                                    <div class="row">
                                        <div class="col">
                                            <div class="card">
                                                <div class="card-header">
                                                    <a>Send Text SMS message</a>
                                                    @php 
                                                        $response = @unserialize(Auth::user()->object_data);
                                                        $vendor = vendor_select(Auth::user()->sms_credentials_default_type);
                                                    @endphp
                                                    <span class="float-right">
                                                        <a href="{{url('smsdew/settings-hub').'/'.$response['auth_id']}}"><i class="fa fa-cog" aria-hidden="true"></i> Settings</a>
                                                    </span>
                                                    
                                                </div>
                                                <div class="card-body form-group mb-0">
                                                    <label>Default Vendor: {{$vendor['name']}}</label>
                                                    <form method="POST" action="{{url('smsdew/send-sms')}}">
                                                        @csrf
                                                        <input type="text" class="form-control form-group" disabled="" value="{{@$response['phone']}}">
                                                        <input type="hidden" class="form-control" name="phone[]" placeholder="98XXXXXXXX" maxlength="14" minlength="10" required="" value="{{@$response['phone']}}">
                                                        <input type="hidden" name="auth_id" value="{{Auth::user()->id}}">
                                                        <div class="form-group">
                                                            <label>Message</label>
                                                            <textarea name="body" class="form-control" placeholder="Enter the text message" rows="3" required=""></textarea>
                                                        </div>
                                                        <button type="submit" class="btn btn-label-brand btn-bold">Send Notification</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                        </div>                                  
                    </div>
                </div>
                <!-- body content end -->
            </div>
        </div>
    </div>                  
</div>

@section('scripts')
@parent
@show
@endsection