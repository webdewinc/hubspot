@extends('layout.app')
@php
$totalTitle = 'Product List';
@endphp
@section('title', $totalTitle)
@section('title-description')

<meta name="description" content="Product List">
    
@endsection
@section('content')
@section('head')
@parent    
@show
<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor bg-white mt-5" id="kt_content">

        <!-- begin:: Content -->
        <div class="kt-container  kt-grid__item kt-grid__item--fluid home-container">
           
            <div class="row">
                <!-- body content start -->
                <div class="col-12">
                    <div class="kt_blog-post  pb-5">
                        
                        <div class="row justify-content-center">
                            <div class="col-xl-8 col-lg-8 col-md-12">
                                <span id="alert-span"></span>
                                @if (!empty($alert))
                                <div class="alert alert-{{$alert}}">
                                    {{ $message }}
                                </div>
                                @endif
                                @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif

                                <div class="row">
                                    <div class="col">
                                        <div class="card">
                                            @foreach($data as $key => $v)
                                            <h5>{{$v['properties']['name']}}</h5>
                                            <h6>Rs. {{$v['properties']['price']}}</h6>
                                            <p>{{$v['properties']['description']}}</p>
                                            <button class="button-show-code" rel="{{$v['id']}}">Product Code</button>
                                            <code id="code-show-{{$v['id']}}" style="display:none;">
                                                {{$v['html']}}
                                            </code>
                                            @endforeach
                                            @if(!empty($next))
                                                <a href="{{$next}}">Next</a>    
                                            @endif                                            
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>                                  
                    </div>
                </div>
                <!-- body content end -->
            </div>
        </div>
    </div>                  
</div>

@section('scripts')
@parent

  <!--begin::Page Scripts(used by this page) -->
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
        <script type="text/javascript">
            "use strict";

            // Class definition
            var KTWizard2 = function () {
                // Base elements
                var wizardEl;
                var formEl;
                var validator;
                var wizard;

                var initValidation = function() {
                    validator = formEl.validate({
                        // Validate only visible fields
                        ignore: ":hidden",

                        // Validation rules
                        rules: {
                           
                            // routes :{
                            //     required: true,
                            //     maxlength: 3,
                            //     digits:true
                            // },
                            // country_code :{
                            //     required: true,
                            //     maxlength: 3,
                            //     digits:true
                            // },
                            // sender :{
                            //     required: true,
                            //     maxlength: 10,
                            // },
                            // MSG91 :{
                            //     required: true,
                            // }
                        },

                        // Display error
                        invalidHandler: function(event, validator) {
                            KTUtil.scrollTop();
                            swal.fire({
                                "title": "",
                                "text": "There are some errors in your submission. Please correct them.",
                                "type": "error",
                                "confirmButtonClass": "btn btn-secondary"
                            });
                        },

                        // Submit valid form
                        // submitHandler: function (form) {

                        // }
                    });
                }

                var initSubmit = function() {
                    var btn = formEl.find('[data-ktwizard-type="action-submit"]');

                    btn.on('click', function(e) {
                        e.preventDefault();

                        if (validator.form()) {
                            $('#kt_form').submit();     
                        }
                    });
                }

                return {
                    // public functions
                    init: function() {
                        wizardEl = KTUtil.get('kt_wizard_v2');
                        formEl = $('#kt_form');
                        initValidation();
                        initSubmit();
                    }
                };
            }();

            jQuery(document).ready(function() {
                KTWizard2.init();
                /** validation for guest example not to match with domain url **/
                jQuery.validator.addMethod("notEqual", function(value, element, param) {
                  return this.optional(element) || value != param;
                }, "Please specify a different value");
                /** end here code **/
                $.validator.addMethod('lessThanEqual', function(value, element, param) {
                    return this.optional(element) || parseInt(value) <= parseInt($(param).val());
                }, "The content cost must be less than number words");
            });

        jQuery(document).on('click','.button-show-code',function(e){
            var rel = $(this).attr('rel');
            $('code').hide();
            $('#code-show-'+rel).show();
        });

        // document.getElementById("copyButton").addEventListener("click", function() {
        //     copyToClipboard(document.getElementById("copyTarget"));
        // });

        // function copyToClipboard(elem) {
        //     // create hidden text element, if it doesn't already exist
        //     var targetId = "_hiddenCopyText_";
        //     var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA" || elem.tagName === "code";
        //     var origSelectionStart, origSelectionEnd;
        //     if (isInput) {
        //         // can just use the original source element for the selection and copy
        //         target = elem;
        //         origSelectionStart = elem.selectionStart;
        //         origSelectionEnd = elem.selectionEnd;
        //     } else {
        //         // must use a temporary form element for the selection and copy
        //         target = document.getElementById(targetId);
        //         if (!target) {
        //             var target = document.createElement("textarea");
        //             target.style.position = "absolute";
        //             target.style.left = "-9999px";
        //             target.style.top = "0";
        //             target.id = targetId;
        //             document.body.appendChild(target);
        //         }
        //         target.textContent = elem.textContent;
        //     }
        //     // select the content
        //     var currentFocus = document.activeElement;
        //     target.focus();
        //     target.setSelectionRange(0, target.value.length);
            
        //     // copy the selection
        //     var succeed;
        //     try {
        //         succeed = document.execCommand("copy");
        //     } catch(e) {
        //         succeed = false;
        //     }
        //     // restore original focus
        //     if (currentFocus && typeof currentFocus.focus === "function") {
        //         currentFocus.focus();
        //     }
            
        //     if (isInput) {
        //         // restore prior selection
        //         elem.setSelectionRange(origSelectionStart, origSelectionEnd);
        //     } else {
        //         // clear temporary content
        //         target.textContent = "";
        //     }
        //     return succeed;
        // }
        </script>     
@show
@endsection