@extends('layout.app')
@php
$totalTitle = 'Settings';
@endphp
@section('title', $totalTitle)
@section('title-description')
<meta name="description" content="Settings">
<style type="text/css">

.smshub-contentBox-colInner {
    padding: 45px 25px 40px;
    border-radius: 10px;
}
.smshub-contentBox-icon {
    margin-bottom: 35px;
}
.smshub-contentBox-iconInner img {
    height: 115px;
}



</style>
@endsection
@section('content')
@section('head')
@parent    
@show
<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor bg-white mt-5" id="kt_content">

        <!-- begin:: Content -->
        <div class="kt-container  kt-grid__item kt-grid__item--fluid home-container">
           
            <div class="row">
                <!-- body content start -->
                <div class="col-12">
                    <div class="kt_blog-post">
                        
                        <div class="row justify-content-center">
                            <?php 
                            $payment_id=0;
                            $payment_id = DB::table('ecommerce_payments')->where('portal_id',Session::get('portal_id'))->value('id');

                              // $payment_id = DB::table('ecommerce_payments')->where('user_id',Auth::user()->id)->where('portal_id',Session::get('portal_id'))->value('id');

                            ?>
                            @if($payment_id > 0)
                                <div class="col-xl-8 col-lg-8 col-md-12">
                                    
                                    @if (!empty($alert))
                                    <div class="alert alert-{{$alert}}">
                                        {{ $message }}
                                    </div>
                                    @endif
                                    @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    @endif

                                    <div class="row">
                                        <div class="col">
                                            <div class="card">
                                                <form id="kt_form" name="kt_form" action="{{url('apps/ecommerce/settings')}}" method="POST">
                                                    <div class="card-header">
                                                        <label>Settings</label>
                                                        <?php
                                                            if(!empty(DB::table('credentials')->where('type','ecommerce')->wherePortalId(Session::get('portal_id'))->value('data'))) {
                                                        ?>
                                                            <div class="float-right">
                                                                <a href="{{url('apps/ecommerce/sync-data')}}">Sync Data</a>
                                                            </div>
                                                        <?php
                                                            }
                                                        ?>
                                                    </div>
                                                    <div class="card-body" id="vendor-append">
                                                        @php
                                                            echo $html;
                                                        @endphp
                                                    </div>
                                                    <div class="float-right">
                                                                <a href="#myModal"  data-toggle="modal" >more info</a>
                                                            </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- docs -->

                                    <!-- Trigger the modal with a button -->
<!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button> -->

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        
        <h4 class="modal-title">Settings Info</h4>
      </div>
      <div class="modal-body">
       <!-- <iframe src="https://www.webdew.com/kb/hubspot/steps-to-install-e-commerce-app" height="600px" width="400px" ></iframe> -->
       <p><b>Stripe PK and Stripe SK : </b> These Fields are used to set stripe payment gateway in your app. Add primary key and secret key in these fields provided by users stripe account.</br>

<b>Paypal  : </b>This Field is used to set PayPal payment gateway in your app. Add the API key for PayPal account in this field for your eCommerce app.</br>

<b>Hapi Key : </b>This Field is used to sync the products available in this account to create a product image field. Fetch your API key from the HubSpot portal and paste it here.</br>

<b>Logo link : </b>This Field is used to set your eCommerce app logo in the header. You can use your company logo for an eCommerce app by uploading the logo image in files in your HubSpot account then copy that image public link and paste it here.</p>
</br>
<div class="float-right">
                                                                <a href="https://www.webdew.com/kb/hubspot/steps-to-install-e-commerce-app" target="_blank"   >full documentation</a>
                                                            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

                                    <!-- docs -->
                                </div>
                            @else

                            <!-- ecommerce - card  -->

<div class="col-12">
    <section class="smshub-contentBox pt-5">
        <div class="container-fluid">
        <div class="row smshub-contentBox-row">
                      <div class="col-xl-4 col-lg-6">
             
              </div>
              <div class="col-xl-4 col-lg-6">
                <div class="card card-custom smshub-contentBox-col kt-portlet kt-iconbox kt-icon-size kt-iconbox--primary kt-iconbox--animate-slow">
                  <div class="smshub-contentBox-colInner text-center">
                    <div class="smshub-contentBox-icon">
                      <div class="smshub-contentBox-iconInner">
                         <img src="/hubspot/public/assets/media/misc/e-commerce.svg" alt="icon1">
                      </div>
                    </div>
                    <div class="smshub-contentBox-content">
                      <h3>Ecommerce App</h3>
                      <p>One more step away to use Exciting Features of our App</p> 
                       Pay only <b>$29</b> to  use the app. </p>
                   
                         
                            <form role="form" action="{{url('/ecommerce-stripe-payment')}}" method="post"  data-cc-on-file="false" data-stripe-publishable-key="pk_test_51HIGy1Et32QR8DEin6O2X1MaUaQeRLUmdx49imFUDG2A5sdTZ94Sl7Mqy0cGikPOnxwpWRDjB9pU4oBeQ2VWuE4O002IlB5paY" id="payment-form">
                                    
                                    <label for="test1" hidden> Basic : $29 </label>
                                    <br>
                                    <br>
                                    <span>
                                        @csrf
                                        <input type="hidden" name="email" value="{{Auth::user()->email}}">
                                        <noscript>You must <a href="http://www.enable-javascript.com" target="_blank">enable JavaScript</a> in your web browser in order to pay via Stripe.</noscript>
                                            <input 
                                                type="submit"
                                                class="btn btn-brand btn-bold kt-mr-10 pay" 
                                                value="Pay $29"
                                                data-key="{{$stripe_P}}"
                                                data-amount="2900"
                                                data-payment="daily"
                                                data-currency="usd"
                                                data-name="stripe Payment"
                                                data-email="{{Auth::user()->email}}"
                                                id="daily"
                                            />
                                            <!-- <input 
                                                type="submit"
                                                class="btn btn-brand btn-bold kt-mr-10 pay" 
                                                value="Pay $29"
                                                data-key="pk_test_51HIGy1Et32QR8DEin6O2X1MaUaQeRLUmdx49imFUDG2A5sdTZ94Sl7Mqy0cGikPOnxwpWRDjB9pU4oBeQ2VWuE4O002IlB5paY"
                                                data-amount="2900"
                                                data-payment="daily"
                                                data-currency="usd"
                                                data-name="stripe Payment"
                                                data-email="{{Auth::user()->email}}"
                                                id="daily"
                                            /> -->
                                            <script src="https://checkout.stripe.com/v2/checkout.js"></script>
                                            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
                                            <script>
                                                $(document).ready(function() {
                                                    $(document).on('click','#daily' ,function(event) {
                                                        event.preventDefault();
                                                        var $button = $(this),
                                                            $form = $button.parents('form');

                                                        var opts = $.extend({}, $button.data(), {
                                                            token: function(result) {
                                                            $form.append($('<input>').attr({ type: 'hidden', name: 'stripeToken', value: result.id })).submit();
                                                            }
                                                        });
                                                        StripeCheckout.open(opts);
                                                    });
                                                });
                                            </script>
                                    </span>
                                </form>                                
                    </div>
                  </div>
                </div>
              </div>
          </div>
      </section>
  </div>
</section>
   <div class="col-xl-4 col-lg-6">
             
              </div>
                         <!-- ecommerce - card -->
                            <!-- <form role="form" action="{{url('/ecommerce-stripe-payment')}}" method="post"  data-cc-on-file="false" data-stripe-publishable-key="{{@$stripe_P}}" id="payment-form"> -->
                                                      
                            @endif
                        </div>                                  
                    </div>
                </div>
                <!-- body content end -->
            </div>
        </div>
    </div>                  
</div>

@section('scripts')
@parent

  <!--begin::Page Scripts(used by this page) -->
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
        <script type="text/javascript">
            "use strict";

            // Class definition
            var KTWizard2 = function () {
                // Base elements
                var wizardEl;
                var formEl;
                var validator;
                var wizard;

                var initValidation = function() {
                    validator = formEl.validate({
                        // Validate only visible fields
                        ignore: ":hidden",

                        // Validation rules
                        rules: {
                           
                            // routes :{
                            //     required: true,
                            //     maxlength: 3,
                            //     digits:true
                            // },
                            // country_code :{
                            //     required: true,
                            //     maxlength: 3,
                            //     digits:true
                            // },
                            // sender :{
                            //     required: true,
                            //     maxlength: 10,
                            // },
                            // MSG91 :{
                            //     required: true,
                            // }
                        },

                        // Display error
                        invalidHandler: function(event, validator) {
                            KTUtil.scrollTop();
                            swal.fire({
                                "title": "",
                                "text": "There are some errors in your submission. Please correct them.",
                                "type": "error",
                                "confirmButtonClass": "btn btn-secondary"
                            });
                        },

                        // Submit valid form
                        // submitHandler: function (form) {

                        // }
                    });
                }

                var initSubmit = function() {
                    var btn = formEl.find('[data-ktwizard-type="action-submit"]');

                    btn.on('click', function(e) {
                        e.preventDefault();

                        if (validator.form()) {
                            $('#kt_form').submit();     
                        }
                    });
                }

                return {
                    // public functions
                    init: function() {
                        wizardEl = KTUtil.get('kt_wizard_v2');
                        formEl = $('#kt_form');
                        initValidation();
                        initSubmit();
                    }
                };
            }();

            jQuery(document).ready(function() {
                KTWizard2.init();
                /** validation for guest example not to match with domain url **/
                jQuery.validator.addMethod("notEqual", function(value, element, param) {
                  return this.optional(element) || value != param;
                }, "Please specify a different value");
                /** end here code **/
                $.validator.addMethod('lessThanEqual', function(value, element, param) {
                    return this.optional(element) || parseInt(value) <= parseInt($(param).val());
                }, "The content cost must be less than number words");
            });

            jQuery(document).on('change','#type',function(e){
                    var vendor = $(this).val();
                    e.preventDefault();
                    $.ajax({
                         headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                          },
                           beforeSend: function() {
                                // setting a timeout
                                $(".loader").fadeIn("slow");
                            },
                          url  : "{{ url('/render-vendor') }}",
                          type : 'POST',
                          data: {
                                'vendor' : vendor,
                             },
                          async: true,
                          //dataType: 'json',
                          enctype: 'multipart/form-data',
                          cache: false,                      
                          success: function(response){
                            $('#vendor-append').html(response);
                            $(".loader").fadeOut("slow");
                          },
                          error: function(){}
                        });
                        e.stopImmediatePropagation();
                        return false; 

            });

        </script>     
@show
@endsection