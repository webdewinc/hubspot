@extends('layout.app')
@php
$totalTitle = 'Home';
@endphp
@section('title', $totalTitle)
@section('title-description')

<meta name="description" content="Home">
    
@endsection
@section('content')
@section('head')
@parent    
@show
<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor bg-white pt-5" id="kt_content">

        <!-- begin:: Content -->
        <div class="kt-container  kt-grid__item kt-grid__item--fluid home-container">
           
            <div class="row">
                <!-- body content start -->
                <div class="col-12">
                    <div class="kt_blog-post pb-5">
                        
                        <div class="row justify-content-center">
                            <div class="col-xl-8 col-lg-8 col-md-12">
                                
                                @if (session('success'))
                                <div class="alert alert-success">
                                    {{ session('success') }}
                                </div>
                                @endif
                                @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif

                                <div class="row">
                                    <div class="col">
                                        <div class="card">
                                            <div class="card-header">
                                                <label>Welcome to SMS app</label>
                                                <span class="float-right">
                                                    @php
                                                        $portal_id = @Auth::user()->portal_id;
                                                        $email = @Auth::user()->email;
                                                    @endphp
                                                    <!-- <a class="btn btn-secondary" href="{{url('/contacts/'.$portal_id.'/contacts/list/view/all/')}}">Go to Contacts list</a> -->
                                                    <!-- <a class="btn btn-label-brand btn-bold kt-ml-5" href="{{url('logout')}}">Logout</a> -->
                                                </span>
                                            </div>
                                            <div class="card-body">
                                                Portal Id : {{$portal_id}}
                                                <br>
                                                Email : {{$email}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>                                  
                    </div>
                </div>
                <!-- body content end -->
            </div>
        </div>
    </div>                  
</div>

@section('scripts')
@parent
@show
@endsection