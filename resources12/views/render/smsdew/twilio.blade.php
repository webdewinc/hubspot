<?php
$vendor = vendor_select('twilio');
?>
<p><a href="javascript:void(0);" title="Webdew Groups" ><strong id="select-vendor-append">{{$vendor['name']}}</strong></a>
<a href="https://www.twilio.com/docs/usage/api" target="_blank" hidden style="float:right;">Api Docs</a>
</p>             
@csrf
<div class="kt-wizard-v2__form">
        
    <?php
        if(empty($data)){
            $button = 'Save';
        } else {
            $button = 'Update';
        }
    ?>
    <div class="form-row">
        <div class="form-group col-md-6 col-12">
            <label class="required">{{$vendor['label']['s_id']}}</label>
            <div class="input-group">
                <input class="form-control" type="text" id="s_id" name="s_id" value="{{@$data['s_id']}}" minlength="25" maxlength="50" required="" autocomplete="off">
            </div>
        </div>
		<div class="form-group col-md-6 col-12">
            <label class="required">{{$vendor['label']['s_token']}}</label>
            <div class="input-group">
                <input class="form-control" type="text" id="s_token" name="s_token" value="{{@$data['s_token']}}" minlength="25" maxlength="100" required="" autocomplete="off">
            </div>
        </div>
    </div>
   
    <div class="form-row">
        <div class="form-group col-md-6 col-12">
            <label class="required">{{$vendor['label']['mobile']}}</label>
            <div class="input-group">
                <input class="form-control" type="text" id="mobile" name="mobile" value="{{@$data['mobile']}}" required="" minlength="10" maxlength="14" autocomplete="off">
            </div>
        </div>
          <div class="form-group col-md-6 col-12">
            <label class="required">{{$vendor['label']['t_hapi_key']}}</label>
            <div class="input-group">
                <input class="form-control" type="text" id="t_hapi_key" name="t_hapi_key" value="{{@$data['t_hapi_key']}}" required="" minlength="10"  autocomplete="off">
            </div>
        </div>
	 </div>
		 <div class="form-group col-md-6 col-12">            
            <div class="input-group">
                
				<label class="kt-checkbox">
				   <input type="checkbox" id="default" name="default" value="twilio" @if(@$type == 'twilio'){{'checked'}}@endif> Set as default
					<span></span>
				</label>
            </div>
        </div>
   
   
    <div class="kt-form__actions">
        <button class="btn btn-label-brand btn-bold"  type="submit" data-ktwizard-type="action-submit">
            {{$button}}
        </button>
    </div>
</div>