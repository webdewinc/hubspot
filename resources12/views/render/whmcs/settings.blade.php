
@csrf
<div class="kt-wizard-v2__form">
        
    <?php
        if(empty($data)){
            $button = 'Save';
        } else {
            $button = 'Update';
        }
    ?>
    <div class="form-row">
        <div class="form-group col-md-6 col-12">
            <label class="required">Username or Identifier</label>
            <div class="input-group">
                <input class="form-control" type="text" id="username" name="username" value="{{@$data['username']}}" minlength="3" maxlength="50" required="" autocomplete="off">
            </div>
        </div>
        <div class="form-group col-md-6 col-12">
            <label class="required">Hashed Password or Secret</label>
            <div class="input-group">
            <input class="form-control" type="password" id="password" name="password" value="{{@$data['password']}}" minlength="10" maxlength="200" required="" autocomplete="off">
            </div>
        </div>
    </div>    

    <div class="form-row">
        <div class="form-group col-md-6 col-12">
            <label class="required">WHMCS domain URL</label>
            <div class="input-group">
                <input class="form-control" type="text" id="domain_url" name="domain_url" value="{{@$data['domain_url']}}" minlength="3" maxlength="50" required="" autocomplete="off">
            </div>
        </div>
    </div>     
    
    <div class="form-row">
        <div class="form-group col-md-12 col-12">
            <label class="required">Hapi Key (Hubspot)</label>
            <input class="form-control" type="text" id="hapi_key" name="hapi_key" value="{{@$data['hapi_key']}}" minlength="10" maxlength="200" required="" autocomplete="off">    
        </div>
    </div>
   
    <div class="kt-form__actions">
        <button class="btn btn-label-brand btn-bold"  type="submit" data-ktwizard-type="action-submit">
            {{$button}}
        </button>
    </div>
</div>