@extends('layout.app')
@php
$totalTitle = 'Pipelines';
@endphp
@section('title', $totalTitle)
@section('title-description')

<meta name="description" content="Pipelines">
    
@endsection
@section('content')
@section('head')
@parent    
@show
<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor bg-white pt-7 pipline-page" id="kt_content">

        <!-- begin:: Content -->
        <div class="kt-container kt-grid__item kt-grid__item--fluid home-container">
           
            <div class="row">
                <!-- body content start -->
                <div class="col-12">
                    <div class="kt_blog-post  pb-5">
                        
                        <div class="row justify-content-center">
                            <div class="col-xl-8 col-lg-8 col-md-12">

                                  <span id="alert-span"></span>
                                                           
                               @if($res['status'] == 'ok')
                                    
                                @endif
                                @if($res['status'] == 'false')
                                    <div class="alert alert-danger">
                                        {{ $res['message'] }}
                                    </div>
                                @endif
                                @if($res['status'] == 'true')
                                    <div class="alert alert-success">
                                        {{ $res['message'] }}
                                    </div>
                                @endif
                             
                             
                              
                                <div class="row">
                                    <div class="col">
                                        <div class="card">
                                            <div class="card-header">
                                                <label>Tickets-Pipeline Sync</label>
                                            </div>
                                            <div class="card-body">
                                              <h3>Unable to Load Pipelines .</h3>
                                                <p>please check your credentials</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                               
                            </div>
                        </div>                                  
                    </div>
                </div>
                <!-- body content end -->
            </div>
        </div>
    </div>                  
</div>


