@extends('layout.app')
@php
$totalTitle = 'Pipelines';
$pipeline['message'] = 'pipelines not found';
@endphp
@section('title', $totalTitle)
@section('title-description')

<meta name="description" content="Pipelines">
    
@endsection
@section('content')
@section('head')
@parent    
@show
<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor bg-white pt-7 pipline-page" id="kt_content">

        <!-- begin:: Content -->
        <div class="kt-container kt-grid__item kt-grid__item--fluid home-container">
           
            <div class="row">
                <!-- body content start -->
                <div class="col-12">
                    <div class="kt_blog-post  pb-5">
                        
                        <div class="row justify-content-center">
                            <div class="col-xl-8 col-lg-8 col-md-12">

                                <span id="alert-span"></span>
                                 @if($pipeline['status'] == 'ok')
                                      <div>
                                         </div>
                                 @elseif($pipeline['status'] == 'true')
                                      <div class="alert alert-success">
                                        {{ $pipeline['message'] }}
                                    </div>
                                 @endif
                               
                               
                                <div class="row">
                                    <div class="col">
                                        <div class="card">
                                            <div class="card-header">
                                                <label>Ticket-Pipeline Sync</label>
                                            </div>
                                      
                                            <div class="card-body">
                                                <form id="kt_form" name="kt_form" action="{{url('apps/whmcs/tickets_pipelines')}}" method="POST">
                                                    <div class="container">
                                                        @csrf
                                                        <div class="row">
                                                            <div class="col-6">
                                                            <h2>
                                                                WHMCS
                                                            </h2>
                                                            </div>
                                                            <div class="col-6">
                                                            <h2>
                                                                HubSpot
                                                            </h2>
                                                            </div>
                                                        </div>
                                                        @if($pipeline['pipeline_exists'] == 'false')
                                                            <div class="row">
                                                                <div class="col-6">
                                                                    @if($pipeline['pipeline_count'] > 0)
                                                                        @for ($i=0; $i < $pipeline['pipeline_count']; $i++)
                                                                            <select name="whmcs_stages[{{$i}}]" required="">
                                                                                <option value="">SELECT</option>
                                                                                @foreach($pipeline['whmcs_stages'] as $stage )
                                                                                    <option value="{{$stage}}">{{$stage}}</option>
                                                                                @endforeach
                                                                            </select>
                                                                            <br>
                                                                        @endfor
                                                                    @else 
                                                                        <span>No data found</span>
                                                                    @endif
                                                                </div>
                                                            <div class="col-6">
                                                                 @if($pipeline['pipeline_count'] > 0)
                                                                        @for ($i=0; $i < $pipeline['pipeline_count']; $i++)
                                                                            <select name="hubspot_stages[{{$i}}]" required="">
                                                                                <option value="">SELECT</option>
                                                                                @foreach($pipeline['hubspot_stages'] as $hubspot_stage)
                                                                                    
                                                                                        <option value="{{$hubspot_stage}}">{{$hubspot_stage}}</option>
                                                                                    
                                                                                @endforeach
                                                                            </select>
                                                                            <br>
                                                                        @endfor
                                                                    @else 
                                                                        <span>No data found</span>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        @elseif($pipeline['pipeline_exists'] == 'true')
                                                                   <div class="row">
                                                                <div class="col-6">
                                                                    @if($pipeline['pipeline_count'] > 0)
                                                                        @for ($i=0; $i < $pipeline['pipeline_count']; $i++)
                                                                         <?php $whmcs_current_stage = $pipeline['current_whmcs_stage'][$i]; ?>
                                                                            <select name="whmcs_stages[{{$i}}]" required="">
                                                                                <option value="">SELECT</option>
                                                                                @foreach($pipeline['whmcs_stages'] as $stage )
                                                                                    <option value="{{$stage}}" <?php if(  $whmcs_current_stage == $stage){ echo "selected" ;}?>>{{$stage}}</option>
                                                                                @endforeach
                                                                            </select>
                                                                            <br>
                                                                        @endfor
                                                                    @else 
                                                                        <span>No data found</span>
                                                                    @endif
                                                                </div>
                                                            <div class="col-6">
                                                                 @if($pipeline['pipeline_count'] > 0)
                                                                        @for ($i=0; $i < $pipeline['pipeline_count']; $i++)
                                                                         <?php $hubspot_current_stage = $pipeline['current_hubspot_stage'][$i]; ?>
                                                                            <select name="hubspot_stages[{{$i}}]" required="">
                                                                                <option value="">SELECT</option>
                                                                                @foreach($pipeline['hubspot_stages'] as $hubspot_stage)
                                                                                    
                                                                                        <option value="{{$hubspot_stage}}" <?php if($hubspot_current_stage == $hubspot_stage){ echo "selected" ;}?>>{{$hubspot_stage}}</option>
                                                                                    
                                                                                @endforeach
                                                                            </select>
                                                                            <br>
                                                                        @endfor
                                                                    @else 
                                                                        <span>No data found</span>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        @else

                                                            <div class="alert alert-danger">
                                                                 {{ $res['message'] }}
                                                             </div>
                                                            
                                                        @endif
                                                  
                                                    </div>
                                                    <div class="card-body">
                                                        <span>
                                                            <button class="btn btn-label-brand btn-bold" id="kt_update_info" type="submit">Update</button>
                                                        </span>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>                                  
                    </div>
                </div>
                <!-- body content end -->
            </div>
        </div>
    </div>                  
</div>

@section('scripts')
@parent


  <!--begin::Page Scripts(used by this page) -->
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

   <!--   <script src="vendor/jquery/dist/jquery.min.js"></script> -->
        <script type="text/javascript">
            "use strict";

        //     // Class definition
        //     var KTWizard2 = function () {
        //         // Base elements
        //         var wizardEl;
        //         var formEl;
        //         var validator;
        //         var wizard;

        //         var initValidation = function() {
        //             validator = formEl.validate({
        //                 // Validate only visible fields
        //                 ignore: ":hidden",

        //                 // Validation rules
        //                 rules: {
                           
        //                     // routes :{
        //                     //     required: true,
        //                     //     maxlength: 3,
        //                     //     digits:true
        //                     // },
        //                     // country_code :{
        //                     //     required: true,
        //                     //     maxlength: 3,
        //                     //     digits:true
        //                     // },
        //                     // sender :{
        //                     //     required: true,
        //                     //     maxlength: 10,
        //                     // },
        //                     // MSG91 :{
        //                     //     required: true,
        //                     // }
        //                 },

        //                 // Display error
        //                 invalidHandler: function(event, validator) {
        //                     KTUtil.scrollTop();
        //                     swal.fire({
        //                         "title": "",
        //                         "text": "There are some errors in your submission. Please correct them.",
        //                         "type": "error",
        //                         "confirmButtonClass": "btn btn-secondary"
        //                     });
        //                 },

        //                 // Submit valid form
        //                 // submitHandler: function (form) {

        //                 // }
        //             });
        //         }

        //         var initSubmit = function() {
        //             var btn = formEl.find('[data-ktwizard-type="action-submit"]');
        //             btn.on('click', function(e) {
        //                 e.preventDefault();

        //                 if (validator.form()) {
        //                     $('#kt_form').submit();     
        //                 }
        //             });
        //         }

        //         return {
        //             // public functions
        //             init: function() {
        //                 wizardEl = KTUtil.get('kt_wizard_v2');
        //                 formEl = $('#kt_form');
        //                 initValidation();
        //                 initSubmit();
        //             }
        //         };
        //     }();
   
        // jQuery(document).on('click','.sync',function(e) {
        //     $(".loader").fadeIn("slow");
        //     //e.preventDefault();
        //     var url = $(this).attr('data-url');
        //     var ds = $(this).attr('data-step');
        //     $('#alert-span').html('');
        //     // e.stopImmediatePropagation();
        //     // return false;
        // });


        $('#kt_update_info').click(function(e) {
            e.preventDefault();

            var btn = $(this);
            var form = $(this).closest('form');

            // form.validate({
            //     rules: {
            //     },
            //     onkeyup: false,
            //     onfocusout: false,
            //     onclick: false
            // });

            // if (!form.valid()) {
            //     return;
            // }

            form.submit();
        });


        </script>
@show
@endsection