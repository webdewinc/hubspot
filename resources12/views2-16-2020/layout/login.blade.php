<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>

    <title>@yield('title')</title>
    
     
    <!-- begin::Head -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @yield('title-description')
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @section('head')
        @include('layout.include.head')
    @show

   

<meta name="description" content="Login">
    

    <!-- end::Head -->
    
</head>
    <body class="kt-page--loading-enabled kt-page--loading kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header--minimize-menu kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-aside--enabled kt-aside--left kt-aside--fixed kt-page--loading">
    <!-- begin::Page loader -->
        @include('layout.include.partials.loader')
    <!-- end::Page Loader -->


        @yield('content')
        

    <!-- end:: Page -->
 

    @section('scripts')
        @include('layout.include.scripts')
    @show   

    <!--begin::Page Scripts(used by this page) -->
        <script src="{{ asset('') . config('app.public_url') . '/assets/js/pages/custom/login/login-general.js'  }}" type="text/javascript"></script> 
    <!--end::Page Scripts -->
        <script type="text/javascript">
        
        $('#kt_logins').click(function(e) {
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');           

            form.validate({
                rules: {
                    user_email: {
                        required: true,
                        email: true
                    },
                    password_login: {
                        required: true
                    }
                },
                messages :{
                    user_email: {
                        required: 'Please enter a valid email address.',
                        email: 'Please enter a valid email address.',
                    },
                    password_login:{ 
                        required:  'Please enter a valid password.'
                    }
                },
                onkeyup: true,
                onfocusout: true,
                onclick: true
            });

            if (!form.valid()) {
                return;
            }

            //btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);

            form.submit();
            
        });
        $('#kt_register').click(function(e) {
            e.preventDefault();

            var btn = $(this);
            var form = $(this).closest('form');


            // Displays the recpatcha form in the element with id "captcha"
                // Recaptcha.create("6LdQRbgUAAAAACb4r31Veez9xCKdTHi86sA2K6Ni", "captcha", {
                // theme: "clean",
                // callback: Recaptcha.focus_response_field
                // });

                // Add reCaptcha validator to form validation
                // jQuery.validator.addMethod("checkCaptcha", (function() {
                // var isCaptchaValid;
                // isCaptchaValid = false;
                // $.ajax({
                //     url: "libs/checkCaptcha.php",
                //     type: "POST",
                //     async: false,
                //     data: {
                //     recaptcha_challenge_field: Recaptcha.get_challenge(),
                //     recaptcha_response_field: Recaptcha.get_response()
                //     },
                //     success: function(resp) {
                //     if (resp === "true") {
                //         isCaptchaValid = true;
                //     } else {
                //         Recaptcha.reload();
                //     }
                //     }
                // });
                // return isCaptchaValid;
                // }), "");

            form.validate({
                rules: {
                    user_code: {
                        required: true,
                        remote: {
                            url: "{{url('varifyusercode')}}",
                            type: "GET",
                            data: {
                                action: function () {
                                    return "1";
                                },
                            }
                        },
                       // letterDigitsOnly: '#user_code' 
                    },
                    user_fname: {
                        required: true,
                        
                    },
                    user_lname: {
                        required: true,
                       
                    },
                    email: {
                        required: true,
                        email: true,                        
                        remote: {
                            url: "{{url('varifyemail')}}",
                            type: "GET",
                            data: {
                                action: function () {
                                    return "1";
                                },
                            }
                        }  
                        
                    },
                    '#password': {
                        required: true,
                    },
                    password_confirm: {
                        required: true,
                        equalTo: "#password"
                    },
                    agree: {
                        required: true
                    },
                    // recaptcha_response_field: {
                    // required: true,
                    // checkCaptcha: true
                    // }
                },
                messages: {
                    email: {
                        required: "Please enter a valid email address.",
                        remote: "Email has been already registered."
                    },
                    user_code: {
                        required: "Please enter a valid username.",
                        remote: "Please enter a valid username.",

                    },
                    user_fname: {
                        required: "Please enter a valid first name.",
                       
                    },
                    user_lname: {
                        required: 'Please enter a valid last name.',
                       
                    },
                    password: {
                        required: 'Please enter a valid password.',
                    },
                    password_confirm: {
                        required: 'Please enter a valid confirm password.',
                        equalTo: 'Confirm password should be same as password.'
                    },
                    agree: {
                        required: 'Please accept the terms & conditions.',
                    },
                    // recaptcha_response_field: {
                    // checkCaptcha: "Your Captcha response was incorrect. Please try again."
                    // }
                },
                onkeyup: true,
                onfocusout: true,
                onclick: true
            });

            if (!form.valid()) {
                return;
            }

            form.submit();
        });

        // jQuery.validator.addMethod("letterDigitsOnly", function(value, element) {
        //   return this.optional(element) || /^[a-zA-Z0-9]*$/i.test(value);
        // }, "Letters and digits only please.");


        $('#kt_login_forgot_pass').click(function(e) {
            e.preventDefault();

            var btn = $(this);
            var form = $(this).closest('form');

            form.validate({
                rules: {
                    email: {
                        required: true,
                        email: true,
                        remote: {
                            url: "{{url('varifyemailForgot')}}",
                            type: "GET",
                            data: {
                                action: function () {
                                    return "1";
                                },
                            }
                        }
                    }
                },
                messages: {
                    email:{
                        required: 'Please enter a valid email address.',
                        remote: "Please enter a valid/existing email address."
                    }
                },
                onkeyup: true,
                onfocusout: true,
                onclick: true
            });

            if (!form.valid()) {
                return;
            }

            //btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
            form.submit();
        });
        $('#kt_logins_reset_custom').click(function(e) {
            e.preventDefault();

            var btn = $(this);
            var form = $(this).closest('form');

            form.validate({
                rules: {
                    email: {
                        required: true,
                    },
                    '#password': {
                        required: true,
                    },
                    password_confirmation: {
                        required: true,
                        equalTo: "#password"
                    },
                },
                messages: {
                    email:{
                        required: 'Please enter a valid email address.',
                        remote: "Please enter a valid/existing email address."
                    },
                    password: {
                        required: 'Please enter a valid password.',
                    },
                    password_confirmation: {
                        required: 'Please enter a valid confirm password.',
                        equalTo: 'Please enter a valid password.'
                    }
                }
            });

            if (!form.valid()) {
                return;
            }

            //btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
            form.submit();
        });

    </script>
    </body>
    <!-- end::Body -->
</html>