@extends('layout.login')
@php
$totalTitle = 'Sign up';
@endphp
@section('title', $totalTitle)


@section('content')

<!-- Authentication Links --> 
@if(session()->has('message'))
    <?php
        $form = session()->get('form');
        if($form == ''){
            $form = 'signin';
            $result = 1;
            $message = 'Password has been changed.';
            $alert = 'success';
        } else {
            $result = 0;
        }
        
    ?>
@else 
    <?php
        $form = 'signin';
        $result = 0;
    ?>
@endif
<div class="kt-grid kt-grid--ver kt-grid--root kt-page">
    <div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v6 kt-login--{{$form}}" id="kt_login">
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--desktop kt-grid--ver-desktop kt-grid--hor-tablet-and-mobile">
            <div class="kt-grid__item  kt-grid__item--order-tablet-and-mobile-2  kt-grid kt-grid--hor kt-login__aside">
                <div class="kt-login__wrapper">
                    <div class="kt-login__container">
                        <div class="kt-login__body">
                            <div class="kt-login__logo">
                                <!-- <a href="#">
                                    <img src="assets/media/company-logos/logo-2.png">
                                </a> -->
                                @include('layout.include.partials.brand')
                            </div>
                            
                            <div class="kt-login__signin">
                                <div class="kt-login__form">
                              
                                </div>
                                <div class="kt-login__actions">
                                    <?php if(isset($app) && !empty($app)) :?>
                                        <?php
                                            $url = url('').'/'. $app . '/auth2-callback';
                                        ?>
                                        <a class="btn btn-label-brand btn-bold" href="{{@$url}}" >Sign In with Hubspot</a>
                                    <?php else : ?>
                                    <a class="btn btn-label-brand btn-bold" href="{{url('auth2-callback')}}" >Sign In with Hubspot</a>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="kt-grid__item kt-grid__item--fluid kt-grid__item--center kt-grid kt-grid--ver kt-login__content" style="/*background-image: url(assets/media/bg/bg-4.jpg);*/">
                <div class="kt-login__section">
                    <div class="kt-login__block">
                        <div class="banner-wrapper text-center">
                            @include('layout.include.partials.banner')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@section('mainscripts')
@parent
@show
@endsection


