<?php
$vendor = vendor_select('plivo');
?>
<p><a href="javascript:void(0);" title="Webdew Groups" ><strong id="select-vendor-append">{{$vendor['name']}}</strong></a>
<a href="https://www.plivo.com/docs/sms/api/message#send-a-message" hidden target="_blank" style="float:right;">Api Docs</a>
</p>             
@csrf
<div class="kt-wizard-v2__form">
        
    <?php
        if(empty($data)){
            $button = 'Save';
        } else {
            $button = 'Update';
        }
    ?>
    <div class="form-row">
        <div class="form-group col-md-6 col-12">
            <label class="required">{{$vendor['label']['a_id']}}</label>
            <div class="input-group">
                <input class="form-control" type="text" id="a_id" name="a_id" value="{{@$data['a_id']}}" minlength="18" maxlength="50" required="" autocomplete="off">
            </div>
        </div>
		<div class="form-group col-md-6 col-12">
            <label class="required">{{$vendor['label']['a_token']}}</label>
            <div class="input-group">
                <input class="form-control" type="text" id="a_token" name="a_token" value="{{@$data['a_token']}}" minlength="18" maxlength="100" required="" autocomplete="off">
            </div>
        </div>
    </div>
    <div class="form-row">
   	<div class="form-group col-md-6 col-12">
            <label class="required">{{$vendor['label']['s_no']}}</label>
            <div class="input-group">
                <input class="form-control" type="text" id="s_no" name="s_no" value="{{@$data['s_no']}}" minlength="10" maxlength="14" required="" autocomplete="off">
            </div>
        </div>
 
    <div class="form-group col-md-6 col-12">
            <label class="required">{{$vendor['label']['t_hapi_key']}}</label>
            <div class="input-group">
                <input class="form-control" type="text" id="t_hapi_key" name="t_hapi_key" value="{{@$data['t_hapi_key']}}" required="" minlength="10"  autocomplete="off">
            </div>
        </div>
 </div>
		 <div class="form-group col-md-6 col-12">            
            <div class="input-group">
                
				<label class="kt-checkbox">
				   <input type="checkbox" id="default" name="default" value="plivo" @if(@$type == 'plivo'){{'checked'}}@endif> Set as default
					<span></span>
				</label>
            </div>
        </div>
   
   
    <div class="kt-form__actions">
        <button class="btn btn-label-brand btn-bold"  type="submit" data-ktwizard-type="action-submit">
            {{$button}}
        </button>
    </div>
</div>