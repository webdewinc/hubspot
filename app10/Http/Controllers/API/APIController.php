<?php
namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\DB;
use App\SearchRecord;
use App\RestClient;
use App\MstDomainUrl;
use App\MstSeoData;
use App\CnfDomainCountry;
use App\MstCountry;
use App\MstOrganicSearch;
use App\MstOrgExtraField;
use App\MstSimilarSite;
use App\MstDaPa;
use App\User;
use App\Chat;

class APIController extends Controller
{
    

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }
      
    public function checkContact($email, $args = [])
    {
        $args['url'] = "https://api.hubapi.com/contacts/v1/contact/email/".$email."/profile?hapikey=" . $args['hapi_key'];
        $result = curlAccess( 'GET', $args );
        $res = ['status' => 1,'result' => $result];
        if(isset($result->status)) {
            if($result->status == 'error'){
                $res = ['status' => 0,'result' => $result];
            }
        } else {
            $res = ['status' => 1,'result' => $result];
        }
        return $res;
    }
    public function contactSync() {
      die;
        // live webdew account - 8bbdfd78-a950-4838-b686-cc0a0c07e0ac
      
        $users = User::whereNotIn('email',['meghna.b@webdew.com','guriqbal.d@webdew.com','randeep.s@webdew.com','randeep.s@webdew.com1','danish@webdew.com', 'danish_cloudnanny@webdew.com','hubspot@webdew.com','sourav.v@webdew.com','deepti.s@webdew.com','divya.v@webdew.com'])->get()->toArray();



        // $args['url'] = 'https://api.hubapi.com/deals/v1/pipelines?hapikey=8bbdfd78-a950-4838-b686-cc0a0c07e0ac';
        // $deals = curlAccess('GET',$args);

        // echo '<pre>';
        // print_r($deals);
        // die;

        // $username= 'r2JpXguEVDViFGsl5VuHnaXN6U6VSqmf';
        // $password= 'Tr74VFTyji94g6d73BvjC6zWL7HdyuaF';
         
        // $username= 'd6inKPQKOJbY5vyXLB8UI6N3r2mlStUt';
        // $password= '7Ou6xFTEt9R1H9haCtbOU3lZFrSXPqmo';

        // $username= 'RHjcycMRiCBe7JXKGWtREPIhkWCKtZzW';
        // $password= 'h7DmPs8Cx10eLgHYS2YEIUtMk60VOT4Z';


        //http://whmcs.webdew.com/includes/api.php
        // $ch = curl_init();
        // curl_setopt($ch, CURLOPT_URL, 'https://phpstack-242348-1101472.cloudwaysapps.com/includes/api.php');
        // curl_setopt($ch, CURLOPT_POST, 1);
        // curl_setopt($ch, CURLOPT_POSTFIELDS,
        //     http_build_query(
        //         array(
        //             'action' => 'GetProducts',
        //             // See https://developers.whmcs.com/api/authentication
        //             'username' => $username,
        //             'password' => $password,
        //             'responsetype' => 'json',
        //         )
        //     )
        // );
        // curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        // $response = curl_exec($ch);
        // curl_close($ch);
        // echo '<pre>';
        // print_r($response);
        // die;

        
        //$args['url'] = 'https://api.hubapi.com/contacts/v1/contact/?hapikey='.env('HUBSPOT_API_KEY');
                             
        foreach ($users as $key => $value) {
          
            $user_id = $value['id'];
            $applicationRecords = DB::table('application_records')->where('user_id',$user_id)->where('app_id' ,'!=' , 3)->get()->toArray();
            if(count($applicationRecords) > 0){
                foreach ($applicationRecords as $k => $v) { 
                    $app_id = $v->app_id;
                    $value['app_type'] = $app_id;
                    $vid = $this->contactSearch($value);
                    if($app_id == 1) {
                        $get_payment = DB::table('payments')->where('user_id',$user_id)->where('amount','>',0)->where('status','succeeded')->where('deal_id',0)->get();
                        $dealname  = 'All in one SMS';
                        foreach ($get_payment as $kp => $vp) {
                            $payment_id = $vp->id;
                            $amount = $vp->amount;
                            $time = time() * 1000;
                            $args['url'] = 'https://api.hubapi.com/deals/v1/deal?hapikey=8bbdfd78-a950-4838-b686-cc0a0c07e0ac';
                            $args['data'] = '{
                                          "associations": {
                                            "associatedCompanyIds": [],
                                            "associatedVids": [
                                              "'.$vid.'"
                                            ]
                                          },
                                          "properties": [
                                            {
                                              "value": "9597429",
                                              "name": "dealstage"
                                            },
                                            {
                                              "value": "'.$dealname.'",
                                              "name": "dealname"
                                            },
                                            {
                                              "value": "9597423",
                                              "name": "pipeline"
                                            },
                                            {
                                              "value": "'. $time .'",
                                              "name": "closedate"
                                            },
                                            {
                                              "value": "'. $amount .'",
                                              "name": "amount"
                                            },
                                            {
                                              "value": "newbusiness",
                                              "name": "dealtype"
                                            }
                                          ]
                                        }';
                            $deals = curlAccess('POST',$args);
                            
                            if(isset($deals->dealId)){
                                DB::table('payments')->where('id',$payment_id)->update([ 'deal_id' => $deals->dealId ]);
                            }

                        }
                    } else if ($app_id == 2){
                        $get_payment = DB::table('whmcs_payments')->where('user_id',$user_id)->where('amount','>',0)->where('status','succeeded')->where('deal_id',0)->get();
                        $dealname = 'WHMCS';
                        foreach ($get_payment as $kp => $vp) {
                            $payment_id = $vp->id;
                            $time = time() * 1000;
                            $amount = $vp->amount;
                            $args['url'] = 'https://api.hubapi.com/deals/v1/deal?hapikey=8bbdfd78-a950-4838-b686-cc0a0c07e0ac';
                            $args['data'] = '{
                                              "associations": {
                                                "associatedCompanyIds": [],
                                                "associatedVids": [
                                                  "'.$vid.'"
                                                ]
                                              },
                                              "properties": [
                                                {
                                                    "value": "9597429",
                                                    "name": "dealstage"
                                                },
                                                {
                                                  "value": "9597423",
                                                  "name": "pipeline"
                                                },
                                                {
                                                  "value": "'.$dealname.'",
                                                  "name": "dealname"
                                                },
                                                {
                                                    "value": "'. $time .'",
                                                    "name": "closedate"
                                                },
                                                {
                                                    "value": "'. $amount .'",
                                                    "name": "amount"
                                                },
                                                {
                                                    "value": "newbusiness",
                                                    "name": "dealtype"
                                                }
                                              ]
                                            }';
                            $deals = curlAccess('POST',$args);
                            if(isset($deals->dealId)){
                                DB::table('whmcs_payments')->where('id',$payment_id)->update([ 'deal_id' => $deals->dealId ]);
                            }
                        }
                    } else if ($app_id == 3){
                        // ecommerce
                    }
                }

                // if(isset($deals)){
                //   echo '<pre>';
                //   print_r($deals);
                //   die;  
                // }
            }
        }
        die;
    }

    private function contactSearch($data){


        $args['url'] = 'https://api.hubapi.com/crm/v3/objects/contacts/search?hapikey=8bbdfd78-a950-4838-b686-cc0a0c07e0ac';
        $args['data'] =  json_encode([
                "filterGroups"=>[
                [
                    "filters"=>[
                        [
                            "propertyName" => "email",
                            "operator" => "EQ",
                            "value" => $data['email']
                        ]
                    ]
                ]
            ]
        ]);
        $result = curlAccess('POST',$args);

        // search contact
        $vid = 0;
        if(!isset($result->status)){
              if($result->total > 0){
                  $vid = $result->results[0]->id;
                   
                  // update contacts
                  $args['url'] = 'https://api.hubapi.com/contacts/v1/contact/vid/'.$vid.'/profile?hapikey=8bbdfd78-a950-4838-b686-cc0a0c07e0ac';
                  
                  $args['data']= json_encode([
                        "properties" => [
                          [
                            "property" => "email",
                            "value" => $data['email']
                          ],
                          [
                            "property" => "firstname",
                            "value" => $data['user_fname']
                          ],
                          [
                            "property" => "lastname",
                            "value" => $data['user_lname']
                          ]
                        ]
                  ]);
             
                  curlAccess('PUT',$args);
                  // update contact

              } else {
                  // insert contacts
                 
                  $args['url'] = 'https://api.hubapi.com/contacts/v1/contact/?hapikey=8bbdfd78-a950-4838-b686-cc0a0c07e0ac';
                  
                  $args['data']= json_encode([
                        "properties" => [
                          [
                            "property" => "email",
                            "value" => $data['email']
                          ],
                          [
                            "property" => "firstname",
                            "value" => $data['user_fname']
                          ],
                          [
                            "property" => "lastname",
                            "value" => $data['user_lname']
                          ]
                        ]
                  ]);
                  $result  = curlAccess('POST',$args);
                  // insert contact

                    if(!isset($result->status)){
                        $vid = $result->vid;
                    }
              }
        } 
        return $vid;
    }
    // public function contactSync() {
    //   die;
    //     $users = User::whereNotIn('email',['meghna.b@webdew.com','guriqbal.d@webdew.com','randeep.s@webdew.com','randeep.s@webdew.com1','danish@webdew.com', 'danish_cloudnanny@webdew.com','hubspot@webdew.com','sourav.v@webdew.com','deepti.s@webdew.com','divya.v@webdew.com'])->get()->toArray();



    //     // $args['url'] = 'https://api.hubapi.com/deals/v1/pipelines?hapikey=8bbdfd78-a950-4838-b686-cc0a0c07e0ac';
    //     // $deals = curlAccess('GET',$args);

    //     // echo '<pre>';
    //     // print_r($deals);
    //     // die;

    //     // $username= 'r2JpXguEVDViFGsl5VuHnaXN6U6VSqmf';
    //     // $password= 'Tr74VFTyji94g6d73BvjC6zWL7HdyuaF';
         
    //     // $username= 'd6inKPQKOJbY5vyXLB8UI6N3r2mlStUt';
    //     // $password= '7Ou6xFTEt9R1H9haCtbOU3lZFrSXPqmo';

    //     // $username= 'RHjcycMRiCBe7JXKGWtREPIhkWCKtZzW';
    //     // $password= 'h7DmPs8Cx10eLgHYS2YEIUtMk60VOT4Z';


    //     //http://whmcs.webdew.com/includes/api.php
    //     // $ch = curl_init();
    //     // curl_setopt($ch, CURLOPT_URL, 'https://phpstack-242348-1101472.cloudwaysapps.com/includes/api.php');
    //     // curl_setopt($ch, CURLOPT_POST, 1);
    //     // curl_setopt($ch, CURLOPT_POSTFIELDS,
    //     //     http_build_query(
    //     //         array(
    //     //             'action' => 'GetProducts',
    //     //             // See https://developers.whmcs.com/api/authentication
    //     //             'username' => $username,
    //     //             'password' => $password,
    //     //             'responsetype' => 'json',
    //     //         )
    //     //     )
    //     // );
    //     // curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    //     // $response = curl_exec($ch);
    //     // curl_close($ch);
    //     // echo '<pre>';
    //     // print_r($response);
    //     // die;

        
    //     //$args['url'] = 'https://api.hubapi.com/contacts/v1/contact/?hapikey='.env('HUBSPOT_API_KEY');
                             
    //     foreach ($users as $key => $value) {
          
    //         $user_id = $value['id'];
    //         $applicationRecords = DB::table('application_records')->where('user_id',$user_id)->where('app_id' ,'!=' , 3)->get()->toArray();
    //         if(count($applicationRecords) > 0){
    //             foreach ($applicationRecords as $k => $v) { 
    //                 $app_id = $v->app_id;
    //                 $value['app_type'] = $app_id;
    //                 $vid = $this->contactSearch($value);
    //                 if($app_id == 1) {
    //                     $get_payment = DB::table('payments')->where('user_id',$user_id)->where('amount','>',0)->where('status','succeeded')->where('deal_id',0)->get();
    //                     $dealname  = 'All in one SMS';
    //                     foreach ($get_payment as $kp => $vp) {
    //                         $payment_id = $vp->id;
    //                         $amount = $vp->amount;
    //                         $time = time() * 1000;
    //                         $args['url'] = 'https://api.hubapi.com/deals/v1/deal?hapikey='.env('HUBSPOT_API_KEY');
    //                         $args['data'] = '{
    //                                       "associations": {
    //                                         "associatedCompanyIds": [],
    //                                         "associatedVids": [
    //                                           "'.$vid.'"
    //                                         ]
    //                                       },
    //                                       "properties": [
    //                                         {
    //                                           "value": "9597429",
    //                                           "name": "dealstage"
    //                                         },
    //                                         {
    //                                           "value": "'.$dealname.'",
    //                                           "name": "dealname"
    //                                         },
    //                                         {
    //                                           "value": "9597423",
    //                                           "name": "pipeline"
    //                                         },
    //                                         {
    //                                           "value": "'. $time .'",
    //                                           "name": "closedate"
    //                                         },
    //                                         {
    //                                           "value": "'. $amount .'",
    //                                           "name": "amount"
    //                                         },
    //                                         {
    //                                           "value": "newbusiness",
    //                                           "name": "dealtype"
    //                                         }
    //                                       ]
    //                                     }';
    //                         $deals = curlAccess('POST',$args);
                            
    //                         if(isset($deals->dealId)){
    //                             DB::table('payments')->where('id',$payment_id)->update([ 'deal_id' => $deals->dealId ]);
    //                         }

    //                     }
    //                 } else if ($app_id == 2){
    //                     $get_payment = DB::table('whmcs_payments')->where('user_id',$user_id)->where('amount','>',0)->where('status','succeeded')->where('deal_id',0)->get();
    //                     $dealname = 'WHMCS';
    //                     foreach ($get_payment as $kp => $vp) {
    //                         $payment_id = $vp->id;
    //                         $time = time() * 1000;
    //                         $amount = $vp->amount;
    //                         $args['url'] = 'https://api.hubapi.com/deals/v1/deal?hapikey='.env('HUBSPOT_API_KEY');
    //                         $args['data'] = '{
    //                                           "associations": {
    //                                             "associatedCompanyIds": [],
    //                                             "associatedVids": [
    //                                               "'.$vid.'"
    //                                             ]
    //                                           },
    //                                           "properties": [
    //                                             {
    //                                                 "value": "9597429",
    //                                                 "name": "dealstage"
    //                                             },
    //                                             {
    //                                               "value": "9597423",
    //                                               "name": "pipeline"
    //                                             },
    //                                             {
    //                                               "value": "'.$dealname.'",
    //                                               "name": "dealname"
    //                                             },
    //                                             {
    //                                                 "value": "'. $time .'",
    //                                                 "name": "closedate"
    //                                             },
    //                                             {
    //                                                 "value": "'. $amount .'",
    //                                                 "name": "amount"
    //                                             },
    //                                             {
    //                                                 "value": "newbusiness",
    //                                                 "name": "dealtype"
    //                                             }
    //                                           ]
    //                                         }';
    //                         $deals = curlAccess('POST',$args);
    //                         if(isset($deals->dealId)){
    //                             DB::table('whmcs_payments')->where('id',$payment_id)->update([ 'deal_id' => $deals->dealId ]);
    //                         }
    //                     }
    //                 } else if ($app_id == 3){
    //                     // ecommerce
    //                 }
    //             }

    //             if(isset($deals)){
    //               echo '<pre>';
    //               print_r($deals);
    //               die;  
    //             }
    //         }
    //     }
    //     die;
    // }

    // private function contactSearch($data){


    //     $args['url'] = 'https://api.hubapi.com/crm/v3/objects/contacts/search?hapikey='.env('HUBSPOT_API_KEY');
    //     $args['data'] =  json_encode([
    //             "filterGroups"=>[
    //             [
    //                 "filters"=>[
    //                     [
    //                         "propertyName" => "email",
    //                         "operator" => "EQ",
    //                         "value" => $data['email']
    //                     ]
    //                 ]
    //             ]
    //         ]
    //     ]);
    //     $result = curlAccess('POST',$args);

    //     // search contact
    //     $vid = 0;
    //     if(!isset($result->status)){
    //           if($result->total > 0){
    //               $vid = $result->results[0]->id;
                   
    //               // update contacts
    //               $args['url'] = 'https://api.hubapi.com/contacts/v1/contact/vid/'.$vid.'/profile?hapikey='.env('HUBSPOT_API_KEY');
                  
    //               $args['data']= json_encode([
    //                     "properties" => [
    //                       [
    //                         "property" => "email",
    //                         "value" => $data['email']
    //                       ],
    //                       [
    //                         "property" => "firstname",
    //                         "value" => $data['user_fname']
    //                       ],
    //                       [
    //                         "property" => "lastname",
    //                         "value" => $data['user_lname']
    //                       ]
    //                     ]
    //               ]);
             
    //               curlAccess('PUT',$args);
    //               // update contact

    //           } else {
    //               // insert contacts
                 
    //               $args['url'] = 'https://api.hubapi.com/contacts/v1/contact/?hapikey='.env('HUBSPOT_API_KEY');
                  
    //               $args['data']= json_encode([
    //                     "properties" => [
    //                       [
    //                         "property" => "email",
    //                         "value" => $data['email']
    //                       ],
    //                       [
    //                         "property" => "firstname",
    //                         "value" => $data['user_fname']
    //                       ],
    //                       [
    //                         "property" => "lastname",
    //                         "value" => $data['user_lname']
    //                       ]
    //                     ]
    //               ]);
    //               $result  = curlAccess('POST',$args);
    //               // insert contact

    //                 if(!isset($result->status)){
    //                     $vid = $result->vid;
    //                 }
    //           }
    //     } 
    //     return $vid;
    // }

}
