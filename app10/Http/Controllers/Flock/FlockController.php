<?php
namespace App\Http\Controllers\Flock;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Support\Facades\Validator;
use Auth;
use Crypt;
use Redirect;
use Hash;
use DB;
use App\User;
use Session;
use Twilio\Rest\Client;
use App\Credential;

class FlockController extends Controller
{

    public function __construct()
    {
        //$this->middleware('auth');
    }
    private function curlAccessH($method, $array, $content_type = 'array' ) 
    {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $array['url']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_ENCODING, "");
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);        
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        switch ($method){
          case "POST":
            curl_setopt($ch, CURLOPT_POST, 1);        
             if ($array['data'])
                curl_setopt($ch, CURLOPT_POSTFIELDS, $array['data']);
             break;
          case "PUT":
             curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
             if ($array['data'])
                curl_setopt($ch, CURLOPT_POSTFIELDS, $array['data']);
             break;
          default:
             curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        }
        

        $headers = array();
        $headers[] = 'Content-Type: application/json';
        if(!empty($array['token'])){
            $headers[] = 'authorization: Bearer '. $array['token'];  
        }

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $result = curl_exec($ch);
        
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);

        if($content_type == 'array'){
            $result = json_decode($result); 
        }  
        return $result;

    }
    public function settings()
    {
        $type = Auth::user()->sms_credentials_default_type;
        $data = Credential::whereUserId(Auth::user()->id)
                            ->where('type', $type)
                            ->value('data');
        $data = unserialize($data);
        $html = view('render.smsdew.'.$type, compact('data','type'));
        return view('smsdew.settings',compact('data','html','type'));
    }
    public function POST_settings(Request $request)
    {
        /* call function*/
        $array = vendor_select($request->type);

        $v = \Validator::make($request->all(), $array['validation']);
        $type = $request->type;

        if ($v->fails()) {
            $errors = $v->errors();
            $data = Credential::whereUserId(Auth::user()->id)
                                ->where('type', $request->type)
                                ->value('data');
            $data = unserialize($data);
            $html = view('render/smsdew/'.$request->type, compact('data'));
            return view('smsdew/settings',compact('errors','data','html','type'));
        } 
        try {
            
            $res = $request->all();
            $result = 0;
            if(!empty($res['default'])){
                User::whereId(Auth::user()->id)->update(['sms_credentials_default_type'=>$res['default'] ]);
                $type = $res['default'];
                $result = 1;
            }

            unset($res['_token']);
            $id = Credential::whereUserId(Auth::user()->id)
                                ->where('type', $res['type'])
                                ->value('id');
            $data = serialize($res);
            if(empty($id)){
                Credential::create([
                                            'data'=> $data,
                                            'user_id' => Auth::user()->id,
                                            'fk_app_id' => 1,
                                            'type'=>$res['type']]);
                $alert = 'success';
                $message = $array['name'].' data has been saved.';
            } else {
                Credential::where('id',$id)->update([
                                                            'data' => $data,
                                                            'fk_app_id' => 1
                                                        ]);
                $alert = 'success';
                $message = $array['name'].' data has been updated.';
            }
        } catch (Exception $e) {
            $alert = 'warning';
            $message = 'Something went wrong in code.';
        }
        
        $data = Credential::whereUserId(Auth::user()->id)
                                ->where('type', $request->type)
                                ->value('data');
        $data = unserialize($data);
        if($result > 0){
            $html = view('render/smsdew/'.$request->type, compact('data','type'));   
        } else {
            $html = view('render/smsdew/'.$request->type, compact('data'));
        }
        
        return view('smsdew.settings',compact('alert','message','data','html','type'));
    }
    public function settings_hub($auth_id)
    {
        $this->auth_access($auth_id);
        $type = Auth::user()->sms_credentials_default_type;
        $data = Credential::whereUserId(Auth::user()->id)
                            ->where('type', $type)
                            ->value('data');
        $data = unserialize($data);
        $html = view('render.smsdew.'.$type, compact('data','type'));
        return view('smsdew.settings-hub',compact('data','html','type'));
    }
    public function POST_settings_hub(Request $request)
    {   
        /* call function*/
        $array = vendor_select($request->type);

        $this->auth_access($request->auth_id);
        $v = \Validator::make($request->all(), $array['validation']);
        $type = $request->type;

        if ($v->fails()) {
            $errors = $v->errors();
            $data = Credential::whereUserId(Auth::user()->id)
                                ->where('type', $request->type)
                                ->value('data');
            $data = unserialize($data);
            $html = view('render/smsdew/'.$request->type, compact('data'));
            return view('smsdew.settings-hub',compact('errors','data','html','type'));
        } 
        try {
            
            $res = $request->all();
            $result = 0;
            if(!empty($res['default'])){
                User::whereId(Auth::user()->id)->update(['sms_credentials_default_type'=>$res['default']]);
                $type = $res['default'];
                $result = 1;
            }

            unset($res['_token']);
            $id = Credential::whereUserId(Auth::user()->id)
                                ->where('type', $res['type'])
                                ->value('id');
            $data = serialize($res);
            if(empty($id)){
                Credential::create([
                                            'data'=> $data,
                                            'user_id' => Auth::user()->id,
                                            'fk_app_id' => 1,
                                            'type'=>$res['type']]);
                $alert = 'success';
                $message = $array['name'].' data has been saved.';
            } else {
                Credential::where('id',$id)->update([
                                                            'data' => $data,
                                                            'fk_app_id' => 1
                                                        ]);
                $alert = 'success';
                $message = $array['name'].' data has been updated.';
            }
        } catch (Exception $e) {
            $alert = 'warning';
            $message = 'Something went wrong in code.';
        }
        
        $data = Credential::whereUserId(Auth::user()->id)
                                ->where('type', $request->type)
                                ->value('data');
        $data = unserialize($data);
        if($result > 0){
            $html = view('render/smsdew/'.$request->type, compact('data','type'));   
        } else {
            $html = view('render/smsdew/'.$request->type, compact('data'));
        }
        
        return view('smsdew.settings-hub',compact('alert','message','data','html','type'));
    }
    public function auth_access($auth_id){
        $user = User::whereId($auth_id)->first();
        Auth::login($user);
    } 
    public function workflowSent(Request $request){
        $data = $request->all();
        $newData['portal_id'] = @$data['origin']['portalId'];
        $newData['actionDefinitionId'] = @$data['origin']['actionDefinitionId'];
        $newData['flock_channel_key'] = @$data['inputFields']['flock_channel_key'];
        $newData['message'] = $data['inputFields']['message'];
        
        $user = User::where('portal_id',$newData['portal_id'])->value('id');
        // Auth::logout();
        if($user){
            // $user = User::where('portal_id',$newData['portal_id'])->first();
            // Auth::login($user);
            $workflows = DB::table('workflows')->insert(['data'=>serialize($newData)]);
            //fa59cdb6-f777-4ce2-a9fd-836b2f5eae9e
            flockNotification($newData['message'],$newData['flock_channel_key']);
        } else {
            $workflows = DB::table('workflows')->insert(['data'=>'Portal Id not found.']);
        }
        
    }
    // for creating defination
    public function definations() {
        
        $args['url'] = 'https://api.hubapi.com/automationextensions/v1/definitions/2356?hapikey=e1964b7f-c16c-453e-9cc0-efcaeb1ced47&applicationId=232314';
        //$args['url'] = 'https://api.hubapi.com/automationextensions/v1/definitions/?hapikey=58723188-d3ff-473b-b9b0-24c927e10c10&applicationId=232314';

        // $token = '49ab6db0-9693-45cc-a0e7-35dc8ae621bb';

        // $args['url'] = 'https://api.flock.co/v1/channels.list?token='.$token;
        // $re = $this->curlAccessH('GET',$args);
        // echo '<pre>';
        // print_r($re);
        // die;

        // $args['data'] = '{
        //     "integrationAppId": "232314",
        //     "extensionName": "Flock1",
        //     "webhookUrl": "https://phpstack-401531-1641101.cloudwaysapps.com/hubspot/flock/workflowSent",
        //     "dataUrl":"https://phpstack-401531-1641101.cloudwaysapps.com/hubspot/flock/workflowSent",
        //     "fieldMetadata": [
        //         {
        //             "label": "Flock channel Key to",
        //             "key": "flock_channel_key",
        //             "fieldType": "TEXT",
        //             "values": [
        //                 {
        //                     "type": "STATIC_VALUE",
        //                     "allowsMergeTags": true
        //                 }
        //             ]
        //         },
        //         {
        //             "label": "Message to",
        //             "key": "message",
        //             "fieldType": "TEXTAREA",
        //             "values": [
        //                 {
        //                     "type": "STATIC_VALUE",
        //                     "allowsMergeTags": true
        //                 }
        //             ]
        //         }
        //     ]
        // }';

        // $args['data'] = '{
        //     "integrationAppId": "232314",
        //     "extensionName": "Flock",
        //     "webhookUrl": "https://phpstack-242348-1570241.cloudwaysapps.com/hubspot/flock/workflowSent",
        //     "dataUrl":"https://phpstack-242348-1570241.cloudwaysapps.com/hubspot/flock/workflowSent",
        //     "fieldMetadata": [
        //         {
        //             "label": "Flock channel Key",
        //             "key": "flock_channel_key",
        //             "fieldType": "TEXT",
        //             "values": [
        //                 {
        //                     "type": "STATIC_VALUE",
        //                     "allowsMergeTags": true
        //                 }
        //             ]
        //         },
        //         {
        //             "label": "Message",
        //             "key": "message",
        //             "fieldType": "TEXTAREA",
        //             "values": [
        //                 {
        //                     "type": "STATIC_VALUE",
        //                     "allowsMergeTags": true
        //                 }
        //             ]
        //         }
        //     ]
        // }';
        $result = $this->curlAccessH('DELETE',$args);
        
        echo '<pre>';
        print_r($result);
        die;
    }
    public function auth2_callback(Request $request) {

        if(env('APP_ENV') == 'live'){
            $provider = new \Flipbox\OAuth2\Client\Provider\HubSpot([
                'clientId'          => config('services.live_flock.clientId'),
                'clientSecret'      => config('services.live_flock.clientSecret'),
                'redirectUri'       => url('/flock/auth2-callback')
            ]);
        } else {
            $provider = new \Flipbox\OAuth2\Client\Provider\HubSpot([
                'clientId'          => config('services.stage_flock.clientId'),
                'clientSecret'      => config('services.stage_flock.clientSecret'),
                'redirectUri'       => url('/flock/auth2-callback')
            ]);
        }
        

        if ( !isset($_GET['code']) ) {
            
            $options = [
                'scope' => ['contacts','timeline'] // array or string
            ];

            // If we don't have an authorization code then get one
            $authUrl = $provider->getAuthorizationUrl($options);
            
            // update

            header('Location: '.$authUrl);
            exit;
            // Check given state against previously stored one to mitigate CSRF attack

        } else {
            
            $result = [];
            // Try to get an access token (using the authorization code grant)
            $token = $provider->getAccessToken('authorization_code', [
                'code' => $_GET['code']
            ]);
            // Optional: Now you have a token you can look up a users profile data
            try {


                // // We got an access token, let's now get the user's details
                $user = $provider->getResourceOwner($token);               
                
                $result['portal_id'] = $user->getHubId();
                $result['email'] = $user->getEmail();
                $result['app_id'] = @$user->getAppId();
                $result['refresh_token'] = @$token->getRefreshToken();
                $result['expires_at'] = @$token->getExpires();
                $result['user_expires_at'] = @$user->getExpires();
                $result['code'] = $_GET['code'];
                $result['expires_at'] = time() + $result['expires_at'] * 0.95;
                $result['auth_token'] = $token->getToken();
                
                $u = User::whereEmail($result['email'])->first();
                if(empty($u->id)) {
                    $validatedData['user_type']       = 'users';
                    $validatedData['activation_code'] = str_random(30).time();
                    $validatedData['user_email_verified'] = 1;
                    $validatedData['user_fname'] = 'New';
                    $validatedData['user_lname'] = 'User';
                    $validatedData['email'] = $result['email'] ;
                    $validatedData['user_status'] = 1;
                    $validatedData['vid'] = 0;
                    app(User::class)->create($validatedData);
                    $u = User::whereEmail($result['email'])->first();
                } 
                Auth::login($u);

                $mapped_app_id = DB::table('mapped_apps')->where('user_id',Auth::user()->id)->where('fk_app_id',4)->value('mapped_app_id');
                if(empty($mapped_app_id)){
                    DB::table('mapped_apps')->insert(['fk_app_id'=> 4,'user_id'=> Auth::user()->id]);
                }
                $scopes = @serialize($user->getScopes());
                  // update
                    $users = User::find(Auth::user()->id);
                    $users->portal_id = $result['portal_id'];
                    $users->hub_email = $user->getEmail();
                    $users->scopes =  $scopes;
                    $users->app_id = @$user->getAppId();
                    $users->refresh_token = @$token->getRefreshToken();
                    $users->expires_at = @$token->getExpires();
                    $users->code = $_GET['code'];
                    $users->sms_credentials_default_type = 'msg91';
                    $users->auth_token = $token->getToken();
                    $users->save();
                  // update


                // redirect to hubspot 
                $hubspotUrl = 'https://app.hubspot.com/';
                if(!empty($result['portal_id']) && isset($result['portal_id'])){
                    $hubspotUrl = 'https://app.hubspot.com/home?portalID='.$result['portal_id'];
                }
                header('Location: '.$hubspotUrl);
                exit;
                // redirect to hubspot 

            } catch (Exception $e) {
                // Failed to get user details
                exit('Oh dear...');
            }
            // Use this to interact with an API on the users behalf
            return redirect('home');
        }
    }
    // for creating defination
    public function terms_service() {
        return view('flock.websites.terms-service');
    }
    // for creating defination
    public function policy() {
        return view('flock.websites.policy');
    }

    public function documentation(){
        return view('flock.websites.documentation');
    }

    public function tagline(){
        return view('flock.websites.tagline');
    }
    public function home()
    {
        $args['url'] = 'https://api.hubapi.com/extensions/sales-objects/v1/object-types?hapikey=e1964b7f-c16c-453e-9cc0-efcaeb1ced47';
        $args['data'] = '{
            "applicationId": 232314,
            "baseUris": [
              "https://example.com/actions"
            ],
            "dataFetchUri": "https://example.com/demo-tickets",
            "title": "DemoTickets",
            "propertyDefinitions": [
              {
                "name": "ticket_type",
                "label": "Ticket type",
                "dataType": "STRING"
              },
              {
                "options": [
                  {
                    "type": "SUCCESS",
                    "label": "In Progress",
                    "name": "inprogress"
                  },
                  {
                    "type": "DEFAULT",
                    "label": "Resolved",
                    "name": "resolved"
                  }
                ],
                "name": "status",
                "label": "Status",
                "dataType": "STATUS"
              },
              {
                "name": "priority",
                "label": "Priority",
                "dataType": "STRING"
              },
              {
                "name": "project",
                "label": "Project",
                "dataType": "STRING"
              }
            ],
            "associatedHubSpotObjectTypes": [
              "COMPANY"
            ],
            "associatedHubSpotObjectTypeProperties": {
              "COMPANY": [
                "domain"
              ]
            }
          }';
        //return view('flock.websites.home');
        $result = $this->curlAccessH('POST',$args);
        
        echo '<pre>';
        print_r($result);
        die;
    }
}