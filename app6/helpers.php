<?php
//function flockNotification($string){
function flockNotification($string,$channel_token) {

    if($string != strip_tags($string)) {
        $data['flockml'] =  "<flockml>".$string."</flockml>" ;  
    } else {
        $data['text'] =  $string;
    }
    $argg['data'] = json_encode($data);
    //$argg['url']  = env('FLOCK_URL');
    $argg['url'] = 'https://api.flock.com/hooks/sendMessage/'.$channel_token;
    $res = curlAccessFlock('POST',$argg);
}
function curlAccessFlock($method, $array, $content_type = 'array' ) {
        
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $array['url']);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    switch ($method){
    case "POST":
        curl_setopt($ch, CURLOPT_POST, 1);        
        if ($array['data'])
            curl_setopt($ch, CURLOPT_POSTFIELDS, $array['data']);
        break;
    case "PUT":
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        if ($array['data'])
            curl_setopt($ch, CURLOPT_POSTFIELDS, $array['data']);
        break;
    default:
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
    }    

    $headers = array();
    $headers[] = 'Content-Type: application/json';
    
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    $result = curl_exec($ch);
    
    if (curl_errno($ch)) {
        echo 'Error:' . curl_error($ch);
    }
    curl_close($ch);

    if($content_type == 'array'){
        $result = json_decode($result); 
    }  
    return $result;
}
function vendor_select($vendor = '') {
       
      $fullarray = [];
      //Listado de proveedores SMS
      $fullarray = [ 
          "msg91"             => [
                                      "name"=>"MSG91",
                                      "label" => [
                                          "msg91"        =>  'Authentication key' ,
                                          "sender"       => 'Sender ID',
                                          "routes"       => 'Routes',
                                          'country_code' => 'Host',
                                          "t_hapi_key" => 'HubSpot API Key',
                                      ],
                                      "fields" => [
                                          "msg91"        =>  'text' ,
                                          "sender"       => 'text',
                                          "routes"       => 'select',
                                          'country_code' => 'select',
                                          "t_hapi_key" =>  'text',
                                      ],
                                      "url" => 'http://my.msgwow.com/api/sendhttp.php',
                                      "validation" => [
                                          'msg91'        => 'required|min:15|max:50',
                                          'sender'       => 'required|min:3|max:10',
                                          'routes'       => 'required',
                                          'country_code' => 'required',
                                          't_hapi_key' => 'required',
                                      ],
                                      "routes" => [
                                          "default"     => 'Default', 
                                          1             => 'Promotional', 
                                          4             => 'Transactional',
                                      ],
                                      'country_code' => [
                                          "0"   => 'International', 
                                          "1"   => 'USA', 
                                          "91"  => 'India',
                                      ]
                                  ], 
          "msgwow"             => [
                                      "name"=>"MSGWOW",
                                      "label" => [
                                          "msgwow"        =>  'Key',
                                          "sender"        => 'Sender ID',
                                          "routes"        => 'Route',
                                          "country_code"  => 'Host',
                                          "t_hapi_key" => 'HubSpot API Key',
                                      ],
                                      "fields" => [
                                          "msgwow"        =>  'text' ,
                                          "sender"        => 'text',
                                          "routes"        => 'select',
                                          "country_code"  => 'select',
                                           "t_hapi_key" =>  'text',
                                      ],
                                      "url" => 'http://my.msgwow.com/api/sendhttp.php',
                                      "validation" => [
                                          'msgwow' => 'required|min:15|max:50',
                                          'sender' => 'required|min:3|max:10',
                                          'routes' => 'required',
                                          'country_code' => 'required',
                                          't_hapi_key' => 'required',
                                      ],
                                      "routes" => [
                                          1             => 'Promotional', 
                                          4             => 'Transactional',
                                      ],
                                      'country_code' => [
                                          "0"   => 'International', 
                                          "1"   => 'USA', 
                                          "91"  => 'India',
                                      ]
                                  ],
          "twilio"             => [
                                      "name"=>"Twilio",
                                      "label" => [
                                          "s_id"       =>  'Account Sid',
                                          "s_token"    => 'Authentication Token',
                                          "mobile"     => 'Mobile Number',
                                          "t_hapi_key" => 'HubSpot API Key',
                                      ],
                                      "fields" => [
                                          "s_id"       =>  'text',
                                          "s_token"    => 'text',
                                          "mobile"     => 'text',
                                          "t_hapi_key" =>  'text',
                                      ],
                                      "validation"  => [
                                          's_id'    => 'required|min:25|max:50',
                                          's_token' => 'required|min:25|max:100',
                                          'mobile'  => 'required|min:10|max:14',
                                          't_hapi_key' => 'required',
                                      ],
                                    ],
        "bulksms"             => [
                                        "name"=>"Bulk Sms",
                                        "label" => [
                                            "u_name"       =>    'User Name',
                                            "pass"         =>    'Password',
                                            "t_hapi_key" => 'HubSpot API Key',
                                        ],
                                        "fields" => [
                                            "u_name"       =>    'text',
                                            "pass"         =>    'password',
                                            "t_hapi_key" =>  'text',
                                        ],
                                        "validation" => [
                                            'u_name'       =>    'required',
                                            'pass'         =>    'required',
                                            't_hapi_key' => 'required',
                                            
                                        ],
                                    ],
        "bulkgate"             => [
                                        "name"              =>    "Bulk Gate",
                                        "label" => [
                                            "app_id"        =>    'Application Id',
                                            "token"         =>    'Application token', 
                                            "sender_id"     =>    'Sender_id',
                                            'unicode'       =>    'Unicode',
                                            "t_hapi_key" => 'HubSpot API Key',
                                        ],
                                        "fields" => [
                                            "app_id"        =>    'text',
                                            "token"         =>    'text',
                                            "sender_id"     =>    'text',
                                            "unicode"       =>     'select',
                                            "t_hapi_key" =>  'text',
                                        ],
                                        "validation" => [
                                            'app_id'        =>    'required',
                                            'token'         =>    'required',
                                            "sender_id"     =>    'required',
                                            "unicode"       =>    'required',
                                            't_hapi_key' => 'required',
                                        ],
                                        "unicode"  =>   [
                                            1            =>      1,
                                            0            =>     0,  
                                        ]
                                    ],
       "textlocal"             => [
                                        "name"               =>  "Text Local",
                                        "label" => [
                                            "api_key"        =>    'API key',
                                            "sender"         =>    "sender",
                                            "t_hapi_key" => 'HubSpot API Key',
                                        ],
                                        "fields" => [
                                            "api_key"        =>    'text',
                                            "sender"         =>    "text",
                                            "t_hapi_key" =>  'text',
                                        ],
                                        "validation" => [
                                            'api_key'        =>    'required',
                                            "sender"         =>    "required",
                                            't_hapi_key' => 'required',
                                        ],
                                    ],
        "kaleyra"             => [
                                        "name"               =>  "kaleyra",
                                        "label" => [
                                            "api_key"        =>    'API key',
                                            "s_id"           =>    'Security Identifier',
                                            "sender_id"      =>     'sender_id',
                                            "t_hapi_key" => 'HubSpot API Key',
                                        ],
                                        "fields" => [
                                            "api_key"        =>    'text',
                                            "s_id"           =>    'text',
                                            "sender_id"      =>     'text',
                                            "t_hapi_key" =>  'text',
                                        ],
                                        "validation" => [
                                            'api_key'        =>    'required',
                                            's_id'           =>    'required',
                                            "sender_id"      =>     'required',
                                            't_hapi_key' => 'required',
                                            
                                        ],
                                    ],
        "plivo"             => [
                                        "name"                         =>      "Plivo",
                                        "label" => [
                                            "a_id"        =>    'Auth Id',
                                            "a_token"     =>    'Auth Token',
                                            "s_no"        =>    'Plivo Number',
                                            "t_hapi_key" => 'HubSpot API Key',
                                           
                                        ],
                                        "fields" => [
                                            "a_id"        =>    'text',
                                            "a_token"     =>    'text',
                                            "s_no"     =>    'text',
                                            "t_hapi_key" =>  'text',
                                            
                                        ],
                                        "validation" => [
                                            'a_id'        =>    'required',
                                            'a_token'     =>    'required',
                                            "s_no"     =>    'required|min:10|max:14',
                                            't_hapi_key' => 'required',
                                            
                                        ],
                                    ],
       "springh"             => [
                                        "name"                         =>      "springedge",
                                        "label" => [
                                            "apikey_se"        =>    'API key',
                                            "sender_id"        =>    "Sender_id",
                                            "t_hapi_key" => 'HubSpot API Key',
                                           
                                        ],
                                        "fields" => [
                                            "apikey_se"        =>    'text',
                                            "sender_id"        =>    'text',
                                            "t_hapi_key" =>  'text',
                                        ],
                                        "validation" => [
                                            'apikey_se'        =>    'required',
                                            "sender_id"        =>    "required",
                                            't_hapi_key' => 'required',
                                            
                                        ],
                                    ],
          "vonage"             => [
                                        "name"                    =>      "vonage",
                                        "label" => [
                                            "apikey_vo"           =>    'API key',
                                            "apisecret_vo"        =>    'API Secret',
                                            "from"              =>    'From',
                                            "t_hapi_key" => 'HubSpot API Key',
                                        
                                           
                                        ],
                                        "fields" => [
                                            "apikey_vo"           =>    'text',
                                            "apisecret_vo"        =>    'text',
                                            "from"              =>    'text',
                                            "t_hapi_key" =>  'text',
                                            
                                        ],
                                        "validation" => [
                                            "apikey_vo"           =>    'required',
                                            "apisecret_vo"        =>    'required',
                                            "from"              =>    'required',
                                            't_hapi_key' => 'required',
                                        ],
                                    ],
        "mobtexting"             => [
                                        "name"                    =>      "mobtexting",
                                        "label" => [
                                            "apikey_mt"        =>    'API key',
                                             "sender"          =>    'sender',
                                             "service"         =>    'service',
                                            "t_hapi_key" => 'HubSpot API Key',

                                           
                                        ],
                                        "fields" => [
                                            "apikey_mt"        =>    'text',
                                            "sender"           =>    'text',
                                             "service"         =>    'select',
                                            "t_hapi_key" =>  'text',

                                            
                                        ],
                                        "validation" => [
                                            "apikey_mt"         =>    'required',
                                            "sender"            =>    'required',
                                            "service"           =>    'required',
                                            't_hapi_key' => 'required',

                                            
                                        ],
                                            "service"   =>[
                                                   
                                          "T"   => 'T', 
                                          "P"   => 'P', 
                                    ]
                                    ],
        "moreify"             => [
                                        "name"                    =>      "moreify",
                                        "label" => [
                                            "project"            =>    'Project',
                                            "password"           =>    'password',
                                            "t_hapi_key" => 'HubSpot API Key',
                                        
                                           
                                        ],
                                        "fields" => [
                                            "project"            =>    'text',
                                            "password"           =>    'text',
                                            "t_hapi_key" =>  'text',
                                            
                                        ],
                                        "validation" => [
                                               "project"         =>    'required',
                                               "password"        =>    'required',
                                            't_hapi_key' => 'required',
                                            
                                        ],
                                    ],
        "clickatell"             => [
                                        "name"                    =>      "clickatell",
                                        "label" => [
                                            "apikey_ct"            =>    'API key',
                                            "t_hapi_key"           => 'HubSpot API Key',
                                           
                                        
                                           
                                        ],
                                        "fields" => [
                                            "apikey_ct"            =>    'text',
                                            "t_hapi_key" =>  'text',
                                        ],
                                        "validation" => [
                                            "apikey_ct"            =>    'required',
                                            't_hapi_key' => 'required',
                                        ],
                                    ]
          

      ];
      if(!empty($vendor)){
          return $fullarray[$vendor];  
      } else {
          return $fullarray;
      }
}
function total_website_count() {
        
    // with another group
    $selectQuery = '';
    return $selectQuery;
        
}
function curlAccessMsg91($method, $array, $content_type = 'array' ) {

      $curl = curl_init();
      curl_setopt_array($curl, array(
          CURLOPT_URL => $array['url'],
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => $method,
          CURLOPT_POSTFIELDS => $array['data'],
          CURLOPT_SSL_VERIFYHOST => 0,
          CURLOPT_SSL_VERIFYPEER => 0,
          CURLOPT_HTTPHEADER => array(
              "authkey: ".$array['token'],
              "content-type: application/json"
          ),
      ));

      $result = curl_exec($curl);
      $err = curl_error($curl);

      curl_close($curl);

      if ($err) {
        echo "cURL Error #:" . $err;
      }

      if($content_type == 'array'){
          $result = json_decode($result); 
      }  
      return $result;

}
function curlAccess($method, $array, $content_type = 'array' ) {
        
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $array['url']);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    switch ($method){
      case "POST":
        curl_setopt($ch, CURLOPT_POST, 1);        
         if ($array['data'])
            curl_setopt($ch, CURLOPT_POSTFIELDS, $array['data']);
         break;
      case "PUT":
         curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
         if ($array['data'])
            curl_setopt($ch, CURLOPT_POSTFIELDS, $array['data']);
         break;
      default:
         curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
    }    

    $headers = array();
    $headers[] = 'Content-Type: application/json';
    $headers[] = 'authorization: Bearer '.env('SEND_GRID_API');
    
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    $result = curl_exec($ch);
    
    if (curl_errno($ch)) {
        echo 'Error:' . curl_error($ch);
    }
    curl_close($ch);

    if($content_type == 'array'){
        $result = json_decode($result); 
    }  
    
    return $result;
}
function sendgridEmail($array){

    $args['url'] = 'https://api.sendgrid.com/v3/mail/send';
    $data = '{
           "from":{
              "email":"noreply@fly.biz"
           },
           "personalizations":[
                {
                     "to":[
                        {
                           "email":"'.$array['email'].'",
                        }
                     ],
                     "subject": "'.$array['subject'].'",
                     "dynamic_template_data": '.$array['data'].'
                  },
           ],
           
           "template_id":"'.$array['template_id'].'"
        }';
        
    $args['data'] = $data;
    $result = curlAccess('POST',$args);
    return $result;
}
function cdd( $array, $heading = 'list' ){

   e($heading . ' begin');

   echo '<pre>';
   print_r($array);
   echo '</pre>';

   e($heading . ' end');

   die;
}
function d( $array, $heading = 'list' ){

   e($heading . ' begin');

   echo '<pre>';
   print_r($array);
   echo '</pre>';   

   e($heading . ' end');
}