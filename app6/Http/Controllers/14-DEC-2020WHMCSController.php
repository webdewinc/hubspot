<?php
namespace App\Http\Controllers\WHMCS;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Support\Facades\Validator;
use Auth;
use Crypt;
use Redirect;
use Hash;
use DB;
use App\User;
use Session;
use Twilio\Rest\Client;
use App\Credential;

class WHMCSController extends Controller
{

    public function __construct()
    {
        //$this->middleware('auth');
    }
    // old and working
    // public function auth2_callback(Request $request) 
    // {
       
    //     if(env('APP_ENV') == 'live'){
    //         $provider = new \Flipbox\OAuth2\Client\Provider\HubSpot([
    //             'clientId'          => config('services.live_whmcs.clientId'),
    //             'clientSecret'      => config('services.live_whmcs.clientSecret'),
    //             'redirectUri'       => url('/whmcs/auth2-callback')
    //         ]);
    //     } else {
    //         $provider = new \Flipbox\OAuth2\Client\Provider\HubSpot([
    //             'clientId'          => config('services.stage_whmcs.clientId'),
    //             'clientSecret'      => config('services.stage_whmcs.clientSecret'),
    //             'redirectUri'       => url('/whmcs/auth2-callback')
    //         ]);
    //     }

    //     if ( !isset($_GET['code']) ) {
            
    //         $options = [
    //             'scope' => ['contacts','timeline'] // array or string
    //         ];

    //         // If we don't have an authorization code then get one
    //         $authUrl = $provider->getAuthorizationUrl($options);
    //         header('Location: '.$authUrl);
    //         exit;
    //         // Check given state against previously stored one to mitigate CSRF attack

    //     } 
    //     // elseif (empty($_GET['state']) || ($_GET['state'] !== Auth::user()->oauth2state)) {

    //     //     exit('Invalid state');

    //     // }
    //     else {

    //         $result = [];
    //         // Try to get an access token (using the authorization code grant)
    //         $token = $provider->getAccessToken('authorization_code', [
    //             'code' => $_GET['code']
    //         ]);
    //         // Optional: Now you have a token you can look up a users profile data
    //         try {
    //             // We got an access token, let's now get the user's details
    //             $user = $provider->getResourceOwner($token);                
                
    //             $result['portal_id'] = $user->getHubId();
    //             $result['email'] = $user->getEmail();
    //             $result['app_id'] = @$user->getAppId();
    //             $result['refresh_token'] = @$token->getRefreshToken();
    //             $result['expires_at'] = @$token->getExpires();
    //             $result['user_expires_at'] = @$user->getExpires();
    //             $result['code'] = $_GET['code'];
    //             $result['expires_at'] = time() + $result['expires_at'] * 0.95;
    //             $result['auth_token'] = $token->getToken();
                
    //             $u = User::whereEmail($result['email'])->first();
    //             if(empty($u->id)) {
    //                 $validatedData['user_type']       = 'users';
    //                 $validatedData['activation_code'] = str_random(30).time();
    //                 $validatedData['user_email_verified'] = 1;
    //                 $validatedData['user_fname'] = 'New';
    //                 $validatedData['user_lname'] = 'User';
    //                 $validatedData['email'] = $result['email'] ;
    //                 $validatedData['user_status'] = 1;
    //                 $validatedData['vid'] = 0;
    //                 app(User::class)->create($validatedData);
    //                 $u = User::whereEmail($result['email'])->first();
    //             } 
    //             Auth::login($u);

    //             $mapped_app_id = DB::table('mapped_apps')->where('user_id',Auth::user()->id)->where('fk_app_id',2)->value('mapped_app_id');
    //             if(empty($mapped_app_id)){
    //                 DB::table('mapped_apps')->insert(['fk_app_id'=> 2,'user_id'=> Auth::user()->id]);
    //             }

    //             $scopes = @serialize($user->getScopes());
    //               // update
    //                 $users = User::find(Auth::user()->id);
    //                 $users->portal_id = $result['portal_id'];
    //                 $users->hub_email = $user->getEmail();
    //                 $users->scopes =  $scopes;
    //                 $users->app_id = @$user->getAppId();
    //                 $users->refresh_token = @$token->getRefreshToken();
    //                 $users->expires_at = @$token->getExpires();
    //                 $users->code = $_GET['code'];
    //                 $users->auth_token = $token->getToken();
    //                 $users->save();
    //               // update
                
    //             // redirect to hubspot 
    //             $hubspotUrl = 'https://app.hubspot.com/';
    //             if(!empty($result['portal_id']) && isset($result['portal_id'])){
    //                 //$hubspotUrl = 'https://app.hubspot.com/home?portalID='.$result['portal_id'];
    //                 $hubspotUrl = 'https://app.hubspot.com/integrations-settings/'.$result['portal_id'].'/installed';
    //             }
    //             header('Location: '.$hubspotUrl);
    //             exit;
    //             // redirect to hubspot 

    //             // $args['data'] = '{
    //             //       "properties": [
    //             //         {
    //             //           "property": "portal_id",
    //             //           "value": "'.$result['portal_id'].'"
    //             //         },
    //             //         {
    //             //           "property": "hub_email",
    //             //           "value": "'.$result['email'].'"
    //             //         },
    //             //         {
    //             //           "property": "refresh_token",
    //             //           "value": "'.$result['refresh_token'].'"
    //             //         },
    //             //         {
    //             //           "property": "token",
    //             //           "value": "'.$result['auth_token'].'"
    //             //         },
    //             //         {
    //             //           "property": "code",
    //             //           "value": "'.$result['code'].'"
    //             //         },
    //             //         {
    //             //           "property": "app_id",
    //             //           "value": "'.$result['app_id'].'"
    //             //         }
    //             //       ]
    //             //     }';
    //             // ;
    //             // $args['url'] = 'https://api.hubapi.com/contacts/v1/contact/email/'.$result['email'].'/profile?hapikey='.env('HUBSPOT_API_KEY');
    //             // $store = $this->curlAccess( 'POST', $args );                
    //             // Use these details to create a new profile

    //         } catch (Exception $e) {
    //             // Failed to get user details
    //             exit('Oh dear...');
    //         }
    //         // Use this to interact with an API on the users behalf
    //         return redirect('home');
    //     }
    // }
    // old and working

    public function auth2_callback(Request $request)
    {
        // $paymentCheck = DB::table('payments')->where('user_id',Auth::user()->id)->where('app_id',1)->value('id');
        // if ($paymentCheck > 0) { //payment is done

            if(env('APP_ENV') == 'live'){
                $provider = new \Flipbox\OAuth2\Client\Provider\HubSpot([
                    'clientId'          => config('services.live_whmcs.clientId'),
                    'clientSecret'      => config('services.live_whmcs.clientSecret'),
                    'redirectUri'       => url('/whmcs/auth2-callback')
                ]);
            } else {
                $provider = new \Flipbox\OAuth2\Client\Provider\HubSpot([
                    'clientId'          => config('services.stage_whmcs.clientId'),
                    'clientSecret'      => config('services.stage_whmcs.clientSecret'),
                    'redirectUri'       => url('/whmcs/auth2-callback')
                ]);
            }

            if (!isset($_GET['code']))
            {

                $options = ['scope' => ['contacts', 'timeline'] // array or string
                ];

                // If we don't have an authorization code then get one
                $authUrl = $provider->getAuthorizationUrl($options);

                // update
                header('Location: ' . $authUrl);
                exit;
                // Check given state against previously stored one to mitigate CSRF attack
                
            }
            else
            {

                $result = [];
                // Try to get an access token (using the authorization code grant)
                $token = $provider->getAccessToken('authorization_code', ['code' => $_GET['code']]);

                //            $_SESSION['token']=$token;
                // Optional: Now you have a token you can look up a users profile data
                try
                {

                    // We got an access token, let's now get the user's details
                    $user = $provider->getResourceOwner($token);

                    $result['portal_id'] = $user->getHubId();
                    $result['email'] = $user->getEmail();
                    $result['app_id'] = @$user->getAppId();
                    $result['refresh_token'] = @$token->getRefreshToken();
                    $result['expires_at'] = @$token->getExpires();
                    $result['user_expires_at'] = @$user->getExpires();
                    $result['code'] = $_GET['code'];
                    $result['expires_at'] = time() + $result['expires_at'] * 0.95;
                    $result['auth_token'] = $token->getToken();

                    $u = User::whereEmail($result['email'])->first();

                    /*===============
                        |insert code end
                        |================
                        */
                        if (empty($u->id))
                        {
                            $validatedData['user_type'] = 'users';
                            $validatedData['activation_code'] = str_random(30) . time();
                            $validatedData['user_email_verified'] = 1;
                            $validatedData['user_fname'] = 'New';
                            $validatedData['user_lname'] = 'User';
                            $validatedData['email'] = $result['email'];
                            $validatedData['user_status'] = 1;
                            $validatedData['vid'] = 0;
                            app(User::class)->create($validatedData);
                            $u = User::whereEmail($result['email'])->first();
                        }
                        Auth::login($u);

                        $mapped_app_id = DB::table('mapped_apps')->where('user_id', Auth::user()
                            ->id)
                            ->where('fk_app_id', 2)
                            ->value('mapped_app_id');
                        if (empty($mapped_app_id))
                        {
                            DB::table('mapped_apps')->insert(['fk_app_id' => 2, 'user_id' => Auth::user()->id]);
                        }

                        $scopes = @serialize($user->getScopes());
                        // update
                        $users = User::find(Auth::user()->id);
                        $users->portal_id = $result['portal_id'];
                        $users->hub_email = $user->getEmail();
                        $users->scopes = $scopes;
                        $users->app_id = @$user->getAppId();
                        $users->refresh_token = @$token->getRefreshToken();
                        $users->expires_at = @$token->getExpires();
                        $users->code = $_GET['code'];
                        $users->auth_token = $token->getToken();
                        $users->save();
                        // update
                        

                    //$user_id = $u->id;
                    $user_id = Auth::user()->id;
                    $portal_id = $result['portal_id'];
                    $refresh_token = $result['refresh_token'];
                    $expiry_time = $result['expires_at'];

                    /*===============
                    |implementing payment check
                    |================
                    */
                    $email = $user->getEmail();

                    $paymentCheck = DB::table('payments')->where('user_id',Auth::user()->id)->where('app_id',2)->value('id');
                    
                    if ($paymentCheck > 0) { //payment is done

                        /*===============
                        |Select query to check if application was already intalled in the same poratl or not
                        |================
                        */

                        $record_check = DB::table('application_records')->where('user_id',Auth::user()->id)->where('app_id',2)->where('portal_id',$portal_id)->value('id');

                        if ($record_check > 0)
                        {
                            DB::table('application_records')->where('app_id',2)->where('user_id',Auth::user()->id)->where('portal_id',$portal_id)->update([
                                        'refresh_token' => $refresh_token,
                                        'expiry_time' => $expiry_time
                                    ]);
                        } else {

                            /*===============
                            |insert code in new table for storing refresh token for different portals
                            |================
                            */

                            $insert = DB::table('application_records')->insertGetId([
                                        'user_id' => $user_id,
                                        'portal_id' => $portal_id,
                                        'client_id' => '',
                                        'secret_id' => '',
                                        'refresh_token' => $refresh_token,
                                        'expiry_time' => $expiry_time,
                                        'app_id' => 2
                                    ]);

                        }

                        // redirect to hubspot
                        $hubspotUrl = 'https://app.hubspot.com/';
                        if (!empty($result['portal_id']) && isset($result['portal_id']))
                        {
                            
                            $hubspotUrl = 'https://app.hubspot.com/integrations-settings/'.$result['portal_id'].'/installed';
                            //$hubspotUrl = 'https://app.hubspot.com/home?portalID=' . $result['portal_id'];
                        }
                        header('Location: ' . $hubspotUrl);
                        exit;
                        // redirect to hubspot
                        
                        
                    } else { 

                        $paymentCheck = DB::table('payments')->insertGetId([
                                                                'app_id' => 2 ,
                                                                'user_id' => Auth::user()->id,
                                                                'stripe_id' => 'manual',
                                                                'amount' => 0,
                                                                'currency_code' => 'usd',
                                                                'status' => 'succeeded',
                                                                'app_type' => 'whmcs',
                                                                'subscription_id' => 0
                                                            ]);

                        /*===============
                        |Select query to check if application was already intalled in the same poratl or not
                        |================
                        */

                        $record_check = DB::table('application_records')->where('user_id',Auth::user()->id)->where('portal_id',$portal_id)->where('app_id',2)->value('id');

                        if ($record_check > 0)
                        {
                            DB::table('application_records')->where('app_id',2)->where('user_id',Auth::user()->id)->where('portal_id',$portal_id)->update([
                                        'refresh_token' => $refresh_token,
                                        'expiry_time' => $expiry_time
                                    ]);
                        } else {

                            /*===============
                            |insert code in new table for storing refresh token for different portals
                            |================
                            */

                            $insert = DB::table('application_records')->insertGetId([
                                        'user_id' => $user_id,
                                        'portal_id' => $portal_id,
                                        'client_id' => '',
                                        'secret_id' => '',
                                        'refresh_token' => $refresh_token,
                                        'expiry_time' => $expiry_time,
                                        'app_id' => 2
                                    ]);

                        }

                        // redirect to hubspot
                        $hubspotUrl = 'https://app.hubspot.com/';
                        if (!empty($result['portal_id']) && isset($result['portal_id']))
                        {
                            
                            $hubspotUrl = 'https://app.hubspot.com/integrations-settings/'.$result['portal_id'].'/installed';
                            //$hubspotUrl = 'https://app.hubspot.com/home?portalID=' . $result['portal_id'];
                        }
                        header('Location: ' . $hubspotUrl);
                        exit;
                        // redirect to hubspot

                        //return redirect('all-in-one-sms');
                        //exit;
                    }
                }
                catch(Exception $e)
                {
                    // Failed to get user details
                    exit('Oh dear...');
                }
                // Use this to interact with an API on the users behalf
                return redirect('/');
            }
        // } else {
        //     return redirect('all-in-one-sms');
        // }
    }
   
    public function fetch_view(){
        Auth::logout();
        if(!empty($_GET['userEmail'])){
            $id = User::whereEmail($_GET['userEmail'])->value('id');
            if(!empty($id)) {
                $auth_token = User::whereEmail($_GET['userEmail'])->value('auth_token');

                DB::table('workflow_results')->insertGetId([
                                    'data' => serialize($_GET),
                                    'response' => serialize($_GET)
                                ]);
                if(!empty($auth_token)) {
                    
                    $results = array("results" => array(),
                                "primaryAction"=> array(
                                    "type"=> "IFRAME",
                                    "width"=> 890,
                                    "height"=> 748,
                                    "uri"=> url("whmcs/settings-hub/".$id."?vid=" . @$_GET['associatedObjectId'] . "&portal_id=" . $GET['portalId'] . "email=" .@$_GET['userEmail'] . "&phone=" . @$_GET['phone']. "&auth_id=" . @$id),
                                    "label"=> "Settings"
                                )
                    );  
                    
                } else {
                    $results = array("results" => array( 0 => array(
                                    "objectId"=> -1,
                                    "title" => "Please login and authorize your account with hubspot",
                                    "link" => env('APP_URL'),
                                    "priority" => "HIGH",
                                    "project" => "API"
                                )
                            )
                        );
                }

            } else {
                $results = array("results" => array( 0 => array(
                            "objectId"=> -1,
                            "title" => "You have to register your account.",
                            "link" => url('/'),
                            "priority" => env('APP_URL'),
                            "project" => "API"
                        )
                    )
                );
            }

        } else {
            $results = array("results" => array( 0 => array(
                        "objectId"=> -1,
                        "title" => "something were wrong.",
                        "link" => env('APP_URL'),
                        "priority" => "HIGH",
                        "project" => "API"
                    )
                )
            );
        }
        return json_encode($results);
    }
    public function refresh_token(){

        $refresh_token = Auth::user()->refresh_token;
        $tokens = "";
        if($refresh_token){            
            if(!empty($refresh_token)){
                
                //if(time() > Auth::user()->expires_at) {
                    
                    $curl = curl_init();
                    curl_setopt_array($curl, array(
                        CURLOPT_URL => 'https://api.hubapi.com/oauth/v1/token',
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => "",
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 30,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => 'POST',
                        CURLOPT_POSTFIELDS => 'grant_type=refresh_token&client_id='.env('HUBSPOT_CLIENT_ID').'&client_secret='.env('HUBSPOT_CLIENT_SECRET').'&refresh_token='.$refresh_token,
                        //CURLOPT_POSTFIELDS => 'grant_type=refresh_token&client_id='.env('HUBSPOT_CLIENT_ID').'&client_secret='.env('HUBSPOT_CLIENT_SECRET').'&scopes=e-commercere&fresh_token='.$refresh_token,
                        CURLOPT_SSL_VERIFYHOST => 0,
                        CURLOPT_SSL_VERIFYPEER => 0,
                        CURLOPT_HTTPHEADER => array(
                            //"authkey: ".Auth::user()->auth_token,
                            "content-type: application/x-www-form-urlencoded;charset=utf-8"
                        ),
                    ));

                    $result = curl_exec($curl);
                    $err = curl_error($curl);
                    curl_close($curl);
                    if ($err) {
                      echo "cURL Error #:" . $err;
                    }
                    $result = json_decode($result);
                    if(isset($result->access_token)){
                        $tokens = $result->access_token;
                        $refresh_token = $result->refresh_token;
                        
                        // updated
                            $users  = User::find(Auth::user()->id);
                            $users->expires_at = time() + $result->expires_in * 0.95;
                            $users->refresh_token = $refresh_token;
                            $users->auth_token = $tokens;
                            //$users->save();
                        // refresh session
                    }
                    
                //}
            }
        } 
        
        return $tokens;
    }
    private function auth_access($auth_id){
        $user = User::whereId($auth_id)->first();
        Auth::login($user);
    }
    /**
     * Hubspot Integration
     */
    public function GET_settings_hub($auth_id)
    {
        if(!empty($_GET)) {
            $response = $_GET;
            User::whereId($_GET['auth_id'])->update(['object_data'=> serialize($response)]);
            $user = User::where('id', $_GET['auth_id'])->first();
            Auth::login($user);
        } else {
            $this->auth_access($auth_id);
        }

        $data = Credential::whereUserId(Auth::user()->id)->where('type','whmcs')->value('data');
        
        $render = 'render.whmcs.settings';
        $data = unserialize($data);
        if(!empty($data)){
            $sync = '1';
        } else {
            $sync = '0';
        }
        $html = view($render, compact('data'));
        return view('whmcs.settings-hub',compact('data','html','sync'));
        
    }
    
    public function POST_settings_hub(Request $request)
    {   
        $array['validation'] =  [
                                    'username' => 'required|min:3|max:50',
                                    'password' => 'required|min:10|max:200',
                                    'hapi_key' => 'required|min:10|max:200',
                                ];
        /* call function*/
        $v = \Validator::make($request->all(), $array['validation']);
        
        $this->auth_access($request->auth_id);
        
        if ($v->fails()) {
            $errors = $v->errors();
            $data = Credential::whereUserId(Auth::user()->id)
                                ->where('type','whmcs')
                                ->value('data');
            $data = unserialize($data);
            $html = view('render.whmcs.settings', compact('data'));
            return view('whmcs.settings-hub',compact('errors','data','html'));
        } 
        try {
            
            $res = $request->all();
            unset($res['_token']);
            $id = Credential::whereUserId(Auth::user()->id)
                                ->where('type','whmcs')
                                ->value('id');
            $data = serialize($res);
            if(empty($id)){
                Credential::create([
                                            'data'=> $data,
                                            'type' => 'whmcs',
                                            'fk_app_id' => 2,
                                            'user_id' => Auth::user()->id,
                                    ]);
                $alert = 'success';
                $message = 'Data has been saved.';
            } else {
                Credential::where('id',$id)->update([
                                                            'data' => $data,
                                                            'type' => 'whmcs',
                                                            'fk_app_id' => 2
                                                        ]);
                $alert = 'success';
                $message = 'Data has been updated.';
            }
        } catch (Exception $e) {
            $alert = 'warning';
            $message = 'Something went wrong in code.';
        }
        
        $data = Credential::whereUserId(Auth::user()->id)
                                ->where('type','whmcs')
                                ->value('data');
        $data = unserialize($data);
        if(!empty($data)){
            $sync = '1';
        } else {
            $sync = '0';
        }
        $html = view('render.whmcs.settings', compact('data'));
        
        return view('whmcs.settings-hub',compact('alert','message','data','html','sync'));
    }

    private function curlAccess($method, $array, $content_type = 'array' ) 
    {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $array['url']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_ENCODING, "");
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);        
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        switch ($method){
          case "POST":
            curl_setopt($ch, CURLOPT_POST, 1);        
             if ($array['data'])
                curl_setopt($ch, CURLOPT_POSTFIELDS, $array['data']);
             break;
          case "PUT":
             curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
             if ($array['data'])
                curl_setopt($ch, CURLOPT_POSTFIELDS, $array['data']);
             break;
          default:
             curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        }

        curl_setopt($ch, CURLOPT_HTTPHEADER, $array['headers']);
        $result = curl_exec($ch);
        
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);

        if($content_type == 'array'){
            $result = json_decode($result); 
        }  
        return $result;

    }
    private function curlAccessH($method, $array, $content_type = 'array' ) 
    {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $array['url']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_ENCODING, "");
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);        
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        switch ($method){
          case "POST":
            curl_setopt($ch, CURLOPT_POST, 1);        
             if ($array['data'])
                curl_setopt($ch, CURLOPT_POSTFIELDS, $array['data']);
             break;
          case "PUT":
             curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
             if ($array['data'])
                curl_setopt($ch, CURLOPT_POSTFIELDS, $array['data']);
             break;
          default:
             curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        }

        $headers = array();
        $headers[] = 'Content-Type: application/json';
        if(!empty($array['token'])){
            $headers[] = 'authorization: Bearer '. $array['token'];  
        }

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $result = curl_exec($ch);
        
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);

        if($content_type == 'array'){
            $result = json_decode($result); 
        }  
        return $result;

    }
    private function curlAccessG($method, $array, $content_type = 'array' ) 
    {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $array['url']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_ENCODING, "");
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);        
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        switch ($method){
          case "POST":
            curl_setopt($ch, CURLOPT_POST, 1);        
             if ($array['data'])
                curl_setopt($ch, CURLOPT_POSTFIELDS, $array['data']);
             break;
          case "PUT":
             curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
             if ($array['data'])
                curl_setopt($ch, CURLOPT_POSTFIELDS, $array['data']);
             break;
          default:
             curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        }

        $headers = array();
        $headers[] = 'Content-Type: application/html';

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $result = curl_exec($ch);
        
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);

        if($content_type == 'array'){
            $result = json_decode($result); 
        }  
        return $result;

    }
    public function settings()
    {
        $data = Credential::whereUserId(Auth::user()->id)
                            ->where('type','whmcs')
                            ->value('data');
        $data = unserialize($data);
        $html = view('render.whmcs.settings', compact('data'));
        return view('whmcs.settings',compact('data','html'));
    }
    public function POST_settings(Request $request)
    {   
        $array['validation'] =  [
                                    'username' => 'required|min:3|max:50',
                                    'password' => 'required|min:10|max:200',
                                    'hapi_key' => 'required|min:10|max:200',
                                ];
        /* call function*/
        $v = \Validator::make($request->all(), $array['validation']);

        if ($v->fails()) {
            $errors = $v->errors();
            $data = Credential::whereUserId(Auth::user()->id)
                                ->where('type','whmcs')
                                ->value('data');
            $data = unserialize($data);
            $html = view('render.whmcs.settings', compact('data'));
            return view('whmcs.settings',compact('errors','data','html'));
        } 
        try {
            
            $res = $request->all();
            unset($res['_token']);
            $id = Credential::whereUserId(Auth::user()->id)
                                ->where('type','whmcs')
                                ->value('id');
            $data = serialize($res);
            if(empty($id)){
                Credential::create([
                                            'data'=> $data,
                                            'fk_app_id' => 2,
                                            'type' => 'whmcs',
                                            'user_id' => Auth::user()->id,
                                    ]);
                $alert = 'success';
                $message = 'Data has been saved.';
            } else {
                Credential::where('id',$id)->update([
                                                            'data' => $data,
                                                            'type' => 'whmcs',
                                                            'fk_app_id' => 2,
                                                        ]);
                $alert = 'success';
                $message = 'Data has been updated.';
            }
        } catch (Exception $e) {
            $alert = 'warning';
            $message = 'Something went wrong in code.';
        }
        
        $data = Credential::whereUserId(Auth::user()->id)
                                ->where('type','whmcs')
                                ->value('data');
        $data = unserialize($data);
        
        $html = view('render.whmcs.settings', compact('data'));
        
        return view('whmcs.settings',compact('alert','message','data','html'));
    }
    public function workflowSent(Request $request){
        $data = $request->all();
        
        $newData['portal_id'] = @$data['origin']['portalId'];
        $newData['actionDefinitionId'] = @$data['origin']['actionDefinitionId'];
        $newData['select_method'] = @$data['inputFields']['select_method'];
        $newData['phone_number'] = @$data['inputFields']['phone_number'];
        $newData['message'] = @$data['inputFields']['message'];

        $v = \Validator::make($newData, [
            'phone_number' => 'required',
            'message' => 'required',
            'select_method' => 'required',
        ]);

        if ($v->fails())
        {
            $errors = $v->errors();
            $workflows = DB::table('workflows')->insert(['data'=>'validation error.']);
            return ['status' => false];
        } else {
            $user = User::where('portal_id',$newData['portal_id'])->value('id');
            Auth::logout();
            if($user){
                $user = User::where('portal_id',$newData['portal_id'])->first();
                Auth::login($user);
                $recipients = $newData["phone_number"];
                $res = $this->validate_phone_number($recipients);
                if ($res['status'] == true) {
                    $workflows = DB::table('workflows')->insert(['data'=>serialize($newData)]);
                    $result = $this->messageAction($newData['message'],$res['phone'],$newData['select_method']);
                } else {
                    $workflows = DB::table('workflows')->insert(['data'=>'Phone number not valid.']);
                }
            } else {
                $workflows = DB::table('workflows')->insert(['data'=>'Portal Id not found.']);
            }
        }
    }
    // for creating defination
    public function definations() {

        
        

        $args['url'] = 'https://api.hubapi.com/automationextensions/v1/definitions?hapikey=4eb79eb6-10be-45b0-8542-6cdfe68b7c02&applicationId=215155';
        $args['data'] = '{
            "integrationAppId": "215155",
            "extensionName": "Send SMS Webdew",
            "webhookUrl": "https://phpstack-242348-1203567.cloudwaysapps.com/smshsapp/workflowSent",
            "dataUrl":"https://phpstack-242348-1203567.cloudwaysapps.com/smshsapp/workflowSent",
            "fieldMetadata": [
                {
                    "label": "Phone Number",
                    "key": "phone_number",
                    "fieldType": "TEXT",
                    "values": [
                        {
                            "type": "STATIC_VALUE",
                            "allowsMergeTags": true
                        }
                    ]
                },
                {
                    "key": "select_method",
                    "label": "Select SMS Method",
                    "fieldType": "SELECT",
                    "hidden": false,
                    "values": [
                        {
                            "type": "STATIC_VALUE",
                            "options": [
                                {
                                  "description": null,
                                  "value": "msgwow",
                                  "readOnly": null,
                                  "text": "MSGWOW",
                                  "displayOrder": 0,
                                  "hidden": false,
                                  "doubleData": null
                                },
                                {
                                  "description": null,
                                  "value": "msg91",
                                  "readOnly": null,
                                  "text": "MSG91",
                                  "displayOrder": 0,
                                  "hidden": false,
                                  "doubleData": null
                                },
                                {
                                    "description": null,
                                    "value": "twilio",
                                    "readOnly": null,
                                    "text": "TWILIO",
                                    "displayOrder": 0,
                                    "hidden": false,
                                    "doubleData": null
                                }
                            ]
                        }
                    ]
                },
                {
                    "label": "Message",
                    "key": "message",
                    "fieldType": "TEXTAREA",
                    "values": [
                        {
                            "type": "STATIC_VALUE",
                            "allowsMergeTags": true
                        }
                    ]
                }
            ]
        }';

        // $result = $this->curlAccessH('POST',$args);
        // echo '<pre>';
        // print_r($result);
        die;
    }

    // function call
    public function curlAccessWHMCS($method, $array, $content_type = 'array' ) {
        $ch = curl_init();
        $domainURL = 'https://phpstack-242348-1101472.cloudwaysapps.com/';
        curl_setopt($ch, CURLOPT_URL, $domainURL . '/includes/api.php');
        
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        switch ($method){
        case "POST":
            curl_setopt($ch, CURLOPT_POST, 1);
            if ($array['data'])
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query( $array['data'] ));
            break;
        case "PUT":
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
            if ($array['data'])
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query( $array['data'] ) );
            break;
        default:
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        }
        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);

        if($content_type == 'array'){
            $result = json_decode($result); 
        }  
        return $result;
    }

    public function curlAccessHS($method, $array, $content_type = 'array' ) {
      
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $array['url']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        switch ($method){
        case "POST":
            curl_setopt($ch, CURLOPT_POST, 1);
            if ($array['data'])
                curl_setopt($ch, CURLOPT_POSTFIELDS,  $array['data'] );
            break;
        case "PUT":
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
            if ($array['data'])
                curl_setopt($ch, CURLOPT_POSTFIELDS,  $array['data']  );
            break;
        default:
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        }

        $headers = array();
        $headers[] = 'Content-Type: application/json';        
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);

        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);

        if($content_type == 'array'){
            $result = json_decode($result); 
        }  
        return $result;
    }
    public function sync_data() {
        return view('whmcs.sync-data');
    }
    public function sync_data_hub($auth_id) {
        $this->auth_access($auth_id);
        return view('whmcs.sync-data-hub');
    }

    public function whmcs_ecommerce(){

        $args['url'] = "https://api.hubapi.com/crm-objects/v1/objects/products/paged?hapikey=ebd3b34f-c079-42d6-9acd-ab262aa8594f&properties=name&properties=description&properties=embed_code";
        $result = $this->curlAccessHS('GET', $args);
        echo '<pre>';
        print_r($result);
        die;
        
        $args['url']  = 'https://api.hubapi.com/extensions/ecomm/v2/stores';
        $args['data'] = '{
                            "id"       : "ecommercebridge-test-store",
                            "label"    : "Ecommerce Bridge Test Store",
                            "adminUri" : "ecommercebridge-test-store.myshopify.com"
                        }';
        
        $args['token'] = Auth::user()->auth_token;
        $result = $this->curlAccessH('PUT', $args);
        print_r($result);
        die;
    }

    public function curlGet($url) {
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => $url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'GET',
          CURLOPT_HTTPHEADER => array(
            'Cookie: __cfduid=d15f14d1984c6b921f0319eb10ff9cce51606378583'
          ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return $response;

        
    }

    public function whmcs_products_hub() {

        $this->auth_access($_GET['auth_id']);
        $users = User::where('id', Auth::user()->id)->get();
        
        if(!empty($users)){
            foreach($users as $k => $v) {
                
                //$credential_all = unserialize($v->credentials->data);
                $credential_all = unserialize(DB::table('credentials')->where('type','whmcs')->whereUserId(Auth::user()->id)->value('data'));
                
                $array['validation'] =  [
                    'username' => 'required|min:3|max:50',
                    'password' => 'required|min:10|max:200',
                    'hapi_key' => 'required|min:10|max:200',
                ];
                /* call function*/
                $v = \Validator::make($credential_all, $array['validation']);
                if ($v->fails()) {
                    $errors = $v->errors();
                    $res = ['status' => false, 'message' => 'Credentials should be updated.'];
                    return $res;
                } 
                $username = $credential_all['username'];
                $password = $credential_all['password'];
                $hapi_key = $credential_all['hapi_key'];

                // Get products start
                    $orderfields = array(
                        'action' => 'GetProducts',
                        'username' => $username,
                        'password' => $password,
                        'responsetype' => 'json',
                    );
                    $args['data'] = $orderfields;
                    $result = $this->curlAccessWHMCS('POST', $args);

               if(!empty($result)){
                    // Get products end
                    if( $result->result != 'error'){
                        foreach($result->products->product as $key => $val) {

                            $pricing_list = json_decode(json_encode($val->pricing), true);
                            
                            $pid = $val->pid;
                            $gid = $val->gid;
                            $name = $val->name;
                            $payType = $val->paytype;
                            $description = $val->description;
                            $currency = 'INR';
                            foreach($pricing_list as $p => $pv){
                                $currency = $p;
                                $monthly_price = $pv['monthly'];
                                $quarterly_price = $pv['quarterly'];
                                $semiannually_price = $pv['semiannually'];
                                $annually_price = $pv['annually'];
                                $biennially_price = $pv['biennially'];
                                $triennially_price = $pv['triennially'];
                            }

                            if($monthly_price < 0){
                                $monthly_price = 0;
                            }
      
                            $id = DB::table('whmcs_sync_products')->where('whmcs_id',$pid)->where('app_user_id', Auth::user()->id)->value('id');
                           //$id=80
                         
                            if($id == 0) {
                                    $product_id = DB::table('whmcs_sync_products')->insertGetId(
                                        ['whmcs_id' => $pid , 'hub_id' => 0, 'product_name' => $name, 'app_user_id' => Auth::user()->id]
                                    );

                                    $args['url'] = "https://api.hubapi.com/crm-objects/v1/objects/products?hapikey=".$hapi_key;

                                    $args['data'] = '[
                                                          {
                                                            "name": "name",
                                                            "value": "'.$name.'"
                                                          },
                                                          {
                                                            "name": "description",
                                                            "value": "'.$description.'"
                                                          },
                                                          {
                                                            "name": "price",
                                                            "value": "'.$monthly_price.'"
                                                          },
                                                          {
                                                            "name": "recurringbillingfrequency",
                                                            "value": "monthly"
                                                          }
                                                        ]';
                                    $result = $this->curlAccessHS('POST', $args);
                             
                                    $success = DB::table('whmcs_sync_products')->where('id',$product_id)->update(
                                        ['hub_id' => $result->objectId]
                                    );
                            } else {
                                
                                    $hub_id = DB::table('whmcs_sync_products')->where('whmcs_id',$pid)->where('app_user_id', Auth::user()->id)->value('hub_id');
                                 
                                    $args['url'] = "https://api.hubapi.com/crm-objects/v1/objects/products/".$hub_id."?hapikey=".$hapi_key;

                                    $args['data'] = '[
                                                        {
                                                            "name": "name",
                                                            "value": "'.$name.'"
                                                        },
                                                        {
                                                            "name": "description",
                                                            "value": "'.$description.'"
                                                        },
                                                        {
                                                            "name": "price",
                                                            "value": "'.$monthly_price.'"
                                                        },
                                                        {
                                                            "name": "hs_price_inr",
                                                            "value": "'.$monthly_price.'"
                                                        },
                                                        {
                                                            "name": "recurringbillingfrequency",
                                                            "value": "monthly"
                                                        },
                                                        {
                                                            "name": "currency_code",
                                                            "value": "'.$currency.'"
                                                        }
                                                    ]';
                                    $result = $this->curlAccessHS('PUT', $args);
                           
                                    $success = DB::table('whmcs_sync_products')->where('id',$id)->update(
                                        ['hub_id' => @$result->objectId, 'product_name' => @$name, 'app_user_id' => Auth::user()->id]
                                    );
                            }
                        }
                    } else {
                        $res = ['status' => false, 'message' => $result->message ];
                        return $res;
                    }
                } else {
                    $res = ['status' => false, 'message' => 'Data not found.' ];
                    return $res;
                }
            }
            $u = url('whmcs-contacts-hub');
            $html = '<button data-step="1"  class="btn btn-label-brand btn-bold sync" rel="contacts" data-url="'.$u.'">Sync Contacts</button>';
            $res = ['status' => true, 'message' => 'Products has been synced.','html' => $html];
            return $res;
        } else {
            $res = ['status' => false, 'message' => 'User not available.'];
            return $res;
        }
    }

    public function whmcs_contacts_hub(){
        $this->auth_access($_GET['auth_id']);
        $users = User::where('id', Auth::user()->id)->get();
        if(!empty($users)){
            foreach($users as $k => $v){
                //$credential_all = unserialize($v->credentials->data);
                $credential_all = unserialize(DB::table('credentials')->where('type','whmcs')->whereUserId(Auth::user()->id)->value('data'));
                $array['validation'] =  [
                    'username' => 'required|min:3|max:50',
                    'password' => 'required|min:10|max:200',
                    'hapi_key' => 'required|min:10|max:200',
                ];
                /* call function*/
                $v = \Validator::make($credential_all, $array['validation']);
                if ($v->fails()) {
                    $errors = $v->errors();
                    $res = ['status' => false, 'message' => 'Credentials should be updated.'];
                    return $res;
                } 
                $username = $credential_all['username'];
                $password = $credential_all['password'];
                $hapi_key = $credential_all['hapi_key'];
        
                // Get products start
                $orderfields = array(
                    'action' => 'GetClients',
                    'username' => $username,
                    'password' => $password,
                    'responsetype' => 'json',
                );
                $args['data'] = $orderfields;
                $result = $this->curlAccessWHMCS('POST', $args);
                if(!empty($result)){
                    // Get products end
                    if($result->result != 'error'){
                        foreach($result->clients->client as $key => $val) {
                            
                            $cid = $val->id;
                            $email = $val->email;
                            $firstname = $val->firstname;
                            $lastname = $val->lastname;

                            $id = DB::table('whmcs_contacts')->where('whmcs_id',$cid)->where('app_user_id', Auth::user()->id)->value('id');
                            if($id == 0){
                                    $args['url'] = "https://api.hubapi.com/contacts/v1/contact/?hapikey=".$hapi_key;
                                    $args['data'] = '{
                                                        "properties": [
                                                            {
                                                                "property": "email",
                                                                "value": "'.$email.'"
                                                            },
                                                            {
                                                                "property": "firstname",
                                                                "value": "'.$firstname.'"
                                                            },
                                                            {
                                                                "property": "lastname",
                                                                "value": "'.$lastname.'"
                                                            }
                                                        ]
                                                    }';
                                    $result = $this->curlAccessHS('POST', $args);
                                    $vid = 0;
                                    if(!empty($result->vid)){
                                        $vid = $result->vid;
                                    }
                                    $contact_id = DB::table('whmcs_contacts')->insertGetId(
                                        ['whmcs_id' => $cid , 'hub_id' => $vid, 'email' => $email, 'app_user_id' => Auth::user()->id]
                                    );
                            } else {
                                    $vid = DB::table('whmcs_contacts')->where('whmcs_id',$cid)->where('app_user_id', Auth::user()->id)->value('hub_id');
                                    $args['url'] = "https://api.hubapi.com/contacts/v1/contact/vid/".$vid."/profile?hapikey=".$hapi_key;
                                    $args['data'] = '{
                                                        "properties": [
                                                            {
                                                                "property": "email",
                                                                "value": "'.$email.'"
                                                            },
                                                            {
                                                                "property": "firstname",
                                                                "value": "'.$firstname.'"
                                                            },
                                                            {
                                                                "property": "lastname",
                                                                "value": "'.$lastname.'"
                                                            }
                                                        ]
                                                    }';
                                
                                    $result = $this->curlAccessHS('PUT', $args);
                                    $contact_id = DB::table('whmcs_contacts')->where('id',$id)->update(
                                        ['whmcs_id' => $cid , 'hub_id' => $vid, 'email' => $email, 'app_user_id' => Auth::user()->id]
                                    );
                            }
                        }
                    } else {
                        $res = ['status' => false, 'message' => $result->message ];
                        return $res;
                    }
                } else {
                    $res = ['status' => false, 'message' => 'Data not found.' ];
                    return $res;
                }
            }
            $u    = url('whmcs-orders-hub');
            $html = '<button data-step="2"  class="btn btn-label-brand btn-bold sync" rel="orders" data-url="'.$u.'">Sync Orders</button>';
            $res  = ['status' => true, 'message' => 'Contacts has been synced','html' => $html];
            return $res;
        } else {
            $res = ['status' => false, 'message' => 'User not available.'];
            return $res;
        }
    }

    public function whmcs_orders_hub(){
        $this->auth_access($_GET['auth_id']);
        $users = User::where('id', Auth::user()->id)->get();
        if(!empty($users)){
            foreach($users as $k => $v) {
                //$credential_all = unserialize($v->credentials->data);
                $credential_all = unserialize(DB::table('credentials')->where('type','whmcs')->whereUserId(Auth::user()->id)->value('data'));
                $array['validation'] =  [
                    'username' => 'required|min:3|max:50',
                    'password' => 'required|min:10|max:200',
                    'hapi_key' => 'required|min:10|max:200',
                ];
                /* call function*/
                $v = \Validator::make($credential_all, $array['validation']);
                if ($v->fails()) {
                    $errors = $v->errors();
                    $res = ['status' => false, 'message' => 'Credentials should be updated.'];
                    return $res;
                } 
                $username = $credential_all['username'];
                $password = $credential_all['password'];
                $hapi_key = $credential_all['hapi_key'];
        
        
                // GEt orders start
                $orderfields = array(
                    'action' => 'GetOrders',
                    'username' => $username,
                    'password' => $password,
                    'responsetype' => 'json',
                );
                // Get orders stop
                $args['data'] = $orderfields;
                $result = $this->curlAccessWHMCS('POST', $args);
                if(!empty($result)){
                    if($result->result != 'error') {
                        foreach($result->orders->order as $key => $val) {
                            $user_id = $val->userid;
                            $amount = $val->amount;
                            //$productid = DB::connection('mysql2')->table('tblhosting')->where('orderid',$val->id)->value('packageid');
                            if(!empty($val->lineitems->lineitem)){
                                foreach($val->lineitems->lineitem as $key2 => $val2) {
                                    $array = explode("-",$val2->product);
                                    $productid = DB::table('whmcs_sync_products')->where('product_name', 'LIKE' ,'%'.trim($array[1]).'%')->where('app_user_id', Auth::user()->id)->value('whmcs_id');
                                    $id = DB::table('whmcs_sync_order_products')->where('product_id', $productid)->where('user_id', $user_id)->where('app_user_id', Auth::user()->id)->where('order_id', $val->id)->value('id');
                                    $hub_product_id = DB::table('whmcs_sync_products')->where('whmcs_id', $productid)->where('app_user_id', Auth::user()->id)->value('hub_id');
                                    $product = DB::table('whmcs_sync_products')->where('whmcs_id', $productid)->where('app_user_id', Auth::user()->id)->value('product_name');
                                    $vid = DB::table('whmcs_contacts')->where('whmcs_id', $user_id)->where('app_user_id', Auth::user()->id)->value('hub_id');
                                    $timestamp = time() * 1000;
                                    if(empty($id)){
                                        // add to cart
                                        $args['url'] = "https://api.hubapi.com/deals/v1/deal?hapikey=".$hapi_key;
                                        $args['data'] = '{
                                            "associations": {
                                            "associatedCompanyIds": [],
                                            "associatedVids": [
                                                '.$vid.'
                                            ]
                                            },
                                            "properties": [
                                            {
                                                "value": "'.$product.'",
                                                "name": "dealname"
                                            },
                                            {
                                                "value": "appointmentscheduled",
                                                "name": "dealstage"
                                            },
                                            {
                                                "value": "default",
                                                "name": "pipeline"
                                            },
                                            {
                                                "value": '.$timestamp.',
                                                "name": "closedate"
                                            },
                                            {
                                                "value": "'.$amount.'",
                                                "name": "amount"
                                            },
                                            {
                                                "value": "newbusiness",
                                                "name": "dealtype"
                                            }
                                            ]
                                        }';

                                        $result = $this->curlAccessHS('POST', $args);
                                        $deal_id = $result->dealId;

                                        // line items
                                        $args['url'] = "https://api.hubapi.com/crm-objects/v1/objects/line_items?hapikey=".$hapi_key;
                                        $args['data'] = '[
                                                            {
                                                                "name": "hs_product_id",
                                                                "value": "'.$hub_product_id.'"
                                                            },
                                                            {
                                                                "name": "quantity",
                                                                "value": "1"
                                                            },
                                                            {
                                                                "name": "price",
                                                                "value": "'.$amount.'"
                                                            },
                                                            {
                                                                "name": "name",
                                                                "value": "'.$product.'"
                                                            }
                                                        ]';
                                        $result = $this->curlAccessHS('POST', $args);
                                        $line_item_id = $result->objectId;
                                        
                                        
                                        $args['url'] = 'https://api.hubapi.com/crm-associations/v1/associations?hapikey='.$hapi_key;
                                        $args['data'] = '{
                                                            "fromObjectId":'.$deal_id.',
                                                            "toObjectId":'. $line_item_id.',
                                                            "category" : "HUBSPOT_DEFINED",
                                                            "definitionId": 19
                                                        }';
                                                        
                                        $result = $this->curlAccessHS('PUT', $args);
                                        
                                        $order_product_id = DB::table('whmcs_sync_order_products')->insertGetId(
                                            ['product_id' => $productid , 'order_id' => $val->id, 'user_id' => $user_id, 'hub_add_to_cart_id' => $deal_id, 'hub_lineitem_id' => $line_item_id, 'app_user_id' => Auth::user()->id]
                                        );

                                    }

                                } 
                            } else {
                                $hub_product_id = DB::table('whmcs_sync_products')->where('whmcs_id', $productid)->where('app_user_id', Auth::user()->id)->value('hub_id');
                                $id = DB::table('whmcs_sync_order_products')->where('user_id', $user_id)->where('app_user_id', Auth::user()->id)->where('order_id', $val->id)->value('id');
                                if(empty($id)){
                                    $order_product_id = DB::table('whmcs_sync_order_products')->insertGetId(
                                        ['product_id' => 0 , 'order_id' => $val->id, 'user_id' => $user_id, 'app_user_id'=> Auth::user()->id]
                                    );
                                }
                            }   
                        }
                    } else {
                        $res = ['status' => false, 'message' => $result->message ];
                        return $res;
                    }
                } else {
                    $res = ['status' => false, 'message' => 'Data not found.'];
                    return $res;
                }
            }
            
            $html = '<button data-step="3" class="btn btn-label-brand btn-bold finish">Success</button>';
            $res = ['status' => true, 'message' => 'Orders has been synced.','html' => $html];
            return $res;
        } else {
            
            $res = ['status' => false, 'message' => 'User not available.'];
            return $res;
        }
    }
    // laravel app functionality
    // public function whmcs_products() {

    //     $users = User::where('id', Auth::user()->id)->get();
        
    //     if(!empty($users)){
    //         foreach($users as $k => $v) {
    //             //$credential_all = unserialize($v->credentials->data);
    //             $credential_all = unserialize(DB::table('credentials')->where('type','whmcs')->whereUserId(Auth::user()->id)->value('data'));
    //             $array['validation'] =  [
    //                 'username' => 'required|min:3|max:50',
    //                 'password' => 'required|min:10|max:200',
    //                 'hapi_key' => 'required|min:10|max:200',
    //             ];
    //             /* call function*/
    //             $v = \Validator::make($credential_all, $array['validation']);
    //             if ($v->fails()) {
    //                 $errors = $v->errors();
    //                 $res = ['status' => false, 'message' => 'Credentials should be updated.'];
    //                 return $res;
    //             } 
    //             $username = $credential_all['username'];
    //             $password = $credential_all['password'];
    //             $hapi_key = $credential_all['hapi_key'];

    //             // Get products start
    //                 $orderfields = array(
    //                     'action' => 'GetProducts',
    //                     'username' => $username,
    //                     'password' => $password,
    //                     'responsetype' => 'json',
    //                 );
    //                 $args['data'] = $orderfields;
    //                 $result = $this->curlAccessWHMCS('POST', $args);
    //             // Get products end

    //             if($result->result != 'error'){
    //                 foreach($result->products->product as $key => $val) {
                        
    //                     $pid = $val->pid;
    //                     $gid = $val->gid;
    //                     $name = $val->name;
    //                     $payType = $val->paytype;
    //                     $description = $val->description;
    //                     $pricing_list = $val->pricing;
    //                     // $monthly_price = $val->pricing->INR->monthly;
    //                     // $quarterly_price = $val->pricing->INR->quarterly;
    //                     // $semiannually_price = $val->pricing->INR->semiannually;
    //                     // $annually_price = $val->pricing->INR->annually;
    //                     // $biennially_price = $val->pricing->INR->biennially;
    //                     // $triennially_price = $val->pricing->INR->triennially;

    //                     $currency = 'INR';
    //                     foreach($pricing_list as $p => $pv){
    //                         $currency = $p;
    //                         $monthly_price = $pv->monthly;
    //                         $quarterly_price = $pv->quarterly;
    //                         $semiannually_price = $pv->semiannually;
    //                         $annually_price = $pv->annually;
    //                         $biennially_price = $pv->biennially;
    //                         $triennially_price = $pv->triennially;
    //                     }

    //                     if($monthly_price < 0){
    //                         $monthly_price = 0;
    //                     }

    //                     $id = DB::table('whmcs_sync_products')->where('whmcs_id',$pid)->where('app_user_id', Auth::user()->id)->value('id');
    //                     if($id == 0) {
    //                             $product_id = DB::table('whmcs_sync_products')->insertGetId(
    //                                 ['whmcs_id' => $pid , 'hub_id' => 0, 'product_name' => $name, 'app_user_id' => Auth::user()->id]
    //                             );

    //                             $args['url'] = "https://api.hubapi.com/crm-objects/v1/objects/products?hapikey=".$hapi_key;

    //                             $args['data'] = '[
    //                                                 {
    //                                                     "name": "name",
    //                                                     "value": "'.$name.'"
    //                                                 },
    //                                                 {
    //                                                     "name": "description",
    //                                                     "value": "'.$description.'"
    //                                                 },
    //                                                 {
    //                                                     "name": "price",
    //                                                     "value": "'.$monthly_price.'"
    //                                                 },
    //                                                 {
    //                                                     "name": "hs_price_inr",
    //                                                     "value": "'.$monthly_price.'"
    //                                                 },
    //                                                 {
    //                                                     "name": "recurringbillingfrequency",
    //                                                     "value": "monthly"
    //                                                 },
    //                                                 {
    //                                                     "name": "currency_code",
    //                                                     "value": "'.$currency.'"
    //                                                 }
    //                                             ]';
    //                             $result = $this->curlAccessHS('POST', $args);
    //                             $success = DB::table('whmcs_sync_products')->where('id',$product_id)->update(
    //                                 ['hub_id' => $result->objectId]
    //                             );
    //                     } else {
    //                             $hub_id       = DB::table('whmcs_sync_products')->where('whmcs_id',$pid)->where('app_user_id', Auth::user()->id)->value('hub_id');
    //                             $args['url']  = "https://api.hubapi.com/crm-objects/v1/objects/products/".$hub_id."?hapikey=".$hapi_key;

    //                             $args['data'] = '[
    //                                                 {
    //                                                     "name": "name",
    //                                                     "value": "'.$name.'"
    //                                                 },
    //                                                 {
    //                                                     "name": "description",
    //                                                     "value": "'.$description.'"
    //                                                 },
    //                                                 {
    //                                                     "name": "price",
    //                                                     "value": "'.$monthly_price.'"
    //                                                 },
    //                                                 {
    //                                                     "name": "hs_price_inr",
    //                                                     "value": "'.$monthly_price.'"
    //                                                 },
    //                                                 {
    //                                                     "name": "recurringbillingfrequency",
    //                                                     "value": "monthly"
    //                                                 },
    //                                                 {
    //                                                     "name": "currency_code",
    //                                                     "value": "'.$currency.'"
    //                                                 }
    //                                             ]';
    //                             $result = $this->curlAccessHS('PUT', $args);
    //                             $success = DB::table('whmcs_sync_products')->where('id',$id)->update(
    //                                 ['hub_id' => $result->objectId, 'product_name' => $name, 'app_user_id' => Auth::user()->id]
    //                             );
    //                     }
    //                 }
    //             } else {
    //                 $res = ['status' => false, 'message' => $result->message ];
    //                 return $res;
    //             }
    //         }
    //         $u = url('whmcs-contacts');
    //         $html = '<button id="nextBtn" data-step="1" class="btn btn-label-brand btn-bold sync" rel="contacts" data-url="'.$u.'">Sync Contacts</button>';
    //         $res = ['status' => true, 'message' => 'Products has been synced.','html' => $html];
    //         return $res;
    //     } else {
    //         $res = ['status' => false, 'message' => 'User not available.'];
    //         return $res;
    //     }
    // }

    // public function whmcs_contacts() {
    //     $users = User::where('id', Auth::user()->id)->get();
    //     if(!empty($users)){
    //         foreach($users as $k => $v) {
    //             //$credential_all = unserialize($v->credentials->data);
    //             $credential_all = unserialize(DB::table('credentials')->where('type','whmcs')->whereUserId(Auth::user()->id)->value('data'));
    //             $array['validation'] =  [
    //                 'username' => 'required|min:3|max:50',
    //                 'password' => 'required|min:10|max:200',
    //                 'hapi_key' => 'required|min:10|max:200',
    //             ];
    //             /* call function*/
    //             $v = \Validator::make($credential_all, $array['validation']);
    //             if ($v->fails()) {
    //                 $errors = $v->errors();
    //                 $res = ['status' => false, 'message' => 'Credentials should be updated.'];
    //                 return $res;
    //             } 
    //             $username = $credential_all['username'];
    //             $password = $credential_all['password'];
    //             $hapi_key = $credential_all['hapi_key'];
        
    //             // Get products start
    //             $orderfields = array(
    //                 'action' => 'GetClients',
    //                 'username' => $username,
    //                 'password' => $password,
    //                 'responsetype' => 'json',
    //             );
    //             $args['data'] = $orderfields;
    //             $result = $this->curlAccessWHMCS('POST', $args);
    //             if($result->result != 'error'){
    //                 // Get products end
    //                 foreach($result->clients->client as $key => $val) {
                        
    //                     $cid = $val->id;
    //                     $email = $val->email;
    //                     $firstname = $val->firstname;
    //                     $lastname = $val->lastname;

    //                     $id = DB::table('whmcs_contacts')->where('whmcs_id',$cid)->where('app_user_id', Auth::user()->id)->value('id');
    //                     if($id == 0){
    //                             $args['url'] = "https://api.hubapi.com/contacts/v1/contact/?hapikey=".$hapi_key;
    //                             $args['data'] = '{
    //                                                 "properties": [
    //                                                     {
    //                                                         "property": "email",
    //                                                         "value": "'.$email.'"
    //                                                     },
    //                                                     {
    //                                                         "property": "firstname",
    //                                                         "value": "'.$firstname.'"
    //                                                     },
    //                                                     {
    //                                                         "property": "lastname",
    //                                                         "value": "'.$lastname.'"
    //                                                     }
    //                                                 ]
    //                                             }';
    //                             $result = $this->curlAccessHS('POST', $args);
    //                             $vid = 0;
    //                             if(!empty($result->vid)){
    //                                 $vid = $result->vid;
    //                             }
    //                             $contact_id = DB::table('whmcs_contacts')->insertGetId(
    //                                 ['whmcs_id' => $cid , 'hub_id' => $vid, 'email' => $email, 'app_user_id' => Auth::user()->id]
    //                             );
    //                     } else {
    //                             $vid = DB::table('whmcs_contacts')->where('whmcs_id',$cid)->where('app_user_id', Auth::user()->id)->value('hub_id');
    //                             $args['url'] = "https://api.hubapi.com/contacts/v1/contact/vid/".$vid."/profile?hapikey=".$hapi_key;
    //                             $args['data'] = '{
    //                                                 "properties": [
    //                                                     {
    //                                                         "property": "email",
    //                                                         "value": "'.$email.'"
    //                                                     },
    //                                                     {
    //                                                         "property": "firstname",
    //                                                         "value": "'.$firstname.'"
    //                                                     },
    //                                                     {
    //                                                         "property": "lastname",
    //                                                         "value": "'.$lastname.'"
    //                                                     }
    //                                                 ]
    //                                             }';
    //                             $result = $this->curlAccessHS('PUT', $args);
    //                             $contact_id = DB::table('whmcs_contacts')->where('id',$id)->update(
    //                                 ['whmcs_id' => $cid , 'hub_id' => $vid, 'email' => $email, 'app_user_id' => Auth::user()->id]
    //                             );
    //                     }
    //                 }
    //             } else {
    //                 $res = ['status' => false, 'message' => $result->message ];
    //                 return $res;
    //             }
    //         }
    //         $u = url('whmcs-orders');
    //         $html = '<button id="nextBtn" data-step="2" class="btn btn-label-brand btn-bold sync" rel="orders" data-url="'.$u.'">Sync Orders</button>';
    //         $res = ['status' => true, 'message' => 'Contacts has been synced','html' => $html];
    //         return $res;
    //     } else {
    //         $res = ['status' => false, 'message' => 'User not available.'];
    //         return $res;
    //     }
    // }

    // public function whmcs_orders() {
    //     $users = User::where('id', Auth::user()->id)->get();
    //     if(!empty($users)){
    //         foreach($users as $k => $v) {
    //             //$credential_all = unserialize($v->credentials->data);
    //             $credential_all = unserialize(DB::table('credentials')->where('type','whmcs')->whereUserId(Auth::user()->id)->value('data'));
    //             $array['validation'] =  [
    //                 'username' => 'required|min:3|max:50',
    //                 'password' => 'required|min:10|max:200',
    //                 'hapi_key' => 'required|min:10|max:200',
    //             ];
    //             /* call function*/
    //             $v = \Validator::make($credential_all, $array['validation']);
    //             if ($v->fails()) {
    //                 $errors = $v->errors();
    //                 $res = ['status' => false, 'message' => 'Credentials should be updated.'];
    //                 return $res;
    //             } 
    //             $username = $credential_all['username'];
    //             $password = $credential_all['password'];
    //             $hapi_key = $credential_all['hapi_key'];
        
        
    //             // GEt orders start
    //             $orderfields = array(
    //                 'action' => 'GetOrders',
    //                 'username' => $username,
    //                 'password' => $password,
    //                 'responsetype' => 'json',
    //             );
    //             // Get orders stop
    //             $args['data'] = $orderfields;
    //             $result = $this->curlAccessWHMCS('POST', $args);
    //             if($result->result != 'error') {
    //                 foreach($result->orders->order as $key => $val) {
    //                     $user_id = $val->userid;
    //                     $amount = $val->amount;
    //                     //$productid = DB::connection('mysql2')->table('tblhosting')->where('orderid',$val->id)->value('packageid');
    //                     if(!empty($val->lineitems->lineitem)){
    //                         foreach($val->lineitems->lineitem as $key2 => $val2) {
    //                             $array = explode("-",$val2->product);
    //                             $productid = DB::table('whmcs_sync_products')->where('product_name', 'LIKE' ,'%'.trim($array[1]).'%')->where('app_user_id', Auth::user()->id)->value('whmcs_id');
    //                             $id = DB::table('whmcs_sync_order_products')->where('product_id', $productid)->where('user_id', $user_id)->where('app_user_id', Auth::user()->id)->where('order_id', $val->id)->value('id');
    //                             $hub_product_id = DB::table('whmcs_sync_products')->where('whmcs_id', $productid)->where('app_user_id', Auth::user()->id)->value('hub_id');
    //                             $product = DB::table('whmcs_sync_products')->where('whmcs_id', $productid)->where('app_user_id', Auth::user()->id)->value('product_name');
    //                             $vid = DB::table('whmcs_contacts')->where('whmcs_id', $user_id)->where('app_user_id', Auth::user()->id)->value('hub_id');
    //                             $timestamp = time() * 1000;
    //                             if(empty($id)){
    //                                 // add to cart
    //                                 $args['url'] = "https://api.hubapi.com/deals/v1/deal?hapikey=".$hapi_key;
    //                                 $args['data'] = '{
    //                                     "associations": {
    //                                     "associatedCompanyIds": [],
    //                                     "associatedVids": [
    //                                         '.$vid.'
    //                                     ]
    //                                     },
    //                                     "properties": [
    //                                     {
    //                                         "value": "'.$product.'",
    //                                         "name": "dealname"
    //                                     },
    //                                     {
    //                                         "value": "appointmentscheduled",
    //                                         "name": "dealstage"
    //                                     },
    //                                     {
    //                                         "value": "default",
    //                                         "name": "pipeline"
    //                                     },
    //                                     {
    //                                         "value": '.$timestamp.',
    //                                         "name": "closedate"
    //                                     },
    //                                     {
    //                                         "value": "'.$amount.'",
    //                                         "name": "amount"
    //                                     },
    //                                     {
    //                                         "value": "newbusiness",
    //                                         "name": "dealtype"
    //                                     }
    //                                     ]
    //                                 }';

    //                                 $result = $this->curlAccessHS('POST', $args);
    //                                 $deal_id = $result->dealId;

    //                                 // line items
    //                                 $args['url'] = "https://api.hubapi.com/crm-objects/v1/objects/line_items?hapikey=".$hapi_key;
    //                                 $args['data'] = '[
    //                                                     {
    //                                                         "name": "hs_product_id",
    //                                                         "value": "'.$hub_product_id.'"
    //                                                     },
    //                                                     {
    //                                                         "name": "quantity",
    //                                                         "value": "1"
    //                                                     },
    //                                                     {
    //                                                         "name": "price",
    //                                                         "value": "'.$amount.'"
    //                                                     },
    //                                                     {
    //                                                         "name": "name",
    //                                                         "value": "'.$product.'"
    //                                                     }
    //                                                 ]';
    //                                 $result = $this->curlAccessHS('POST', $args);
    //                                 $line_item_id = $result->objectId;
                                    
                                    
    //                                 $args['url'] = 'https://api.hubapi.com/crm-associations/v1/associations?hapikey='.$hapi_key;
    //                                 $args['data'] = '{
    //                                                     "fromObjectId":'.$deal_id.',
    //                                                     "toObjectId":'. $line_item_id.',
    //                                                     "category" : "HUBSPOT_DEFINED",
    //                                                     "definitionId": 19
    //                                                 }';
                                                    
    //                                 $result = $this->curlAccessHS('PUT', $args);
    //                                 $order_product_id = DB::table('whmcs_sync_order_products')->insertGetId(
    //                                     ['product_id' => $productid , 'order_id' => $val->id, 'user_id' => $user_id, 'hub_add_to_cart_id' => $deal_id, 'hub_lineitem_id' => $line_item_id, 'app_user_id' => Auth::user()->id]
    //                                 );

    //                             }

    //                         } 
    //                     } else {
    //                         $hub_product_id = DB::table('whmcs_sync_products')->where('whmcs_id', $productid)->where('app_user_id', Auth::user()->id)->value('hub_id');
    //                         $id = DB::table('whmcs_sync_order_products')->where('user_id', $user_id)->where('app_user_id', Auth::user()->id)->where('order_id', $val->id)->value('id');
    //                         if(empty($id)){
    //                             $order_product_id = DB::table('whmcs_sync_order_products')->insertGetId(
    //                                 ['product_id' => 0 , 'order_id' => $val->id, 'user_id' => $user_id, 'app_user_id'=> Auth::user()->id]
    //                             );
    //                         }
    //                     }   
    //                 }
    //             } else {
    //                 $res = ['status' => false, 'message' => $result->message ];
    //                 return $res;
    //             }
    //         }
            
    //         $html = '<button data-step="3" class="btn btn-label-brand btn-bold finish">Success</button>';
    //         $res = ['status' => true, 'message' => 'Orders has been synced.','html' => $html];
    //         return $res;
    //     } else {
            
    //         $res = ['status' => false, 'message' => 'User not available.'];
    //         return $res;
    //     }
    // }
    // laravel app functionality
    public function get_product_list() {
        
        $users = User::where('id', Auth::user()->id)->first();
        $hapi_key = '';
        if(!empty(DB::table('credentials')->where('type','whmcs')->whereUserId(Auth::user()->id)->value('data'))){
            //$credential_all = unserialize($users->credentials->data);
            $credential_all = unserialize(DB::table('credentials')->where('type','whmcs')->whereUserId(Auth::user()->id)->value('data'));
            $hapi_key = $credential_all['hapi_key'];
        }
        
        if(!empty($hapi_key)){
            $args['url'] = 'https://api.hubapi.com/crm/v3/objects/products?limit=10&hapikey='.$hapi_key;
            if(isset($_GET['offset'])){
                $args['url'] = 'https://api.hubapi.com/crm/v3/objects/products?limit=10&after=' . $_GET['offset'] . '&hapikey='.$hapi_key;
            }
            
            $results = $this->curlAccessH('GET',$args);
            $data = [];
            $next = '';
            $prev = '';
            if(!empty($results->results)){
                foreach($results->results as $key => $value) {
                    $v = json_decode(json_encode($value), true);
                    $data[$key] = $v ;
                    $product_id = $v['id'];
                    $product_name = $v['properties']['name'];
                    $product_price = $v['properties']['price'];
                    $html = view('button.cart-button',compact(['product_id','hapi_key','product_price','product_name']));
                    $data[$key]['html'] = $html;
                }
                if(!empty($results->paging)){
                    $next = url('get-product-list') .'?offset='. $results->paging->next->after;
                }
            }
            return view('product-list-code',compact('data','next'));
        } else {
            echo 'Hubspot key is required.';
        }
    }
    // for creating defination
    // for creating defination
    public function terms_service() {
        return view('whmcs.websites.terms-service');
    }
    // for creating defination
    public function policy() {
        return view('whmcs.websites.policy');
    }

    public function documentation(){
        return view('whmcs.websites.documentation');
    }

    public function tagline(){
        return view('whmcs.websites.tagline');
    }
    public function home()
    {
        return view('whmcs.websites.home');
    }
}