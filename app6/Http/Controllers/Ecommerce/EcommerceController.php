<?php
namespace App\Http\Controllers\Ecommerce;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Support\Facades\Validator;
use Auth;
use Crypt;
use Redirect;
use Hash;
use DB;
use App\User;
use Session;
use Twilio\Rest\Client;
use App\Credential;

class EcommerceController extends Controller
{

    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function auth2_callback(Request $request) 
    {
       
        if(env('APP_ENV') == 'live'){
            $provider = new \Flipbox\OAuth2\Client\Provider\HubSpot([
                'clientId'          => config('services.live_ecommerceCartButton.clientId'),
                'clientSecret'      => config('services.live_ecommerceCartButton.clientSecret'),
                'redirectUri'       => url('/ecommerce/auth2-callback')
            ]);
        } else {
            $provider = new \Flipbox\OAuth2\Client\Provider\HubSpot([
                'clientId'          => config('services.stage_ecommerceCartButton.clientId'),
                'clientSecret'      => config('services.stage_ecommerceCartButton.clientSecret'),
                'redirectUri'       => url('/ecommerce/auth2-callback')
            ]);
        }
        
        if ( !isset($_GET['code']) ) {
            
            $options = [
                'scope' => ['contacts','timeline'] // array or string
            ];

            // If we don't have an authorization code then get one
            $authUrl = $provider->getAuthorizationUrl($options);
            header('Location: '.$authUrl);
            exit;
            // Check given state against previously stored one to mitigate CSRF attack

        } 
        // elseif (empty($_GET['state']) || ($_GET['state'] !== Auth::user()->oauth2state)) {

        //     exit('Invalid state');

        // }
        else {

            $result = [];
            // Try to get an access token (using the authorization code grant)
            $token = $provider->getAccessToken('authorization_code', [
                'code' => $_GET['code']
            ]);
            // Optional: Now you have a token you can look up a users profile data
            try {
                // We got an access token, let's now get the user's details
                $user = $provider->getResourceOwner($token);
                
                $result['portal_id'] = $user->getHubId();
                $result['email'] = $user->getEmail();
                $result['app_id'] = @$user->getAppId();
                $result['refresh_token'] = @$token->getRefreshToken();
                $result['expires_at'] = @$token->getExpires();
                $result['user_expires_at'] = @$user->getExpires();
                $result['code'] = $_GET['code'];
                $result['expires_at'] = time() + $result['expires_at'] * 0.95;
                $result['auth_token'] = $token->getToken();
                
                $u = User::whereEmail($result['email'])->first();
                if(empty($u->id)) {
                    $validatedData['user_type']       = 'users';
                    $validatedData['activation_code'] = str_random(30).time();
                    $validatedData['user_email_verified'] = 1;
                    $validatedData['user_fname'] = 'New';
                    $validatedData['user_lname'] = 'User';
                    $validatedData['email'] = $result['email'];
                    $validatedData['user_status'] = 1;
                    $validatedData['vid'] = 0;
                    app(User::class)->create($validatedData);
                    $u = User::whereEmail($result['email'])->first();
                } 
                Auth::login($u);

                $mapped_app_id = DB::table('mapped_apps')->where('user_id',Auth::user()->id)->where('fk_app_id',3)->value('mapped_app_id');
                if(empty($mapped_app_id)){
                    DB::table('mapped_apps')->insert(['fk_app_id'=> 3,'user_id'=> Auth::user()->id]);
                }

                $scopes = @serialize($user->getScopes());
                  // update
                    $users = User::find(Auth::user()->id);
                    $users->portal_id = $result['portal_id'];
                    $users->hub_email = $user->getEmail();
                    $users->scopes =  $scopes;
                    $users->app_id = @$user->getAppId();
                    $users->refresh_token = @$token->getRefreshToken();
                    $users->expires_at = @$token->getExpires();
                    $users->code = $_GET['code'];
                    $users->auth_token = $token->getToken();
                    $users->save();
                  // update
                
                // redirect to hubspot 
                $hubspotUrl = 'https://app.hubspot.com/';
                if(!empty($result['portal_id']) && isset($result['portal_id'])){
                    $hubspotUrl = 'https://app.hubspot.com/home?portalID='.$result['portal_id'];
                }
                header('Location: '.$hubspotUrl);
                exit;
                // redirect to hubspot 

                // $args['data'] = '{
                //       "properties": [
                //         {
                //           "property": "portal_id",
                //           "value": "'.$result['portal_id'].'"
                //         },
                //         {
                //           "property": "hub_email",
                //           "value": "'.$result['email'].'"
                //         },
                //         {
                //           "property": "refresh_token",
                //           "value": "'.$result['refresh_token'].'"
                //         },
                //         {
                //           "property": "token",
                //           "value": "'.$result['auth_token'].'"
                //         },
                //         {
                //           "property": "code",
                //           "value": "'.$result['code'].'"
                //         },
                //         {
                //           "property": "app_id",
                //           "value": "'.$result['app_id'].'"
                //         }
                //       ]
                //     }';
                // ;
                // $args['url'] = 'https://api.hubapi.com/contacts/v1/contact/email/'.$result['email'].'/profile?hapikey='.env('HUBSPOT_API_KEY');
                // $store = $this->curlAccess( 'POST', $args );                
                // Use these details to create a new profile

            } catch (Exception $e) {
                // Failed to get user details
                exit('Oh dear...');
            }
            // Use this to interact with an API on the users behalf
            return redirect('home');
        }
    }
    
    public function product_details($product_id, $hapi_key = '', $razor_key) {
        
        if(!empty($hapi_key) && !empty($product_id) && !empty($razor_key)) {
            $hapi_key = Crypt::decrypt($hapi_key);
            $product_id = Crypt::decrypt($product_id);
            $razor_key = Crypt::decrypt($razor_key);

            $args['url'] = 'https://api.hubapi.com/crm-objects/v1/objects/products/'. $product_id .'?properties=name&properties=description&properties=price&hapikey='.$hapi_key;
            $results = $this->curlAccessH( 'GET' , $args );
            $data = [];
            $next = '';
            $prev = '';
            $v = json_decode(json_encode($results), true);
            $product_name = $v['properties']['name']['value'];
            $product_price = $v['properties']['price']['value'];
            return view('button.cart-button',compact(['product_id','hapi_key','product_price','product_name','razor_key']));

        } else {
            echo 'Hubspot key is required.';
        }
        return view('product-details');

    }
    public function fetch_view(){
        Auth::logout();
        if(!empty($_GET['userEmail'])){
            $id = User::whereEmail($_GET['userEmail'])->value('id');
            if(!empty($id)) {
                $auth_token = User::whereEmail($_GET['userEmail'])->value('auth_token');
                if(!empty($auth_token)) {
                    
                    $results = array("results" => array(),
                                "primaryAction"=> array(
                                    "type"=> "IFRAME",
                                    "width"=> 890,
                                    "height"=> 748,
                                    "uri"=> url("ecommerce/settings-hub?vid=" . @$_GET['associatedObjectId'] . "&email=" .@$_GET['userEmail'] . "&phone=" . @$_GET['phone']. "&auth_id=" . @$id),
                                    "label"=> "Settings"
                                )
                    );  
                    
                } else {
                    $results = array("results" => array( 0 => array(
                                    "objectId"=> -1,
                                    "title" => "Please login and authorize your account with hubspot",
                                    "link" => env('APP_URL'),
                                    "priority" => "HIGH",
                                    "project" => "API"
                                )
                            )
                        );
                }

            } else {
                $results = array("results" => array( 0 => array(
                            "objectId"=> -1,
                            "title" => "You have to register your account.",
                            "link" => url('/'),
                            "priority" => env('APP_URL'),
                            "project" => "API"
                        )
                    )
                );
            }

        } else {
            $results = array("results" => array( 0 => array(
                        "objectId"=> -1,
                        "title" => "something were wrong.",
                        "link" => env('APP_URL'),
                        "priority" => "HIGH",
                        "project" => "API"
                    )
                )
            );
        }
        return json_encode($results);
    }
    public function refresh_token(){

        $refresh_token = Auth::user()->refresh_token;
        $tokens = "";
        if($refresh_token){            
            if(!empty($refresh_token)){
                
                //if(time() > Auth::user()->expires_at) {
                    
                    $curl = curl_init();
                    curl_setopt_array($curl, array(
                        CURLOPT_URL => 'https://api.hubapi.com/oauth/v1/token',
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => "",
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 30,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => 'POST',
                        CURLOPT_POSTFIELDS => 'grant_type=refresh_token&client_id='.env('HUBSPOT_CLIENT_ID').'&client_secret='.env('HUBSPOT_CLIENT_SECRET').'&refresh_token='.$refresh_token,
                        //CURLOPT_POSTFIELDS => 'grant_type=refresh_token&client_id='.env('HUBSPOT_CLIENT_ID').'&client_secret='.env('HUBSPOT_CLIENT_SECRET').'&scopes=e-commercere&fresh_token='.$refresh_token,
                        CURLOPT_SSL_VERIFYHOST => 0,
                        CURLOPT_SSL_VERIFYPEER => 0,
                        CURLOPT_HTTPHEADER => array(
                            //"authkey: ".Auth::user()->auth_token,
                            "content-type: application/x-www-form-urlencoded;charset=utf-8"
                        ),
                    ));

                    $result = curl_exec($curl);
                    $err = curl_error($curl);
                    curl_close($curl);
                    if ($err) {
                      echo "cURL Error #:" . $err;
                    }
                    $result = json_decode($result);
                    if(isset($result->access_token)){
                        $tokens = $result->access_token;
                        $refresh_token = $result->refresh_token;
                        
                        // updated
                            $users = User::find(Auth::user()->id);
                            $users->expires_at = time() + $result->expires_in * 0.95;
                            $users->refresh_token = $refresh_token;
                            $users->auth_token = $tokens;
                            //$users->save();
                        // refresh session
                    }
                    
                //}
            }
        } 
        
        return $tokens;
    }
    /**
     * Hubspot Integration
     */
    public function GET_settings_hub(Request $request)
    {
        if(!empty($_GET)) {
            $response = $_GET;
            User::whereId($_GET['auth_id'])->update(['object_data'=> serialize($response)]);
            $user = User::where('id', $_GET['auth_id'])->first();
            Auth::login($user);
        } 
        $data = Credential::whereUserId(Auth::user()->id)->where('type','ecommerce')->value('data');
        
        $render = 'render.ecommerce.settings';
        $data = unserialize($data);
        $html = view($render, compact('data'));
        return view('ecommerce.settings-hub',compact('data','html'));
        
    }
    
    public function POST_settings_hub(Request $request)
    {   
        $array['validation'] =  [
                        'stripe_sk_key' => 'required|min:25|max:200',
                        'stripe_pk_key' => 'required|min:25|max:200',
                        'paypal_key' => 'required|min:2|max:200',
                        'razor_key' => 'required|min:20|max:200',
                        'hapi_key' => 'required|min:30|max:200',
                    ];
        /* call function*/
        $v = \Validator::make($request->all(), $array['validation']);
        $u = User::whereId($request->auth_id)->first();
        Auth::login($u);
        if ($v->fails()) {
            $errors = $v->errors();
            $data = Credential::whereUserId(Auth::user()->id)
                                ->whereType('ecommerce')
                                ->value('data');
            $data = unserialize($data);
            $html = view('render.ecommerce.settings', compact('data'));
            return view('ecommerce.settings',compact('errors','data','html'));
        } 
        try {
            
            $res = $request->all();
            unset($res['_token']);
            unset($res['auth_id']);
            $id = Credential::whereUserId(Auth::user()->id)
                                ->whereType('ecommerce')
                                ->value('id');
            $data = serialize($res);
            if(empty($id)){
                Credential::create([
                                    'data'=> $data,
                                    'type'=> 'ecommerce',
                                    'fk_app_id'=> 3,
                                    'user_id' => Auth::user()->id,
                                ]);
                $alert = 'success';
                $message = 'Data has been saved.';
            } else {
                Credential::where('id',$id)->update([
                                                    'data' => $data,
                                                    'type'=> 'ecommerce',
                                                    'fk_app_id'=> 3
                                                ]);
                $alert = 'success';
                $message = 'Data has been updated.';
            }
        } catch (Exception $e) {
            $alert = 'warning';
            $message = 'Something went wrong in code.';
        }
        
        $data = Credential::whereUserId(Auth::user()->id)
                                ->whereType('ecommerce')
                                ->value('data');
        $data = unserialize($data);
        $html = view('render.ecommerce.settings', compact('data'));
        
        return view('ecommerce.settings-hub',compact('alert','message','data','html'));
    } 

    private function curlAccess($method, $array, $content_type = 'array' ) 
    {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $array['url']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_ENCODING, "");
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);        
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        switch ($method){
          case "POST":
            curl_setopt($ch, CURLOPT_POST, 1);        
             if ($array['data'])
                curl_setopt($ch, CURLOPT_POSTFIELDS, $array['data']);
             break;
          case "PUT":
             curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
             if ($array['data'])
                curl_setopt($ch, CURLOPT_POSTFIELDS, $array['data']);
             break;
          default:
             curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        }

        curl_setopt($ch, CURLOPT_HTTPHEADER, $array['headers']);
        $result = curl_exec($ch);
        
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);

        if($content_type == 'array'){
            $result = json_decode($result); 
        }  
        return $result;

    }
    private function curlAccessH($method, $array, $content_type = 'array' ) 
    {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $array['url']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_ENCODING, "");
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);        
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        switch ($method){
          case "POST":
            curl_setopt($ch, CURLOPT_POST, 1);        
             if ($array['data'])
                curl_setopt($ch, CURLOPT_POSTFIELDS, $array['data']);
             break;
          case "PUT":
             curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
             if ($array['data'])
                curl_setopt($ch, CURLOPT_POSTFIELDS, $array['data']);
             break;
          default:
             curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        }

        $headers = array();
        $headers[] = 'Content-Type: application/json';
        if(!empty($array['token'])){
            $headers[] = 'authorization: Bearer '. $array['token'];  
        }

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $result = curl_exec($ch);
        
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);

        if($content_type == 'array'){
            $result = json_decode($result); 
        }  
        return $result;

    }
    private function curlAccessG($method, $array, $content_type = 'array' ) 
    {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $array['url']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_ENCODING, "");
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);        
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        switch ($method){
          case "POST":
            curl_setopt($ch, CURLOPT_POST, 1);        
             if ($array['data'])
                curl_setopt($ch, CURLOPT_POSTFIELDS, $array['data']);
             break;
          case "PUT":
             curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
             if ($array['data'])
                curl_setopt($ch, CURLOPT_POSTFIELDS, $array['data']);
             break;
          default:
             curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        }

        $headers = array();
        $headers[] = 'Content-Type: application/json';

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $result = curl_exec($ch);
        
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);

        if($content_type == 'array'){
            $result = json_decode($result); 
        }  
        return $result;

    }
    public function settings()
    {
        $data = Credential::whereUserId(Auth::user()->id)
                            ->whereType('ecommerce')
                            ->value('data');
        $data = unserialize($data);
        $html = view('render.ecommerce.settings', compact('data'));
        return view('ecommerce.settings',compact('data','html'));
    }
    public function POST_settings(Request $request)
    {   
        $array['validation'] =  [
                                    'stripe_sk_key' => 'required|min:25|max:200',
                                    'stripe_pk_key' => 'required|min:25|max:200',
                                    'paypal_key' => 'required|min:2|max:200',
                                    'razor_key' => 'required|min:20|max:200',
                                    'hapi_key' => 'required|min:30|max:200',
                                ];
        /* call function*/
        $v = \Validator::make($request->all(), $array['validation']);

        if ($v->fails()) {
            $errors = $v->errors();
            $data = Credential::whereUserId(Auth::user()->id)
                                ->whereType('ecommerce')
                                ->value('data');
            $data = unserialize($data);
            $html = view('render.ecommerce.settings', compact('data'));
            return view('ecommerce.settings',compact('errors','data','html'));
        } 
        try {
            
            $res = $request->all();
            unset($res['_token']);
            $id = Credential::whereUserId(Auth::user()->id)
                                ->whereType('ecommerce')
                                ->value('id');
            $data = serialize($res);
            if(empty($id)){
                Credential::create([
                                            'data'=> $data,
                                            'type' => 'ecommerce',
                                            'fk_app_id' => 3,
                                            'user_id' => Auth::user()->id,
                                    ]);
                $alert = 'success';
                $message = 'Data has been saved.';
            } else {
                Credential::where('id',$id)->update([
                                                            'data' => $data,
                                                            'type' => 'ecommerce',
                                                            'fk_app_id' => 3,
                                                        ]);
                $alert = 'success';
                $message = 'Data has been updated.';
            }
        } catch (Exception $e) {
            $alert = 'warning';
            $message = 'Something went wrong in code.';
        }
        
        $data = Credential::whereUserId(Auth::user()->id)
                                ->whereType('ecommerce')
                                ->value('data');
        $data = unserialize($data);
        $html = view('render.ecommerce.settings', compact('data'));
        
        return view('ecommerce.settings',compact('alert','message','data','html'));
    }    
    public function workflowSent(Request $request){
        $data = $request->all();
        
        $newData['portal_id'] = @$data['origin']['portalId'];
        $newData['actionDefinitionId'] = @$data['origin']['actionDefinitionId'];
        $newData['select_method'] = @$data['inputFields']['select_method'];
        $newData['phone_number'] = @$data['inputFields']['phone_number'];
        $newData['message'] = @$data['inputFields']['message'];

        $v = \Validator::make($newData, [
            'phone_number' => 'required',
            'message' => 'required',
            'select_method' => 'required',
        ]);

        if ($v->fails())
        {
            $errors = $v->errors();
            $workflows = DB::table('workflows')->insert(['data'=>'validation error.']);
            return ['status' => false];
        } else {
            $user = User::where('portal_id',$newData['portal_id'])->value('id');
            Auth::logout();
            if($user){
                $user = User::where('portal_id',$newData['portal_id'])->first();
                Auth::login($user);
                $recipients = $newData["phone_number"];
                $res = $this->validate_phone_number($recipients);
                if ($res['status'] == true) {
                    $workflows = DB::table('workflows')->insert(['data'=>serialize($newData)]);
                    $result = $this->messageAction($newData['message'],$res['phone'],$newData['select_method']);
                } else {
                    $workflows = DB::table('workflows')->insert(['data'=>'Phone number not valid.']);
                }
            } else {
                $workflows = DB::table('workflows')->insert(['data'=>'Portal Id not found.']);
            }
        }
    }
    // for creating defination
    public function definations() {

        // $test = DB::table('test')->get();
        // echo '<pre>';
        // foreach($test as $ke => $val){
        //     $te = unserialize($val->data);
        //     print_r($te);
        // }
        
        
        // die;

        
        
        // $curl = curl_init();
        // curl_setopt_array($curl, array(
        //     CURLOPT_URL => 'https://api.hubapi.com/automationextensions/v1/definitions/1568?hapikey=4eb79eb6-10be-45b0-8542-6cdfe68b7c02&applicationId=215155',
        //     CURLOPT_RETURNTRANSFER => true,
        //     CURLOPT_ENCODING => "",
        //     CURLOPT_MAXREDIRS => 10,
        //     CURLOPT_TIMEOUT => 30,
        //     CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        //     CURLOPT_CUSTOMREQUEST => "DELETE",
        //     CURLOPT_SSL_VERIFYHOST => 0,
        //     CURLOPT_SSL_VERIFYPEER => 0,
        //     CURLOPT_HTTPHEADER => array(
        //         "content-type: application/json"
        //     ),
        // ));

        // $result = curl_exec($curl);
        // $err = curl_error($curl);

        // curl_close($curl);

        // if ($err) {
        //   echo "cURL Error #:" . $err;
        // }

        // print_r($result);

        // die;
        $args['url'] = 'https://api.hubapi.com/automationextensions/v1/definitions?hapikey=4eb79eb6-10be-45b0-8542-6cdfe68b7c02&applicationId=215155';
        $args['data'] = '{
            "integrationAppId": "215155",
            "extensionName": "Send SMS Webdew",
            "webhookUrl": "https://phpstack-242348-1203567.cloudwaysapps.com/smshsapp/workflowSent",
            "dataUrl":"https://phpstack-242348-1203567.cloudwaysapps.com/smshsapp/workflowSent",
            "fieldMetadata": [
                {
                    "label": "Phone Number",
                    "key": "phone_number",
                    "fieldType": "TEXT",
                    "values": [
                        {
                            "type": "STATIC_VALUE",
                            "allowsMergeTags": true
                        }
                    ]
                },
                {
                    "key": "select_method",
                    "label": "Select SMS Method",
                    "fieldType": "SELECT",
                    "hidden": false,
                    "values": [
                        {
                            "type": "STATIC_VALUE",
                            "options": [
                                {
                                  "description": null,
                                  "value": "msgwow",
                                  "readOnly": null,
                                  "text": "MSGWOW",
                                  "displayOrder": 0,
                                  "hidden": false,
                                  "doubleData": null
                                },
                                {
                                  "description": null,
                                  "value": "msg91",
                                  "readOnly": null,
                                  "text": "MSG91",
                                  "displayOrder": 0,
                                  "hidden": false,
                                  "doubleData": null
                                },
                                {
                                    "description": null,
                                    "value": "twilio",
                                    "readOnly": null,
                                    "text": "TWILIO",
                                    "displayOrder": 0,
                                    "hidden": false,
                                    "doubleData": null
                                }
                            ]
                        }
                    ]
                },
                {
                    "label": "Message",
                    "key": "message",
                    "fieldType": "TEXTAREA",
                    "values": [
                        {
                            "type": "STATIC_VALUE",
                            "allowsMergeTags": true
                        }
                    ]
                }
            ]
        }';
        // $args['data'] = '{
        //     "integrationAppId": "215155",
        //     "extensionName": "Send SMS Webdew",
        //     "webhookUrl": "https://phpstack-242348-1203567.cloudwaysapps.com/smshsapp/workflowSent",
        //     "dataUrl":"https://phpstack-242348-1203567.cloudwaysapps.com/smshsapp/workflowSent",
        //     "fieldMetadata": [
        //         {
        //             "label": "Phone Number",
        //             "key": "phone_number",
        //             "fieldType": "TEXT",
        //             "values": [
        //                 {
        //                     "type": "STATIC_VALUE",
        //                     "allowsMergeTags": true
        //                 }
        //             ]
        //         },
        //         {
        //             "label": "Message",
        //             "key": "message",
        //             "fieldType": "TEXTAREA",
        //             "values": [
        //                 {
        //                     "type": "STATIC_VALUE",
        //                     "allowsMergeTags": true
        //                 }
        //             ]
        //         }
        //     ]
        // }';
        // $result = $this->curlAccessH('POST',$args);
        // echo '<pre>';
        // print_r($result);
        die;
    }

    // function call
    public function curlAccessWHMCS($method, $array, $content_type = 'array' ) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'http://phpstack-242348-1101472.cloudwaysapps.com/includes/api.php');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        switch ($method){
        case "POST":
            curl_setopt($ch, CURLOPT_POST, 1);
            if ($array['data'])
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query( $array['data'] ));
            break;
        case "PUT":
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
            if ($array['data'])
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query( $array['data'] ) );
            break;
        default:
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        }
        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);

        if($content_type == 'array'){
            $result = json_decode($result); 
        }  
        return $result;
    }

    public function curlAccessHS($method, $array, $content_type = 'array' ) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $array['url']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        switch ($method){
        case "POST":
            curl_setopt($ch, CURLOPT_POST, 1);
            if ($array['data'])
                curl_setopt($ch, CURLOPT_POSTFIELDS,  $array['data'] );
            break;
        case "PUT":
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
            if ($array['data'])
                curl_setopt($ch, CURLOPT_POSTFIELDS,  $array['data']  );
            break;
        default:
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        }

        $headers = array();
        $headers[] = 'Content-Type: application/json';        
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);

        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);

        if($content_type == 'array'){
            $result = json_decode($result); 
        }  
        return $result;
    }
    public function sync_data($auth_id) {
        $auth = User::whereId($auth_id)->first();
        $hapi_key = '';
        print_r("hello");
        if(!empty(DB::table('credentials')->where('type','ecommerce')->whereUserId($auth_id)->value('data'))) {
            //$credential_all = unserialize($auth->credentials->data);
            $credential_all = unserialize(DB::table('credentials')->where('type','ecommerce')->whereUserId($auth_id)->value('data'));
            $hapi_key = $credential_all['hapi_key'];
        }
        if(!empty($hapi_key)){
            Auth::login($auth);
            if($auth->id){
                $args['url'] = 'https://api.hubapi.com/automation/v3/workflows?hapikey='.$hapi_key;
                $result = $this->curlAccessH('GET', $args);
                return view('ecommerce.sync-data',compact('result'));
            }
        }
    }

    public function whmcs_ecommerce(){
        //224529
        //https://api.hubapi.com/integrations/v1/<application id>/timeline/event-types?hapikey=48711f76-ba59-4116-9a14-273895d6ceef&userId=9502143
        // $hapi_key = env('hapi_key');
        // $args['url'] = 'https://api.hubapi.com/extensions/ecomm/v2/settings?appId=224529&hapikey=48711f76-ba59-4116-9a14-273895d6ceef';
        // $args['data'] = '{
        //                     "enabled": true,
        //                     "webhookUri": null,
        //                     "mappings": {
        //                     "CONTACT": {
        //                         "properties": [
        //                             {
        //                                 "externalPropertyName": "firstname",
        //                                 "hubspotPropertyName": "firstname",
        //                                 "dataType": "STRING"
        //                             },
        //                             {
        //                                 "externalPropertyName": "familyname",
        //                                 "hubspotPropertyName": "lastname",
        //                                 "dataType": "STRING"
        //                             },
        //                             {
        //                                 "externalPropertyName": "customer_email",
        //                                 "hubspotPropertyName": "email",
        //                                 "dataType": "STRING"
        //                             },
        //                             {
        //                                 "externalPropertyName": "phone_number",
        //                                 "hubspotPropertyName": "mobilephone",
        //                                 "dataType": "STRING"
        //                             }
        //                         ]
        //                     },
        //                     "DEAL": {
        //                         "properties": [
        //                             {
        //                                 "externalPropertyName": "purchase_date",
        //                                 "hubspotPropertyName": "closedate",
        //                                 "dataType": "STRING"
        //                             },
        //                             {
        //                                 "externalPropertyName": "name",
        //                                 "hubspotPropertyName": "dealname",
        //                                 "dataType": "STRING"
        //                             },
        //                             {
        //                                 "externalPropertyName": "stage",
        //                                 "hubspotPropertyName": "dealstage",
        //                                 "dataType": "STRING"
        //                             },
        //                             {
        //                                 "externalPropertyName": "abandoned_cart_url",
        //                                 "hubspotPropertyName": "ip__ecomm_bride__abandoned_cart_url",
        //                                 "dataType": "STRING"
        //                             }
        //                         ]
        //                     },
        //                     "PRODUCT": {
        //                         "properties": [
        //                             {
        //                                 "externalPropertyName": "product_description",
        //                                 "hubspotPropertyName": "description",
        //                                 "dataType": "STRING"
        //                             },
        //                             {
        //                                 "externalPropertyName": "price",
        //                                 "hubspotPropertyName": "price",
        //                                 "dataType": "NUMBER"
        //                             },
        //                             {
        //                                 "externalPropertyName": "product_name",
        //                                 "hubspotPropertyName": "name",
        //                                 "dataType": "STRING"
        //                             }
        //                         ]
        //                     },
        //                     "LINE_ITEM": {
        //                         "properties": [
        //                             {
        //                                 "externalPropertyName": "tax_amount",
        //                                 "hubspotPropertyName": "tax",
        //                                 "dataType": "NUMBER"
        //                             },
        //                             {
        //                                 "externalPropertyName": "num_items",
        //                                 "hubspotPropertyName": "quantity",
        //                                 "dataType": "NUMBER"
        //                             },
        //                             {
        //                                 "externalPropertyName": "price",
        //                                 "hubspotPropertyName": "price",
        //                                 "dataType": "NUMBER"
        //                             },
        //                             {
        //                                 "externalPropertyName": "discount_amount",
        //                                 "hubspotPropertyName": "discount",
        //                                 "dataType": "NUMBER"
        //                             }
        //                         ]
        //                     }
        //                     }
        //                 }';
        // $result = $this->curlAccessHS('PUT', $args);
        //$this->refresh_token();

        

        // $args['url'] = 'https://api.hubapi.com/oauth/v1/access-tokens/'.Auth::user()->auth_token;
        // $result = $this->curlAccessH('GET', $args);
        // print_r($result);
        // die;

        $args['url'] = "https://api.hubapi.com/crm-objects/v1/objects/products/paged?hapikey=ebd3b34f-c079-42d6-9acd-ab262aa8594f&properties=name&properties=description&properties=embed_code";
        $result = $this->curlAccessHS('GET', $args);
        echo '<pre>';
        print_r($result);
        die;
        
        $args['url']  = 'https://api.hubapi.com/extensions/ecomm/v2/stores';
        $args['data'] = '{
                            "id"       : "ecommercebridge-test-store",
                            "label"    : "Ecommerce Bridge Test Store",
                            "adminUri" : "ecommercebridge-test-store.myshopify.com"
                        }';
        
        $args['token'] = Auth::user()->auth_token;
        $result = $this->curlAccessH('PUT', $args);
        print_r($result);
        die;
    }
 
    public function get_product_list_workflow($auth_id) {
        $users = User::where('id', $auth_id)->first();
        $hapi_key = '';
        if(!empty(DB::table('credentials')->where('type','ecommerce')->whereUserId($auth_id)->value('data'))){
            $credential_all = unserialize(DB::table('credentials')->where('type','ecommerce')->whereUserId($auth_id)->value('data'));
            $hapi_key = $credential_all['hapi_key'];
        }
        if(!empty($hapi_key)){
            Auth::login($users);
            $args['url'] = 'https://api.hubapi.com/automation/v3/workflows?hapikey='.$hapi_key;
            $url = url('get-product-list-hub').'/'.$auth_id;
            $args['data'] = '{
                            "name": "Workflow for product Sync",
                            "type": "DRIP_DELAY",
                            "onlyEnrollsManually": true,
                            "actions": [
                                {
                                    "type": "DELAY",
                                    "delayMillis": 360000
                                },
                                {
                                    "type": "WEBHOOK",
                                    "url": "'.$url.'",
                                    "method": "POST"
                                }
                            ]
                        }';

            $result = $this->curlAccessH('POST', $args);
            //return view('ecommerce.sync-data',compact('result'));
            return ['status'=> true, 'message' => 'Workflow has been created.'];
        } else {
            return ['status'=> false, 'message' => 'Hubspot key is required.'];
        }
    }
    public function get_product_list() {
        
        $users = User::where('id', Auth::user()->id)->first();
        $hapi_key = '';
        if(!empty(DB::table('credentials')->where('type','ecommerce')->whereUserId(Auth::user()->id)->value('data'))){
            $credential_all = unserialize(DB::table('credentials')->where('type','ecommerce')->whereUserId(Auth::user()->id)->value('data'));
            
            $hapi_key = $credential_all['hapi_key'];
            $razor_key = $credential_all['razor_key'];
            $paypal_key = $credential_all['paypal_key'];
            $stripe_sk_key = $credential_all['stripe_sk_key'];
            $stripe_pk_key = $credential_all['stripe_pk_key'];
        }
        if(!empty($hapi_key)){
            // Create product property
                $argsp['data'] = '{
                    "name": "embed_code",
                    "label": "Embed code",
                    "description": "Embed code",
                    "groupName": "productinformation",
                    "type": "string",
                    "fieldType": "text",
                    "formField": true,
                    "displayOrder": 6,
                    "options": [
                    ]
                }';
                $argsp['url'] = 'https://api.hubapi.com/properties/v1/products/properties?hapikey='.$hapi_key;
                $resp = $this->curlAccessG('POST',$argsp);
            // Create product property

            $absolute_url = DB::table('ecommerce_templates')->where('user_id', Auth::user()->id )->value('slug');
            if(empty($absolute_url)){
                $args['url'] = "https://api.hubapi.com/content/api/v2/pages?hapikey=".$hapi_key;
                $milliseconds = time() * 1000;
                $args['data'] = '{
                    "name": "Checkout Page",
                    "template_path" : "test-theme/templates/cart-design.html",
                    "publish_immediately" : 1,
                    "publish_date" : "'.$milliseconds.'",
                    "slug" : "checkout_web_page"
                }';
                $res = $this->curlAccessG('POST',$args);
                if(isset($res->status)){
                    echo 'Template is not able to create.';
                    die;
                } else {
                    $absolute_url = $res->absolute_url;
                    $page_id = $res->analytics_page_id;
                    $args['url'] = "https://api.hubapi.com/content/api/v2/pages/".$page_id."/publish-action?hapikey=".$hapi_key;
                    $args['data'] = '{"action":"schedule-publish"}';
                    $res = $this->curlAccessG('POST',$args);
                    DB::table('ecommerce_templates')->insert(['slug' => $absolute_url, 'page_id' => $page_id, 'user_id' => Auth::user()->id ]);
                }
            }

            $args['url'] = 'https://api.hubapi.com/crm/v3/objects/products?limit=10&hapikey='.$hapi_key;
            if(isset($_GET['offset'])){
                $args['url'] = 'https://api.hubapi.com/crm/v3/objects/products?limit=10&after=' . $_GET['offset'] . '&hapikey='.$hapi_key;
            }
            
            $results = $this->curlAccessH('GET',$args);
            $data = [];
            $next = '';
            $prev = '';
            if(!empty($results->results)){
                foreach($results->results as $key => $value) {
                    $v = json_decode(json_encode($value), true);
                    $data[$key] = $v ;
                    $product_id = $v['id'];
                    
                    //$embed_url = 'http://localhost/projects/laravel/HSAPP/hsEcommerceCartButton/product_details/'.Crypt::encrypt($product_id).'/'.Crypt::encrypt($hapi_key).'/'.Crypt::encrypt($razor_key);
                    $embed_url = $absolute_url.'?product_id='.$product_id;
                    $product_name = $v['properties']['name'];
                    $product_price = $v['properties']['price'];
                    $html = view('button.cart-button',compact(['product_id','hapi_key','product_price','product_name','razor_key']));
                   
                    $data[$key]['html'] = $html;
                    $args['url'] = "https://api.hubapi.com/crm-objects/v1/objects/products/". $product_id ."?hapikey=".$hapi_key;
                    $args['data'] = '[
                                        {
                                        "name": "embed_code",
                                        "value": "'.$embed_url.'"
                                        }
                                    ]';
                    $results = $this->curlAccessH('PUT',$args);
                }
                if(!empty($results->paging)){
                    $next = url('get-product-list') .'?offset='. $results->paging->next->after;
                }
            }
            return view('product-list-code',compact('data','next'));
        } else {
            echo 'Hubspot key is required.';
        }
    }
    public function get_product_list_hub($auth_id) {
      
        $users = User::where('id', $auth_id)->first();
        $hapi_key = '';
        if(!empty(DB::table('credentials')->where('type','ecommerce')->whereUserId($auth_id)->value('data'))){
  
            $credential_all = unserialize(DB::table('credentials')->where('type','ecommerce')->whereUserId($auth_id)->value('data'));
            $hapi_key = $credential_all['hapi_key'];
            $razor_key = $credential_all['razor_key'];
            $paypal_key = $credential_all['paypal_key'];
            $stripe_sk_key = $credential_all['stripe_sk_key'];
            $stripe_pk_key = $credential_all['stripe_pk_key'];
        }
       if(!empty($hapi_key) && !empty($razor_key)){

            // Create product property
            $argsp['data'] = '{
                                "name": "embed_code",
                                "label": "Embed code",
                                "description": "Embed code",
                                "groupName": "productinformation",
                                "type": "string",
                                "fieldType": "text",
                                "formField": true,
                                "displayOrder": 6,
                                "options": [
                                ]
                            },
                            {
                                "name": "full_embed_code",
                                "label": "Full Embed code",
                                "description": "Full Embed code",
                                "groupName": "productinformation",
                                "type": "string",
                                "fieldType": "text",
                                "formField": true,
                                "displayOrder": 6,
                                "options": [
                                ]
                            }';
            $argsp['url'] = 'https://api.hubapi.com/properties/v1/products/properties?hapikey='.$hapi_key;
            $resp = $this->curlAccessG('POST',$argsp);
           
           //this will add property if it's not there
                        $argsp['data'] = '
                            {
                                "name": "full_embed_code",
                                "label": "Full Embed code",
                                "description": "Full Embed code",
                                "groupName": "productinformation",
                                "type": "string",
                                "fieldType": "text",
                                "formField": true,
                                "displayOrder": 6,
                                "options": [
                                ]
                            }';
                $argsp['url'] = 'https://api.hubapi.com/properties/v1/products/properties?hapikey='.$hapi_key;
               $resp = $this->curlAccessG('POST',$argsp);



            // Template start
            $absolute_url = DB::table('ecommerce_templates')->where('user_id', $auth_id )->value('slug');
            if(empty($absolute_url)){
                $args['url'] = "https://api.hubapi.com/content/api/v2/pages?hapikey=".$hapi_key;
                $milliseconds = time() * 1000;
                $args['data'] = '{
                    "name": "Checkout Page",
                    "template_path" : "test-theme/templates/cart-design.html",
                    "publish_immediately" : 1,
                    "publish_date" : "'.$milliseconds.'",
                    "slug" : "checkout_web_page"
                }';
                $res = $this->curlAccessG('POST',$args);
                if(isset($res->status)){
                    echo 'Template is not able to create.';
                    die;
                } else {
                    $absolute_url = $res->absolute_url;
                    $page_id = $res->analytics_page_id;
                    $args['url'] = "https://api.hubapi.com/content/api/v2/pages/".$page_id."/publish-action?hapikey=".$hapi_key;
                    $args['data'] = '{"action":"schedule-publish"}';
                    $res = $this->curlAccessG('POST',$args);
                    DB::table('ecommerce_templates')->insert(['slug' => $absolute_url, 'page_id' => $page_id, 'user_id' => $auth_id ]);
                }

            }
            // Template start
            $args['url'] = 'https://api.hubapi.com/crm/v3/objects/products?limit=10&properties=price&properties=name&properties=description&properties=currency_code&properties=full_embed_code&hapikey='.$hapi_key;
            if(isset($_GET['offset'])){
                $args['url'] = 'https://api.hubapi.com/crm/v3/objects/products?limit=10&properties=price&properties=name&properties=description&properties=currency_code&properties=full_embed_code&after=' . $_GET['offset'] . '&hapikey='.$hapi_key;
            }
            $results = $this->curlAccessH('GET',$args);
            
           
           try{
              $array_result=array($results);
              $url=$array_result[0]->paging->next->link;
              $pagingurl=$url."&hapikey=$hapi_key";  //here we are saving the next page url code with embeding api key if its not available then it will raise exception and the code will go into catch statement.     

            $data = [];
            $next = '';
            $prev = '';
            $i = 1;
            $count=$array_result[0]->results;
            $len = count($count);//keeping count of result items i.e. products
        
            if(!empty($results->results)){
                foreach($results->results as $key => $value) {
                    if($i!=$len){

                    $v = json_decode(json_encode($value), true);
                    $data[$key] = $v;
                    $product_id = $v['id'];
                    
                        $embed_url = $absolute_url.'?product_id='.$product_id;
                        $full_embed_url = $absolute_url.'?product_id='.$product_id.'&hapi_key='.$hapi_key;
           
                        if(!empty($paypal_key)){
                            $full_embed_url .= '&paypal='.$paypal_key;
                        }
                        if(!empty($stripe_pk_key) && !empty($stripe_sk_key)){
                            $full_embed_url .= '&stripe_sk='.$stripe_sk_key;
                            $full_embed_url .= '&stripe_pk='.$stripe_pk_key;
                        }
                        $product_name = $v['properties']['name'];
                        $product_price = $v['properties']['price'];
                
                        $args['url'] = "https://api.hubapi.com/crm-objects/v1/objects/products/". $product_id ."?hapikey=".$hapi_key;
                        $args['data'] = '[
                                            {
                                                "name": "embed_code",
                                                "value": "'.$embed_url.'"
                                            },
                                            {
                                                "name": "full_embed_code",
                                                "value": "'.$full_embed_url.'"
                                            }
                                        ]';
                        $results = $this->curlAccessH('PUT',$args);
                    }
                    elseif($i==$len){//when last item comes..it should call the next page so in the end we have appended the function call
   
                    $v = json_decode(json_encode($value), true);
                    $data[$key] = $v;
                    $product_id = $v['id'];
                        $embed_url = $absolute_url.'?product_id='.$product_id;
                        $full_embed_url = $absolute_url.'?product_id='.$product_id.'&hapi_key='.$hapi_key;
               
                        if(!empty($paypal_key)){
                            $full_embed_url .= '&paypal='.$paypal_key;
                        }
                        if(!empty($stripe_pk_key) && !empty($stripe_sk_key)){
                            $full_embed_url .= '&stripe_sk='.$stripe_sk_key;
                            $full_embed_url .= '&stripe_pk='.$stripe_pk_key;
                        }
                        $product_name = $v['properties']['name'];
                        $product_price = $v['properties']['price'];
                        $args['url'] = "https://api.hubapi.com/crm-objects/v1/objects/products/". $product_id ."?hapikey=".$hapi_key;
                        $args['data'] = '[
                                            {
                                                "name": "embed_code",
                                                "value": "'.$embed_url.'"
                                            },
                                            {
                                                "name": "full_embed_code",
                                                "value": "'.$full_embed_url.'"
                                            }
                                        ]';
                        $results = $this->curlAccessH('PUT',$args);
                        $this->calling_object($pagingurl,$absolute_url,$hapi_key);//this function calls the next page 
                    }//else if ends
                  $i++;
                }//foreach ends
 
             }//outer if ends
          }//try ends
           catch(\throwable $err)//when there are less then 10 products then this catch code will get execute
            {
                 if(!empty($results->results)){
                foreach($results->results as $key => $value) {
                    $v = json_decode(json_encode($value), true);
                    $data[$key] = $v;
                    $product_id = $v['id'];
               
                        $embed_url = $absolute_url.'?product_id='.$product_id;
                        $full_embed_url = $absolute_url.'?product_id='.$product_id.'&hapi_key='.$hapi_key;
                
                        if(!empty($paypal_key)){
                            $full_embed_url .= '&paypal='.$paypal_key;
                        }
                        if(!empty($stripe_pk_key) && !empty($stripe_sk_key)){
                            $full_embed_url .= '&stripe_sk='.$stripe_sk_key;
                            $full_embed_url .= '&stripe_pk='.$stripe_pk_key;
                        }
                        $product_name = $v['properties']['name'];
                        $product_price = $v['properties']['price'];
   
                        $args['url'] = "https://api.hubapi.com/crm-objects/v1/objects/products/". $product_id ."?hapikey=".$hapi_key;
                        $args['data'] = '[
                                            {
                                                "name": "embed_code",
                                                "value": "'.$embed_url.'"
                                            },
                                            {
                                                "name": "full_embed_code",
                                                "value": "'.$full_embed_url.'"
                                            }
                                        ]';
                        $results = $this->curlAccessH('PUT',$args);
                   
                 
                 }//catch foreach ends

          }//catch if ends
      }//catch ends 

            return ['status'=> true, 'message' => 'Cart link has been set in product embed code property.'];
        } else {
            $message = 'Hubspot key is required.';
            return ['status'=> false, 'message' =>$message];
        }
    }

    // for creating defination
    public function terms_service() {
        return view('ecommerce.websites.terms-service');
    }
    // for creating defination
    public function policy() {
        return view('ecommerce.websites.policy');
    }

    public function documentation(){
        return view('ecommerce.websites.documentation');
    }

    public function tagline(){
        return view('ecommerce.websites.tagline');
    }
    public function home()
    {
        return view('ecommerce.websites.home');
    }
    
      public function ctabutton()
    {
        return view('render.ecommerce.cta');
    }
    public function calling_object($url,$absolute_url,$hapi_key)
    {   
        //this function will simply recives thhe url and call the next product page 
        $args['url']=$url;
        $results = $this->curlAccessH('GET',$args);
        $this->process_results($results,$absolute_url,$hapi_key);//after getting prodcuts we will pass it on to another function to process that page and see if there is another page left or not.
        
                   
        
    }
    
    public function process_results($results,$absolute_url,$hapi_key)
    {     
          $paging_url="";
          $array_result=array($results);
          
          try{
          $url=$array_result[0]->paging->next->link;//if there is no other page left then this will raise exception and code will go into catch command
           
          $paging_url=$url."&hapikey=$hapi_key";//else it will store the next page url and we will make the call to calling_object function  
          $i = 1;
          $count=$array_result[0]->results;
          $len = count($count);//keeps count of products in the result
          if(!empty($results->results)){
                foreach($results->results as $key => $value) {
                   
                    if($i!=$len){
                        $v = json_decode(json_encode($value), true);
                    $data[$key] = $v;
                    $product_id = $v['id'];

                        $embed_url = $absolute_url.'?product_id='.$product_id;
                        $full_embed_url = $absolute_url.'?product_id='.$product_id.'&hapi_key='.$hapi_key;
             
                        if(!empty($paypal_key)){
                            $full_embed_url .= '&paypal='.$paypal_key;
                        }
                        if(!empty($stripe_pk_key) && !empty($stripe_sk_key)){
                            $full_embed_url .= '&stripe_sk='.$stripe_sk_key;
                            $full_embed_url .= '&stripe_pk='.$stripe_pk_key;
                        }
                        $product_name = $v['properties']['name'];
                        $product_price = $v['properties']['price'];
                        $args['url'] = "https://api.hubapi.com/crm-objects/v1/objects/products/". $product_id ."?hapikey=".$hapi_key;
                        $args['data'] = '[
                                            {
                                                "name": "embed_code",
                                                "value": "'.$embed_url.'"
                                            },
                                            {
                                                "name": "full_embed_code",
                                                "value": "'.$full_embed_url.'"
                                            }
                                        ]';
                        $results = $this->curlAccessH('PUT',$args);
                    
                  }
           else{
             $v = json_decode(json_encode($value), true);
                    $data[$key] = $v;
                    $product_id = $v['id'];
            
                        $embed_url = $absolute_url.'?product_id='.$product_id;
                        $full_embed_url = $absolute_url.'?product_id='.$product_id.'&hapi_key='.$hapi_key;
                      
                        if(!empty($paypal_key)){
                            $full_embed_url .= '&paypal='.$paypal_key;
                        }
                        if(!empty($stripe_pk_key) && !empty($stripe_sk_key)){
                            $full_embed_url .= '&stripe_sk='.$stripe_sk_key;
                            $full_embed_url .= '&stripe_pk='.$stripe_pk_key;
                        }
                        $product_name = $v['properties']['name'];
                        $product_price = $v['properties']['price'];
                       
                        $args['url'] = "https://api.hubapi.com/crm-objects/v1/objects/products/". $product_id ."?hapikey=".$hapi_key;
                        $args['data'] = '[
                                            {
                                                "name": "embed_code",
                                                "value": "'.$embed_url.'"
                                            },
                                            {
                                                "name": "full_embed_code",
                                                "value": "'.$full_embed_url.'"
                                            }
                                        ]';
                        $results = $this->curlAccessH('PUT',$args);
                           
                            if($url!=""){   
                            $this->calling_object($paging_url,$absolute_url,$hapi_key);    
                            }
                            else{
                                return ['status'=> true, 'message' => 'Cart link has been set in product embed code property.'];
                            }
                        
                    }//else ends
                    $i++;
              }//foreach ends
           }//outer if ends
       }//try ends
        catch(\Throwable $e){
        
                if(!empty($results->results)){
                foreach($results->results as $key => $value) {
                    $v = json_decode(json_encode($value), true);
                    $data[$key] = $v;
                    $product_id = $v['id'];
                    

                 
                        $embed_url = $absolute_url.'?product_id='.$product_id;
                        $full_embed_url = $absolute_url.'?product_id='.$product_id.'&hapi_key='.$hapi_key;
                      
                        if(!empty($paypal_key)){
                            $full_embed_url .= '&paypal='.$paypal_key;
                        }
                        if(!empty($stripe_pk_key) && !empty($stripe_sk_key)){
                            $full_embed_url .= '&stripe_sk='.$stripe_sk_key;
                            $full_embed_url .= '&stripe_pk='.$stripe_pk_key;
                        }
                        $product_name = $v['properties']['name'];
                        $product_price = $v['properties']['price'];
                     
                        $args['url'] = "https://api.hubapi.com/crm-objects/v1/objects/products/". $product_id ."?hapikey=".$hapi_key;
                        $args['data'] = '[
                                            {
                                                "name": "embed_code",
                                                "value": "'.$embed_url.'"
                                            },
                                            {
                                                "name": "full_embed_code",
                                                "value": "'.$full_embed_url.'"
                                            }
                                        ]';
                        $results = $this->curlAccessH('PUT',$args);
                       } //foreach end     
                  }//if end 
                   return ['status'=> true, 'message' => 'Cart link has been set in product embed code property.'];

             }//catch end
      }//function end
}//class end
    
