<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Support\Facades\Validator;
use Auth;
use Crypt;
use Redirect;
use Hash;
use DB;
use App\User;
use Session;
use Twilio\Rest\Client;

class LoginController extends Controller
{
    public function validateURL($app){
        if($app != 'smsdew' && $app != 'whmcs'){
            abort(404);
        } 
    }
    public function login($app){
        $this->validateURL($app);
        return view('/auth/login-auth',compact('app'));
    }
    public function login_redirect_sms($app = 'smsdew'){
        return redirect($app.'/login');
    }
    public function login_redirect($app){
        $this->validateURL($app);
        return redirect($app.'/login');
    }
    public function logout($app){
        Auth::logout();
        return redirect($app.'/login');   
    }
}