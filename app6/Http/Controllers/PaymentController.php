<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Support\Facades\Validator;
use Auth;
use Crypt;
use Redirect;
use Hash;
use DB;
use App\User;
use App\Credential;
use Stripe\Error\Card;
use Stripe;
use Session;

class PaymentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * handling payment with POST
     */
    public function handlePost(Request $request)
    {
     
        try {


            $app_type = $request->type;
            $app_id = $request->app_id;
            $amount = DB::table('apps')->where('id',$app_id)->value('amount');
            $app_name = DB::table('apps')->where('id',$app_id)->value('name');
            if($amount > 0) {
                Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
                $response= Stripe\Charge::create ([
                    "amount" => $amount * 100,
                    "currency" => "usd",
                    "source" => $request->stripeToken,
                    "description" => $app_name. " Plugin Charges" 
                ]);
             
                $status = $response->status;
                if($status == 'succeeded') {
             
                    $id = $response->id;
                    $amount = $response->amount / 100;
                    $currency = $response->currency;
                    $customer_name = $request->request->get('cardholder');
                    $status = $response->status;
                    $email  = $response->billing_details->name;
                    $res = json_encode($response);
                    $payment_status = $response->paid;
                    $receipt_url = @$response->receipt_url;

                    $auth_email = Auth::user()->email;

                    $payment_id = DB::table('payments')->insertGetId([
                                                            'app_id' => $app_id,
                                                            'app_type' => $app_type,
                                                            'stripe_id' => $id,
                                                            'user_id' => Auth::user()->id,
                                                            'amount' => $amount,
                                                            'currency_code' => $currency,
                                                            'status' => $status,
                                                            'all_response' => serialize($res) ,
                                                            'receipt_url' => $receipt_url
                                                    ]);


                    // check subscription 

                        $args['url'] = 'https://api.hubapi.com/crm/v3/objects/subscriptions/search?hapikey='.env('HUBSPOT_API_KEY');
                        $args['data'] =  json_encode([
                                            "filterGroups" => [
                                              [
                                                "filters" => [
                                                  [
                                                    "propertyName" => "email",
                                                    "operator" => "EQ",
                                                    "value" => Auth::user()->email
                                                  ]
                                                ]
                                              ]
                                            ]
                                          ]);

                        $result = curlAccess('POST',$args);
                    
                    if(!isset($result->status)){
                        
                        // insert subscription
                        $args['url'] = 'https://api.hubapi.com/crm/v3/objects/'.env('subscription_key').'/?hapikey='.env('HUBSPOT_API_KEY');
                        
                        if(!empty(Auth::user()->firstname) || !empty(Auth::user()->lastname)){
                            $fullname = @Auth::user()->firstname .' '.Auth::user()->lastname;
                        } else {
                            $fullname = @Auth::user()->email;
                        }
                        // if(env('APP_ENV') == 'live'){
                        //     if($app_id == 1){
                        //         $app_val = 'All in one SMS App';
                        //     } else if($app_id == 2){
                        //         $app_val = 'WHMCS HubSpot App';
                        //     } else if($app_id == 3){
                        //         $app_val = 'HubSpot eCommerce App';
                        //     }
                        //     $args['data']= json_encode([
                        //           "properties" => [
                        //               "email" => Auth::user()->email,
                        //               "name" => $fullname,
                        //               "app_type" => $app_val,
                        //               "subscription_id" => $payment_id
                        //           ]
                        //     ]);
                        //     // live
                        // } else {
                        //stage
                            $args['data']= json_encode([
                                  "properties" => [
                                      "email" => Auth::user()->email,
                                      "name" => $fullname,
                                      "app_type" => $app_id,
                                      "amount" => $amount,
                                      "subscription_id" => $payment_id
                                  ]
                            ]);
                        
                        //}
                        $result = curlAccess('POST',$args);

                        DB::table('payments')->where('id',$payment_id)->update(['subscription_id' => $result->id ]);
                        // insert subscription
                    }
                    // check subscription

                    // search contact
                    $args['url'] = 'https://api.hubapi.com/crm/v3/objects/contacts/search?hapikey='.env('HUBSPOT_API_KEY');
                    $args['data'] =  json_encode([
                                        "filterGroups"=>[
                                          [
                                            "filters"=>[
                                              [
                                                "propertyName" => "email",
                                                "operator" => "EQ",
                                                "value" => Auth::user()->email
                                              ]
                                            ]
                                          ]
                                        ]
                                      ]);

                    $result = curlAccess('POST',$args);
                    // search contact
                    $vid = 0;
                    if(!isset($result->status)){
                        if($result->total > 0){
                            $vid = $result->results[0]->id;
                        } else {
                            // insert contacts
                            $args['url'] = 'https://api.hubapi.com/contacts/v1/contact/?hapikey='.env('HUBSPOT_API_KEY');
                            
                            $args['data']= json_encode([
                                  "properties" => [
                                    [
                                      "property" => "email",
                                      "value" => Auth::user()->email
                                    ],
                                    [
                                      "property" => "firstname",
                                      "value" => Auth::user()->firstname
                                    ],
                                    [
                                      "property" => "lastname",
                                      "value" => Auth::user()->lastname
                                    ]
                                  ]
                            ]);
                       
                            $result  = curlAccess('POST',$args);
                            if(!isset($result->status)){
                                $vid = $result->vid;
                            }
                            // insert contact
                        }

                        // // create deals
                        // $timestamp = time() *1000;
              
                        // $args['url'] = 'https://api.hubapi.com/deals/v1/deal?hapikey='.env('HUBSPOT_API_KEY');
                        
                        // $args['data']= '{
                        //       "associations": {
                        //         "associatedCompanyIds": [],
                        //         "associatedVids": [
                        //           '.$vid.'
                        //         ]
                        //       },
                        //       "properties": [
                        //         {
                        //           "value": "'.$app_name.' Deal",
                        //           "name": "dealname"
                        //         },
                        //         {
                        //           "value": "appointmentscheduled",
                        //           "name": "dealstage"
                        //         },
                        //         {
                        //           "value": "default",
                        //           "name": "pipeline"
                        //         },
                        //         {
                        //           "value": "51771313",
                        //           "name": "hubspot_owner_id"
                        //         },
                        //         {
                        //           "value": '.$timestamp.',
                        //           "name": "closedate"
                        //         },
                        //         {
                        //           "value": "'.$amount.'",
                        //           "name": "amount"
                        //         },
                        //         {
                        //           "value": "newbusiness",
                        //           "name": "dealtype"
                        //         }
                        //       ]
                        //     }';
                   
                        // curlAccess('POST',$args);
                        // create deals
                        Session::flash('success', 'Payment has been successfully processed.');
                        return Redirect::to($app_type."/auth2-callback");

                    }
                    
                    
                } else {
                    Session::flash('transaction-failed','transaction failed.please try again.');
                    return view('all-app',compact("posts")); 
                }
            } else {
                DB::table('payments')->insert([
                                                'app_id' => $app_id,
                                                'app_type' => $app_type,
                                                'stripe_id' => 0,
                                                'user_id' => Auth::user()->id,
                                                'amount' => $amount,
                                                'currency_code' => 'usd',
                                                'status' => 'succeeded',
                                                'all_response' => NULL,
                                                'receipt_url' => NULL
                                        ]);
            }
            return back();
        } catch(\throwable $e) {
            echo 'Message: ' .$e->getMessage();
        }
    }
    

}
