<?php
namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\DB;
use App\SearchRecord;
use App\RestClient;
use App\MstDomainUrl;
use App\MstSeoData;
use App\CnfDomainCountry;
use App\MstCountry;
use App\MstOrganicSearch;
use App\MstOrgExtraField;
use App\MstSimilarSite;
use App\MstDaPa;
use App\User;
use App\Chat;

class APIController extends Controller
{
    

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }
      
    public function checkContact($email, $args = [])
    {
        $args['url'] = "https://api.hubapi.com/contacts/v1/contact/email/".$email."/profile?hapikey=" . $args['hapi_key'];
        $result = curlAccess( 'GET', $args );
        $res = ['status' => 1,'result' => $result];
        if(isset($result->status)) {
            if($result->status == 'error'){
                $res = ['status' => 0,'result' => $result];
            }
        } else {
            $res = ['status' => 1,'result' => $result];
        }
        return $res;
    }

}
