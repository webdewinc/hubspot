<?php
namespace App\Http\Controllers\SMS;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Support\Facades\Validator;
use Auth;
use Crypt;
use Redirect;
use Hash;
use DB;
use App\User;
use Session;
use Twilio\Rest\Client;
use App\Credential;
use App\SmsSend;
use Illuminate\Support\Facades\URL;

use Stripe\Error\Card;
use Stripe;

class SMSController extends Controller
{

    public function __construct()
    {
        //$this->middleware('auth');
        
    }

    public function fetch_view()
    {

        Auth::logout();
        if (!empty($_GET['userEmail']))
        {

            $id = User::whereEmail($_GET['userEmail'])->value('id');

            if (!empty($id))
            {
                $auth_token = User::whereEmail($_GET['userEmail'])->value('auth_token');

                if (!empty($auth_token))
                {
                    if (!empty($_GET['phone']) || !empty($_GET['mobilephone']))
                    {
                        if (empty($_GET['mobilephone'])) {
                            $_GET['phone'] = $_GET['phone'];
                        } else {
                            $_GET['phone'] = $_GET['mobilephone'];
                        }
                        // $user = User::whereEmail($_GET['userEmail'])->first();
                        // Auth::login($user);
                        $pay_id = DB::table('payments')->where('user_id',$id)->where('app_id',1)->value('id');
                        if($pay_id > 0){
                            $results = array(
                                "results" => array() ,
                                "primaryAction" => array(
                                    "type" => "IFRAME",
                                    "width" => 890,
                                    "height" => 748,
                                    "uri" => url("smsdew/send-sms-app/" . @$id . "?vid=" . @$_GET['associatedObjectId'] . "&email=" . @$_GET['userEmail'] . "&phone=" . @$_GET['phone'] . "&auth_id=" . @$id) ,
                                    "label" => "Text SMS"
                                )
                            );
                        } else {
                            $results = array(
                                "results" => array(
                                    0 => array(
                                        "objectId" => - 1,
                                        "title" => "Pay for this app",
                                        "link" => url("all-in-one-sms"),
                                        "priority" => "HIGH",
                                        "project" => "API"
                                    )
                                )
                            );
                        }

                    }
                    else
                    {
                        $results = array(
                            "results" => array(
                                0 => array(
                                    "objectId" => - 1,
                                    "title" => "Phone or mobile number is required.",
                                    "priority" => "HIGH",
                                    "project" => "API"
                                )
                            )
                        );
                    }
                }
                else
                {
                    $results = array(
                        "results" => array(
                            0 => array(
                                "objectId" => - 1,
                                "title" => "Please login and authorize your account with hubspot",
                                "link" => env('APP_URL') ,
                                "priority" => "HIGH",
                                "project" => "API"
                            )
                        )
                    );
                }

            }
            else
            {
                $results = array(
                    "results" => array(
                        0 => array(
                            "objectId" => - 1,
                            "title" => "You have to register your account.",
                            "link" => url('/') ,
                            "priority" => env('APP_URL') ,
                            "project" => "API"
                        )
                    )
                );
            }

        }
        else
        {
            $results = array(
                "results" => array(
                    0 => array(
                        "objectId" => - 1,
                        "title" => "something were wrong.",
                        "link" => env('APP_URL') ,
                        "priority" => "HIGH",
                        "project" => "API"
                    )
                )
            );
        }
        return json_encode($results);
    }

    public function refresh_token()
    {

        $refresh_token = Auth::user()->refresh_token;

        if ($refresh_token)
        {

            if (!empty($refresh_token))
            {

                if (time() > Auth::user()->expires_at)
                {

                    $curl = curl_init();
                    curl_setopt_array($curl, array(
                        CURLOPT_URL => 'https://api.hubapi.com/oauth/v1/token',
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => "",
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 30,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => 'POST',
                        CURLOPT_POSTFIELDS => 'grant_type=refresh_token&client_id=' . $clientid . '&client_secret=' . $clientSecret . '&refresh_token=' . $refresh_token,
                        CURLOPT_SSL_VERIFYHOST => 0,
                        CURLOPT_SSL_VERIFYPEER => 0,
                        CURLOPT_HTTPHEADER => array(
                            "authkey: " . Auth::user()->auth_token,
                            "content-type: application/x-www-form-urlencoded;charset=utf-8"
                        ) ,
                    ));

                    $result = curl_exec($curl);
                    $err = curl_error($curl);
                    curl_close($curl);
                    if ($err)
                    {
                        echo "cURL Error #:" . $err;
                    }
                    $result = json_decode($result);

                    //                    print_r($result);
                    //                    die;
                    // updated
                    $users = User::find(Auth::user()->id);
                    $users->expires_at = time() + $result->expires_in * 0.95;
                    $users->auth_token = $tokens;
                    // refresh session
                    
                }
            }
        }
    }
    /**
     * Hubspot Integration
     */

    public function GET_send_sms($auth_id)
    {

        if (!empty($_GET))
        {
            $response = $_GET;
            User::whereId($_GET['auth_id'])->update(['object_data' => serialize($response) ]);
            $user = User::where('id', $_GET['auth_id'])->first();
            Auth::login($user);
        }
        else
        {
            $this->auth_access($auth_id);
        }

        $type = @Auth::user()->sms_credentials_default_type;

        if (!empty($type))
        {
            $data = Credential::whereUserId(Auth::user()->id)
                ->where('type', Auth::user()
                ->sms_credentials_default_type)
                ->value('data');
        }

        $render = 'render.smsdew.' . $type;
        $data = unserialize($data);
        if (!empty($data))
        {
            return view('smsdew/send-sms');
        }
        else
        {
            $html = view($render, compact('data', 'type'));
            return view('smsdew.settings', compact('data', 'html'));
        }
    }
    /**
     * Hubspot Integration
     */
    public function GET_send_sms_app($auth_id)
    {

        if (!empty($_GET))
        {
            $response = $_GET;
            User::whereId($_GET['auth_id'])->update(['object_data' => serialize($response) ]);
            $user = User::where('id', $_GET['auth_id'])->first();
            Auth::login($user);
        }
        else
        {
            $this->auth_access($auth_id);
        }

        $type = @Auth::user()->sms_credentials_default_type;

        if (!empty($type))
        {
            $data = Credential::whereUserId(Auth::user()->id)
                ->where('type', Auth::user()
                ->sms_credentials_default_type)
                ->value('data');

        }
        $render = 'render.smsdew.' . $type;
        $data = unserialize($data);

        if (!empty($data))
        {
            return view('smsdew.send-sms-app');
        }
        else
        {
            $html = view($render, compact('data', 'type'));
            return view('smsdew.settings-hub', compact('data', 'html'));
        }
    }

    private function objectData($type)
    {

        $object_data = unserialize(Auth::user()->object_data);
        //$data = Credential::whereUserId(Auth::user()->id)->whereType(Auth::user()->sms_credentials_default_type)->value('data');
        $data = Credential::whereUserId(Auth::user()->id)
            ->whereType($type)->value('data');
        $data = unserialize($data);
        return $data;
    }
    // function apg_sms_codifica_el_mensaje( $mensaje ) {
    //     return apply_filters( 'apg_sms_message_return', urlencode( html_entity_decode( $mensaje, ENT_QUOTES, "UTF-8" ) ), $mensaje);
    // }
    private function messageAction($body, $phone, $default_type ,$workflow_email)
    {
        //this funciton is used in  POST_send_sms ,POST_send_sms_app and workflowSent functions
        //this function sends post request according to different methods ..and retrives the data   ..and finnaly returns response in json
        switch ($default_type)
        {
            case 'msg91':
                try
                {

                    $data = $this->objectData($default_type);
                    $auth_key = @$data['msg91'];
                    $routes = @$data['routes'];
                    $sender = @$data['sender'];
                    $message = urlencode($body);

                    try
                    {

                        $hub_api_key = @$data['t_hapi_key'];

                        //getting owner
                        $owner = $this->owner($hub_api_key);
                        $c = json_decode($owner);
                        if ($c->message) //this property gets generated only when error occurs
                        
                        {

                            $message = $c->message;
                            return $response = ['status' => 'False', 'message' => "$message"];

                        }
                    }
                    catch(\throwable $err)
                    { //code will enter here only when correct api is provided
                        

                        $email = $c->results[0]->email;
                        $firstname = $c->results[0]->firstName;

                        $lastname = $c->results[0]->lastName;
                        $ownerid = $c->results[0]->id;
                        $ownername = "$firstname  $lastname";

                        //setting time zone
                        $timestamp = time() * 1000;

                             //getting updated object data from db to extract vid
                        try{//this will create error when it is entered through messageing app but not through workflow
                              $contact_id  = $this->getvid($hub_api_key, $workflow_email);
                           } catch (\throwable $err) {
                            //we will enter here only when sending msg through app but not from workflow
                             $object_data = unserialize(Auth::user()->object_data);
                              $contact_id = $object_data['vid'];
                        }

                        if (!empty($auth_key) && !empty($routes) && !empty($sender))
                        {

                            $args['data'] = "{ \"sender\": \"$sender\", \"route\": \"$routes\",  \"sms\": [ { \"message\": \"$message\", \"to\": [ \"$phone\" ] } ] }";
                            $args['url'] = 'https://api.msg91.com/api/v2/sendsms';

                            $args['headers'] = array(
                                "authkey: " . $data['msg91'],
                                "content-type: application/json"
                            );

                            $response = $this->curlAccess('POST', $args);
                            $status = $response->type;

                            if (!empty(@$response->type))
                            {
                                $vendor = 'msg91';
                                $activity_response = $this->successfullActivityJson($hub_api_key, $ownerid, $contact_id, $body, $phone, $vendor, $ownername); //success activity
                                return $response = ['status' => true, 'message' => 'Message has been sent by msg91'];
                            }
                            else
                            {

                                $error = $this->errorActivityJson($hub_api_key, $ownerid, $contact_id, $phone); //eror activity
                                return $response = ['status' => false, 'message' => 'Something were wrong in credentials'];
                            }
                        }
                        else
                        {
                            $error = $this->errorActivityJson($hub_api_key, $ownerid, $contact_id, $phone); //eror activity
                            

                            return $response = ['status' => false, 'message' => 'Something were wrong in msg91 credentials'];
                        }
                    }
                }
                catch(Exception $e)
                {
                    $error = $this->errorActivityJson($hub_api_key, $ownerid, $contact_id, $phone); //error activity
                    return $response = ['status' => false, 'message' => 'something were wrong in msg91.'];
                }

            break;
            case 'msgwow':
                try
                {

                    $data = $this->objectData($default_type);
                    $auth_key = @$data['msgwow'];
                    $routes = @$data['routes'];
                    $sender = @$data['sender'];

                    try
                    {

                        $hub_api_key = @$data['t_hapi_key'];

                        //getting owner
                        $owner = $this->owner($hub_api_key);
                        $c = json_decode($owner);
                        if ($c->message) //this property gets generated only when error occurs
                        
                        {
                            $message = $c->message;
                            return $response = ['status' => 'False', 'message' => "$message"];
                        }
                    }
                    catch(\throwable $err)
                    {

                        $email = $c->results[0]->email;
                        $firstname = $c->results[0]->firstName;

                        $lastname = $c->results[0]->lastName;
                        $ownerid = $c->results[0]->id;
                        $ownername = "$firstname  $lastname";

                        //setting asian time zone
                        $timestamp = time() * 1000;

                             //getting updated object data from db to extract vid
                        try{//this will create error when it is entered through messageing app but not through workflow
                              $contact_id  = $this->getvid($hub_api_key, $workflow_email);
                           } catch (\throwable $err) {
                            //we will enter here only when sending msg through app but not from workflow
                             $object_data = unserialize(Auth::user()->object_data);
                              $contact_id = $object_data['vid'];
                        }

                        if (!empty($auth_key) && !empty($routes) && !empty($sender))
                        {

                            //Prepare you post parameters
                            $args['data'] = array(
                                'authkey' => $auth_key,
                                'mobiles' => $phone,
                                'message' => urlencode($body) ,
                                'sender' => $sender,
                                'route' => $routes
                            );

                            //API URL
                            $args['url'] = "http://my.msgwow.com/api/sendhttp.php";
                            $response = $this->curlAccess('POST', $args, 'json');
                            //                            print_r($response);
                            if (!empty($response))
                            {
                                $vendor = 'msgwow';
                                $activity_res = $this->successfullActivityJson($hub_api_key, $ownerid, $contact_id, $body, $phone, $vendor, $ownername); //success activity
                                return $response = ['status' => true, 'message' => 'Message has been sent by msgwow'];
                            }
                            else
                            {
                                $error = $this->errorActivityJson($hub_api_key, $ownerid, $contact_id, $phone); //error activity
                                return $response = ['status' => false, 'message' => 'Something were wrong in msgwow api while sending.'];
                            }

                        }
                        else
                        {

                            $error = $this->errorActivityJson($hub_api_key, $ownerid, $contact_id, $phone); //error activity
                            return $response = ['status' => false, 'message' => 'Something were wrong in msgwow credentials'];
                        }
                    }
                }
                catch(Exception $e)
                {

                    $error = $this->errorActivityJson($hub_api_key, $ownerid, $contact_id, $phone); //error activity
                    return $response = ['status' => false, 'message' => 'something were wrong in msgwow.'];
                }
            break;
            case 'twilio':
                try
                {

                    $data = $this->objectData($default_type);
                    $account_sid = @$data['s_id'];
                    $auth_token = @$data['s_token'];
                    $twilio_number = @$data['mobile'];
                    $string = "$account_sid" . ":" . "$auth_token";
                    $authcode = base64_encode($string);
                    $phoneno = '+' . $phone;

                    try
                    {

                        $hub_api_key = @$data['t_hapi_key'];

                        //getting owner
                        $owner = $this->owner($hub_api_key);
                        $c = json_decode($owner);
                        if ($c->message) //this property gets generated only when error occurs
                        
                        {
                            $message = $c->message;
                            return $response = ['status' => 'False', 'message' => "$message"];
                        }
                    }
                    catch(\throwable $err)
                    { //the code will enter in this catch only if correct api is provided
                        

                        $email = $c->results[0]->email;
                        $firstname = $c->results[0]->firstName;

                        $lastname = $c->results[0]->lastName;
                        $ownerid = $c->results[0]->id;
                        $ownername = "$firstname  $lastname";

                        $timestamp = time() * 1000;

                            //getting updated object data from db to extract vid
                        try{//this will create error when it is entered through messageing app but not through workflow
                              $contact_id  = $this->getvid($hub_api_key, $workflow_email);
                           } catch (\throwable $err) {
                            //we will enter here only when sending msg through app but not from workflow
                             $object_data = unserialize(Auth::user()->object_data);
                              $contact_id = $object_data['vid'];
                        }

                        if (!empty($account_sid) && !empty($auth_token) && !empty($twilio_number))
                        {

                            $args['headers'] = ["Authorization: Basic $authcode", "Content-Type: application/x-www-form-urlencoded"];
                            $args['data'] = "To=$phoneno&From=$twilio_number&Body=$body";

                            $args['url'] = "https://api.twilio.com/2010-04-01/Accounts/$account_sid/Messages.json";

                            $twilio = $this->curlAccess('POST', $args);

                            try
                            {
                                $code = $twilio->code;
                                if ($code != "")
                                {

                                    $error = $this->errorActivityJson($hub_api_key, $ownerid, $contact_id, $phoneno); //error activity
                                    return $response = ['status' => false, 'message' => $twilio->message];
                                }
                                else
                                {
                                    //                            print_r("inside else");
                                    //
                                    $url = "https://api.hubapi.com/engagements/v1/engagements?hapikey=$hub_api_key";

                                    $header = ['Content-Type: application/json'];
                                    $data = '{
                                                "engagement": {
                                                    "active": true,
                                                    "ownerId": ' . $ownerid . ',
                                                    "type": "NOTE",
                                                    "timestamp": ' . $timestamp . '
                                                },
                                                "associations": {
                                                    "contactIds": [' . $contact_id . '],
                                                    "companyIds": [ ],
                                                    "dealIds": [ ],
                                                    "ownerIds": [ ],
                                                    "ticketIds":[ ]
                                                },
                                                "attachments": [
                                                    {
                                                        "id": 4241968539
                                                    }
                                                ],
                                                "metadata": {
                                                    "body": "message: ' . $body . ' </br></br> To:' . $phoneno . ' </br> From:' . $twilio_number . ' </br> vendor:twilio </br> </br> owner:' . $ownername . ' </br> </br>Thanks!"  
                                                    }
                                            }';

                                    $activity = $this->smsActivity($url, $header, $data);

                                    return $response = ['status' => true, 'message' => 'Message has been sent by twilio'];
                                }
                            }

                            catch(\throwable $err)
                            { //this will only catch the exception when msg is sent so ..add success message inside it
                                //                             print_r("inside catch");
                                $url = "https://api.hubapi.com/engagements/v1/engagements?hapikey=$hub_api_key";

                                //                            $msg=json_encode($body);
                                $header = ['Content-Type: application/json'];
                                $data = '{
                                    "engagement": {
                                        "active": true,
                                        "ownerId": ' . $ownerid . ',
                                        "type": "NOTE",
                                        "timestamp": ' . $timestamp . '
                                    },
                                    "associations": {
                                        "contactIds": [' . $contact_id . '],
                                        "companyIds": [ ],
                                        "dealIds": [ ],
                                        "ownerIds": [ ],
                                        "ticketIds":[ ]
                                    },
                                    "attachments": [
                                        {
                                            "id": 4241968539
                                        }
                                    ],
                                    "metadata": {
                                        "body": "message: ' . $body . ' </br></br> To:' . $phoneno . ' </br> From:' . $twilio_number . ' </br> vendor:twilio </br> </br> owner:' . $ownername . ' </br> </br>Thanks!"  
                                        }
                                }';

                                $activity = $this->smsActivity($url, $header, $data); //function for creating activity
                                return $response = ['status' => true, 'message' => 'Message has been sent by twilio'];
                            }
                        }
                    }

                }
                catch(Exception $e)
                {

                    $error = $this->errorActivityJson($hub_api_key, $ownerid, $contact_id, $phoneno); //creating error activity
                    return $response = ['status' => false, 'message' => 'something were wrong in twilio.'];
                }
            break;
            case 'bulksms':
                try
                {
                    $data = $this->objectData($default_type);
                    $username = @$data['u_name'];
                    $password = @$data['pass'];
                    $string = "$username" . ":" . "$password";
                    $phoneNo = "+" . $phone;
                    $base64code = base64_encode($string);

                    try
                    {

                        $hub_api_key = @$data['t_hapi_key'];

                        //getting owner
                        $owner = $this->owner($hub_api_key);
                        $c = json_decode($owner);
                        if ($c->message) //this property gets generated only when error occurs
                        
                        {
                            $message = $c->message;
                            return $response = ['status' => 'False', 'message' => "$message"];
                        }
                    }
                    catch(\throwable $err)
                    {

                        $email = $c->results[0]->email;
                        $firstname = $c->results[0]->firstName;
                        $lastname = $c->results[0]->lastName;
                        $ownerid = $c->results[0]->id;
                        $ownername = "$firstname  $lastname";

                        //setting asian time zone
                        $timestamp = time() * 1000;

                             //getting updated object data from db to extract vid
                        try{//this will create error when it is entered through messageing app but not through workflow
                              $contact_id  = $this->getvid($hub_api_key, $workflow_email);
                           } catch (\throwable $err) {
                            //we will enter here only when sending msg through app but not from workflow
                             $object_data = unserialize(Auth::user()->object_data);
                              $contact_id = $object_data['vid'];
                        }

                        if (!empty($username) && !empty($password) && !empty($phoneNo))
                        {

                            $url = "https://api.bulksms.com/v1/messages";
                            $header = array(
                                "Authorization: Basic $base64code",
                                "Content-Type: application/json"
                            );
                            $text = "{\r\n    \"to\": \"$phoneNo\",\r\n    \"body\": \"$body\"\r\n}";
                            $response = $this->SmsCurlAccess($url, $header, $text);
                            $response_array = json_decode($response);
                            //print_r($response_array);
                            try
                            {
                                if ($response_array->status != '401')
                                { //checks if credentials are same or not...401 error means credentials are not correct
                                    try
                                    {
                                        if ($response_array[0]->type == 'SENT')
                                        { //checks if msg is delivered or not..
                                            $vendor = 'bulksms';
                                            $success = $this->successfullActivityJson($hub_api_key, $ownerid, $contact_id, $body, $phoneNo, $vendor, $ownername); //success activity
                                            return $response = ['status' => true, 'message' => 'Message has been sent by Bulk Sms'];
                                        }
                                        else
                                        {

                                            $error = $this->errorActivityJson($hub_api_key, $ownerid, $contact_id, $phoneNo); //error activity
                                            return $response = ['status' => false, 'message' => 'Sorry your message was not delivered.Check your account balance'];
                                        }
                                    }
                                    catch(\Throwable $e)
                                    { //handles error if credentials are correct but message is not delivered
                                        $error = $this->errorActivityJson($hub_api_key, $ownerid, $contact_id, $phoneNo); //error activity
                                        $message = $e->getMessage();
                                        return $response = ['status' => false, 'message' => "$message"];
                                    }
                                }

                                else
                                {
                                    $error = $this->errorActivityJson($hub_api_key, $ownerid, $contact_id, $phoneNo); //error activity
                                    return $response = ['status' => false, 'message' => 'Message not Delivered.Please check your Credentials'];
                                }

                            }
                            catch(\Throwable $e)
                            { //handles error if credentials were correct and message was succeffully delivered
                                if ($response_array[0]->type == 'SENT')
                                { //message is sent but creedential error is not there
                                    $vendor = 'bulksms';
                                    $activity_res = $this->successfullActivityJson($hub_api_key, $ownerid, $contact_id, $body, $phoneNo, $vendor, $ownername);

                                    return $response = ['status' => true, 'message' => 'Message has been sent by Bulk Sms'];
                                }
                                else
                                {

                                    $error = $this->errorActivityJson($hub_api_key, $ownerid, $contact_id, $phoneNo); //error activity
                                    $message = $e->getMessage(); //credential error is there
                                    return $response = ['status' => false, 'message' => "$message"];
                                }

                            }
                        }
                    }
                }
                catch(Exception $e)
                {

                    $error = $this->errorActivityJson($hub_api_key, $ownerid, $contact_id, $phoneNo); //error activity
                    $activity = $this->smsActivity($url, $header, $errdata);

                    $message = $e->getMessage();
                    return $response = ['status' => false, 'message' => "$message"];
                }
            break;

            case 'bulkgate':
                try
                {
                    $data = $this->objectData($default_type);
                    $application_id = @$data['app_id'];
                    $sender_id = @$data['sender_id'];
                    $unicode = @$data['unicode'];
                    $application_token = @$data['token'];
                    $phoneNo = "+" . $phone;

                    try
                    {

                        $hub_api_key = @$data['t_hapi_key'];

                        //getting owner
                        $owner = $this->owner($hub_api_key);
                        $c = json_decode($owner);
                        if ($c->message) //this property gets generated only when error occurs
                        
                        {
                            $message = $c->message;
                            return $response = ['status' => 'False', 'message' => "$message"];
                        }
                    }
                    catch(\throwable $err)
                    {

                        $email = $c->results[0]->email;
                        $firstname = $c->results[0]->firstName;

                        $lastname = $c->results[0]->lastName;
                        $ownerid = $c->results[0]->id;
                        $ownername = "$firstname  $lastname";

                        //setting asian time zone
                        $timestamp = time() * 1000;

                             //getting updated object data from db to extract vid
                        try{//this will create error when it is entered through messageing app but not through workflow
                              $contact_id  = $this->getvid($hub_api_key, $workflow_email);
                           } catch (\throwable $err) {
                            //we will enter here only when sending msg through app but not from workflow
                             $object_data = unserialize(Auth::user()->object_data);
                              $contact_id = $object_data['vid'];
                        }

                        if (!empty($application_id) && !empty($application_token) && !empty($phoneNo))
                        {

                            $url = "https://portal.bulkgate.com/api/1.0/simple/transactional";
                            $header = array(
                                "Content-Type: application/json"
                            );
                            $message = "{\r\n    \"application_id\": \"$application_id\", \r\n    \"application_token\": \"$application_token\", \r\n    \"number\": \"$phoneNo\", \r\n    \"text\": \"$body \", \r\n    \"unicode\": $unicode,\r\n    \"flash\": false,\r\n    \"sender_id\": \"gText\",\r\n    \"sender_id_value\": \"$sender_id\",\r\n    \"country\": \"gb\"\r\n}";
                            $check = "";
                            $response = $this->SmsCurlAccess($url, $header, $message);
                            //basically we are getting two different response objects for success and failure
                            //when we try to display the message of first object the other object is not generated which cause exception ..and thats why we have added try catch blocks.
                            try
                            {
                                $status = json_decode($response);
                                if ($status->status == "accepted")
                                {
                                    $check = "accepted";
                                    $vendor = 'bulkgate';

                                    $activity_res = $this->successfullActivityJson($hub_api_key, $ownerid, $contact_id, $body, $phoneNo, $vendor, $ownername); //successfull activity
                                    //
                                    return $response = ['status' => true, 'message' => 'Message has been sent by Bulkgate'];
                                }
                                else
                                {

                                    $error = $this->errorActivityJson($hub_api_key, $ownerid, $contact_id, $phoneNo); //error activity
                                    $check = "rejected";
                                    return $response = ['status' => false, 'message' => $status->error];
                                }

                            }
                            catch(\throwable $err)
                            {
                                $message = $err->getMessage();
                                try
                                {
                                    if ($status->error)
                                    {
                                        $error = $this->errorActivityJson($hub_api_key, $ownerid, $contact_id, $phoneNo); //error activity
                                        return $response = ['status' => false, 'message' => $status->error];
                                    }
                                }
                                catch(\throwable $er)
                                {
                                    $vendor = 'bulkgate';

                                    $activity_res = $this->successfullActivityJson($hub_api_key, $ownerid, $contact_id, $body, $phoneNo, $vendor, $ownername); //successfull activity
                                    return $response = ['status' => true, 'message' => 'Message has been sent by Bulkgate'];
                                }

                            }
                        }
                    }
                }
                catch(Exception $e)
                {
                    $error = $this->errorActivityJson($hub_api_key, $ownerid, $contact_id, $phoneNo); //error activity
                    return $response = ['status' => false, 'message' => 'something were wrong in Bulkgate.'];
                }
            break;

            case 'textlocal':
                try
                {
                    $data = $this->objectData($default_type);
                    $api_key = @$data['api_key'];
                    $sender_code = @$data['sender'];
                    $phoneNo = "+" . $phone;

                    try
                    {

                        $hub_api_key = @$data['t_hapi_key'];

                        //getting owner
                        $owner = $this->owner($hub_api_key);
                        $c = json_decode($owner);
                        if ($c->message) //this property gets generated only when error occurs
                        
                        {
                            $message = $c->message;
                            return $response = ['status' => 'False', 'message' => "$message"];
                        }
                    }
                    catch(\throwable $err)
                    {

                        $email = $c->results[0]->email;
                        $firstname = $c->results[0]->firstName;

                        $lastname = $c->results[0]->lastName;
                        $ownerid = $c->results[0]->id;
                        $ownername = "$firstname  $lastname";

                        //setting asian time zone
                        $timestamp = time() * 1000;

                           //getting updated object data from db to extract vid
                        try{//this will create error when it is entered through messageing app but not through workflow
                              $contact_id  = $this->getvid($hub_api_key, $workflow_email);
                           } catch (\throwable $err) {
                            //we will enter here only when sending msg through app but not from workflow
                             $object_data = unserialize(Auth::user()->object_data);
                              $contact_id = $object_data['vid'];
                        }

                        if (!empty($api_key) && !empty($phoneNo))
                        {

                            $url = "https://api.textlocal.in/send";
                            $header = array(
                                "Content-Type: application/x-www-form-urlencoded"
                            );

                            $message = rawurlencode($body);
                            $sender = urlencode("$sender_code");

                            $textbody = "apikey=$api_key&numbers=$phoneNo&sender=$sender&message=$message";

                            $response = $this->SmsCurlAccess($url, $header, $textbody);

                            $response_array = json_decode($response);

                            if ($response_array->status == 'success')
                            {
                                $vendor = 'textlocal';

                                $activity_res = $this->successfullActivityJson($hub_api_key, $ownerid, $contact_id, $body, $phoneNo, $vendor, $ownername); //successfull activity
                                

                                return $response = ['status' => true, 'message' => 'Message has been sent by textlocal'];
                            }
                            elseif ($response_array->status == 'failure')
                            {
                                $error = $this->errorActivityJson($hub_api_key, $ownerid, $contact_id, $phoneNo); //error activity
                                $response_message = $response_array->errors[0]->message;
                                return $response = ['status' => false, 'message' => "$response_message"];
                            }
                            else
                            {
                                $error = $this->errorActivityJson($hub_api_key, $ownerid, $contact_id, $phoneNo); //error activity
                                return $response = ['status' => false, 'message' => 'something went wrong in textlocal.'];
                            }
                        }
                    }
                }
                catch(Exception $e)
                {

                    $error = $this->errorActivityJson($hub_api_key, $ownerid, $contact_id, $phoneNo); //error activity
                    return $response = ['status' => false, 'message' => 'something went wrong in textlocal.'];
                }
            break;

            case 'kaleyra':
                try
                {
                    $data = $this->objectData($default_type);
                    $api_key = @$data['api_key'];
                    $s_id = @$data['s_id'];
                    $sender_id = @$data['sender_id'];
                    $phoneNo = "+" . $phone;

                    try
                    {

                        $hub_api_key = @$data['t_hapi_key'];

                        //getting owner
                        $owner = $this->owner($hub_api_key);
                        $c = json_decode($owner);
                        if ($c->message) //this property gets generated only when error occurs
                        
                        {
                            $message = $c->message;
                            return $response = ['status' => 'False', 'message' => "$message"];
                        }
                    }
                    catch(\throwable $err)
                    {

                        $email = $c->results[0]->email;
                        $firstname = $c->results[0]->firstName;

                        $lastname = $c->results[0]->lastName;
                        $ownerid = $c->results[0]->id;
                        $ownername = "$firstname  $lastname";

                        //setting asian time zone
                        $timestamp = time() * 1000;

                             //getting updated object data from db to extract vid
                        try{//this will create error when it is entered through messageing app but not through workflow
                              $contact_id  = $this->getvid($hub_api_key, $workflow_email);
                           } catch (\throwable $err) {
                            //we will enter here only when sending msg through app but not from workflow
                             $object_data = unserialize(Auth::user()->object_data);
                              $contact_id = $object_data['vid'];
                        }

                        if (!empty($s_id) && !empty($api_key) && !empty($phoneNo))
                        {

                            $url = "https://api.kaleyra.io/v1/$s_id/messages";
                            $header = array(
                                "api-key: $api_key"
                            );
                            //                            $sender  ='KLRHXA';
                            $sender = $sender_id;

                            $textbody = array(
                                "to" => "$phoneNo",
                                "body" => "$body",
                                "sender" => "$sender"
                            );

                            $response = $this->SmsCurlAccess($url, $header, $textbody);
                            $r = json_decode($response);
                            //                               print_r($r);
                            try
                            {
                                if (empty($r->error))
                                {

                                    $vendor = 'kaleyra';
                                    $activity_res = $this->successfullActivityJson($hub_api_key, $ownerid, $contact_id, $body, $phoneNo, $vendor, $ownername); //successfull activity
                                    return $response = ['status' => true, 'message' => 'Message has been sent by Kaleyra'];
                                }
                                else
                                {
                                    $message = $r
                                        ->error->error;
                                    //                                print_r($r);
                                    $error = $this->errorActivityJson($hub_api_key, $ownerid, $contact_id, $phoneNo); //error activity
                                    return $response = ['status' => false, 'message' => "$message"];
                                }
                            }
                            catch(\Throwable $e) //this will only work message has been sent and
                            
                            {

                                $vendor = 'kaleyra';
                                $activity_res = $this->successfullActivityJson($hub_api_key, $ownerid, $contact_id, $body, $phoneNo, $vendor, $ownername); //successfull activity
                                

                                return $response = ['status' => true, 'message' => 'Message has been sent by Kaleyra'];
                            }

                        }
                    }
                }
                catch(Exception $e)
                {

                    $error = $this->errorActivityJson($hub_api_key, $ownerid, $contact_id, $phoneNo); //error activity
                    return $response = ['status' => true, 'message' => 'something were wrong in Kaleyra.'];
                }
            break;
            case 'plivo':
                try
                {
//                    
                    $data = $this->objectData($default_type);
                    $auth_id = @$data['a_id'];
                    $auth_key = @$data['a_token'];
                    $src_no = @$data['s_no'];
                    $phoneNo = $phone;
                    $string = "$auth_id" . ":" . "$auth_key";
                    $authentication = base64_encode($string);

                    try
                    {

                        $hub_api_key = @$data['t_hapi_key'];

                        //getting owner
                        $owner = $this->owner($hub_api_key);
                        $c = json_decode($owner);
                        if ($c->message) //this property gets generated only when HUBAPI error occurs
                        
                        {
                            $message = $c->message;
                            return $response = ['status' => 'False', 'message' => "$message"];
                        }
                    }
                    catch(\throwable $err)
                    {

                        $email = $c->results[0]->email;
                        $firstname = $c->results[0]->firstName;

                        $lastname = $c->results[0]->lastName;
                        $ownerid = $c->results[0]->id;
                        $ownername = "$firstname  $lastname";

                        //setting asian time zone
                        $timestamp = time() * 1000;

                        //getting updated object data from db to extract vid
                        try{//this will create error when it is entered through messageing app but not through workflow
                              $contact_id  = $this->getvid($hub_api_key, $workflow_email);
                           } catch (\throwable $err) {
                            //we will enter here only when sending msg through app but not from workflow
                             $object_data = unserialize(Auth::user()->object_data);
                              $contact_id = $object_data['vid'];
                        }
                        
//                        DB::table('vid_checks')->insert(['vid' => $contact_id ]);
                        
                        if (!empty($auth_id) && !empty($auth_key) && !empty($phoneNo))
                        {

                            $url = "https://api.plivo.com/v1/Account/$auth_id/Message/";
                            $header = array(
                                "Authorization: Basic $authentication",
                                "Content-Type: application/json"
                            );

                            $text = "{\"src\":\"$src_no\",\r\n\"dst\": \"$phoneNo\",\r\n\"text\":\"$body \"\r\n}";

                            $response_message = $this->SmsCurlAccess($url, $header, $text);

                            try
                            {
                                $message = json_decode($response_message);
                                $type = var_dump($message);
                                //                              $print_r($type);
                                //                            print_r($message->message_uuid[0]);
                                if (!empty($message))
                                {

                                    $vendor = 'Plivo';
                                    $activity_res = $this->successfullActivityJson($hub_api_key, $ownerid, $contact_id, $body, $phoneNo, $vendor, $ownername);

                                    return $response = ['status' => true, 'message' => 'Message has been sent by plivo'];
                                }
                                else
                                {
                                    $error = $this->errorActivityJson($hub_api_key, $ownerid, $contact_id, $phoneNo); //error activity
                                    return $response = ['status' => false, 'message' => 'something were wrong in plivo.'];
                                }
                            }
                            catch(\throwable $e) //this catches the message uuid execption when message is not sent
                            
                            {
                                $error = $this->errorActivityJson($hub_api_key, $ownerid, $contact_id, $phoneNo); //error activity
                                return $response = ['status' => false, 'message' => "$response_message"];
                            }
                        }
                    }
                }
                catch(Exception $e)
                { //exception for any other error
                    $error = $this->errorActivityJson($hub_api_key, $ownerid, $contact_id, $phoneNo); //error activity
                    return $response = ['status' => false, 'message' => 'something were wrong in plivo.'];
                }
            break;
            case 'springh':
                try
                {
                    $data = $this->objectData($default_type);
                    $api_key = @$data['apikey_se'];
                    $sender_id = @$data['sender_id'];
                    $phoneNo = $phone;

                    try
                    {

                        $hub_api_key = @$data['t_hapi_key'];

                        //getting owner
                        $owner = $this->owner($hub_api_key);
                        $c = json_decode($owner);
                        if ($c->message) //this property gets generated only when error occurs
                        
                        {
                            $message = $c->message;
                            return $response = ['status' => 'False', 'message' => "$message"];
                        }
                    }
                    catch(\throwable $err)
                    {

                        $email = $c->results[0]->email;
                        $firstname = $c->results[0]->firstName;

                        $lastname = $c->results[0]->lastName;
                        $ownerid = $c->results[0]->id;
                        $ownername = "$firstname  $lastname";

                        //setting asian time zone
                        $timestamp = time() * 1000;

                        //getting updated object data from db to extract vid
                        try{//this will create error when it is entered through messageing app but not through workflow
                              $contact_id  = $this->getvid($hub_api_key, $workflow_email);
                           } catch (\throwable $err) {
                            //we will enter here only when sending msg through app but not from workflow
                             $object_data = unserialize(Auth::user()->object_data);
                              $contact_id = $object_data['vid'];
                        }
                        if (!empty($api_key) && !empty($phoneNo))
                        {

                            $url = "https://instantalerts.co/api/web/send";
                            $header = array(
                            );

                            $text = array(
                                "sender" => "$sender_id",
                                "to" => "$phoneNo",
                                "message" => "$body",
                                "format" => "json",
                                "apikey" => "$api_key"
                            );
                            $response = $this->SmsCurlAccess($url, $header, $text);
                            $res = json_decode($response);
                            try
                            {
                                $message_sent = $res->MessageIDs;
                                $message_error = $res->error;
                                if (!empty($message_sent))
                                {

                                    $vendor = 'springedge';
                                    $activity_res = $this->successfullActivityJson($hub_api_key, $ownerid, $contact_id, $body, $phoneNo, $vendor, $ownername);

                                    //                                print_r($activity);
                                    return $response = ['status' => true, 'message' => 'Message has been sent by springedge'];
                                }
                            }
                            catch(\throwable $e) //catches both errors and message sent objects
                            
                            {
                                try
                                {
                                    if ($res->error)
                                    { //this catches the errors
                                        $error = $this->errorActivityJson($hub_api_key, $ownerid, $contact_id, $phoneNo); //error activity
                                        $message_error = $res->error;
                                        return $response = ['status' => false, 'message' => "$message_error"];
                                    }
                                    else
                                    {

                                        $vendor = 'springedge';
                                        $activity_res = $this->successfullActivityJson($hub_api_key, $ownerid, $contact_id, $body, $phoneNo, $vendor, $ownername);

                                        //
                                        //                                print_r($activity);
                                        return $response = ['status' => true, 'message' => 'Message has been sent by springedge'];
                                    }
                                }
                                catch(\throwable $er)
                                { //this catches the error of above error object which is not generated
                                    $vendor = 'springedge';
                                    $activity_res = $this->successfullActivityJson($hub_api_key, $ownerid, $contact_id, $body, $phoneNo, $vendor, $ownername);

                                    //                                print_r($activity);
                                    return $response = ['status' => true, 'message' => 'Message has been sent by springedge'];
                                }
                            }
                        }
                    }
                }
                catch(Exception $e)
                {

                    $error = $this->errorActivityJson($hub_api_key, $ownerid, $contact_id, $phoneNo); //error activity
                    return $response = ['status' => false, 'message' => 'something were wrong in springedge.'];
                }
            break;
            case 'vonage':
                try
                {
                    $data = $this->objectData($default_type);
                    $api_key = @$data['apikey_vo'];
                    $api_secret = @$data['apisecret_vo'];
                    $from_vonage = @$data['from'];
                    $phoneNo = $phone;

                    try
                    {

                        $hub_api_key = @$data['t_hapi_key'];

                        //getting owner
                        $owner = $this->owner($hub_api_key);
                        $c = json_decode($owner);
                        if ($c->message) //this property gets generated only when error occurs
                        
                        {
                            $message = $c->message;
                            return $response = ['status' => 'False', 'message' => "$message"];
                        }
                    }
                    catch(\throwable $err)
                    {

                        $email = $c->results[0]->email;
                        $firstname = $c->results[0]->firstName;

                        $lastname = $c->results[0]->lastName;
                        $ownerid = $c->results[0]->id;
                        $ownername = "$firstname  $lastname";

                        //setting asian time zone
                        $timestamp = time() * 1000;

                        
                            //getting updated object data from db to extract vid
                        try{//this will create error when it is entered through messageing app but not through workflow
                              $contact_id  = $this->getvid($hub_api_key, $workflow_email);
                           } catch (\throwable $err) {
                            //we will enter here only when sending msg through app but not from workflow
                             $object_data = unserialize(Auth::user()->object_data);
                              $contact_id = $object_data['vid'];
                        }

                        if (!empty($api_key) && !empty($phoneNo) && !empty($api_secret))
                        {

                            $url = "https://rest.nexmo.com/sms/json";
                            $header = array(
                                "Content-Type: application/x-www-form-urlencoded"
                            );
                            $message = rawurlencode($body);
                            //                            $from    = rawurlencode("Vonage SMS API");
                            $from = rawurlencode($from_vonage);
                            $sender = urlencode('TXTLCL');

                            $textbody = "api_key=$api_key&api_secret=$api_secret&to=$phoneNo&from=$from&text=$message";

                            $response = $this->SmsCurlAccess($url, $header, $textbody);
                            $res = json_decode($response);
                            $status = $res->messages[0]->status;
                            if ($status == 0)
                            {

                                $vendor = 'Vonage';
                                $activity_res = $this->successfullActivityJson($hub_api_key, $ownerid, $contact_id, $body, $phoneNo, $vendor, $ownername); //successactivity
                                

                                return $response = ['status' => true, 'message' => 'Message has been sent by vonage'];
                            }
                            elseif ($status == 4)
                            {
                                $error = $this->errorActivityJson($hub_api_key, $ownerid, $contact_id, $phoneNo); //error activity
                                return $response = ['status' => false, 'message' => "Bad-Credentials"];
                            }
                            else
                            {

                                $error = $this->errorActivityJson($hub_api_key, $ownerid, $contact_id, $phoneNo); //error activity
                                return $response = ['status' => false, 'message' => "something were wrong in vonage."];
                            }

                        }
                    }
                }
                catch(Exception $e)
                {

                    $error = $this->errorActivityJson($hub_api_key, $ownerid, $contact_id, $phoneNo); //error activity
                    return $response = ['status' => false, 'message' => 'something were wrong in vonage.'];
                }
            break;
            case 'mobtexting':
                try
                {
                    $data = $this->objectData($default_type);
                    $api_key = @$data['apikey_mt'];
                    $sender = @$data['sender'];
                    $service = @$data['service'];
                    $phoneNo = $phone;

                    try
                    {

                        $hub_api_key = @$data['t_hapi_key'];

                        //getting owner
                        $owner = $this->owner($hub_api_key);
                        $c = json_decode($owner);
                        if ($c->message) //this property gets generated only when error occurs
                        
                        {
                            $message = $c->message;
                            return $response = ['status' => 'False', 'message' => "$message"];
                        }
                    }
                    catch(\throwable $err)
                    {

                        $email = $c->results[0]->email;
                        $firstname = $c->results[0]->firstName;

                        $lastname = $c->results[0]->lastName;
                        $ownerid = $c->results[0]->id;
                        $ownername = "$firstname  $lastname";

                        //setting asian time zone
                        $timestamp = time() * 1000;
                       
                             //getting updated object data from db to extract vid
                        try{//this will create error when it is entered through messageing app but not through workflow
                              $contact_id  = $this->getvid($hub_api_key, $workflow_email);
                           } catch (\throwable $err) {
                            //we will enter here only when sending msg through app but not from workflow
                             $object_data = unserialize(Auth::user()->object_data);
                              $contact_id = $object_data['vid'];
                        }

                        if (!empty($api_key) && !empty($phoneNo))
                        {

                            $url = "https://portal.mobtexting.com/api/v2/sms/send";
                            $header = array(
                                "Authorization: Bearer $api_key"
                            );
                            $text = array(
                                'message' => "$body",
                                'sender' => "$sender",
                                'to' => "$phoneNo",
                                'service' => "$service"
                            );

                            $response = $this->SmsCurlAccess($url, $header, $text);
                            $res = json_decode($response);
                            $status = $res->status;
                            $message = $res->message;
                            if ($status == "200")
                            {

                                $vendor = 'mobtexting';
                                $activity_res = $this->successfullActivityJson($hub_api_key, $ownerid, $contact_id, $body, $phoneNo, $vendor, $ownername); //successactivity
                                

                                //                                print_r($activity);
                                return $response = ['status' => true, 'message' => 'Message has been sent by mobtexting'];
                            }
                            else
                            {
                                $error = $this->errorActivityJson($hub_api_key, $ownerid, $contact_id, $phoneNo); //error activity
                                return $response = ['status' => false, 'message' => $message];
                            }
                        }
                    }
                }
                catch(Exception $e)
                {
                    $error = $this->errorActivityJson($hub_api_key, $ownerid, $contact_id, $phoneNo); //error activity
                    return $response = ['status' => false, 'message' => 'something were wrong in mobtexting.'];
                }
            break;

            case 'moreify':
                try
                {
                    $data = $this->objectData($default_type);
                    $project = @$data['project'];
                    $password = @$data['password'];
                    $phoneNo = "+" . $phone;

                    try
                    {

                        $hub_api_key = @$data['t_hapi_key'];

                        //getting owner
                        $owner = $this->owner($hub_api_key);
                        $c = json_decode($owner);
                        if ($c->message) //this property gets generated only when error occurs
                        
                        {
                            $message = $c->message;
                            return $response = ['status' => 'False', 'message' => "$message"];
                        }
                    }
                    catch(\throwable $err)
                    {

                        $email = $c->results[0]->email;
                        $firstname = $c->results[0]->firstName;

                        $lastname = $c->results[0]->lastName;
                        $ownerid = $c->results[0]->id;
                        $ownername = "$firstname  $lastname";

                        //setting asian time zone
                        $timestamp = time() * 1000;

                          //getting updated object data from db to extract vid
                        try{//this will create error when it is entered through messageing app but not through workflow
                              $contact_id  = $this->getvid($hub_api_key, $workflow_email);
                           } catch (\throwable $err) {
                            //we will enter here only when sending msg through app but not from workflow
                             $object_data = unserialize(Auth::user()->object_data);
                              $contact_id = $object_data['vid'];
                        }
                        
                        if (!empty($project) && !empty($phoneNo) && !empty($password))
                        {

                            $url = "https://mapi.moreify.com/api/v1/sendSms";
                            $header = array();
                            $text = array(
                                'project' => "$project",
                                'password' => "$password",
                                'phonenumber' => "$phoneNo",
                                'message' => "$body"
                            );

                            $response = $this->SmsCurlAccess($url, $header, $text);
                            $res = json_decode($response);
                            $status = $res->success;
                            if ($status == "true")
                            {

                                $vendor = 'moreify';
                                $activity_res = $this->successfullActivityJson($hub_api_key, $ownerid, $contact_id, $body, $phoneNo, $vendor, $ownername); //successactivity
                                //                                print_r($activity);
                                return $response = ['status' => true, 'message' => 'Message has been sent by moreify'];
                            }
                            else
                            {

                                $error = $this->errorActivityJson($hub_api_key, $ownerid, $contact_id, $phoneNo); //error activity
                                return $response = ['status' => false, 'message' => $res->errorMessage];
                            }

                        }
                    }
                }
                catch(Exception $e)
                {
                    $error = $this->errorActivityJson($hub_api_key, $ownerid, $contact_id, $phoneNo); //error activity
                    return $response = ['status' => false, 'message' => 'something were wrong in moreify.'];
                }
            break;

            case 'clickatell':
                try
                {
                    $data = $this->objectData($default_type);
                    $api_key = @$data['apikey_ct'];
                    $phoneNo = $phone;

                    try
                    {

                        $hub_api_key = @$data['t_hapi_key'];

                        //getting owner
                        $owner = $this->owner($hub_api_key);
                        $c = json_decode($owner);
                        if ($c->message) //this property gets generated only when error occurs
                        
                        {
                            $message = $c->message;
                            return $response = ['status' => 'False', 'message' => "$message"];
                        }
                    }
                    catch(\throwable $err)
                    {

                        $email = $c->results[0]->email;
                        $firstname = $c->results[0]->firstName;

                        $lastname = $c->results[0]->lastName;
                        $ownerid = $c->results[0]->id;
                        $ownername = "$firstname  $lastname";

                        //setting asian time zone
                        $timestamp = time() * 1000;

                             //getting updated object data from db to extract vid
                        try{//this will create error when it is entered through messageing app but not through workflow
                              $contact_id  = $this->getvid($hub_api_key, $workflow_email);
                           } catch (\throwable $err) {
                            //we will enter here only when sending msg through app but not from workflow
                             $object_data = unserialize(Auth::user()->object_data);
                              $contact_id = $object_data['vid'];
                        }

                        if (!empty($api_key) && !empty($phoneNo))
                        {

                            $url = "https://platform.clickatell.com/v1/message";
                            $header = array(
                                "Authorization: $api_key",
                                "Content-Type: application/json"
                            );
                            $text = "{\"messages\": [{ \"channel\": \"sms\", \"to\": \"$phoneNo\", \"content\": \"$body\" }]}";

                            $response = $this->SmsCurlAccess($url, $header, $text);
                            $res = json_decode($response);
                            $error = $res->error;
                            if (empty($error))
                            {

                                $vendor = 'clickatell';
                                $activity_res = $this->successfullActivityJson($hub_api_key, $ownerid, $contact_id, $body, $phoneNo, $vendor, $ownername); //successactivity
                                return $response = ['status' => true, 'message' => 'Message has been sent by clickatell'];
                            }
                            else
                            {
                                $error = $this->errorActivityJson($hub_api_key, $ownerid, $contact_id, $phoneNo); //error activity
                                return $response = ['status' => false, 'message' => $error->description];
                            }
                        }
                    }
                }
                catch(Exception $e)
                {
                    $error = $this->errorActivityJson($hub_api_key, $ownerid, $contact_id, $phoneNo); //error activity
                    return $response = ['status' => false, 'message' => 'something were wrong in clickatell.'];
                }
            break;
            default:
                return ['status' => false, 'message' => 'Service provider not selected.'];
            break;
        }
    }

    public function POST_send_sms(Request $request)
    {

        $data = $request->all();

        $v = \Validator::make($request->all() , ['phone' => 'required|array', 'body' => 'required|min:3|max:1000', ]);
        $this->auth_access($data['auth_id']);
        if ($v->fails())
        {
            $errors = $v->errors();
            return view('smsdew.send-sms', compact('errors'));
        }
        $recipients = $data["phone"];

        $alert = "warning";
        $message = "something went wrong.";
        // iterate over the array of recipients and send a twilio request for each
        foreach ($recipients as $recipient)
        {

            $res = $this->validate_phone_number($recipient);
            if ($res['status'] == true)
            {
                $email='';
                $result = $this->messageAction($data['body'], $res['phone'], Auth::user()->sms_credentials_default_type,$email);
                if ($result['status'] === true)
                {
                    $alert = 'success';
                }
                else
                {
                    $alert = 'danger';
                }
                $message = $result['message'];

            }
            else
            {
                $message = "Invalid phone number";
                $alert = "danger";
            }
        }
        return view('smsdew/send-sms', compact('alert', 'message'));
    }

    public function POST_send_sms_app(Request $request)
    {

        $data = $request->all();

        $v = \Validator::make($request->all() , ['phone' => 'required|array', 'body' => 'required|min:3|max:1000', ]);
        $this->auth_access($data['auth_id']);
        if ($v->fails())
        {
            $errors = $v->errors();
            return view('smsdew.send-sms-app', compact('errors'));
        }
        $recipients = $data["phone"];

        $alert = "warning";
        $message = "something went wrong.";
        // iterate over the array of recipients and send a twilio request for each
        foreach ($recipients as $recipient)
        {

            $res = $this->validate_phone_number($recipient);
            if ($res['status'] == true)
            {
                $email='';
                $result = $this->messageAction($data['body'], $res['phone'], Auth::user()->sms_credentials_default_type,$email);
                if ($result['status'] === true)
                {
                    $alert = 'success';
                }
                else
                {
                    $alert = 'danger';
                }
                $message = $result['message'];

            }
            else
            {
                $message = "Invalid phone number";
                $alert = "danger";
            }
        }
        return view('smsdew.send-sms-app', compact('alert', 'message'));
    }

    private function validate_phone_number($phone)
    {
        // Allow +, - and . in phone number
        $filtered_phone_number = filter_var($phone, FILTER_SANITIZE_NUMBER_INT);

        // Remove "-" from number
        $phone_to_check = preg_replace('/[^0-9\-]/', "", $filtered_phone_number);
        $phone_to_check = str_replace("-", "", $phone_to_check);

        // Check the lenght of number
        // This can be customized if you want phone number from a specific country
        if (strlen($phone_to_check) < 10 || strlen($phone_to_check) > 14)
        {
            return ['status' => false, 'phone' => $phone_to_check];
        }
        else
        {
            return ['status' => true, 'phone' => $phone_to_check];
        }
    }

    private function curlAccess($method, $array, $content_type = 'array')
    {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $array['url']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_ENCODING, "");
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        switch ($method)
        {
            case "POST":
                curl_setopt($ch, CURLOPT_POST, true);
                if ($array['data']) curl_setopt($ch, CURLOPT_POSTFIELDS, $array['data']);
                break;
            case "PUT":
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
                if ($array['data']) curl_setopt($ch, CURLOPT_POSTFIELDS, $array['data']);
                break;
            default:
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        }

        if (!empty($array['headers']))
        {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $array['headers']);
        }

        $result = curl_exec($ch);
        //        print_r($result);
        //        die;
        if (curl_errno($ch))
        {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);

        if ($content_type == 'array')
        {
            $result = json_decode($result);
        }
        return $result;

    }

    private function curlAccessH($method, $array, $content_type = 'array')
    {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $array['url']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_ENCODING, "");
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        switch ($method)
        {
            case "POST":
                curl_setopt($ch, CURLOPT_POST, 1);
                if ($array['data']) curl_setopt($ch, CURLOPT_POSTFIELDS, $array['data']);
                break;
            case "PUT":
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
                if ($array['data']) curl_setopt($ch, CURLOPT_POSTFIELDS, $array['data']);
                break;
            default:
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        }

        $headers = array();
        $headers[] = 'Content-Type: application/json';
        if (!empty($array['token']))
        {
            $headers[] = 'authorization: Bearer ' . $array['token'];
        }

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $result = curl_exec($ch);

        if (curl_errno($ch))
        {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);

        if ($content_type == 'array')
        {
            $result = json_decode($result);
        }
        return $result;

    }

    public function settings()
    {
        $type = Auth::user()->sms_credentials_default_type;
        $data = Credential::whereUserId(Auth::user()->id)
            ->where('type', $type)->value('data');
        $data = unserialize($data);
        $html = view('render.smsdew.' . $type, compact('data', 'type'));
        return view('smsdew.settings', compact('data', 'html', 'type'));
    }

    public function POST_settings(Request $request)
    {
        /* call function*/
        $array = vendor_select($request->type);

        $v = \Validator::make($request->all() , $array['validation']);
        $type = $request->type;

        if ($v->fails())
        {
            $errors = $v->errors();
            $data = Credential::whereUserId(Auth::user()->id)
                ->where('type', $request->type)
                ->value('data');
            $data = unserialize($data);
            $html = view('render/smsdew/' . $request->type, compact('data'));
            return view('smsdew/settings', compact('errors', 'data', 'html', 'type'));
        }
        try
        {

            $res = $request->all();
            $result = 0;
            if (!empty($res['default']))
            {
                User::whereId(Auth::user()->id)
                    ->update(['sms_credentials_default_type' => $res['default']]);
                $type = $res['default'];
                $result = 1;
            }

            unset($res['_token']);
            $id = Credential::whereUserId(Auth::user()->id)
                ->where('type', $res['type'])->value('id');
            $data = serialize($res);
            if (empty($id))
            {
                Credential::create(['data' => $data, 'user_id' => Auth::user()->id, 'fk_app_id' => 1, 'type' => $res['type']]);
                $alert = 'success';
                $message = $array['name'] . ' data has been saved.';
            }
            else
            {
                Credential::where('id', $id)->update(['data' => $data, 'fk_app_id' => 1]);
                $alert = 'success';
                $message = $array['name'] . ' data has been updated.';
            }
        }
        catch(Exception $e)
        {
            $alert = 'warning';
            $message = 'Something went wrong in code.';
        }

        $data = Credential::whereUserId(Auth::user()->id)
            ->where('type', $request->type)
            ->value('data');
        $data = unserialize($data);
        if ($result > 0)
        {
            $html = view('render/smsdew/' . $request->type, compact('data', 'type'));
        }
        else
        {
            $html = view('render/smsdew/' . $request->type, compact('data'));
        }

        return view('smsdew.settings', compact('alert', 'message', 'data', 'html', 'type'));
    }

    public function settings_hub($auth_id)
    {
        $this->auth_access($auth_id);
        $type = Auth::user()->sms_credentials_default_type;
        $data = Credential::whereUserId(Auth::user()->id)
            ->where('type', $type)->value('data');
        $data = unserialize($data);
        $html = view('render.smsdew.' . $type, compact('data', 'type'));
        return view('smsdew.settings-hub', compact('data', 'html', 'type'));
    }

    public function POST_settings_hub(Request $request)
    {
        /* call function*/
        try
        {
            $array = vendor_select($request->type);
            $this->auth_access($request->auth_id);
            $v = \Validator::make($request->all() , $array['validation']);
            $type = $request->type;

            //        $check=$request->all();
            //        print_r($check);
            if ($v->fails())
            {
                $errors = $v->errors();
                $data = Credential::whereUserId(Auth::user()->id)
                    ->where('type', $request->type)
                    ->value('data');
                $data = unserialize($data);
                $html = view('render/smsdew/' . $request->type, compact('data'));
                return view('smsdew.settings-hub', compact('errors', 'data', 'html', 'type'));
            }
            try
            {

                $res = $request->all();
                $result = 0;
                if (!empty($res['default']))
                {
                    User::whereId(Auth::user()->id)
                        ->update(['sms_credentials_default_type' => $res['default']]);
                    $type = $res['default'];
                    $result = 1;
                }

                unset($res['_token']);
                $id = Credential::whereUserId(Auth::user()->id)
                    ->where('type', $res['type'])->value('id');
                $data = serialize($res);
                if (empty($id))
                {
                    Credential::create(['data' => $data, 'user_id' => Auth::user()->id, 'fk_app_id' => 1, 'type' => $res['type']]);
                    $alert = 'success';
                    $message = $array['name'] . ' data has been saved.';
                }
                else
                {
                    Credential::where('id', $id)->update(['data' => $data, 'fk_app_id' => 1]);
                    $alert = 'success';
                    $message = $array['name'] . ' data has been updated.';
                }
            }
            catch(Exception $e)
            {
                $alert = 'warning';
                $message = 'Something went wrong in code.';
            }

            $data = Credential::whereUserId(Auth::user()->id)
                ->where('type', $request->type)
                ->value('data');
            $data = unserialize($data);
            if ($result > 0)
            {
                $html = view('render/smsdew/' . $request->type, compact('data', 'type'));
            }
            else
            {
                $html = view('render/smsdew/' . $request->type, compact('data'));
            }

            return view('smsdew.settings-hub', compact('alert', 'message', 'data', 'html', 'type'));
        }
        catch(Exception $er)
        {
            echo 'Message: ' . $er->getMessage();
        }
    }

    public function auth_access($auth_id)
    {
        $user = User::whereId($auth_id)->first();
        Auth::login($user);
    }

    public function render_vendor($auth_id)
    {
        $this->auth_access($auth_id);
        $type = Auth::user()->sms_credentials_default_type;
        $vendor = $_GET['vendor'];
        $data = Credential::whereUserId(Auth::user()->id)
            ->where('type', $vendor)->value('data');
        $data = @unserialize($data);
        $views = 'render/smsdew/' . $vendor;
        return view($views, compact('data', 'type'));
    }

    // public function checking()
    // {
    //     echo "inside";
    // }

    // public function check(REQUEST $request)
    // {

    //     $data = $request->all();
    //     //        print_r($data);
    //     //        DB::table('results')->insert(['data'=>"workflow worked"]);
    //     //        $workflows = DB::table('results')->insert(['data'=>serialize($data)]);
    //     //        print_r($workflows);
    //     $portal_id = $data['portal_id'];

    //     //        print_r($data);
    //     //        $workflows = DB::table('results')->insert(['data'=>'Phone number not valid.']);
    //     //        print_r($workflows);
    //     //        die;
    //     //        print_r("testing data");
    //     //        print_r($data);
        

    //     //                $this->auth_access($auth_id);
    //     //        $type = Auth::user()->sms_credentials_default_type;
    //     //        $data = Credential::whereUserId(Auth::user()->id)
    //     //                            ->where('type', $type)
    //     //                            ->value('data');
    //     //        $data = unserialize($data);
        

    //     $method = DB::select("select sms_credentials_default_type,hub_email from users where portal_id =$portal_id");
    //     //        print_r($method);
    //     //        die;
    //     $newData['portal_id'] = @$data['origin']['portalId'];
    //     $newData['actionDefinitionId'] = @$data['origin']['actionDefinitionId'];
    //     //        $newData['select_method'] = @$data['inputFields']['select_method'];
    //     $newData['select_method'] = $method;
    //     $newData['phone_number'] = @$data['inputFields']['phone_number'];
    //     $newData['message'] = @urlencode($data['inputFields']['message']);
    //     if ($newData['select_method'] == 'twilio')
    //     {

    //         $newData['message'] = @$data['inputFields']['message'];
    //     }
    //     //print_r($newData);
    //     //        die;
    //     $user = User::where('portal_id', $newData['portal_id'])->value('id');

    //     Auth::logout();
    //     if ($user)
    //     {

    //         $user = User::where('portal_id', $newData['portal_id'])->first();
    //         Auth::login($user);
    //         $recipients = $newData["phone_number"];
    //         $res = $this->validate_phone_number($recipients);
    //         if ($res['status'] == true)
    //         {

    //             $workflows = DB::table('workflows')->insert(['data' => serialize($newData) ]);
    //             $result = $this->messageAction($newData['message'], $res['phone'], $newData['select_method']);
    //             //                    DB::table('results')->where->('id',1)->update(['data'=>serialize($result)]);
    //             $workflows = DB::table('results')->insert(['data' => serialize($result) ]);
    //         }
    //         else
    //         {

    //             $workflows = DB::table('workflows')->insert(['data' => 'Phone number not valid.']);
    //         }
    //     }
    //     else
    //     {

    //         $workflows = DB::table('workflows')->insert(['data' => 'Portal Id not found.']);
    //     }

    // }

     private function getvid($hubapi, $email)
     {
         $url = "https://api.hubapi.com/contacts/v1/contact/email/$email/profile?hapikey=$hubapi";

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => $url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'GET',
          CURLOPT_HTTPHEADER => array(
            'Cookie: __cfduid=d15f14d1984c6b921f0319eb10ff9cce51606378583'
          ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
       $response_array= json_decode($response);
       
       $vid= $response_array->vid;
    
         return $vid;

     }

    public function workflowSent(Request $request)
    {

        $data = $request->all();
        $data2=serialize($data);
             
//                $res=DB::table('workflow_results')->insert(['data'=>'workflow chlya..']);
        
        $newData['portal_id'] = @$data['origin']['portalId'];
        $newData['actionDefinitionId'] = @$data['origin']['actionDefinitionId'];
        $newData['email'] = @$data['inputFields']['email'];
        $newData['phone_number'] = @$data['inputFields']['phone_number'];
          
        
         
          
        //        $result=DB::table('workflow_results')->insert(['data'=>$newData['email']]);
        

//        $user = User::where('portal_id', $newData['portal_id'])->value('id');
        $id= DB::table('application_records')->where('portal_id', $newData['portal_id'])->value('user_id');
       $newData['id']=$id;
        Auth::logout();
      
            $newData['select_method'] = DB::table('users')->where('id',$id)->value('sms_credentials_default_type');
                 
//        $extracted=json_encode($newData);
//           $result=DB::table('workflow_results')->insert(['response'=>$id,'data'=>$extracted]);
        
        if (!empty($id))
        {
          $user = User::where('id', $id)->first();
          Auth::login($user);
            
          
            $newData['message'] = @urlencode($data['inputFields']['message']);
//           $extracted=json_encode($newData);
//            $result=DB::table('workflow_results')->insert(['response'=>$data2,'data'=>$extracted]);
//            if ($newData['select_method'] == 'twilio')
//            {
//                $newData['message'] = @$data['inputFields']['message'];
//            }
            $recipients = $newData["phone_number"];
            $res = $this->validate_phone_number($recipients);
       
           
        
            if ($res['status'] == true)
            { 
                $extracted=json_encode($newData);
                $result=DB::table('workflow_results')->insert(['response'=>$data2,'data'=>$extracted]);  
                
                
                $workflows = DB::table('workflows')->insert(['data' => serialize($newData) ]);
                $result = $this->messageAction($newData['message'], $res['phone'], $newData['select_method'],$newData['email']);
            }
            else
            {
                $workflows = DB::table('workflows')->insert(['data' => 'Phone number not valid.']);
            }
        }
        else
        {
            $workflows = DB::table('workflows')->insert(['data' => 'Portal Id not found.']);
        }
    }

    /*
    
    public function definations() {
    
    //    "dataUrl":"https://phpstack-242348-1203567.cloudwaysapps.com/hubspot/smsdew/workflowSent",
    $header=[
    'Content-Type: application/json'
    ];
    $url = 'https://api.hubapi.com/automationextensions/v1/definitions?hapikey=e1964b7f-c16c-453e-9cc0-efcaeb1ced47&applicationId=226805';
    //         https://phpstack-242348-1570241.cloudwaysapps.com/hubspot/smsdew/check
    $data = '{
    "integrationAppId": "226805",
    "extensionName": " SMS Hub 3 ",
    "webhookUrl": "https://phpstack-242348-1203567.cloudwaysapps.com/hubspot/smsdew/workflowSent",
    "dataUrl":"https://phpstack-242348-1203567.cloudwaysapps.com/hubspot/smsdew/workflowSent",
    "fieldMetadata":[
        {
            "label": "Phone Number",
            "key": "phone_number",
            "fieldType": "TEXT",
            "values": [
                {
                    "type": "STATIC_VALUE",
                    "allowsMergeTags": true
                }
            ]
        },
        {
            "label": "Message",
            "key": "message",
            "fieldType": "TEXTAREA",
            "values": [
                {
                    "type": "STATIC_VALUE",
                    "allowsMergeTags": true
                }
            ]
        }
    ]
    }';
    
    $results = $this->SmsCurlAccess($url,$header,$data);
    //     print_r($results);
    //        die;
    }
    
    */

    public function definations()
    {
        // update
        // $args['url'] = 'https://api.hubapi.com/automationextensions/v1/definitions/2228?hapikey=e1964b7f-c16c-453e-9cc0-efcaeb1ced47&applicationId=235589';

        // $args['data'] = '{
        //     "integrationAppId": "235589",
        //     "extensionName": "SMS DEW",
        //     "id" : 2228,
        //     "version" : 0,
        //     "latestVersion" : 0,
        //     "webhookUrl": "https://apps.webdew.com/hubspot/smsdew/workflowSent",
        //     "dataUrl":"https://apps.webdew.com/hubspot/smsdew/workflowSent",
        //     "fieldMetadata": [
        //         {
        //             "label": "Email",
        //             "key": "email",
        //             "fieldType": "TEXT",
        //             "values": [
        //                 {
        //                     "type": "STATIC_VALUE",
        //                     "allowsMergeTags": true
        //                 }
        //             ]
        //         },
        //         {
        //             "label": "Mobile Number",
        //             "key": "phone_number",
        //             "fieldType": "TEXT",
        //             "values": [
        //                 {
        //                     "type": "STATIC_VALUE",
        //                     "allowsMergeTags": true
        //                 }
        //             ]
        //         },
        //         {
        //             "label": "Message",
        //             "key": "message",
        //             "fieldType": "TEXTAREA",
        //             "values": [
        //                 {
        //                     "type": "STATIC_VALUE",
        //                     "allowsMergeTags": true
        //                 }
        //             ]
        //         }
        //     ]
        // }';
        // update

        // create
                $args['url'] = 'https://api.hubapi.com/automationextensions/v1/definitions/2367?hapikey=e1964b7f-c16c-453e-9cc0-efcaeb1ced47&applicationId=235589';

        // $args['data'] = '{
        //     "integrationAppId": "235589",
        //     "extensionName": "All in One SMS Stage",
        //     "webhookUrl": "https://phpstack-401531-1635280.cloudwaysapps.com/hubspot/smsdew/workflowSent",
        //     "dataUrl":"https://phpstack-401531-1635280.cloudwaysapps.com/hubspot/smsdew/workflowSent",
        //     "fieldMetadata": [
        //         {
        //             "label": "Email",
        //             "key": "email",
        //             "fieldType": "TEXT",
        //             "values": [
        //                 {
        //                     "type": "STATIC_VALUE",
        //                     "allowsMergeTags": true
        //                 }
        //             ]
        //         },
        //         {
        //             "label": "Mobile Number",
        //             "key": "phone_number",
        //             "fieldType": "TEXT",
        //             "values": [
        //                 {
        //                     "type": "STATIC_VALUE",
        //                     "allowsMergeTags": true
        //                 }
        //             ]
        //         },
        //         {
        //             "label": "Message",
        //             "key": "message",
        //             "fieldType": "TEXTAREA",
        //             "values": [
        //                 {
        //                     "type": "STATIC_VALUE",
        //                     "allowsMergeTags": true
        //                 }
        //             ]
        //         }
        //     ]
        // }';
        // $result = $this->curlAccess('GET',$args);
        // echo '<pre>';
        // print_r($result);
        // die;

    }

    // old payment base code working
    // public function auth2_callback(Request $request)
    // {
    //     $paymentCheck = DB::table('payments')->where('user_id',Auth::user()->id)->where('app_id',1)->value('id');
    //     if ($paymentCheck > 0) { //payment is done

    //         if (env('APP_ENV') == 'live')
    //         {
    //             $provider = new \Flipbox\OAuth2\Client\Provider\HubSpot(['clientId' => config('services.live_smsdew.clientId') , 'clientSecret' => config('services.live_smsdew.clientSecret') , 'redirectUri' => url('/smsdew/auth2-callback') ]);
    //         }
    //         else
    //         {
    //             $provider = new \Flipbox\OAuth2\Client\Provider\HubSpot(['clientId' => config('services.stage_smsdew.clientId') , 'clientSecret' => config('services.stage_smsdew.clientSecret') , 'redirectUri' => url('/smsdew/auth2-callback') ]);
    //         }

    //         if (!isset($_GET['code']))
    //         {

    //             $options = ['scope' => ['contacts', 'timeline'] // array or string
    //             ];

    //             // If we don't have an authorization code then get one
    //             $authUrl = $provider->getAuthorizationUrl($options);

    //             // update
    //             header('Location: ' . $authUrl);
    //             exit;
    //             // Check given state against previously stored one to mitigate CSRF attack
                
    //         }
    //         else
    //         {

    //             $result = [];
    //             // Try to get an access token (using the authorization code grant)
    //             $token = $provider->getAccessToken('authorization_code', ['code' => $_GET['code']]);

    //             //            $_SESSION['token']=$token;
    //             // Optional: Now you have a token you can look up a users profile data
    //             try
    //             {

    //                 // We got an access token, let's now get the user's details
    //                 $user = $provider->getResourceOwner($token);

    //                 $result['portal_id'] = $user->getHubId();
    //                 $result['email'] = $user->getEmail();
    //                 $result['app_id'] = @$user->getAppId();
    //                 $result['refresh_token'] = @$token->getRefreshToken();
    //                 $result['expires_at'] = @$token->getExpires();
    //                 $result['user_expires_at'] = @$user->getExpires();
    //                 $result['code'] = $_GET['code'];
    //                 $result['expires_at'] = time() + $result['expires_at'] * 0.95;
    //                 $result['auth_token'] = $token->getToken();

    //                 $u = User::whereEmail($result['email'])->first();


    //                 $user_id = $u->id;
    //                 $portal_id = $result['portal_id'];
    //                 $refresh_token = $result['refresh_token'];
    //                 $expiry_time = $result['expires_at'];

    //                 /*===============
    //                 |implementing payment check
    //                 |================
    //                 */
    //                 $email = $user->getEmail();

    //                 $paymentCheck = DB::table('payments')->where('user_id',Auth::user()->id)->where('app_id',1)->value('id');
                    
    //                 if ($paymentCheck > 0) { //payment is done

    //                     /*===============
    //                     |Select query to check if application was already intalled in the same poratl or not
    //                     |================
    //                     */

    //                     $record_check = DB::table('application_records')->where('user_id',Auth::user()->id)->where('portal_id',$portal_id)->value('id');

    //                     if ($record_check > 0)
    //                     {
    //                         DB::table('application_records')->where('user_id',Auth::user()->id)->where('portal_id',$portal_id)->update([
    //                                     'refresh_token' => $refresh_token,
    //                                     'expiry_time' => $expiry_time
    //                                 ]);
    //                     } else {

    //                         /*===============
    //                         |insert code in new table for storing refresh token for different portals
    //                         |================
    //                         */

    //                         $insert = DB::table('application_records')->insertGetId([
    //                                     'user_id' => $user_id,
    //                                     'portal_id' => $portal_id,
    //                                     'client_id' => '',
    //                                     'secret_id' => '',
    //                                     'refresh_token' => $refresh_token,
    //                                     'expiry_time' => $expiry_time
    //                                 ]);

    //                     }
    //                     /*===============
    //                     |insert code end
    //                     |================
    //                     */
    //                     if (empty($u->id))
    //                     {
    //                         $validatedData['user_type'] = 'users';
    //                         $validatedData['activation_code'] = str_random(30) . time();
    //                         $validatedData['user_email_verified'] = 1;
    //                         $validatedData['user_fname'] = 'New';
    //                         $validatedData['user_lname'] = 'User';
    //                         $validatedData['email'] = $result['email'];
    //                         $validatedData['user_status'] = 1;
    //                         $validatedData['vid'] = 0;
    //                         app(User::class)->create($validatedData);
    //                         $u = User::whereEmail($result['email'])->first();
    //                     }
    //                     //Auth::login($u);

    //                     $mapped_app_id = DB::table('mapped_apps')->where('user_id', Auth::user()
    //                         ->id)
    //                         ->where('fk_app_id', 1)
    //                         ->value('mapped_app_id');
    //                     if (empty($mapped_app_id))
    //                     {
    //                         DB::table('mapped_apps')->insert(['fk_app_id' => 1, 'user_id' => Auth::user()->id]);
    //                     }

    //                     $scopes = @serialize($user->getScopes());
    //                     // update
    //                     $users = User::find(Auth::user()->id);
    //                     $users->portal_id = $result['portal_id'];
    //                     $users->hub_email = $user->getEmail();
    //                     $users->scopes = $scopes;
    //                     $users->app_id = @$user->getAppId();
    //                     $users->refresh_token = @$token->getRefreshToken();
    //                     $users->expires_at = @$token->getExpires();
    //                     $users->code = $_GET['code'];
    //                     $users->sms_credentials_default_type = 'msg91';
    //                     $users->auth_token = $token->getToken();
    //                     $users->save();
    //                     // update
                        

    //                     // redirect to hubspot
    //                     $hubspotUrl = 'https://app.hubspot.com/';
    //                     if (!empty($result['portal_id']) && isset($result['portal_id']))
    //                     {
                            
    //                         $hubspotUrl = 'https://app.hubspot.com/integrations-settings/'.$result['portal_id'].'/installed';
    //                         //$hubspotUrl = 'https://app.hubspot.com/home?portalID=' . $result['portal_id'];
    //                     }
    //                     header('Location: ' . $hubspotUrl);
    //                     exit;
    //                     // redirect to hubspot
                        

    //                     // $result['portal_id'] = $user->getHubId();
    //                     // $result['email'] = $user->getEmail();
    //                     // $result['app_id'] = @$user->getAppId();
    //                     // $result['refresh_token'] = @$token->getRefreshToken();
    //                     // $result['expires_at'] = @$token->getExpires();
    //                     // $result['user_expires_at'] = @$user->getExpires();
    //                     // $result['code'] = $_GET['code'];
    //                     // $result['expires_at'] = time() + $result['expires_at'] * 0.95;
    //                     // $result['auth_token'] = $token->getToken();
    //                     // $result['user_fname'] = 'User';
    //                     // $result['oauth2state'] = @$_GET['state'];
    //                     // $result['user_email_verified'] = 1;
    //                     // $result['user_status'] = 1;
    //                     // $result['sms_credentials_default_type'] = '';
    //                     //   $scopes = @serialize($user->getScopes());
    //                     //   if(!empty($result['email'])){
    //                     //         $user_result = User::where('email',$result['email'])->first();
    //                     //         $count = User::where('email',$result['email'])->count();
    //                     //         if($count > 0){
    //                     //             Auth::login($user_result);
    //                     //             // update
    //                     //             $users = User::find(Auth::user()->id);
    //                     //             $users->portal_id = $result['portal_id'];
    //                     //             $users->hub_email = $user->getEmail();
    //                     //             $users->scopes =  $scopes;
    //                     //             $users->app_id = @$user->getAppId();
    //                     //             $users->refresh_token = @$token->getRefreshToken();
    //                     //             $users->expires_at = @$token->getExpires();
    //                     //             $users->code = $_GET['code'];
    //                     //             $users->auth_token = $token->getToken();
    //                     //             $users->save();
    //                     //             $mapped_app_id = DB::table('mapped_apps')->where('user_id',Auth::user()->id())->where('fk_app_id',1)->value('mapped_app_id');
    //                     //             if(empty($mapped_app_id)){
    //                     //                 DB::table('mapped_apps')->insert(['fk_app_id'=> 1,'user_id'=> Auth::user()->id()]);
    //                     //             }
    //                     //             // update
    //                     //         } else {
    //                     //             unset($result['user_expires_at']);
    //                     //             User::create($result);
    //                     //             $user_result = User::where('email',$result['email'])->first();
    //                     //             Auth::login($user_result);
    //                     //             $mapped_app_id = DB::table('mapped_apps')->where('user_id',Auth::user()->id())->where('fk_app_id',1)->value('mapped_app_id');
    //                     //             if(empty($mapped_app_id)){
    //                     //                 DB::table('mapped_apps')->insert(['fk_app_id'=> 1,'user_id'=> Auth::user()->id()]);
    //                     //             }
    //                     //         }
    //                     //   }
    //                     // $args['data'] = '{
    //                     //       "properties": [
                                
    //                     //         {
    //                     //           "property": "email",
    //                     //           "value": "'.$result['email'].'"
    //                     //         },
    //                     //         {
    //                     //           "property": "refresh_token",
    //                     //           "value": "'.$result['refresh_token'].'"
    //                     //         },
    //                     //         {
    //                     //           "property": "token",
    //                     //           "value": "'.$result['auth_token'].'"
    //                     //         },
    //                     //         {
    //                     //           "property": "code",
    //                     //           "value": "'.$result['code'].'"
    //                     //         },
    //                     //         {
    //                     //           "property": "app_id",
    //                     //           "value": "'.$result['app_id'].'"
    //                     //         }
    //                     //       ]
    //                     //     }';
    //                     // ;
    //                     // $args['url'] = 'https://api.hubapi.com/contacts/v1/contact/email/'.$result['email'].'/profile?hapikey='.env('HUBSPOT_API_KEY');
    //                     // $store = $this->curlAccess( 'POST', $args );
    //                     // Use these details to create a new profile
                        
    //                 } else { 
    //                     return redirect('all-in-one-sms');
    //                     exit;
    //                 }
    //             }
    //             catch(Exception $e)
    //             {
    //                 // Failed to get user details
    //                 exit('Oh dear...');
    //             }
    //             // Use this to interact with an API on the users behalf
    //             return redirect('/');
    //         }
    //     } else {
    //         return redirect('all-in-one-sms');
    //     }
    // }
    // old payment base code working 

    public function auth2_callback(Request $request)
    {
        // $paymentCheck = DB::table('payments')->where('user_id',Auth::user()->id)->where('app_id',1)->value('id');
        // if ($paymentCheck > 0) { //payment is done

            if (env('APP_ENV') == 'live')
            {
                $provider = new \Flipbox\OAuth2\Client\Provider\HubSpot(['clientId' => config('services.live_smsdew.clientId') , 'clientSecret' => config('services.live_smsdew.clientSecret') , 'redirectUri' => url('/smsdew/auth2-callback') ]);
            }
            else
            {
                $provider = new \Flipbox\OAuth2\Client\Provider\HubSpot(['clientId' => config('services.stage_smsdew.clientId') , 'clientSecret' => config('services.stage_smsdew.clientSecret') , 'redirectUri' => url('/smsdew/auth2-callback') ]);
            }

            if (!isset($_GET['code']))
            {

                $options = ['scope' => ['contacts', 'timeline'] // array or string
                ];

                // If we don't have an authorization code then get one
                $authUrl = $provider->getAuthorizationUrl($options);

                // update
                header('Location: ' . $authUrl);
                exit;
                // Check given state against previously stored one to mitigate CSRF attack
                
            }
            else
            {

                $result = [];
                // Try to get an access token (using the authorization code grant)
                $token = $provider->getAccessToken('authorization_code', ['code' => $_GET['code']]);

                //            $_SESSION['token']=$token;
                // Optional: Now you have a token you can look up a users profile data
                try
                {

                    // We got an access token, let's now get the user's details
                    $user = $provider->getResourceOwner($token);

                    $result['portal_id'] = $user->getHubId();
                    $result['email'] = $user->getEmail();
                    $result['app_id'] = @$user->getAppId();
                    $result['refresh_token'] = @$token->getRefreshToken();
                    $result['expires_at'] = @$token->getExpires();
                    $result['user_expires_at'] = @$user->getExpires();
                    $result['code'] = $_GET['code'];
                    $result['expires_at'] = time() + $result['expires_at'] * 0.95;
                    $result['auth_token'] = $token->getToken();

                    $u = User::whereEmail($result['email'])->first();

                    /*===============
                        |insert code end
                        |================
                        */
                        if (empty($u->id))
                        {
                            $validatedData['user_type'] = 'users';
                            $validatedData['activation_code'] = str_random(30) . time();
                            $validatedData['user_email_verified'] = 1;
                            $validatedData['user_fname'] = 'New';
                            $validatedData['user_lname'] = 'User';
                            $validatedData['email'] = $result['email'];
                            $validatedData['user_status'] = 1;
                            $validatedData['vid'] = 0;
                            app(User::class)->create($validatedData);
                            $u = User::whereEmail($result['email'])->first();
                        }
                        Auth::login($u);

                        $mapped_app_id = DB::table('mapped_apps')->where('user_id', Auth::user()
                            ->id)
                            ->where('fk_app_id', 1)
                            ->value('mapped_app_id');
                        if (empty($mapped_app_id))
                        {
                            DB::table('mapped_apps')->insert(['fk_app_id' => 1, 'user_id' => Auth::user()->id]);
                        }

                        $scopes = @serialize($user->getScopes());
                        // update
                        $users = User::find(Auth::user()->id);
                        $users->portal_id = $result['portal_id'];
                        $users->hub_email = $user->getEmail();
                        $users->scopes = $scopes;
                        $users->app_id = @$user->getAppId();
                        $users->refresh_token = @$token->getRefreshToken();
                        $users->expires_at = @$token->getExpires();
                        $users->code = $_GET['code'];
                        $users->sms_credentials_default_type = 'msg91';
                        $users->auth_token = $token->getToken();
                        $users->save();
                        // update
                        

                    //$user_id = $u->id;
                    $user_id = Auth::user()->id;
                    $portal_id = $result['portal_id'];
                    $refresh_token = $result['refresh_token'];
                    $expiry_time = $result['expires_at'];

                    /*===============
                    |implementing payment check
                    |================
                    */
                    $email = $user->getEmail();

                    $paymentCheck = DB::table('payments')->where('user_id',Auth::user()->id)->where('app_id',1)->value('id');
                    
                    if ($paymentCheck > 0) { //payment is done

                        /*===============
                        |Select query to check if application was already intalled in the same poratl or not
                        |================
                        */

                        $record_check = DB::table('application_records')->where('user_id',Auth::user()->id)->where('portal_id',$portal_id)->value('id');

                        if ($record_check > 0)
                        {
                            DB::table('application_records')->where('user_id',Auth::user()->id)->where('portal_id',$portal_id)->update([
                                        'refresh_token' => $refresh_token,
                                        'expiry_time' => $expiry_time
                                    ]);
                        } else {

                            /*===============
                            |insert code in new table for storing refresh token for different portals
                            |================
                            */

                            $insert = DB::table('application_records')->insertGetId([
                                        'user_id' => $user_id,
                                        'portal_id' => $portal_id,
                                        'client_id' => '',
                                        'secret_id' => '',
                                        'refresh_token' => $refresh_token,
                                        'expiry_time' => $expiry_time
                                    ]);

                        }

                        // redirect to hubspot
                        $hubspotUrl = 'https://app.hubspot.com/';
                        if (!empty($result['portal_id']) && isset($result['portal_id']))
                        {
                            
                            $hubspotUrl = 'https://app.hubspot.com/integrations-settings/'.$result['portal_id'].'/installed';
                            //$hubspotUrl = 'https://app.hubspot.com/home?portalID=' . $result['portal_id'];
                        }
                        header('Location: ' . $hubspotUrl);
                        exit;
                        // redirect to hubspot
                        

                        // $result['portal_id'] = $user->getHubId();
                        // $result['email'] = $user->getEmail();
                        // $result['app_id'] = @$user->getAppId();
                        // $result['refresh_token'] = @$token->getRefreshToken();
                        // $result['expires_at'] = @$token->getExpires();
                        // $result['user_expires_at'] = @$user->getExpires();
                        // $result['code'] = $_GET['code'];
                        // $result['expires_at'] = time() + $result['expires_at'] * 0.95;
                        // $result['auth_token'] = $token->getToken();
                        // $result['user_fname'] = 'User';
                        // $result['oauth2state'] = @$_GET['state'];
                        // $result['user_email_verified'] = 1;
                        // $result['user_status'] = 1;
                        // $result['sms_credentials_default_type'] = '';
                        //   $scopes = @serialize($user->getScopes());
                        //   if(!empty($result['email'])){
                        //         $user_result = User::where('email',$result['email'])->first();
                        //         $count = User::where('email',$result['email'])->count();
                        //         if($count > 0){
                        //             Auth::login($user_result);
                        //             // update
                        //             $users = User::find(Auth::user()->id);
                        //             $users->portal_id = $result['portal_id'];
                        //             $users->hub_email = $user->getEmail();
                        //             $users->scopes =  $scopes;
                        //             $users->app_id = @$user->getAppId();
                        //             $users->refresh_token = @$token->getRefreshToken();
                        //             $users->expires_at = @$token->getExpires();
                        //             $users->code = $_GET['code'];
                        //             $users->auth_token = $token->getToken();
                        //             $users->save();
                        //             $mapped_app_id = DB::table('mapped_apps')->where('user_id',Auth::user()->id())->where('fk_app_id',1)->value('mapped_app_id');
                        //             if(empty($mapped_app_id)){
                        //                 DB::table('mapped_apps')->insert(['fk_app_id'=> 1,'user_id'=> Auth::user()->id()]);
                        //             }
                        //             // update
                        //         } else {
                        //             unset($result['user_expires_at']);
                        //             User::create($result);
                        //             $user_result = User::where('email',$result['email'])->first();
                        //             Auth::login($user_result);
                        //             $mapped_app_id = DB::table('mapped_apps')->where('user_id',Auth::user()->id())->where('fk_app_id',1)->value('mapped_app_id');
                        //             if(empty($mapped_app_id)){
                        //                 DB::table('mapped_apps')->insert(['fk_app_id'=> 1,'user_id'=> Auth::user()->id()]);
                        //             }
                        //         }
                        //   }
                        // $args['data'] = '{
                        //       "properties": [
                                
                        //         {
                        //           "property": "email",
                        //           "value": "'.$result['email'].'"
                        //         },
                        //         {
                        //           "property": "refresh_token",
                        //           "value": "'.$result['refresh_token'].'"
                        //         },
                        //         {
                        //           "property": "token",
                        //           "value": "'.$result['auth_token'].'"
                        //         },
                        //         {
                        //           "property": "code",
                        //           "value": "'.$result['code'].'"
                        //         },
                        //         {
                        //           "property": "app_id",
                        //           "value": "'.$result['app_id'].'"
                        //         }
                        //       ]
                        //     }';
                        // ;
                        // $args['url'] = 'https://api.hubapi.com/contacts/v1/contact/email/'.$result['email'].'/profile?hapikey='.env('HUBSPOT_API_KEY');
                        // $store = $this->curlAccess( 'POST', $args );
                        // Use these details to create a new profile
                        
                    } else { 

                        $paymentCheck = DB::table('payments')->insertGetId([
                                                                'app_id' => 1 ,
                                                                'user_id' => Auth::user()->id,
                                                                'stripe_id' => 'manual',
                                                                'amount' => 0,
                                                                'currency_code' => 'usd',
                                                                'status' => 'succeeded',
                                                                'app_type' => 'smsdew',
                                                                'subscription_id' => 0
                                                            ]);

                        /*===============
                        |Select query to check if application was already intalled in the same poratl or not
                        |================
                        */

                        $record_check = DB::table('application_records')->where('user_id',Auth::user()->id)->where('portal_id',$portal_id)->value('id');

                        if ($record_check > 0)
                        {
                            DB::table('application_records')->where('user_id',Auth::user()->id)->where('portal_id',$portal_id)->update([
                                        'refresh_token' => $refresh_token,
                                        'expiry_time' => $expiry_time
                                    ]);
                        } else {

                            /*===============
                            |insert code in new table for storing refresh token for different portals
                            |================
                            */

                            $insert = DB::table('application_records')->insertGetId([
                                        'user_id' => $user_id,
                                        'portal_id' => $portal_id,
                                        'client_id' => '',
                                        'secret_id' => '',
                                        'refresh_token' => $refresh_token,
                                        'expiry_time' => $expiry_time
                                    ]);

                        }

                        // redirect to hubspot
                        $hubspotUrl = 'https://app.hubspot.com/';
                        if (!empty($result['portal_id']) && isset($result['portal_id']))
                        {
                            
                            $hubspotUrl = 'https://app.hubspot.com/integrations-settings/'.$result['portal_id'].'/installed';
                            //$hubspotUrl = 'https://app.hubspot.com/home?portalID=' . $result['portal_id'];
                        }
                        header('Location: ' . $hubspotUrl);
                        exit;
                        // redirect to hubspot

                        //return redirect('all-in-one-sms');
                        //exit;
                    }
                }
                catch(Exception $e)
                {
                    // Failed to get user details
                    exit('Oh dear...');
                }
                // Use this to interact with an API on the users behalf
                return redirect('/');
            }
        // } else {
        //     return redirect('all-in-one-sms');
        // }
    }
    // for creating defination
    public function terms_service()
    {
        return view('smsdew.websites.terms-service');
    }
    // for creating defination
    public function policy()
    {
        return view('smsdew.websites.policy');
    }

    public function documentation()
    {

        return view('smsdew.websites.documentation');
    }

    public function tagline()
    {
        return view('smsdew.websites.tagline');
    }

    public function home()
    {
        return view('smsdew.websites.home');
    }

    /////////////////////
    private function SmsCurlAccess($url, $header, $body)
    {

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $body,
            CURLOPT_HTTPHEADER => $header,
        ));

        $response = curl_exec($curl);
        curl_close($curl);
        //                           $check=json_decode($response);
        //                            print_r($check);
        return $response;

    }

    private function smsActivity($url, $header, $body)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $body,
            CURLOPT_HTTPHEADER => $header,
        ));

        $response = curl_exec($curl);
        curl_close($curl);
        //                           $check=json_decode($response);
        //                            print_r($check);
        return $response;
    }

    private function owner($hapikey)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.hubapi.com/crm/v3/owners/?limit=100&hapikey=$hapikey",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'Cookie: __cfduid=d15f14d1984c6b921f0319eb10ff9cce51606378583'
            ) ,
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return $response;
    }

    
    // public function handleGet()
    // {
    //     try
    //     {
    //         return view('pay');
    //     }
    //     catch(\throwable $err)
    //     {
    //         return redirect('https://phpstack-242348-1570241.cloudwaysapps.com/hubspot/smsdew/');
    //     }
    // }

    /**
     * handling payment with POST
     */
    // public function handlePost(Request $request)
    // {

    //     try
    //     {
    //         Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
    //         $response = Stripe\Charge::create(["amount" => 19 * 100, "currency" => "usd", "source" => $request->stripeToken, "description" => "Making test payment."]);

    //         Session::flash('success', 'Payment has been successfully processed.');

    //         $id = $response->id;
    //         $amount = '19';
    //         $currency = $response->currency;
    //         $customer_name = $request
    //             ->request
    //             ->get('cardholder');
    //         $status = $response->status;
    //         $email = $response
    //             ->billing_details->name;
    //         $res = json_encode($response);
    //         $payment_status = $response->paid;
    //         $email = $response
    //             ->billing_details->name;

    //         //email already exists check
    //         $email_count = DB::select("select count(*) AS email_count  from tbl_payments where email='$email'");
    //         $email_check = $email_count[0]->email_count;

    //         if ($email_check == '0')
    //         { //enters this code only if email is unique
    //             if ($payment_status == '1')
    //             {
    //                 Session::flash('success', 'Payment has been successfully processed.');
    //                 /*________________________
    //                 |
    //                 |  this check is for checking if user is in our contact already or not
    //                 |__________________________
                    
    //                 */
    //                 $email_check = $this->contactCheck($email);
    //                 $email_response_array = json_decode($email_check);
    //                 try
    //                 {
    //                     $status = $email_response_array->status;
    //                     if ($status == "error")
    //                     {
    //                         $check = '0';

    //                     }

    //                 }
    //                 catch(\throwable $err)
    //                 {
    //                     $check = '1';
    //                     $vid = $email_response_array->vid;
    //                 }
    //                 if ($check == '1')
    //                 {
    //                     //user will enter here only if he is a contact
    //                     //                   $vid=DB::select("select vid from users where hub_email='$email'");
    //                     /*________________________
    //                     |
    //                     |  Creating deal  --- parameters required:hapikey,companyid,contactid,ownerid,and all other details of deal..like amount, name of deal,creation date,
    //                     |__________________________
                        
    //                     */
    //                     $timestamp = time() * 1000;

    //                     $url = 'https://api.hubapi.com/deals/v1/deal?hapikey=0fab6b1c-02b4-49d4-ab72-15a1f13a6642';
    //                     $header = ['Content-Type: application/json'];
    //                     $data = '{
    //           "associations": {
    //             "associatedCompanyIds": [
    //               4903564852  
    //             ],
    //             "associatedVids": [
    //               ' . $vid . '
    //             ]
    //           },
    //           "properties": [
    //             {
    //               "value": "sms-dew Deal",
    //               "name": "dealname"
    //             },
    //             {
    //               "value": "appointmentscheduled",
    //               "name": "dealstage"
    //             },
    //             {
    //               "value": "default",
    //               "name": "pipeline"
    //             },
    //             {
    //               "value": "51771313",
    //               "name": "hubspot_owner_id"
    //             },
    //             {
    //               "value": ' . $timestamp . ',
    //               "name": "closedate"
    //             },
    //             {
    //               "value": "19",
    //               "name": "amount"
    //             },
    //             {
    //               "value": "newbusiness",
    //               "name": "dealtype"
    //             }
    //           ]
    //         }';

    //                     $this->curlCode($url, $header, $data);

    //                 }
    //                 else
    //                 {
    //                     $url = "https://api.hubapi.com/contacts/v1/contact/?hapikey=0fab6b1c-02b4-49d4-ab72-15a1f13a6642";
    //                     $body = '{
    //               "properties": [
    //                 {
    //                   "property": "email",
    //                   "value": "' . $email . '"
    //                 }
    //               ]
    //             }';

    //                     $header = ['Content-Type: application/json'];
    //                     $result = $this->curlCode($url, $header, $body);
    //                     $re = json_decode($result);
    //                     //                   print_r(var_dump($res));
    //                     $vi = $re->vid;
    //                     //                   $portal=$res->portal-id;
    //                     $mail = $re
    //                         ->properties
    //                         ->email->value;
    //                     //                   echo "email = $mail" ;
    //                     //                   echo "portal_id = $portal";
    //                     //                   echo "vid = $vi";
    //                     //                    print_r($res);
                        

    //                     $timestamp = time() * 1000;

    //                     $url = 'https://api.hubapi.com/deals/v1/deal?hapikey=0fab6b1c-02b4-49d4-ab72-15a1f13a6642';
    //                     $header = ['Content-Type: application/json'];
    //                     $data = '{
    //           "associations": {
    //             "associatedCompanyIds": [
    //               4903564852  
    //             ],
    //             "associatedVids": [
    //               ' . $vi . '
    //             ]
    //           },
    //           "properties": [
    //             {
    //               "value": "sms-dew Deal",
    //               "name": "dealname"
    //             },
    //             {
    //               "value": "appointmentscheduled",
    //               "name": "dealstage"
    //             },
    //             {
    //               "value": "default",
    //               "name": "pipeline"
    //             },
    //             {
    //               "value": "51771313",
    //               "name": "hubspot_owner_id"
    //             },
    //             {
    //               "value": ' . $timestamp . ',
    //               "name": "closedate"
    //             },
    //             {
    //               "value": "19",
    //               "name": "amount"
    //             },
    //             {
    //               "value": "newbusiness",
    //               "name": "dealtype"
    //             }
    //           ]
    //         }';

    //                     $this->curlCode($url, $header, $data);

    //                 }

    //                 $app_name = 'sms dew';
    //                 $insert = DB::insert('insert into tbl_payments(email,app_name,stripe_id,amount,currency_code,payment_status,payment_response) values(?,?,?,?,?,?,?)', ["$email", "$app_name", "$id", "$amount", "$currency", "$status", "$res"]);

    //                 return Redirect::to("https://app.hubspot.com/oauth/authorize?client_id=27045765-d704-4ca3-b3a5-2a936f4ab4c0&redirect_uri=https://apps.webdew.com/hubspot/smsdew/auth2-callback&scope=contacts%20timeline");
    //             }
    //             else
    //             {
    //                 $posts = 1;
    //                 Session::flash('transaction-failed', 'transaction failed.please try again.');
    //                 return view('smsdew.websites.home', compact("posts"));
    //             }
    //         }
    //         else
    //         {
    //             //              echo "<script>";
    //             //              echo "alert('email already exists')";
    //             //              echo "<script>";
    //             //            $posts='fail';
    //             Session::flash('mailexists', 'email already exists');
    //             $posts = 1;
    //             //               return view('smsdew.websites.home');
    //             return view("smsdew.websites.home", compact("posts"));
    //         }

    //         return back();
    //     }
    //     catch(\throwable $e)
    //     {
    //         echo 'Message: ' . $e->getMessage();
    //     }
    // }

    // public function stripepay()
    // {
    //     return view('render.smsdew.pay');
    // }

    public function curlCode($url, $header, $body)
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $body,
            CURLOPT_HTTPHEADER => $header,
        ));

        $response = curl_exec($curl);
        curl_close($curl);
        //                           $check=json_decode($response);
        //                            print_r($check);
        return $response;
    }

    // public function contactCheck($email)
    // {

    //     $curl = curl_init();

    //     curl_setopt_array($curl, array(
    //         CURLOPT_URL => 'https://api.hubapi.com/contacts/v1/contact/email/' . $email . '/profile?hapikey=58723188-d3ff-473b-b9b0-24c927e10c10',
    //         CURLOPT_RETURNTRANSFER => true,
    //         CURLOPT_ENCODING => '',
    //         CURLOPT_MAXREDIRS => 10,
    //         CURLOPT_TIMEOUT => 0,
    //         CURLOPT_FOLLOWLOCATION => true,
    //         CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    //         CURLOPT_CUSTOMREQUEST => 'GET',
    //         CURLOPT_HTTPHEADER => array(
    //             'Cookie: __cfduid=d15f14d1984c6b921f0319eb10ff9cce51606378583'
    //         ) ,
    //     ));

    //     $response = curl_exec($curl);

    //     curl_close($curl);
    //     return $response;

    // }

    public function errorActivityJson($hub_api_key, $ownerid, $contact_id, $phone)
    {
        $timestamp = time() * 1000;

        $url = "https://api.hubapi.com/engagements/v1/engagements?hapikey=$hub_api_key";

        $header = ['Content-Type: application/json'];
        $data = '{
                            "engagement": {
                                "active": true,
                                "ownerId": ' . $ownerid . ',
                                "type": "NOTE",
                                "timestamp": ' . $timestamp . '
                            },
                            "associations": {
                                "contactIds": [' . $contact_id . '],
                                "companyIds": [ ],
                                "dealIds": [ ],
                                "ownerIds": [ ],
                                "ticketIds":[ ]
                            },
                            "attachments": [
                                {
                                    "id": 4241968539
                                }
                            ],
                            "metadata":  {
                                "body":"<div style=\"background-color:#ffe6e6;padding-top:5px;padding-left:15px;padding-right:15px;border:2px solid red; \"><b>There was an issue sending message to this contact(' . $phone . ')</b></br> Your account was suspending from sending message due to non-compliance or deliverabilty issues.</div>"   
                                }
                        }';

        $activity = $this->smsActivity($url, $header, $data);
        return $activity;

    }

    public function successfullActivityJson($hub_api_key, $ownerid, $contact_id, $body, $phone, $vendor, $ownername)
    {

        $timestamp = time() * 1000;

        $url = "https://api.hubapi.com/engagements/v1/engagements?hapikey=$hub_api_key";

        $header = ['Content-Type: application/json'];
        $success_data = '{
                            "engagement": {
                                "active": true,
                                "ownerId": ' . $ownerid . ',
                                "type": "NOTE",
                                "timestamp": ' . $timestamp . '
                            },
                            "associations": {
                                "contactIds": [' . $contact_id . '],
                                "companyIds": [ ],
                                "dealIds": [ ],
                                "ownerIds": [ ],
                                "ticketIds":[ ]
                            },
                            "attachments": [
                                {
                                    "id": 4241968539
                                }
                            ],
                            "metadata": {
                                "body": "message: ' . $body . ' </br></br> To:' . $phone . ' </br> </br> vendor:' . $vendor . ' </br> </br> owner:' . $ownername . ' </br> </br>Thanks!"  
                                }
                        }';

        $activity = $this->smsActivity($url, $header, $success_data);

        return $activity;
    }
    
    public function all_in_sms(){

        return view('smsdew.all-in-one-sms');
    }

    public function index() {   
        return view('all-app');
    }
}
        
