@extends('layout.login')
@php
$totalTitle = 'Data Sync';
@endphp
@section('title', $totalTitle)
@section('title-description')

<meta name="description" content="Data Sync">
    
@endsection
@section('content')
@section('head')
@parent    
@show
<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor bg-white mt-5" id="kt_content">

        <!-- begin:: Content -->
        <div class="kt-container  kt-grid__item kt-grid__item--fluid home-container">
           
            <div class="row">
                <!-- body content start -->
                <div class="col-12">
                    <div class="kt_blog-post  pb-5">
                        
                        <div class="row justify-content-center">
                            <div class="col-xl-8 col-lg-8 col-md-12">
                                <span id="alert-span"></span>
                                @if (!empty($alert))
                                <div class="alert alert-{{$alert}}">
                                    {{ $message }}
                                </div>
                                @endif
                                @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif

                                <div class="row">
                                    <div class="col">
                                        <div class="card">
                                            <div class="card-header">
                                                <label>Sync Data</label>
                                            </div>
                                            <div class="card-body" id="vendor-append">
                                            <button id="product" class="btn btn-label-brand btn-bold sync" rel="products">Sync Cart link</button>
                                            <!--
                                                @if(empty($result->workflows))
                                                    <button id="product" class="btn btn-label-brand btn-bold sync" rel="products">Create Workflow</button>
                                                @else
                                                    @foreach($result->workflows as $key => $value)
                                                    <?php
                                                        // if($value->name == 'Workflow for product Sync'){
                                                        //     echo '<b>NAME : </b>';
                                                        //     echo $value->name;
                                                        //     break;
                                                        // }
                                                        
                                                    ?>
                                                    @endforeach
                                                @endif
                                                -->
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>                                  
                    </div>
                </div>
                <!-- body content end -->
            </div>
        </div>
    </div>                  
</div>

@section('scripts')
@parent

  <!--begin::Page Scripts(used by this page) -->
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
        <script type="text/javascript">

        jQuery(document).on('click','.sync',function(e){
            var url = "{{url('get-product-list-hub')}}"+'/'+"{{Auth::user()->id}}";
            $('#alert-span').html('');
            e.preventDefault();
            $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    beforeSend: function() {
                        // setting a timeout
                        $(".loader").fadeIn("slow");
                    },
                    url  : url,
                    type : 'GET',
                    async: true,
                    enctype: 'multipart/form-data',
                    cache: false,                      
                    success: function(response){
                        if(response.status === true){
                            $('#vendor-append').html(response.html);
                            $('#alert-span').html('<div class="alert alert-success">'+response.message+'</div>');
                        } else {
                            $('#alert-span').html('<div class="alert alert-danger">'+response.message+'</div>');
                        }
                        $(".loader").fadeOut("slow");
                    },
                    error: function(){}
                });
                e.stopImmediatePropagation();
                return false; 
            });

        </script>     
@show
@endsection