
	<!--begin: User bar -->
	<div class="kt-header__topbar-item kt-header__topbar-item--user">
		<div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,0px">
			<span class="kt-header__topbar-welcome kt-visible-desktop">Hi,</span>
			<span class="kt-header__topbar-username kt-visible-desktop">{{Auth::user()->user_fname}}</span>
			@php
                $image = @Auth::user()->image;
                $name = '';
                $lname = '';
            @endphp
            @if(!empty($image))
                @php
                    $image = url('storage/app/') . '/' . $image;
                @endphp
            @else
                @php
                    $name = strtoupper($rest = substr(@Auth::user()->user_fname, 0, 1));
                    $lname = strtoupper($rest = substr(@Auth::user()->user_lname, 0, 1));
                @endphp
            @endif
            @if(!empty($image))
                
                    <img src="{{$image}}" alt="Pic">           
                
            @else
                @php
                    $image = asset('') . config('app.public_url') .'/assets/media/users/default.jpg';
                @endphp
                @if(!empty($name))                    
                    <!--use below badge element instead the user avatar to display username's first letter(remove kt-hidden class to display it) -->
					<span class="kt-badge kt-badge--username kt-badge--unified-primary kt-badge--lg kt-badge--rounded kt-badge--bold">{{$name}}{{$lname}}</span>
                @else
                    <img src="{{$image}}" alt="Pic">
                @endif
            @endif
			
		</div>
		 
		<div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-xl">

			<!--begin: Head -->
			<div class="kt-user-card kt-user-card--skin-light kt-notification-item-padding-x">
				<div class="kt-user-card__avatar">

					@php
		                $image = @Auth::user()->image;
		                $name = '';
		                $lname = '';
		            @endphp
		            @if(!empty($image))
		                @php
		                    $image = url('storage/app/') . '/' . $image;
		                @endphp
		            @else
		                @php
		                    $name = strtoupper($rest = substr(@Auth::user()->user_fname, 0, 1));
		                    $lname = strtoupper($rest = substr(@Auth::user()->user_lname, 0, 1));
		                @endphp
		            @endif
		            @if(!empty($image))
		                
		                    <img src="{{$image}}" class="kt-hidden-" alt="Pic" >
		                
		            @else
		                @php
		                    $image = asset('') . config('app.public_url') .'/assets/media/users/default.jpg';
		                @endphp
		                @if(!empty($name) || !empty($lname))
							<!--use below badge element instead the user avatar to display username's first letter(remove kt-hidden class to display it) -->
							<span class="kt-badge kt-badge--username kt-badge--unified-primary kt-badge--lg kt-badge--rounded kt-badge--bold">{{$name}}{{$lname}}</span>
		                @else
		                    <img src="{{$image}}" class="kt-hidden-" alt="Pic" >
		                @endif
		            @endif
					
				</div>
				<div class="kt-user-card__name">
					{{Auth::user()->user_fname}} {{Auth::user()->user_lname}}
				</div>
				@if(Auth::user()->user_type != 'admin')
				<!-- <div class="kt-user-card__badge">
					<span class="btn btn-label-primary btn-sm btn-bold btn-font-md">{{ucfirst(Auth::user()->switched)}}</span>
				</div> -->
				@else
				<!-- <div class="kt-user-card__badge">
					<span class="btn btn-label-primary btn-sm btn-bold btn-font-md">{{ucfirst(Auth::user()->user_type)}}</span>
				</div> -->
				@endif
			</div>

			<!--end: Head -->

			<!--begin: Navigation -->

			<div class="kt-notification">
				@include('layout.include.partials.right-sidebar')
				
				<div class="kt-notification__custom kt-space-between">				
					<a class="btn btn-label btn-label-brand btn-sm btn-bold" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();">
                        {{ __('Sign Out') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
				</div>
			</div>

			<!--end: Navigation -->
		</div>
		
	</div>
