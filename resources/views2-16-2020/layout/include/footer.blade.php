<!-- begin:: Footer -->
<div class="kt-footer kt-grid__item" id="kt_footer">
	<div class="kt_footer-bottom">
		<div class="kt-container ">
			<div class="kt-footer__wrapper row">
				<div class="col-md-6">
					<div class="kt-footer__copyright text-primary kt-font-bold">
						Copyright  2020 - 2021 Webdew Apps All Rights Reserved
					</div>
				</div>
				<div class="col-md-6">
					<div class="kt-footer__menu justify-content-center justify-content-xl-end justify-content-lg-end justify-content-md-end pl-0">
						<a href="https://www.webdew.com/help/hubspot/all-in-one-sms/privacy-policy" target="_blank" class="kt-link pl-0 ml-0 text-primary">Privacy Policy</a>
						<a href="https://www.webdew.com/help/terms-and-service" target="_blank" class="kt-link text-primary">Terms of Service</a>
					</div>
				</div>																	
			</div>
		</div>
	</div>
</div>
<!-- end:: Footer -->