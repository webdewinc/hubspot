@extends('layout.app')
<head>
    <link rel="stylesheet"
        href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link rel="stylesheet"
        href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css">
</head>
<div class="main-content-wrap">
<br>
  <h3 style='text-align:center' class="text-primary">WHMCS LOGS</h3>
  <br>
  <div class="table-wrap">
<table class="table table-bordered" id="table">
  <thead>
    <tr>
      <th scope="col">S.No.</th>
      <th scope="col">Products</th>
      <th scope="col">Updated Products</th>
      <th scope="col">Contacts</th>
      <th scope="col">Updated Contacts</th>
      <th scope="col">Deals</th>
      <th scope="col">Updated Deals</th>
      <th scope="col">Message</th>
      <th scope="col">Status</th>
      <th scope="col">Created At</th>
    </tr>
  </thead>
  <tbody>
      <?php 
        // $result = DB::table('whmcs_logs')->where('portal_id','9200613')->get();
        $portal_id = "";
        $portal_id = Session::get('portal_id');
        $result = DB::select("select * from whmcs_logs where portal_id=".$portal_id);
        // print_r($result);
        if(!empty($result)){
          foreach ($result as $value) {
            echo "<tr>";
            echo "<th scope='row'>".$value->id."</th>" ;
            echo "<td>".$value->products."</td>";
            echo "<td>".$value->update_products."</td>";
            echo "<td>".$value->contacts."</td>";
            echo "<td>".$value->update_contacts."</td>";
            echo "<td>".$value->deals."</td>";
            echo "<td>".$value->update_deals."</td>";
            echo "<td>".$value->message."</td>";
            echo "<td>".$value->status."</td>";
            echo "<td>".$value->created_at."</td>";
            echo "</tr>";
          }
        }
      ?>
  </tbody>
</table>
</div>
</div>
<style type="text/css">
	
.main-content-wrap {
    width: 1380px;
    margin: 120px auto 80px;
    padding:0 30px;
}
div#table_length {
    text-align: left;
    display: inline-block;
}
div.dataTables_wrapper div.dataTables_length select {
    width: 75px;
    display: inline-block;
    margin: 0 15px;
    padding: 0 8px;
    height:34px;
}
div#table_filter input {
    background: #fff;
    font-size: 15px;
    outline: none;
    height: 36px;
    border-color: #e5f5ff;
}
div#table_wrapper .row {
    width: 100%;
}
div#table_filter label {
    display: block;
    text-align: right;
    vertical-align: middle;
}
ul.pagination li {
    margin-left: 15px;
}
ul.pagination {
    justify-content: flex-end;
}
th{
    font-size: 15px;
    outline:none !important;
}
td {
    font-size: 15px;
}
ul.pagination li a {
    cursor: pointer !important;
    border-color: #e5f5ff !important;
}
.pagination>.active>a, .pagination>.active>a:focus, .pagination>.active>a:hover, .pagination>.active>span, .pagination>.active>span:focus, .pagination>.active>span:hover {
    background-color: #0091ae ;
    border-color: #0091ae ;
}
.table>caption+thead>tr:first-child>td, .table>caption+thead>tr:first-child>th, .table>colgroup+thead>tr:first-child>td, .table>colgroup+thead>tr:first-child>th, .table>thead:first-child>tr:first-child>td, .table>thead:first-child>tr:first-child>th {
    border-top: 0;
    border-bottom: 0;
}
.table-bordered>tbody>tr>td, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>td, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>thead>tr>th {
    border: 1px solid #d7d7d7;
}
.dataTables_wrapper .dataTable {
    margin: 20px 0 !important;
}
</style>
</body>
</html>


