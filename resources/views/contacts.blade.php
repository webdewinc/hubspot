@extends('layout.app')
@php
$totalTitle = 'Contacts';
@endphp
@section('title', $totalTitle)
@section('title-description')

<meta name="description" content="Contacts">
    
@endsection
@section('content')
@section('head')
@parent    
@show
<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor bg-white mt-5" id="kt_content">

        <!-- begin:: Content -->
        <div class="kt-container  kt-grid__item kt-grid__item--fluid home-container">
           
            <div class="row">
                <!-- body content start -->
                <div class="col-12">
                    <div class="kt_blog-post pb-5">
                        
                        <div class="row justify-content-center">
                            <div class="col-xl-8 col-lg-8 col-md-12">
                                
                                @if (session('success'))
                                <div class="alert alert-success">
                                    {{ session('success') }}
                                </div>
                                @endif
                                @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif

                                <div class="row">
                                    <div class="col">
                                        <div class="card">
                                            <div class="card-header">
                                                <label>Select contacts</label>
                                                <span class="float-right">
                                                    <a class="btn btn-primary" href="{{url('/home')}}">Home</a>
                                                </span>
                                            </div>
                                            <div class="card-body">
                                                <div class="form-group">
                                                    <?php
                                                    $html = 'No contact numbers available';
                                                    ?>
                                                    <ul>
                                                        @foreach ($contacts->contacts as $key => $contact)
                                                            @php
                                                                $res = json_decode(json_encode($contact), true);
                                                                $phone_number = @$contact->properties->phone->value;
                                                                $email = @$contact->properties->email->value;
                                                                $firstname = @$contact->properties->firstname->value;
                                                                $lastname = @$contact->properties->lastname->value;
                                                                $portal_id = @$res['portal-id'];
                                                                $vid = @$contact->vid;
                                                            @endphp
                                                            @if(!empty($phone_number))
                                                                <li><a href="{{url('/contacts/'.$portal_id.'/contact/'.$vid)}}"> {{$firstname}} {{$lastname}} </a></li>
                                                                @php
                                                                    $html = '';
                                                                @endphp
                                                            @endif

                                                        @endforeach
                                                        <h3>{{$html}}</h3>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>                                  
                    </div>
                </div>
                <!-- body content end -->
            </div>
        </div>
    </div>                  
</div>

@section('scripts')
@parent
@show
@endsection