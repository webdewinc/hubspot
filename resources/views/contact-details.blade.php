@extends('layout.app')
@php
$totalTitle = 'Contact Details';
@endphp
@section('title', $totalTitle)
@section('title-description')

<meta name="description" content="Contact Details">
    
@endsection
@section('content')
@section('head')
@parent    
@show
<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor bg-white mt-5" id="kt_content">

        <!-- begin:: Content -->
        <div class="kt-container  kt-grid__item kt-grid__item--fluid home-container">
           
            <div class="row">
                <!-- body content start -->
                <div class="col-12">
                    <div class="kt_blog-post pb-5">
                        
                        <div class="row justify-content-center">
                            <div class="col-xl-8 col-lg-8 col-md-12">
                                
                                @if (session('success'))
                                <div class="alert alert-success">
                                    {{ session('success') }}
                                </div>
                                @endif
                                @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif

                                <div class="row">
                                    <div class="col">
                                        <div class="card">
                                            <div class="card-header">
                                                Send SMS message
                                                <span class="float-right">
                                                    <a class="btn btn-primary" href="{{url('/home')}}">Home</a>
                                                    @php
                                                        $portal_id = @Auth::user()->portal_id;
                                                        $email = @Auth::user()->email;
                                                    @endphp
                                                    &nbsp;
                                                    <a class="btn btn-primary" href="{{url('/contacts/'.$portal_id.'/contacts/list/view/all/')}}">Go to Contacts list</a>
                                                </span>
                                            </div>
                                            <div class="card-body">
                                                <form method="POST" action="{{url('custom')}}">
                                                    @csrf
                                                    <div class="form-group">
                                                        <?php
                                                            $phone_number = @$contacts->properties->phone->value;
                                                            $firstname = @$contacts->properties->firstname->value;
                                                            $lastname = @$contacts->properties->lastname->value;
                                                        ?>
                                                        <input type="hidden" name="users[]" value="{{$phone_number}}"> 
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Notification Message to : </label>
                                                        <br>
                                                        <span>{{$firstname}} {{$lastname}}</span>
                                                        <textarea name="body" class="form-control" rows="3"></textarea>
                                                    </div>
                                                    <button type="submit" class="btn btn-label-brand btn-bold">Send Notification</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>                                  
                    </div>
                </div>
                <!-- body content end -->
            </div>
        </div>
    </div>                  
</div>

@section('scripts')
@parent
@show
@endsection