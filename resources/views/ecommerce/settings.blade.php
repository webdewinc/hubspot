@extends('layout.app')
@php
$totalTitle = 'Settings';
@endphp
@section('title', $totalTitle)
@section('title-description')
<meta name="description" content="Settings">
<style type="text/css">

.smshub-contentBox-colInner {
    padding: 45px 25px 40px;
    border-radius: 10px;
}
.smshub-contentBox-icon {
    margin-bottom: 35px;
}
.smshub-contentBox-iconInner img {
    height: 115px;
}



</style>
<style type="text/css">
    #myModal .modal-dialog {
        max-width: 1300px;
    }
    #myModal .modal-dialog img {
        width: 100%;
    }
</style>
@endsection
@section('content')
@section('head')
@parent    
@show
<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor bg-white mt-5" id="kt_content">

        <!-- begin:: Content -->
        <div class="kt-container  kt-grid__item kt-grid__item--fluid home-container">
           
            <div class="row">
                <!-- body content start -->
                <div class="col-12">
                    <div class="kt_blog-post">
                        
                        <div class="row justify-content-center">
                            <?php 
                            $payment_id=0;
                            // $payment_id = DB::table('ecommerce_payments')->where('portal_id',Session::get('portal_id'))->value('id');

                        
                        $portal_id = \Session::get('portal_id');
                        $id = \DB::table('ecommerce_payments')->where('user_id',Auth::user()->id)->where('app_id',3)->where('portal_id',$portal_id)->whereDate('end_date', '>=', \Carbon\Carbon::now()->toDateString())->value('id');

                        $payment_id = \DB::table('ecommerce_payments')->where('user_id',Auth::user()->id)->where('app_id',3)->where('portal_id',$portal_id)->whereDate('end_date', '>=', \Carbon\Carbon::now()->toDateString())->value('id');

                        $expiry_date = \DB::table('ecommerce_payments')->where('user_id',Auth::user()->id)->where('app_id',3)->where('portal_id',$portal_id)->whereDate('end_date', '>=', \Carbon\Carbon::now()->toDateString())->value('end_date');
                        
                        $date = date_create($expiry_date);
                        date_sub($date,date_interval_create_from_date_string("15 days"));
                        $warning_date =  date_format($date,"Y-m-d");
                        $current_date = date("Y-m-d");
                        
                        

                              // $payment_id = DB::table('ecommerce_payments')->where('user_id',Auth::user()->id)->where('portal_id',Session::get('portal_id'))->value('id');

                            ?>

                        @if($current_date >= $warning_date && $current_date <= $expiry_date)
                             <div class="alert alert-warning">
                              <strong>Warning! </strong> Your Subscription will get expired on {{$expiry_date}}
                            </div>
                        @endif

                        @if(!empty($id))
                            @if($current_date>$expiry_date)
                                <div class="alert alert-danger">
                                  Your 1 month subscription is expired!
                                 please click on renew button to restart the services.
                                </div>
                            @endif
                        @else
                         <?php
                            $payments = \DB::table('ecommerce_payments')->where('user_id',Auth::user()->id)->where('app_id',3)->where('portal_id',$portal_id)->value('id');
                        ?>
                            @if(!empty($payments))
                                <div class="alert alert-danger">
                                  Your 1 month subscription is expired!<br>
                                  Please click on renew button to restart the services.
                                </div>
                            @endif
                        @endif
                            @if($payment_id > 0)

                                <div class="col-xl-8 col-lg-8 col-md-12">
                                    
                                    @if (!empty($alert))
                                    <div class="alert alert-{{$alert}}">
                                        {{ $message }}
                                    </div>
                                    @endif
                                    @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    @endif

                                    <div class="row">
                                        <div class="col">
                                            <div class="card">
                                                <form id="kt_form" name="kt_form" action="{{url('apps/ecommerce/settings')}}" method="POST">
                                                    <div class="card-header">
                                                        <label>Settings</label>
                                                        <?php
                                                            if(!empty(DB::table('credentials')->where('type','ecommerce')->wherePortalId(Session::get('portal_id'))->value('data'))) {
                                                        ?>
                                                            <div class="float-right">
                                                                <a href="{{url('apps/ecommerce/sync-data')}}">Sync Data</a>
                                                            </div>
                                                        <?php
                                                            }
                                                        ?>
                                                    </div>
                                                    <br>
                                                    <div class="alert alert-warning" role="alert">
                                                      &nbsp;For more information about below credentials. please <a href="#myModal"  data-toggle="modal">&nbsp;Click here</a>
                                                        
                                                    </div>
                                                    <br>
                                                    <div class="card-body" id="vendor-append">
                                                        @php
                                                            echo $html;
                                                        @endphp
                                                    </div>
                                                    
                                                </form>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- docs -->

                                    <!-- Trigger the modal with a button -->
<!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button> -->

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        
        <h4 class="modal-title">Settings Info</h4>
      </div>
      <div class="modal-body">
       <!-- <iframe src="https://www.webdew.com/kb/hubspot/steps-to-install-e-commerce-app" height="600px" width="400px" ></iframe> -->
       <h2>Steps to install E-commerce app</h2>
        <p>Marketplace : </p>


        <p>Go to marketplace -> Ecommerce cart Button</p>

        <p>Installation : </p></p>

        <p>Give portal permission after connecting the app.</p>
       <img src="{{url('public/images/ecommerce/1.png')}}">

       <p>After installation page will be redirected to the below page.</p>
       <img src="{{url('public/images/ecommerce/2.png')}}">

       <p>Click on the login button to log in to the application as the above screenshot. After login below page will be opened.</p>

 

        <p>After Successful login User will be redirected to the all apps page of webdew.</p>

         

        <p>Now you have to do the setting of your app, click on Ecommerce Settings by clicking on the menu->ecommerce Settings or Installed button shown in the eCommerce card.</p>

        <p>To unlock the Settings options, the user has to pay 29 dollars.</p>

        <p>Follow the process of payment as Shown in screenshots below and then do the required settings of the app.</p>
       <img src="{{url('public/images/ecommerce/3.png')}}">


       <img src="{{url('public/images/ecommerce/4.png')}}">
       <img src="{{url('public/images/ecommerce/5.png')}}">

       <p>Settings :</p>

        <p>Select the menu on the top right corner and select Ecommerce settings.</p>

       <img src="{{url('public/images/ecommerce/6.png')}}">
       <p>Put all credentials here for creating cart links.</p>
       <img src="{{url('public/images/ecommerce/7.png')}}">
        <p><b>Stripe PK and Stripe SK : </b> These Fields are used to set stripe payment gateway in your app. Add primary key and secret key in these fields provided by users stripe account.</br>

        <b>Paypal  : </b>This Field is used to set PayPal payment gateway in your app. Add the API key for PayPal account in this field for your eCommerce app.</br>

        <b>Hapi Key : </b>This Field is used to sync the products available in this account to create a product image field. Fetch your API key from the HubSpot portal and paste it here.</br>

        <b>Logo link : </b>This Field is used to set your eCommerce app logo in the header. You can use your company logo for an eCommerce app by uploading the logo image in files in your HubSpot account then copy that image public link and paste it here.</p>
        <br>

        <p>How to get the hub api key?</p>

        <p>1.) Go to settings.</p>
       <img src="{{url('public/images/ecommerce/8.png')}}">

       <p>2.) Go to the API KEY option in Integration Section.</p>
       <img src="{{url('public/images/ecommerce/9.png')}}">
       <p>3.) Click on the show and then copy the hub API key and use it in your application.</p>
       <img src="{{url('public/images/ecommerce/10.png')}}">
       <p>Sync :</p>

        <p>After setting the settings click on Sync Data on the top right as shown in the above screenshot. Click Sync Cart Link and create Product Image field in products of HubSpot account.</p>
       <img src="{{url('public/images/ecommerce/11.png')}}">
       <p>Click on the Sync Cart link button to create a product image field in your products available in your HubSpot account.</p>

        <p>Sync taking some time for processing.</p>
       <img src="{{url('public/images/ecommerce/12.png')}}">

       <p>Templates: Four templates will get created in design tools after installing the eCommerce app.</p>

        <p>Products</p>
        <p>Cart</p>
        <p>Product_detail</p>
        <p>Checkout</p>

       <img src="{{url('public/images/ecommerce/13.png')}}">

       <p>Pages : </p>

        <p>The user has to create four pages and apply those four templates as follows.</p>

        <p>Goto landing page option from marketing dropdown menu.</p>

       <img src="{{url('public/images/ecommerce/14.png')}}">
       <p>2.) Click on create and select the landing page option</p>

       <img src="{{url('public/images/ecommerce/15.png')}}">
       <p>3.) Select your respective template</p>
       <img src="{{url('public/images/ecommerce/16.png')}}">
       <p>4. )Add internal page name</p>
       <img src="{{url('public/images/ecommerce/17.png')}}">
       <p>5.)User needs to set page title before publishing the page</p>
       <img src="{{url('public/images/ecommerce/18.png')}}">
       <p>6.) Set page title and publish the page</p>
       <img src="{{url('public/images/ecommerce/19.png')}}">
       <p>Note: set your page titles as the same as template names.</p>
        <p>The title for the Product_detail template will be product_detail. </p>

        <p>The title for the Products template will be products. </p>

        <p>The title for the checkout template will be checkout.</p>

        <p>The title for the Cart template will be cart.</p>
       <img src="{{url('public/images/ecommerce/20.png')}}">

       <p>Products :</p>

        <p>Adding Images in Files to use it in products and for Logo</p>

        <p>Goto to Marketing->Files and templates -> Files</p>
        <p>Now click on the upload files button to add images</p>


       <img src="{{url('public/images/ecommerce/21.png')}}">
       <p>After uploading the images copy the public URL to use it in products</p>
       <img src="{{url('public/images/ecommerce/22.png')}}">
       <p>Goto Products library, open any product and find property “Product Image” which has been created during sync data.</p>
       <img src="{{url('public/images/ecommerce/23.png')}}">
       <img src="{{url('public/images/ecommerce/24.png')}}">
       <p>After setting all product images and setting the logo image in webdew setting page, you are now good to go to use the app.</p>

 

        <p>NOTE: Currently Ecommerce app accepts only dollar format price. It could be any dollar AUD, Ca or American dollar.</p>

        <p>for eg: the product price will be $ 120.</p>

        <p>Initially open the Products page and here user will see all its products available in HubSpot account</p>

        <p>Products:</p>

         
       <img src="{{url('public/images/ecommerce/25.png')}}">
       <img src="{{url('public/images/ecommerce/26.png')}}">
       <p>User can add products to the cart by clicking on add to cart button and view details of that particular product by clicking on the view details button.</p>

        <p>Product Details:</p>
       <img src="{{url('public/images/ecommerce/27.png')}}">
       Cart :

        <p>After clicking on the view cart button below page will be opened.</p>
       <img src="{{url('public/images/ecommerce/28.png')}}">

       <p>Checkout Page : </p>

        <p>After clicking on the checkout button below the page will be shown. Click continue after filling all required field.</p>

       <img src="{{url('public/images/ecommerce/29.png')}}">
       <p>After continue shipping information shown.</p>

       <img src="{{url('public/images/ecommerce/30.png')}}">
       <p>Payments :</p>

        <p>Stripe payment gateway implemented for cards and PayPal for the direct account.</p>
       <img src="{{url('public/images/ecommerce/31.png')}}">
       <p>Fill correct card details here and pay.</p>
       <img src="{{url('public/images/ecommerce/32.png')}}">
       <p>If you want to pay with PayPal then the below screen will be shown.</p>
       <img src="{{url('public/images/ecommerce/33.png')}}">
       <p>After payment successfully done</p>
       <img src="{{url('public/images/ecommerce/34.png')}}">
       <p>HubSpot Data : </p>

        <p>After payment contacts, deals and line items have been generated on the HubSpot account.</p>
       <img src="{{url('public/images/ecommerce/35.png')}}">

    
      
        </br>



        <div class="float-right">
            <a href="https://www.webdew.com/kb/hubspot/steps-to-install-e-commerce-app" target="_blank"   >Full documentation</a>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

                                    <!-- docs -->
                                </div>
                            @else

                            <!-- ecommerce - card  -->

<div class="col-12">
    <section class="smshub-contentBox pt-5">
        <div class="container-fluid">
        <div class="row smshub-contentBox-row">
                      <div class="col-xl-4 col-lg-3">
             
              </div>
              <div class="col-xl-4 col-lg-6">
                <div class="card card-custom smshub-contentBox-col kt-portlet kt-iconbox kt-icon-size kt-iconbox--primary kt-iconbox--animate-slow">
                  <div class="smshub-contentBox-colInner text-center">
                    <div class="smshub-contentBox-icon">
                      <div class="smshub-contentBox-iconInner">
                         <img src="/hubspot/public/assets/media/misc/e-commerce.svg" alt="icon1">
                      </div>
                    </div>
                    <div class="smshub-contentBox-content">
                      <h3>Ecommerce App</h3>
                      <p>One more step away to use Exciting Features of our App</p> 
                       Pay only <b>$29 for one month of installation</b> to use the app. </p>
                   
                         
                            <!-- <form role="form" action="{{url('/ecommerce-stripe-payment')}}" method="post"  data-cc-on-file="false" data-stripe-publishable-key="pk_test_51HIGy1Et32QR8DEin6O2X1MaUaQeRLUmdx49imFUDG2A5sdTZ94Sl7Mqy0cGikPOnxwpWRDjB9pU4oBeQ2VWuE4O002IlB5paY" id="payment-form"> -->
                            <form role="form" action="{{url('/ecommerce-stripe-payment')}}" method="post"  data-cc-on-file="false" data-stripe-publishable-key="{{$stripe_P}}" id="payment-form">                                

                            
                                
                                <label for="test1" hidden> Basic : $29 </label>
                                <br>
                                <br>
                                <span>
                                    @csrf
                                    <input type="hidden" name="email" value="{{Auth::user()->email}}">
                                    <noscript>You must <a href="http://www.enable-javascript.com" target="_blank">enable JavaScript</a> in your web browser in order to pay via Stripe.</noscript>

                                    @if(Auth::user()->email == 'randeep.s@webdew.com')
                                        <input 
                                            type="submit"
                                            class="btn btn-brand btn-bold kt-mr-10 pay" 
                                            value="Pay $29"
                                            data-key="pk_test_51HIGy1Et32QR8DEin6O2X1MaUaQeRLUmdx49imFUDG2A5sdTZ94Sl7Mqy0cGikPOnxwpWRDjB9pU4oBeQ2VWuE4O002IlB5paY"
                                            data-amount="2900"
                                            data-payment="daily"
                                            data-currency="usd"
                                            data-name="stripe Payment"
                                            data-email="{{Auth::user()->email}}"
                                            id="daily"
                                        />
                                    @else
                                        <input 
                                            type="submit"
                                            class="btn btn-brand btn-bold kt-mr-10 pay" 
                                            value="Pay $29"
                                            data-key="{{$stripe_P}}"
                                            data-amount="2900"
                                            data-payment="daily"
                                            data-currency="usd"
                                            data-name="stripe Payment"
                                            data-email="{{Auth::user()->email}}"
                                            id="daily"
                                        />
                                    @endif
                                        
                                        <script src="https://checkout.stripe.com/v2/checkout.js"></script>
                                        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
                                        <script>
                                            $(document).ready(function() {
                                                $(document).on('click','#daily' ,function(event) {
                                                    event.preventDefault();
                                                    var $button = $(this),
                                                        $form = $button.parents('form');

                                                    var opts = $.extend({}, $button.data(), {
                                                        token: function(result) {
                                                        $form.append($('<input>').attr({ type: 'hidden', name: 'stripeToken', value: result.id })).submit();
                                                        }
                                                    });
                                                    StripeCheckout.open(opts);
                                                });
                                            });
                                        </script>
                                </span>
                            </form>                                
                    </div>
                  </div>
                </div>
              </div>
          </div>
      </section>
  </div>
</section>
   <div class="col-xl-4 col-lg-6">
             
              </div>
                         <!-- ecommerce - card -->
                            <!-- <form role="form" action="{{url('/ecommerce-stripe-payment')}}" method="post"  data-cc-on-file="false" data-stripe-publishable-key="{{@$stripe_P}}" id="payment-form"> -->
                                                      
                            @endif
                        </div>                                  
                    </div>
                </div>
                <!-- body content end -->
            </div>
        </div>
    </div>                  
</div>

@section('scripts')
@parent

  <!--begin::Page Scripts(used by this page) -->
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
        <script type="text/javascript">
            "use strict";

            // Class definition
            var KTWizard2 = function () {
                // Base elements
                var wizardEl;
                var formEl;
                var validator;
                var wizard;

                var initValidation = function() {
                    validator = formEl.validate({
                        // Validate only visible fields
                        ignore: ":hidden",

                        // Validation rules
                        rules: {
                           
                            // routes :{
                            //     required: true,
                            //     maxlength: 3,
                            //     digits:true
                            // },
                            // country_code :{
                            //     required: true,
                            //     maxlength: 3,
                            //     digits:true
                            // },
                            // sender :{
                            //     required: true,
                            //     maxlength: 10,
                            // },
                            // MSG91 :{
                            //     required: true,
                            // }
                        },

                        // Display error
                        invalidHandler: function(event, validator) {
                            KTUtil.scrollTop();
                            swal.fire({
                                "title": "",
                                "text": "There are some errors in your submission. Please correct them.",
                                "type": "error",
                                "confirmButtonClass": "btn btn-secondary"
                            });
                        },

                        // Submit valid form
                        // submitHandler: function (form) {

                        // }
                    });
                }

                var initSubmit = function() {
                    var btn = formEl.find('[data-ktwizard-type="action-submit"]');

                    btn.on('click', function(e) {
                        e.preventDefault();

                        if (validator.form()) {
                            $('#kt_form').submit();     
                        }
                    });
                }

                return {
                    // public functions
                    init: function() {
                        wizardEl = KTUtil.get('kt_wizard_v2');
                        formEl = $('#kt_form');
                        initValidation();
                        initSubmit();
                    }
                };
            }();

            jQuery(document).ready(function() {
                KTWizard2.init();
                /** validation for guest example not to match with domain url **/
                jQuery.validator.addMethod("notEqual", function(value, element, param) {
                  return this.optional(element) || value != param;
                }, "Please specify a different value");
                /** end here code **/
                $.validator.addMethod('lessThanEqual', function(value, element, param) {
                    return this.optional(element) || parseInt(value) <= parseInt($(param).val());
                }, "The content cost must be less than number words");
            });

            jQuery(document).on('change','#type',function(e){
                    var vendor = $(this).val();
                    e.preventDefault();
                    $.ajax({
                         headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                          },
                           beforeSend: function() {
                                // setting a timeout
                                $(".loader").fadeIn("slow");
                            },
                          url  : "{{ url('/render-vendor') }}",
                          type : 'POST',
                          data: {
                                'vendor' : vendor,
                             },
                          async: true,
                          //dataType: 'json',
                          enctype: 'multipart/form-data',
                          cache: false,                      
                          success: function(response){
                            $('#vendor-append').html(response);
                            $(".loader").fadeOut("slow");
                          },
                          error: function(){}
                        });
                        e.stopImmediatePropagation();
                        return false; 

            });

        </script>     
@show
@endsection