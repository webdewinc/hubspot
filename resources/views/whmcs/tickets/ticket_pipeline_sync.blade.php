@extends('layout.app')
@php
$totalTitle = 'Pipelines';
@endphp
@section('title', $totalTitle)
@section('title-description')

<meta name="description" content="Pipelines">
    
@endsection
@section('content')
@section('head')
@parent    
@show
<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor bg-white pt-7 pipline-page" id="kt_content">

        <!-- begin:: Content -->
        <div class="kt-container kt-grid__item kt-grid__item--fluid home-container">
           
            <div class="row">
                <!-- body content start -->
                <div class="col-12">
                    <div class="kt_blog-post  pb-5">
                        
                        <div class="row justify-content-center">
                            <div class="col-xl-8 col-lg-8 col-md-12">

                                  <span id="alert-span"></span>
                                                           
                               @if($res['status'] == 'ok')
                                    
                                @endif
                                @if($res['status'] === false)
                                    <div class="alert alert-danger">
                                        {{ $res['message'] }}
                                    </div>
                                @endif
                                @if($res['status'] === true)
                                    <div class="alert alert-success">
                                        {{ $res['message'] }}
                                    </div>
                                @endif
                             
                             
                              
                                <div class="row">
                                    <div class="col">
                                        <div class="card">
                                            <div class="card-header">
                                                <label>Tickets-Pipeline Sync</label>
                                            </div>
                                            <div class="card-body">
                                                <form id="kt_form" name="kt_form" action="{{url('apps/whmcs/tickets_pipeline')}}" method="POST" >@csrf

                                                    <div class="container">
                                                       
                                                        <div class="row">
                                                            <div class="col-6">
                                                                <h2>
                                                                WHMCS
                                                            </h2>
                                                            </div>
                                                            <div class="col-6">
                                                                <h2>
                                                                HubSpot
                                                            </h2>
                                                            </div>
                                                        </div>

                                                  
                                                            <input type="hidden" name="edit" value="1">

                               <?php  
                                
                                 $user_id = Auth::user()->id;
                                $id = DB::table('whmcs_tickets_pipelines')->where('user_id', $user_id)->where('portal_id',Session::get('portal_id'))->value('id');
                                if(empty($id)){
                                ?>
                                                            <div class="row">

                                                                <div class="col-6">
                                                                 
                                                                            <select name="first_whmcs_pipeline" required="">
                                                                                <option value="">SELECT</option>
                                                                               
                                                                                    <option value="Open" >Open </option>
                                                                                    <option value="Answered">Answered </option>
                                                                                    <option value="Customer-Reply">Customer - reply </option>
                                                                                    <option value="On Hold">On Hold </option>
                                                                                    <option value="In Progress">In progress </option>
                                                                                    <option value="Closed">Closed </option>

                                                                              
                                                                            </select>
                                                                            <br>
                                                                              <select name="second_whmcs_pipeline" required="">
                                                                                <option value="">SELECT</option>
                                                                               
                                                                                    <option value="Open" >Open </option>
                                                                                    <option value="Answered">Answered </option>
                                                                                    <option value="Customer-Reply">Customer - reply </option>
                                                                                    <option value="On Hold">On Hold </option>
                                                                                    <option value="In Progress">In progress </option>
                                                                                    <option value="Closed">Closed </option>

                                                                              
                                                                            </select>
                                                                             <br>
                                                                                  <select name="third_whmcs_pipeline" required="">
                                                                                <option value="">SELECT</option>
                                                                               
                                                                                    <option value="Open" >Open </option>
                                                                                    <option value="Answered">Answered </option>
                                                                                    <option value="Customer-Reply">Customer - reply </option>
                                                                                    <option value="On Hold">On Hold </option>
                                                                                    <option value="In Progress">In progress </option>
                                                                                    <option value="Closed">Closed </option>

                                                                              
                                                                            </select>
                                                                             <br>
                                                                                 <select name="fourth_whmcs_pipeline" required="">
                                                                                <option value="">SELECT</option>
                                                                               
                                                                                   <option value="Open" >Open </option>
                                                                                    <option value="Answered">Answered </option>
                                                                                    <option value="Customer-Reply">Customer - reply </option>
                                                                                    <option value="On Hold">On Hold </option>
                                                                                    <option value="In Progress">In progress </option>
                                                                                    <option value="Closed">Closed </option>

                                                                              
                                                                            </select>
                                                                          
                                                                           <br>
                                                                                 <select name="fifth_whmcs_pipeline" required="">
                                                                                <option value="">SELECT</option>
                                                                               
                                                                                    <option value="Open" >Open </option>
                                                                                    <option value="Answered">Answered </option>
                                                                                    <option value="Customer-Reply">Customer - reply </option>
                                                                                    <option value="On Hold">On Hold </option>
                                                                                    <option value="In Progress">In progress </option>
                                                                                    <option value="Closed">Closed </option>

                                                                              
                                                                            </select>

                                                                             <br>
                                                                                 <select name="sixth_whmcs_pipeline" required="">
                                                                                <option value="">SELECT</option>
                                                                               
                                                                                   <option value="Open" >Open </option>
                                                                                    <option value="Answered">Answered </option>
                                                                                    <option value="Customer-Reply">Customer - reply </option>
                                                                                    <option value="On Hold">On Hold </option>
                                                                                    <option value="In Progress">In progress </option>
                                                                                    <option value="Closed">Closed </option>

                                                                              
                                                                            </select>


                                                                      
                                                                      
                                                             </div>
                                                                <div class="col-6">

                                                                              <select name="first_hubspot_pipeline" required="">
                                                                                <option value="">SELECT</option>
                                                                               
                                                                                    <option value="New" >New </option>
                                                                                    <option value="waiting_on_contact" >Waiting On Contact </option>
                                                                                    <option value="waiting_on_us">Waiting On Us </option>
                                                                                    <option value="closed">Closed </option>
                                                                                    

                                                                              
                                                                            </select>
                                                                   
                                                                      <br>   
                                                                      <select name="second_hubspot_pipeline" required="">
                                                                                <option value="">SELECT</option>
                                                                               
                                                                                    <option value="New" >New </option>
                                                                                    <option value="waiting_on_contact" >Waiting On Contact </option>
                                                                                    <option value="waiting_on_us">Waiting On Us </option>
                                                                                    <option value="closed">Closed </option>
                                                                                    

                                                                              
                                                                            </select>
                                                                               <br>
                                                                                <select name="third_hubspot_pipeline" required="">
                                                                                <option value="">SELECT</option>
                                                                               
                                                                                    <option value="New" >New </option>
                                                                                    <option value="waiting_on_contact" >Waiting On Contact </option>
                                                                                    <option value="waiting_on_us">Waiting On Us </option>
                                                                                    <option value="closed">Closed </option>
                                                                                    

                                                                              
                                                                            </select>
                                                                               <br>
                                                                          <select name="fourth_hubspot_pipeline" required="">
                                                                                <option value="">SELECT</option>
                                                                               
                                                                                    <option value="New" >New </option>
                                                                                    <option value="waiting_on_contact" >Waiting On Contact </option>
                                                                                    <option value="waiting_on_us">Waiting On Us </option>
                                                                                    <option value="closed">Closed </option>
                                                                                    

                                                                              
                                                                            </select>
                                                                                <br>
                                                                          <select name="fifth_hubspot_pipeline" required="">
                                                                                <option value="">SELECT</option>
                                                                               
                                                                                    <option value="New" >New </option>
                                                                                    <option value="waiting_on_contact" >Waiting On Contact </option>
                                                                                    <option value="waiting_on_us">Waiting On Us </option>
                                                                                    <option value="closed">Closed </option>
                                                                                    

                                                                              
                                                                            </select>
                                                                                <br>
                                                                          <select name="sixth_hubspot_pipeline" required="">
                                                                                <option value="">SELECT</option>
                                                                               
                                                                                    <option value="New" >New </option>
                                                                                    <option value="waiting_on_contact" >Waiting On Contact </option>
                                                                                    <option value="waiting_on_us">Waiting On Us </option>
                                                                                    <option value="closed">Closed </option>
                                                                                    

                                                                              
                                                                            </select>

                                                                </div>
                                                         
                                                        
                                                        
                                                    </div>
                                            <?php }else{
                                              

                                                $first_whmcs_pipeline = DB::table('whmcs_tickets_pipelines')->where('user_id', $user_id)->where('portal_id',Session::get('portal_id'))->where('pipeline_number','1')->value('whmcs_pipeline');

                                                $second_whmcs_pipeline = DB::table('whmcs_tickets_pipelines')->where('user_id', $user_id)->where('portal_id',Session::get('portal_id'))->where('pipeline_number','2')->value('whmcs_pipeline');

                                                $third_whmcs_pipeline = DB::table('whmcs_tickets_pipelines')->where('user_id', $user_id)->where('portal_id',Session::get('portal_id'))->where('pipeline_number','3')->value('whmcs_pipeline');

                                                $fourth_whmcs_pipeline = DB::table('whmcs_tickets_pipelines')->where('user_id', $user_id)->where('portal_id',Session::get('portal_id'))->where('pipeline_number','4')->value('whmcs_pipeline');

                                                $fifth_whmcs_pipeline = DB::table('whmcs_tickets_pipelines')->where('user_id', $user_id)->where('portal_id',Session::get('portal_id'))->where('pipeline_number','5')->value('whmcs_pipeline');

                                                $sixth_whmcs_pipeline = DB::table('whmcs_tickets_pipelines')->where('user_id', $user_id)->where('portal_id',Session::get('portal_id'))->where('pipeline_number','6')->value('whmcs_pipeline');


                                                $first_hubspot_pipeline = DB::table('whmcs_tickets_pipelines')->where('user_id', $user_id)->where('portal_id',Session::get('portal_id'))->where('pipeline_number','1')->value('hubspot_pipeline');

                                                $second_hubspot_pipeline = DB::table('whmcs_tickets_pipelines')->where('user_id', $user_id)->where('portal_id',Session::get('portal_id'))->where('pipeline_number','2')->value('hubspot_pipeline');

                                                $third_hubspot_pipeline = DB::table('whmcs_tickets_pipelines')->where('user_id', $user_id)->where('portal_id',Session::get('portal_id'))->where('pipeline_number','3')->value('hubspot_pipeline');

                                                $fourth_hubspot_pipeline = DB::table('whmcs_tickets_pipelines')->where('user_id', $user_id)->where('portal_id',Session::get('portal_id'))->where('pipeline_number','4')->value('hubspot_pipeline');

                                                $fifth_hubspot_pipeline = DB::table('whmcs_tickets_pipelines')->where('user_id', $user_id)->where('portal_id',Session::get('portal_id'))->where('pipeline_number','5')->value('hubspot_pipeline');

                                                $sixth_hubspot_pipeline = DB::table('whmcs_tickets_pipelines')->where('user_id', $user_id)->where('portal_id',Session::get('portal_id'))->where('pipeline_number','6')->value('hubspot_pipeline');



                                                ?>

                                                     <div class="row">
                                                                <div class="col-6">
                                                                 
                                                                            <select name="first_whmcs_pipeline" required="" value=<?php echo $first_whmcs_pipeline; ?>>
                                                                                <option value=>SELECT</option>
                                                                               
                                                                                    <option value="Open" <?php echo $first_whmcs_pipeline == "Open"? "selected" : ""; ?>>Open </option>
                                                                                    <option value="Answered" <?php echo $first_whmcs_pipeline == "Answered"? "selected" : ""; ?>>Answered </option>
                                                                                    <option value="Customer-Reply"<?php echo $first_whmcs_pipeline == "Customer-Reply"? "selected" : ""; ?>>Customer - reply </option>
                                                                                    <option value="On Hold" <?php echo $first_whmcs_pipeline == "On Hold"? "selected" : ""; ?>>On Hold </option>
                                                                                    <option value="In Progress" <?php echo $first_whmcs_pipeline == "In Progress"? "selected" : ""; ?>>In progress </option>
                                                                                    <option value="Closed" <?php echo $first_whmcs_pipeline == "Closed"? "selected" : ""; ?>>Closed </option>

                                                                              
                                                                            </select>
                                                                            <br>
                                                                              <select name="second_whmcs_pipeline" required="">
                                                                                <option value=<?php echo $second_whmcs_pipeline; ?>>SELECT</option>
                                                                               
                                                                                    <option value="Open" <?php echo $second_whmcs_pipeline == "Open"? "selected" : ""; ?>>Open </option>
                                                                                    <option value="Answered" <?php echo $second_whmcs_pipeline == "Answered"? "selected" : ""; ?>>Answered </option>
                                                                                    <option value="Customer-Reply" <?php echo $second_whmcs_pipeline == "Customer-Reply"? "selected" : ""; ?>>Customer - reply </option>
                                                                                    <option value="On Hold" <?php echo $second_whmcs_pipeline == "On Hold"? "selected" : ""; ?>>On Hold </option>
                                                                                    <option value="In Progress" <?php echo $second_whmcs_pipeline == "In Progress"? "selected" : ""; ?>>In progress </option>
                                                                                    <option value="Closed" <?php echo $second_whmcs_pipeline == "Closed"? "selected" : ""; ?>>Closed </option>

                                                                              
                                                                            </select>
                                                                             <br>
                                                                                      <select name="third_whmcs_pipeline" required="">
                                                                                <option value=<?php echo $third_whmcs_pipeline; ?>>SELECT</option>
                                                                               
                                                                                    <option value="Open" <?php echo $third_whmcs_pipeline == "Open"? "selected" : ""; ?>>Open </option>
                                                                                    <option value="Answered" <?php echo $third_whmcs_pipeline == "Answered"? "selected" : ""; ?>>Answered </option>
                                                                                    <option value="Customer-Reply" <?php echo $third_whmcs_pipeline == "Customer-Reply"? "selected" : ""; ?>>Customer - reply </option>
                                                                                    <option value="On Hold" <?php echo $third_whmcs_pipeline == "On Hold"? "selected" : ""; ?>>On Hold </option>
                                                                                    <option value="In Progress" <?php echo $third_whmcs_pipeline == "In Progress"? "selected" : ""; ?>>In progress </option>
                                                                                    <option value="Closed" <?php echo $third_whmcs_pipeline == "Closed"? "selected" : ""; ?>>Closed </option>

                                                                              
                                                                            </select>
                                                                             <br>
                                                                                
                                                                                    <select name="fourth_whmcs_pipeline" required="">
                                                                                <option value=<?php echo $fourth_whmcs_pipeline; ?>>SELECT</option>
                                                                               
                                                                                    <option value="Open" <?php echo $fourth_whmcs_pipeline == "Open"? "selected" : ""; ?>>Open </option>
                                                                                    <option value="Answered" <?php echo $fourth_whmcs_pipeline == "Answered"? "selected" : ""; ?>>Answered </option>
                                                                                    <option value="Customer-Reply" <?php echo $fourth_whmcs_pipeline == "Customer-Reply"? "selected" : ""; ?>>Customer - reply </option>
                                                                                    <option value="On Hold" <?php echo $fourth_whmcs_pipeline == "On Hold"? "selected" : ""; ?>>On Hold </option>
                                                                                    <option value="In Progress" <?php echo $fourth_whmcs_pipeline == "In Progress"? "selected" : ""; ?>>In progress </option>
                                                                                    <option value="Closed" <?php echo $fourth_whmcs_pipeline == "Closed"? "selected" : ""; ?>>Closed </option>
                                                                              
                                                                            </select>

                                                                            <br>
                                                                                
                                                                                    <select name="fifth_whmcs_pipeline" required="">
                                                                                <option value=<?php echo $fifth_whmcs_pipeline; ?>>SELECT</option>
                                                                               
                                                                                    <option value="Open" <?php echo $fifth_whmcs_pipeline == "Open"? "selected" : ""; ?>>Open </option>
                                                                                    <option value="Answered" <?php echo $fifth_whmcs_pipeline == "Answered"? "selected" : ""; ?>>Answered </option>
                                                                                    <option value="Customer-Reply" <?php echo $fifth_whmcs_pipeline == "Customer-Reply"? "selected" : ""; ?>>Customer - reply </option>
                                                                                    <option value="On Hold" <?php echo $fifth_whmcs_pipeline == "On Hold"? "selected" : ""; ?>>On Hold </option>
                                                                                    <option value="In Progress" <?php echo $fifth_whmcs_pipeline == "In Progress"? "selected" : ""; ?>>In progress </option>
                                                                                    <option value="Closed" <?php echo $fifth_whmcs_pipeline == "Closed"? "selected" : ""; ?>>Closed </option>

                                                                              
                                                                            </select>

                                                                            <br>
                                                                                
                                                                                    <select name="sixth_whmcs_pipeline" required="">
                                                                                <option value=<?php echo $sixth_whmcs_pipeline; ?>>SELECT</option>
                                                                               
                                                                                   
                                                                                    <option value="Open" <?php echo $sixth_whmcs_pipeline == "Open"? "selected" : ""; ?>>Open </option>
                                                                                    <option value="Answered" <?php echo $sixth_whmcs_pipeline == "Answered"? "selected" : ""; ?>>Answered </option>
                                                                                    <option value="Customer-Reply" <?php echo $sixth_whmcs_pipeline == "Customer-Reply"? "selected" : ""; ?>>Customer - reply </option>
                                                                                    <option value="On Hold" <?php echo $sixth_whmcs_pipeline == "On Hold"? "selected" : ""; ?>>On Hold </option>
                                                                                    <option value="In Progress" <?php echo $sixth_whmcs_pipeline == "In Progress"? "selected" : ""; ?>>In progress </option>
                                                                                    <option value="Closed" <?php echo $sixth_whmcs_pipeline == "Closed"? "selected" : ""; ?>>Closed </option>

                                                                              
                                                                            </select>


                                                                      
                                                                      
                                                             </div>
                                                                <div class="col-6">
                                                                   
                                                                              <select name="first_hubspot_pipeline" required="">
                                                                                <option value=<?php echo $first_hubspot_pipeline; ?>>SELECT</option>
                                                                               
                                                                                    <option value="New" <?php echo $first_hubspot_pipeline == "New"? "selected" : ""; ?> >New </option>
                                                                                    <option value="waiting_on_contact" <?php echo $first_hubspot_pipeline == "waiting_on_contact"? "selected" : ""; ?> >Waiting On Contact </option>
                                                                                    <option value="waiting_on_us" <?php echo $first_hubspot_pipeline == "waiting_on_us"? "selected" : ""; ?>>Waiting On Us </option>
                                                                                    <option value="closed" <?php echo $first_hubspot_pipeline == "closed"? "selected" : ""; ?> >Closed </option>
                                                                                    

                                                                              
                                                                            </select>
                                                                   
                                                                      <br>   
                                                                      <select name="second_hubspot_pipeline" required="">
                                                                                 <option value=<?php echo $second_hubspot_pipeline; ?>>SELECT</option>
                                                                               
                                                                                    <option value="New" <?php echo $second_hubspot_pipeline == "New"? "selected" : ""; ?> >New </option>
                                                                                    <option value="waiting_on_contact" <?php echo $second_hubspot_pipeline == "waiting_on_contact"? "selected" : ""; ?> >Waiting On Contact </option>
                                                                                    <option value="waiting_on_us" <?php echo $second_hubspot_pipeline == "waiting_on_us"? "selected" : ""; ?>>Waiting On Us </option>
                                                                                    <option value="closed" <?php echo $second_hubspot_pipeline == "closed"? "selected" : ""; ?> >Closed </option>
                                                                                    

                                                                              
                                                                            </select>
                                                                               <br>
                                                                                <select name="third_hubspot_pipeline" required="">
                                                                              <option value=<?php echo $third_hubspot_pipeline; ?>>SELECT</option>
                                                                               
                                                                                    <option value="New" <?php echo $third_hubspot_pipeline == "New"? "selected" : ""; ?> >New </option>
                                                                                    <option value="waiting_on_contact" <?php echo $third_hubspot_pipeline == "waiting_on_contact"? "selected" : ""; ?> >Waiting On Contact </option>
                                                                                    <option value="waiting_on_us" <?php echo $third_hubspot_pipeline == "waiting_on_us"? "selected" : ""; ?>>Waiting On Us </option>
                                                                                    <option value="closed" <?php echo $third_hubspot_pipeline == "closed"? "selected" : ""; ?> >Closed </option>
                                                                                    
                                                                              
                                                                            </select>
                                                                               <br>
                                                                          <select name="fourth_hubspot_pipeline" required="">
                                                                                <option value=<?php echo $fourth_hubspot_pipeline; ?>>SELECT</option>
                                                                              
                                                                                     <option value="New" <?php echo $fourth_hubspot_pipeline == "New"? "selected" : ""; ?> >New </option>
                                                                                    <option value="waiting_on_contact" <?php echo $fourth_hubspot_pipeline == "waiting_on_contact"? "selected" : ""; ?> >Waiting On Contact </option>
                                                                                    <option value="waiting_on_us" <?php echo $fourth_hubspot_pipeline == "waiting_on_us"? "selected" : ""; ?>>Waiting On Us </option>
                                                                                    <option value="closed" <?php echo $fourth_hubspot_pipeline == "closed"? "selected" : ""; ?> >Closed </option>
                                                                                    

                                                                              
                                                                            </select>

                                                                                     <br>
                                                                          <select name="fifth_hubspot_pipeline" required="">
                                                                                <option value=<?php echo $fifth_hubspot_pipeline; ?>>SELECT</option>
                                                                              
                                                                                     <option value="New" <?php echo $fifth_hubspot_pipeline == "New"? "selected" : ""; ?> >New </option>
                                                                                    <option value="waiting_on_contact" <?php echo $fifth_hubspot_pipeline == "waiting_on_contact"? "selected" : ""; ?> >Waiting On Contact </option>
                                                                                    <option value="waiting_on_us" <?php echo $fifth_hubspot_pipeline == "waiting_on_us"? "selected" : ""; ?>>Waiting On Us </option>
                                                                                    <option value="closed" <?php echo $fifth_hubspot_pipeline == "closed"? "selected" : ""; ?> >Closed </option>
                                                                                    

                                                                              
                                                                            </select>

                                                                                     <br>
                                                                          <select name="sixth_hubspot_pipeline" required="">
                                                                                <option value=<?php echo $sixth_hubspot_pipeline; ?>>SELECT</option>
                                                                              
                                                                                     <option value="New" <?php echo $sixth_hubspot_pipeline == "New"? "selected" : ""; ?> >New </option>
                                                                                    <option value="waiting_on_contact" <?php echo $sixth_hubspot_pipeline == "waiting_on_contact"? "selected" : ""; ?> >Waiting On Contact </option>
                                                                                    <option value="waiting_on_us" <?php echo $sixth_hubspot_pipeline == "waiting_on_us"? "selected" : ""; ?>>Waiting On Us </option>
                                                                                    <option value="closed" <?php echo $sixth_hubspot_pipeline == "closed"? "selected" : ""; ?> >Closed </option>
                                                                                    

                                                                              
                                                                            </select>

                                                                </div>
                                                         
                                                        
                                                        
                                                    </div>


                                           <?php }
                                            ?>
                                                    <div class="card-body">
                                                        <span>
                                                            <button class="btn btn-label-brand btn-bold" id="kt_update_info" type="submit">Update</button>
                                                        </span>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                               
                            </div>
                        </div>                                  
                    </div>
                </div>
                <!-- body content end -->
            </div>
        </div>
    </div>                  
</div>


