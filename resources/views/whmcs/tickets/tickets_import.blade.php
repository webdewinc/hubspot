@extends('layout.app')
@php
$totalTitle = 'Data Import';
@endphp
@section('title', $totalTitle)
@section('title-description')

<meta name="description" content="Data Import">
    
@endsection
@section('content')
@section('head')
@parent    
@show
<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor bg-white pt-7 import-page" id="kt_content">

        <!-- begin:: Content -->
        <div class="kt-container kt-grid__item kt-grid__item--fluid home-container">
           
            <div class="row">
                <!-- body content start -->
                <div class="col-12">
                    <div class="kt_blog-post  pb-5">
                        
                        <div class="row justify-content-center">
                            <div class="col-xl-8 col-lg-8 col-md-12">
                                <span id="alert-span"></span>
                                @if ($res['status'] == "ok")
                                <div >
                                   
                                </div>
                                @endif
                                @if ($res['status'] == "true")
                                <div class="alert alert-success">
                                    
                                       
                                        {{$res['message']}}
                                       
                                    
                                </div>
                                @endif
                                 @if ($res['status'] == "false")
                                <div class="alert alert-danger">
                                    
                                       
                                        {{$res['message']}}
                                       
                                    
                                </div>
                                @endif

                                <div class="row">
                                    <div class="col">
                                        <div class="card">
                                            <div class="card-header">
                                                <label>Import Data</label>
                                            </div>
                                            <div class="card-body">
                                                <div class="container">

                                                    <style type="text/css">
                                                        #stepProgressBar  {
                                                            display: flex;
                                                            justify-content: space-between;
                                                            align-items: flex-end;
                                                            width: 300px;
                                                            margin: 0 auto;
                                                            margin-bottom: 40px;
                                                        }
                                                        .step  {
                                                            text-align:  center;
                                                        }
                                                        /*.step-text  {
                                                            margin-bottom:  10px;
                                                            color:  #ff7a59;
                                                        }*/
                                                        /*.bullet {
                                                            border: 1px solid #ff7a59;
                                                            height: 20px;
                                                            width: 20px;
                                                            border-radius: 100%;
                                                            color: #ff7a59;
                                                            display: inline-block;
                                                            position: relative;
                                                            transition: background-color 500ms;
                                                            line-height:20px;
                                                        }*/
                                                        .step-text  {
                                                            margin-bottom:  10px;
                                                            color:  #000;
                                                        }
                                                        .bullet {
                                                            border: 1px solid #000;
                                                            height: 20px;
                                                            width: 20px;
                                                            border-radius: 100%;
                                                            color: #000;
                                                            display: inline-block;
                                                            position: relative;
                                                            transition: background-color 500ms;
                                                            line-height:20px;
                                                        }
                                                        .bullet.completed  {
                                                            color:  white;
                                                            background-color:  #ff7a59;
                                                        }
                                                        .bullet.completed::after {
                                                            content: '';
                                                            position: absolute;
                                                            right: -60px;
                                                            bottom: 10px;
                                                            height: 1px;
                                                            width: 10%;
                                                            background-color: #ff7a59;
                                                        }
                                                        /* Base styles and helper stuff */
                                                        .hidden  {
                                                            display:  none;
                                                        }
                                                        .text-center  {
                                                            text-align:  center;
                                                        }
                                                        .container  {
                                                            max-width: 400px;
                                                            margin: 0 auto;
                                                            margin-top:  20px;
                                                            padding:  40px;
                                                        }
                                                    </style>
                                                   
                                                  
                                                </div>
                                                <div class="card-body">
                                                    
                                            		<form action="{{url('apps/whmcs/tickets_import')}}" method="POST">@csrf
                                            			<button type="submit" class="btn btn-primary">Import tickets </button>
                                            		</form>
                                                                   
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>                                  
                    </div>
                </div>
                <!-- body content end -->
            </div>
        </div>
    </div>                  
</div>

