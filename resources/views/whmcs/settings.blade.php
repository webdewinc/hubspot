@extends('layout.app')
@php
$totalTitle = 'Settings';
@endphp
@section('title', $totalTitle)
@section('title-description')

<meta name="description" content="Settings">
    
@endsection
@section('content')
@section('head')
@parent    
@show
<style type="text/css">
    #myModal .modal-dialog {
        max-width: 1300px;
    }
    #myModal .modal-dialog img {
        width: 100%;
    }
</style>
<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor pt-7 setting-page" id="kt_content">

        <!-- begin:: Content -->
        <div class="kt-container  kt-grid__item kt-grid__item--fluid home-container">
           
            <div class="row">
                <!-- body content start -->
                <div class="col-12">
                    <div class="kt_blog-post">
                        
                        <div class="row justify-content-center">
                            <div class="col-xl-5 col-lg-5 col-md-6 pannel-login">
                                
                                @if (!empty($alert))
                                <div class="alert alert-{{$alert}}">
                                    {{ $message }}
                                </div>
                                @endif
                                @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif

                                <div class="row">
                                    <div class="col">
                                        <div class="card">
                                            <form id="kt_form" name="kt_form" action="{{url('apps/whmcs/settings')}}" method="POST">
                                                <div class="card-header">
                                                    <h2 class="text-center"><label>WHMCS Credentials</label></h2>
                                                    @if(@$sync == 1)
                                                    <!-- <span class="float-right"><a href="{{url('apps/whmcs/import')}}">Data Import</a></span> -->
                                                    @endif
                                                </div>
                                                <br>
                                                    <div class="alert alert-warning" role="alert">
                                                      &nbsp;For more information about below credentials. please <a href="#myModal"  data-toggle="modal">&nbsp;Click here</a>
                                                    </div>
                                                <br>
                                                <div class="card-body" id="vendor-append">
                                                    @php
                                                        echo $html;
                                                    @endphp
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="col-xl-7 col-lg-7 col-md-6 pannel-image text-center">
                                <div class="whmcs-logo">
                                    <!-- <img src="/hubspot/public/assets/media/misc/whmcs-logo-sm.png" alt="image" style="width: 350px; max-width: 100%;"> -->
                                    <!-- <img src="/hubspot/public/assets/media/misc/LogoBlue.png" alt="image" style="width: 350px; max-width: 100%;"> -->
                                    <img src="/hubspot/public/assets/media/misc/LogoBlue.png" alt="image" style="width: 70%;">
                                </div>
                            </div>
                        </div>                                  
                    </div>
                </div>
                <!-- body content end -->
            </div>
        </div>
    </div>                  
</div>

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        
        <h4 class="modal-title">Settings Info</h4>
      </div>
      <div class="modal-body">
        <p><b>1.)WHMCS Credentials Process</b></p>
        <ul>
            <li> <a href="#username">username and secret of whmcs account</a> </li>
            <li><a href="#hubapi">Hubapi key </a> </li>

        </ul>
        <p><b><a href="#pipeline">2.)Pipeline Process</a></b></p>
        <p><b><a href="#import">3.)Import Process</a></b></p>
        <p>Select WHMCS Credentials for the setting of WHMCS and Hubspot</p>

        <img src="{{url('public/images/whmcs_doc_images/1.png')}}">

        <p>Go to WHMCS admin panel and the username and password need.</p>

        <img src="{{url('public/images/whmcs_doc_images/2.png')}}">

        <p>After login please click on “Manage API Credentials” for generating new credentials. Refer to the below screenshot.</p>

        <img src="{{url('public/images/whmcs_doc_images/3.png')}}">


        <p>Click on Generate New Credentials and select all options as per your requirement.</p>

        <img src="{{url('public/images/whmcs_doc_images/4.png')}}">


        <p  >Click on the “Generate” Button then the “Identifier” & “Secret” code has been generated.</p>


        <img src="{{url('public/images/whmcs_doc_images/5.png')}}">
        <p>Put these above credentials on WHMCS credentials settings.</p>



        <p>Note: Users should not add any Html in the product description as it may cause an error during import.</p>

        <img src="{{url('public/images/whmcs_doc_images/6.png')}}">
        <p>It should be a simple description as shown in the below Screenshot:</p>

        <img src="{{url('public/images/whmcs_doc_images/7.png')}}">
        <p>Note: If you will delete any imported product/data from Hubspot then you won't be able to resync data again.</p>

        <p id="hubapi"><b>HubSpot Key Process:</b></p>

        <p>Go to HubSpot portal -> click on the gear button of the top-right menu.</p>

        

        <img src="{{url('public/images/whmcs_doc_images/8.png')}}">

        <p>Navigate to the left menus and select Integrations :</p>

        

        <img src="{{url('public/images/whmcs_doc_images/9.png')}}">
        <p>After Selected Integrations then select “API Key” and copy the HubSpot key.</p>
      

        <img src="{{url('public/images/whmcs_doc_images/10.png')}}">
        <p id="pipeline"><b>Pipeline</b></p>

        <p>Path of pipeline</p>
        
        

        <img src="{{url('public/images/whmcs_doc_images/11.png')}}">

        <p>If you will face any IP issue during the opening pipeline or sync.</p>
        

        <img src="{{url('public/images/whmcs_doc_images/12.png')}}">

        <p><b>Please follow the below instruction : </b></p>

        <p>Go to Admin portal -> general Settings -> security</p>

        <p>Add Ip address here for API permission.</p>

        

        <img src="{{url('public/images/whmcs_doc_images/13.png')}}">

        <p>Add IP here for permission.</p>

        

        <img src="{{url('public/images/whmcs_doc_images/14.png')}}">

        <p>After adding IP here.</p>

        

        <img src="{{url('public/images/whmcs_doc_images/15.png')}}">

        <p>If You will get any banned IP address then </p>

        <p>Go to admin portal -> Banned IP’s on the left sidebar.</p>

        

        <img src="{{url('public/images/whmcs_doc_images/16.png')}}">

        <p>Remove banned IP from here. For reset permissions.</p>

        


        <img src="{{url('public/images/whmcs_doc_images/17.png')}}">
        <p>API roles should all select the checkbox of api’s function. please check the screens below for better understanding. </p>

        

        <img src="{{url('public/images/whmcs_doc_images/18.png')}}">
        <p>All API's function should be selected for API permissions</p>

        


        <img src="{{url('public/images/whmcs_doc_images/19.png')}}">
        <p>This is for all users of WHMCS.</p>

        

        <img src="{{url('public/images/whmcs_doc_images/20.png')}}">
        <p>Refer to the below screenshot for Orders sync.</p>

        

        <img src="{{url('public/images/whmcs_doc_images/21.png')}}">
        <p>Sync your WHMCS pipeline here.</p>

        

        <img src="{{url('public/images/whmcs_doc_images/22.png')}}">

        <p id="import"><b>Import Process</b></p>

        <p>Go to Import </p>

        

        <img src="{{url('public/images/whmcs_doc_images/23.png')}}">

        <p>Click on import products</p>

        


        <img src="{{url('public/images/whmcs_doc_images/24.png')}}">
        <p>Processing start for syncing</p>

        

        <img src="{{url('public/images/whmcs_doc_images/25.png')}}">
        <p>After Successfully imported data get a Success Button and Finish process shown as below screen. please have a look.</p>

        

        <img src="{{url('public/images/whmcs_doc_images/26.png')}}">
        <p>Automation Pricing

        <p>Pay pricing for automation Daily Sync for every 24 hours and Hour Sync for every 1 hour.</p>

        

        <img src="{{url('public/images/whmcs_doc_images/27.png')}}">

       

        <div class="float-right">
            <a href="https://www.webdew.com/kb/hubspot/steps-to-do-whmcs-integrations" target="_blank" >Full documentation</a>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

@section('scripts')
@parent

  <!--begin::Page Scripts(used by this page) -->
        <script src="htt
        ps://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
        <script type="text/javascript">
            "use strict";

            // Class definition
            var KTWizard2 = function () {
                // Base elements
                var wizardEl;
                var formEl;
                var validator;
                var wizard;

                var initValidation = function() {
                    validator = formEl.validate({
                        // Validate only visible fields
                        ignore: ":hidden",

                        // Validation rules
                        rules: {
                           
                            // routes :{
                            //     required: true,
                            //     maxlength: 3,
                            //     digits:true
                            // },
                            // country_code :{
                            //     required: true,
                            //     maxlength: 3,
                            //     digits:true
                            // },
                            // sender :{
                            //     required: true,
                            //     maxlength: 10,
                            // },
                            // MSG91 :{
                            //     required: true,
                            // }
                        },

                        // Display error
                        invalidHandler: function(event, validator) {
                            KTUtil.scrollTop();
                            swal.fire({
                                "title": "",
                                "text": "There are some errors in your submission. Please correct them.",
                                "type": "error",
                                "confirmButtonClass": "btn btn-secondary"
                            });
                        },

                        // Submit valid form
                        // submitHandler: function (form) {

                        // }
                    });
                }

                var initSubmit = function() {
                    var btn = formEl.find('[data-ktwizard-type="action-submit"]');

                    btn.on('click', function(e) {
                        e.preventDefault();

                        if (validator.form()) {
                            $('#kt_form').submit();     
                        }
                    });
                }

                return {
                    // public functions
                    init: function() {
                        wizardEl = KTUtil.get('kt_wizard_v2');
                        formEl = $('#kt_form');
                        initValidation();
                        initSubmit();
                    }
                };
            }();

            jQuery(document).ready(function() {
                KTWizard2.init();
                /** validation for guest example not to match with domain url **/
                jQuery.validator.addMethod("notEqual", function(value, element, param) {
                  return this.optional(element) || value != param;
                }, "Please specify a different value");
                /** end here code **/
                $.validator.addMethod('lessThanEqual', function(value, element, param) {
                    return this.optional(element) || parseInt(value) <= parseInt($(param).val());
                }, "The content cost must be less than number words");
            });

            jQuery(document).on('change','#type',function(e){
                    var vendor = $(this).val();
                    e.preventDefault();
                    $.ajax({
                         headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                          },
                           beforeSend: function() {
                                // setting a timeout
                                $(".loader").fadeIn("slow");
                            },
                          url  : "{{ url('/whmcs/render-vendor') }}",
                          type : 'POST',
                          data: {
                                'vendor' : vendor,
                             },
                          async: true,
                          //dataType: 'json',
                          enctype: 'multipart/form-data',
                          cache: false,                      
                          success: function(response){
                            $('#vendor-append').html(response);
                            $(".loader").fadeOut("slow");
                          },
                          error: function(){}
                        });
                        e.stopImmediatePropagation();
                        return false; 

            });

        </script>     
@show
@endsection