@extends('layout.app')
@php
$totalTitle = 'Data Automation';
@endphp
@section('title', $totalTitle)
@section('title-description')
<meta name="description" content="Data Automation">
@endsection
@section('content')
@section('head')
@parent    
@show
<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor bg-white pt-7 automate-page" id="kt_content">

        <!-- begin:: Content -->
        <div class="kt-container kt-grid__item kt-grid__item--fluid home-container">
           
            <div class="row">
                <!-- body content start -->
                <div class="col-12">
                    <div class="kt_blog-post pb-5">
                        
                        <div class="row justify-content-center">
                            <div class="col-xl-8 col-lg-8 col-md-12">
                                <span id="alert-span"></span>
                                @if(session()->has('message'))
                                    <div class="alert alert-{{ session()->get('alert') }}">
	                                    {{ session()->get('message') }}
	                                </div>
                                @endif

                                <div class="row">
                                    <div class="col">
                                        <div class="card">
                                            <div class="card-header">
                                                <label>Automation pricing</label>
                                            </div>
                                            <div class="card-body">
                                                <div class="card-body">
                                                	<?php
                                                		// $exist_id = DB::table('whmcs_payments')->where('portal_id',Auth::user()->portal_id)->where('end_date','>=',date('Y-m-d'))->value('id');
                                                		// $exist = DB::table('whmcs_payments')->where('portal_id',Auth::user()->portal_id)->where('end_date','>=',date('Y-m-d'))->first();

                                                		// $exist_id = DB::table('whmcs_payments')->where('portal_id',Auth::user()->portal_id)->value('id');
                                                		// $exist = DB::table('whmcs_payments')->where('portal_id',Auth::user()->portal_id)->first();
		                                                    	
                                                	?>

                                                	<div class="row">
                                                		<div class="col-lg-6 col-md-6">
                                                			<?php
										                        $portal_id = \Session::get('portal_id');
										                        $total_count = \DB::table('whmcs_payments')->where('user_id',Auth::user()->id)->where('app_id',2)->whereIn('app_type',['daily_month','daily_year'])->where('portal_id',$portal_id)->count();
										                        $payment_list = \DB::table('whmcs_payments')->where('user_id',Auth::user()->id)->where('app_id',2)->whereIn('app_type',['daily_month','daily_year'])->where('portal_id',$portal_id)->get()->toArray();
										                        $id = '';
										                        $expiry_date = '';
										                        $exist = '';
										                        $exist_id = 0;
										                        if($total_count > 0){
										                            $id = $payment_list[$total_count - 1]->id;
										                            $exist_id = $id;
										                            $expiry_date = $payment_list[$total_count - 1]->end_date;
										                            $exist = $payment_list[$total_count - 1];
 										                            $date = date_create($expiry_date);
										                            date_sub($date,date_interval_create_from_date_string("15 days"));
										                            $warning_date =  date_format($date,"Y-m-d");
										                            $current_date = date("Y-m-d");
										                        ?>   
										                            @if($current_date > $warning_date && $current_date < $expiry_date)
										                                 <div class="alert alert-warning">
										                                  <strong>Warning! </strong> Your Subscription will get expired on {{$expiry_date}}
										                                </div>
										                                <h2>Daily Sync </h2>
										                            @endif
										                            
										                            @if($current_date > $expiry_date)
										                                <div class="alert alert-danger">
										                                  Your Subscription for WHMCS app has been expired on {{$expiry_date}}.
										                                  Please renew it now.
										                                </div>
										                                <h2>Daily Sync </h2>
										                            @else
										                            	<h2>

			                                                			@if(@$exist->app_type == "daily_month" || @$exist->app_type == "daily_year")
			                                                				<input type="radio" name="app_type" @if($exist->app_type == "daily_month") {{"checked"}} @endif>
			                                                			@endif

			                                                			Daily Sync 
				                                                			@if(@$exist->app_type == "daily_month" || @$exist->app_type == "daily_year")
				                                                			 	<span class="active-batch" style="font-size: 13px">Active till
				                                                			 	<?php
				                                                			 		$date = date_create($exist->end_date);
																					echo date_format($date,"m-d-Y");
				                                                			 	?> </span>
				                                                			@endif
			                                                			</h2>
										                            @endif
										                        <?php
										                        } else {
										                        	?>
										                        	<h2>Daily Sync </h2>
										                        	<?php
										                        }
										                        ?>
                                                			

															<p>After every 24 hours sync your WHMCS to HubSpot.</p>
															<h3>Pricing : </h3>
															<?php
									                        $exist_id = DB::table('whmcs_payments')->where('portal_id',Auth::user()->portal_id)->where('end_date','>=',date('Y-m-d'))->value('id');
                                            				$exist = DB::table('whmcs_payments')->where('portal_id',Auth::user()->portal_id)->where('end_date','>=',date('Y-m-d'))->first();

									                        ?>   
															@if($exist_id < 1)
																<form role="form" action="{{url('/whmcs-stripe-payment')}}" method="post"  data-cc-on-file="false" data-stripe-publishable-key="{{$stripe_P}}" id="payment-form">
																	<h4> <input type="radio" class="radio_button" name="daily" value="daily_month" data-amount="9" id="test1" checked>
																	<label for="test1"> Monthly : $9 </label></h4>
																	<h4> <input type="radio" class="radio_button" name="daily" value="daily_year" data-amount="90" id="test2"><label for="test2"> Yearly : $90 </label></h4>
																	<br>
																	<input type="radio" name="daily_sync_time" value="24" id="test3" checked> <label for="test3">Daily</label>
																	<br>
																	<br>
				                                                    <span>
											                            @csrf
											                            <input type="hidden" name="email" value="{{Auth::user()->email}}">
											                            <input type="hidden" name="amount" id="daily_amount" value="9">
											                            <input type="hidden" name="sync_time" value="24">
											                            <noscript>You must <a href="http://www.enable-javascript.com" target="_blank">enable JavaScript</a> in your web browser in order to pay via Stripe.</noscript>
											                                <input 
											                                    type="submit"
											                                    class="btn btn-brand btn-bold kt-mr-10 pay" 
											                                    value="Pay $9"
											                                    data-key="{{$stripe_P}}"
											                                    data-amount="900"
											                                    data-payment="daily"
											                                    data-currency="usd"
											                                    data-name="stripe Payment"
											                                    data-email="{{Auth::user()->email}}"
											                                    id="daily"
											                                />
											                                <script src="https://checkout.stripe.com/v2/checkout.js"></script>
											                                <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
											                                <script>
											                                    $(document).ready(function() {
											                                        $(document).on('click','#daily' ,function(event) {
											                                            event.preventDefault();


											                                            var $button = $(this),
											                                                $form = $button.parents('form');


											                                            var opts = $.extend({}, $button.data(), {
											                                                token: function(result) {
											                                                $form.append($('<input>').attr({ type: 'hidden', name: 'stripeToken', value: result.id })).submit();
											                                                }
											                                            });


											                                            StripeCheckout.open(opts);
											                                        });
											                                    });
											                                </script>
				                                                    </span>
			                                                    </form>
		                                                    @else
		                                                    	@if($exist->app_type == "daily_month" || $exist->app_type == "daily_year") 
		                                                    	<form action="{{url('whmcs-payment-update')}}" method="POST">
	                                                    		@csrf
	                                                    		<input type="hidden" name="id" value="{{$exist_id}}">
	                                                    		@endif
			                                                    	<h4><input type="radio" class="radio_button" name="daily" value="daily_month" data-amount="9" id="test4"@if($exist->app_type == "daily_month") {{"checked"}} @endif ><label for="test4"> Monthly : $9 </label></h4>
																	<h4><input type="radio" class="radio_button" name="daily" value="daily_year" data-amount="90" id="test5" @if($exist->app_type == "daily_year") {{"checked"}} @endif><label for="test5"> Yearly : $90 </label></h4>
																	<br>
																	<input type="radio" name="daily_sync_time" value="24" id="test6" @if($exist->sync_time == 24) {{"checked"}} @endif> <label for="test6"> Daily</label>
																	<br>
																	<br>
																@if($exist->app_type == "daily_month" || $exist->app_type == "daily_year") 
																	<!-- <button type="submit" class="btn btn-brand btn-bold kt-mr-10">
																		Update
																	</button> -->
																</form>
																@endif
		                                                    @endif
                                                		</div>
                                                		<div class="col-lg-6 col-md-6">
                                                			<?php
										                        $portal_id = \Session::get('portal_id');
										                        $total_count = \DB::table('whmcs_payments')->where('user_id',Auth::user()->id)->where('app_id',2)->whereIn('app_type',['hour_month','hour_year'])->where('portal_id',$portal_id)->count();
										                        $payment_list = \DB::table('whmcs_payments')->whereIn('app_type',['hour_month','hour_year'])->where('user_id',Auth::user()->id)->where('app_id',2)->where('portal_id',$portal_id)->get()->toArray();
										                        $id = '';
										                        $expiry_date = '';
										                        $exist = '';
										                        $exist_id = 0;
										                        if($total_count > 0){
										                            $id = $payment_list[$total_count - 1]->id;
										                            $exist_id = $id;
										                            $expiry_date = $payment_list[$total_count - 1]->end_date;
										                            $exist = $payment_list[$total_count - 1];
 										                            $date = date_create($expiry_date);
										                            date_sub($date,date_interval_create_from_date_string("15 days"));
										                            $warning_date =  date_format($date,"Y-m-d");
										                            $current_date = date("Y-m-d");
										                        ?>   
										                            @if($current_date > $warning_date && $current_date < $expiry_date)
										                                 <div class="alert alert-warning">
										                                  <strong>Warning! </strong> Your Subscription will get expired on {{$expiry_date}}
										                                </div>
										                                <h2>Hour Sync </h2>
										                            @endif
										                            
										                            @if($current_date > $expiry_date)
										                                <div class="alert alert-danger">
										                                  Your Subscription for WHMCS app has been expired on {{$expiry_date}}.
										                                  Please renew it now.
										                                </div>
										                                <h2>Hour Sync </h2>
										                            @else
										                            	<h2>
										                            		@if(@$exist->app_type == "hour_month" || @$exist->app_type == "hour_year") 
				                                                				<input type="radio" name="app_type" data-amount="29" @if($exist->app_type == "hour_month" || $exist->app_type == "hour_year") {{"checked"}} @endif>
				                                                			@endif
				                                                			Hour Sync 
				                                                			
			                                                			 	@if(@$exist->app_type == "hour_month" || @$exist->app_type == "hour_year") 
			                                                			 		<span style="font-size: 13px">Active till 
			                                                			 			<?php
					                                                			 		$date = date_create($exist->end_date);
																						echo date_format($date,"m-d-Y");
					                                                			 	?>
			                                                			 		</span>
			                                                			 	@endif
			                                                			</h2>
										                            @endif
										                        <?php
										                        } else {
										                        	?>
										                        		<h2>Hour Sync </h2>
										                        	<?php
										                        }
										                        ?>
                                                			
															<p>After every hour sync your WHMCS to HubSpot.</p>
															<h3>Pricing : </h3>

															<?php
										                        $exist_id = DB::table('whmcs_payments')->where('portal_id',Auth::user()->portal_id)->where('end_date','>=',date('Y-m-d'))->value('id');
                                                				$exist = DB::table('whmcs_payments')->where('portal_id',Auth::user()->portal_id)->where('end_date','>=',date('Y-m-d'))->first();

										                        ?>   
															@if($exist_id < 1)
			                                                    <form role="form" action="{{url('/whmcs-stripe-payment')}}" method="post"  data-cc-on-file="false" data-stripe-publishable-key="{{$stripe_P}}" id="payment-form">
																	<h4> <input type="radio" class="radio_button" name="hour" value="hour_month" data-amount="29" id="test7" checked>
																		<label for="test7">Monthly : $29 </label></h4>
																	<h4> <input type="radio" class="radio_button" name="hour" value="hour_year" data-amount="290" id="test8"><label for="test8"> Yearly : $290</label> </h4>
																		<br>
																		<input type="radio" name="hour_sync_time" value="1" id="test9" checked> 
																		<label for="test9">Hourly</label>
																		<br>
																		<!-- <input type="radio" name="hour_sync_time" value="2" id="test10"> 
																		<label for="test10">Every 2 hours</label>
																		<br>
																		<input type="radio" name="hour_sync_time" value="4" id="test11"> 
																		<label for="test11">Every 4 hours</label>
																		<br>
																		<input type="radio" name="hour_sync_time" value="8" id="test12"> 
																		<label for="test12">Every 8 hours</label>
																		<br>
																		<input type="radio" name="hour_sync_time" value="16" id="test13"> 
																		<label for="test13">Every 16 hours</label>
																		<br>
																		<input type="radio" name="hour_sync_time" value="20" id="test14"> 
																		<label for="test14">Every 20 hours</label>
																		<br> -->
																		<br>
				                                                    <span>
											                            @csrf
											                            <input type="hidden" name="email" value="{{Auth::user()->email}}">
											                            <input type="hidden" name="amount" id="hour_amount" value="29">
											                            <input type="hidden" name="sync_time" value="1">
											                            <noscript>You must <a href="http://www.enable-javascript.com" target="_blank">enable JavaScript</a> in your web browser in order to pay via Stripe.</noscript>
											                                <input 
											                                    type="submit"
											                                    class="btn btn-brand btn-bold kt-mr-10 pay" 
											                                    value="Pay $29"
											                                    data-payment="hour"
											                                    data-key="{{$stripe_P}}"
											                                    data-amount="2900"
											                                    data-currency="usd"
											                                    data-name="stripe Payment"
											                                    data-email="{{Auth::user()->email}}"
											                                    id="hour"
											                                />
											                                <script src="https://checkout.stripe.com/v2/checkout.js"></script>
											                                <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
											                                <script>
											                                    $(document).ready(function() {
											                                        $(document).on('click','#hour' ,function(event) {
											                                            event.preventDefault();

											                                            var $button = $(this),
											                                                $form = $button.parents('form');


											                                            var opts = $.extend({}, $button.data(), {
											                                                token: function(result) {
											                                                $form.append($('<input>').attr({ type: 'hidden', name: 'stripeToken', value: result.id })).submit();
											                                                }
											                                            });


											                                            StripeCheckout.open(opts);
											                                        });
											                                    });
											                                </script>
				                                                    </span>
			                                                    </form>
		                                                    @else
		                                                    	@if($exist->app_type == "hour_month" || $exist->app_type == "hour_year") 
		                                                    	<form action="{{url('whmcs-payment-update')}}" method="POST">
		                                                    		@csrf
		                                                    		<input type="hidden" name="id" value="{{$exist_id}}">
		                                                    	@endif
			                                                    	<h4> <input type="radio" class="radio_button" name="hour" value="hour_month" data-amount="29" 
			                                                    		id="test15" @if($exist->app_type == "hour_month") {{"checked"}} @endif><label for="test15"> Monthly : $29 </label></h4>
																	<h4> <input type="radio" class="radio_button" name="hour" value="hour_year" data-amount="290"
																	id="test25" @if($exist->app_type == "hour_year") {{"checked"}} @endif><label for="test25"> Yearly : $290 </label></h4>
																	<br>
																	<input type="radio" name="hour_sync_time" value="1" id="test16" @if($exist->sync_time == 1) {{"checked"}} @endif> 
																	<label for="test16">Hourly</label>
																	<br>
																	<!-- <input type="radio" name="hour_sync_time" value="2" id="test17" @if($exist->sync_time == 2) {{"checked"}} @endif> 
																	<label for="test17">Every 2 hours</label>
																	<br>
																	<input type="radio" name="hour_sync_time" value="4" id="test18" @if($exist->sync_time == 4) {{"checked"}} @endif> 
																	<label for="test18">Every 4 hours</label>
																	<br>
																	<input type="radio" name="hour_sync_time" value="8" id="test19" @if($exist->sync_time == 8) {{"checked"}} @endif> 
																	<label for="test19">Every 8 hours</label>
																	<br>
																	<input type="radio" name="hour_sync_time" value="16" id="test20" @if($exist->sync_time == 16) {{"checked"}} @endif>
																	<label for="test20"> Every 16 hours</label>
																	<br>
																	<input type="radio" name="hour_sync_time" value="20" id="test21" @if($exist->sync_time == 20) {{"checked"}} @endif>
																	<label for="test21"> Every 20 hours</label>
																	<br> -->
																	<br>
																@if($exist->app_type == "hour_month" || $exist->app_type == "hour_year") 
																	<!-- <button type="submit" class="btn btn-brand btn-bold kt-mr-10">
																		Update
																	</button> -->
																</form>
																@endif
		                                                    @endif
                                                		</div>
                                                	</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>                                  
                    </div>
                </div>
                <!-- body content end -->
            </div>
        </div>
    </div>                  
</div>

@section('scripts')
@parent

  <!--begin::Page Scripts(used by this page) -->
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
        <script type="text/javascript">

        jQuery(document).on('click','.radio_button',function(e) {
        	//e.preventDefault();
            var amount = $(this).attr('data-amount');
            var name = $(this).attr('name');
            $('#'+name+'_amount').val(amount);
            $('#'+name).val('Pay $' + amount);
            amount = amount * 100;            
            $('#'+name).attr('data-amount',amount);
            console.log(amount);
            //e.stopImmediatePropagation();
            //return false;
        });
        //    "use strict";

            // // Class definition
            // var KTWizard2 = function () {
            //     // Base elements
            //     var wizardEl;
            //     var formEl;
            //     var validator;
            //     var wizard;

            //     var initValidation = function() {
            //         validator = formEl.validate({
            //             // Validate only visible fields
            //             ignore: ":hidden",

            //             // Validation rules
            //             rules: {
                           
            //                 // routes :{
            //                 //     required: true,
            //                 //     maxlength: 3,
            //                 //     digits:true
            //                 // },
            //                 // country_code :{
            //                 //     required: true,
            //                 //     maxlength: 3,
            //                 //     digits:true
            //                 // },
            //                 // sender :{
            //                 //     required: true,
            //                 //     maxlength: 10,
            //                 // },
            //                 // MSG91 :{
            //                 //     required: true,
            //                 // }
            //             },

            //             // Display error
            //             invalidHandler: function(event, validator) {
            //                 KTUtil.scrollTop();
            //                 swal.fire({
            //                     "title": "",
            //                     "text": "There are some errors in your submission. Please correct them.",
            //                     "type": "error",
            //                     "confirmButtonClass": "btn btn-secondary"
            //                 });
            //             },

            //             // Submit valid form
            //             // submitHandler: function (form) {

            //             // }
            //         });
            //     }

            //     var initSubmit = function() {
            //         var btn = formEl.find('[data-ktwizard-type="action-submit"]');

            //         btn.on('click', function(e) {
            //             e.preventDefault();

            //             if (validator.form()) {
            //                 $('#kt_form').submit();     
            //             }
            //         });
            //     }

            //     return {
            //         // public functions
            //         init: function() {
            //             wizardEl = KTUtil.get('kt_wizard_v2');
            //             formEl = $('#kt_form');
            //             initValidation();
            //             initSubmit();
            //         }
            //     };
            // }();

            // jQuery(document).ready(function() {
            //     KTWizard2.init();
            //     /** validation for guest example not to match with domain url **/
            //     jQuery.validator.addMethod("notEqual", function(value, element, param) {
            //       return this.optional(element) || value != param;
            //     }, "Please specify a different value");
            //     /** end here code **/
            //     $.validator.addMethod('lessThanEqual', function(value, element, param) {
            //         return this.optional(element) || parseInt(value) <= parseInt($(param).val());
            //     }, "The content cost must be less than number words");
            // });

        // function loadData(url, ds){
        //     $.ajax({
        //         headers: {
        //             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        //         },
        //         beforeSend: function() {
        //             // setting a timeout
        //             //$(".loader").fadeIn("slow");
        //         },
        //         url  : url,
        //         type : 'GET',
        //         async: true,
        //         //dataType: 'json',
        //         enctype: 'multipart/form-data',
        //         cache: false,                      
        //         success: function(response){
        //             if(response.status === true){
        //                 const  bullets =  [...document.querySelectorAll('.bullet')];
        //                 if(response.name == 'product') {
        //                     bullets[0].classList.add('completed');    
        //                 }
        //                 if(response.name == 'contact') {
        //                     bullets[1].classList.add('completed');    
        //                 }
                        
        //                 $('#vendor-append').html(response.html);
        //                 $('#alert-span').html('<div class="alert alert-success">'+response.message+'</div>');
        //                 if(response.name == 'finish') {
        //                     bullets[2].classList.add('completed');
        //                     bullets[3].classList.add('completed');
        //                     $(".loader").fadeOut("slow");
        //                 } else {
        //                     var url = $('.sync').attr('data-url');
        //                     var ds = $('.sync').attr('data-step');
        //                     loadData(url, ds);
        //                 }
                        
        //             } else {
        //                 $('#alert-span').html('<div class="alert alert-danger">'+response.message+'</div>');
        //                 $(".loader").fadeOut("slow");
        //             }
                    
        //         },
        //         error: function(){

        //         }
        //     });
        // }
        // // jQuery(document).on('click','.finish',function(e){
        // //     const  bullets  =  [...document.querySelectorAll('.bullet')];
        // //     bullets[3].classList.add('completed');
        // // });   
        // jQuery(document).on('click','.sync',function(e) {
        //     $(".loader").fadeIn("slow");
        //     //e.preventDefault();
        //     var url = $(this).attr('data-url');
        //     var ds = $(this).attr('data-step');
        //     $('#alert-span').html('');
        //     loadData(url, ds);
        //     // e.stopImmediatePropagation();
        //     // return false;
        // });

        </script>
@show
@endsection