<form role="form" action="{{ route('stripe.payment') }}" method="post"  data-cc-on-file="false" 
                                                    data-stripe-publishable-key="{{ env('STRIPE_KEY') }}"  id="payment-form">
                                                @csrf
                                                    <input id="amount" value="" name="amount" type="hidden">
                                                    <input type="hidden" id="tot_price" value="19" />
                                                    <input type="hidden" name="stripe_promo_id" id="stripe_promo_id" value="0" />
                                                    <input type="hidden" id="stripe_wallet" name="wallet" value="19">

 
                                                    <noscript>You must <a href="http://www.enable-javascript.com" target="_blank">enable JavaScript</a> in your web browser in order to pay via Stripe.</noscript>
                                                       
                                                        <!-- <i class="fa fa-credit-card"></i> -->
                                                        
                                                        <input 
                                                            type="submit"
                                                            class="btn btn-brand btn-upper btn-block" 
                                                            value="Pay Now with Card"
                                                            data-key={{env('STRIPE_KEY')}}
                                                            data-amount="1900"
                                                            data-currency="usd"
                                                            data-name="GuestPostEngine"
                                                            data-email=""
                                                            id="stripe"
                                                            
                                                        />
                                                        
                                                        <script src="https://checkout.stripe.com/v2/checkout.js"></script>
                                                        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
                                                        
                                                        <script>
                                                        $(document).ready(function() {
                                                            $(document).on('click','#stripe' ,function(event) {
                                                                event.preventDefault();

 
                                                                var $button = $(this),
                                                                    $form = $button.parents('form');

 
                                                                var opts = $.extend({}, $button.data(), {
                                                                    token: function(result) {
                                                                    $form.append($('<input>').attr({ type: 'hidden', name: 'stripeToken', value: result.id })).submit();
                                                                    }
                                                                });

 
                                                                StripeCheckout.open(opts);
                                                            });
                                                        });
                                                        </script>
                                                </form>