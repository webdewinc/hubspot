<?php
$vendor = vendor_select('bulkgate');
?>
<p><a href="javascript:void(0);" title="Webdew Groups" ><strong id="select-vendor-append">{{$vendor['name']}}</strong></a>
<a href="https://www.bulkgate.com/en/developers/http-sms-api/" target="_blank" hidden style="float:right;">Api Docs</a>

<button type="button" data-toggle="modal" data-target="#myModal" style="float:right;">more info</button>

</p> 

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog" >

    <!-- Modal content-->
    <div class="modal-content">
   
      <div class="modal-body">
        <h4>To find Api credentials follow the below mentioned steps:</h4>
      </div>
      <ul>
        <li>Open your bulk Gate Account</li>
        <li>goto Madule and APi</li>
        <li>Click on simple Api</li>   
      </ul>

      <img src="{{url('public/images/sms_vendors/BulkGate 1.png')}}">
      <br>
       <ul>
       <li>Click on Create API</li>  
      </ul>
      <img src="{{url('public/images/sms_vendors/bulkgate2.png')}}">
      <br>
        <ul>
        <li>Copy application ID</li>
        <li>Copy application Token</li> 
      </ul>
      <img src="{{url('public/images/sms_vendors/bukgate 3.png')}}">
      <br>

     <!--  bukgate 3.png

     -->  
    
      
     
      <p id="hubapi"><b>HubSpot Key Process:</b></p>

        <p>Go to HubSpot portal -> click on the gear button of the top-right menu.</p>


        

        <img src="{{url('public/images/whmcs_doc_images/8.png')}}">

        <p>Navigate to the left menus and select Integrations :</p>

        <img src="{{url('public/images/whmcs_doc_images/9.png')}}">
       
        <p>After Selected Integrations then select “API Key” and copy the HubSpot key.</p>

         <img src="{{url('public/images/whmcs_doc_images/10.png')}}">

         <a href="https://portal.bulkgate.com/sign/up"  target="_blank" style="float:right;">Full Documentation</a>

      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>            
@csrf
<div class="kt-wizard-v2__form">
        
    <?php
        if(empty($data)){
            $button = 'Save';
        } else {
            $button = 'Update';
        }
    ?>
    <div class="form-row">
        <div class="form-group col-md-6 col-12">
            <label class="required">{{$vendor['label']['app_id']}}</label>
            <div class="input-group">
                <input class="form-control" type="text" id="app_id" name="app_id" value="{{@$data['app_id']}}" required="" autocomplete="off">
            </div>
        </div>
		<div class="form-group col-md-6 col-12">
            <label class="required">{{$vendor['label']['token']}}</label>
            <div class="input-group">
                <input class="form-control" type="text" id="token" name="token" value="{{@$data['token']}}"  required="" autocomplete="off">
            </div>
        </div>
    </div>
    
         <div class="form-row">
		<div class="form-group col-md-6 col-12">
            <label class="required">{{$vendor['label']['sender_id']}}</label>
            <div class="input-group">
                <input class="form-control" type="text" id="sender_id" name="sender_id" value="{{@$data['sender_id']}}"  required="" autocomplete="off">
            </div>
        </div>
        
             <div class="form-group col-md-6 col-12">
            <label class="required">{{$vendor['label']['unicode']}}</label>
            <div class="input-group">
                <select class="form-control" type="text" id="unicode" name="unicode" required="">
                    <option value=""></option>
                    <option value="1" @if(@$data['unicode'] == 1) {{'selected'}} @endif>{{$vendor['unicode'][1]}}</option>
                    <option value="0" @if(@$data['unicode'] == 0) {{'selected'}} @endif>{{$vendor['unicode'][0]}}</option>
                </select>
            </div>
        </div>
    </div>
   
      <div class="form-row">
          <div class="form-group col-md-6 col-12">
            <label class="required">{{$vendor['label']['t_hapi_key']}}</label>
            <div class="input-group">
                <input class="form-control" type="text" id="t_hapi_key" name="t_hapi_key" value="{{@$data['t_hapi_key']}}" required="" minlength="10"  autocomplete="off">
            </div>
        </div>
	 </div>

		 <div class="form-group col-md-6 col-12">            
            <div class="input-group">
                
				<label class="kt-checkbox">
				   <input type="checkbox" id="default" name="default" value="bulkgate" @if(@$type == 'bulkgate'){{'checked'}}@endif> Set as default
					<span></span>
				</label>
            </div>
        </div>
   
   
    <div class="kt-form__actions">
        <button class="btn btn-label-brand btn-bold"  type="submit" data-ktwizard-type="action-submit">
            {{$button}}
        </button>
    </div>
</div>