<?php
$vendor = vendor_select('bulksms');
?>
<p><a href="javascript:void(0);" title="Webdew Groups" ><strong id="select-vendor-append">{{$vendor['name']}}</strong></a>
<a href="https://www.bulksms.com/developer/json/v1/#" target="_blank"  hidden style="float:right;">Api Docs</a>

 <button type="button" data-toggle="modal" data-target="#myModal" style="float:right;">more info</button>

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog" >

    <!-- Modal content-->
    <div class="modal-content">
   
      <div class="modal-body">
        <h4>To find Api credentials follow the below mentioned steps:</h4>
      </div>
    
      <ul>
        <li>create bulksms account</li>
        <li>use same username and password in these fields </li>
        <li>copy and paste the hubspot api key </li>

      </ul>
     
      
     
      <p id="hubapi"><b>HubSpot Key Process:</b></p>

        <p>Go to HubSpot portal -> click on the gear button of the top-right menu.</p>


        

        <img src="{{url('public/images/whmcs_doc_images/8.png')}}">

        <p>Navigate to the left menus and select Integrations :</p>

        <img src="{{url('public/images/whmcs_doc_images/9.png')}}">
       
        <p>After Selected Integrations then select “API Key” and copy the HubSpot key.</p>

         <img src="{{url('public/images/whmcs_doc_images/10.png')}}">
      <a href="https://www.bulksms.com/developer/json/v1/#" target="_blank"  style="float:right;">Full Documentation</a>

         <!-- <a href="https://www.twilio.com/docs/usage/api" target="_blank"  style="float:right;">Full Documentation</a> -->

      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>





</p>  




@csrf
<div class="kt-wizard-v2__form">
        
    <?php
        if(empty($data)){
            $button = 'Save';
        } else {
            $button = 'Update';
        }
    ?>
    <div class="form-row">
        <div class="form-group col-md-6 col-12">
            <label class="required">{{$vendor['label']['u_name']}}</label>
            <div class="input-group">
                <input class="form-control" type="text" id="u_name" name="u_name" value="{{@$data['u_name']}}" required="" autocomplete="off">
            </div>
        </div>
		<div class="form-group col-md-6 col-12">
            <label class="required">{{$vendor['label']['pass']}}</label>
            <div class="input-group">
                <input class="form-control" type="password" id="pass" name="pass" value="{{@$data['pass']}}"  required="" autocomplete="off">
            </div>
        </div>
    </div>
          <div class="form-row">
          <div class="form-group col-md-6 col-12">
            <label class="required">{{$vendor['label']['t_hapi_key']}}</label>
            <div class="input-group">
                <input class="form-control" type="text" id="t_hapi_key" name="t_hapi_key" value="{{@$data['t_hapi_key']}}" required="" minlength="10"  autocomplete="off">
            </div>
        </div>
	 </div>
   
 
		 <div class="form-group col-md-6 col-12">            
            <div class="input-group">
                
				<label class="kt-checkbox">
				   <input type="checkbox" id="default" name="default" value="bulksms" @if(@$type == 'bulksms'){{'checked'}}@endif> Set as default
					<span></span>
				</label>
            </div>
        </div>
   
   
    <div class="kt-form__actions">
        <button class="btn btn-label-brand btn-bold"  type="submit" data-ktwizard-type="action-submit">
            {{$button}}
        </button>
    </div>
</div>