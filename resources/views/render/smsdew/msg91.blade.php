<?php
$vendor = vendor_select('msg91');
?>
<div><a href="javascript:void(0);" title="Webdew Groups" ><strong id="select-vendor-append">{{$vendor['name']}}</strong></a> 
    <a href="https://docs.msg91.com/collection/msg91-api-integration/5/pages/139" hidden target="_blank" style="float:right;">Api Docs</a>   

<button type="button" data-toggle="modal" data-target="#myModal" style="float:right;">more info</button>

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog" >

    <!-- Modal content-->
    <div class="modal-content">
   
      <div class="modal-body">
        <h4>To find Api credentials follow the below mentioned steps:</h4>
      </div>
      <ul>
        <li>Open your Msg91 Account</li>
        <li>click on sms API</li>
        <li>click on auth key</li>
        <li>Now generate the auth key copy it into the setting fields</li>
        <li>After getting your auth key , click on sender id option in menu .create new sender id and then copy that into your setting fields</li> 
        <li>For sending sms to indian no . your account must be dlt verified .After getting verified copy your dlt id and paste it into respective settings fields</li>   
      </ul>

      <img src="{{url('public/images/sms_vendors/msg91.png')}}">
      
     
      
     
      <p id="hubapi"><b>HubSpot Key Process:</b></p>

        <p>Go to HubSpot portal -> click on the gear button of the top-right menu.</p>


        

        <img src="{{url('public/images/whmcs_doc_images/8.png')}}">

        <p>Navigate to the left menus and select Integrations :</p>

        <img src="{{url('public/images/whmcs_doc_images/9.png')}}">
       
        <p>After Selected Integrations then select “API Key” and copy the HubSpot key.</p>

         <img src="{{url('public/images/whmcs_doc_images/10.png')}}">

         <a href="https://docs.msg91.com/collection/msg91-api-integration/5/pages/139"  target="_blank" style="float:right;">Full Documentation</a>

      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


     </div>     
     
@csrf
<div class="kt-wizard-v2__form">
        
    <?php
        if(empty($data)){
            $button = 'Save';
        } else {
            $button = 'Update';
        }
    ?>
    <div class="form-row">
        <div class="form-group col-md-6 col-12">
            <label class="required">{{$vendor['label']['msg91']}}</label>
            <div class="input-group">
                <input class="form-control" type="text" id="msg91" name="msg91" value="{{@$data['msg91']}}" minlength="15" maxlength="50" required="" autocomplete="off">
            </div>
        </div>
		<div class="form-group col-md-6 col-12">
            <label class="required">{{$vendor['label']['routes']}}</label>
            <div class="input-group">
                <select class="form-control" id="routes" name="routes" required="">
                    <option value=""></option>
<!--                     <option value="default" @if(@$data['routes'] == $vendor['routes']['default']) {{'selected'}} @endif>{{$vendor['routes']['default']}}</option> -->
                    <option value="1" @if(@$data['routes'] == 1) {{'selected'}} @endif>{{$vendor['routes'][1]}}</option>
                    <option value="4" @if(@$data['routes'] == 4) {{'selected'}} @endif>{{$vendor['routes'][4]}}</option>
                </select>
            </div>
        </div>
    </div>     
    
        <div class="form-row">
            <div class="form-group col-md-6 col-12">
                <label class="required">{{$vendor['label']['country_code']}}</label>
                <div class="input-group">
                    <select class="form-control" type="text" id="country_code" name="country_code" required="">
                        <option value=""></option>
                        <option value="0" @if(@$data['country_code'] == 0) {{'selected'}} @endif>{{$vendor['country_code']['0']}}</option>
                        <option value="1" @if(@$data['country_code'] == 1) {{'selected'}} @endif>{{$vendor['country_code']['1']}}</option>
                        <option value="91" @if(@$data['country_code'] == 91) {{'selected'}} @endif>{{$vendor['country_code']['91']}}</option>
                    </select>
                </div>
            </div>
			<div class="form-group col-md-6 col-12">
				<label class="required">{{$vendor['label']['sender']}}</label>
				<div class="input-group">
					<input class="form-control" type="text" id="sender" name="sender" value="{{@$data['sender']}}" required="" minlength="3" maxlength="10" autocomplete="off">
				</div>
			</div>
        </div>
   
    <div class="form-row">
          <div class="form-group col-md-6 col-12">
            <label class="required">{{$vendor['label']['t_hapi_key']}}</label>
            <div class="input-group">
                <input class="form-control" type="text" id="t_hapi_key" name="t_hapi_key" value="{{@$data['t_hapi_key']}}" required="" minlength="10"  autocomplete="off">
            </div>
        </div>
        <div class="form-group col-md-6 col-12">
            <label class="required">{{$vendor['label']['dlt_key']}}</label>
            <div class="input-group">
                <input class="form-control" type="text" id="dlt_key" name="dlt_key" value="{{@$data['dlt_key']}}" required="" minlength="10" autocomplete="off">
            </div>
        </div>
	 </div>
   
    <div class="form-group row">
        <div class="col-md-6 col-12">            
            <div class="input-group">
                <!--<input type="checkbox" id="default" name="default" value="msg91" @if(@$type == 'msg91'){{'checked'}}@endif> <label>Set as default</label>-->
				<label class="kt-checkbox">
				   <input type="checkbox" id="default" name="default" value="msg91" @if(@$type == 'msg91'){{'checked'}}@endif> Set as default
					<span></span>
				</label>
            </div>
        </div>
    </div>
    <div class="kt-form__actions">
        <button class="btn btn-label-brand btn-bold"  type="submit" data-ktwizard-type="action-submit">
            {{$button}}
        </button>
    </div>
</div>