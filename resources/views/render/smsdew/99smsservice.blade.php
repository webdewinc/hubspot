<?php
$vendor = vendor_select('99smsservice');
?>
<div>

    <a href="javascript:void(0);" title="Webdew Groups" ><strong id="select-vendor-append">{{$vendor['name']}}</strong></a>   

    <button type="button" data-toggle="modal" data-target="#myModal" style="float:right;">more info</button>

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog" >

    <!-- Modal content-->
    <div class="modal-content">
   
      <div class="modal-body">
        <h4>To find Api credentials follow the below mentioned steps:</h4>
      </div>
    
      
     
      
     
      <p id="hubapi"><b>HubSpot Key Process:</b></p>

        <p>Go to HubSpot portal -> click on the gear button of the top-right menu.</p>


        

        <img src="{{url('public/images/whmcs_doc_images/8.png')}}">

        <p>Navigate to the left menus and select Integrations :</p>

        <img src="{{url('public/images/whmcs_doc_images/9.png')}}">
       
        <p>After Selected Integrations then select “API Key” and copy the HubSpot key.</p>

         <img src="{{url('public/images/whmcs_doc_images/10.png')}}">
        <a href="https://docs.msg91.com/collection/msg91-api-integration/5/pages/139" target="_blank" style="float:right;">Full Documentation</a> 
         <!-- <a href="https://www.twilio.com/docs/usage/api" target="_blank"  style="float:right;">Full Documentation</a> -->

      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>



</p>  



 </div>     
     
@csrf
<div class="kt-wizard-v2__form">
        
    <?php
        if(empty($data)){
            $button = 'Save';
        } else {
            $button = 'Update';
        }
    ?>
    <div class="form-row">
        <div class="form-group col-md-6 col-12">
            <label class="required">{{$vendor['label']['apikey_ct']}}</label>
            <div class="input-group">
                <input class="form-control" type="text" id="apikey_ct" name="apikey_ct" value="{{@$data['apikey_ct']}}" minlength="15" maxlength="50" required="" autocomplete="off">
            </div>
        </div>
			<div class="form-group col-md-6 col-12">
				<label class="required">{{$vendor['label']['from']}}</label>
				<div class="input-group">
					<input class="form-control" type="text" id="from" name="from" value="{{@$data['from']}}" required="" minlength="3" maxlength="10" autocomplete="off">
				</div>
			</div>
    </div>     
    
        <div class="form-row">
          <div class="form-group col-md-6 col-12">
            <label class="required">{{$vendor['label']['t_hapi_key']}}</label>
            <div class="input-group">
                <input class="form-control" type="password" id="t_hapi_key" name="t_hapi_key" value="{{@$data['t_hapi_key']}}" required="" minlength="10"  autocomplete="off">
            </div>
        </div> 

        </div>
   

   
    <div class="form-group row">
        <div class="col-md-6 col-12">            
            <div class="input-group">
                <!--<input type="checkbox" id="default" name="default" value="msg91" @if(@$type == 'msg91'){{'checked'}}@endif> <label>Set as default</label>-->
				<label class="kt-checkbox">
				   <input type="checkbox" id="default" name="default" value="99smsservice" @if(@$type == '99smsservice'){{'checked'}}@endif> Set as default
					<span></span>
				</label>
            </div>
        </div>
    </div>
    <div class="kt-form__actions">
        <button class="btn btn-label-brand btn-bold"  type="submit" data-ktwizard-type="action-submit">
            {{$button}}
        </button>
    </div>
</div>