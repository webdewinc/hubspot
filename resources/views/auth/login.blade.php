@extends('layout.login')
@php
$totalTitle = 'Sign up';
@endphp
@section('title', $totalTitle)


@section('content')

<!-- Authentication Links --> 
@if(session()->has('message'))
    <?php
        $form = session()->get('form');
        if($form == ''){
            $form = 'signin';
            $result = 1;
            $message = 'Password has been changed.';
            $alert = 'success';
        } else {
            $result = 0;
        }
        
    ?>
@else 
    <?php
        $form = 'signin';
        $result = 0;
    ?>
@endif
<style type="text/css">
    .kt-grid__item.kt-grid__item--fluid.kt-grid__item--center.kt-grid.kt-grid--ver.kt-login__content {
        background: #009bff;
    }
    .fa {
  padding: 20px;
  font-size: 30px;
  width: 50px;
  text-align: center;
  text-decoration: none;
  margin: 5px 2px;
  border-radius:2%;
}
.fa:hover {
    opacity: 0.7;
}
.fa-linkedin {
  background: #007bb5;
  color: white;
}
.fa-google {
  background: #dd4b39;
  color: white;
}
.hubspot{
    height: 54px;
    width: 53px;
    border-radius: 2%;
    margin-right: 3px;
}
.hubspot:hover {
    opacity: 0.7;
}

  
</style>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<div class="kt-grid kt-grid--ver kt-grid--root kt-page">
    <div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v6 kt-login--{{$form}}" id="kt_login">
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--desktop kt-grid--ver-desktop kt-grid--hor-tablet-and-mobile">
            <div class="kt-grid__item  kt-grid__item--order-tablet-and-mobile-2  kt-grid kt-grid--hor kt-login__aside">
                <div class="kt-login__wrapper">
                    <div class="kt-login__container">
                        <div class="kt-login__body">
                            <div class="kt-login__logo">
                                <!-- <a href="#">
                                    <img src="assets/media/company-logos/logo-2.png">
                                </a> -->
                                @include('layout.include.partials.brand')
                            </div>
                            
                            <div class="kt-login__signin">
                                <div class="kt-login__head">
                                    <h3 class="kt-login__title">Sign In</h3>
                                </div>                               

                                <div class="kt-login__form">
                                    @if(session()->has('message'))
                                        @if(session()->get('form') == 'signin')
                                        <div class="alert alert-outline-{{ session()->get('alert') }} fade show" role="alert">
                                            <div class="alert-icon"><i class="flaticon-warning"></i></div>
                                            <div class="alert-text">{{ session()->get('message') }}</div>
                                            <div class="alert-close">
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                    <span aria-hidden="true"><i class="la la-close"></i></span>
                                                </button>
                                            </div>
                                        </div>
                                        @else
                                            @if($result == 1)
                                            <div class="alert alert-outline-{{ $alert }} fade show" role="alert">
                                                <div class="alert-icon"><i class="flaticon-warning"></i></div>
                                                <div class="alert-text">{{ $message }}</div>
                                                <div class="alert-close">
                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                        <span aria-hidden="true"><i class="la la-close"></i></span>
                                                    </button>
                                                </div>
                                            </div> 
                                            @endif
                                        @endif
                                    @endif
                                    

                                    <form class="kt-form" method="POST" action="{{ route('login') }}">
                                        @csrf
                                        <div class="form-group">
											<label class="required">Email Address</label>
                                            <div class="input-group">
                                                <input id="user_email" type="email" class="form-control @error('user_email') is-invalid @enderror" name="user_email" value="{{ old('user_email') }}" required autocomplete="email" autofocus placeholder="Email" maxlength="100">
                                            </div>
                                                @error('user_email')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            <span class="form-text text-muted">We'll never share your email with anyone else.</span>
                                        </div>
                                        <div class="form-group">
											<label class="required">Password</label>  
                                            <div class="input-group">
												
                                                <input id="password_login" type="password" class="form-control @error('password_login') is-invalid @enderror" name="password_login" required autocomplete="password" placeholder="Password" minlength="8" maxlength="15">
                                            </div>

                                            
                                              <span class="msg-error error"></span>
                                            @error('password_login')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>

                                       <div>
                                        <div class="g-recaptcha" id="recaptcha" data-sitekey="6Lf0ycYaAAAAABVooxSE1vEPqxJ3DVUkc8KXEuCP"></div>

                                          @if ($errors->has('g-recaptcha-response'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                                            </span>
                                        @endif
                                         </div>

                                        <div class="kt-login__extra">
                                            <label class="kt-checkbox">
                                                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}> Remember me
                                                <span></span>
                                            </label>
                                            
                                            @if (Route::has('password.request'))
                                                <a class="kt-link" href="{{ route('password.request') }}"  id="kt_login_forgot">
                                                    {{ __('Forget Password ?') }}
                                                </a>
                                            @endif

                                        </div>
                                        <div class="kt-login__actions">
                                            <button type="submit" id="kt_logins" class="btn btn-label-brand btn-bold">
                                                    {{ __('Sign In') }}
                                            </button>
                                            <br>
                                            <hr>
                                            
                                             <!-- <a  href="{{url('/auth2-callback')}}" ><img src="{{ url('/public/images/Hubspot-b.png') }}" class="hubspot"></a>  -->
                                              <a href="{{ url('/login/linkedin') }}" class="fa fa-linkedin"></a>
                                         <a href="{{ url('/redirect') }}" class="fa fa-google"> 
                                           </a>
                                        </div>
                                    </form>   
                                  
                                        
                                               
                                              
                                                                                   
                                </div>
                            </div>
                            <div class="kt-login__signup">
                                <div class="kt-login__head">
                                    <h3 class="kt-login__title">Sign Up</h3>
                                    <div class="kt-login__desc">Enter your details to create your account.</div>
                                </div>
                                <div class="kt-login__form"> 
                                    @if(session()->has('message'))
                                        @if(session()->get('form') == 'signup')
                                        <div class="alert alert-outline-{{ session()->get('alert') }} fade show" role="alert">
                                            <div class="alert-icon"><i class="flaticon-warning"></i></div>
                                            <div class="alert-text">{{ session()->get('message') }}</div>
                                            <div class="alert-close">
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                    <span aria-hidden="true"><i class="la la-close"></i></span>
                                                </button>
                                            </div>
                                        </div>
                                        @endif
                                        
                                    @endif                                
                                    <form method="POST" class="kt-form" action="{{ route('register') }}">
                                    @csrf
                                        <div class="form-group">
											<label class="required">First Name</label>
                                            <div class="input-group">
                                                
                                                <input id="first_name" type="text" class="form-control @error('first_name') is-invalid @enderror" name="user_fname" value="{{ old('first_name') }}" required autocomplete="first_name" autofocus placeholder="First Name">
                                            </div>
                                            @error('first_name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
											<label class="required">Last Name</label>
                                            <div class="input-group">
                                                
                                                <input id="last_name" type="text" class="form-control @error('last_name') is-invalid @enderror" name="user_lname" value="{{ old('last_name') }}" required autocomplete="last_name" autofocus placeholder="Last Name">
                                            </div>
                                            @error('last_name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
											<label class="required">Email Address</label>
                                            <div class="input-group">
                                                
                                                <input id="user_code" type="text" class="form-control @error('user_code') is-invalid @enderror" name="user_code" value="{{ old('user_code') }}" required autocomplete="user_code" autofocus placeholder="Username">
                                            </div>
                                            @error('user_code')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
											<label class="required">Email Address</label>
                                            <div class="input-group">
                                                
                                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Email">
                                            </div>
                                            @error('email')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
											<label class="required">Password</label>
                                            <div class="input-group">
                                                
                                                <span toggle="#password" class="fa fa-fw toggle-password fa-eye-slash"></span>
                                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="password" placeholder="Password" minlength="8" maxlength="15">
                                            </div>
                                            @error('password')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
											<label class="required">Confirm Password</label>
                                            <div class="input-group">
                                                
                                                <span toggle="#password_confirm" class="fa fa-fw toggle-password fa-eye-slash"></span>
                                                <input id="password_confirm" type="password" class="form-control" name="password_confirm" required autocomplete="password_confirm" placeholder="Confirm Password" minlength="8" maxlength="15">
                                            </div>
                                        </div>
                                        <!-- <div class="g-recaptcha" id="recaptcha" data-sitekey="6LdQRbgUAAAAACb4r31Veez9xCKdTHi86sA2K6Ni"></div> -->
                                        <span class="msg-error error"></span>
                                        <div id="captcha"></div>
                                        <div class="kt-login__extra">
                                            <label class="kt-checkbox">
                                                <input type="checkbox" name="agree">
                                                <span></span>
                                                I Agree the <a href="https://help.fly.biz/terms-of-service" target="_blank" class="kt-link">terms and conditions</a>.
                                               
                                            </label>
                                        </div>
                                        <div class="kt-login__actions">
                                            <button class="btn btn-primary btn-bold" id="kt_register">Sign Up</button>
                                            <a href="{{URL::asset('signin')}}" class="btn btn-label-primary btn-bold">Sign In</a>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="kt-login__forgot">
                                <div class="kt-login__head">
                                    <h3 class="kt-login__title">Forget Password ?</h3>
                                    <div class="kt-login__desc">Enter your email to reset your password.</div>
                                </div>
                                <div class="kt-login__form">
                                    @if(session()->has('message'))
                                        @if(session()->get('form') == 'forgot')
                                        <div class="alert alert-outline-{{ session()->get('alert') }} fade show" role="alert">
                                            <div class="alert-icon"><i class="flaticon-warning"></i></div>
                                            <div class="alert-text">{{ session()->get('message') }}</div>
                                            <div class="alert-close">
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                    <span aria-hidden="true"><i class="la la-close"></i></span>
                                                </button>
                                            </div>
                                        </div>
                                        @endif
                                    @endif
                                    <form class="kt-form" method="POST" action="{{ route('password.email') }}">
                                        @csrf
                                        <div class="form-group">
											<label class="required">Email Address</label>
                                            <div class="input-group">
                                                                                         
                                                <input maxlength="100"  autocomplete="off" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required placeholder="Email">
                                            </div> 
                                            
                                            @if ($errors->has('email'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                            @endif 
                                        </div>
                                        <div class="kt-login__actions">
                                            <button type="submit" id="kt_login_forgot_pass" class="btn btn-label-brand btn-bold">
                                                {{ __('Reset Password') }}
                                            </button>
                                            <button id="kt_login_forgot_cancel" class="btn btn-secondary">Cancel</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        
                        </div>
                    </div>
                    <div class="kt-login__account">
                        <span class="kt-login__account-msg">
                            Don't have an account yet ?
                        </span>&nbsp;&nbsp;
                        <a href="{{URL::asset('signup')}}" class="kt-login__account-link btn-link">Sign Up!</a>
                    </div>
                </div>
            </div>
            <div class="kt-grid__item kt-grid__item--fluid kt-grid__item--center kt-grid kt-grid--ver kt-login__content" style="/*background-image: url(assets/media/bg/bg-4.jpg);*/">
                <div class="kt-login__section">
                    <div class="kt-login__block">
                        <div class="banner-wrapper text-center">
                            @include('layout.include.partials.banner')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@section('mainscripts')
@parent

    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script>
        $(".toggle-password").click(function() {
            $(this).toggleClass("fa-eye-slash fa-eye");
            var input = $($(this).attr("toggle"));
            if (input.attr("type") == "password") {
                input.attr("type", "text");
            } else {
                input.attr("type", "password");
            }
        });


        // var vcc = function(g_recaptcha_response) {
        //     var $captcha = $('#recaptcha');

        //     $('.msg-error').text('');
        //     $captcha.removeClass("error");
        // };

        // var $captcha = $('#recaptcha'),
        //     response = grecaptcha.getResponse();

        // if (response.length === 0) {
        //     $('.msg-error').text("reCAPTCHA is mandatory");
        //     if (!$captcha.hasClass("error")) {
        //         $captcha.addClass("error");
        //     }
        // } else {
        //     $('.msg-error').text('');
        //     $captcha.removeClass("error");
        // }
    </script>
@show
@endsection


