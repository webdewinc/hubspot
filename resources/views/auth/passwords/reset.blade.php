@extends('layout.login')
@php
$totalTitle = 'Reset Password';
@endphp
@section('title', $totalTitle)
@section('content')

<!-- Authentication Links --> 
<div class="kt-grid kt-grid--ver kt-grid--root kt-page">
    <div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v6 kt-login--signin" id="kt_login">
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--desktop kt-grid--ver-desktop kt-grid--hor-tablet-and-mobile">
            <div class="kt-grid__item  kt-grid__item--order-tablet-and-mobile-2  kt-grid kt-grid--hor kt-login__aside">
                <div class="kt-login__wrapper">
                    <div class="kt-login__container">
                        <div class="kt-login__body">
                            <div class="kt-login__logo">
                                <!-- <a href="#">
                                    <img src="assets/media/company-logos/logo-2.png">
                                </a> -->
                        
                                @include('layout.include.partials.brand')
                            </div>
                            <div class="kt-login__signin">
                                <div class="kt-login__head">
                                    <h3 class="kt-login__title">{{ __('Reset Password') }}</h3>
                                </div>                               

                                <div class="kt-login__form">
                                    @if(session()->has('message'))
                                        @if(session()->get('form') == 'signin')
                                        <div class="alert alert-outline-{{ session()->get('alert') }} fade show" role="alert">
                                            <div class="alert-icon"><i class="flaticon-warning"></i></div>
                                            <div class="alert-text">{{ session()->get('message') }}</div>
                                            <div class="alert-close">
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                    <span aria-hidden="true"><i class="la la-close"></i></span>
                                                </button>
                                            </div>
                                        </div>
                                        @endif
                                    @endif
                                    

                                    <form method="POST" class="kt-form-reset" action="{{ route('password.update') }}">
                                    @csrf
                                        <input type="hidden" name="token" value="{{ $token }}">
                                        <div class="form-group"> 
                                            <div class="input-group">
                                                <div class="input-group-prepend"><span class="input-group-text"><i class="la la-envelope"></i></span></div>
                                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus maxlength="100">
                                            </div> 
                                                @error('email')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                        </div>
                                        <div class="form-group">

                                            <div class="input-group">
                                                <div class="input-group-prepend"><span class="input-group-text"><i class="la la-lock"></i></span></div>
                                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" placeholder="Enter Password"  minlength="8" maxlength="15">
                                            </div>

                                            @error('password')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="form-group">

                                            <div class="input-group">
                                                <div class="input-group-prepend"><span class="input-group-text"><i class="la la-lock"></i></span></div>                                                
                                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password" placeholder="Enter Confirm password" minlength="8" maxlength="15">
                                            </div>
                                        </div>

                                        <div class="kt-login__actions">
                                            <button type="submit" id="kt_logins_reset_custom" class="btn btn-label-brand btn-bold">
                                                    {{ __('Reset Password') }}
                                            </button>
                                        </div>
                                    </form>                                   
                                </div>
                            </div>
                        </div>
                    </div>                   
                </div>
            </div> 

            <div class="kt-grid__item kt-grid__item--fluid kt-grid__item--center kt-grid kt-grid--ver kt-login__content" style="/*background-image: url(assets/media/bg/bg-4.jpg);*/">
                <div class="kt-login__section">
                    <div class="kt-login__block">
                        <div class="banner-wrapper text-center">
                            @include('layout.include.partials.banner')
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

@endsection