@extends('layout.app')
@php
$totalTitle = 'Home';
@endphp
@section('title', $totalTitle)
@section('title-description')

<meta name="description" content="Home">

<style type="text/css">

.smshub-contentBox-colInner {
    padding: 45px 25px 40px;
    border-radius: 10px;
}
.smshub-contentBox-icon {
    margin-bottom: 35px;
}
.smshub-contentBox-iconInner img {
    height: 115px;
}



</style>
    
@endsection
@section('content')
@section('head')
@parent    
@show
<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor bg-white pt-5" id="kt_content">

        <!-- begin:: Content -->
        <div class="kt-container  kt-grid__item kt-grid__item--fluid home-container">
           
            <div class="row">
                <!-- body content start -->
                <!-- <div class="col-12">
                    <div class="kt_blog-post pb-5">
                        
                        <div class="row justify-content-center">
                            <div class="col-xl-8 col-lg-8 col-md-12">
                                
                                @if (session('success'))
                                <div class="alert alert-success">
                                    {{ session('success') }}
                                </div>
                                @endif
                                @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif

                                <div class="row">
                                    <div class="col">
                                        <div class="card">
                                            <div class="card-header">
                                                
                                            </div>
                                            <div class="card-body">
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>                                  
                    </div>
                </div> -->


<div class="col-12">
    <section class="smshub-contentBox pt-5">
          <div class="container-fluid">
            <div class="row">
              <div class="col-12">
                <div class="smshub-contentBox-head text-center pb-5">
                  <h2>Our Apps</h2>
                  <p>From right within your HubSpot you can send  text messages on the go with All-In-One Sms App.<br>Sync your Whmcs Data with WHMCS app.<br>Shop as you like with E-commerce app.</p>

                </div>
              </div>
            </div>
            <div class="row smshub-contentBox-row">
              <div class="col-xl-4 col-lg-6">
                <div class="card card-custom smshub-contentBox-col kt-portlet kt-iconbox kt-icon-size kt-iconbox--primary kt-iconbox--animate-slow">
                  <div class="smshub-contentBox-colInner text-center">
                    <div class="smshub-contentBox-icon">
                      <div class="smshub-contentBox-iconInner">
                        <?php
                        $portal_id = \Session::get('portal_id');
                        $total_count = \DB::table('payments')->where('user_id',Auth::user()->id)->where('app_id',1)->where('portal_id',$portal_id)->count();
                        $payment_list = \DB::table('payments')->where('user_id',Auth::user()->id)->where('app_id',1)->where('portal_id',$portal_id)->get()->toArray();
                        $id = '';
                        $expiry_date = '';
                        if($total_count > 0){
                            $id = $payment_list[$total_count - 1]->id;
                            $expiry_date = $payment_list[$total_count - 1]->end_date;

                            $date = date_create($expiry_date);
                            date_sub($date,date_interval_create_from_date_string("15 days"));
                            $warning_date =  date_format($date,"Y-m-d");
                            $current_date = date("Y-m-d");
                        ?>   
                            @if($current_date > $warning_date && $current_date < $expiry_date)
                                 <div class="alert alert-warning">
                                  <strong>Warning! </strong> Your Subscription will get expired on {{$expiry_date}}
                                </div>
                            @endif
                            
                            @if($current_date > $expiry_date)
                                <div class="alert alert-danger">
                                  Your Subscription for All in One SMS app has been expired on {{$expiry_date}}.
                                  Please renew it now.
                                </div>
                            @endif
                        <?php
                        }
                        ?>
                        
                        <img src="/hubspot/public/assets/media/misc/HubSpot All in One SMS App.png" alt="icon1">
                      </div>
                    </div>
                    <div class="smshub-contentBox-content">
                      <h3>All in One SMS</h3>
                      <p>Send messages from any phone number in the US or Canada. Even if the person using the phone isn't! 
                       Pay only <b>$19</b> for one year to use the app. </p>
                      @if(Auth::check())
                        @if(empty($portal_id))
                        <?php
                            $total_count = \DB::table('payments')->where('user_id',Auth::user()->id)->where('app_id',1)->whereDate('end_date','>=',date('Y-m-d'))->count();
                            $payment_list = \DB::table('payments')->where('user_id',Auth::user()->id)->where('app_id',1)->whereDate('end_date','>=',date('Y-m-d'))->get()->toArray();
                            
                            ?>
                        @endif
                        @if($total_count < 1)
                            <!-- <a href="{{env('APP_URL').'ecommerce/auth2-callback'}}" class="btn btn-brand btn-bold kt-mr-10">Install App</a> -->
                            <form role="form" action="{{url('/stripe-payment')}}" method="post"  data-cc-on-file="false" data-stripe-publishable-key="{{env('STRIPE_KEY')}}" id="payment-form">
                            @csrf
                                <input type="hidden" name="email" value="{{Auth::user()->email}}">
                                <input type="hidden" name="type" value="smsdew">
                                <input type="hidden" name="app_id" value="1">
                                <noscript>You must <a href="http://www.enable-javascript.com" target="_blank">enable JavaScript</a> in your web browser in order to pay via Stripe.</noscript>
                                    @if(Auth::user()->email == 'randeep.s@webdew.com' || Auth::user()->email == 'guriqbal.d@webdew.com' || Auth::user()->email == 'dhillon.iqbal.116@gmail.com' || Auth::user()->email == 'paramjeet.s@webdew.com')
                                        
                                        <input 
                                        type="submit"
                                        class="btn btn-brand btn-bold kt-mr-10" 
                                        value="Pay $19"
                                        data-key="pk_test_51HIGy1Et32QR8DEin6O2X1MaUaQeRLUmdx49imFUDG2A5sdTZ94Sl7Mqy0cGikPOnxwpWRDjB9pU4oBeQ2VWuE4O002IlB5paY"
                                        data-amount="1900"
                                        data-currency="usd"
                                        data-name="stripe Payment"
                                        data-email="{{Auth::user()->email}}"
                                        id="stripe"
                                    />
                                    @else
                                    <input 
                                        type="submit"
                                        class="btn btn-brand btn-bold kt-mr-10" 
                                        value="Pay $19"
                                        data-key="{{env('STRIPE_KEY')}}"
                                        data-amount="1900"
                                        data-currency="usd"
                                        data-name="stripe Payment"
                                        data-email="{{Auth::user()->email}}"
                                        id="stripe"
                                    />
                                    @endif
                                    <script src="https://checkout.stripe.com/v2/checkout.js"></script>
                                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
                                    <script>
                                        $(document).ready(function() {
                                            $(document).on('click','#stripe' ,function(event) {
                                                event.preventDefault();


                                                var $button = $(this),
                                                    $form = $button.parents('form');


                                                var opts = $.extend({}, $button.data(), {
                                                    token: function(result) {
                                                    $form.append($('<input>').attr({ type: 'hidden', name: 'stripeToken', value: result.id })).submit();
                                                    }
                                                });


                                                StripeCheckout.open(opts);
                                            });
                                        });
                                    </script>
                              </form>
                          @else 
                            <?php
                            if($total_count > 0){
                                $id = $payment_list[$total_count - 1]->id;
                                $expiry_date = $payment_list[$total_count - 1]->end_date;

                                $date = date_create($expiry_date);
                                date_sub($date,date_interval_create_from_date_string("15 days"));
                                $warning_date =  date_format($date,"Y-m-d");
                                $current_date = date("Y-m-d");
                            ?>  
                                @if($current_date > $expiry_date)
                                     @if(Auth::user()->email == 'randeep.s@webdew.com' || Auth::user()->email == 'guriqbal.d@webdew.com')
                                          <form role="form" action="{{url('/stripe-payment')}}" method="post"  data-cc-on-file="false" data-stripe-publishable-key="pk_test_51HIGy1Et32QR8DEin6O2X1MaUaQeRLUmdx49imFUDG2A5sdTZ94Sl7Mqy0cGikPOnxwpWRDjB9pU4oBeQ2VWuE4O002IlB5paY" id="payment-form">
                                          @else 
                                          <form role="form" action="{{url('/stripe-payment')}}" method="post"  data-cc-on-file="false" data-stripe-publishable-key="{{env('STRIPE_KEY')}}" id="payment-form">
                                          @endif
                                            @csrf
                                            <input type="hidden" name="email" value="{{Auth::user()->email}}">
                                            <input type="hidden" name="type" value="smsdew">
                                            <input type="hidden" name="app_id" value="1">
                                            <noscript>You must <a href="http://www.enable-javascript.com" target="_blank">enable JavaScript</a> in your web browser in order to pay via Stripe.</noscript>
                                                @if(Auth::user()->email == 'randeep.s@webdew.com' || Auth::user()->email == 'guriqbal.d@webdew.com' || Auth::user()->email == 'dhillon.iqbal.116@gmail.com' ||  Auth::user()->email == 'paramjeet.s@webdew.com')
                                                    
                                                    <input 
                                                    type="submit"
                                                    class="btn btn-brand btn-bold kt-mr-10" 
                                                    value="Renew Now"
                                                    data-key="pk_test_51HIGy1Et32QR8DEin6O2X1MaUaQeRLUmdx49imFUDG2A5sdTZ94Sl7Mqy0cGikPOnxwpWRDjB9pU4oBeQ2VWuE4O002IlB5paY"
                                                    data-amount="1900"
                                                    data-currency="usd"
                                                    data-name="stripe Payment"
                                                    data-email="{{Auth::user()->email}}"
                                                    id="stripe"
                                                />
                                                @else
                                                    <input 
                                                    type="submit"
                                                    class="btn btn-brand btn-bold kt-mr-10" 
                                                    value="Renew Now"
                                                    data-key="{{env('STRIPE_KEY')}}"
                                                    data-amount="1900"
                                                    data-currency="usd"
                                                    data-name="stripe Payment"
                                                    data-email="{{Auth::user()->email}}"
                                                    id="stripe"
                                                />
                                                @endif
                                                
                                                <script src="https://checkout.stripe.com/v2/checkout.js"></script>
                                                <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
                                                <script>
                                                    $(document).ready(function() {
                                                        $(document).on('click','#stripe' ,function(event) {
                                                            event.preventDefault();


                                                            var $button = $(this),
                                                                $form = $button.parents('form');


                                                            var opts = $.extend({}, $button.data(), {
                                                                token: function(result) {
                                                                $form.append($('<input>').attr({ type: 'hidden', name: 'stripeToken', value: result.id })).submit();
                                                                }
                                                            });


                                                            StripeCheckout.open(opts);
                                                        });
                                                    });
                                                </script>
                                          </form> 
                                @else
                                    @if(empty($portal_id))
                                        <?php
                                        $total_count = \DB::table('application_records')->where('user_id',Auth::user()->id)->where('app_id',1)->count();
                                        if($total_count < 1){
                                            ?>
                                                <a class="btn btn-brand btn-bold kt-mr-10" href="{{url('smsdew/auth2-callback')}}">Install App</a>
                                            <?php    
                                        }
                                        ?>
                                    @else 
                                        <?php
                                        $total_count = \DB::table('application_records')->where('user_id',Auth::user()->id)->where('app_id',1)->where('portal_id',$portal_id)->count();
                                        if($total_count < 1){
                                            ?>
                                                <a class="btn btn-brand btn-bold kt-mr-10" href="{{url('smsdew/auth2-callback')}}">Install App</a>
                                            <?php    
                                        } else {
                                        ?>
                                        <button class="btn btn-brand btn-bold kt-mr-10">Installed</button>
                                        <?php } ?>
                                    @endif
                                    
                                @endif
                            <?php
                            }
                            ?>
                          @endif

                    @endif                        
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-xl-4 col-lg-6">
                <div class="card card-custom smshub-contentBox-col kt-portlet kt-iconbox kt-icon-size kt-iconbox--primary kt-iconbox--animate-slow">
                  <div class="smshub-contentBox-colInner text-center">
                    <div class="smshub-contentBox-icon">
                      <div class="smshub-contentBox-iconInner">
                        <?php
                        $portal_id = \Session::get('portal_id');
                        $total_count = \DB::table('whmcs_payments')->where('user_id',Auth::user()->id)->where('app_id',2)->where('portal_id',$portal_id)->count();
                        $payment_list = \DB::table('whmcs_payments')->where('user_id',Auth::user()->id)->where('app_id',2)->where('portal_id',$portal_id)->get()->toArray();
                        $id = '';
                        $expiry_date = '';
                        if($total_count > 0){
                            $id = $payment_list[$total_count - 1]->id;
                            $expiry_date = $payment_list[$total_count - 1]->end_date;

                            $date = date_create($expiry_date);
                            date_sub($date,date_interval_create_from_date_string("15 days"));
                            $warning_date =  date_format($date,"Y-m-d");
                            $current_date = date("Y-m-d");
                        ?>   
                            @if($current_date > $warning_date && $current_date < $expiry_date)
                                 <div class="alert alert-warning">
                                  <strong>Warning! </strong> Your Subscription will get expired on {{$expiry_date}}
                                </div>
                            @endif
                            
                            @if($current_date > $expiry_date)
                                <div class="alert alert-danger">
                                  Your Subscription for WHMCS app has been expired on {{$expiry_date}}.
                                  Please renew it now <a href="{{env('APP_URL').'apps/whmcs/automation-pricing'}}">Click here</a>
                                </div>
                            @endif
                        <?php
                        }
                        ?>
                        <img src="/hubspot/public/assets/media/misc/376_Whmcs_logo-512.png" alt="image" >
                      </div>
                    </div>
                    <div class="smshub-contentBox-content">
                      <h3>WHMCS</h3>
                        <p>Sync your Whmcs <b>Products,Contacts and Orders</b> to your Hubspot account with just one click!</p>
                        <p>Its just <b>$9</b> for one time for installation and one time single sync of <b>Products, Contacts and Orders</b> and for automation sync have different plans.</p>


                        @if(Auth::check())

                        @php
                        //check if the app is installed for the same portal by this user or not 
                        $portal_id = \Session::get('portal_id');
                        //$id_new = \DB::table('payments')->where('app_id',2)->where('portal_id',$portal_id)->value('id');
                        $exi = DB::table('application_records')->where('portal_id',$portal_id)->where('app_id', 2)->value('id');


                        @endphp

                        @if($exi < 1)

                        @if(empty($portal_id))
                        <?php
                            $total_count = \DB::table('payments')->where('user_id',Auth::user()->id)->where('app_id',2)->whereDate('end_date','>=',date('Y-m-d'))->count();
                            ?>
                        @endif
                        @if($total_count > 0)
                            <a href="{{env('APP_URL').'whmcs/auth2-callback'}}" class="btn btn-brand btn-bold kt-mr-10">Install App</a>
                        @else 
                        <form role="form" action="{{url('/stripe-payment')}}" method="post"  data-cc-on-file="false" data-stripe-publishable-key="{{env('STRIPE_KEY')}}" id="payment-form">
                            @csrf
                            <input type="hidden" name="email" value="{{Auth::user()->email}}">
                            <input type="hidden" name="type" value="whmcs">
                            <input type="hidden" name="app_id" value="2">
                            <noscript>You must <a href="http://www.enable-javascript.com" target="_blank">enable JavaScript</a> in your web browser in order to pay via Stripe.</noscript>
                                 @if(Auth::user()->email == 'randeep.s@webdew.com' || Auth::user()->email == 'guriqbal.d@webdew.com' || Auth::user()->email == 'dhillon.iqbal.116@gmail.com' ||  Auth::user()->email == 'paramjeet.s@webdew.com')
                                        
                                        <input 
                                        type="submit"
                                        class="btn btn-brand btn-bold kt-mr-10" 
                                        value="Pay $9"
                                        data-key="pk_test_51HIGy1Et32QR8DEin6O2X1MaUaQeRLUmdx49imFUDG2A5sdTZ94Sl7Mqy0cGikPOnxwpWRDjB9pU4oBeQ2VWuE4O002IlB5paY"
                                        data-amount="900"
                                        data-currency="usd"
                                        data-name="stripe Payment"
                                        data-email="{{Auth::user()->email}}"
                                        id="stripe"
                                    />
                                @else
                                <input 
                                    type="submit"
                                    class="btn btn-brand btn-bold kt-mr-10" 
                                    value="Pay $9"
                                    data-key="{{env('STRIPE_KEY')}}"
                                    data-amount="900"
                                    data-currency="usd"
                                    data-name="stripe Payment"
                                    data-email="{{Auth::user()->email}}"
                                    id="stripe"
                                />
                                @endif
                                <script src="https://checkout.stripe.com/v2/checkout.js"></script>
                                <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
                                <script>
                                    $(document).ready(function() {
                                        $(document).on('click','#stripe' ,function(event) {
                                            event.preventDefault();


                                            var $button = $(this),
                                                $form = $button.parents('form');


                                            var opts = $.extend({}, $button.data(), {
                                                token: function(result) {
                                                $form.append($('<input>').attr({ type: 'hidden', name: 'stripeToken', value: result.id })).submit();
                                                }
                                            });


                                            StripeCheckout.open(opts);
                                        });
                                    });
                                </script>
                          </form>
                        <!--  data-key="pk_test_51HIGy1Et32QR8DEin6O2X1MaUaQeRLUmdx49imFUDG2A5sdTZ94Sl7Mqy0cGikPOnxwpWRDjB9pU4oBeQ2VWuE4O002IlB5paY" -->
                        @endif
                       @else
                            <!-- <button class="btn btn-brand btn-bold kt-mr-10">Installed</button> -->
                          <a href="{{env('APP_URL').'apps/whmcs/automation-pricing'}}" class="btn btn-brand btn-bold kt-mr-10">Automation</a>
                        @endif
                      @endif
                      
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-xl-4 col-lg-6">
                <div class="card card-custom smshub-contentBox-col kt-portlet kt-iconbox kt-icon-size kt-iconbox--primary kt-iconbox--animate-slow">
                  <div class="smshub-contentBox-colInner text-center">
                    <div class="smshub-contentBox-icon">
                      <div class="smshub-contentBox-iconInner">

                        <?php
                        $portal_id = \Session::get('portal_id');
                        $total_count = \DB::table('ecommerce_payments')->where('user_id',Auth::user()->id)->where('app_id',3)->where('portal_id',$portal_id)->count();
                        $payment_list = \DB::table('ecommerce_payments')->where('user_id',Auth::user()->id)->where('app_id',3)->where('portal_id',$portal_id)->get()->toArray();
                        $id = '';
                        $expiry_date = '';
                        if($total_count > 0){
                            $id = $payment_list[$total_count - 1]->id;
                            $expiry_date = $payment_list[$total_count - 1]->end_date;

                            $date = date_create($expiry_date);
                            date_sub($date,date_interval_create_from_date_string("15 days"));
                            $warning_date =  date_format($date,"Y-m-d");
                            $current_date = date("Y-m-d");
                        ?>   
                            @if($current_date > $warning_date && $current_date < $expiry_date)
                                 <div class="alert alert-warning">
                                  <strong>Warning! </strong> Your Subscription will get expired on {{$expiry_date}}
                                </div>
                            @endif
                            
                            @if($current_date > $expiry_date)
                                <div class="alert alert-danger">
                                  Your Subscription for Ecommerce app has been expired on {{$expiry_date}}.
                                  Please renew it now.
                                </div>
                            @endif
                        <?php
                        }
                        ?>
                        <img src="/hubspot/public/assets/media/misc/e-commerce.svg" alt="icon1">
                      </div>
                    </div>
                    <div class="smshub-contentBox-content">
                      <h3>E-Commerce</h3>
                      <p>Create your own Ecommerce Site with this amazing app in few simple steps.</p>
                      <p>Its just <b>$29</b> for a month.</p>
                      @if(empty($portal_id))
                        <?php
                            $total_count = \DB::table('ecommerce_payments')->where('user_id',Auth::user()->id)->where('app_id',3)->whereDate('end_date','>=',date('Y-m-d'))->count();
                            $payment_list = \DB::table('ecommerce_payments')->where('user_id',Auth::user()->id)->where('app_id',3)->whereDate('end_date','>=',date('Y-m-d'))->get()->toArray();
                        ?>
                      @endif

                      @if($total_count < 1)
                        <!-- <a href="{{env('APP_URL').'ecommerce/auth2-callback'}}" class="btn btn-brand btn-bold kt-mr-10">Install App</a> -->
                        <form role="form" action="{{url('/ecommerce-stripe-payment')}}" method="post"  data-cc-on-file="false" data-stripe-publishable-key="{{env('STRIPE_KEY')}}" id="payment-form">
                            <label for="test1" hidden> Basic : $29 </label>
                            <span>
                                @csrf
                                <input type="hidden" name="email" value="{{Auth::user()->email}}">
                                <input type="hidden" name="type" value="ecommerce">
                                <input type="hidden" name="app_id" value="3">
                                <noscript>You must <a href="http://www.enable-javascript.com" target="_blank">enable JavaScript</a> in your web browser in order to pay via Stripe.</noscript>

                                @if(Auth::user()->email == 'randeep.s@webdew.com' || Auth::user()->email == 'guriqbal.d@webdew.com' ||  Auth::user()->email == 'paramjeet.s@webdew.com' || Auth::user()->email == 'dhillon.iqbal.116@gmail.com')
                                    <input 
                                        type="submit"
                                        class="btn btn-brand btn-bold kt-mr-10 pay" 
                                        value="Pay $29"
                                        data-key="pk_test_51HIGy1Et32QR8DEin6O2X1MaUaQeRLUmdx49imFUDG2A5sdTZ94Sl7Mqy0cGikPOnxwpWRDjB9pU4oBeQ2VWuE4O002IlB5paY"
                                        data-amount="2900"
                                        data-payment="daily"
                                        data-currency="usd"
                                        data-name="stripe Payment"
                                        data-email="{{Auth::user()->email}}"
                                        id="daily"
                                    />
                                @else
                                    <input 
                                        type="submit"
                                        class="btn btn-brand btn-bold kt-mr-10 pay" 
                                        value="Pay $29"
                                        data-key="{{env('STRIPE_KEY')}}"
                                        data-amount="2900"
                                        data-payment="daily"
                                        data-currency="usd"
                                        data-name="stripe Payment"
                                        data-email="{{Auth::user()->email}}"
                                        id="daily"
                                    />
                                @endif
                                    
                                    <script src="https://checkout.stripe.com/v2/checkout.js"></script>
                                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
                                    <script>
                                        $(document).ready(function() {
                                            $(document).on('click','#daily' ,function(event) {
                                                event.preventDefault();
                                                var $button = $(this),
                                                    $form = $button.parents('form');

                                                var opts = $.extend({}, $button.data(), {
                                                    token: function(result) {
                                                    $form.append($('<input>').attr({ type: 'hidden', name: 'stripeToken', value: result.id })).submit();
                                                    }
                                                });
                                                StripeCheckout.open(opts);
                                            });
                                        });
                                    </script>
                            </span>
                        </form> 
                      @else 
                        <?php
                        if($total_count > 0){
                            $id = $payment_list[$total_count - 1]->id;
                            $expiry_date = $payment_list[$total_count - 1]->end_date;

                            $date = date_create($expiry_date);
                            date_sub($date,date_interval_create_from_date_string("15 days"));
                            $warning_date =  date_format($date,"Y-m-d");
                            $current_date = date("Y-m-d");
                        ?>  
                            @if($current_date > $expiry_date)
                                 <form role="form" action="{{url('/ecommerce-stripe-payment')}}" method="post"  data-cc-on-file="false" data-stripe-publishable-key="{{env('STRIPE_KEY')}}" id="payment-form">
                                    <input type="hidden" name="email" value="{{Auth::user()->email}}">
                                    <input type="hidden" name="type" value="ecommerce">
                                    <input type="hidden" name="app_id" value="3">
                                    <label for="test1" hidden> Basic : $29 </label>
                                    <span>
                                        @csrf
                                        <input type="hidden" name="email" value="{{Auth::user()->email}}">
                                        <noscript>You must <a href="http://www.enable-javascript.com" target="_blank">enable JavaScript</a> in your web browser in order to pay via Stripe.</noscript>

                                        @if(Auth::user()->email == 'randeep.s@webdew.com' || Auth::user()->email == 'guriqbal.d@webdew.com' || Auth::user()->email == 'paramjeet.s@webdew.com' || Auth::user()->email == 'dhillon.iqbal.116@gmail.com')
                                            <input 
                                                type="submit"
                                                class="btn btn-brand btn-bold kt-mr-10 pay" 
                                                value="Pay $29"
                                                data-key="pk_test_51HIGy1Et32QR8DEin6O2X1MaUaQeRLUmdx49imFUDG2A5sdTZ94Sl7Mqy0cGikPOnxwpWRDjB9pU4oBeQ2VWuE4O002IlB5paY"
                                                data-amount="2900"
                                                data-payment="daily"
                                                data-currency="usd"
                                                data-name="stripe Payment"
                                                data-email="{{Auth::user()->email}}"
                                                id="daily"
                                            />
                                        @else
                                            <input 
                                                type="submit"
                                                class="btn btn-brand btn-bold kt-mr-10 pay" 
                                                value="Pay $29"
                                                data-key="{{env('STRIPE_KEY')}}"
                                                data-amount="2900"
                                                data-payment="daily"
                                                data-currency="usd"
                                                data-name="stripe Payment"
                                                data-email="{{Auth::user()->email}}"
                                                id="daily"
                                            />
                                        @endif
                                            
                                            <script src="https://checkout.stripe.com/v2/checkout.js"></script>
                                            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
                                            <script>
                                                $(document).ready(function() {
                                                    $(document).on('click','#daily' ,function(event) {
                                                        event.preventDefault();
                                                        var $button = $(this),
                                                            $form = $button.parents('form');

                                                        var opts = $.extend({}, $button.data(), {
                                                            token: function(result) {
                                                            $form.append($('<input>').attr({ type: 'hidden', name: 'stripeToken', value: result.id })).submit();
                                                            }
                                                        });
                                                        StripeCheckout.open(opts);
                                                    });
                                                });
                                            </script>
                                    </span>
                                </form> 
                            @else
                                @if(empty($portal_id))
                                    <?php
                                    $total_count = \DB::table('application_records')->where('user_id',Auth::user()->id)->where('app_id',3)->count();
                                    if($total_count < 1){
                                        ?>
                                            <a class="btn btn-brand btn-bold kt-mr-10" href="{{url('ecommerce/auth2-callback')}}">Install App</a>
                                        <?php    
                                    }
                                    ?>
                                @else 
                                     <?php
                                        $total_count = \DB::table('application_records')->where('user_id',Auth::user()->id)->where('app_id',3)->where('portal_id',$portal_id)->count();
                                        if($total_count < 1){
                                            ?>
                                                <a class="btn btn-brand btn-bold kt-mr-10" href="{{url('ecommerce/auth2-callback')}}">Install App</a>
                                            <?php    
                                        } else {
                                        ?>
                                    <a href="{{env('APP_URL').'apps/ecommerce/settings'}}" class="btn btn-brand btn-bold kt-mr-10">Installed</a>
                                    <?php } ?>
                                @endif
                            @endif
                        <?php
                        }
                        ?>
                      @endif
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
    </div>






                <!-- body content end -->
            </div>
        </div>
    </div>                  
</div>

@section('scripts')
@parent
@show
@endsection