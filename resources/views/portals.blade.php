@extends('layout.app')
<head>
    <link rel="stylesheet"
        href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link rel="stylesheet"
        href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>
<div class="main-content-wrap">
<br>
  <h3 style='text-align:center' class="text-primary">Portals</h3>
  <br>


  <form method="GET" action="{{url('/')}}">
  <div class="table-wrap">
<table class="table table-bordered portaltable" id="table">
  <thead>
    <tr>
      <th scope="col">All Portals</th>
    
    
      <th scope="col">Application</th>
    </tr>
  </thead>
  <tbody>
  
    <?php
    $i=1;
          foreach($sms_portals as $portals){

            echo "<tr>";
            echo "<td><input type='radio' class='form-check-input' id='radio'".$i." name='portal_id' value='".$portals."' required >".$portals."</td>" ;
          
            echo "<td>All In One SMS</td>" ;
            echo "</tr>";
            $i++;
          }

      ?>
        
      <?php
          foreach($whmcs_portals as $portals){
            echo "<tr>";
            echo "<td><input type='radio' class='form-check-input' id='radio'".$i." name='portal_id' value='".$portals."' required >".$portals."</td>" ;
           
            echo "<td>WHMCS</td>" ;
            echo "</tr>";
            $i++;
          }
      ?>

      <?php
          foreach($ecommerce_portals as $portals){
            echo "<tr>";
            echo "<td><input type='radio' class='form-check-input' id='radio'".$i." name='portal_id' value='".$portals."' required >".$portals."</td>" ;
           
            echo "<td>Ecommerce</td>" ;
            echo "</tr>";
            $i++;
          }
      ?>
  </tbody>


</table>
<input type="submit" id="submit" value="next" class="btn btn-primary">
</form>
</div>
</div>

<script>
$(document).ready(function(){
 $("#submit").click(function(event) {
 event.preventDefault(); 
 var selectedportal = $("input[name='portal_id']:checked").val();   
     

$.get("{{url('/portal_session')}}", {portal_id:selectedportal}, function (data, textStatus, jqXHR) {

  
    window.location.replace("{{url('/')}}");
});


     });
  });



 
</script>



<style type="text/css">
  
.main-content-wrap {
    width: 1380px;
    margin: 120px auto 80px;
    padding:0 30px;
}
div#table_length {
    text-align: left;
    display: inline-block;
}
div.dataTables_wrapper div.dataTables_length select {
    width: 75px;
    display: inline-block;
    margin: 0 15px;
    padding: 0 8px;
    height:34px;
}
div#table_filter input {
    background: #fff;
    font-size: 15px;
    outline: none;
    height: 36px;
    border-color: #e5f5ff;
}
div#table_wrapper .row {
    width: 100%;
}
div#table_filter label {
    display: block;
    text-align: right;
    vertical-align: middle;
}
ul.pagination li {
    margin-left: 15px;
}
ul.pagination {
    justify-content: flex-end;
}
th{
    font-size: 15px;
    outline:none !important;
}
td {
    font-size: 15px;
}
ul.pagination li a {
    cursor: pointer !important;
    border-color: #e5f5ff !important;
}
.pagination>.active>a, .pagination>.active>a:focus, .pagination>.active>a:hover, .pagination>.active>span, .pagination>.active>span:focus, .pagination>.active>span:hover {
    background-color: #0091ae ;
    border-color: #0091ae ;
}
.table>caption+thead>tr:first-child>td, .table>caption+thead>tr:first-child>th, .table>colgroup+thead>tr:first-child>td, .table>colgroup+thead>tr:first-child>th, .table>thead:first-child>tr:first-child>td, .table>thead:first-child>tr:first-child>th {
    border-top: 0;
    border-bottom: 0;
}
.table-bordered>tbody>tr>td, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>td, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>thead>tr>th {
    border: 1px solid #d7d7d7;
}
.dataTables_wrapper .dataTable {
    margin: 20px 0 !important;
}
.portaltable input.form-check-input {
    position: relative;
    left: 0;
}
div#partials-profile {
    display: none !important;
}
.signout_portal{
  display:inherit !important;
}

</style>
</body>
</html>


