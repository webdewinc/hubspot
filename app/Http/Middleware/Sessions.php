<?php
namespace App\Http\Middleware;

use Session;
use Closure;
use DB;

class Sessions
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {   
        $portal_id = Session::get('portal_id');

        $count = DB::table('application_records')->where('user_id',\Auth::user()->id)->count();

        if( empty($portal_id) && $count > 0 ){
            return redirect()->to('portals');
        } 
        return $next($request);
    }
}