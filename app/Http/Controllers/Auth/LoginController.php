<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Socialite;
use Auth;
use App\User;
use Session;
use DB;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/signin';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
         //$this->middleware('guest')->except('logout');
    }




    // public function login(Request $request)
    // {
    //     if(Auth::attempt(['email' => $request->user_email, 'password' => $request->password_login, 'user_type' => 'users'],($request->remember == 'on') ? true : false)) {

    //         if(Auth::user()->user_email_verified == 1) {
    //             return redirect('/');
    //             exit;
                
    //         } else {
    //             Auth::logout();
    //             return redirect()->to('/signin')->with(['message'=> 'Please activate your account first.', 'alert' => 'danger','form' => 'signin']);
    //         }
            

    //     } else {
           
    //         $user = User::where('email', $request->user_email)
    //               ->where('password',md5($request->password_login))
    //               ->where('user_type','users')
    //               ->first();
    //         $user_exist = json_decode(json_encode($user), true);
    //         if($user_exist){
    //             Auth::login($user);
    //             if($user_exist['user_type'] == 'users'){
                   
    //                 if($user_exist['user_email_verified'] == 1) {
    //                     return redirect('/');
    //                 } else {
                        
    //                     Auth::logout();
    //                     return redirect()->to('/signin')->with(['message'=> 'Please activate your account first.', 'alert' => 'danger','form' => 'signin']);    
    //                 }
    //             } else {
    //                 return redirect()->to('/signin')->with(['message'=> 'Something went wrong in user_type and switched user.', 'alert' => 'danger','form' => 'signin']);
    //             }
    //         } else {
    //             return redirect()->to('/signin')->with(['message'=> 'Please enter a valid email address and password.', 'alert' => 'danger','form' => 'signin']);

    //         }            
    //     }
    // }
    
    public function portals()
    {
      

      $sms_portals = DB::table('application_records')
                 ->select(['portal_id'])
                 ->where('user_id',Auth::user()->id)
                 ->where('app_id', 1)
                 ->groupBy('portal_id')
                 ->pluck('portal_id');
      $whmcs_portals = DB::table('application_records')
                 ->select(['portal_id'])
                 ->where('user_id',Auth::user()->id)
                 ->where('app_id', 2)
                 ->groupBy('portal_id')
                 ->pluck('portal_id');
      $ecommerce_portals = DB::table('application_records')
                 ->select(['portal_id'])
                 ->where('user_id',Auth::user()->id)
                 ->where('app_id', 3)
                 ->groupBy('portal_id')
                 ->pluck('portal_id');
      return view('portals',compact('sms_portals','whmcs_portals','ecommerce_portals'));
      // $email = Session::get('email');
      
      // // print_r($email);
      // $u_id = DB::table('users')->where('email',$email)->value('id');
      // $count = DB::select('select count(*) as count from payments where user_id='.$u_id.' and (app_id=1 or app_id=2) ' );
      // $count = $count[0]->count;
     
      //   if($count==1){
      //     $portal_id = DB::select('select portal_id from payments where  user_id='.$u_id.' and (app_id=1 or app_id=2) ');
      //     $portal_id = $portal_id[0]->portal_id;
         
      //     Session::put('portal_id',$portal_id);
      //     return redirect('/');
      //   }
      
      // // print_r($u_id);
      // $sms_portal =  DB::table('payments')->distinct('portal_id')->where('user_id',$u_id)->where('app_id',1)->get('portal_id')->toArray();
      // // print_r($sms_portal);
      // $whmcs_portal = DB::table('payments')->distinct('portal_id')->where('user_id',$u_id)->where('app_id',2)->get('portal_id')->toArray();
      // // print_r($whmcs_portal);
      // $sms_portals=[];
      // $i=0;
      
      // foreach($sms_portal as $portals){
      //   $sms_portals[$i]=$portals->portal_id;
      //   $i++;
      //   // echo "<br>";
      // }
      // $whmcs_portals=[];
      // $j=0;
      //  foreach($whmcs_portal as $portals2){
      //   $whmcs_portals[$j]=$portals2->portal_id;
        
      //   $j++;
      //   // echo "<br>";
      // }
      // if(!empty($sms_portals) || !empty($whmcs_portals)){

      //     return view('portals',compact('sms_portals','whmcs_portals'));
      // }
      // else{
      //   return redirect('/');
      // }

    }

  public function settings(){
        return view('settings-page');
      }

       public function login(Request $request)
    {   

            if(isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response']))
        { 
            $secret = '6Lf0ycYaAAAAAJVIGpVpfFVpi1Rpc75SLeF8RkA6';
            $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
            $responseData = json_decode($verifyResponse);
            if($responseData->success)
            {
               
                try {

                        if(Auth::attempt(['email' => $request->user_email, 'password' => $request->password_login, 'user_type' => 'users'],($request->remember == 'on') ? true : false)) {
                            
                            if(Auth::user()->user_email_verified == 1) {
                              Session::put('email',$request->user_email);
                                $count = DB::table('application_records')->where('user_id',\Auth::user()->id)->count();
                                if( $count > 0 ){
                                    return redirect()->to('portals');
                                } else {
                                  return redirect('/');
                                }
                                
                            } else {
                                Auth::logout();
                                return redirect()->to('/signin')->with(['message'=> 'Please activate your account first.', 'alert' => 'danger','form' => 'signin']);
                            }
                            

                        } else {
                                    $user = User::where('email', $request->user_email)
                                          ->where('password',md5($request->password_login))
                                          ->where('user_type','users')
                                          ->first();
                                    $user_exist = json_decode(json_encode($user), true);
                                    if($user_exist){
                                        Auth::login($user);
                                        if($user_exist['user_type'] == 'users'){
                                           
                                            if($user_exist['user_email_verified'] == 1) {
                                                Session::put('email',$request->user_email);
                                                 $count = DB::table('application_records')->where('user_id',\Auth::user()->id)->count();
                                                  if( $count > 0 ){
                                                      return redirect()->to('portals');
                                                  } else {
                                                    return redirect('/');
                                                  }
                                            } else {
                                                
                                                Auth::logout();
                                                return redirect()->to('/signin')->with(['message'=> 'Please activate your account first.', 'alert' => 'danger','form' => 'signin']);    
                                            }
                                        } else {
                                            return redirect()->to('/signin')->with(['message'=> 'Something went wrong in user_type and switched user.', 'alert' => 'danger','form' => 'signin']);
                                        }
                                    } else {
                                        return redirect()->to('/signin')->with(['message'=> 'Please enter a valid email address and password.', 'alert' => 'danger','form' => 'signin']);

                                    }            
                                }

                } catch (\Exception $exception) {
                        //logger()->error($exception);
                        return redirect()->to('/signin')->with(['message'=> 'Unable to signin.', 'alert' => 'danger','form' => 'signup']);                
                    }
            }
            else {
           
                $errMsg = 'Robot verification failed, please try again.';
                return redirect()->to('/signin')->with(['message'=> $errMsg, 'alert' => 'danger','form' => 'signin']); 
            }
        }
          else {
          
                $errMsg = 'Robot verification failed, please try again.';
                return redirect()->to('/signin')->with(['message'=> $errMsg, 'alert' => 'danger','form' => 'signin']); 
            }



    }


    /**
     * @param Request $request
     * @throws ValidationException
     */
    protected function sendFailedLoginResponse(Request $request)
    {
        $request->session()->put('login_error', trans('auth.failed'));
        throw ValidationException::withMessages(
            [
                'error' => [trans('auth.failed')],
            ]
        );
    }

    /**
     * @param Request $request
     * @throws ValidationException
     */
    protected function logout(Request $request)
    {
        Auth::logout();
        Session::forget('portal_id');
        return redirect()->to('signin');
    }

    public function portal_session(Request $request){

        $portal_id=$_GET['portal_id'];
        Session::put('portal_id',$portal_id);
        return Session::get('portal_id');
    }
         /**
     * Redirect the user to the Linkedin authentication page.
     *
     * @return Response
     */
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->scopes(['r_liteprofile', 'r_emailaddress'])->redirect();
    }
   
    /**
     * Obtain the user information from Linkedin.
     *
     * @return Response
     */
    public function handleProviderCallback($provider)
    {
        $user = Socialite::driver($provider)->stateless()->user();
        $authUser = $this->findOrCreateUser($user, $provider);

         // Auth::login($authUser, true);
        $count = DB::table('application_records')->where('user_id',\Auth::user()->id)->count();
        if( $count > 0 ){
            return redirect()->to('portals');
        } else {
          return redirect('/');
        }
        
        
        //  $authUser = User::where('email',$user->email)->first();
        //  // Auth::login($user_result);
        //      Auth::login($authUser);
        //  $webdewUrl = 'https://apps.webdew.com/hubspot/';
        //  header('Location: ' . $webdewUrl);
        //  // // redirect($webdewUrl);
        //                 exit;
    }
   
    /**
     * If a user has registered before using social auth, return the user
     * else, create a new user object.
     * @param  $user Socialite user object
     * @param $provider Social auth provider
     * @return  User
     */
    public function findOrCreateUser($user, $provider)
    {
        $authUser = User::where('provider_id', $user->id)->first();
        if ($authUser) {
        	   $authUser = User::where('email',$user->email)->first();
               Session::put('email',$user->email);
             Auth::login($authUser);
          // return redirect('/');
          //   exit;
            return $authUser;
        }
        else{ User::create([
            'user_fname'     => $user->name,
            'email'    => $user->email,
            'provider' => $provider,
            'provider_id' => $user->id
        ]);

  			$authUser = User::where('email',$user->email)->first();
        Session::put('email',$user->email);
             // Auth::login($authUser);
             // return redirect('/');
                // exit; 
                 return $authUser;         

        	}
    }

    //linkedin ends


    //google auth starts

    /**
  * Redirect the user to the Google authentication page.
  *
  * @return \Illuminate\Http\Response
  */
    public function googleRedirectToProvider()
    {
        return Socialite::driver('google')->redirect();
    }

     /**
     * Obtain the user information from Google.
     *
     * @return \Illuminate\Http\Response
     */
    public function googleHandleProviderCallback()
    {

        try {
            // $user = Socialite::driver('google')->user();
          $user = Socialite::driver('google')->stateless()->user();
          // print_r($user->id)
          

        } catch (\Exception $e) {

            return redirect('/login');
        }


         $authUser = User::where('email', $user->email)->first();
        

        if ($authUser) {
            //print_r(Auth::check($authUser));
             //print_r(Auth::user($authUser));
              Auth::login($authUser);
               Session::put('email',$user->email);
                                  

                                  $count = DB::table('application_records')->where('user_id',\Auth::user()->id)->count();
        if( $count > 0 ){
            return redirect()->to('portals');
        } else {
          return redirect('/');
        }

              // return redirect('/auth2-callback');
              // $webdewUrl = 'https://apps.webdew.com/hubspot/';
              // header('Location: ' . $webdewUrl);
               //redirect($webdewUrl);

                        exit;
        // return redirect($this->redirectTo);
        }
       else{
        User::create([
            'user_fname'     => $user->name,
            'email'          => $user->email,
            'google_id'      => $user->id
        ]);
        $authUser = User::where('email', $user->email)->first();
        
         Auth::login($authUser);
          Session::put('email',$user->email);

        $count = DB::table('application_records')->where('user_id',\Auth::user()->id)->count();
        if( $count > 0 ){
            return redirect()->to('portals');
        } else {
          return redirect('/');
        }
        
         // return redirect('/auth2-callback');
         // $webdewUrl = 'https://apps.webdew.com/hubspot/';
         // header('Location: ' . $webdewUrl);
                        exit;
            }

        // return redirect()->to('/home');
    }
}
