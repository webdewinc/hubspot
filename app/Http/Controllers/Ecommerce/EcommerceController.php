<?php
namespace App\Http\Controllers\Ecommerce;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Support\Facades\Validator;
use Auth;
use Crypt;
use Redirect;
use Hash;
use DB;
use App\User;
use Session;
use Twilio\Rest\Client;
use App\Credential;


class EcommerceController extends Controller
{

    public function __construct() {
        //$this->middleware('auth');
    }
    
    public function auth2_callback(Request $request) {



        if(env('APP_ENV') == 'live'){
            $provider = new \Flipbox\OAuth2\Client\Provider\HubSpot([
                'clientId'          => config('services.live_ecommerceCartButton.clientId'),
                'clientSecret'      => config('services.live_ecommerceCartButton.clientSecret'),
                'redirectUri'       => url('/ecommerce/auth2-callback')
            ]);
        } else {
            $provider = new \Flipbox\OAuth2\Client\Provider\HubSpot([
                'clientId'          => config('services.stage_ecommerceCartButton.clientId'),
                'clientSecret'      => config('services.stage_ecommerceCartButton.clientSecret'),
                'redirectUri'       => url('/ecommerce/auth2-callback')
            ]);
        }
        
        if ( !isset($_GET['code']) ) {
            
            $options = [
                'scope' => ['contacts','timeline','content','oauth'] // array or string
            ];

            // If we don't have an authorization code then get one
            $authUrl = $provider->getAuthorizationUrl($options);
            header('Location: '.$authUrl);
            exit;
            // Check given state against previously stored one to mitigate CSRF attack

        } 
        // elseif (empty($_GET['state']) || ($_GET['state'] !== Auth::user()->oauth2state)) {

        //     exit('Invalid state');

        // }
        else {

            $result = [];
            // Try to get an access token (using the authorization code grant)
            $token = $provider->getAccessToken('authorization_code', [
                'code' => $_GET['code']
            ]);
            // Optional: Now you have a token you can look up a users profile data
            try {
                // We got an access token, let's now get the user's details
                $user = $provider->getResourceOwner($token);
                
                $result['portal_id'] = $user->getHubId();
                $result['email'] = $user->getEmail();
                $result['app_id'] = @$user->getAppId();
                $result['refresh_token'] = @$token->getRefreshToken();
                $result['expires_at'] = @$token->getExpires();
                $result['user_expires_at'] = @$user->getExpires();
                $result['code'] = $_GET['code'];
                $result['expires_at'] = time() + $result['expires_at'] * 0.95;
                $result['auth_token'] = $token->getToken();
                $token = $token->getToken();

				$this->templates_generation($token);	
                

                $u = User::whereEmail($result['email'])->first();
                if(empty($u->id)) {
                    $validatedData['user_type']       = 'users';
                    $validatedData['activation_code'] = str_random(30).time();
                    $validatedData['user_email_verified'] = 1;
                    $validatedData['user_fname'] = 'New';
                    $validatedData['user_lname'] = 'User';
                    $validatedData['email'] = $result['email'];
                    $validatedData['user_status'] = 1;
                    $validatedData['vid'] = 0;
                    app(User::class)->create($validatedData);
                    $u = User::whereEmail($result['email'])->first();
                } 
                Auth::login($u);

                $mapped_app_id = DB::table('mapped_apps')->where('user_id',Auth::user()->id)->where('fk_app_id',3)->value('mapped_app_id');
                if(empty($mapped_app_id)){
                    DB::table('mapped_apps')->insert(['fk_app_id'=> 3,'user_id'=> Auth::user()->id]);
                }

                $scopes = @serialize($user->getScopes());
                  // update
                    $users = User::find(Auth::user()->id);
                    $users->portal_id = $result['portal_id'];
                    $users->hub_email = $user->getEmail();
                    $users->scopes =  $scopes;
                    $users->app_id = @$user->getAppId();
                    $users->refresh_token =$result['refresh_token'];
                    $users->expires_at =$result['expires_at'];
                    $users->code = $_GET['code'];
                    $users->auth_token = $token;
                    $users->save();
                  // update
                
                //$user_id = $u->id;
                $user_id = Auth::user()->id;
                $portal_id = $result['portal_id'];
                $refresh_token = $result['refresh_token'];
                $expiry_time = $result['expires_at'];

                /*===============
                    |implementing payment check
                    |================
                    */
                    $email = $user->getEmail();


                    // $paymentCheck = DB::table('payments')->where('user_id',Auth::user()->id)->where('app_id',3)->value('id');

                    
                    // if ($paymentCheck > 0) { //payment is done

                        /*===============
                        |Select query to check if application was already intalled in the same poratl or not
                        |================
                        */

                        $record_check = DB::table('application_records')->where('app_id',3)->where('portal_id',$portal_id)->value('id');

                        if ($record_check > 0)
                        {
                            DB::table('application_records')->where('app_id',3)->where('portal_id',$portal_id)->update([
                                        'refresh_token' => $refresh_token,
                                        'expiry_time' => $expiry_time,
                                        'email' => $user->getEmail()
                                    ]);
                        } else {

                            /*===============
                            |insert code in new table for storing refresh token for different portals
                            |================
                            */

                            $insert = DB::table('application_records')->insertGetId([
                                        'user_id' => $user_id,
                                        'portal_id' => $portal_id,
                                        'client_id' => '',
                                        'secret_id' => '',
                                        'refresh_token' => $refresh_token,
                                        'expiry_time' => $expiry_time,
                                        'app_id' => 3,
                                        'email' => $user->getEmail()
                                    ]);

                        }

                        $user_id = DB::table('users')->where('email',session::get('email'))->value('id');             

                        $get_payment_id = DB::table('ecommerce_payments')->where('user_id',$user_id)->where('app_id',3)->value('id');

                        // $get_payment_id = DB::table('ecommerce_payments')->where('user_id',$user_id)->where('app_id',3)->where('portal_id','=',NULL)->value('id');
                        if($get_payment_id > 0){
                            DB::table('ecommerce_payments')->where('user_id',$user_id)->where('id',$get_payment_id)->update(['portal_id' => $result['portal_id']]);
                        }
                        Session::put('portal_id', $result['portal_id']);
                        // redirect to hubspot
                        // $hubspotUrl = 'https://app.hubspot.com/';
                        // if (!empty($result['portal_id']) && isset($result['portal_id']))
                        // {
                            
                        //     $hubspotUrl = 'https://app.hubspot.com/integrations-settings/'.$result['portal_id'].'/installed';
                        //     //$hubspotUrl = 'https://app.hubspot.com/home?portalID=' . $result['portal_id'];
                        // }
                        // header('Location: ' . $hubspotUrl);
                        // exit;
                        // redirect to hubspot
                        //return redirect('/apps/whmcs/import');



					   if(Auth::check()){
                            return redirect('/');
                       } else {
                            $webdewUrl = 'https://apps.webdew.com/hubspot/';
                            header('Location: ' . $webdewUrl);
                            exit;
                       }
                        
                    //} 
                    //else { 

                    //     $paymentCheck = DB::table('payments')->insertGetId([
                    //                                             'app_id' => 3,
                    //                                             'user_id' => Auth::user()->id,
                    //                                             'stripe_id' => 'manual',
                    //                                             'amount' => 0,
                    //                                             'currency_code' => 'usd',
                    //                                             'status' => 'succeeded',
                    //                                             'app_type' => 'ecommerce',
                    //                                             'subscription_id' => 0
                    //                                         ]);

                    //     /*===============
                    //     |Select query to check if application was already intalled in the same poratl or not
                    //     |================
                    //     */

                    //     $record_check = DB::table('application_records')->where('portal_id',$portal_id)->where('app_id',3)->value('id');

                    //     if ($record_check > 0)
                    //     {
                    //         DB::table('application_records')->where('app_id',3)->where('portal_id',$portal_id)->update([
                    //                     'refresh_token' => $refresh_token,
                    //                     'expiry_time' => $expiry_time,
                    //                     'email' => $user->getEmail()
                    //                 ]);
                    //     } else {

                    //         ===============
                    //         |insert code in new table for storing refresh token for different portals
                    //         |================
                            
                    //         $insert = DB::table('application_records')->insertGetId([
                    //                     'user_id' => $user_id,
                    //                     'portal_id' => $portal_id,
                    //                     'client_id' => '',
                    //                     'secret_id' => '',
                    //                     'refresh_token' => $refresh_token,
                    //                     'expiry_time' => $expiry_time,
                    //                     'app_id' => 3,
                    //                     'email' => $user->getEmail()
                    //                 ]);

                    //     }

                    //     // redirect to hubspot
                    //     // $hubspotUrl = 'https://app.hubspot.com/';
                    //     // if (!empty($result['portal_id']) && isset($result['portal_id']))
                    //     // {
                            
                    //     //     $hubspotUrl = 'https://app.hubspot.com/integrations-settings/'.$result['portal_id'].'/installed';
                    //     //     //$hubspotUrl = 'https://app.hubspot.com/home?portalID=' . $result['portal_id'];
                    //     // }
                    //     // header('Location: ' . $hubspotUrl);
                    //     // exit;
                    //     // redirect to hubspot
                    //     //$webdewUrl = 'https://www.webdew.com/hubspot/apps/ecommerce';
                    //     $webdewUrl = 'https://www.webdew.com/hubspot/apps/whmcs';
                    //     header('Location: ' . $webdewUrl);
                    //     exit;

                    //     //exit;
                    // }

                // redirect to hubspot 
                // $hubspotUrl = 'https://app.hubspot.com/';
                // if(!empty($result['portal_id']) && isset($result['portal_id'])){
                //     $hubspotUrl = 'https://app.hubspot.com/home?portalID='.$result['portal_id'];
                // }
                // header('Location: '.$hubspotUrl);
                // exit;
                // redirect to hubspot 

                // $args['data'] = '{
                //       "properties": [
                //         {
                //           "property": "portal_id",
                //           "value": "'.$result['portal_id'].'"
                //         },
                //         {
                //           "property": "hub_email",
                //           "value": "'.$result['email'].'"
                //         },
                //         {
                //           "property": "refresh_token",
                //           "value": "'.$result['refresh_token'].'"
                //         },
                //         {
                //           "property": "token",
                //           "value": "'.$result['auth_token'].'"
                //         },
                //         {
                //           "property": "code",
                //           "value": "'.$result['code'].'"
                //         },
                //         {
                //           "property": "app_id",
                //           "value": "'.$result['app_id'].'"
                //         }
                //       ]
                //     }';
                // ;
                // $args['url'] = 'https://api.hubapi.com/contacts/v1/contact/email/'.$result['email'].'/profile?hapikey='.env('HUBSPOT_API_KEY');
                // $store = $this->curlAccess( 'POST', $args );                
                // Use these details to create a new profile

            } catch (Exception $e) {
                // Failed to get user details
                exit('Oh dear...');
            }
            // Use this to interact with an API on the users behalf
            return redirect('/');
        }
    }


public function curlShippingTemplate($hapi_key)
{

         $curl = curl_init();
 
         curl_setopt_array($curl, array(
          CURLOPT_URL => 'https://api.hubapi.com/content/api/v2/templates?hapikey='.$hapi_key,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS =>'{
            "category_id": 1,
            "folder": "example",
            "is_available_for_new_content": "True",
            "template_type": 4,        
            "path":"custom/checkoutApi",
            "source":"<!--\\n    templateType: page\\n    isAvailableForNewContent: true\\n-->\\n<!doctype html>\\n<html>\\n  <head>\\n    <meta charset=\\"utf-8\\">\\n    <title>{{ content.html_title }}</title>\\n    <meta name=\\"description\\" content=\\"{{ content.meta_description }}\\">\\n    {{ standard_header_includes }}\\n    \\n<!--     loader css -->\\n<style>\\n.lds-roller {\\n  display: inline-block;\\n  position: relative;\\n  width: 80px;\\n  height: 80px;\\n}\\n.lds-roller div {\\n  animation: lds-roller 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;\\n  transform-origin: 40px 40px;\\n}\\n.lds-roller div:after {\\n    content: \\" \\";\\n    display: block;\\n    position: absolute;\\n    width: 7px;\\n    height: 7px;\\n    border-radius: 50%;\\n    background:#00A9FA;\\n    margin: -4px 0 0 -4px;\\n}\\n.lds-roller div:nth-child(1) {\\n    animation-delay: -0.036s;\\n}\\n.lds-roller div:nth-child(1):after {\\n    top: 63px;\\n    left: 63px;\\n}\\n.lds-roller div:nth-child(2) {\\n    animation-delay: -0.072s;\\n}\\n.lds-roller div:nth-child(2):after {\\n    top: 68px;\\n    left: 56px;\\n}\\n.lds-roller div:nth-child(3) {\\n    animation-delay: -0.108s;\\n}\\n.lds-roller div:nth-child(3):after {\\n    top: 71px;\\n    left: 48px;\\n}\\n.lds-roller div:nth-child(4) {\\n  animation-delay: -0.144s;\\n}\\n.lds-roller div:nth-child(4):after {\\n  top: 72px;\\n  left: 40px;\\n}\\n.lds-roller div:nth-child(5) {\\n  animation-delay: -0.18s;\\n}\\n.lds-roller div:nth-child(5):after {\\n  top: 71px;\\n  left: 32px;\\n}\\n.lds-roller div:nth-child(6) {\\n  animation-delay: -0.216s;\\n}\\n.lds-roller div:nth-child(6):after {\\n  top: 68px;\\n  left: 24px;\\n}\\n.lds-roller div:nth-child(7) {\\n  animation-delay: -0.252s;\\n}\\n.lds-roller div:nth-child(7):after {\\n  top: 63px;\\n  left: 17px;\\n}\\n.lds-roller div:nth-child(8) {\\n  animation-delay: -0.288s;\\n}\\n.lds-roller div:nth-child(8):after {\\n  top: 56px;\\n  left: 12px;\\n}\\n@keyframes lds-roller {\\n  0% {\\n    transform: rotate(0deg);\\n  }\\n  100% {\\n    transform: rotate(360deg);\\n  }\\n}\\n.loader-wrapper {\\n  width: 100%;\\n  height: 100%;\\n  position: absolute;\\n  top: 0;\\n  left: 0;\\n  background-color: #ffffffba;\\n  display:flex;\\n  justify-content: center;\\n  align-items: center;\\n  z-index:99999999999;\\n}\\n  \\n    a.confirm_order.btn.hidden{\\n      display:none;\\n  }\\n  \\n  @import url(\'https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap\');\\n.module-wrap{\\nfont-family: \'Montserrat\', sans-serif;\\npadding-bottom:60px;\\n}\\nbody{\\nmargin:0;\\n}\\nh1.page-heading {\\n    margin: 0 0 30px;\\n    font-size: 30px;\\n    color: #333;\\n}\\n/* left side start here */\\n.tabber-wrap .tabber-tabs {\\n    border-bottom: 1px solid #e9e9e9;\\n    list-style: none;\\n    padding: 0;\\n    margin: 0;\\n}\\n.tabber-wrap .tabber-tabs > li {\\n    float: left;\\n}\\n.tabber-wrap .tabber-tabs > li > a {\\n    line-height: 20px;\\n    text-decoration: none;\\n    display: inline-block;\\n    color:#333;\\n    font-size:16px;\\n}\\n.tabber-wrap .tabber-tabs > li > span{\\n    display:inline-block;\\n    padding:0 15px;\\n}\\n.tabber-wrap .tabber-tabs > .active > a, \\n.tabber-wrap .tabber-tabs > .active > a:hover, \\n.tabber-wrap .tabber-tabs > .active > a:focus {\\n    cursor: default;\\n    color:#00A9FA;\\n}\\n.tabber-wrap .tabber-content .tab-pane.active:nth-child(1) {\\n    border-top-left-radius: 0;\\n}\\n.tabber-wrap .tabber-content .block h3 {\\n  display: none;\\n}\\n.tabber-wrap .tabber-content .tab-pane:nth-child(1) {\\n  border-top-left-radius: 0;\\n}\\n.tabber-wrap .tabber-content .tab-pane {\\n  background: #fff;\\n  border: 0; \\n  border-radius: 4px; \\n  display: block; \\n  height: 0; \\n  opacity: 0;\\n  overflow: hidden; \\n  transition: opacity 400ms;\\n}\\n.tabber-wrap .tabber-content .tab-pane.active {\\n  height: auto; \\n  opacity: 1;\\n  overflow: visible; \\n  padding: 8px;\\n}\\n\\n\\n@media screen and (max-width:991px){\\n  a.hs-rss-title span {\\n    font-size: 22px;\\n  }\\n}\\n@media screen and (max-width:767px){\\n  a.hs-rss-title span {\\n    font-size: 20px;\\n}\\n\\n}\\n  @media screen and (max-width:567px){\\n  a.hs-rss-title span {\\n    font-size: 17px;\\n}\\n    .hs-rss-item.hs-with-featured-image .hs-rss-item-text {\\n    line-height: 22px;\\n}\\n}\\n\\n\\n/* shipping form start here */\\n\\n\\nform.shipping-payment-form input {\\n    width: 100%;\\n    height: 40px;\\n    padding: 0 12px;\\n    outline: none;\\n    margin-bottom: 10px;\\n    box-sizing: border-box;\\n    border-radius:6px;\\n    border:none;\\n    border:2px solid #7a7a7a33;\\n    color:#7a7a7a;\\n}\\nform.shipping-payment-form input:focus{\\n   border:2px solid #00A9FA;\\n}\\nlabel.error {\\n    color: red;\\n    font-size: 14px;\\n    margin-bottom: 5px;\\n    display: block;\\n}\\ninput[type=\\"checkbox\\"] {\\n    width: 16px !important;\\n    height: 16px !important;\\n    display: inline-block;\\n    margin: 0 8px 0 0 !important;\\n    vertical-align: middle;\\n}\\n.name {\\n    display: flex;\\n    justify-content: space-between;\\n}\\n\\n.name .name-field-wrap {\\n    width: 49% !important;\\n}\\n\\n.field-wrap h2 {\\n    font-weight: 400;\\n    font-size: 22px;\\n    margin: 40px 0 20px;\\n}\\n.custom-continue-button button {\\n    font-size: 15px;\\n    text-transform: uppercase;\\n    outline: none;\\n    text-decoration: none;\\n    display: inline-block;\\n    background: #00A9FA;\\n    padding: 12px 15px;\\n    color: #fff;\\n    transition:0.3s;\\n    border:none;\\n}\\n.custom-continue-button a:hover {\\n    box-shadow: 0 0 2px 0 #7a7a7a;\\n}\\n.custom-continue-button a {\\n    font-size: 15px;\\n    text-transform: uppercase;\\n    outline: none;\\n    text-decoration: none;\\n    display: inline-block;\\n    background: #00A9FA;\\n    padding: 12px 15px;\\n    color: #fff;\\n    transition:0.3s;\\n    border:none;\\n}\\n.custom-continue-button button:hover {\\n    box-shadow: 0 0 2px 0 #7a7a7a;\\n}\\n\\n.custom-continue-button {\\n    padding: 20px 0;\\n    display: flex;\\n    justify-content: space-between;\\n}\\nul.shipping-details-list a {\\n    display: block;\\n    width: 15%;\\n    font-size: 16px;\\n}\\n\\nul.shipping-details-list p {\\n    width: 70%;\\n    font-size: 16px;\\n    margin:0;\\n}\\nul.shipping-details-list span {\\n    display: block;\\n    width: 15%;\\n    font-size: 16px;\\n}\\nul.shipping-details-list li {\\n    display: flex;\\n    justify-content: left;\\n    align-items: center;\\n    width: 100%;\\n    border-top: 1px solid #7a7a7a33;\\n    padding:15px 0;\\n}\\nul.shipping-details-list li:first-child{\\n   border:none;\\n}\\nul.shipping-details-list {\\n    padding:0 15px;\\n    border: 2px solid #7a7a7a33;\\n    margin-top:40px;\\n    border-radius:6px;\\n}\\nul.shipping-details-list li a {\\n    color: #00A9FA;\\n    text-decoration:none;\\n}\\nul.shipping-details-list h4 {\\n   text-align: right;\\n    margin: 0 0 0 auto;\\n    width: 15%;\\n}\\nul.shipping-details-list input {\\n    margin-right: 10px;\\n}\\n.payment-acceptance p {\\n    font-size: 16px;\\n    margin: 15px 0;\\n}\\n.payment-acceptance figure img {\\n    width: 100%;\\n}\\n.payment-acceptance figure {\\n    margin: 0 auto;\\n    width: 100px;\\n    height: 100px;\\n}\\n.payment-acceptance {\\n    padding: 20px;\\n    text-align: center;\\n    background: #fafafa;\\n    border-radius: 6px;\\n}\\n/* right side start here */\\n\\n.module-wrap {\\n    display: flex;\\n    justify-content: space-between;\\n}\\n.right-content {\\n    width: 40%;\\n    background: #fafafa;\\n    padding: 20px 30px 30px;\\n    height: 80vh;\\n}\\nul.clearfix.tabber-tabs {\\n    border: none;\\n}\\n.left-content {\\n    width: 60%;\\n    padding: 20px 30px 30px;\\n}\\nfigure.cart-product-img {\\n    width: 60px;\\n    margin: 0;\\n}\\nfigure.cart-product-img img{\\n   width:100%;\\n\\n}\\n.cart-products {\\n    display: flex;\\n    justify-content: left;\\n    align-items: center;\\n}\\n.cart-products h4 {\\n    font-size: 16px;\\n    margin: 0;\\n    padding: 0 15px;\\n}\\nul.total-billing li {\\n    display: flex;\\n    justify-content: space-between;\\n    align-items: center;\\n}\\nul.total-billing{\\n   padding:0;\\n}\\nul.total-billing h6 {\\n    font-size: 16px;\\n    margin:0;\\n}\\nul.charges {\\n    width: 100%;\\n    padding: 0;\\n    border-top: 1px solid #7a7a7a33;\\n    border-bottom: 1px solid #7a7a7a33;\\n    padding: 15px 0;\\n}\\nul.charges li {\\n    display: flex;\\n    justify-content: space-between;\\n}\\nul.charges li {\\n    line-height: 0px;\\n}\\nul.total-billing p, ul.total-billing small {\\n    color: #7a7a7a;\\n}\\n.container {\\n    max-width: 1200px;\\n    margin: 0 auto;\\n    padding:0 15px;\\n}\\n\\n/* dropdown */\\n\\n/*the container must be positioned relative:*/\\n.custom-select {\\n  position: relative;\\n}\\n\\n.custom-select select {\\n  display: none; /*hide original SELECT element:*/\\n}\\n\\n.select-selected {\\n  background-color: #fff;\\n}\\nul.shipping-details-list li a:hover {\\n    text-decoration: underline;\\n}\\n.payment-acceptance .btn-brand {\\n    background-color: #00a9fa;\\n    border: none;\\n    color: #fff;\\n    outline: none;\\n}\\n.payment-acceptance .btn-brand:hover{\\n  background-color :#04a0eb;\\n}\\n/*style the arrow inside the select element:*/\\n.select-selected:after {\\n  position: absolute;\\n  content: \\"\\";\\n  top: 18px;\\n  right: 16px;\\n  width: 0;\\n  height: 0;\\n  border: 6px solid transparent;\\n  border-color: #A59F9F transparent transparent transparent;\\n}\\n\\n/*point the arrow upwards when the select box is open (active):*/\\n.select-selected.select-arrow-active:after {\\n  border-color: transparent transparent #A59F9F transparent;\\n  top: 11px;\\n}\\n.select-selected{\\nborder:2px solid #7a7a7a33;\\n}\\n/*style the items (options), including the selected item:*/\\n.select-items div,.select-selected {\\n  color: #A59F9F;\\n  padding: 10px 12px;\\n  cursor: pointer;\\n  user-select: none;\\n  height:40px;\\n  border-radius:8px;\\n  font-size:14px;\\n  font-weight:400;\\n  box-sizing: border-box;\\n}\\n\\n/*style items (options):*/\\n.select-items {\\n  position: absolute;\\n  background-color: #fff;\\n  top: 100%;\\n  left: 0;\\n  right: 0;\\n  z-index: 99;\\n  border:2px solid #7a7a7a33;\\n}\\n\\n/*hide the items when the select box is closed:*/\\n.select-hide {\\n  display: none;\\n}\\n\\n.select-items div:hover, .same-as-selected {\\n  background-color: #00A9FA;\\n  color:#fff;\\n}\\n\\n\\n.location-region {\\n    display: flex;\\n    justify-content: space-between;\\n    padding-bottom:10px;\\n}\\nlabel.error {\\n    color: red;\\n}\\n\\n.cart-products h4 {\\n    padding: 10px 0;\\n}\\n@media screen and (max-width:767px){\\n .module-wrap {\\n    flex-wrap: wrap;\\n}\\n  .left-content {\\n    width: 100%;\\n    padding:0px;\\n}\\n  .right-content{\\n    width:100%;\\n  }\\n  .tabber-wrap .tabber-tabs > li > span {\\n    padding: 0 6px;\\n}\\n  .tabber-wrap .tabber-tabs>li>a {\\n    font-size: 14px;\\n    padding-left: 0;\\n    padding-right: 0;\\n}\\n  .name .name-field-wrap {\\n    width: 100% !important;\\n}\\n.dropdown-item-menu {\\n    width: 100% !important;\\n}\\n  .location-region {\\n    flex-wrap: wrap;\\n}\\n  .name {\\n    flex-wrap: wrap;\\n}\\n  ul.shipping-details-list li {\\n    padding: 15px 0;\\n    flex-direction: column;\\n    justify-content: start;\\n    align-items: flex-start;\\n}\\n  ul.shipping-details-list span, ul.shipping-details-list p, ul.shipping-details-list a {\\n    width: 100%;\\n    font-size:15px;\\n}\\n}\\n\\n/* modal */\\n.modal-window {\\n\\t position: fixed;\\n\\t background-color:#2c2e2f99;\\n\\t top: 0;\\n\\t right: 0;\\n\\t bottom: 0;\\n\\t left: 0;\\n\\t z-index: 999;\\n\\t visibility: hidden;\\n\\t opacity: 0;\\n\\t pointer-events: none;\\n\\t transition: all 0.3s;\\n}\\n .modal-window:target {\\n\\t visibility: visible;\\n\\t opacity: 1;\\n\\t pointer-events: auto;\\n}\\n .modal-window > div {\\n\\t width: 400px;\\n\\t position: absolute;\\n\\t top: 50%;\\n\\t left: 50%;\\n\\t transform: translate(-50%, -50%);\\n\\t padding: 2em;\\n\\t background: #fff;\\n   transition:0.3s ease-in-out;\\n}\\na.modal-close {\\n    text-decoration: none;\\n    background: #00A9FA;\\n    display: inline-block;\\n    color: #fff;\\n    padding: 4px 10px 6px;\\n    margin-top: 10px;\\n    font-size: 14px;\\n}\\n .modal-window header {\\n\\t font-weight: bold;\\n}\\n .modal-window h1 {\\n\\t font-size: 150%;\\n\\t margin: 0 0 15px;\\n}\\n .modal-close:hover {\\n\\t background:#00a2ef;\\n}\\n/* Demo Styles */\\n a {\\n\\t color: inherit;\\n}\\n .modal-window div:not(:last-of-type) {\\n\\t margin-bottom: 15px;\\n}\\n .btn {\\n\\t background-color: #00A9FA;\\n   color:#fff;\\n\\t padding: 1em 1.5em;\\n\\t border-radius: 3px;\\n\\t text-decoration: none;\\n}\\n.btn{\\n   background-color:#00a2ef;\\n}\\n.modal-box {\\n    box-shadow: 0 0 7px 0 #00000012;\\n}\\n .btn i {\\n\\t padding-right: 0.3em;\\n}\\n \\n\\n\\t</style>\\n\\n  </head>\\n  <body>\\n    \\n<!--     loader start here -->\\n    <div class=\\"loader-wrapper\\">\\n    <div id=\\"loader\\" class=\\"lds-roller\\"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>\\n        </div>\\n       <script>\\n        $(window).on(\\"load\\",function(){\\n          $(\\".loader-wrapper\\").fadeOut(\\"slow\\");\\n        });\\n      </script>\\n   \\n<!--     loader end here -->\\n    \\n    \\n    <!--   E-cart Module   -->\\n   <script src=\\"https://unpkg.com/sweetalert/dist/sweetalert.min.js\\"></script>\\n<div class=\\"container\\">\\n            <div class=\\"module-wrap\\">\\n                <div class=\\"left-content\\">\\n                    <h1 class=\\"page-heading\\">\\n                        Web Dev Store\\n                    </h1>\\n                    <div class=\\"tabber-wrap blog-tabber\\">\\n                        <ul class=\\"clearfix tabber-tabs\\">\\n<!--                             <li class=\\"custom-tab\\"><a href=\\"#tab-0\\">Cart</a><span>></span></li> -->\\n                            <li class=\\"custom-tab\\"><a href=\\"/cart_web_page\\">Cart</a><span>></span></li>\\n                            <li class=\\"custom-tab active\\"><a href=\\"#tab-1\\" id=\\"information\\">Information</a><span>></span></li>\\n                            <li class=\\"custom-tab\\"><a id=\\"shipping\\">Shipping</a><span>></span></li>\\n                            <li class=\\"custom-tab\\"><a id=\\"payment\\">Payment</a></li>\\n                        </ul>\\n                        <div class=\\"tabber-content\\">\\n                            \\n<!--                             <form class=\\"shipping-payment-form\\" name=\\"user-info\\" role=\\"form\\" action=\\"https://apps.webdew.com/hubspot/ecommerce/stripePay\\" method=\\"POST\\" id=\\"payment-form\\">   -->\\n                          <form class=\\"shipping-payment-form\\" name=\\"user-info\\" role=\\"form\\" id=\\"payment-form\\">  \\n                              <input type=\\"hidden\\" name=\\"portal_id\\" id=\\"portal_id\\" value=\\"{{portal_id}}\\">\\n                              <input type=\\"hidden\\" name=\\"amount\\" id=\\"amount\\" value=\\"\\">\\n                                <div class=\\"hidden_list hidden\\">\\n                                </div>\\n<!--                                 <div class=\\"tab-pane\\" id=\\"tab-0\\">\\n                                    <div class=\\"products-cart-wrap\\">\\n                                      <table style=\\"width:100%\\" >\\n                                          <tr >\\n                                              <th width=\\"55%\\">Product</th>\\n                                              <th width=\\"15%\\">Price</th>\\n                                              <th width=\\"15%\\">quantity</th>\\n                                              <th width=\\"15%\\">Total</th>\\n                                          </tr>\\n                                      </table>\\n                                      <table id=\\"list1\\"  style=\\"width:100%\\">\\n                                      </table>\\n                                  </div>\\n                                </div> -->\\n                                <div class=\\"tab-pane active\\" id=\\"tab-1\\">\\n                                    <div class=\\"shipping-form-wrap\\">\\n                                        <div class=\\"field-wrap\\">\\n                                            <h2>Contact Information</h2>\\n                                            <input type=\\"email\\" name=\\"email_mobile\\" id=\\"email-mobile\\" Placeholder=\\"Email \\" required><br>\\n                                            <!-- <input type=\\"checkbox\\" name=\\"check\\">\\n                                                <label>\\n                                                Keep me up to date on news and exclusive offers\\n                                                </label> \\n                                            -->\\n                                        </div>\\n                                        <div class=\\"field-wrap\\">\\n                                            <h2>Shipping address</h2>\\n                                            <div class=\\"name\\">\\n                                              <div class=\\"name-field-wrap\\">\\n                                                \\n                                      \\n                                                <input type=\\"text\\" name=\\"fname\\" id=\'first-name\' Placeholder=\\"First name\\" required>\\n                                                  <p id=\'first-name-error\'></p>\\n                                                        </div>\\n                                                 <div class=\\"name-field-wrap\\">\\n                                                <input type=\\"text\\" name=\\"lname\\" id=\'last-name\' Placeholder=\\"Last name (Optional)\\" ><span id=\'last-name-error\'></span>\\n                                                  </div>  </div>\\n                                             \\n                                            <input type=\\"text\\" name=\\"address\\" Placeholder=\\"Address\\" id=\'address\' required><span id=\'address-error\'></span>\\n                                            <input type=\\"text\\" name=\\"apartment\\" Placeholder=\\"Apartment,suite,etc (Optional)\\" id=\'apartment\'><span id=\'apartment-error\'></span>\\n                                            <input type=\\"text\\" name=\\"city\\" Placeholder=\\"City\\" id=\'city\' required><span id=\'city-error\'></span>\\n                                            <div class=\\"location-region\\">\\n                                                <div style=\\"width:32%;\\" class=\\"dropdown-item-menu\\">\\n                                                    <div class=\\"custom-select\\">\\n                                                        <input required id=\\"country\\" name=\\"country\\" placeholder=\\"Enter Country\\">\\n                                                    </div>\\n                                                </div>\\n                                                <div style=\\"width:32%;\\" class=\\"dropdown-item-menu\\">\\n                                                    <div class=\\"custom-select\\">\\n                                                        <input name=\\"state\\" id=\\"state\\" required=\\"\\" placeholder=\\"Enter State\\">\\n                                                    </div>\\n                                                </div>\\n                                                 <div style=\\"width:32%;\\" class=\\"dropdown-item-menu\\">\\n                                                    <div class=\\"custom-select\\">\\n                                                <input type=\\"text\\" name=\\"pin_portal\\" placeholder=\\"PIN code\\" id=\\"pin\\">\\n                                           </div>\\n                                                </div>\\n                                                   </div>\\n                                            <!-- <input type=\\"checkbox\\" name=\\"check\\">\\n                                            <label>\\n                                            Save this information for next time\\n                                            </label> -->\\n                                        </div>\\n                                        <div class=\\"custom-continue-button\\">\\n                                            <!-- <a href=\\"#\\">Back</a> -->\\n                                            <button id=\'user-info\'>Continue</button> \\n                                          <!--        success modal -->\\n    <div class=\\"interior\\">\\n    <a class=\\"confirm_order btn hidden\\" href=\\"#open-modal\\">Confirm Order</a>\\n  </div>\\n\\n\\n<div id=\\"open-modal\\" class=\\"modal-window\\">\\n  <div class=\\"modal-box\\">\\n    <h1>Successfully Booked</h1>\\n    <div>Your order is successfully done!</div>\\n     <a href=\\"#\\" title=\\"Close\\" class=\\"modal-close\\">OK</a>\\n    </div>\\n</div>\\n<!-- end success modal -->\\n                                        </div>\\n                                    </div>\\n                                </div>\\n                                <div class=\\"tab-pane\\" id=\\"tab-2\\">\\n                                    <ul class=\\"shipping-details-list\\">\\n                                        <li>\\n                                            <span>\\n                                            Contact\\n                                            </span>\\n                                            <p class=\'contact_append\'> </p>\\n                                            <a class=\\"change_back\\" rel=\\"1\\" href=\\"javascript:void(0);\\">\\n                                            change\\n                                            </a>\\n                                        </li>\\n                                        <li>\\n                                            <span>\\n                                            Ship to\\n                                            </span>\\n                                            <p class=\\"shipping_append\\"> </p>\\n                                            <a class=\\"change_back\\" rel=\\"1\\" href=\\"javascript:void(0);\\">\\n                                            change\\n                                            </a>\\n                                        </li>\\n                                    </ul>\\n                                    <div class=\\"field-wrap\\">\\n                                        <!-- <h2>Shipping method</h2>\\n                                        <ul class=\\"shipping-details-list\\" style=\\"margin-top:0;\\">\\n                                            <li>\\n                                                <input type=\\"radio\\" name=\\"standard\\" checked=\\"checked\\">\\n                                                <p>\\n                                                    Card\\n                                                </p>\\n                                                <h4>\\n                                                    Free\\n                                                </h4>\\n                                            </li>\\n                                        </ul> -->\\n                                    </div>\\n                                    <div class=\\"custom-continue-button\\">\\n                                        <a class=\\"change_back\\" rel=\\"1\\" href=\\"javascript:void(0);\\">Back</a>\\n                                        <a href=\\"javascript:void(0);\\" id=\\"shipping_next\\" rel=\\"3\\">Continue</a>\\n                                    </div>\\n                                </div>\\n                                <div class=\\"tab-pane\\" id=\\"tab-3\\">\\n                                    <ul class=\\"shipping-details-list\\">\\n                                        <li>\\n                                            <span>\\n                                            Contact\\n                                            </span>\\n                                            <p class=\\"contact_append\\"> </p>\\n                                            <a class=\\"change_back\\" rel=\\"1\\" href=\\"javascript:void(0);\\">\\n                                            change\\n                                            </a>\\n                                        </li>\\n                                        <li>\\n                                            <span>\\n                                            Ship to\\n                                            </span>\\n                                            <p class=\\"shipping_append\\"></p>\\n                                            <a class=\\"change_back\\" rel=\\"1\\" href=\\"javascript:void(0);\\">\\n                                            change\\n                                            </a>\\n                                        </li>\\n                                        <li>\\n                                            <span>\\n                                            Method\\n                                            </span>\\n                                            <p>Card</p>\\n                                        </li>\\n                                    </ul>\\n                                    <div class=\\"field-wrap\\">\\n                                        <h2>Payment</h2>\\n                                        <div class=\\"payment-acceptance\\">\\n                                          \\n<!--                                             <figure>\\n                                                <img src=\\"{{ module.product_count_image.src }}\\" alt=\\"{{ module.product_count_image.alt }}\\" {{ loadingAttr }} {{ sizeAttrs }}>\\n                                            </figure> -->\\n                                            \\n                                        <!-- stripe -->\\n                                        \\n                                            <input \\n                                                            type=\\"submit\\"\\n                                                            class=\\"btn btn-brand btn-upper btn-block\\" \\n                                                            value=\\"Pay Now with Card\\"\\n                                                            data-key=\\"\\"\\n                                                            data-amount=\\"\\"\\n                                                            data-currency=\\"usd\\"\\n                                                            data-name=\\"Ecommerce\\"\\n                                                            data-email=\\"\\"\\n                                                            id=\\"stripe\\"\\n                                                        />\\n                                        \\n                                            <script src=\\"https://checkout.stripe.com/v2/checkout.js\\"></script>\\n                                            <script type=\\"text/javascript\\">\\n                                              var cors_api_url = \'https://cors-anywhere.herokuapp.com/\';\\n                                              function doCORSRequest(options, printResult) {\\n                                                var x = new XMLHttpRequest();\\n                                                x.open(options.method, cors_api_url + options.url);\\n                                                x.onload = x.onerror = function() {\\n                                                  var result = {};\\n                                                  result.method =  options.method\\n                                                  result.url = options.url;\\n                                                  result.status = x.status;\\n                                                  result.text = x.statusText;\\n                                                  result.responseText = x.responseText;\\n                                                  printResult(result);\\n\\n                                                };\\n                                                if (/^POST/i.test(options.method)) {\\n                                                    x.setRequestHeader(\'Content-Type\', options.content_type);\\n                                                } else if (/^PUT/i.test(options.method)) {\\n                                                    x.setRequestHeader(\'Content-Type\', options.content_type);\\n                                                    x.setRequestHeader(\\"x-requested-with\\", \\"XMLHttpRequest\\");\\n                                                } else {\\n                                                    x.setRequestHeader(\'Content-Type\', options.content_type);\\n                                                }\\n                                                x.send(options.data);\\n                                              }\\n                                              //restart the local storage\\n                                              function ClearAll() {\\n                                                sessionStorage.clear();\\n                                                localStorage.clear();\\n                                              }\\n                                              $(document).ready(function() {\\n                                                  $(document).on(\'click\',\'#stripe\' ,function(event) {\\n                                                      event.preventDefault();\\n                                                      var $button = $(this),\\n                                                          $form = $button.parents(\'form\');\\n                                                      var opts = $.extend({\\n                                                          amount: $(\'#amount\').val(),\\n                                                          currency: \'usd\',\\n                                                          email : $(\'#email-mobile\').val()\\n                                                      }, $button.data(), {\\n                                                          token: function(result) {\\n                                                             $(\\".loader-wrapper\\").fadeIn(\\"slow\\");\\n                                                             //console.log(result);\\n                                                             $form.append($(\'<input>\').attr({ id: \\"stripeToken\\", type: \'hidden\', name: \'stripeToken\', value: result.id }));\\n                                                            \\n                                                            \\n                                                            $.ajax ({\\n                                                type: \\"POST\\",\\n                                                //url: \'https://apps.webdew.com/hubspot/ecommerce/stripePay?dat=\'+ form.serialize() +\'&portal_id=\'+$(\'#portal_id\').val()+\'&local=\' + loca ,\\n                                                url: \'https://apps.webdew.com/hubspot/ecommerce/stripePay?status=true&\'+$(\'#payment-form\').serialize(),\\n                                                dataType: \'jsonp\',\\n                        //                         headers: {\\n                        //                           \'Authorization\': \\"Basic \\" + btoa(\\"myusername\\" + \\":\\" + \\"mypassword\\")\\n                        //                         },\\n                                                success: function (data, status){\\n                                                  console.log(data);\\n                                                   if(data.status === true){\\n                                                                swal(data.message);\\n                                                                ClearAll();\\n                                                                var base_url = window.location.origin;\\n                                                                console.log(base_url);\\n                                                                window.location.replace(base_url+\\"/cart\\");\\n                                                              } else {\\n                                                                   swal(data.message);\\n                                                              }\\n                                                              $(\\".loader-wrapper\\").fadeOut(\\"slow\\");\\n                                                },\\n                                                error: function(jqXhr, textStatus, errorMessage){\\n                                                  console.log(textStatus);\\n                                                  console.log(errorMessage);\\n                                                }\\n                                              });\\n                                              \\n//                                                             doCORSRequest({\\n//                                                               method: \'GET\',\\n//                                                               content_type :\'application/json\',\\n//                                                               url:\'https://apps.webdew.com/hubspot/ecommerce/stripePay?portal_id=\'+$(\'#portal_id\').val()+\'&\'+$(\'#payment-form\').serialize(),\\n//                                                             }, function printResult(result) {\\n//                                                               result = JSON.parse(result.responseText);\\n//                                                               if(result.status === true){\\n//                                                                 swal(result.message);\\n//                                                                 ClearAll();\\n//                                                               } else {\\n//                                                                    swal(result.message);\\n//                                                               }\\n//                                                               $(\\".loader-wrapper\\").fadeOut(\\"slow\\");\\n//                                                             });\\n                                                          }\\n                                                      });\\n                                                      StripeCheckout.open(opts);\\n                                                  });\\n                                              });\\n                                              </script>\\n                                        \\n                                        <!-- stripe -->\\n\\n\\n                                        <!-- Paypal -->\\n\\n                                             <div class=\\"wd_proceed-payment\\">\\n                                                <div id=\\"paypal-button-container\\"></div>\\n                                                <script id=\\"paypalScrpt\\" src=\\"https://www.paypal.com/sdk/js?client-id=sb&amp;currency=USD\\" data-sdk-integration-source=\\"button-factory\\"></script>\\n                                                <script>\\n                                                  function runLoad(){\\n                                                      // Render the PayPal button into #paypal-button-container\\n                                                      paypal.Buttons({\\n                                                        // Set up the transaction\\n                                                        style: {\\n                                                            layout : \'horizontal\',\\n                                                            color  : \'gold\',\\n                                                            shape  : \'rect\',\\n                                                            height :  38,\\n                                                            tagline: \'false\',\\n                                                            label : \'pay\'\\n                                                        },\\n                                                        createOrder: function(data, actions) {\\n                                                            return actions.order.create({\\n                                                                purchase_units: [{\\n                                                                    amount: {\\n                                                                        value: $(\'#amount\').val() / 100,\\n                                                                    }\\n                                                                }]\\n                                                            });\\n                                                        },\\n                                                        // Finalize the transaction\\n                                                        onApprove: function(data, actions) {\\n                                                            $(\\".loader-wrapper\\").fadeIn(\\"slow\\");\\n                                                            return actions.order.capture().then(function(details) {\\n                                                                var payment_id = details.id;\\n                                                                var payment_status = details.status;\\n                                                                var address1 = details.payer.address.address_line_1;\\n                                                                var adminarea1 = details.payer.address.admin_area_1;\\n                                                                var adminarea2 = details.payer.address.admin_ares_2;\\n                                                                var postal_code = details.payer.address.postal_code;\\n                                                                var country_code = details.payer.address.country_code;\\n                                                                var address = details.payer.email_address;\\n                                                                var given_name = details.payer.name.given_name;\\n                                                                var surname = details.payer.name.surname;\\n                                                                var payer_id = details.payer.payer_id;\\n                                                              \\n//                                                                 doCORSRequest({\\n//                                                                   method: \'GET\',\\n//                                                                   content_type :\'application/json\',\\n//                                                                   url:\'https://apps.webdew.com/hubspot/ecommerce/PayPal?surname=\'+surname+\'&given_name=\'+given_name+\'&country_code=\'+country_code+\'&address=\'+address1+\'&adminarea1=\'+adminarea1+\'&adminarea2=\'+adminarea2+\'&payer_id=\'+payer_id+\'&payment_id=\'+payment_id+\'&payment_status=\'+payment_status+\'&postal_code=\'+postal_code+\'&portal_id=\'+$(\'#portal_id\').val()+\'&\'+$(\'#payment-form\').serialize(),\\n//                                                                 }, function printResult(result) {\\n//                                                                     result = JSON.parse(result.responseText);\\n//                                                                     if(result.status === true){\\n//                                                                         swal(result.message);\\n//                                                                         ClearAll();\\n//                                                                     } else {\\n//                                                                         swal(result.message);\\n//                                                                     }\\n//                                                                     $(\\".loader-wrapper\\").fadeOut(\\"slow\\");\\n//                                                                 });\\n//                                                                 \\n                                                                                     $.ajax ({\\n                                                type: \\"POST\\",\\n                                                //url: \'https://apps.webdew.com/hubspot/ecommerce/stripePay?dat=\'+ form.serialize() +\'&portal_id=\'+$(\'#portal_id\').val()+\'&local=\' + loca ,\\n                                                url:\'https://apps.webdew.com/hubspot/ecommerce/PayPal?surname=\'+surname+\'&given_name=\'+given_name+\'&country_code=\'+country_code+\'&address=\'+address1+\'&adminarea1=\'+adminarea1+\'&adminarea2=\'+adminarea2+\'&payer_id=\'+payer_id+\'&payment_id=\'+payment_id+\'&payment_status=\'+payment_status+\'&postal_code=\'+postal_code+\'&portal_id=\'+$(\'#portal_id\').val()+\'&\'+$(\'#payment-form\').serialize(),\\n                                                dataType: \'jsonp\',\\n                        //                         headers: {\\n                        //                           \'Authorization\': \\"Basic \\" + btoa(\\"myusername\\" + \\":\\" + \\"mypassword\\")\\n                        //                         },\\n                                                success: function (data, status){\\n                                                  console.log(data);\\n                                                   if(data.status === true){\\n                                                                swal(data.message);\\n                                                                ClearAll();\\n                                                                var base_url = window.location.origin;\\n                                                     console.log(base_url);\\n                                                                window.location.replace(base_url+\\"/cart\\");\\n                                                              } else {\\n                                                                   swal(data.message);\\n                                                              }\\n                                                              $(\\".loader-wrapper\\").fadeOut(\\"slow\\");\\n                                                },\\n                                                error: function(jqXhr, textStatus, errorMessage){\\n                                                  console.log(textStatus);\\n                                                  console.log(errorMessage);\\n                                                }\\n                                              });\\n                                                                \\n                                                            });\\n                                                        }\\n                                                    }).render(\'#paypal-button-container\');\\n                                                  }\\n                                                </script> \\n                                            </div>\\n\\n                                        <!--  Paypal  -->\\n                                        </div>\\n                                    </div>\\n                                    <div class=\\"custom-continue-button\\">\\n                                        <a class=\\"change_back\\" rel=\\"2\\" href=\\"javascript:void(0);\\">Back</a>\\n                                    </div>\\n                                </div>\\n                            </form>\\n                        </div>\\n                    </div>\\n                </div>\\n                <div class=\\"right-content\\">\\n                    <ul class=\\"total-billing\\">\\n                         <li>\\n                            <div class=\\"cart-products\\">\\n                                <h4>\\n                                    Checkout \\n                                </h4>\\n                            </div>\\n<!--                             <h6>\\n                                Rs. 1.00\\n                            </h6> -->\\n                        </li> \\n                        <li>\\n                            <ul class=\\"charges\\">\\n                                <li>\\n                                    <p>\\n                                        Subtotal\\n                                    </p>\\n                                    <h6 class=\\"checkout\\">\\n                                        0.00\\n                                    </h6>\\n                                </li>\\n<!--                                 <li>\\n                                    <p>\\n                                        Shipping\\n                                    </p>\\n                                    <small>Calculated at next step</small>\\n                                </li> -->\\n<!--                                 <li>\\n                                    <p>\\n                                        Taxes (estimated)\\n                                    </p>\\n                                    <h6>\\n                                        Rs. 0.09\\n                                    </h6>\\n                                </li> -->\\n                            </ul>\\n                        </li>\\n                        <li>\\n                            <h4>\\n                                Total\\n                            </h4>\\n                            <h2 class=\\"checkout\\">\\n                                0.00\\n                            </h2>\\n                        </li>\\n                    </ul>\\n                </div>\\n            </div>\\n        </div>\\n\\n\\n        \\n        \\n         \\n        <script type=\\"text/javascript\\" src=\\"https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js\\"></script>\\n        <script type=\\"text/javascript\\">\\n            // Wait for the DOM to be ready\\n            $(function() {                \\n                // Initialize form validation on the registration form.\\n                // It has the name attribute \\"registration\\"\\n                $(\\"form[name=\'user-info\']\\").validate({\\n                    // Specify validation rules\\n                    rules: {\\n                      // The key name on the left side is the name attribute\\n                      // of an input field. Validation rules are defined\\n                      // on the right side\\n                      email_mobile: {\\n                        required: true\\n                      },\\n                      fname: {\\n                        required: true\\n                      },\\n                      address: {\\n                        required: true\\n                      },\\n                      city: {\\n                        required: true\\n                      },\\n                      country: {\\n                        required: true\\n                      },\\n                      state: {\\n                        required: true\\n                      },\\n                      pin_portal:{\\n                        required: true \\n                      }\\n                    },\\n                    // Make sure the form is submitted to the destination defined\\n                    // in the \\"action\\" attribute of the form when valid\\n                    submitHandler: function(form) {\\n                        all_unactive();\\n                        details_append();\\n                        $(\'#tab-2\').addClass(\'active\');\\n                        $(\'#shipping\').parent().addClass(\'active\');\\n                        //var x = $(\'#stripeToken\').val();\\n                        //if(x != \'\' && typeof x !== \\"undefined\\"){\\n                          //  form.submit();\\n                          //var queryString = $(\'#payment-form\').serialize();\\n                          //console.log(queryString);\\n                         //console.log(localStorage);\\n//                          $.ajax({\\n//                              type: \\"POST\\",\\n//                              url: \'https://apps.webdew.com/hubspot/ecommerce/stripePay\',\\n//                              data: { form: form.serialize(), localData : localStorage }, // serializes form input\\n//                              success: function(data){\\n//                                  console.log(data);\\n//                              }\\n//                         });\\n                      \\n                      \\n                        \\n//                               $.ajax ({\\n//                                 type: \\"POST\\",\\n//                                 //url: \'https://apps.webdew.com/hubspot/ecommerce/stripePay?dat=\'+ form.serialize() +\'&portal_id=\'+$(\'#portal_id\').val()+\'&local=\' + loca ,\\n//                                 url: \'https://apps.webdew.com/hubspot/ecommerce/stripePay?portal_id=\'+$(\'#portal_id\').val()+\'&\'+$(\'#payment-form\').serialize(),\\n//                                 dataType: \'jsonp\', <!----- JSONP ADDITION ----->\\n//         //                         headers: {\\n//         //                           \'Authorization\': \\"Basic \\" + btoa(\\"myusername\\" + \\":\\" + \\"mypassword\\")\\n//         //                         },\\n//                                 success: function (data, status){\\n//                                   console.log(data);\\n//                                   alert(\'Thanks!\'); \\n//                                 },\\n//                                 error: function(jqXhr, textStatus, errorMessage){\\n//                                   console.log(textStatus);\\n//                                   console.log(errorMessage);\\n//                                 }\\n//                               });\\n\\n//                                \\n                        //}\\n                    }\\n                });\\n                function details_append(){\\n                    var email_mobile = $(\'#email-mobile\').val();\\n                    var fname = $(\'#first-name\').val();\\n                    var lname = $(\'#last-name\').val();\\n                    var con_append = email_mobile + \'\\\\n\' + fname + \' \' + lname\\n                    $(\'#stripe\').attr(\'data-email\',email_mobile);\\n                    $(\'.contact_append\').text(con_append);\\n                    var address = $(\'#address\').val();\\n                    var city = $(\'#city\').val();\\n                    var state = $(\'#state\').val();\\n                    var country = $(\'#country\').val();\\n                    var pin_code = $(\'#pin\').val();\\n                    var ship_append = address + \'\\\\n\' + city + \', \' + pin_code + \'\\\\n\' + state + \', \' + country\\n                    $(\'.shipping_append\').text(ship_append);\\n                }\\n                $(\'.change_back, #shipping_next\').click(function(){\\n                    var tab = $(this).attr(\'rel\');\\n                    all_unactive();\\n                    $(\'#tab-\'+tab).addClass(\'active\');\\n                    if(tab == 1) {\\n                        $(\'#information\').parent().addClass(\'active\');  \\n                    }\\n                    if(tab == 2) {\\n                        $(\'#shipping\').parent().addClass(\'active\');\\n                    }\\n                    if(tab == 3) {\\n                        $(\'#payment\').parent().addClass(\'active\');\\n                    }\\n                });\\n\\n                function all_unactive(){\\n                    $(\'.tabber-wrap.blog-tabber .tabber-tabs .custom-tab\').removeClass(\'active\');\\n                    $(\'.tabber-wrap.blog-tabber .tabber-content .tab-pane\').removeClass(\'active\');\\n                }\\n            });\\n          \\n    \\n        </script>\\n    \\n    <script>\\n    \\nfunction CheckBrowser() {\\n    if (\'localStorage\' in window && window[\'localStorage\'] !== null) {\\n      // we can use sessionStorage object to store data\\n      return true;\\n    } else {\\n      return false;\\n    }\\n  }\\n\\n//below step can be done via PHP and AJAX too. \\n  function doShowAll() {\\n    if (CheckBrowser()) {\\n      var key = \\"\\";\\n      var list = \\"\\";\\n      var i = 0;\\n      var grandTotal = 0;\\n      var htmlPro = \'\';\\n      localStorage.removeItem(\\"rzp_device_id\\");\\n      localStorage.removeItem(\\"__paypal_storage__\\");\\n      //for more advance feature, you can set cap on max items in the cart\\n      \\n      for (i = 0; i <= localStorage.length-1; i++) {\\n        key = localStorage.key(i);\\n        \\n        if(key!=\\"rzp_device_id\\" || key!=\\"__paypal_storage__\\")\\n        {\\n          \\n        var val = localStorage.getItem(key);\\n        val = JSON.parse(val);\\n        var price = val.price;\\n        var total = val.quantity * price;\\n        \\n        if(key != \'__paypal_storage__\'){\\n          htmlPro += \'<input type=\\"hidden\\" name=\\"pro[\'+key+\']\\" value=\\"\'+ price + \'_\' + val.quantity +\'\\">\';\\n          grandTotal += total;\\n//            list += \'<tr><td width=\\"55%\\"><div class=\\"product-img-name  cart-item-img\\"><figure class=\\"product-img-wrap\\"><img src=\\"\\" alt=\\"\\"></figure><div class=\\"product-name\\"><h5>\'+val.name+\'</h5><button class=\\"del\\" rel=\\"\'+ key +\'\\">Remove</button></div></div></td><td width=\\"15%\\">$\'+val.price+\'</td><td width=\\"15%\\"><input type=\\"number\\" id=\\"quantity\\" class=\\"plus\\" name=\\"quantity\\" rel=\\"\'+ key +\'\\" value=\\"\' + val.quantity + \'\\" min=\\"1\\"></td><td width=\\"15%\\">$\'+total+\'</td></tr>\';\\n            list += \'<tr><td width=\\"55%\\"><div class=\\"product-img-name  cart-item-img\\"><figure class=\\"product-img-wrap\\"><img src=\\"\\" alt=\\"\\"></figure><div class=\\"product-name\\"><h5>\'+val.name+\'</h5></div></div></td><td width=\\"15%\\">$\'+val.price+\'</td><td width=\\"15%\\"><input type=\\"number\\" id=\\"quantity\\" class=\\"plus\\" name=\\"quantity\\" rel=\\"\'+ key +\'\\" value=\\"\' + val.quantity + \'\\" min=\\"1\\"></td><td width=\\"15%\\">$\'+total+\'</td></tr>\';\\n        }\\n      }\\n    }\\n      $(\'.hidden_list\').html(htmlPro);\\n      //if no item exists in the cart\\n      if (list != \\"\\" ) {\\n        $(\'.grand-total\').html(\'$\'+grandTotal);\\n        $(\'.checkout\').attr(\'id\',\'grandTotal\');\\n        $(\'.checkout\').attr(\'rel\',grandTotal);\\n        $(\'.checkout\').html(\'$\'+grandTotal);\\n        $(\'#grandTotalValue\').val(grandTotal);\\n        $(\'#stripe\').attr(\'data-amount\',grandTotal * 100);\\n        $(\'#amount\').val(grandTotal * 100);\\n        \\n      } else {\\n        \\n        list += \'<tr><td width=\\"55%\\"><div class=\\"product-img-name\\"><figure class=\\"product-img-wrap\\"><img src=\\"\\" alt=\\"\\"></figure><div class=\\"product-name\\"><h5>No Item</h5></div></div></td><td width=\\"15%\\"></td><td width=\\"15%\\"></td><td width=\\"15%\\"></td></tr>\';\\n          \\n          $(\'.grand-total\').html(\'0\');\\n          $(\'.checkout\').attr(\'id\',\'grandTotal\');\\n          $(\'.checkout\').attr(\'rel\',\'0\');\\n          $(\'.checkout\').html(\'Checkout 0\');\\n          $(\'#grandTotalValue\').val(0);\\n          $(\'#stripe\').attr(\'data-amount\',0);\\n          $(\'#amount\').val(0);\\n      }\\n      runLoad();\\n      //bind the data to html table\\n      //you can use jQuery too....\\n      //document.getElementById(\'list1\').innerHTML = list;\\n\\n      jQuery(function($)\\n      {\\n          var $cartHeader = $(\\".cart-header\\");\\n          var $cartContent = $(\\".cart-content\\");\\n  \\n          function onResize()\\n          {\\n              var wh = $(window).height();\\n              var ch = wh - $cartHeader.height();\\n  \\n              $cartContent.css({\\n                  \\"height\\": ch\\n              });\\n          }\\n  \\n          $(window).resize(onResize); onResize();\\n  \\n          $(\'.cart-tab-cell\').click(function(){\\n              console.log(\\"click\\");\\n              $(\'.cart-panel\').toggleClass(\\"current-section__favorites\\");\\n          });\\n  \\n      });\\n    } else {\\n      alert(\'Cannot save shopping list as your browser does not support HTML 5\');\\n    }\\n  }\\n  doShowAll();\\n  //below function may be redundant\\n  function getPortal(){\\n    $.ajax ({\\n      type: \\"GET\\",\\n      url: \'https://apps.webdew.com/hubspot/ecommerce/\'+$(\'#portal_id\').val(),\\n      dataType: \'jsonp\',\\n      success: function (result, status){\\n        if(result.status === true){\\n            $(\'#stripe\').attr(\'data-key\',result.data.stripe_pk_key);\\n            $(\'#paypalScrpt\').attr(\'src\',\'https://www.paypal.com/sdk/js?client-id=\'+result.data.paypal_key+\'&amp;currency=USD\');\\n            //runLoad();\\n        }\\n      },\\n      error: function(jqXhr, textStatus, errorMessage){\\n        console.log(textStatus);\\n        console.log(errorMessage);\\n      }\\n    });\\n  }\\n  getPortal();\\n  \\n \\nfunction hsOnReadyLoadTabber() { \\n  // Variables   \\n  var i;\\n  var tabPane = document.getElementsByClassName(\\"tab-pane\\"); \\n  var tabAnchor = document.querySelectorAll(\\".tabber-tabs a\\");\\n  \\n  // Loop through all of the tab anchors \\n  for (i = 0; i < tabAnchor.length; i++) {\\n\\n    // Click function for when tab is clicked on\\n    tabAnchor[i].addEventListener(\\"click\\", function(e){\\n      e.preventDefault(); \\n      var tabIsOpen = this.parentElement.classList.contains(\\"active\\");\\n      var tabPaneId = this.getAttribute(\\"href\\").substring(1);\\n      var activeTabPane = document.getElementById(tabPaneId); \\n      // If the tab clicked is not already opened\\n      if (tabIsOpen === false) { \\n        for (i = 0; i < tabAnchor.length; i++) {\\n          // Removes active class on all tab anchors\\n          tabAnchor[i].parentElement.classList.remove(\\"active\\");\\n        }\\n        for (i = 0; i < tabPane.length; i++) {\\n          // Removes active class on all tab panes\\n          tabPane[i].classList.remove(\\"active\\");\\n        }\\n        // Adds active class to the active tab pane and anchor\\n        this.parentElement.classList.add(\\"active\\");\\n        activeTabPane.classList.add(\\"active\\");\\n      }\\n    });   \\n  }\\n} \\n\\nif (document.readyState === \\"complete\\" ||\\n    (document.readyState !== \\"loading\\" && !document.documentElement.doScroll)\\n   ) {\\n  hsOnReadyLoadTabber();\\n} else {\\n  document.addEventListener(\\"DOMContentLoaded\\", hsOnReadyLoadTabber);\\n}\\n\\n\\n\\n\\n\\n// dropdown\\n// \\n// var x, i, j, l, ll, selElmnt, a, b, c;\\n/*look for any elements with the class \\"custom-select\\":*/\\nx = document.getElementsByClassName(\\"custom-select\\");\\nl = x.length;\\nfor (i = 0; i < l; i++) {\\n  selElmnt = x[i].getElementsByTagName(\\"select\\")[0];\\n  ll = selElmnt.length;\\n  /*for each element, create a new DIV that will act as the selected item:*/\\n  a = document.createElement(\\"DIV\\");\\n  a.setAttribute(\\"class\\", \\"select-selected\\");\\n  a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;\\n  x[i].appendChild(a);\\n  /*for each element, create a new DIV that will contain the option list:*/\\n  b = document.createElement(\\"DIV\\");\\n  b.setAttribute(\\"class\\", \\"select-items select-hide\\");\\n  for (j = 1; j < ll; j++) {\\n    /*for each option in the original select element,\\n    create a new DIV that will act as an option item:*/\\n    c = document.createElement(\\"DIV\\");\\n    c.innerHTML = selElmnt.options[j].innerHTML;\\n    c.addEventListener(\\"click\\", function(e) {\\n        /*when an item is clicked, update the original select box,\\n        and the selected item:*/\\n        var y, i, k, s, h, sl, yl;\\n        s = this.parentNode.parentNode.getElementsByTagName(\\"select\\")[0];\\n        sl = s.length;\\n        h = this.parentNode.previousSibling;\\n        for (i = 0; i < sl; i++) {\\n          if (s.options[i].innerHTML == this.innerHTML) {\\n            s.selectedIndex = i;\\n            h.innerHTML = this.innerHTML;\\n            y = this.parentNode.getElementsByClassName(\\"same-as-selected\\");\\n            yl = y.length;\\n            for (k = 0; k < yl; k++) {\\n              y[k].removeAttribute(\\"class\\");\\n            }\\n            this.setAttribute(\\"class\\", \\"same-as-selected\\");\\n            break;\\n          }\\n        }\\n        h.click();\\n    });\\n    b.appendChild(c);\\n  }\\n  x[i].appendChild(b);\\n  a.addEventListener(\\"click\\", function(e) {\\n      /*when the select box is clicked, close any other select boxes,\\n      and open/close the current select box:*/\\n      e.stopPropagation();\\n      closeAllSelect(this);\\n      this.nextSibling.classList.toggle(\\"select-hide\\");\\n      this.classList.toggle(\\"select-arrow-active\\");\\n    });\\n}\\nfunction closeAllSelect(elmnt) {\\n  /*a function that will close all select boxes in the document,\\n  except the current select box:*/\\n  var x, y, i, xl, yl, arrNo = [];\\n  x = document.getElementsByClassName(\\"select-items\\");\\n  y = document.getElementsByClassName(\\"select-selected\\");\\n  xl = x.length;\\n  yl = y.length;\\n  for (i = 0; i < yl; i++) {\\n    if (elmnt == y[i]) {\\n      arrNo.push(i)\\n    } else {\\n      y[i].classList.remove(\\"select-arrow-active\\");\\n    }\\n  }\\n  for (i = 0; i < xl; i++) {\\n    if (arrNo.indexOf(i)) {\\n      x[i].classList.add(\\"select-hide\\");\\n    }\\n  }\\n}\\n/*if the user clicks anywhere outside the select box,\\nthen close all select boxes:*/\\ndocument.addEventListener(\\"click\\", closeAllSelect);\\n\\n\\n\\n    </script>\\n    <!--   E-cart Module end  -->  \\n    \\n    {{ standard_footer_includes }}\\n  </body>\\n</html>"
         }',
          CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json',
            'Cookie: __cfduid=d8707b68473db1e73feda0f9e7ec6c21e1611835199'
          ),
         ));

         $response = curl_exec($curl);

         curl_close($curl);
         // echo $response;
         return 1;
}

public function curlCartTemplate($hapi_key){

          $curl = curl_init();

          curl_setopt_array($curl, array(
          CURLOPT_URL => 'https://api.hubapi.com/content/api/v2/templates?hapikey='.$hapi_key,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS =>'{
            "category_id": 1,
            "folder": "example",
            "is_available_for_new_content": "True",
            "template_type": 4,        
            "path":"custom/cartApi",
            "source":"<!--\\n    templateType: page\\n    isAvailableForNewContent: true\\n-->\\n<!doctype html>\\n<html>\\n  <head>\\n    <meta charset=\\"utf-8\\">\\n    <title>{{ content.html_title }}</title>\\n       <link rel=\\"stylesheet\\" href=\\"https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css\\">\\n<link rel=\\"stylesheet\\" href=\\"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css\\">\\n<link href=\\"https://fonts.googleapis.com/css2?family=DM+Sans:wght@500&display=swap\\" rel=\\"stylesheet\\">\\n<script src=\\"https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js\\"></script>\\n<script src=\\"https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js\\"></script>\\n    <meta name=\\"description\\" content=\\"{{ content.meta_description }}\\">\\n    {{ standard_header_includes }}\\n  </head>\\n  \\n  <style>\\n  @import url(\'https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap\');\\ntable{\\nfont-family: \'Montserrat\', sans-serif;\\n}\\nbody{\\nmargin:0;\\n}\\nfigure.product-img-wrap {\\n    width: 90px;\\n    height: 90px;\\n    margin: 0 40px 0 0;\\n}\\nfigure.product-img-wrap img{\\n    width:100%;\\n}\\n.product-img-name{\\ndisplay:flex;\\n  justify-content: left;\\n    align-items: center;\\n}\\n.product-name h5 {\\n    font-size: 18px;\\n    margin: 0 0 5px;\\n    color:#333;\\n}\\n.products-cart-wrap table th {\\n    font-size: 16px;\\n    text-transform: uppercase;\\n    color: #7a7a7a;\\n    text-align: left;\\n}\\n.products-cart-wrap table td{\\n     font-size: 16px;\\n    color: #7a7a7a;\\n    text-align: left;\\n}\\n input#quantity {\\n    width: 40px;\\n    height: 25px;\\n    outline: none;\\n    box-shadow: none;\\n    border: none;\\n    border: 1px solid #7a7a7a33;\\n}\\n.products-cart-wrap table tbody tr {\\n    border: 1px solid #7a7a7a33;\\n}\\ntd,th{\\n padding:15px;\\n}\\n.product-name a , .product-name button , .top-heading a{\\n    color: #7a7a7a;\\n    background:none;\\n    padding:0;\\n    border:none;\\n}\\n.product-name a:hover{\\n   color:#333;\\n}\\n.products-cart-wrap table {\\n    border-spacing: 0;\\n}\\n.top-heading h1 {\\n    font-size: 30px;\\n    margin: 0 0 10px 0;\\n}\\n.top-heading {\\n    text-align: center;\\n    padding: 30px 0 40px;\\n}\\n.total-checkout {\\n    text-align: right;\\n    padding: 30px 0;\\n}\\n.sub-total-checkout h3 {\\n    font-size: 22px;\\n    margin: 0;\\n}\\n.sub-total-checkout {\\n    display: flex;\\n    justify-content: space-between;\\n    width: 24%;\\n    margin: 0 0 0 auto;\\n    align-items: center;\\n    color:#7a7a7a;\\n}\\n.sub-total-checkout p {\\n    margin: 0;\\n    font-size: 16px;\\n}\\n.total-checkout h5 {\\n    font-size: 16px;\\n    margin: 0;\\n    padding: 20px 0 40px;\\n       color:#7a7a7a;\\n}\\n.checkout-button-custom a {\\n    font-size: 15px;\\n    text-transform: uppercase;\\n    outline: none;\\n    text-decoration: none !important;\\n    display: inline-block;\\n    background: #00A9FA;\\n    padding: 12px 15px;\\n    color: #fff;\\n    transition:0.3s;\\n}\\n.checkout-button-custom a:hover {\\n     color: #fff;\\n    box-shadow: 0 0 2px 0 #7a7a7a;\\n}\\n.checkout-button-custom button {\\n    font-size: 15px;\\n    text-transform: uppercase;\\n    outline: none;\\n    text-decoration: none;\\n    display: inline-block;\\n    background: #00A9FA;\\n    padding: 12px 15px;\\n    color: #fff;\\n    transition:0.3s;\\n}\\n.checkout-button-custom button:hover {\\n    box-shadow: 0 0 2px 0 #7a7a7a;\\n}\\n.container {\\n    max-width: 1200px;\\n    min-width: 800px;\\n    margin: 0 auto;\\n     padding:0 15px;\\n}\\n\\nbody{\\n  background-color:white !important;\\n}\\nlabel{\\n  margin-bottom: 5px!important;\\n  margin-top: 5px!important;\\n}</style>\\n  <body>\\n    <!--   E-cart Module   -->\\n\\n{% set query = request.query_dict %}\\n{% set product_id = query.product_id %}\\n{% set contact = crm_object(\\"product\\", product_id, \'id,price,createdate,name,full_embed_code, description\') %}\\n<div class=\\"card\\">\\n    <p>\\n        <button style=\\"display: none;\\" data-obid=\\"{{product_id}}\\" data-name=\\"{{ contact.name }}\\" data-description=\\"{{ contact.description }}\\" data-embed_code=\\"{{ contact.full_embed_code }}\\" data-price=\\"{{ contact.price }}\\" class=\\"add_cart\\">Add to Cart ${{ contact.price }}</button>\\n    </p>\\n    <input id=\\"grandTotalValue\\" type=\\"hidden\\">\\n</div>\\n<div class=\\"container\\">\\n<div class=\\"checkout-cart\\">\\n<div class=\\"top-heading\\">\\n    <h1>\\n        Your Cart\\n    </h1>\\n<!--     <a href=\\"#\\">Continue Shopping</a> -->\\n</div>\\n<div class=\\"products-cart-wrap\\">\\n    <table style=\\"width:100%\\" >\\n        <tr >\\n            <th width=\\"55%\\" style=\\"padding-left:40px;\\">Product</th>\\n            <th width=\\"15%\\">Price</th>\\n            <th width=\\"15%\\">quantity</th>\\n            <th width=\\"15%\\">Total</th>\\n        </tr>\\n    </table>\\n    <table id=\\"list1\\" style=\\"width:100%\\">\\n    </table>\\n</div>\\n<div class=\\"total-checkout\\">\\n    <div class=\\"sub-total-checkout \\" id=\\"grandTotalValue\\">\\n        <h3>\\n            Subtotal\\n        </h3>\\n        <p class=\\"checkout-button-custom checkout \\"> \\n        </p>\\n    </div>\\n    <h5>\\n        Taxes and shipping calculated at checkout\\n    </h5>\\n    <div class=\\"checkout-button-custom\\">\\n        <a href=\\"/checkout\\" class=\\"checkout-label\\" data-toggle=\\"modal\\">\\n        Checkout\\n        </a>\\n    </div>\\n</div>\\n<div>\\n</div>\\n<script src=\\"https://static.hsappstatic.net/jquery-libs/static-1.1/jquery/jquery-1.7.1.js\\"></script>\\n<script src=\\"https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js\\"></script>\\n<script>\\n    $(document).on(\'click\',\'.checkout-label\',function(){\\n        $(\'.cart-wrapper\').hide();\\n    });\\n    function SaveItem(id, obj) {\\n      sessionStorage.setItem(id, obj);\\n      localStorage.setItem(id,obj);\\n    }\\n    \\n    //------------------------------------------------------------------------------\\n    //change an existing key=>value in the HTML5 storage\\n    function ModifyItem(id, obj) {\\n      //check if key exists\\n      if (sessionStorage.getItem(id) != null) {\\n        //update\\n        sessionStorage.setItem(id,obj);\\n      }\\n      if(localStorage.getItem(id) != null)\\n      {\\n        localStorage.setItem(id,obj);\\n      }\\n    }\\n    //-------------------------------------------------------------------------\\n    //delete an existing key=>value from the HTML5 storage\\n    function RemoveItem(id) {\\n      sessionStorage.removeItem(id);\\n      localStorage.removeItem(id);\\n    }\\n    //-------------------------------------------------------------------------------------\\n    //restart the local storage\\n    function ClearAll() {\\n      sessionStorage.clear();\\n      localStorage.clear();\\n    }\\n    //--------------------------------------------------------------------------------------\\n    // dynamically populate the table with shopping list items\\n    //below step can be done via PHP and AJAX too. \\n    function doShowAll() {\\n      if (CheckBrowser()) {\\n        var key = \\"\\";\\n        var list = \\"\\";\\n        var i = 0;\\n        var grandTotal = 0;\\n        localStorage.removeItem(\\"rzp_device_id\\");\\n        localStorage.removeItem(\\"__paypal_storage__\\");\\n        //for more advance feature, you can set cap on max items in the cart\\n        for (i = 0; i <= localStorage.length-1; i++) {\\n          key = localStorage.key(i);\\n          if(key!=\\"rzp_device_id\\" || key!=\\"__paypal_storage__\\")\\n          { \\n            var val = localStorage.getItem(key);\\n            val = JSON.parse(val);\\n            var price = val.price;\\n            var total = val.quantity * price;\\n            grandTotal += total;\\n            list += \'<tr><td width=\\"55%\\"><div class=\\"product-img-name cart-item-img\\"><figure class=\\"product-img-wrap\\"><img src=\\"\\" alt=\\"\\"></figure><div class=\\"product-name\\"><h5>\'+val.name+\'</h5><button class=\\"del\\" rel=\\"\'+ key +\'\\">Remove</button></div></div></td><td width=\\"15%\\">$\'+val.price+\'</td><td width=\\"15%\\"><input type=\\"number\\" id=\\"quantity\\" class=\\"plus\\" name=\\"quantity\\" rel=\\"\'+ key +\'\\" value=\\"\' + val.quantity + \'\\" min=\\"1\\"></td><td width=\\"15%\\">$\'+total+\'</td></tr>\';\\n          }\\n        }\\n        //if no item exists in the cart\\n        if (list != \\"\\" ) {\\n          $(\'.grand-total\').html(\'$\'+grandTotal);\\n          $(\'.checkout\').attr(\'id\',\'grandTotal\');\\n          $(\'.checkout\').attr(\'rel\',grandTotal);\\n          $(\'.checkout\').html(\'$\'+grandTotal);\\n          $(\'#grandTotalValue\').val(grandTotal);\\n        } else {\\n          list += \'<tr><td width=\\"55%\\"><div class=\\"product-img-name\\"><figure class=\\"product-img-wrap\\"><img src=\\"\\" alt=\\"\\"></figure><div class=\\"product-name\\"><h5>No Item</h5></div></div></td><td width=\\"15%\\"></td><td width=\\"15%\\"></td><td width=\\"15%\\"></td></tr>\';\\n          $(\'.grand-total\').html(\'0\');\\n          $(\'.checkout\').attr(\'id\',\'grandTotal\');\\n          $(\'.checkout\').attr(\'rel\',\'0\');\\n          $(\'.checkout\').html(\'Checkout 0\');\\n          $(\'#grandTotalValue\').val(0);\\n        }\\n        //you can use jQuery too....\\n        document.getElementById(\'list1\').innerHTML = list;\\n      } else {\\n        alert(\'Cannot save shopping list as your browser does not support HTML 5\');\\n      }\\n    }\\n    doShowAll();\\n    //below function may be redundant\\n    function CheckBrowser() {\\n      if (\'localStorage\' in window && window[\'localStorage\'] !== null) {\\n        // we can use sessionStorage object to store data\\n        return true;\\n      } else {\\n        return false;\\n      }\\n    }\\n    \\n    $(document).on(\'change\',\'.plus\',function(){\\n      var value = $(this).attr(\'value\');\\n      var id = $(this).attr(\'rel\');\\n      var data = localStorage.getItem(id);\\n      data = JSON.parse(data);\\n      var qty = data.quantity + 1;\\n      var obj = {\\n        name : data.name,\\n        quantity : qty,\\n        price : data.price\\n      }\\n      obj = JSON.stringify(obj);\\n      ModifyItem(id, obj);\\n      doShowAll();\\n    });\\n    //   $(document).on(\'click\',\'.minus\',function(){\\n    //     var id = $(this).attr(\'rel\');\\n    //     var data = localStorage.getItem(id);\\n    //     data = JSON.parse(data);\\n    //     if(data.quantity > 1){\\n    //         var qty = data.quantity - 1;\\n    //         var obj = {\\n    //           name : data.name,\\n    //           quantity : qty,\\n    //           price : data.price\\n    //         }\\n    //         obj = JSON.stringify(obj);\\n    //         ModifyItem(id, obj);\\n    //     }\\n    //     doShowAll();\\n    //   });\\n    $(document).on(\'click\',\'.del\',function(){\\n      RemoveItem($(this).attr(\'rel\'));\\n      doShowAll();\\n    });\\n    $(document).on(\'click\',\'#clear\',function(){\\n      ClearAll();\\n      doShowAll();\\n    });\\n    \\n    $(document).ready(function(){\\n      var id = $(\'.add_cart\').attr(\'data-obID\');\\n      if(id == \'\'){\\n        id = 0;\\n      }\\n      if(id > 0){\\n        var data = localStorage.getItem(id);\\n        data = JSON.parse(data);\\n        if(data){\\n          if(data.quantity > 0){\\n              var qty = data.quantity + 1;\\n              var obj = {\\n                name : data.name,\\n                quantity : qty,\\n                price : data.price\\n              }\\n              obj = JSON.stringify(obj);\\n              ModifyItem(id, obj);\\n          }\\n        }else {\\n            var name = $(\'.add_cart\').attr(\'data-name\');\\n            if(name != \'\'){\\n                var new_price = $(\'.add_cart\').attr(\'data-price\');\\n                var price = new_price.replace(\\"रू\\", \\"\\");\\n                price = price.replace(\\"$\\", \\"\\");\\n                var object = {\\n                    name : name,\\n                    quantity : 1,\\n                    price : price\\n                }\\n                object = JSON.stringify(object);\\n                SaveItem(id, object);\\n            }\\n        }\\n      }\\n      doShowAll();\\n    });\\n</script>\\n    <!--   E-cart Module end  -->  \\n    {{ standard_footer_includes }}\\n  </body>\\n</html>"
         }',
          CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json',
            'Cookie: __cfduid=d8707b68473db1e73feda0f9e7ec6c21e1611835199'
          ),
         ));

         $response = curl_exec($curl);

         curl_close($curl);
         return 1;

}

    public function product_details($product_id, $hapi_key = '', $razor_key) {
        
        if(!empty($hapi_key) && !empty($product_id) && !empty($razor_key)) {
            $hapi_key = Crypt::decrypt($hapi_key);
            $product_id = Crypt::decrypt($product_id);
            $razor_key = Crypt::decrypt($razor_key);

            $args['url'] = 'https://api.hubapi.com/crm-objects/v1/objects/products/'. $product_id .'?properties=name&properties=description&properties=price&hapikey='.$hapi_key;
            $results = $this->curlAccessH( 'GET' , $args );
            $data = [];
            $next = '';
            $prev = '';
            $v = json_decode(json_encode($results), true);
            $product_name = $v['properties']['name']['value'];
            $product_price = $v['properties']['price']['value'];
            return view('button.cart-button',compact(['product_id','hapi_key','product_price','product_name','razor_key']));

        } else {
            echo 'Hubspot key is required.';
        }
        return view('product-details');
    }
    public function fetch_view(){
        Auth::logout();
        if(!empty($_GET['userEmail'])){
            $id = User::whereEmail($_GET['userEmail'])->value('id');
            if(!empty($id)) {
                $auth_token = User::whereEmail($_GET['userEmail'])->value('auth_token');
                if(!empty($auth_token)) {
                    
                    $results = array("results" => array(),
                                "primaryAction"=> array(
                                    "type"=> "IFRAME",
                                    "width"=> 890,
                                    "height"=> 748,
                                    "uri"=> url("ecommerce/settings-hub?vid=" . @$_GET['associatedObjectId'] . "&email=" .@$_GET['userEmail'] . "&phone=" . @$_GET['phone']. "&auth_id=" . @$id),
                                    "label"=> "Settings"
                                )
                    );  
                    
                } else {
                    $results = array("results" => array( 0 => array(
                                    "objectId"=> -1,
                                    "title" => "Please login and authorize your account with hubspot",
                                    "link" => env('APP_URL'),
                                    "priority" => "HIGH",
                                    "project" => "API"
                                )
                            )
                        );
                }

            } else {
                $results = array("results" => array( 0 => array(
                            "objectId"=> -1,
                            "title" => "You have to register your account.",
                            "link" => url('/'),
                            "priority" => env('APP_URL'),
                            "project" => "API"
                        )
                    )
                );
            }

        } else {
            $results = array("results" => array( 0 => array(
                        "objectId"=> -1,
                        "title" => "something were wrong.",
                        "link" => env('APP_URL'),
                        "priority" => "HIGH",
                        "project" => "API"
                    )
                )
            );
        }
        return json_encode($results);
    }
    public function refresh_token(){

        $refresh_token = Auth::user()->refresh_token;
        $tokens = "";
        if($refresh_token){            
            if(!empty($refresh_token)){
                
                //if(time() > Auth::user()->expires_at) {
                    
                    $curl = curl_init();
                    curl_setopt_array($curl, array(
                        CURLOPT_URL => 'https://api.hubapi.com/oauth/v1/token',
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => "",
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 30,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => 'POST',
                        CURLOPT_POSTFIELDS => 'grant_type=refresh_token&client_id='.env('HUBSPOT_CLIENT_ID').'&client_secret='.env('HUBSPOT_CLIENT_SECRET').'&refresh_token='.$refresh_token,
                        //CURLOPT_POSTFIELDS => 'grant_type=refresh_token&client_id='.env('HUBSPOT_CLIENT_ID').'&client_secret='.env('HUBSPOT_CLIENT_SECRET').'&scopes=e-commercere&fresh_token='.$refresh_token,
                        CURLOPT_SSL_VERIFYHOST => 0,
                        CURLOPT_SSL_VERIFYPEER => 0,
                        CURLOPT_HTTPHEADER => array(
                            //"authkey: ".Auth::user()->auth_token,
                            "content-type: application/x-www-form-urlencoded;charset=utf-8"
                        ),
                    ));

                    $result = curl_exec($curl);
                    $err = curl_error($curl);
                    curl_close($curl);
                    if ($err) {
                      echo "cURL Error #:" . $err;
                    }
                    $result = json_decode($result);
                    if(isset($result->access_token)){
                        $tokens = $result->access_token;
                        $refresh_token = $result->refresh_token;
                        
                        // updated
                            $users = User::find(Auth::user()->id);
                            $users->expires_at = time() + $result->expires_in * 0.95;
                            $users->refresh_token = $refresh_token;
                            $users->auth_token = $tokens;
                            //$users->save();
                        // refresh session
                    }
                    
                //}
            }
        } 
        
        return $tokens;
    }
    /**
     * Hubspot Integration
     */
    public function GET_settings_hub(Request $request) {
        if(!empty($_GET)) {
            $response = $_GET;
            User::whereId($_GET['auth_id'])->update(['object_data'=> serialize($response)]);
            $user = User::where('id', $_GET['auth_id'])->first();
            Auth::login($user);
        } 
        $data = Credential::whereUserId(Auth::user()->id)->where('type','ecommerce')->value('data');
        
        $render = 'render.ecommerce.settings';
        $data = unserialize($data);
        $html = view($render, compact('data'));
        return view('ecommerce.settings-hub',compact('data','html'));
        
    }
    
    public function POST_settings_hub(Request $request)
    {   
        $array['validation'] =  [
                        'stripe_sk_key' => 'required|min:25|max:200',
                        'stripe_pk_key' => 'required|min:25|max:200',
                        'paypal_key' => 'required|min:2|max:200',
                        // 'razor_key' => 'required|min:20|max:200',
                        'hapi_key' => 'required|min:30',
                    ];
        /* call function*/
        $v = \Validator::make($request->all(), $array['validation']);
        $u = User::whereId($request->auth_id)->first();
        Auth::login($u);
        if ($v->fails()) {
            $errors = $v->errors();
            $data = Credential::whereUserId(Auth::user()->id)
                                ->whereType('ecommerce')
                                ->value('data');
            $data = unserialize($data);
            $html = view('render.ecommerce.settings', compact('data'));
            return view('ecommerce.settings',compact('errors','data','html'));
        } 
        try {
            
            $res = $request->all();
            unset($res['_token']);
            unset($res['auth_id']);
            $id = Credential::whereUserId(Auth::user()->id)
                                ->whereType('ecommerce')
                                ->value('id');
            $data = serialize($res);
            if(empty($id)){
                Credential::create([
                                    'data'=> $data,
                                    'type'=> 'ecommerce',
                                    'fk_app_id'=> 3,
                                    'user_id' => Auth::user()->id,
                                ]);
                $alert = 'success';
                $message = 'Data has been saved.';
            } else {
                Credential::where('id',$id)->update([
                                                    'data' => $data,
                                                    'type'=> 'ecommerce',
                                                    'fk_app_id'=> 3
                                                ]);
                $alert = 'success';
                $message = 'Data has been updated.';
            }
        } catch (Exception $e) {
            $alert = 'warning';
            $message = 'Something went wrong in code.';
        }
        
        $data = Credential::whereUserId(Auth::user()->id)
                                ->whereType('ecommerce')
                                ->value('data');
        $data = unserialize($data);
        $html = view('render.ecommerce.settings', compact('data'));
        
        return view('ecommerce.settings-hub',compact('alert','message','data','html'));
    } 

    public function GET_sync_data() {
        if(!empty(DB::table('credentials')->where('type','ecommerce')->wherePortalId(Session::get('portal_id'))->value('data'))) {
            $credential_all = unserialize(DB::table('credentials')->where('type','ecommerce')->wherePortalId(Session::get('portal_id'))->value('data'));
            $hapi_key = $credential_all['hapi_key'];
            return view('ecommerce.sync-data-web');
        } else {
            return redirect('/apps/ecommerce/sync-data');
        }
        
    }
    public function get_theme_credentials($portal_id){
        if(!empty(DB::table('credentials')->where('type','ecommerce')->wherePortalId($portal_id)->value('data'))) {
            $credential_all = unserialize(DB::table('credentials')->where('type','ecommerce')->wherePortalId($portal_id)->value('data'));
            
            $res = ['status' => true, 'data' => $credential_all];
            if(isset($_GET['callback'])){
                unset($credential_all['razor_key']);
                unset($credential_all['stripe_sk_key']);
                $res = ['status' => true, 'data' => $credential_all];
                echo $_GET['callback']."(".json_encode($res).");";

                // $object=json_encode($res);
                // echo "payment_keys(".$object.");";
                exit;
            }
        } else {
            $res = ['status' => false];
            if(isset($_GET['callback'])){
                echo $_GET['callback']."(".json_encode($res).");";
               // $object=json_encode($res);
               //  echo "payment_keys(".$object.");"
                exit;
            }

        }
        return $res;
    }
    // private function curlAccess($method, $array, $content_type = 'array' ) 
    // {

    //     $ch = curl_init();
    //     curl_setopt($ch, CURLOPT_URL, $array['url']);
    //     curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    //     curl_setopt($ch, CURLOPT_ENCODING, "");
    //     curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
    //     curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    //     curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);        
    //     curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    //     curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    //     curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    //     switch ($method){
    //       case "POST":
    //         curl_setopt($ch, CURLOPT_POST, 1);        
    //          if ($array['data'])
    //             curl_setopt($ch, CURLOPT_POSTFIELDS, $array['data']);
    //          break;
    //       case "PUT":
    //          curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
    //          if ($array['data'])
    //             curl_setopt($ch, CURLOPT_POSTFIELDS, $array['data']);
    //          break;
    //       default:
    //          curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
    //     }

    //     curl_setopt($ch, CURLOPT_HTTPHEADER, $array['headers']);
    //     $result = curl_exec($ch);
        
    //     if (curl_errno($ch)) {
    //         echo 'Error:' . curl_error($ch);
    //     }
    //     curl_close($ch);

    //     if($content_type == 'array'){
    //         $result = json_decode($result); 
    //     }  
    //     return $result;

    // }
    private function curlAccessH($method, $array, $content_type = 'array' ) 
    {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $array['url']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_ENCODING, "");
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);        
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        switch ($method){
          case "POST":
            curl_setopt($ch, CURLOPT_POST, 1);        
             if ($array['data'])
                curl_setopt($ch, CURLOPT_POSTFIELDS, $array['data']);
             break;
          case "PUT":
             curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
             if ($array['data'])
                curl_setopt($ch, CURLOPT_POSTFIELDS, $array['data']);
             break;
          default:
             curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        }

        $headers = array();
        $headers[] = 'Content-Type: application/json';
        if(!empty($array['token'])){
            $headers[] = 'authorization: Bearer '. $array['token'];  
        }

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $result = curl_exec($ch);
        
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);

        if($content_type == 'array'){
            $result = json_decode($result); 
        }  
        return $result;

    }
    private function curlAccessG($method, $array, $content_type = 'array' ) 
    {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $array['url']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_ENCODING, "");
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);        
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        switch ($method){
          case "POST":
            curl_setopt($ch, CURLOPT_POST, 1);        
             if ($array['data'])
                curl_setopt($ch, CURLOPT_POSTFIELDS, $array['data']);
             break;
          case "PUT":
             curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
             if ($array['data'])
                curl_setopt($ch, CURLOPT_POSTFIELDS, $array['data']);
             break;
          default:
             curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        }

        $headers = array();
        $headers[] = 'Content-Type: application/json';

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $result = curl_exec($ch);
        
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);

        if($content_type == 'array'){
            $result = json_decode($result); 
        }  
        return $result;

    }
    
    public function settings()
    {

        $data = Credential::where('portal_id',Session::get('portal_id'))
                            ->whereType('ecommerce')
                            ->value('data');
        $data = unserialize($data);
        $html = view('render.ecommerce.settings', compact('data'));
        $payment_id = DB::table('ecommerce_payments')->where('user_id',Auth::user()->id)->where('portal_id',Session::get('portal_id'))->value('id');
        $env_app = env('APP_ENV');
        $stripe_P = config('services.'.$env_app.'_stripe.pk');
        $stripe_S = config('services.'.$env_app.'_stripe.sk');
        return view('ecommerce.settings',compact('data','html','payment_id','stripe_P','stripe_S'));
    }
    public function POST_settings(Request $request)
    {   
        $array['validation'] =  [
                                    'stripe_sk_key' => 'required|min:25|max:200',
                                    'stripe_pk_key' => 'required|min:25|max:200',
                                    'paypal_key' => 'required|min:2|max:200',
                                    //'razor_key' => 'required|min:20|max:200',
                                    'hapi_key' => 'required|min:30|max:200'
                                    
                                ];
        /* call function*/
        $v = \Validator::make($request->all(), $array['validation']);

        if ($v->fails()) {
            $errors = $v->errors();
            $data = Credential::whereType('ecommerce')
                                ->where('portal_id', Session::get('portal_id'))
                                ->value('data');
            $data = unserialize($data);
            
             // $data['hapi_key'] = Crypt::decryptString($data['hapi_key']);
             
            $html = view('render.ecommerce.settings', compact('data'));
            return view('ecommerce.settings',compact('errors','data','html'));
        } 
        try {
            
            $res = $request->all();



            $res['hapi_key'] =Crypt::encryptString($res['hapi_key']);//encrypting hubapikey
            
            //for decrypting , Crypt::decryptString($value)

             // print_r(Crypt::decryptString($res['hapi_key']));
             // die;

            unset($res['_token']);
            $id = Credential::whereType('ecommerce')
                                ->where('portal_id', Session::get('portal_id'))
                                ->value('id');
            $data = serialize($res);
            if(empty($id)){
                Credential::create([
                                            'data'=> $data,
                                            'type' => 'ecommerce',
                                            'fk_app_id' => 3,
                                            'portal_id' => Session::get('portal_id'),
                                            'user_id' => Auth::user()->id,
                                    ]);
                 DB::update('update users set ecommerce_logo="'.$res['ecommerce_logo'].'" where id='.Auth::user()->id);
                $alert = 'success';
                $message = 'Data has been saved.';
            } else {
                Credential::where('id',$id)->update([
                                                            'data' => $data,
                                                            'type' => 'ecommerce',
                                                            'portal_id' => Session::get('portal_id'),
                                                            'fk_app_id' => 3,
                                                        ]);
                  DB::update('update users set ecommerce_logo="'.$res['ecommerce_logo'].'" where id='.Auth::user()->id);
                $alert = 'success';
                $message = 'Data has been updated.';
            }
        } catch (Exception $e) {
            $alert = 'warning';
            $message = 'Something went wrong in code.';
        }
        
        $data = Credential::whereType('ecommerce')
                                ->where('portal_id', Session::get('portal_id'))
                                ->value('data');
        $data = unserialize($data);
         // $data['hapi_key'] = Crypt::decryptString($data['hapi_key']);
        $html = view('render.ecommerce.settings', compact('data'));
        
        return view('ecommerce.settings',compact('alert','message','data','html'));
    }

    public function logo_address(){
     
            $portal_id = $_GET['portal_id'];
             // echo "$portal_id";
        	$link = DB::select('select ecommerce_logo from users where portal_id='.$portal_id);
    		$ecommerce_logo =$link[0]->ecommerce_logo;

    		
    	    $object = ['status' => true, 'message' => 'logo exist','data' => $ecommerce_logo ];
        	// $final = json_encode($object);
        	
        	// $final_object=json_encode($object);
        	 
        	 echo "logo_address"."(".json_encode($object).");";
        	 // echo $response;
             exit;

    }



    public function curlAccessHS($method, $array, $content_type = 'array' ) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $array['url']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        switch ($method){
        case "POST":
            curl_setopt($ch, CURLOPT_POST, 1);
            if ($array['data'])
                curl_setopt($ch, CURLOPT_POSTFIELDS,  $array['data'] );
            break;
        case "PUT":
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
            if ($array['data'])
                curl_setopt($ch, CURLOPT_POSTFIELDS,  $array['data']  );
            break;
        default:
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        }

        $headers = array();
        $headers[] = 'Content-Type: application/json';        
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);

        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);

        if($content_type == 'array'){
            $result = json_decode($result); 
        }  
        return $result;
    }
    public function sync_data($auth_id) {

        $auth = User::whereId($auth_id)->first();
        $hapi_key = '';

        if(!empty(DB::table('credentials')->where('type','ecommerce')->whereUserId($auth_id)->value('data'))) {
            //$credential_all = unserialize($auth->credentials->data);
            $credential_all = unserialize(DB::table('credentials')->where('type','ecommerce')->whereUserId($auth_id)->value('data'));
            $hapi_key = Crypt::decryptString($credential_all['hapi_key']);
        }
        if(!empty($hapi_key)){
            Auth::login($auth);
            if($auth->id){
                $args['url'] = 'https://api.hubapi.com/automation/v3/workflows?hapikey='.$hapi_key;
                $result = $this->curlAccessH('GET', $args);
                return view('ecommerce.sync-data',compact('result'));
            }
        }
    }

    public function whmcs_ecommerce(){
        //224529
        //https://api.hubapi.com/integrations/v1/<application id>/timeline/event-types?hapikey=48711f76-ba59-4116-9a14-273895d6ceef&userId=9502143
        // $hapi_key = env('hapi_key');
        $args['url'] = 'https://api.hubapi.com/extensions/ecomm/v2/settings?appId=224529&hapikey=48711f76-ba59-4116-9a14-273895d6ceef';
        $args['data'] = '{
                            "enabled": true,
                            "webhookUri": null,
                            "mappings": {
                            "CONTACT": {
                                "properties": [
                                    {
                                        "externalPropertyName": "firstname",
                                        "hubspotPropertyName": "firstname",
                                        "dataType": "STRING"
                                    },
                                    {
                                        "externalPropertyName": "familyname",
                                        "hubspotPropertyName": "lastname",
                                        "dataType": "STRING"
                                    },
                                    {
                                        "externalPropertyName": "customer_email",
                                        "hubspotPropertyName": "email",
                                        "dataType": "STRING"
                                    },
                                    {
                                        "externalPropertyName": "phone_number",
                                        "hubspotPropertyName": "mobilephone",
                                        "dataType": "STRING"
                                    }
                                ]
                            },
                            "DEAL": {
                                "properties": [
                                    {
                                        "externalPropertyName": "purchase_date",
                                        "hubspotPropertyName": "closedate",
                                        "dataType": "STRING"
                                    },
                                    {
                                        "externalPropertyName": "name",
                                        "hubspotPropertyName": "dealname",
                                        "dataType": "STRING"
                                    },
                                    {
                                        "externalPropertyName": "stage",
                                        "hubspotPropertyName": "dealstage",
                                        "dataType": "STRING"
                                    },
                                    {
                                        "externalPropertyName": "abandoned_cart_url",
                                        "hubspotPropertyName": "ip__ecomm_bride__abandoned_cart_url",
                                        "dataType": "STRING"
                                    }
                                ]
                            },
                            "PRODUCT": {
                                "properties": [
                                    {
                                        "externalPropertyName": "product_description",
                                        "hubspotPropertyName": "description",
                                        "dataType": "STRING"
                                    },
                                    {
                                        "externalPropertyName": "price",
                                        "hubspotPropertyName": "price",
                                        "dataType": "NUMBER"
                                    },
                                    {
                                        "externalPropertyName": "product_name",
                                        "hubspotPropertyName": "name",
                                        "dataType": "STRING"
                                    }
                                ]
                            },
                            "LINE_ITEM": {
                                "properties": [
                                    {
                                        "externalPropertyName": "tax_amount",
                                        "hubspotPropertyName": "tax",
                                        "dataType": "NUMBER"
                                    },
                                    {
                                        "externalPropertyName": "num_items",
                                        "hubspotPropertyName": "quantity",
                                        "dataType": "NUMBER"
                                    },
                                    {
                                        "externalPropertyName": "price",
                                        "hubspotPropertyName": "price",
                                        "dataType": "NUMBER"
                                    },
                                    {
                                        "externalPropertyName": "discount_amount",
                                        "hubspotPropertyName": "discount",
                                        "dataType": "NUMBER"
                                    }
                                ]
                            }
                            }
                        }';
        // $result = $this->curlAccessHS('PUT', $args);
        //$result = $this->curlAccessHS('PUT', $args);
        echo '<pre>';
        print_r($result);
        die;
        //$this->refresh_token();

        

        // $args['url'] = 'https://api.hubapi.com/oauth/v1/access-tokens/'.Auth::user()->auth_token;
        // $result = $this->curlAccessH('GET', $args);
        // print_r($result);
        // die;

        $args['url'] = "https://api.hubapi.com/crm-objects/v1/objects/products/paged?hapikey=ebd3b34f-c079-42d6-9acd-ab262aa8594f&properties=name&properties=description&properties=embed_code";
        $result = $this->curlAccessHS('GET', $args);
        echo '<pre>';
        print_r($result);
        die;
        
        $args['url']  = 'https://api.hubapi.com/extensions/ecomm/v2/stores';
        $args['data'] = '{
                            "id"       : "ecommercebridge-test-store",
                            "label"    : "Ecommerce Bridge Test Store",
                            "adminUri" : "ecommercebridge-test-store.myshopify.com"
                        }';
        
        $args['token'] = Auth::user()->auth_token;
        $result = $this->curlAccessH('PUT', $args);
        print_r($result);
        die;
    }
 
    public function get_product_list_workflow($auth_id) {
        $users = User::where('id', $auth_id)->first();
        $hapi_key = '';
        if(!empty(DB::table('credentials')->where('type','ecommerce')->whereUserId($auth_id)->value('data'))){
            $credential_all = unserialize(DB::table('credentials')->where('type','ecommerce')->whereUserId($auth_id)->value('data'));
            $hapi_key = Crypt::decryptString($credential_all['hapi_key']);
        }
        if(!empty($hapi_key)){
            Auth::login($users);

            $args['url'] = 'https://api.hubapi.com/automation/v3/workflows?hapikey='.$hapi_key;
            $url = url('get-product-list-hub').'/'.$auth_id;
            $args['data'] = '{
                            "name": "Workflow for product Sync",
                            "type": "DRIP_DELAY",
                            "onlyEnrollsManually": true,
                            "actions": [
                                {
                                    "type": "DELAY",
                                    "delayMillis": 360000
                                },
                                {
                                    "type": "WEBHOOK",
                                    "url": "'.$url.'",
                                    "method": "POST"
                                }
                            ]
                        }';

            $result = $this->curlAccessH('POST', $args);
            //return view('ecommerce.sync-data',compact('result'));
            return ['status'=> true, 'message' => 'Workflow has been created.'];
        } else {
            return ['status'=> false, 'message' => 'Hubspot key is required.'];
        }
    }

     public function templates_generation($token)
     {

                       
                 //cart_page
        		

    			 $curl = curl_init();

    			 curl_setopt_array($curl, array(
    			  CURLOPT_URL => 'https://api.hubapi.com/content/api/v2/templates',
    			  CURLOPT_RETURNTRANSFER => true,
    			  CURLOPT_ENCODING => '',
    			  CURLOPT_MAXREDIRS => 10,
    			  CURLOPT_TIMEOUT => 0,
    			  CURLOPT_FOLLOWLOCATION => true,
    			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    			  CURLOPT_CUSTOMREQUEST => 'POST',
    			  CURLOPT_POSTFIELDS =>'{
    			    "category_id": 1,
    			    "folder": "example",
    			    "is_available_for_new_content": "False",
    			    "template_type": 4,        
    			    "path":"webdew/cart",
    			    "source":"<!--\\n    templateType: page\\n    isAvailableForNewContent: true\\n-->\\n<!doctype html>\\n<html>\\n  <head>\\n    <meta charset=\\"utf-8\\">\\n    <title>{{ content.html_title }}</title>\\n    <meta name=\\"viewport\\" content= \\"width=device-width, initial-scale=1.0\\">\\n       <link rel=\\"stylesheet\\" href=\\"https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css\\">\\n        <link rel=\\"stylesheet\\" href=\\"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css\\">\\n        <link href=\\"https://fonts.googleapis.com/css2?family=DM+Sans:wght@500&display=swap\\" rel=\\"stylesheet\\">\\n        <link rel=\\"stylesheet\\" href=\\"https://pro.fontawesome.com/releases/v5.10.0/css/all.css\\">\\n        <script src=\\"https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js\\"></script>\\n        <script src=\\"https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js\\"></script>\\n    <meta name=\\"description\\" content=\\"{{ content.meta_description }}\\">\\n    {{ standard_header_includes }}\\n  </head>\\n  \\n{% require_css %}\\n<style> \\n  @import url(\'https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap\');\\n\\nhtml {\\n  height: 100%;\\n  box-sizing: border-box;\\n}\\nbody {\\n  position: relative;\\n  padding-bottom: 6rem;\\n  min-height: 100%;\\n  \\n}\\n\\n  /*------- header---------*/\\n\\n{% set brand_color = \\"#00a9fa\\" %}\\n\\nimg{ max-width: 100%; }\\n.ecm_header{ width: 100%; display: inline-block; position: relative;  padding: 15px 0; background-color: #ffffff; box-shadow: 0 0px 10px #eee; }\\n.cms_head{ display: flex; flex-wrap: wrap; justify-content: space-between; align-items: center; }\\n.cms_head .logo{ max-width: 180px; }\\n.cms_head .logo img {   width: 100%;}\\n\\n.ecm_product_warp{ padding: 50px 0; }\\n.ecm_product_row{\\n  display: -ms-flexbox;\\n  display: flex;\\n  -ms-flex-wrap: wrap;\\n  flex-wrap: wrap;\\n\\n}\\n\\n.ecm_product_col-3 {\\n -ms-flex: 0 0 25%;\\n  flex: 0 0 25%;\\n  max-width: 25%;\\n  padding: 0 15px;\\n  margin-bottom: 30px;\\n}\\n\\n.ecm_product_img{ background-color: #efefef; padding: 15px; position: relative; }\\na.ecm_view_detail {\\n    display: none;\\n    background-color: {{brand_color}} ;\\n    color: #ffffff;\\n    position: absolute;\\n    bottom: 0;\\n    left: 0;\\n    width: 100%;\\n    padding: 10px 0;\\n    text-align: center;\\n    text-decoration: none;\\n}\\n.ecm_product_box:hover a.ecm_view_detail { display: block; }\\n\\n.ecm_product_detail{ padding: 10px 0; text-align: center; }\\n\\nspan#cart_item {\\n    cursor: pointer;\\n}\\n\\n.ecm_product_list {\\n    /*display: none;*/\\n    position: fixed;\\n    top: 0;\\n    right: -320px;\\n    width: 320px;\\n    background-color: #fff;\\n    z-index: 99;\\n    height: 100%;\\n    overflow: auto;\\n    padding: 25px 0 0 0;\\n    border-left: 1px solid #ccc;    \\n      -webkit-transition: all 1s; \\n      transition: all 1s; \\n      opacity:0;\\n      visibility: hidden;\\n} \\n\\n.ecm_header.menu_fix  .ecm_product_list{\\n      right: 0;\\n      /*display: block;*/\\n      -webkit-transition: all 1s; \\n      transition: all 1s; \\n      visibility: visible;\\n      opacity: 1;\\n}\\n\\ndiv#close_cart {\\n    display: flex;\\n    text-align: right;\\n    justify-content: flex-end;\\n    align-items: center;\\n    margin: 0 25px 10px 0;\\n    border-radius: 2px;\\n       color: {{brand_color}};\\n       cursor: pointer;\\n}\\ndiv#close_cart i.fas {\\n    font-size: 20px;\\n    color: {{brand_color}};\\n    margin: 0 4px 0 0;\\n}\\n.ecm_product_list ul{ margin: 0; padding: 0; }\\n.ecm_product_list ul li {\\n    list-style: none;\\n    padding: 12px 25px;\\n    border-bottom: 1px solid #ccc;\\n}\\n\\n.product_list_row{ \\n   display: flex; \\n    flex-wrap: wrap; \\n\\n    justify-content: space-between;\\n}\\n.ecm_description h4 {\\n    margin: 0;\\n    padding: 0;\\n    font-size: 15px;\\n}\\n\\n.product_thum {\\n    width: 60px;\\n}\\n\\n\\n.eca_product_cart span {cursor:pointer; }\\n.eca_product_cart .number{\\n  margin:5px 0 0 0; text-align: center;\\n }\\n    .eca_product_cart .minus, \\n    .eca_product_cart .plus{\\n      width:20px;\\n      /*height:20px;*/\\n      background:#f2f2f2;\\n      border-radius:4px;\\n      padding:3px 5px 3px 5px;\\n      border:1px solid #ddd;\\n      display: inline-block;\\n      vertical-align: middle;\\n      text-align: center;\\n    }\\n    .eca_product_cart td input{\\n      height:30px;\\n      width: 40px;\\n      text-align: center;\\n      font-size: 15px;\\n      border:1px solid #ddd;\\n      border-radius:4px;\\n      display: inline-block;\\n      vertical-align: middle;\\n      \\n\\n}\\n.ecm_product_list > h4 {\\n    font-size: 14px;\\n    padding: 10px 15px 15px;\\n    border-bottom: 1px solid #ccc;\\n    font-weight: 700;\\n}\\n.ecm_close i.fas.fa-trash-alt{ color: {{brand_color}}; cursor: pointer; }\\n.total_cart {\\n    margin: 15px 0;\\n    width: 100%;\\n}\\n.total {\\n    display: flex;\\n    justify-content: space-between;\\n    padding: 0 15px;\\n}\\n.total span.label{ color: #333333; }\\n.total span.label span {\\n    font-size: 15px;\\n}\\n\\n.total span.money{ color: {{brand_color}}; }\\n.btn-checkout {\\n    padding: 0 15px;\\n    width: 100%;\\n    display: inline-block;\\n    text-align: center;\\n}\\n\\n.btn-checkout a{ \\n   background-color: {{brand_color}};\\n   color: #ffffff;\\n   padding: 12px 0;\\n   width: 100%;\\n   display: inline-block;\\n   border-radius: 30px;\\n   text-decoration: none;\\n   border:2px solid transparent;\\n  }\\n\\n  .btn-checkout a:hover{ \\n   color: {{brand_color}};  \\n   border:2px solid {{brand_color}};\\n   background-color: #ffffff;\\n  }\\n\\n\\n\\n.btn-view-cart {\\n    padding: 0 15px;\\n    width: 100%;\\n    display: inline-block;\\n    text-align: center;\\n    margin-top: 15px;\\n}\\n\\n.btn-view-cart a{ \\n   color: {{brand_color}};\\n   padding: 12px 0;\\n   width: 100%;\\n   display: inline-block;\\n   border-radius: 30px;\\n   border:2px solid {{brand_color}};\\n    text-decoration: none;\\n  }\\n.btn-view-cart a:hover{ background-color: {{brand_color}}; color: #ffffff; }\\n  /*------- header---------*/\\ntable{\\nfont-family: \'Montserrat\', sans-serif;\\n}\\nbody{\\nmargin:0;\\n}\\nfigure.product-img-wrap {\\n    width: 90px;\\n    height: 90px;\\n    margin: 0 40px 0 0;\\n}\\nfigure.product-img-wrap img{\\n    width:100%;\\n}\\n.product-img-name{\\ndisplay:flex;\\n  justify-content: left;\\n    align-items: center;\\n}\\n.product-name h5 {\\n    font-size: 18px;\\n    margin: 0 0 5px;\\n    color:#333;\\n}\\n.products-cart-wrap table th {\\n    font-size: 16px;\\n    text-transform: uppercase;\\n    color: #7a7a7a;\\n    text-align: left;\\n}\\n.products-cart-wrap table td{\\n     font-size: 16px;\\n    color: #7a7a7a;\\n    text-align: left;\\n}\\n input#quantity {\\n    width: 40px;\\n    height: 25px;\\n    outline: none;\\n    box-shadow: none;\\n    border: none;\\n    border: 1px solid #7a7a7a33;\\n}\\n.products-cart-wrap table tbody tr {\\n    border: 1px solid #7a7a7a33;\\n}\\n\\n.product-name a , .product-name button , .top-heading a{\\n    color: #7a7a7a;\\n    background:none;\\n    padding:0;\\n    border:none;\\n}\\n.product-name a:hover{\\n   color:#333;\\n}\\n.products-cart-wrap table {\\n    border-spacing: 0;\\n}\\n.top-heading h1 {\\n    font-size: 30px;\\n    margin: 0 0 10px 0;\\n}\\n.top-heading {\\n    text-align: center;\\n    padding: 30px 0 40px;\\n}\\n.total-checkout {\\n    text-align: right;\\n    padding: 30px 0;\\n}\\n.sub-total-checkout h3 {\\n    font-size: 22px;\\n    margin: 0;\\n}\\n.sub-total-checkout {\\n    display: flex;\\n    justify-content: space-between;\\n    width: 24%;\\n    margin: 0 0 0 auto;\\n    align-items: center;\\n    color:#7a7a7a;\\n}\\n.sub-total-checkout p {\\n    margin: 0;\\n    font-size: 16px;\\n}\\n.total-checkout h5 {\\n    font-size: 16px;\\n    margin: 0;\\n    padding: 20px 0 40px;\\n       color:#7a7a7a;\\n}\\n.checkout-button-custom a {\\n    font-size: 15px;\\n    text-transform: uppercase;\\n    outline: none;\\n    text-decoration: none !important;\\n    display: inline-block;\\n    background: #00A9FA;\\n    padding: 12px 15px;\\n    color: #fff;\\n    transition:0.3s;\\n}\\n.checkout-button-custom a:hover {\\n     color: #fff;\\n    box-shadow: 0 0 2px 0 #7a7a7a;\\n}\\n.checkout-button-custom button {\\n    font-size: 15px;\\n    text-transform: uppercase;\\n    outline: none;\\n    text-decoration: none;\\n    display: inline-block;\\n    background: #00A9FA;\\n    padding: 12px 15px;\\n    color: #fff;\\n    transition:0.3s;\\n}\\n.checkout-button-custom button:hover {\\n    box-shadow: 0 0 2px 0 #7a7a7a;\\n}\\n.container {\\n    max-width: 1200px;\\n    /*min-width: 800px;*/\\n    margin: 0 auto;\\n     padding:0 15px;\\n     width: 100%;\\n}\\n\\n\\nlabel{\\n  margin-bottom: 5px!important;\\n  margin-top: 5px!important;\\n}\\n\\n\\n/*cart*/\\n.eca_cart_wrap h2{ text-align: center; font-weight: 700; margin-bottom: 45px; }\\n\\n.eca_container_wrap{ padding: 50px 0; width: 100%; display: inline-block; }\\n.eca_product_cart{ float: left; width: 100%; overflow: auto; }\\n\\n.eca_product_cart table{ float: left; width: 100%; }\\n.eca_product_cart table tr th{ text-transform: uppercase; font-weight: 700; font-size: 15px; background-color: #f8f8f8;  }\\n.eca_product_cart table tr td{    }\\n.eca_product_cart table tr th,\\n.eca_product_cart table tr td{\\n     border: 1px solid #ddd; \\n     padding: 13px;\\n\\n}\\n.eca_calculated {   width: 100%;  float: left;}\\n.eca_checkout_wrap {  max-width: 330px;  float: right; width: 100%; }\\n\\n.eca_checkout_wrap h4{   display: block;  font-size: 16px;  font-weight: 600;   margin-bottom: 20px;}\\n.eca_checkout_wrap a{ \\n  background-color: {{brand_color}};\\n    color: #ffffff;\\n    padding: 8px 20px;\\n    display: inline-block;\\n    border-radius: 30px;\\n    text-decoration: none;\\n    border: 2px solid transparent;\\n    text-align: center;\\n}\\n\\n.eca_checkout_wrap a:hover{ \\n   color: {{brand_color}};  \\n   border:2px solid {{brand_color}};\\n   background-color: #ffffff;\\n}\\n.back_arrow_btn {\\n    padding: 20px 15px;\\n    width: 100%;\\n    display: inline-block;\\n}\\n\\n.back_arrow_btn a{\\n   color: {{brand_color}}; text-decoration: none;\\n} \\n.eca_product_thums { text-align: center; }\\n.eca_product_thums img { max-width: 135px; }\\nspan.eca_total_price {   font-size: 20px;}\\n\\n.close_eca i.fas.fa-times{ cursor: pointer; color: {{ brand_color }} }\\n/*cart*/\\n\\n\\n/*footer*/\\n.ecm_footer{ \\n  position: absolute;\\n  right: 0;\\n  bottom: 0;\\n  left: 0;\\n  background-color: {{ brand_color }};     padding: 15px 0; \\n}\\n.ecm_footer_copy_right {  text-align: center;}\\n.ecm_footer_copy_right p { color: #ffffff;     margin: 0;}\\n\\n\\n\\n</style>\\n{% end_require_css %}\\n  <body>\\n\\n\\n    <div class=\\"ecm_header\\">\\n    <div class=\\"container\\">\\n        <div class=\\"cms_head\\">\\n            <div class=\\"logo\\">\\n                <a href=\\"#\\">\\n                   <img id=\\"portal_id\\" portal_id=\\"{{ portal_id }}\\" src=\\"\\" alt=\\"Brand-logo\\">\\n                </a>\\n            </div>\\n          \\n        </div>\\n    </div>\\n</div>\\n\\n\\n   \\n<!-- cart start -->\\n<div class=\\"eca_container_wrap\\">\\n    <div class=\\"container\\">\\n      <div class=\\"back_arrow_btn\\">\\n             <a href=\\"{{ get_asset_url(\'/webdew/products\') }}\\"><i class=\\"fas fa-long-arrow-alt-left\\"></i> Back</a>\\n          </div>\\n       <div class=\\"eca_row_fluid\\">\\n           <div class=\\"eca_cart_wrap\\">\\n               <div class=\\"cart_head_t\\"><h2>Your Cart</h2></div>\\n               <div class=\\"eca_product_cart\\">\\n                  <table>\\n                    <thead>\\n                        <tr>\\n                            <th width=\\"20%\\" >Product</th>\\n                            <th width=\\"35%\\" >Description</th>\\n                            <th width=\\"10%\\" >Price</th>\\n                            <th width=\\"15%\\" style=\\"text-align: center;\\" >quantity</th> \\n                            <th width=\\"10%\\" >Total</th>\\n                            <th width=\\"10%\\" > Remove </th>\\n                        </tr>\\n                    </thead>\\n                      <tbody id=\\"product_item\\">\\n               \\n<!--                         <tr id=\\"empty_cart\\"><td colspan=\\"10\\"><div class=\'eca_product_thums\'><h3> Your Cart is Empty.</h3><p>please go <a href=\\"#\\">back</a> and add items.</p></div></td></tr> -->\\n                      </tbody>\\n                    <tbody>\\n                       <tr>\\n                            <td colspan=\\"3\\" style=\\"border:0\\"></td>\\n                              <td style=\\"border:0\\">\\n                                <h4 class=\\"ecm_sub_total\\">Sub Total</h4>\\n                              </td>\\n                              <td style=\\"border:0\\">\\n                               <span class=\\"eca_total_price money\\"></span>\\n                             </td>\\n                             <td style=\\"border:0\\">\\n                                  <div class=\\"eca_calculated\\">\\n                            <div class=\\"eca_checkout_wrap\\">\\n                                 <a href=\\"{{ get_asset_url(\'/webdew/checkout\') }}\\" class=\\"eca_checkout_wrap\\">Checkout</a>\\n                              </div>\\n                            </div>\\n                             </td>\\n                          </tr>\\n                    </tbody>\\n                  </table>\\n\\n                  <div class=\\"eca_calculated\\">\\n                      <div class=\\"eca_checkout_wrap\\">\\n                        <h4>Taxes and shipping calculated at checkout</h4>\\n                      </div>\\n                  </div>\\n               </div>\\n           </div>\\n       </div>\\n    </div>\\n</div>\\n\\n<!-- End cart -->\\n\\n   <div class=\\"ecm_footer\\">\\n       <div class=\\"container\\">\\n          <div class=\\"ecm_footer_copy_right\\">\\n             <p>Copyright 2021 • All Rights Reserved • Terms of Service • Privacy Policy</p>\\n          </div>\\n       </div>\\n    </div>\\n\\n\\n<script src=\\"https://static.hsappstatic.net/jquery-libs/static-1.1/jquery/jquery-1.7.1.js\\"></script>\\n<script src=\\"https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js\\"></script>\\n    \\n      <script type=\\"text/javascript\\">\\n            function roundFloat(num,dec){\\n            var d = 1;\\n            for (var i=0; i<dec; i++){\\n                d += \\"0\\";\\n            }\\n            return Math.round(num * d) / d;\\n        }\\n\\n        \\n        \\n    function logo_address(object){\\n       $(\\"#portal_id\\").attr(\\"src\\",object.data);\\n        }\\n      \\n   \\n    $(document).ready(function() {\\n      var portal_id = $(\\"#portal_id\\").attr(\\"portal_id\\");\\n      var base_url = \\"https://apps.webdew.com/hubspot\\";\\n    $.ajax({\\n        url:\\"https://apps.webdew.com/hubspot/apps/ecommerce/logo?&portal_id=\\"+portal_id,\\n        dataType: \'jsonp\',\\n        async:true\\n        });\\n         \\n  });\\n     \\n        \\n        \\n        \\n    $(document).ready(function() {\\n      \\n      //this function is used to subtract cart items\\n      $(document).on(\'click\',\'.minus\', function()\\n      {     \\n            var $input = $(this).parent().find(\'input\');\\n            var price  = $(this).attr(\'minus_price\');//getting price from button\\n            var  id    = $(this).attr(\'prod_minus\');//getting product id from button\\n            var items = localStorage.getItem(\'items\');\\n        \\n          if($input.val() > 1 ){\\n          \\n           //converting price into number format\\n            var str = String(price);\\n            str = str.replace(\\",\\", \\"\\");\\n            str = str.replace(\\"$\\", \\"\\");\\n           //converting price into number format\\n           \\n            var cart_total = localStorage.getItem(\'total\');//getting total from local storage\\n            var total = parseFloat(cart_total);\\n            \\n            total -= parseFloat(price);//subtracting product price from total\\n            total = roundFloat(total,4);\\n            localStorage.setItem(\\"total\\",total);//setting total in local storage\\n            $(\\".money\\").text(\\" $ \\"+total);//reflecting back into cart\\n        \\n            let product_detail = localStorage.getItem(id);\\n            var product = JSON.parse(product_detail);\\n            var product_id = product.product_id;\\n            product.item -= 1;\\n            \\n            product.total -= parseFloat(str); \\n            total = roundFloat(product.total,4);\\n            localStorage.setItem(id,JSON.stringify(product));\\n            $(\\".product_total\\"+product_id).text(\\"$\\"+product.total);\\n        \\n             var items = localStorage.getItem(\\"items\\");\\n            var int_items = parseInt(items);\\n            int_items -= 1;\\n            localStorage.setItem(\\"items\\",int_items);\\n            $(\\"#cart_count\\").text(int_items);\\n        \\n            var count = parseInt($input.val()) - 1;\\n            count = count < 1 ? 1 : count;\\n            $input.val(count);\\n            $input.change();\\n          }\\n                    \\n            return false;\\n      });\\n      \\n         //this function is used to add cart items into cart\\n       $(document).on(\'click\', \'.plus\', function()\\n      {     \\n            var $input = $(this).parent().find(\'input\');\\n            var price = $(this).attr(\'add_price\');// this price is in dollar format\\n            var  id= $(this).attr(\'prod_add\');\\n           \\n            //converting it to normal number\\n            var str = String(price);\\n            str = str.replace(\\",\\", \\"\\");\\n            str = str.replace(\\"$\\", \\"\\");\\n            //converting it to normal number\\n            \\n            var cart_total = localStorage.getItem(\'total\');\\n            var total = parseFloat(cart_total);\\n            \\n            total += parseFloat(price);\\n             total = roundFloat(total,4);\\n            localStorage.setItem(\\"total\\",total);\\n            $(\\".money\\").text(\\" $ \\"+total);\\n         \\n             let product_detail = localStorage.getItem(id);\\n             var product = JSON.parse(product_detail);\\n             var product_id = product.product_id;\\n             product.item += 1;\\n             product.total += parseFloat(str);\\n            total = roundFloat(product.total,4);\\n            localStorage.setItem(id,JSON.stringify(product));\\n             $(\\".product_total\\"+product_id).text(\\"$\\"+product.total);\\n         \\n            var items = localStorage.getItem(\\"items\\");\\n            var int_items = parseInt(items);\\n            int_items += 1;\\n            localStorage.setItem(\\"items\\",int_items);\\n            $(\\"#cart_count\\").text(int_items);\\n         \\n            $input.val(parseInt($input.val()) + 1);\\n            $input.change();\\n            return false;\\n      });\\n      \\n             $(document).on(\'click\', \'#delete\', function()\\n      {     \\n            \\n            var item_id = $(this).attr(\'item_id\');\\n            $(\\".list_id\\"+item_id).remove();\\n            \\n             \\n           \\n            var product_detail = localStorage.getItem(item_id);\\n            var product = JSON.parse(product_detail);\\n            \\n            var item_price = product.total;\\n\\n            var cart_total = localStorage.getItem(\'total\');\\n            var total = parseFloat(cart_total);\\n            \\n            total -= parseFloat(item_price);\\n            total = roundFloat(total,4);\\n            localStorage.setItem(\\"total\\",total);\\n            $(\\".money\\").text(\\" $ \\"+total);\\n             \\n           \\n               \\n               \\n            //storing item count in local storage\\n            var items = localStorage.getItem(\\"items\\");\\n            var int_items = parseInt(items);\\n            \\n            var items = localStorage.getItem(item_id);\\n            var items_object = JSON.parse(items);\\n            var product_items = items_object.item;\\n\\n               \\n            int_items -= parseInt(product_items);\\n            localStorage.setItem(\\"items\\",int_items);\\n            //storing item count in local storage\\n        \\n             $(\\"#cart_count\\").text(int_items);\\n             \\n             localStorage.removeItem(item_id);\\n             var item_count= localStorage.getItem(\'items\');               \\n         \\n            if(item_count==0)\\n              {\\n                $(\\"#product_item\\").append(\\"<tr><td colspan=\'10\'><div class=\'eca_product_thums\'><h3> Your Cart is Empty.</h3><p>please go <a href=\'{{ get_asset_url(\'/webdew/products\') }}\'>back</a> and add items.</p></div></td></tr>\\");\\n              }\\n            return false;\\n      });\\n      \\n    });\\n      \\n      $(document).on(\'click\', \'.product_details\', function(e)\\n      {     e.preventDefault();\\n            var product_id = $(this).attr(\'prod_id\');\\n            var product_price = $(this).attr(\'prod_price\');\\n            var product_name = $(this).attr(\'prod_name\');\\n            var href = $(this).attr(\'href\');\\n            var product_description =$(this).attr(\'prod_desc\');\\n            var product_info={\'product_id\':product_id,\'product_name\':product_name,\'product_price\':product_price,\'product_description\':product_description};\\n            localStorage.setItem(\'product_info\',JSON.stringify(product_info));\\n            window.location.replace(href);\\n          \\n      });\\n      \\n      \\n      \\n      \\n          //add to cart button funcitonality\\n          $(document).ready(function() {\\n          //setting cart total and items to 0\\n          if(localStorage.getItem(\'items\'))\\n          {  \\n           var item_count= localStorage.getItem(\'items\');\\n         \\n            if(item_count==0)\\n              {\\n                $(\\"#product_item\\").append(\\"<tr><td colspan=\'10\'><div class=\'eca_product_thums\'><h3> Your Cart is Empty.</h3><p>please go <a href=\'{{ get_asset_url(\'/webdew/products\') }}\'>back</a> and add items.</p></div></td></tr>\\");\\n              }\\n                let nextValue;\\n                    for (let i = 0; i < localStorage.length; i++)\\n                    {\\n                        nextValue = localStorage.key(i);\\n                          if(nextValue != \\"total\\" && nextValue != \\"items\\" && nextValue !=\\"product_info\\" &&nextValue != \\"__paypal_storage__\\" )\\n                          {\\n                              var item = localStorage.getItem(nextValue);\\n                                \\n                                  var items_object = JSON.parse(item);\\n                                  var product_id = nextValue; \\n                                  var product_name = items_object.product_name;\\n                                  var product_price = items_object.product_price;\\n                                  var product_total = items_object.total;\\n//                                   var product_rate = items_object.total;\\n                            \\n                            \\n                                var str = String(product_price);\\n                                str = str.replace(\\",\\", \\"\\");        \\n                                str = str.replace(\\"$\\", \\"\\");\\n                                var product_rate = parseFloat(str);\\n                                var image_path = items_object.image_path;\\n//                                $(\\"#product_item\\").append(\\"<li class=\'list_id\\"+product_id+\\"\'><div class=\'product_list_row\'><div class=\'product_thum\'><img src=\'https://f.hubspotusercontent40.net/hubfs/7323845/product.png\\\\\' alt=\'\'></div><div class=\'ecm_description\'><h4>\\"+product_name+\\"</h4><span class=\'price\'>\\"+product_price+\\"</span><div class=\'number\'><span class=\'minus\' minus_price=\\"+product_rate+\\" prod_minus=\\"+product_id+\\">-</span><input type=\'text\' value=\\"+items_object.item+\\" /><span class=\'plus\' add_price=\\"+product_rate+\\" prod_add=\\"+product_id+\\">+</span></div></div><div class=\'ecm_close\'><i class=\'fas fa-trash-alt\' item_id=\\"+product_id+\\" id=\'delete\'></i></div></div></li>\\");\\n                          \\n                            \\n                           $(\\"#product_item\\").append(\\"<tr class=\'list_id\\"+product_id+\\"\'><td><div class=\'eca_product_thums\'><img src=\'\\"+image_path+\\"\' alt=\'\'></div></td><td><div><h4>\\"+product_name+\\"</h4></div></td><td class=\'price\'>\\"+product_price+\\"</td><td><div class=\'number\'><span class=\'minus\'  minus_price=\\"+product_rate+\\" prod_minus=\\"+product_id+\\">-</span><input type=\'text\' disabled value=\\"+items_object.item+\\" /><span class=\'plus\' add_price=\\"+product_rate+\\" prod_add=\\"+product_id+\\">+</span></div></td><td class=\'product_total\\"+product_id+\\"\'>$\\"+product_total+\\"</td><td align=\'center\'><div class=\'close_eca\'><i class=\'fas fa-times\' item_id=\\"+product_id+\\" id=\'delete\'></i></div></td></tr>\\");\\n                            \\n                            \\n                            \\n                            \\n                                \\n                          }\\n                    } \\n                        var total = localStorage.getItem(\'total\');\\n                        $(\\".money\\").text(\\"$\\"+total);\\n\\n                        var items = localStorage.getItem(\'items\');\\n                        $(\\"#cart_count\\").text(items);\\n            \\n            \\n          }\\n            else\\n            {\\n              localStorage.setItem(\\"total\\",0);\\n              localStorage.setItem(\\"items\\",0);\\n            }\\n           \\n           \\n         \\n         \\n    });\\n    \\n    </script>\\n\\n    <script type=\\"text/javascript\\">\\n\\n\\n    $(document).ready(function() {\\n\\n        $(\\"#cart_item\\").click(function() {\\n            $(\'.ecm_header\').addClass(\'menu_fix\');\\n        });\\n\\n        $(\\"#close_cart\\").click(function() {\\n            $(\'.ecm_header\').removeClass(\'menu_fix\');\\n        });\\n    });\\n\\n    </script>\\n\\n\\n\\n    <!--   E-cart Module end  -->  \\n    {{ standard_footer_includes }}\\n  </body>\\n</html>\\n"
    			    }',
    			  CURLOPT_HTTPHEADER => array(
    			   'Authorization: Bearer '.$token, 
    			    'Content-Type: application/json',
    			    'Cookie: __cfduid=dbee73ed9d0d3ee23fabe7672fb9b848a1614580870'
    			  ),
    			 ));

    			 $response = curl_exec($curl);

    			 curl_close($curl);

                 //cart page ends

                 //product page starts
    		

    			 $curl = curl_init();

    			 curl_setopt_array($curl, array(
    			  CURLOPT_URL => 'https://api.hubapi.com/content/api/v2/templates',
    			  CURLOPT_RETURNTRANSFER => true,
    			  CURLOPT_ENCODING => '',
    			  CURLOPT_MAXREDIRS => 10,
    			  CURLOPT_TIMEOUT => 0,
    			  CURLOPT_FOLLOWLOCATION => true,
    			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    			  CURLOPT_CUSTOMREQUEST => 'POST',
    			  CURLOPT_POSTFIELDS =>'{
    			    "category_id": 1,
    			    "folder": "example",
    			    "is_available_for_new_content": "False",
    			    "template_type": 4,        
    			    "path":"webdew/products",
    			    "source":"<!--\\n    templateType: page\\n    isAvailableForNewContent: true\\n-->\\n<!doctype html>\\n<html>\\n\\n<head>\\n    <meta charset=\\"utf-8\\">\\n    <title>{{ content.html_title }}</title>\\n    <link rel=\\"stylesheet\\" href=\\"https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css\\">\\n    <link rel=\\"stylesheet\\" href=\\"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css\\">\\n    <link href=\\"https://fonts.googleapis.com/css2?family=DM+Sans:wght@500&display=swap\\" rel=\\"stylesheet\\">\\n    <script src=\\"https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js\\"></script>\\n    <script src=\\"https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js\\"></script>\\n   \\n    <link rel=\\"stylesheet\\" href=\\"https://pro.fontawesome.com/releases/v5.10.0/css/all.css\\">\\n    <meta name=\\"description\\" content=\\"{{ content.meta_description }}\\">\\n    {{ standard_header_includes }}\\n</head>\\n{% require_css %}\\n<style>\\n\\nhtml {\\n  height: 100%;\\n  box-sizing: border-box;\\n}\\nbody {\\n  position: relative;\\n  padding-bottom: 6rem;\\n  min-height: 100%;\\n  \\n} \\n    @import url(\'https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap\');\\n\\n{% set brand_color = \\"#00a9fa\\" %}\\n\\nimg{ max-width: 100%; }\\n.ecm_header{ width: 100%; display: inline-block; position: relative;  padding: 15px 0; }\\n.cms_head{ display: flex; flex-wrap: wrap; justify-content: space-between; align-items: center; }\\n.cms_head .logo{ max-width: 180px; }\\n.cms_head .logo img {   width: 100%;}\\n\\n.ecm_product_warp{ padding: 50px 0; }\\n.ecm_product_row{\\n  display: -ms-flexbox;\\n  display: flex;\\n  -ms-flex-wrap: wrap;\\n  flex-wrap: wrap;\\n\\n}\\n\\n.ecm_product_col-3 {\\n -ms-flex: 0 0 25%;\\n  flex: 0 0 25%;\\n  max-width: 25%;\\n  padding: 0 15px;\\n  margin-bottom: 30px;\\n}\\n\\n.ecm_product_img{ background-color: #efefef; padding: 15px; position: relative; }\\na.ecm_view_detail {\\n    display: none;\\n    background-color: {{brand_color}} ;\\n    color: #ffffff;\\n    position: absolute;\\n    bottom: 0;\\n    left: 0;\\n    width: 100%;\\n    padding: 10px 0;\\n    text-align: center;\\n    text-decoration: none;\\n}\\n.ecm_product_box:hover a.ecm_view_detail { display: block; }\\n\\n.ecm_product_detail{ padding: 10px 0; text-align: center; }\\n\\nspan#cart_item {\\n    cursor: pointer;\\n}\\n\\n.ecm_product_list {\\n    /*display: none;*/\\n    position: fixed;\\n    top: 0;\\n    right: -320px;\\n    width: 320px;\\n    background-color: #fff;\\n    z-index: 99;\\n    height: 100%;\\n    overflow: auto;\\n    padding: 25px 0 0 0;\\n    border-left: 1px solid #ccc;    \\n      -webkit-transition: all 1s; \\n      transition: all 1s; \\n      opacity:0;\\n      visibility: hidden;\\n} \\n\\n.ecm_header.menu_fix  .ecm_product_list{\\n      right: 0;\\n      /*display: block;*/\\n      -webkit-transition: all 1s; \\n      transition: all 1s; \\n      visibility: visible;\\n      opacity: 1;\\n}\\n\\ndiv#close_cart {\\n    display: flex;\\n    text-align: right;\\n    justify-content: flex-end;\\n    align-items: center;\\n    margin: 0 25px 10px 0;\\n    border-radius: 2px;\\n       color: {{brand_color}};\\n       cursor: pointer;\\n}\\ndiv#close_cart i.fas {\\n    font-size: 20px;\\n    color: {{brand_color}};\\n    margin: 0 4px 0 0;\\n}\\n.ecm_product_list ul{ margin: 0; padding: 0; }\\n.ecm_product_list ul li {\\n    list-style: none;\\n    padding: 12px 25px;\\n    border-bottom: 1px solid #ccc;\\n}\\n\\n.product_list_row{ \\n   display: flex; \\n    flex-wrap: wrap; \\n\\n    justify-content: space-between;\\n}\\n.ecm_description h4 {\\n    margin: 0;\\n    padding: 0;\\n    font-size: 15px;\\n}\\n\\n.product_thum {\\n    width: 60px;\\n}\\n\\n\\n.product_list_row span {cursor:pointer; }\\n.product_list_row .number{\\n  margin:5px 0 0 0;\\n }\\n    .product_list_row .minus, .product_list_row .plus{\\n      width:20px;\\n      /*height:20px;*/\\n      background:#f2f2f2;\\n      border-radius:4px;\\n      padding:3px 5px 3px 5px;\\n      border:1px solid #ddd;\\n      display: inline-block;\\n      vertical-align: middle;\\n      text-align: center;\\n    }\\n    .product_list_row input{\\n      height:30px;\\n      width: 40px;\\n      text-align: center;\\n      font-size: 15px;\\n      border:1px solid #ddd;\\n      border-radius:4px;\\n      display: inline-block;\\n      vertical-align: middle;\\n      \\n\\n}\\n.ecm_product_list > h4 {\\n    font-size: 14px;\\n    padding: 10px 15px 15px;\\n    border-bottom: 1px solid #ccc;\\n    font-weight: 700;\\n}\\n.ecm_close i.fas.fa-trash-alt{ color: {{brand_color}}; cursor: pointer; }\\n.total_cart {\\n    margin: 15px 0;\\n    width: 100%;\\n}\\n.total {\\n    display: flex;\\n    justify-content: space-between;\\n    padding: 0 15px;\\n}\\n.total span.label{ color: #333333; }\\n.total span.label span {\\n    font-size: 15px;\\n}\\n\\n.total span.money{ color: {{brand_color}}; }\\n.btn-checkout {\\n    padding: 0 15px;\\n    width: 100%;\\n    display: inline-block;\\n    text-align: center;\\n}\\n\\n.btn-checkout a{ \\n   background-color: {{brand_color}};\\n   color: #ffffff;\\n   padding: 12px 0;\\n   width: 100%;\\n   display: inline-block;\\n   border-radius: 30px;\\n   text-decoration: none;\\n   border:2px solid transparent;\\n  }\\n\\n  .btn-checkout a:hover{ \\n   color: {{brand_color}};  \\n   border:2px solid {{brand_color}};\\n   background-color: #ffffff;\\n  }\\n\\n\\n\\n.btn-view-cart {\\n    padding: 0 15px;\\n    width: 100%;\\n    display: inline-block;\\n    text-align: center;\\n    margin-top: 15px;\\n}\\n\\n.btn-view-cart a{ \\n   color: {{brand_color}};\\n   padding: 12px 0;\\n   width: 100%;\\n   display: inline-block;\\n   border-radius: 30px;\\n   border:2px solid {{brand_color}};\\n    text-decoration: none;\\n  }\\n.btn-view-cart a:hover{ background-color: {{brand_color}}; color: #ffffff; }\\n\\n\\n.simple-pagination ul {\\n  margin: 0 0 20px;\\n  padding: 0;\\n  list-style: none;\\n  text-align: center;\\n}\\n\\n.simple-pagination li {\\n  display: inline-block;\\n  margin-right: 5px;\\n}\\n\\n.simple-pagination li a,\\n.simple-pagination li span {\\n  color: #666;\\n  padding: 5px 10px;\\n  text-decoration: none;\\n  border: 1px solid #EEE;\\n  background-color: #FFF;\\n  box-shadow: 0px 0px 10px 0px #EEE;\\n}\\n\\n.simple-pagination .current {\\n  color: #FFF;\\n  background-color: {{brand_color}};\\n  border-color: {{brand_color}};\\n}\\n\\n.simple-pagination .prev.current,\\n.simple-pagination .next.current {\\n  background: {{brand_color}};\\n}\\n\\n\\n.add_to_cart_btn {\\n    width: 100%;\\n    display: inline-block;\\n}\\n\\n\\n.add_to_cart_btn a{ \\n  background-color: {{brand_color}};\\n    color: #ffffff;\\n    padding: 8px 20px;\\n    width: 100%;\\n    display: inline-block;\\n    border-radius: 30px;\\n    text-decoration: none;\\n    border: 2px solid transparent;\\n    text-align: center;\\n  }\\n\\n.add_to_cart_btn  a:hover{ \\n   color: {{brand_color}};  \\n   border:2px solid {{brand_color}};\\n   background-color: #ffffff;\\n  }\\n\\n\\n/*footer*/\\n.ecm_footer{ \\n  position: absolute;\\n  right: 0;\\n  bottom: 0;\\n  left: 0;\\n  background-color: {{ brand_color }};     padding: 15px 0; \\n}\\n.ecm_footer_copy_right {  text-align: center;}\\n.ecm_footer_copy_right p { color: #ffffff;     margin: 0;}\\n\\n\\n\\n@media (max-width:992px){\\n  \\n.ecm_product_col-3 {\\n -ms-flex: 0 0 50%;\\n  flex: 0 0 50%;\\n  max-width: 50%;\\n  padding: 0 15px;\\n  margin-bottom: 30px;\\n}\\n\\n\\n}\\n\\n@media (max-width:580px){\\n\\n  .ecm_product_col-3 {\\n -ms-flex: 0 0 100%;\\n  flex: 0 0 100%;\\n  max-width: 100%;\\n  padding: 0 15px;\\n  margin-bottom: 30px;\\n}\\n\\n}\\n\\n\\n</style>\\n{% end_require_css %}\\n\\n<body>\\n    <!--   E-cart Module   -->\\n    {% set query = request.query_dict %}\\n    {% set product_id = query.product_id %}\\n  {% set contact = crm_objects(\\"product\\",\'\') %} \\n  {% set product_count = contact.total %}\\n  {% set product = crm_objects(\\"product\\",\\"limit=\\"~product_count,\\"id,price,createdate,name,image,description\\") %}\\n    <div class=\\"ecm_header\\">\\n        <div class=\\"container\\">\\n            <div class=\\"cms_head\\">\\n                <div class=\\"logo\\">\\n                   \\n                    <a href=\\"#\\">\\n                        <img id=\\"portal_id\\" portal_id=\\"{{ portal_id }}\\" src=\\"\\" alt=\\"Brand-logo\\">\\n                    </a>\\n                </div>\\n                <div class=\\"cart_right\\">\\n                     <span id=\\"cart_item\\"> <i class=\\"fas fa-shopping-cart\\"></i> cart item (<span id=\\"cart_count\\">0</span>) </span>\\n                    <div class=\\"ecm_product_list\\">\\n                        <div id=\\"close_cart\\"> <i class=\\"fas fa-times\\"></i> Close</div>\\n                        <h4>SHOPPING CART :</h4>\\n                        <ul id=\\"product_item\\">\\n                          \\n                        </ul>\\n                        <div class=\\"total_cart\\">\\n                            <div class=\\"total\\">\\n                                <span class=\\"label\\">\\n                                    <span>\\n                                        Total:\\n                                    </span>\\n                                </span>\\n                                <span class=\\"price\\"><span class=\\"money\\"></span></span>\\n                            </div>\\n                        </div>\\n                        <div class=\\"btn-checkout\\">\\n                            <a href=\\"{{ get_asset_url(\'/webdew/checkout\') }}\\">Proceed to Checkout</a>\\n                          \\n                        </div>\\n                        <div class=\\"btn-view-cart\\">\\n                            <a href=\\"{{ get_asset_url(\'/webdew/cart\') }}\\">View Cart</a>\\n                        </div>\\n                    </div>\\n                </div>\\n            </div>\\n        </div>\\n    </div>\\n    <div class=\\"ecm_product_warp\\">\\n        <div class=\\"container\\">\\n       \\n            <div class=\\"ecm_product_row\\">\\n         \\n                <!-- loop Product -->\\n                {% for contacts in product.results %}\\n            \\n            {# {% set contact = crm_object(\\"product\\",contacts.id, \'id,price,createdate,name,image,description\') %}#}\\n                <div class=\\"ecm_product_col-3\\">\\n                    <div class=\\"ecm_product_box\\">\\n                        <div class=\\"ecm_product_img\\">\\n                            <img id=\\"image\\" src=\\"{{contacts.image}}\\" alt=\\"image not found\\">\\n                            <a href=\\"{{ get_asset_url(\'/webdew/product_detail\') }}&product_id={{contacts.id}}\\" class=\\"ecm_view_detail product_details\\"  prod_id=\\"{{contacts.id}}\\"  prod_name =\\"{{contacts.name}}\\" prod_price=\\"{{contacts.price}}\\" prod_desc=\\"{{contacts.description}}\\"  src=\\"{{contacts.image}}\\" >View Details</a>\\n                        </div>\\n                        <div class=\\"ecm_product_detail\\">\\n                            <h4>{{contacts.name}}</h4>\\n                            <span>{{contacts.price}}</span>\\n                        </div>\\n                        <div class=\\"add_to_cart_btn\\">\\n                           <a href=\\"#\\"  class=\\"add_cart\\" product_id=\\"{{contacts.id}}\\" src=\\"{{contacts.image}}\\" product_name=\\"{{contacts.name}}\\" product_price=\\"{{contacts.price}}\\" >Add To Cart</a>\\n                        </div>\\n                    </div>\\n                </div>\\n        \\n               {% endfor %}\\n                <!-- loop Product -->\\n              \\n            </div>\\n <div id=\\"pagination-container\\"></div>\\n\\n        </div>\\n\\n       \\n\\n\\n    </div>\\n\\n     <div class=\\"ecm_footer\\">\\n       <div class=\\"container\\">\\n          <div class=\\"ecm_footer_copy_right\\">\\n             <p>Copyright 2021 • All Rights Reserved • Terms of Service • Privacy Policy</p>\\n          </div>\\n       </div>\\n    </div>\\n    {{ standard_footer_includes }}\\n\\n   <script src=\\"https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js\\"></script>\\n  <script src=\\"https://cdnjs.cloudflare.com/ajax/libs/simplePagination.js/1.6/jquery.simplePagination.js\\"></script>\\n\\n\\n\\n    <script type=\\"text/javascript\\">\\n            function roundFloat(num,dec){\\n            var d = 1;\\n            for (var i=0; i<dec; i++){\\n                d += \\"0\\";\\n            }\\n                return Math.round(num * d) / d;\\n            }\\n   \\n \\n      \\n      function logo_address(object){\\n       $(\\"#portal_id\\").attr(\\"src\\",object.data);\\n        }\\n      \\n   \\n    $(document).ready(function() {\\n      var portal_id = $(\\"#portal_id\\").attr(\\"portal_id\\");\\n      var base_url = \\"https://apps.webdew.com/hubspot\\";\\n    $.ajax({\\n        url:\\"https://apps.webdew.com/hubspot/apps/ecommerce/logo?&portal_id=\\"+portal_id,\\n        dataType: \'jsonp\',\\n        async:true\\n        });\\n         \\n  });\\n\\n      \\n      //this function is used to subtract cart items\\n      $(document).on(\'click\', \'.minus\', function()\\n      {     \\n            var $input = $(this).parent().find(\'input\');\\n            var price  = $(this).attr(\'minus_price\');//getting price from button\\n            var  id    = $(this).attr(\'prod_minus\');//getting product id from button\\n            var items  = localStorage.getItem(\'items\');\\n        \\n          if($input.val() > 1 ){\\n          \\n           //converting price into number format\\n            var str = String(price);\\n            str = str.replace(\\",\\", \\"\\");\\n            str = str.replace(\\"$\\", \\"\\");\\n           //converting price into number format\\n           \\n            var cart_total = localStorage.getItem(\'total\');//getting total from local storage\\n            var total = parseFloat(cart_total);\\n            \\n            total -= parseFloat(price);//subtracting product price from total\\n            total = roundFloat(total,4);\\n            localStorage.setItem(\\"total\\",total);//setting total in local storage\\n            $(\\".money\\").text(\\" $ \\"+total+\\" USD \\");//reflecting back into cart\\n        \\n            let product_detail = localStorage.getItem(id);\\n            var product = JSON.parse(product_detail);\\n            product.item -= 1;\\n            \\n            product.total -= parseFloat(str); \\n            product.total = roundFloat(product.total,4);\\n            localStorage.setItem(id,JSON.stringify(product));\\n        \\n            var items = localStorage.getItem(\\"items\\");\\n            var int_items = parseInt(items);\\n            int_items -= 1;\\n            localStorage.setItem(\\"items\\",int_items);\\n            $(\\"#cart_count\\").text(int_items);\\n        \\n            var count = parseInt($input.val()) - 1;\\n            count = count < 1 ? 1 : count;\\n            $input.val(count);\\n            $input.change();\\n          }\\n        \\n            return false;\\n      });\\n      \\n         //this function is used to add cart items into cart\\n       $(document).on(\'click\', \'.plus\', function()\\n      {     \\n            \\n            var $input = $(this).parent().find(\'input\');\\n            var price = $(this).attr(\'add_price\');// this price is in dollar format\\n            var  id= $(this).attr(\'prod_add\');\\n           \\n            //converting it to normal number\\n            var str = String(price);\\n            str = str.replace(\\",\\", \\"\\");\\n            str = str.replace(\\"$\\", \\"\\");\\n            //converting it to normal number\\n            \\n            var cart_total = localStorage.getItem(\'total\');\\n            var total = parseFloat(cart_total);\\n            \\n            total += parseFloat(price);\\n             total = roundFloat(total,4);\\n            localStorage.setItem(\\"total\\",total);\\n            $(\\".money\\").text(\\" $ \\"+total+\\" USD \\");\\n         \\n             let product_detail = localStorage.getItem(id);\\n             var product = JSON.parse(product_detail);\\n             product.item += 1;\\n             product.total += parseFloat(str);\\n              product.total = roundFloat(product.total,4);\\n            localStorage.setItem(id,JSON.stringify(product));\\n         \\n            var items = localStorage.getItem(\\"items\\");\\n            var int_items = parseInt(items);\\n            int_items += 1;\\n            localStorage.setItem(\\"items\\",int_items);\\n            $(\\"#cart_count\\").text(int_items);\\n         \\n            $input.val(parseInt($input.val()) + 1);\\n            $input.change();\\n            return false;\\n      });\\n      \\n             $(document).on(\'click\', \'#delete\', function()\\n      {     \\n            \\n            var item_id = $(this).attr(\'item_id\');\\n            $(\\".list_id\\"+item_id).remove();\\n            \\n             \\n           \\n            var product_detail = localStorage.getItem(item_id);\\n            var product = JSON.parse(product_detail);\\n            \\n            var item_price = product.total;\\n\\n            var cart_total = localStorage.getItem(\'total\');\\n            var total = parseFloat(cart_total);\\n            \\n            total -= parseFloat(item_price);\\n            total = roundFloat(total,4);\\n            localStorage.setItem(\\"total\\",total);\\n            $(\\".money\\").text(\\" $ \\"+total+\\" USD \\");\\n             \\n           \\n               \\n               \\n            //storing item count in local storage\\n            var items = localStorage.getItem(\\"items\\");\\n            var int_items = parseInt(items);\\n            \\n            var items = localStorage.getItem(item_id);\\n            var items_object = JSON.parse(items);\\n            var product_items = items_object.item;\\n\\n               \\n            int_items -= parseInt(product_items);\\n            localStorage.setItem(\\"items\\",int_items);\\n            //storing item count in local storage\\n        \\n             $(\\"#cart_count\\").text(int_items);\\n             \\n             localStorage.removeItem(item_id);\\n\\n            return false;\\n      });\\n      \\n  \\n      \\n      $(document).on(\'click\', \'.product_details\', function(e)\\n      {     e.preventDefault();\\n            var product_id = $(this).attr(\'prod_id\');\\n            var product_price = $(this).attr(\'prod_price\');\\n            var product_name = $(this).attr(\'prod_name\');\\n            var href = $(this).attr(\'href\');\\n            var product_description = $(this).attr(\'prod_desc\');\\n            var image_path = $(this).attr(\'src\');\\n            var product_info={\'product_id\':product_id,\'product_name\':product_name,\'product_price\':product_price,\'product_description\':product_description,\'image_path\':image_path};\\n            localStorage.setItem(\'product_info\',JSON.stringify(product_info));\\n            window.location.replace(href);\\n          \\n      });\\n      \\n      \\n      \\n      \\n          //add to cart button funcitonality\\n          $(document).ready(function() {\\n          //setting cart total and items to 0\\n          if(localStorage.getItem(\'items\'))\\n          {\\n                let nextValue;\\n                    for (let i = 0; i < localStorage.length; i++)\\n                    {\\n                        nextValue = localStorage.key(i);\\n                          if(nextValue != \\"total\\" && nextValue != \\"items\\" && nextValue !=\\"product_info\\" &&nextValue != \\"__paypal_storage__\\" )\\n                          {\\n                              var item = localStorage.getItem(nextValue);\\n                                \\n                                  var items_object = JSON.parse(item);\\n                                  var product_id = nextValue; \\n                                  var product_name = items_object.product_name;\\n                                  var product_price = items_object.product_price;\\n//                                   var product_rate = items_object.total;\\n                            \\n                            \\n                                var str = String(product_price);\\n                                str = str.replace(\\",\\", \\"\\");\\n                                str = str.replace(\\"$\\", \\"\\");\\n                                var product_rate = parseFloat(str);\\n                                var image_path = items_object.image_path;\\n                               $(\\"#product_item\\").append(\\"<li class=\'list_id\\"+product_id+\\"\'><div class=\'product_list_row\'><div class=\'product_thum\'><img src=\'\\"+image_path+\\"\' alt=\'\'></div><div class=\'ecm_description\'><h4>\\"+product_name+\\"</h4><span class=\'price\'>\\"+product_price+\\"</span><div class=\'number\'><span class=\'minus\' minus_price=\\"+product_rate+\\" prod_minus=\\"+product_id+\\">-</span><input type=\'text\' disabled value=\\"+items_object.item+\\" /><span class=\'plus\' add_price=\\"+product_rate+\\" prod_add=\\"+product_id+\\">+</span></div></div><div class=\'ecm_close\'><i class=\'fas fa-trash-alt\' item_id=\\"+product_id+\\" id=\'delete\'></i></div></div></li>\\");\\n                             \\n                                \\n                          }\\n                    } \\n                        var total = localStorage.getItem(\'total\');\\n                        $(\\".money\\").text(\\" $ \\"+total+\\" USD \\");\\n\\n                        var items = localStorage.getItem(\'items\');\\n                        $(\\"#cart_count\\").text(items);\\n            \\n          }\\n            else\\n            {\\n              localStorage.setItem(\\"total\\",0);\\n              localStorage.setItem(\\"items\\",0);\\n            }\\n           \\n           \\n         \\n          $(\'.add_cart\').click(function() {\\n        \\n            var $this = $(this);\\n            var product_id = $this.attr(\'product_id\');\\n            var product_name = $this.attr(\'product_name\');\\n            var product_price = $this.attr(\'product_price\');\\n            var image_path = $this.attr(\'src\');\\n            \\n            //checking if item already in cart or not\\n            var items = localStorage.getItem(product_id);\\n            \\n            if (localStorage.getItem(product_id)) {//if item does exist in cart then we will enter in this if\\n             \\n            var str = String(product_price);\\n            str = str.replace(\\",\\", \\"\\");\\n            str = str.replace(\\"$\\", \\"\\");\\n            var product_rate = parseFloat(str);\\n            var cart_total = localStorage.getItem(\'total\');\\n            var total = parseFloat(cart_total);\\n            \\n            total += product_rate;\\n              total = roundFloat(total,4);\\n            var items = localStorage.getItem(product_id);\\n            var items_object = JSON.parse(items);\\n            items_object.item += 1;\\n            items_object.total += product_rate;\\n            items_object.total = roundFloat(items_object.total,4);\\n            //updating  product and total in local storage  \\n            localStorage.setItem(product_id,JSON.stringify(items_object));\\n            localStorage.setItem(\\"total\\",total);\\n            \\n             //storing item count in local storage\\n            var items = localStorage.getItem(\\"items\\");\\n            var int_items = parseInt(items);\\n            int_items += 1;\\n            localStorage.setItem(\\"items\\",int_items);\\n            //storing item count in local storage\\n\\n             $(\\"#cart_count\\").text(int_items);\\n            \\n            //updating cart item\\n            $(\'.list_id\'+product_id).remove();\\n            $(\\"#product_item\\").append(\\"<li class=\'list_id\\"+product_id+\\"\'><div class=\'product_list_row\'><div class=\'product_thum\'><img src=\'\\"+image_path+\\"\' alt=\'\'></div><div class=\'ecm_description\'><h4>\\"+product_name+\\"</h4><span class=\'price\'>\\"+product_price+\\"</span><div class=\'number\'><span class=\'minus\' minus_price=\\"+product_rate+\\" prod_minus=\\"+product_id+\\">-</span><input type=\'text\' disabled value=\\"+items_object.item+\\" /><span class=\'plus\' add_price=\\"+product_rate+\\" prod_add=\\"+product_id+\\">+</span></div></div><div class=\'ecm_close\'><i class=\'fas fa-trash-alt\' item_id=\\"+product_id+\\" id=\'delete\'></i></div></div></li>\\");\\n            $(\\".money\\").text(\\" $ \\"+total+\\" USD \\");\\n            //updating cart item\\n           }\\n            else{//if item does not exists \\n              var str = String(product_price);\\n            str = str.replace(\\",\\", \\"\\");\\n            str = str.replace(\\"$\\", \\"\\");\\n            var product_rate = parseFloat(str);\\n            var cart_total = localStorage.getItem(\'total\');\\n            var total = parseFloat(cart_total);\\n            var image_path = $this.attr(\'src\');\\n            total += product_rate;\\n            total = roundFloat(total,4);\\n            //storing product in local storage\\n            var product_detail = { \'product_id\': product_id, \'product_name\': product_name, \'product_price\': product_price ,\'item\': 1 ,\'total\':product_rate,\'image_path\':image_path};    \\n            localStorage.setItem(product_id,JSON.stringify(product_detail));\\n            localStorage.setItem(\\"total\\",total);\\n            //storing product in local storage\\n         \\n            //storing item count in local storage\\n            var items = localStorage.getItem(\\"items\\");\\n            var int_items = parseInt(items);\\n            int_items += 1;\\n            localStorage.setItem(\\"items\\",int_items);\\n            //storing item count in local storage\\n            \\n            //updating cart count\\n//             var cart_count=$(\'#cart_count\').text();//getting cart count\\n//             var y =  parseInt(cart_count);\\n//             var x=1;\\n//             y += x;\\n            $(\\"#cart_count\\").text(int_items);//adding one and reflecting back count\\n            //updating cart count\\n \\n            //adding cart item\\n          $(\\"#product_item\\").append(\\"<li class=\'list_id\\"+product_id+\\"\'><div class=\'product_list_row\'><div class=\'product_thum\'><img src=\'\\"+image_path+\\"\' alt=\'\'></div><div class=\'ecm_description\'><h4>\\"+product_name+\\"</h4><span class=\'price\'>\\"+product_price+\\"</span><div class=\'number\'><span class=\'minus\' minus_price=\\"+product_rate+\\" prod_minus=\\"+product_id+\\">-</span><input type=\'text\' disabled value=\'1\' /><span class=\'plus\' add_price=\\"+product_rate+\\" prod_add=\\"+product_id+\\">+</span></div></div><div class=\'ecm_close\'><i class=\'fas fa-trash-alt\' item_id=\\"+product_id+\\" id=\'delete\'></i></div></div></li>\\");\\n          $(\\".money\\").text(\\" $ \\"+total+\\" USD \\");\\n              \\n           }\\n          \\n            \\n        });\\n    });\\n   //add to cart button funcitonality ends \\n\\n    $(document).ready(function() {\\n\\n        $(\\"#cart_item\\").click(function() {\\n            $(\'.ecm_header\').addClass(\'menu_fix\');\\n        });\\n\\n        $(\\"#close_cart\\").click(function() {\\n            $(\'.ecm_header\').removeClass(\'menu_fix\');\\n        });\\n    });\\n\\n\\n    // jQuery Plugin: http://flaviusmatis.github.io/simplePagination.js/\\n\\nvar items = $(\\".ecm_product_row .ecm_product_col-3\\");\\n    var numItems = items.length;\\n    var perPage = 8;\\n\\n    items.slice(perPage).hide();8\\n\\n    $(\'#pagination-container\').pagination({\\n        items: numItems,\\n        itemsOnPage: perPage,\\n        prevText: \\"&laquo;\\",\\n        nextText: \\"&raquo;\\",\\n        onPageClick: function (pageNumber) {\\n            var showFrom = perPage * (pageNumber - 1);\\n            var showTo = showFrom + perPage;\\n            items.hide().slice(showFrom, showTo).show();\\n        }\\n    });\\n\\n\\n\\n\\n    </script>\\n\\n\\n     \\n\\n</body>\\n\\n</html>"
    			    }',
    			  CURLOPT_HTTPHEADER => array(
    			    'Authorization: Bearer '.$token,
    			    'Content-Type: application/json',
    			    'Cookie: __cfduid=dbee73ed9d0d3ee23fabe7672fb9b848a1614580870'
    			  ),
    			 ));

    			 $response = curl_exec($curl);

    			 curl_close($curl);


                 //product page ends

                 //product_details


        		 $curl = curl_init();

        		 curl_setopt_array($curl, array(
        		  CURLOPT_URL => 'https://api.hubapi.com/content/api/v2/templates',
        		  CURLOPT_RETURNTRANSFER => true,
        		  CURLOPT_ENCODING => '',
        		  CURLOPT_MAXREDIRS => 10,
        		  CURLOPT_TIMEOUT => 0,
        		  CURLOPT_FOLLOWLOCATION => true,
        		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        		  CURLOPT_CUSTOMREQUEST => 'POST',
        		  CURLOPT_POSTFIELDS =>'{
        		    "category_id": 1,
        		    "folder": "example",
        		    "is_available_for_new_content": "False",
        		    "template_type": 4,        
        		    "path":"webdew/product_detail",
        		    "source":"<!--\\n    templateType: Page\\n    isAvailableForNewContent: true\\n-->\\n<!doctype html>\\n<html>\\n\\n<head>\\n    <meta charset=\\"utf-8\\">\\n    <title>{{ content.html_title }}</title>\\n    <link rel=\\"stylesheet\\" href=\\"https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css\\">\\n    <link rel=\\"stylesheet\\" href=\\"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css\\">\\n    <link href=\\"https://fonts.googleapis.com/css2?family=DM+Sans:wght@500&display=swap\\" rel=\\"stylesheet\\">\\n    <script src=\\"https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js\\"></script>\\n    <script src=\\"https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js\\"></script>\\n   \\n    <link rel=\\"stylesheet\\" href=\\"https://pro.fontawesome.com/releases/v5.10.0/css/all.css\\">\\n    <meta name=\\"description\\" content=\\"{{ content.meta_description }}\\">\\n    {{ standard_header_includes }}\\n</head>\\n\\n{% require_css %}\\n<style>\\n\\nhtml {\\n  height: 100%;\\n  box-sizing: border-box;\\n}\\nbody {\\n  position: relative;\\n  padding-bottom: 6rem;\\n  min-height: 100%;\\n  \\n} \\n \\n    @import url(\'https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap\');\\n\\n\\n{% set brand_color = \\"#00a9fa\\" %}\\n\\nhtml {\\n  height: 100%;\\n  box-sizing: border-box;\\n}\\nbody {\\n  position: relative;\\n  padding-bottom: 6rem;\\n  min-height: 100%;\\n}\\nimg{ max-width: 100%;}\\n\\n.ecm_header{ width: 100%; display: inline-block; position: relative;  padding: 15px 0;     background-color: #ffffff;\\n    box-shadow: 0 0px 10px #eee;}\\n.cms_head{ display: flex; flex-wrap: wrap; justify-content: space-between; align-items: center; }\\n.cms_head .logo{ max-width: 180px; }\\n.cms_head .logo img {   width: 100%;}\\n\\n.ecm_product_warp{ padding: 50px 0; }\\n.ecm_product_row{\\n  display: -ms-flexbox;\\n  display: flex;\\n  -ms-flex-wrap: wrap;\\n  flex-wrap: wrap;\\n\\n}\\n  \\n.ecm_product_col-6 {\\n -ms-flex: 0 0 50%;\\n  flex: 0 0 50%;\\n  max-width: 50%;\\n  padding: 0 15px;\\n \\n}\\n.ecm_product_img{ background-color: #efefef; padding: 15px; position: relative; }\\na.ecm_view_detail {\\n    display: none;\\n    background-color: {{brand_color}} ;\\n    color: #ffffff;\\n    position: absolute;\\n    bottom: 0;\\n    left: 0;\\n    width: 100%;\\n    padding: 10px 0;\\n    text-align: center;\\n    text-decoration: none;\\n}\\n.ecm_product_box:hover a.ecm_view_detail { display: block; }\\n\\n.ecm_product_detail{ padding: 10px 0; text-align: center; }\\n\\nspan#cart_item {\\n    cursor: pointer;\\n}\\n.ecm_product_list {\\n    /*display: none;*/\\n    position: fixed;\\n    top: 0;\\n    right: -320px;\\n    width: 320px;\\n    background-color: #fff;\\n    z-index: 99;\\n    height: 100%;\\n    overflow: auto;\\n    padding: 25px 0 0 0;\\n    border-left: 1px solid #ccc;    \\n    -webkit-transition: all 1s; \\n    transition: all 1s; \\n    opacity:0;\\n    visibility: hidden;\\n} \\n.ecm_header.menu_fix  .ecm_product_list{\\n    right: 0;\\n      /*display: block;*/\\n  -webkit-transition: all 1s; \\n    transition: all 1s; \\n    visibility: visible;\\n    opacity: 1;\\n}\\ndiv#close_cart {\\n    display: flex;\\n    text-align: right;\\n    justify-content: flex-end;\\n    align-items: center;\\n    margin: 0 25px 10px 0;\\n    border-radius: 2px;\\n    color: {{brand_color}};\\n    cursor: pointer;\\n}\\ndiv#close_cart i.fas {\\n    font-size: 20px;\\n    color: {{brand_color}};\\n    margin: 0 4px 0 0;\\n}\\n.ecm_product_list ul{ margin: 0; padding: 0; }\\n.ecm_product_list ul li {\\n    list-style: none;\\n    padding: 12px 25px;\\n    border-bottom: 1px solid #ccc;\\n}\\n.product_list_row{ \\n  display: flex; \\n  flex-wrap: wrap;\\n  justify-content: space-between;\\n}\\n.ecm_description h4 {\\n  margin: 0;\\n  padding: 0;\\n  font-size: 15px;\\n}\\n.product_thum {  width: 60px;}\\n\\n.product_list_row span {cursor:pointer; }\\n.product_list_row .number{\\n  margin:5px 0 0 0;\\n}\\n.product_list_row .minus, .product_list_row .plus{\\n    width:20px;\\n    /*height:20px;*/\\n    background:#f2f2f2;\\n    border-radius:4px;\\n    padding:3px 5px 3px 5px;\\n    border:1px solid #ddd;\\n    display: inline-block;\\n    vertical-align: middle;\\n    text-align: center;\\n}\\n.product_list_row input{\\n    height:30px;\\n    width: 40px;\\n    text-align: center;\\n    font-size: 15px;\\n    border:1px solid #ddd;\\n    border-radius:4px;\\n    display: inline-block;\\n    vertical-align: middle;    \\n\\n}\\n.ecm_product_list > h4 {\\n    font-size: 14px;\\n    padding: 10px 15px 15px;\\n    border-bottom: 1px solid #ccc;\\n    font-weight: 700;\\n}\\n.ecm_close i.fas.fa-trash-alt{ color: {{brand_color}}; cursor: pointer; }\\n.total_cart {\\n    margin: 15px 0;\\n    width: 100%;\\n}\\n.total {\\n    display: flex;\\n    justify-content: space-between;\\n    padding: 0 15px;\\n}\\n.total span.label{ color: #333333; }\\n.total span.label span {\\n    font-size: 15px;\\n}\\n.total span.money{ color:{{brand_color}}; }\\n.btn-checkout {\\n    padding: 0 15px;\\n    width: 100%;\\n    display: inline-block;\\n    text-align: center;\\n}\\n.btn-checkout a{ \\n   background-color: {{brand_color}};\\n   color: #ffffff;\\n   padding: 12px 0;\\n   width: 100%;\\n   display: inline-block;\\n   border-radius: 30px;\\n   text-decoration: none;\\n   border:2px solid transparent;\\n}\\n.btn-checkout a:hover{ \\n   color: {{brand_color}};  \\n   border:2px solid {{brand_color}};\\n   background-color: #ffffff;\\n}\\n.btn-view-cart {\\n    padding: 0 15px;\\n    width: 100%;\\n    display: inline-block;\\n    text-align: center;\\n    margin-top: 15px;\\n}\\n.btn-view-cart a{ \\n   color: {{brand_color}};\\n   padding: 12px 0;\\n   width: 100%;\\n   display: inline-block;\\n   border-radius: 30px;\\n   border:2px solid {{brand_color}};\\n    text-decoration: none;\\n  }\\n.btn-view-cart a:hover{ background-color: {{brand_color}}; color: #ffffff; }\\n.add_to_cart_btn {\\n    width: 100%;\\n    display: inline-block;\\n}\\n.add_to_cart_btn a{ \\n  background-color: {{brand_color}};\\n    color: #ffffff;\\n    padding: 8px 20px;\\n    width: 100%;\\n    display: inline-block;\\n    border-radius: 30px;\\n    text-decoration: none;\\n    border: 2px solid transparent;\\n    text-align: center;\\n}\\n.add_to_cart_btn  a:hover{ \\n   color: {{brand_color}};  \\n   border:2px solid {{brand_color}};\\n   background-color: #ffffff;\\n  }\\n\\n.ecm_product_col-6 .ecm_product_detail {\\n    text-align: left; margin: 0;\\n    padding: 0;\\n}\\n\\n.ecm_product_col-6 .add_to_cart_btn a {\\n    width: auto;\\n}\\n\\n.ecm_product_col-6 .ecm_product_detail h4{ font-size: 32px; margin-bottom: 30px; margin-top: 0; }\\n.ecm_product_col-6 .ecm_product_detail p{ font-size: 16px; margin-bottom: 28px; }\\n\\n.ecm_product_col-6 .ecm_product_detail span{ font-size: 24px; margin-bottom: 28px; color: {{brand_color}}; display: block;  }\\n\\n\\n.ecm_product_col-6.ecm_product_dcrt {\\n    padding-top: 25px;\\n}\\n\\n.back_arrow_btn {\\n    padding: 20px 15px;\\n    width: 100%;\\n    display: inline-block;\\n}\\n\\n.back_arrow_btn a{\\n   color: {{brand_color}}; text-decoration: none;\\n} \\n\\n/*footer*/\\n.ecm_footer{ \\n  position: absolute;\\n  right: 0;\\n  bottom: 0;\\n  left: 0;\\n  background-color: {{ brand_color }};     padding: 15px 0; \\n}\\n.ecm_footer_copy_right {  text-align: center;}\\n.ecm_footer_copy_right p { color: #ffffff;     margin: 0;}\\n\\n\\n@media (max-width:992px){\\n  \\n.ecm_product_col-6 {\\n -ms-flex: 0 0 50%;\\n  flex: 0 0 50%;\\n  max-width: 50%;\\n  padding: 0 15px;\\n  margin-bottom: 30px;\\n}\\n}\\n\\n@media (max-width:580px){\\n\\n  .ecm_product_col-6 {\\n -ms-flex: 0 0 100%;\\n  flex: 0 0 100%;\\n  max-width: 100%;\\n  padding: 0 15px;\\n  margin-bottom: 30px;\\n}\\n\\n}\\n\\n\\n</style>\\n{% end_require_css %}\\n\\n<body>\\n    <!--   E-cart Module   -->\\n    {% set query = request.query_dict %}\\n    {% set product_id = query.product_id %}\\n     {% set contact = crm_object(\\"product\\", product_id, \'id,price,createdate,name,image, description\') %} \\n    <div class=\\"ecm_header\\">\\n        <div class=\\"container\\">\\n            <div class=\\"cms_head\\">\\n                <div class=\\"logo\\">\\n                    <a href=\\"#\\">\\n                         <img id=\\"portal_id\\" portal_id=\\"{{ portal_id }}\\" src=\\"\\" alt=\\"Brand-logo\\">\\n                    </a>\\n                </div>\\n                <div class=\\"cart_right\\">\\n                   <span id=\\"cart_item\\"> <i class=\\"fas fa-shopping-cart\\"></i> cart item (<span id=\\"cart_count\\">0</span>) </span>\\n                    <div class=\\"ecm_product_list\\">\\n                        <div id=\\"close_cart\\"> <i class=\\"fas fa-times\\"></i> Close</div>\\n                        <h4>SHOPPING CART :</h4>\\n                          <ul id=\\"product_item\\">\\n                          \\n                        </ul>\\n                        <div class=\\"total_cart\\">\\n                            <div class=\\"total\\">\\n                                <span class=\\"label\\">\\n                                    <span>\\n                                        Total:\\n                                    </span>\\n                                </span>\\n                                <span class=\\"price\\"><span class=\\"money\\"></span></span>\\n                            </div>\\n                        </div>\\n                        <div class=\\"btn-checkout\\">\\n                           <a href=\\"{{ get_asset_url(\'/webdew/checkout\') }}\\">Proceed to Checkout</a>\\n                        </div>\\n                        <div class=\\"btn-view-cart\\">\\n                            <a href=\\"{{ get_asset_url(\'/webdew/cart\') }}\\">View Cart</a>\\n                        </div>\\n                    </div>\\n                </div>\\n            </div>\\n        </div>\\n    </div>\\n    <div class=\\"ecm_product_warp ecm_product_detail_wrap\\">\\n        <div class=\\"container\\">\\n          <div class=\\"back_arrow_btn\\">\\n             <a href=\\"{{ get_asset_url(\'/webdew/products\') }}\\"><i class=\\"fas fa-long-arrow-alt-left\\"></i> Back</a>\\n          </div>\\n\\n            <div class=\\"ecm_product_row\\">\\n        \\n                <div class=\\"ecm_product_col-6\\">\\n                    <div class=\\"ecm_product_box\\">\\n                        <div class=\\"ecm_product_img\\">\\n                            <img src=\\"{{contact.image}}\\" alt=\\"\\">\\n                           </div>\\n                        \\n                    </div>\\n                </div>\\n       \\n\\n                 <div class=\\"ecm_product_col-6 ecm_product_dcrt \\">\\n                  <div class=\\"ecm_product_detail\\">\\n                            <h4 class=\\"product_title\\"></h4>\\n                            <p class=\\"product_description\\"></p>\\n                            <span class=\\"product_price\\"></span>\\n                        </div>\\n                        <div class=\\"add_to_cart_btn\\">\\n                           <a href=\\"#\\" class=\\"add_cart\\" >Add To Cart</a>\\n                           \\n<!--                            <a href=\\"#\\">Buy it Now</a> -->\\n                        </div>\\n                 </div>\\n           \\n            </div>\\n\\n\\n        </div>\\n\\n       \\n\\n\\n    </div>\\n    <div class=\\"ecm_footer\\">\\n       <div class=\\"container\\">\\n          <div class=\\"ecm_footer_copy_right\\">\\n             <p>Copyright 2021 • All Rights Reserved • Terms of Service • Privacy Policy</p>\\n          </div>\\n       </div>\\n    </div>\\n    {{ standard_footer_includes }}\\n\\n   <script src=\\"https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js\\"></script>\\n  <script src=\\"https://cdnjs.cloudflare.com/ajax/libs/simplePagination.js/1.6/jquery.simplePagination.js\\"></script>\\n\\n\\n\\n    <script type=\\"text/javascript\\">\\n      \\n        function roundFloat(num,dec){\\n        var d = 1;\\n        for (var i=0; i<dec; i++){\\n            d += \\"0\\";\\n        }\\n        return Math.round(num * d) / d;\\n         }\\n   \\n      \\n            function logo_address(object){\\n       $(\\"#portal_id\\").attr(\\"src\\",object.data);\\n        }\\n      \\n   \\n    $(document).ready(function() {\\n      var portal_id = $(\\"#portal_id\\").attr(\\"portal_id\\");\\n      var base_url = \\"https://apps.webdew.com/hubspot\\";\\n    $.ajax({\\n        url:\\"https://apps.webdew.com/hubspot/apps/ecommerce/logo?&portal_id=\\"+portal_id,\\n        dataType: \'jsonp\',\\n        async:true\\n        });\\n         \\n  });\\n      \\n      \\n    $(document).ready(function() {\\n        $(\'.minus\').click(function() {\\n            var $input = $(this).parent().find(\'input\');\\n            var count = parseInt($input.val()) - 1;\\n            count = count < 1 ? 1 : count;\\n            $input.val(count);\\n            $input.change();\\n            return false;\\n        });\\n        $(\'.plus\').click(function() {\\n            var $input = $(this).parent().find(\'input\');\\n            $input.val(parseInt($input.val()) + 1);\\n            $input.change();\\n            return false;\\n        });\\n    });\\n\\n    $(document).ready(function() {\\n\\n        $(\\"#cart_item\\").click(function() {\\n            $(\'.ecm_header\').addClass(\'menu_fix\');\\n        });\\n\\n        $(\\"#close_cart\\").click(function() {\\n            $(\'.ecm_header\').removeClass(\'menu_fix\');\\n        });\\n    });\\n\\n\\n    // jQuery Plugin: http://flaviusmatis.github.io/simplePagination.js/\\n\\nvar items = $(\\".ecm_product_row .ecm_product_col-3\\");\\n    var numItems = items.length;\\n    var perPage = 5;\\n\\n    items.slice(perPage).hide();8\\n\\n    $(\'#pagination-container\').pagination({\\n        items: numItems,\\n        itemsOnPage: perPage,\\n        prevText: \\"&laquo;\\",\\n        nextText: \\"&raquo;\\",\\n        onPageClick: function (pageNumber) {\\n            var showFrom = perPage * (pageNumber - 1);\\n            var showTo = showFrom + perPage;\\n            items.hide().slice(showFrom, showTo).show();\\n        }\\n    });\\n\\n\\n $(document).on(\'click\', \'.minus\', function()\\n      {     \\n            var $input = $(this).parent().find(\'input\');\\n            var price  = $(this).attr(\'minus_price\');//getting price from button\\n            var  id    = $(this).attr(\'prod_minus\');//getting product id from button\\n            var items = localStorage.getItem(\'items\');\\n            \\n           if($input.val() > 1  ){\\n             \\n           //converting price into number format\\n            var str = String(price);\\n            str = str.replace(\\",\\", \\"\\");\\n            str = str.replace(\\"$\\", \\"\\");\\n           //converting price into number format\\n           \\n            var cart_total = localStorage.getItem(\'total\');//getting total from local storage\\n            var total = parseFloat(cart_total);\\n            \\n            total -= parseFloat(price);//subtracting product price from total\\n             total = roundFloat(total,4);\\n            localStorage.setItem(\\"total\\",total);//setting total in local storage\\n            $(\\".money\\").text(\\" $ \\"+total+\\" USD \\");//reflecting back into cart\\n        \\n            let product_detail = localStorage.getItem(id);\\n            var product = JSON.parse(product_detail);\\n            product.item -= 1;\\n            \\n            product.total -= parseFloat(str); \\n             product.total = roundFloat(product.total,4);\\n            localStorage.setItem(id,JSON.stringify(product));\\n        \\n            var items = localStorage.getItem(\\"items\\");\\n            var int_items = parseInt(items);\\n            int_items -= 1;\\n            localStorage.setItem(\\"items\\",int_items);\\n            $(\\"#cart_count\\").text(int_items);\\n        \\n            var count = parseInt($input.val()) - 1;\\n            count = count < 1 ? 1 : count;\\n            $input.val(count);\\n            $input.change();\\n            \\n           }\\n         return false;\\n      });\\n      \\n         //this function is used to add cart items into cart\\n       $(document).on(\'click\', \'.plus\', function()\\n      {     \\n            \\n            var $input = $(this).parent().find(\'input\');\\n            var price = $(this).attr(\'add_price\');// this price is in dollar format\\n            var  id= $(this).attr(\'prod_add\');\\n           \\n            //converting it to normal number\\n            var str = String(price);\\n            str = str.replace(\\",\\", \\"\\");\\n            str = str.replace(\\"$\\", \\"\\");\\n            //converting it to normal number\\n            \\n            var cart_total = localStorage.getItem(\'total\');\\n            var total = parseFloat(cart_total);\\n            \\n            total += parseFloat(price);\\n             total = roundFloat(total,4);\\n            localStorage.setItem(\\"total\\",total);\\n            $(\\".money\\").text(\\" $ \\"+total+\\" USD \\");\\n         \\n             let product_detail = localStorage.getItem(id);\\n             var product = JSON.parse(product_detail);\\n             product.item += 1;\\n             product.total += parseFloat(str);\\n              product.total = roundFloat(product.total,4);\\n            localStorage.setItem(id,JSON.stringify(product));\\n         \\n            var items = localStorage.getItem(\\"items\\");\\n            var int_items = parseInt(items);\\n            int_items += 1;\\n            localStorage.setItem(\\"items\\",int_items);\\n            $(\\"#cart_count\\").text(int_items);\\n         \\n            $input.val(parseInt($input.val()) + 1);\\n            $input.change();\\n            return false;\\n      });\\n      \\n             $(document).on(\'click\', \'#delete\', function()\\n      {     \\n            \\n            var item_id = $(this).attr(\'item_id\');\\n            $(\\".list_id\\"+item_id).remove();\\n            \\n             \\n           \\n            var product_detail = localStorage.getItem(item_id);\\n            var product = JSON.parse(product_detail);\\n            \\n            var item_price = product.total;\\n\\n            var cart_total = localStorage.getItem(\'total\');\\n            var total = parseFloat(cart_total);\\n            \\n            total -= parseFloat(item_price);\\n            total = roundFloat(total,4);\\n            localStorage.setItem(\\"total\\",total);\\n            $(\\".money\\").text(\\" $ \\"+total+\\" USD \\");\\n             \\n           \\n               \\n               \\n            //storing item count in local storage\\n            var items = localStorage.getItem(\\"items\\");\\n            var int_items = parseInt(items);\\n            \\n            var items = localStorage.getItem(item_id);\\n            var items_object = JSON.parse(items);\\n            var product_items = items_object.item;\\n\\n               \\n            int_items -= parseInt(product_items);\\n            localStorage.setItem(\\"items\\",int_items);\\n            //storing item count in local storage\\n        \\n             $(\\"#cart_count\\").text(int_items);\\n             \\n             localStorage.removeItem(item_id);\\n\\n            return false;\\n      });\\n      \\n   \\n      \\n          //add to cart button funcitonality\\n          $(document).ready(function() {\\n            \\n      //       <!-- setting product details on page  -->\\n         \\n            //these function return the get paramteres\\n            function getSearchParameters() {\\n              var prmstr = window.location.search.substr(1);\\n              return prmstr != null && prmstr != \\"\\" ? transformToAssocArray(prmstr) : {};\\n          }\\n\\n          function transformToAssocArray( prmstr ) {\\n              var params = {};\\n              var prmarr = prmstr.split(\\"&\\");\\n              for ( var i = 0; i < prmarr.length; i++) {\\n                  var tmparr = prmarr[i].split(\\"=\\");\\n                  params[tmparr[0]] = tmparr[1];\\n              }\\n              return params;\\n          }\\n          //these function return the get parametres ends\\n            var params = getSearchParameters();\\n            var product_id = params[\'product_id\'];            \\n            var price = params[\'product_price\'];\\n            var product_name = params[\'product_name\'];\\n            var product_name = unescape(product_name);\\n            \\n            var product_info = localStorage.getItem(\'product_info\');\\n            var items_object = JSON.parse(product_info);\\n            product_name = items_object.product_name;\\n            price = items_object.product_price;\\n            var image_path = items_object.image_path;\\n            var product_description = items_object.product_description;\\n            \\n            $(\\".product_price\\").text(price);\\n            $(\\".product_title\\").text(product_name);\\n            $(\\".product_description\\").text(product_description);\\n            \\n            $(\\".add_cart\\").attr(\'product_id\',product_id);\\n            $(\\".add_cart\\").attr(\'product_name\',product_name);\\n            $(\\".add_cart\\").attr(\'product_price\',price);\\n            $(\\".add_cart\\").attr(\'src\',image_path);\\n     //       <!-- setting product details on page ends -->\\n        \\n            \\n          //checking if cart is empty or not \\n          if(localStorage.getItem(\'items\')){\\n            //if not empty then we will enter here and set the items into cart\\n                let nextValue;\\n                    for (let i = 0; i < localStorage.length; i++)\\n                    {\\n                        nextValue = localStorage.key(i);\\n                          if(nextValue != \\"total\\" && nextValue != \\"items\\" && nextValue !=\\"product_info\\" &&nextValue != \\"__paypal_storage__\\")\\n                          {\\n                              var item = localStorage.getItem(nextValue);\\n                                \\n                                  var items_object = JSON.parse(item);\\n                                  var product_id = nextValue; \\n                                  var product_name = items_object.product_name;\\n                                  var product_price = items_object.product_price;\\n//                                   var product_rate = items_object.total;                                 \\n                                var str = String(product_price);\\n                                str = str.replace(\\",\\", \\"\\");\\n                                str = str.replace(\\"$\\", \\"\\");\\n                                var product_rate = parseFloat(str);\\n                            \\n                                var image_path = items_object.image_path;\\n                            \\n                               $(\\"#product_item\\").append(\\"<li class=\'list_id\\"+product_id+\\"\'><div class=\'product_list_row\'><div class=\'product_thum\'><img src=\'\\"+image_path+\\"\' alt=\'\'></div><div class=\'ecm_description\'><h4>\\"+product_name+\\"</h4><span class=\'price\'>\\"+product_price+\\"</span><div class=\'number\'><span class=\'minus\' minus_price=\\"+product_rate+\\" prod_minus=\\"+product_id+\\">-</span><input type=\'text\' disabled value=\\"+items_object.item+\\" /><span class=\'plus\' add_price=\\"+product_rate+\\" prod_add=\\"+product_id+\\">+</span></div></div><div class=\'ecm_close\'><i class=\'fas fa-trash-alt\' item_id=\\"+product_id+\\" id=\'delete\'></i></div></div></li>\\");\\n                             \\n                                \\n                          }\\n                    } \\n                        var total = localStorage.getItem(\'total\');\\n                         total = roundFloat(total,4);\\n                        $(\\".money\\").text(\\" $ \\"+total+\\" USD \\");\\n\\n                        var items = localStorage.getItem(\'items\');\\n                        $(\\"#cart_count\\").text(items);\\n            \\n            \\n             }\\n            else{\\n              localStorage.setItem(\\"total\\",0);\\n              localStorage.setItem(\\"items\\",0);\\n            }\\n           \\n           \\n         \\n          $(\'.add_cart\').click(function() {\\n         \\n            var $this = $(this);\\n            var product_id = $this.attr(\'product_id\');\\n            var product_name = $this.attr(\'product_name\');\\n            var product_price = $this.attr(\'product_price\');\\n           var image_path = $this.attr(\'src\');\\n            //checking if item already in cart or not\\n            var items = localStorage.getItem(product_id);\\n            \\n            if (localStorage.getItem(product_id)) {//if item does exist in cart then we will enter in this if\\n             \\n            var str = String(product_price);\\n            str = str.replace(\\",\\", \\"\\");\\n            str = str.replace(\\"$\\", \\"\\");\\n            var product_rate = parseFloat(str);\\n            var cart_total = localStorage.getItem(\'total\');\\n            var total = parseFloat(cart_total);\\n            \\n            total += product_rate;\\n            total = roundFloat(total,4);\\n            var items = localStorage.getItem(product_id);\\n            var items_object = JSON.parse(items);\\n            items_object.item += 1;\\n            items_object.total += product_rate;\\n             items_object.total = roundFloat(items_object.total,4);\\n            //updating  product and total in local storage  \\n            localStorage.setItem(product_id,JSON.stringify(items_object));\\n            localStorage.setItem(\\"total\\",total);\\n            \\n             //storing item count in local storage\\n            var items = localStorage.getItem(\\"items\\");\\n            var int_items = parseInt(items);\\n            int_items += 1;\\n            localStorage.setItem(\\"items\\",int_items);\\n            //storing item count in local storage\\n\\n             $(\\"#cart_count\\").text(int_items);\\n              \\n            //updating cart item\\n            $(\'.list_id\'+product_id).remove();\\n            $(\\"#product_item\\").append(\\"<li class=\'list_id\\"+product_id+\\"\'><div class=\'product_list_row\'><div class=\'product_thum\'><img src=\'\\"+image_path+\\"\' alt=\'\'></div><div class=\'ecm_description\'><h4>\\"+product_name+\\"</h4><span class=\'price\'>\\"+product_price+\\"</span><div class=\'number\'><span class=\'minus\' minus_price=\\"+product_rate+\\" prod_minus=\\"+product_id+\\">-</span><input type=\'text\' disabled value=\\"+items_object.item+\\" /><span class=\'plus\' add_price=\\"+product_rate+\\" prod_add=\\"+product_id+\\">+</span></div></div><div class=\'ecm_close\'><i class=\'fas fa-trash-alt\' item_id=\\"+product_id+\\" id=\'delete\'></i></div></div></li>\\");\\n            $(\\".money\\").text(\\" $ \\"+total+\\" USD \\");\\n            //updating cart item\\n           }\\n            else{//if item does not exists \\n              var str = String(product_price);\\n            str = str.replace(\\",\\", \\"\\");\\n            str = str.replace(\\"$\\", \\"\\");\\n            var product_rate = parseFloat(str);\\n            var cart_total = localStorage.getItem(\'total\');\\n            var total = parseFloat(cart_total);\\n            \\n            total += product_rate;\\n            total = roundFloat(total,4);\\n            //storing product in local storage\\n            var product_detail = { \'product_id\': product_id, \'product_name\': product_name, \'product_price\': product_price ,\'item\': 1 ,\'total\':product_rate,\'image_path\':image_path};    \\n            localStorage.setItem(product_id,JSON.stringify(product_detail));\\n            localStorage.setItem(\\"total\\",total);\\n            //storing product in local storage\\n         \\n            //storing item count in local storage\\n            var items = localStorage.getItem(\\"items\\");\\n            var int_items = parseInt(items);\\n            int_items += 1;\\n            localStorage.setItem(\\"items\\",int_items);\\n            //storing item count in local storage\\n            \\n            //updating cart count\\n//             var cart_count=$(\'#cart_count\').text();//getting cart count\\n//             var y =  parseInt(cart_count);\\n//             var x=1;\\n//             y += x;\\n            $(\\"#cart_count\\").text(int_items);//adding one and reflecting back count\\n            //updating cart count\\n \\n            //adding cart item\\n          $(\\"#product_item\\").append(\\"<li class=\'list_id\\"+product_id+\\"\'><div class=\'product_list_row\'><div class=\'product_thum\'><img src=\'\\"+image_path+\\"\' alt=\'\'></div><div class=\'ecm_description\'><h4>\\"+product_name+\\"</h4><span class=\'price\'>\\"+product_price+\\"</span><div class=\'number\'><span class=\'minus\' minus_price=\\"+product_rate+\\" prod_minus=\\"+product_id+\\">-</span><input type=\'text\' disabled value=\'1\' /><span class=\'plus\' add_price=\\"+product_rate+\\" prod_add=\\"+product_id+\\">+</span></div></div><div class=\'ecm_close\'><i class=\'fas fa-trash-alt\' item_id=\\"+product_id+\\" id=\'delete\'></i></div></div></li>\\");\\n          $(\\".money\\").text(\\" $ \\"+total+\\" USD \\");\\n              \\n           }\\n          \\n            \\n        });\\n    });\\n   //add to cart button funcitonality   \\n\\n    </script>\\n\\n\\n     \\n\\n</body>\\n\\n</html>"
        		    }',
        		  CURLOPT_HTTPHEADER => array(
        		    'Authorization: Bearer '.$token,
        		    'Content-Type: application/json',
        		    'Cookie: __cfduid=dbee73ed9d0d3ee23fabe7672fb9b848a1614580870'
        		  ),
        		 ));

        		 $response = curl_exec($curl);

        		 curl_close($curl);

    		    
                 //product_detail page ends

                 //checkout page starts
    		
            

                 $curl = curl_init();

                 curl_setopt_array($curl, array(
                  CURLOPT_URL => 'https://api.hubapi.com/content/api/v2/templates',
                  CURLOPT_RETURNTRANSFER => true,
                  CURLOPT_ENCODING => '',
                  CURLOPT_MAXREDIRS => 10,
                  CURLOPT_TIMEOUT => 0,
                  CURLOPT_FOLLOWLOCATION => true,
                  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                  CURLOPT_CUSTOMREQUEST => 'POST',
                  CURLOPT_POSTFIELDS =>'{
                    "category_id": 1,
                    "folder": "example",
                    "is_available_for_new_content": "False",
                    "template_type": 4,        
                    "path":"webdew/checkout",
                    "source":"<!--\\n    templateType: page\\n    isAvailableForNewContent: true\\n-->\\n<!doctype html>\\n<html>\\n  <head>\\n    <meta charset=\\"utf-8\\">\\n    <title>{{ content.html_title }}</title>\\n    <link rel=\\"stylesheet\\" href=\\"https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css\\">\\n    <link rel=\\"stylesheet\\" href=\\"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css\\">\\n    <link href=\\"https://fonts.googleapis.com/css2?family=DM+Sans:wght@500&display=swap\\" rel=\\"stylesheet\\">\\n    <script src=\\"https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js\\"></script>\\n    <script src=\\"https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js\\"></script>\\n   \\n    <link rel=\\"stylesheet\\" href=\\"https://pro.fontawesome.com/releases/v5.10.0/css/all.css\\">\\n    <meta name=\\"description\\" content=\\"{{ content.meta_description }}\\">\\n    {{ standard_header_includes }}\\n    \\n<!--     loader css -->\\n{% require_css %}\\n<style>\\n\\nhtml {\\n  height: 100%;\\n  box-sizing: border-box;\\n}\\nbody {\\n  position: relative;\\n  padding-bottom: 6rem;\\n  min-height: 100%;\\n  \\n} \\n\\n  /*------- header---------*/\\n\\n{% set brand_color = \\"#00a9fa\\" %}\\n\\nimg{ max-width: 100%; }\\n.ecm_header{ width: 100%; display: inline-block; position: relative;  padding: 15px 0; background-color: #ffffff; box-shadow: 0 0px 10px #eee; }\\n.cms_head{ display: flex; flex-wrap: wrap; justify-content: space-between; align-items: center; }\\n.cms_head .logo{ max-width: 180px; }\\n.cms_head .logo img {   width: 100%;}\\n\\n.ecm_product_warp{ padding: 50px 0; }\\n.ecm_product_row{\\n  display: -ms-flexbox;\\n  display: flex;\\n  -ms-flex-wrap: wrap;\\n  flex-wrap: wrap;\\n\\n}\\n\\n.ecm_product_col-3 {\\n -ms-flex: 0 0 25%;\\n  flex: 0 0 25%;\\n  max-width: 25%;\\n  padding: 0 15px;\\n  margin-bottom: 30px;\\n}\\n\\n.ecm_product_img{ background-color: #efefef; padding: 15px; position: relative; }\\na.ecm_view_detail {\\n    display: none;\\n    background-color: {{brand_color}} ;\\n    color: #ffffff;\\n    position: absolute;\\n    bottom: 0;\\n    left: 0;\\n    width: 100%;\\n    padding: 10px 0;\\n    text-align: center;\\n    text-decoration: none;\\n}\\n.ecm_product_box:hover a.ecm_view_detail { display: block; }\\n\\n.ecm_product_detail{ padding: 10px 0; text-align: center; }\\n\\nspan#cart_item {\\n    cursor: pointer;\\n}\\n\\n.ecm_product_list {\\n    /*display: none;*/\\n    position: fixed;\\n    top: 0;\\n    right: -320px;\\n    width: 320px;\\n    background-color: #fff;\\n    z-index: 99;\\n    height: 100%;\\n    overflow: auto;\\n    padding: 25px 0 0 0;\\n    border-left: 1px solid #ccc;    \\n      -webkit-transition: all 1s; \\n      transition: all 1s; \\n      opacity:0;\\n      visibility: hidden;\\n} \\n\\n.ecm_header.menu_fix  .ecm_product_list{\\n      right: 0;\\n      /*display: block;*/\\n      -webkit-transition: all 1s; \\n      transition: all 1s; \\n      visibility: visible;\\n      opacity: 1;\\n}\\n\\ndiv#close_cart {\\n    display: flex;\\n    text-align: right;\\n    justify-content: flex-end;\\n    align-items: center;\\n    margin: 0 25px 10px 0;\\n    border-radius: 2px;\\n       color: {{brand_color}};\\n       cursor: pointer;\\n}\\ndiv#close_cart i.fas {\\n    font-size: 20px;\\n    color: {{brand_color}};\\n    margin: 0 4px 0 0;\\n}\\n.ecm_product_list ul{ margin: 0; padding: 0; }\\n.ecm_product_list ul li {\\n    list-style: none;\\n    padding: 12px 25px;\\n    border-bottom: 1px solid #ccc;\\n}\\n\\n.product_list_row{ \\n   display: flex; \\n    flex-wrap: wrap; \\n\\n    justify-content: space-between;\\n}\\n.ecm_description h4 {\\n    margin: 0;\\n    padding: 0;\\n    font-size: 15px;\\n}\\n\\n.product_thum {\\n    width: 60px;\\n}\\n\\n\\n.eca_product_cart span {cursor:pointer; }\\n.eca_product_cart .number{\\n  margin:5px 0 0 0; text-align: center;\\n }\\n    .eca_product_cart .minus, \\n    .eca_product_cart .plus{\\n      width:20px;\\n      /*height:20px;*/\\n      background:#f2f2f2;\\n      border-radius:4px;\\n      padding:3px 5px 3px 5px;\\n      border:1px solid #ddd;\\n      display: inline-block;\\n      vertical-align: middle;\\n      text-align: center;\\n    }\\n    .eca_product_cart td input{\\n      height:30px;\\n      width: 40px;\\n      text-align: center;\\n      font-size: 15px;\\n      border:1px solid #ddd;\\n      border-radius:4px;\\n      display: inline-block;\\n      vertical-align: middle;\\n      \\n\\n}\\n.ecm_product_list > h4 {\\n    font-size: 14px;\\n    padding: 10px 15px 15px;\\n    border-bottom: 1px solid #ccc;\\n    font-weight: 700;\\n}\\n.ecm_close i.fas.fa-trash-alt{ color: {{brand_color}}; cursor: pointer; }\\n.total_cart {\\n    margin: 15px 0;\\n    width: 100%;\\n}\\n.total {\\n    display: flex;\\n    justify-content: space-between;\\n    padding: 0 15px;\\n}\\n.total span.label{ color: #333333; }\\n.total span.label span {\\n    font-size: 15px;\\n}\\n\\n.total span.money{ color: {{brand_color}}; }\\n.btn-checkout {\\n    padding: 0 15px;\\n    width: 100%;\\n    display: inline-block;\\n    text-align: center;\\n}\\n\\n.btn-checkout a{ \\n   background-color: {{brand_color}};\\n   color: #ffffff;\\n   padding: 12px 0;\\n   width: 100%;\\n   display: inline-block;\\n   border-radius: 30px;\\n   text-decoration: none;\\n   border:2px solid transparent;\\n  }\\n\\n  .btn-checkout a:hover{ \\n   color: {{brand_color}};  \\n   border:2px solid {{brand_color}};\\n   background-color: #ffffff;\\n  }\\n\\n\\n\\n.btn-view-cart {\\n    padding: 0 15px;\\n    width: 100%;\\n    display: inline-block;\\n    text-align: center;\\n    margin-top: 15px;\\n}\\n\\n.btn-view-cart a{ \\n   color: {{brand_color}};\\n   padding: 12px 0;\\n   width: 100%;\\n   display: inline-block;\\n   border-radius: 30px;\\n   border:2px solid {{brand_color}};\\n    text-decoration: none;\\n  }\\n.btn-view-cart a:hover{ background-color: {{brand_color}}; color: #ffffff; }\\n  /*------- header---------*/\\n\\n\\n.lds-roller {\\n  display: inline-block;\\n  position: relative;\\n  width: 80px;\\n  height: 80px;\\n}\\n.lds-roller div {\\n  animation: lds-roller 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;\\n  transform-origin: 40px 40px;\\n}\\n.lds-roller div:after {\\n    content: \\" \\";\\n    display: block;\\n    position: absolute;\\n    width: 7px;\\n    height: 7px;\\n    border-radius: 50%;\\n    background:#00A9FA;\\n    margin: -4px 0 0 -4px;\\n}\\n.lds-roller div:nth-child(1) {\\n    animation-delay: -0.036s;\\n}\\n.lds-roller div:nth-child(1):after {\\n    top: 63px;\\n    left: 63px;\\n}\\n.lds-roller div:nth-child(2) {\\n    animation-delay: -0.072s;\\n}\\n.lds-roller div:nth-child(2):after {\\n    top: 68px;\\n    left: 56px;\\n}\\n.lds-roller div:nth-child(3) {\\n    animation-delay: -0.108s;\\n}\\n.lds-roller div:nth-child(3):after {\\n    top: 71px;\\n    left: 48px;\\n}\\n.lds-roller div:nth-child(4) {\\n  animation-delay: -0.144s;\\n}\\n.lds-roller div:nth-child(4):after {\\n  top: 72px;\\n  left: 40px;\\n}\\n.lds-roller div:nth-child(5) {\\n  animation-delay: -0.18s;\\n}\\n.lds-roller div:nth-child(5):after {\\n  top: 71px;\\n  left: 32px;\\n}\\n.lds-roller div:nth-child(6) {\\n  animation-delay: -0.216s;\\n}\\n.lds-roller div:nth-child(6):after {\\n  top: 68px;\\n  left: 24px;\\n}\\n.lds-roller div:nth-child(7) {\\n  animation-delay: -0.252s;\\n}\\n.lds-roller div:nth-child(7):after {\\n  top: 63px;\\n  left: 17px;\\n}\\n.lds-roller div:nth-child(8) {\\n  animation-delay: -0.288s;\\n}\\n.lds-roller div:nth-child(8):after {\\n  top: 56px;\\n  left: 12px;\\n}\\n@keyframes lds-roller {\\n  0% {\\n    transform: rotate(0deg);\\n  }\\n  100% {\\n    transform: rotate(360deg);\\n  }\\n}\\n.loader-wrapper {\\n  width: 100%;\\n  height: 100%;\\n  position: absolute;\\n  top: 0;\\n  left: 0;\\n  background-color: #ffffffba;\\n  display:flex;\\n  justify-content: center;\\n  align-items: center;\\n  z-index:99999999999;\\n}\\n  \\n    a.confirm_order.btn.hidden{\\n      display:none;\\n  }\\n  \\n@import url(\'https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap\');\\n.module-wrap{\\nfont-family: \'Montserrat\', sans-serif;\\npadding-bottom:60px;\\n}\\nbody{\\nmargin:0;\\n}\\nh1.page-heading {\\n    margin: 0 0 30px;\\n    font-size: 30px;\\n    color: #333;\\n}\\n/* left side start here */\\n.tabber-wrap .tabber-tabs {\\n    border-bottom: 1px solid #e9e9e9;\\n    list-style: none;\\n    padding: 0;\\n    margin: 0;\\n}\\n.tabber-wrap .tabber-tabs > li {\\n    float: left;\\n}\\n.tabber-wrap .tabber-tabs > li > a {\\n    line-height: 20px;\\n    text-decoration: none;\\n    display: inline-block;\\n    color:#333;\\n    font-size:16px;\\n}\\n.tabber-wrap .tabber-tabs > li > span{\\n    display:inline-block;\\n    padding:0 15px;\\n}\\n.tabber-wrap .tabber-tabs > .active > a, \\n.tabber-wrap .tabber-tabs > .active > a:hover, \\n.tabber-wrap .tabber-tabs > .active > a:focus {\\n    cursor: default;\\n    color:#00A9FA;\\n}\\n.tabber-wrap .tabber-content .tab-pane.active:nth-child(1) {\\n    border-top-left-radius: 0;\\n}\\n.tabber-wrap .tabber-content .block h3 {\\n  display: none;\\n}\\n.tabber-wrap .tabber-content .tab-pane:nth-child(1) {\\n  border-top-left-radius: 0;\\n}\\n.tabber-wrap .tabber-content .tab-pane {\\n  background: #fff;\\n  border: 0; \\n  border-radius: 4px; \\n  display: block; \\n  height: 0; \\n  opacity: 0;\\n  overflow: hidden; \\n  transition: opacity 400ms;\\n}\\n.tabber-wrap .tabber-content .tab-pane.active {\\n  height: auto; \\n  opacity: 1;\\n  overflow: visible; \\n  padding: 8px;\\n}\\n\\n\\n@media screen and (max-width:991px){\\n  a.hs-rss-title span {\\n    font-size: 22px;\\n  }\\n}\\n@media screen and (max-width:767px){\\n  a.hs-rss-title span {\\n    font-size: 20px;\\n}\\n\\n}\\n  @media screen and (max-width:567px){\\n  a.hs-rss-title span {\\n    font-size: 17px;\\n}\\n    .hs-rss-item.hs-with-featured-image .hs-rss-item-text {\\n    line-height: 22px;\\n}\\n}\\n\\n\\n/* shipping form start here */\\n\\n\\nform.shipping-payment-form input {\\n    width: 100%;\\n    height: 40px;\\n    padding: 0 12px;\\n    outline: none;\\n    margin-bottom: 10px;\\n    box-sizing: border-box;\\n    border-radius:6px;\\n    border:none;\\n    border:2px solid #7a7a7a33;\\n    color:#7a7a7a;\\n}\\nform.shipping-payment-form input:focus{\\n   border:2px solid #00A9FA;\\n}\\nlabel.error {\\n    color: red;\\n    font-size: 14px;\\n    margin-bottom: 5px;\\n    display: block;\\n}\\ninput[type=\\"checkbox\\"] {\\n    width: 16px !important;\\n    height: 16px !important;\\n    display: inline-block;\\n    margin: 0 8px 0 0 !important;\\n    vertical-align: middle;\\n}\\n.name {\\n    display: flex;\\n    justify-content: space-between;\\n}\\n\\n.name .name-field-wrap {\\n    width: 49% !important;\\n}\\n\\n.field-wrap h2 {\\n    font-weight: 400;\\n    font-size: 22px;\\n    margin: 40px 0 20px;\\n}\\n.custom-continue-button button {\\n    font-size: 15px;\\n    text-transform: uppercase;\\n    outline: none;\\n    text-decoration: none;\\n    display: inline-block;\\n    background: #00A9FA;\\n    padding: 12px 15px;\\n    color: #fff;\\n    transition:0.3s;\\n    border:none;\\n}\\n.custom-continue-button a:hover {\\n    box-shadow: 0 0 2px 0 #7a7a7a;\\n}\\n.custom-continue-button a {\\n    font-size: 15px;\\n    text-transform: uppercase;\\n    outline: none;\\n    text-decoration: none;\\n    display: inline-block;\\n    background: #00A9FA;\\n    padding: 12px 15px;\\n    color: #fff;\\n    transition:0.3s;\\n    border:none;\\n}\\n.custom-continue-button button:hover {\\n    box-shadow: 0 0 2px 0 #7a7a7a;\\n}\\n\\n.custom-continue-button {\\n    padding: 20px 0;\\n    display: flex;\\n    justify-content: space-between;\\n}\\nul.shipping-details-list a {\\n    display: block;\\n    width: 15%;\\n    font-size: 16px;\\n}\\n\\nul.shipping-details-list p {\\n    width: 70%;\\n    font-size: 16px;\\n    margin:0;\\n}\\nul.shipping-details-list span {\\n    display: block;\\n    width: 15%;\\n    font-size: 16px;\\n}\\nul.shipping-details-list li {\\n    display: flex;\\n    justify-content: left;\\n    align-items: center;\\n    width: 100%;\\n    border-top: 1px solid #7a7a7a33;\\n    padding:15px 0;\\n}\\nul.shipping-details-list li:first-child{\\n   border:none;\\n}\\nul.shipping-details-list {\\n    padding:0 15px;\\n    border: 2px solid #7a7a7a33;\\n    margin-top:40px;\\n    border-radius:6px;\\n}\\nul.shipping-details-list li a {\\n    color: #00A9FA;\\n    text-decoration:none;\\n}\\nul.shipping-details-list h4 {\\n   text-align: right;\\n    margin: 0 0 0 auto;\\n    width: 15%;\\n}\\nul.shipping-details-list input {\\n    margin-right: 10px;\\n}\\n.payment-acceptance p {\\n    font-size: 16px;\\n    margin: 15px 0;\\n}\\n.payment-acceptance figure img {\\n    width: 100%;\\n}\\n.payment-acceptance figure {\\n    margin: 0 auto;\\n    width: 100px;\\n    height: 100px;\\n}\\n.payment-acceptance {\\n    padding: 20px;\\n    text-align: center;\\n    background: #fafafa;\\n    border-radius: 6px;\\n}\\n/* right side start here */\\n\\n.module-wrap {\\n    display: flex;\\n    justify-content: space-between;\\n}\\n.right-content {\\n    width: 40%;\\n    background: #fafafa;\\n    padding: 20px 30px 30px;\\n    height: 80vh;\\n}\\nul.clearfix.tabber-tabs {\\n    border: none;\\n}\\n.left-content {\\n    width: 60%;\\n    padding: 20px 30px 30px;\\n}\\nfigure.cart-product-img {\\n    width: 60px;\\n    margin: 0;\\n}\\nfigure.cart-product-img img{\\n   width:100%;\\n\\n}\\n.cart-products {\\n    display: flex;\\n    justify-content: left;\\n    align-items: center;\\n}\\n.cart-products h4 {\\n    font-size: 16px;\\n    margin: 0;\\n    padding: 0 15px;\\n}\\nul.total-billing li {\\n    display: flex;\\n    justify-content: space-between;\\n    align-items: center;\\n}\\nul.total-billing{\\n   padding:0;\\n}\\nul.total-billing h6 {\\n    font-size: 16px;\\n    margin:0;\\n}\\nul.charges {\\n    width: 100%;\\n    padding: 0;\\n    border-top: 1px solid #7a7a7a33;\\n    border-bottom: 1px solid #7a7a7a33;\\n    padding: 15px 0;\\n}\\nul.charges li {\\n    display: flex;\\n    justify-content: space-between;\\n}\\nul.charges li {\\n    line-height: 0px;\\n}\\nul.total-billing p, ul.total-billing small {\\n    color: #7a7a7a;\\n}\\n.container {\\n    max-width: 1200px;\\n    margin: 0 auto;\\n    padding:0 15px;\\n}\\n\\n/* dropdown */\\n\\n/*the container must be positioned relative:*/\\n.custom-select {\\n  position: relative;\\n}\\n\\n.custom-select select {\\n  display: none; /*hide original SELECT element:*/\\n}\\n\\n.select-selected {\\n  background-color: #fff;\\n}\\nul.shipping-details-list li a:hover {\\n    text-decoration: underline;\\n}\\n.payment-acceptance .btn-brand {\\n    background-color: #00a9fa;\\n    border: none;\\n    color: #fff;\\n    outline: none;\\n}\\n.payment-acceptance .btn-brand:hover{\\n  background-color :#04a0eb;\\n}\\n/*style the arrow inside the select element:*/\\n.select-selected:after {\\n  position: absolute;\\n  content: \\"\\";\\n  top: 18px;\\n  right: 16px;\\n  width: 0;\\n  height: 0;\\n  border: 6px solid transparent;\\n  border-color: #A59F9F transparent transparent transparent;\\n}\\n\\n/*point the arrow upwards when the select box is open (active):*/\\n.select-selected.select-arrow-active:after {\\n  border-color: transparent transparent #A59F9F transparent;\\n  top: 11px;\\n}\\n.select-selected{\\nborder:2px solid #7a7a7a33;\\n}\\n/*style the items (options), including the selected item:*/\\n.select-items div,.select-selected {\\n  color: #A59F9F;\\n  padding: 10px 12px;\\n  cursor: pointer;\\n  user-select: none;\\n  height:40px;\\n  border-radius:8px;\\n  font-size:14px;\\n  font-weight:400;\\n  box-sizing: border-box;\\n}\\n\\n/*style items (options):*/\\n.select-items {\\n  position: absolute;\\n  background-color: #fff;\\n  top: 100%;\\n  left: 0;\\n  right: 0;\\n  z-index: 99;\\n  border:2px solid #7a7a7a33;\\n}\\n\\n/*hide the items when the select box is closed:*/\\n.select-hide {\\n  display: none;\\n}\\n\\n.select-items div:hover, .same-as-selected {\\n  background-color: #00A9FA;\\n  color:#fff;\\n}\\n\\n\\n.location-region {\\n    display: flex;\\n    justify-content: space-between;\\n    padding-bottom:10px;\\n}\\nlabel.error {\\n    color: red;\\n}\\n\\n.cart-products h4 {\\n    padding: 10px 0;\\n}\\n\\n.eca_checkout_wrap{ padding-top: 50px; }\\n\\n/*footer*/\\n.ecm_footer{ \\n  position: absolute;\\n  right: 0;\\n  bottom: 0;\\n  left: 0;\\n  background-color: {{ brand_color }};     padding: 15px 0; \\n}\\n.ecm_footer_copy_right {  text-align: center;}\\n.ecm_footer_copy_right p { color: #ffffff;     margin: 0;}\\n\\n@media screen and (max-width:767px){\\n .module-wrap {\\n    flex-wrap: wrap;\\n}\\n  .left-content {\\n    width: 100%;\\n    padding:0px;\\n}\\n  .right-content{\\n    width:100%;\\n  }\\n  .tabber-wrap .tabber-tabs > li > span {\\n    padding: 0 6px;\\n}\\n  .tabber-wrap .tabber-tabs>li>a {\\n    font-size: 14px;\\n    padding-left: 0;\\n    padding-right: 0;\\n}\\n  .name .name-field-wrap {\\n    width: 100% !important;\\n}\\n.dropdown-item-menu {\\n    width: 100% !important;\\n}\\n  .location-region {\\n    flex-wrap: wrap;\\n}\\n  .name {\\n    flex-wrap: wrap;\\n}\\n  ul.shipping-details-list li {\\n    padding: 15px 0;\\n    flex-direction: column;\\n    justify-content: start;\\n    align-items: flex-start;\\n}\\n  ul.shipping-details-list span, ul.shipping-details-list p, ul.shipping-details-list a {\\n    width: 100%;\\n    font-size:15px;\\n}\\n}\\n\\n/* modal */\\n.modal-window {\\n   position: fixed;\\n   background-color:#2c2e2f99;\\n   top: 0;\\n   right: 0;\\n   bottom: 0;\\n   left: 0;\\n   z-index: 999;\\n   visibility: hidden;\\n   opacity: 0;\\n   pointer-events: none;\\n   transition: all 0.3s;\\n}\\n .modal-window:target {\\n   visibility: visible;\\n   opacity: 1;\\n   pointer-events: auto;\\n}\\n .modal-window > div {\\n   width: 400px;\\n   position: absolute;\\n   top: 50%;\\n   left: 50%;\\n   transform: translate(-50%, -50%);\\n   padding: 2em;\\n   background: #fff;\\n   transition:0.3s ease-in-out;\\n}\\na.modal-close {\\n    text-decoration: none;\\n    background: #00A9FA;\\n    display: inline-block;\\n    color: #fff;\\n    padding: 4px 10px 6px;\\n    margin-top: 10px;\\n    font-size: 14px;\\n}\\n .modal-window header {\\n   font-weight: bold;\\n}\\n .modal-window h1 {\\n   font-size: 150%;\\n   margin: 0 0 15px;\\n}\\n .modal-close:hover {\\n   background:#00a2ef;\\n}\\n/* Demo Styles */\\n a {\\n   color: inherit;\\n}\\n .modal-window div:not(:last-of-type) {\\n   margin-bottom: 15px;\\n}\\n .btn {\\n   background-color: #00A9FA;\\n   color:#fff;\\n   padding: 1em 1.5em;\\n   border-radius: 3px;\\n   text-decoration: none;\\n}\\n.btn{\\n   background-color:#00a2ef;\\n}\\n.modal-box {\\n    box-shadow: 0 0 7px 0 #00000012;\\n}\\n .btn i {\\n   padding-right: 0.3em;\\n}\\n  .back_arrow_btn {\\n    padding: 20px 15px;\\n    width: 100%;\\n    display: inline-block;\\n}\\n\\n.back_arrow_btn a{\\n   color: {{brand_color}}; text-decoration: none;\\n} \\n \\n\\n  </style>\\n  {% end_require_css %}\\n\\n\\n  </head>\\n  <body>\\n    \\n<!--     loader start here -->\\n    <div class=\\"loader-wrapper\\">\\n    <div id=\\"loader\\" class=\\"lds-roller\\"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>\\n        </div>\\n       <script>\\n        $(window).on(\\"load\\",function(){\\n          $(\\".loader-wrapper\\").fadeOut(\\"slow\\");\\n        });\\n      </script>\\n   \\n<!--     loader end here -->\\n\\n<div class=\\"ecm_header\\">\\n        <div class=\\"container\\">\\n            <div class=\\"cms_head\\">\\n                <div class=\\"logo\\">\\n                    <a href=\\"#\\">\\n                        <img id=\\"img_portal_id\\" portal_id=\\"{{ portal_id }}\\" src=\\"\\" alt=\\"Brand-logo\\">\\n                    </a>\\n                </div>\\n                \\n            </div>\\n        </div>\\n    </div>\\n\\n    \\n    \\n    <!--   E-cart Module   -->\\n   <script src=\\"https://unpkg.com/sweetalert/dist/sweetalert.min.js\\"></script>\\n    <button hidden link=\\"{{ get_asset_url(\'/webdew/thank_you\') }}\\" id=\\"product_page\\"></button>\\n   <div class=\\"eca_checkout_wrap\\">\\n    <div class=\\"container\\">\\n         <div class=\\"back_arrow_btn\\">\\n             <a href=\\"{{ get_asset_url(\'/webdew/cart\') }}\\"><i class=\\"fas fa-long-arrow-alt-left\\"></i> Back</a>\\n          </div>\\n            <div class=\\"module-wrap\\">\\n                <div class=\\"left-content\\">\\n                    <h1 class=\\"page-heading\\">\\n                        Web Dev Store\\n                    </h1>\\n                    <div class=\\"tabber-wrap blog-tabber\\">\\n                        <ul class=\\"clearfix tabber-tabs\\">\\n<!--                             <li class=\\"custom-tab\\"><a href=\\"#tab-0\\">Cart</a><span>></span></li> -->\\n                            <li class=\\"custom-tab\\"><a href=\\"/cart_web_page\\">Cart</a><span>></span></li>\\n                            <li class=\\"custom-tab active\\"><a href=\\"#tab-1\\" id=\\"information\\">Information</a><span>></span></li>\\n                            <li class=\\"custom-tab\\"><a id=\\"shipping\\">Shipping</a><span>></span></li>\\n                            <li class=\\"custom-tab\\"><a id=\\"payment\\">Payment</a></li>\\n                        </ul>\\n                        <div class=\\"tabber-content\\">\\n                            \\n<!--                             <form class=\\"shipping-payment-form\\" name=\\"user-info\\" role=\\"form\\" action=\\"https://apps.webdew.com/hubspot/ecommerce/stripePay\\" method=\\"POST\\" id=\\"payment-form\\">   -->\\n                          <form class=\\"shipping-payment-form\\" name=\\"user-info\\" role=\\"form\\" id=\\"payment-form\\">  \\n                              <input type=\\"hidden\\" name=\\"portal_id\\" id=\\"portal_id\\" value=\\"{{portal_id}}\\">\\n                              <input type=\\"hidden\\" name=\\"amount\\" id=\\"amount\\" value=\\"\\">\\n                                <div class=\\"hidden_list hidden\\">\\n                                </div>\\n<!--                                 <div class=\\"tab-pane\\" id=\\"tab-0\\">\\n                                    <div class=\\"products-cart-wrap\\">\\n                                      <table style=\\"width:100%\\" >\\n                                          <tr >\\n                                              <th width=\\"55%\\">Product</th>\\n                                              <th width=\\"15%\\">Price</th>\\n                                              <th width=\\"15%\\">quantity</th>\\n                                              <th width=\\"15%\\">Total</th>\\n                                          </tr>\\n                                      </table>\\n                                      <table id=\\"list1\\"  style=\\"width:100%\\">\\n                                      </table>\\n                                  </div>\\n                                </div> -->\\n                                <div class=\\"tab-pane active\\" id=\\"tab-1\\">\\n                                    <div class=\\"shipping-form-wrap\\">\\n                                        <div class=\\"field-wrap\\">\\n                                            <h2>Contact Information</h2>\\n                                            <input type=\\"email\\" name=\\"email_mobile\\" id=\\"email-mobile\\" Placeholder=\\"Email \\" required><br>\\n                                            <!-- <input type=\\"checkbox\\" name=\\"check\\">\\n                                                <label>\\n                                                Keep me up to date on news and exclusive offers\\n                                                </label> \\n                                            -->\\n                                        </div>\\n                                        <div class=\\"field-wrap\\">\\n                                            <h2>Shipping address</h2>\\n                                            <div class=\\"name\\">\\n                                              <div class=\\"name-field-wrap\\">\\n                                                \\n                                      \\n                                                <input type=\\"text\\" name=\\"fname\\" id=\'first-name\' Placeholder=\\"First name\\" required>\\n                                                  <p id=\'first-name-error\'></p>\\n                                                        </div>\\n                                                 <div class=\\"name-field-wrap\\">\\n                                                <input type=\\"text\\" name=\\"lname\\" id=\'last-name\' Placeholder=\\"Last name (Optional)\\" ><span id=\'last-name-error\'></span>\\n                                                  </div>  </div>\\n                                             \\n                                            <input type=\\"text\\" name=\\"address\\" Placeholder=\\"Address\\" id=\'address\' required><span id=\'address-error\'></span>\\n                                            <input type=\\"text\\" name=\\"apartment\\" Placeholder=\\"Apartment,suite,etc (Optional)\\" id=\'apartment\'><span id=\'apartment-error\'></span>\\n                                            <input type=\\"text\\" name=\\"city\\" Placeholder=\\"City\\" id=\'city\' required><span id=\'city-error\'></span>\\n                                            <div class=\\"location-region\\">\\n                                                <div style=\\"width:32%;\\" class=\\"dropdown-item-menu\\">\\n                                                    <div class=\\"custom-select\\">\\n                                                        <input required id=\\"country\\" name=\\"country\\" placeholder=\\"Enter Country\\">\\n                                                    </div>\\n                                                </div>\\n                                                <div style=\\"width:32%;\\" class=\\"dropdown-item-menu\\">\\n                                                    <div class=\\"custom-select\\">\\n                                                        <input name=\\"state\\" id=\\"state\\" required=\\"\\" placeholder=\\"Enter State\\">\\n                                                    </div>\\n                                                </div>\\n                                                 <div style=\\"width:32%;\\" class=\\"dropdown-item-menu\\">\\n                                                    <div class=\\"custom-select\\">\\n                                                <input type=\\"text\\" name=\\"pin_portal\\" placeholder=\\"PIN code\\" id=\\"pin\\">\\n                                           </div>\\n                                                </div>\\n                                                   </div>\\n                                            <!-- <input type=\\"checkbox\\" name=\\"check\\">\\n                                            <label>\\n                                            Save this information for next time\\n                                            </label> -->\\n                                        </div>\\n                                        <div class=\\"custom-continue-button\\">\\n                                            <!-- <a href=\\"#\\">Back</a> -->\\n                                            <button id=\'user-info\'>Continue</button> \\n                                          <!--        success modal -->\\n    <div class=\\"interior\\">\\n    <a class=\\"confirm_order btn hidden\\" href=\\"#open-modal\\">Confirm Order</a>\\n  </div>\\n\\n\\n<div id=\\"open-modal\\" class=\\"modal-window\\">\\n  <div class=\\"modal-box\\">\\n    <h1>Successfully Booked</h1>\\n    <div>Your order is successfully done!</div>\\n     <a href=\\"#\\" title=\\"Close\\" class=\\"modal-close\\">OK</a>\\n    </div>\\n</div>\\n<!-- end success modal -->\\n                                        </div>\\n                                    </div>\\n                                </div>\\n                                <div class=\\"tab-pane\\" id=\\"tab-2\\">\\n                                    <ul class=\\"shipping-details-list\\">\\n                                        <li>\\n                                            <span>\\n                                            Contact\\n                                            </span>\\n                                            <p class=\'contact_append\'> </p>\\n                                            <a class=\\"change_back\\" rel=\\"1\\" href=\\"javascript:void(0);\\">\\n                                            change\\n                                            </a>\\n                                        </li>\\n                                        <li>\\n                                            <span>\\n                                            Ship to\\n                                            </span>\\n                                            <p class=\\"shipping_append\\"> </p>\\n                                            <a class=\\"change_back\\" rel=\\"1\\" href=\\"javascript:void(0);\\">\\n                                            change\\n                                            </a>\\n                                        </li>\\n                                    </ul>\\n                                    <div class=\\"field-wrap\\">\\n                                        <!-- <h2>Shipping method</h2>\\n                                        <ul class=\\"shipping-details-list\\" style=\\"margin-top:0;\\">\\n                                            <li>\\n                                                <input type=\\"radio\\" name=\\"standard\\" checked=\\"checked\\">\\n                                                <p>\\n                                                    Card\\n                                                </p>\\n                                                <h4>\\n                                                    Free\\n                                                </h4>\\n                                            </li>\\n                                        </ul> -->\\n                                    </div>\\n                                    <div class=\\"custom-continue-button\\">\\n                                        <a class=\\"change_back\\" rel=\\"1\\" href=\\"javascript:void(0);\\">Back</a>\\n                                        <a href=\\"javascript:void(0);\\" id=\\"shipping_next\\" rel=\\"3\\">Continue</a>\\n                                    </div>\\n                                </div>\\n                                <div class=\\"tab-pane\\" id=\\"tab-3\\">\\n                                    <ul class=\\"shipping-details-list\\">\\n                                        <li>\\n                                            <span>\\n                                            Contact\\n                                            </span>\\n                                            <p class=\\"contact_append\\"> </p>\\n                                            <a class=\\"change_back\\" rel=\\"1\\" href=\\"javascript:void(0);\\">\\n                                            change\\n                                            </a>\\n                                        </li>\\n                                        <li>\\n                                            <span>\\n                                            Ship to\\n                                            </span>\\n                                            <p class=\\"shipping_append\\"></p>\\n                                            <a class=\\"change_back\\" rel=\\"1\\" href=\\"javascript:void(0);\\">\\n                                            change\\n                                            </a>\\n                                        </li>\\n                                        <li>\\n                                            <span>\\n                                            Method\\n                                            </span>\\n                                            <p>Card</p>\\n                                        </li>\\n                                    </ul>\\n                                    <div class=\\"field-wrap\\">\\n                                        <h2>Payment</h2>\\n                                        <div class=\\"payment-acceptance\\">\\n                                          \\n<!--                                             <figure>\\n                                                <img src=\\"{{ module.product_count_image.src }}\\" alt=\\"{{ module.product_count_image.alt }}\\" {{ loadingAttr }} {{ sizeAttrs }}>\\n                                            </figure> -->\\n                                            \\n                                        <!-- stripe -->\\n                                        \\n                                            <input \\n                                                            type=\\"submit\\"\\n                                                            class=\\"btn btn-brand btn-upper btn-block\\" \\n                                                            value=\\"Pay Now with Card\\"\\n                                                            data-key=\\"\\"\\n                                                            data-amount=\\"\\"\\n                                                            data-currency=\\"usd\\"\\n                                                            data-name=\\"Ecommerce\\"\\n                                                            data-email=\\"\\"\\n                                                            id=\\"stripe\\"\\n                                                        />\\n                                        \\n                                            <script src=\\"https://checkout.stripe.com/v2/checkout.js\\"></script>\\n                                            <script type=\\"text/javascript\\">\\n                                              var cors_api_url = \'https://cors-anywhere.herokuapp.com/\';\\n                                              function doCORSRequest(options, printResult) {\\n                                                var x = new XMLHttpRequest();\\n                                                x.open(options.method, cors_api_url + options.url);\\n                                                x.onload = x.onerror = function() {\\n                                                  var result = {};\\n                                                  result.method =  options.method\\n                                                  result.url = options.url;\\n                                                  result.status = x.status;\\n                                                  result.text = x.statusText;\\n                                                  result.responseText = x.responseText;\\n                                                  printResult(result);\\n\\n                                                };\\n                                                if (/^POST/i.test(options.method)) {\\n                                                    x.setRequestHeader(\'Content-Type\', options.content_type);\\n                                                } else if (/^PUT/i.test(options.method)) {\\n                                                    x.setRequestHeader(\'Content-Type\', options.content_type);\\n                                                    x.setRequestHeader(\\"x-requested-with\\", \\"XMLHttpRequest\\");\\n                                                } else {\\n                                                    x.setRequestHeader(\'Content-Type\', options.content_type);\\n                                                }\\n                                                x.send(options.data);\\n                                              }\\n                                              //restart the local storage\\n                                              function ClearAll() {\\n                                                sessionStorage.clear();\\n                                                localStorage.clear();\\n                                              }\\n                                              $(document).ready(function() {\\n                                                  $(document).on(\'click\',\'#stripe\' ,function(event) {\\n                                                      event.preventDefault();\\n                                                      var $button = $(this),\\n                                                          $form = $button.parents(\'form\');\\n                                                      var opts = $.extend({\\n                                                          amount: $(\'#amount\').val(),\\n                                                          currency: \'usd\',\\n                                                          email : $(\'#email-mobile\').val()\\n                                                      }, $button.data(), {\\n                                                          token: function(result) {\\n                                                             $(\\".loader-wrapper\\").fadeIn(\\"slow\\");\\n                                                             //console.log(result);\\n                                                             $form.append($(\'<input>\').attr({ id: \\"stripeToken\\", type: \'hidden\', name: \'stripeToken\', value: result.id }));\\n                                                            \\n                                                            \\n                                                            $.ajax ({\\n                                                type: \\"POST\\",\\n                                                //url: \'https://apps.webdew.com/hubspot/ecommerce/stripePay?dat=\'+ form.serialize() +\'&portal_id=\'+$(\'#portal_id\').val()+\'&local=\' + loca ,\\n                                                url: \'https://apps.webdew.com/hubspot/ecommerce/stripePay?status=true&\'+$(\'#payment-form\').serialize(),\\n                                                dataType: \'jsonp\',\\n                        //                         headers: {\\n                        //                           \'Authorization\': \\"Basic \\" + btoa(\\"myusername\\" + \\":\\" + \\"mypassword\\")\\n                        //                         },\\n                                                success: function (data, status){\\n                                                  console.log(data);\\n                                                   if(data.status === true){\\n                                                                swal(data.message);\\n                                                                ClearAll();\\n                                                                var base_url = window.location.origin;\\n                                                                var product_page_link = $(\\"#product_page\\").attr(\\"link\\");\\n                                                                \\n                                                                console.log(base_url);\\n                                                                window.location.replace(product_page_link);\\n                                                              } else {\\n                                                                   swal(data.message);\\n                                                              }\\n                                                              $(\\".loader-wrapper\\").fadeOut(\\"slow\\");\\n                                                },\\n                                                error: function(jqXhr, textStatus, errorMessage){\\n                                                  console.log(textStatus);\\n                                                  console.log(errorMessage);\\n                                                }\\n                                              });\\n                                              \\n//                                                             doCORSRequest({\\n//                                                               method: \'GET\',\\n//                                                               content_type :\'application/json\',\\n//                                                               url:\'https://apps.webdew.com/hubspot/ecommerce/stripePay?portal_id=\'+$(\'#portal_id\').val()+\'&\'+$(\'#payment-form\').serialize(),\\n//                                                             }, function printResult(result) {\\n//                                                               result = JSON.parse(result.responseText);\\n//                                                               if(result.status === true){\\n//                                                                 swal(result.message);\\n//                                                                 ClearAll();\\n//                                                               } else {\\n//                                                                    swal(result.message);\\n//                                                               }\\n//                                                               $(\\".loader-wrapper\\").fadeOut(\\"slow\\");\\n//                                                             });\\n                                                          }\\n                                                      });\\n                                                      StripeCheckout.open(opts);\\n                                                  });\\n                                              });\\n                                              </script>\\n                                        \\n                                        <!-- stripe -->\\n\\n\\n                                        <!-- Paypal -->\\n\\n                                             <div class=\\"wd_proceed-payment\\">\\n                                                <div id=\\"paypal-button-container\\"></div>\\n                                                <script id=\\"paypalScrpt\\" src=\\"https://www.paypal.com/sdk/js?client-id=sb&amp;currency=USD\\" data-sdk-integration-source=\\"button-factory\\"></script>\\n                                                <script>\\n                                                  function runLoad(){\\n                                                      // Render the PayPal button into #paypal-button-container\\n                                                      paypal.Buttons({\\n                                                        // Set up the transaction\\n                                                        style: {\\n                                                            layout : \'horizontal\',\\n                                                            color  : \'gold\',\\n                                                            shape  : \'rect\',\\n                                                            height :  38,\\n                                                            tagline: \'false\',\\n                                                            label : \'pay\'\\n                                                        },\\n                                                        createOrder: function(data, actions) {\\n                                                            return actions.order.create({\\n                                                                purchase_units: [{\\n                                                                    amount: {\\n                                                                        value: $(\'#amount\').val(),\\n                                                                    }\\n                                                                }]\\n                                                            });\\n                                                        },\\n                                                        // Finalize the transaction\\n                                                        onApprove: function(data, actions) {\\n                                                            $(\\".loader-wrapper\\").fadeIn(\\"slow\\");\\n                                                            return actions.order.capture().then(function(details) {\\n                                                                var payment_id = details.id;\\n                                                                var payment_status = details.status;\\n                                                                var address1 = details.payer.address.address_line_1;\\n                                                                var adminarea1 = details.payer.address.admin_area_1;\\n                                                                var adminarea2 = details.payer.address.admin_ares_2;\\n                                                                var postal_code = details.payer.address.postal_code;\\n                                                                var country_code = details.payer.address.country_code;\\n                                                                var address = details.payer.email_address;\\n                                                                var given_name = details.payer.name.given_name;\\n                                                                var surname = details.payer.name.surname;\\n                                                                var payer_id = details.payer.payer_id;\\n                                                              \\n//                                                                 doCORSRequest({\\n//                                                                   method: \'GET\',\\n//                                                                   content_type :\'application/json\',\\n//                                                                   url:\'https://apps.webdew.com/hubspot/ecommerce/PayPal?surname=\'+surname+\'&given_name=\'+given_name+\'&country_code=\'+country_code+\'&address=\'+address1+\'&adminarea1=\'+adminarea1+\'&adminarea2=\'+adminarea2+\'&payer_id=\'+payer_id+\'&payment_id=\'+payment_id+\'&payment_status=\'+payment_status+\'&postal_code=\'+postal_code+\'&portal_id=\'+$(\'#portal_id\').val()+\'&\'+$(\'#payment-form\').serialize(),\\n//                                                                 }, function printResult(result) {\\n//                                                                     result = JSON.parse(result.responseText);\\n//                                                                     if(result.status === true){\\n//                                                                         swal(result.message);\\n//                                                                         ClearAll();\\n//                                                                     } else {\\n//                                                                         swal(result.message);\\n//                                                                     }\\n//                                                                     $(\\".loader-wrapper\\").fadeOut(\\"slow\\");\\n//                                                                 });\\n//                                                                 \\n                                                                                     $.ajax ({\\n                                                type: \\"POST\\",\\n                                                //url: \'https://apps.webdew.com/hubspot/ecommerce/stripePay?dat=\'+ form.serialize() +\'&portal_id=\'+$(\'#portal_id\').val()+\'&local=\' + loca ,\\n                                                url:\'https://apps.webdew.com/hubspot/ecommerce/PayPal?surname=\'+surname+\'&given_name=\'+given_name+\'&country_code=\'+country_code+\'&address=\'+address1+\'&adminarea1=\'+adminarea1+\'&adminarea2=\'+adminarea2+\'&payer_id=\'+payer_id+\'&payment_id=\'+payment_id+\'&payment_status=\'+payment_status+\'&postal_code=\'+postal_code+\'&portal_id=\'+$(\'#portal_id\').val()+\'&\'+$(\'#payment-form\').serialize(),\\n                                                dataType: \'jsonp\',\\n                        //                         headers: {\\n                        //                           \'Authorization\': \\"Basic \\" + btoa(\\"myusername\\" + \\":\\" + \\"mypassword\\")\\n                        //                         },\\n                                                success: function (data, status){\\n                                                  console.log(data);\\n                                                   if(data.status === true){\\n                                                                swal(data.message\\n                                                                  );\\n                                                                ClearAll();\\n                                                                var base_url = window.location.origin;\\n                                                                console.log(base_url);\\n                                                               var product_page_link = $(\\"#product_page\\").attr(\\"link\\");\\n                                                                \\n                                                                console.log(base_url);\\n                                                                window.location.replace(product_page_link);\\n                                                              } else {\\n                                                                   swal(data.message);\\n                                                              }\\n                                                              $(\\".loader-wrapper\\").fadeOut(\\"slow\\");\\n                                                },\\n                                                error: function(jqXhr, textStatus, errorMessage){\\n                                                  console.log(textStatus);\\n                                                  console.log(errorMessage);\\n                                                }\\n                                              });\\n                                                                \\n                                                            });\\n                                                        }\\n                                                    }).render(\'#paypal-button-container\');\\n                                                  }\\n                                                </script> \\n                                            </div>\\n\\n                                        <!--  Paypal  -->\\n                                        </div>\\n                                    </div>\\n                                    <div class=\\"custom-continue-button\\">\\n                                        <a class=\\"change_back\\" rel=\\"2\\" href=\\"javascript:void(0);\\">Back</a>\\n                                    </div>\\n                                </div>\\n                            </form>\\n                        </div>\\n                    </div>\\n                </div>\\n                <div class=\\"right-content\\">\\n                    <ul class=\\"total-billing\\">\\n                         <li>\\n                            <div class=\\"cart-products\\">\\n                                <h4>\\n                                    Checkout \\n                                </h4>\\n                            </div>\\n<!--                             <h6>\\n                                Rs. 1.00\\n                            </h6> -->\\n                        </li> \\n                        <li>\\n                            <ul class=\\"charges\\">\\n                                <li>\\n                                    <p>\\n                                        Subtotal\\n                                    </p>\\n                                    <h6 class=\\"checkout\\">\\n                                        0.00\\n                                    </h6>\\n                                </li>\\n<!--                                 <li>\\n                                    <p>\\n                                        Shipping\\n                                    </p>\\n                                    <small>Calculated at next step</small>\\n                                </li> -->\\n<!--                                 <li>\\n                                    <p>\\n                                        Taxes (estimated)\\n                                    </p>\\n                                    <h6>\\n                                        Rs. 0.09\\n                                    </h6>\\n                                </li> -->\\n                            </ul>\\n                        </li>\\n                        <li>\\n                            <h4>\\n                                Total\\n                            </h4>\\n                            <h2 class=\\"checkout\\" >\\n                                0.00\\n                            </h2>\\n                        </li>\\n                    </ul>\\n                </div>\\n            </div>\\n        </div>\\n        </div>\\n\\n\\n<div class=\\"ecm_footer\\">\\n       <div class=\\"container\\">\\n          <div class=\\"ecm_footer_copy_right\\">\\n             <p>Copyright 2021 • All Rights Reserved • Terms of Service • Privacy Policy</p>\\n          </div>\\n       </div>\\n    </div>\\n\\n\\n        \\n         \\n        <script type=\\"text/javascript\\" src=\\"https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js\\"></script>\\n        <script type=\\"text/javascript\\">\\n            // Wait for the DOM to be ready\\n            $(function() {                \\n                // Initialize form validation on the registration form.\\n                // It has the name attribute \\"registration\\"\\n                $(\\"form[name=\'user-info\']\\").validate({\\n                    // Specify validation rules\\n                    rules: {\\n                      // The key name on the left side is the name attribute\\n                      // of an input field. Validation rules are defined\\n                      // on the right side\\n                      email_mobile: {\\n                        required: true\\n                      },\\n                      fname: {\\n                        required: true\\n                      },\\n                      address: {\\n                        required: true\\n                      },\\n                      city: {\\n                        required: true\\n                      },\\n                      country: {\\n                        required: true\\n                      },\\n                      state: {\\n                        required: true\\n                      },\\n                      pin_portal:{\\n                        required: true \\n                      }\\n                    },\\n                    // Make sure the form is submitted to the destination defined\\n                    // in the \\"action\\" attribute of the form when valid\\n                    submitHandler: function(form) {\\n                        all_unactive();\\n                        details_append();\\n                        $(\'#tab-2\').addClass(\'active\');\\n                        $(\'#shipping\').parent().addClass(\'active\');\\n                        //var x = $(\'#stripeToken\').val();\\n                        //if(x != \'\' && typeof x !== \\"undefined\\"){\\n                          //  form.submit();\\n                          //var queryString = $(\'#payment-form\').serialize();\\n                          //console.log(queryString);\\n                         //console.log(localStorage);\\n//                          $.ajax({\\n//                              type: \\"POST\\",\\n//                              url: \'https://apps.webdew.com/hubspot/ecommerce/stripePay\',\\n//                              data: { form: form.serialize(), localData : localStorage }, // serializes form input\\n//                              success: function(data){\\n//                                  console.log(data);\\n//                              }\\n//                         });\\n                      \\n                      \\n                        \\n//                               $.ajax ({\\n//                                 type: \\"POST\\",\\n//                                 //url: \'https://apps.webdew.com/hubspot/ecommerce/stripePay?dat=\'+ form.serialize() +\'&portal_id=\'+$(\'#portal_id\').val()+\'&local=\' + loca ,\\n//                                 url: \'https://apps.webdew.com/hubspot/ecommerce/stripePay?portal_id=\'+$(\'#portal_id\').val()+\'&\'+$(\'#payment-form\').serialize(),\\n//                                 dataType: \'jsonp\', <!----- JSONP ADDITION ----->\\n//         //                         headers: {\\n//         //                           \'Authorization\': \\"Basic \\" + btoa(\\"myusername\\" + \\":\\" + \\"mypassword\\")\\n//         //                         },\\n//                                 success: function (data, status){\\n//                                   console.log(data);\\n//                                   alert(\'Thanks!\'); \\n//                                 },\\n//                                 error: function(jqXhr, textStatus, errorMessage){\\n//                                   console.log(textStatus);\\n//                                   console.log(errorMessage);\\n//                                 }\\n//                               });\\n\\n//                                \\n                        //}\\n                    }\\n                });\\n                function details_append(){\\n                    var email_mobile = $(\'#email-mobile\').val();\\n                    var fname = $(\'#first-name\').val();\\n                    var lname = $(\'#last-name\').val();\\n                    var con_append = email_mobile + \'\\\\n\' + fname + \' \' + lname\\n                    $(\'#stripe\').attr(\'data-email\',email_mobile);\\n                    $(\'.contact_append\').text(con_append);\\n                    var address = $(\'#address\').val();\\n                    var city = $(\'#city\').val();\\n                    var state = $(\'#state\').val();\\n                    var country = $(\'#country\').val();\\n                    var pin_code = $(\'#pin\').val();\\n                    var ship_append = address + \'\\\\n\' + city + \', \' + pin_code + \'\\\\n\' + state + \', \' + country\\n                    $(\'.shipping_append\').text(ship_append);\\n                }\\n                $(\'.change_back, #shipping_next\').click(function(){\\n                    var tab = $(this).attr(\'rel\');\\n                    all_unactive();\\n                    $(\'#tab-\'+tab).addClass(\'active\');\\n                    if(tab == 1) {\\n                        $(\'#information\').parent().addClass(\'active\');  \\n                    }\\n                    if(tab == 2) {\\n                        $(\'#shipping\').parent().addClass(\'active\');\\n                    }\\n                    if(tab == 3) {\\n                        $(\'#payment\').parent().addClass(\'active\');\\n                    }\\n                });\\n\\n                function all_unactive(){\\n                    $(\'.tabber-wrap.blog-tabber .tabber-tabs .custom-tab\').removeClass(\'active\');\\n                    $(\'.tabber-wrap.blog-tabber .tabber-content .tab-pane\').removeClass(\'active\');\\n                }\\n            });\\n          \\n    \\n        </script>\\n    \\n    <script>\\n      \\n      function roundFloat(num,dec){\\n      var d = 1;\\n      for (var i=0; i<dec; i++){\\n          d += \\"0\\";\\n      }\\n      return Math.round(num * d) / d;\\n      }\\n   \\n     function logo_address(object){\\n       $(\\"#img_portal_id\\").attr(\\"src\\",object.data);\\n        }\\n      \\n   \\n    $(document).ready(function() {\\n      var portal_id = $(\\"#img_portal_id\\").attr(\\"portal_id\\");\\n      var base_url = \\"https://apps.webdew.com/hubspot\\";\\n    $.ajax({\\n        url:\\"https://apps.webdew.com/hubspot/apps/ecommerce/logo?&portal_id=\\"+portal_id,\\n        dataType: \'jsonp\',\\n        async:true\\n        });\\n         \\n  });\\nfunction CheckBrowser() {\\n    if (\'localStorage\' in window && window[\'localStorage\'] !== null) {\\n      // we can use sessionStorage object to store data\\n      return true;\\n    } else {\\n      return false;\\n    }\\n  }\\n\\n//below step can be done via PHP and AJAX too. \\n  function doShowAll() {\\n    if (CheckBrowser()) {\\n      var key = \\"\\";\\n      var list = \\"\\";\\n      var i = 0;\\n      var grandTotal = 0;\\n      var htmlPro = \'\';\\n//       localStorage.removeItem(\\"rzp_device_id\\");\\n//       localStorage.removeItem(\\"__paypal_storage__\\");\\n      //for more advance feature, you can set cap on max items in the cart\\n      var total = localStorage.getItem(\\"total\\");\\n      \\n      $(\\".checkout\\").text(\\"$\\"+total);\\n      $(\\"#amount\\").val(total);\\n      \\n      for (i = 0; i <= localStorage.length-1; i++) {\\n        key = localStorage.key(i);\\n        \\n        if(key!=\\"rzp_device_id\\" || key!=\\"__paypal_storage__\\")\\n        {\\n          \\n        var val = localStorage.getItem(key);\\n        val = JSON.parse(val);\\n        var price = val.price;\\n        var total = val.quantity * price;\\n        \\n        if(key != \'__paypal_storage__\'){\\n            htmlPro += \'<input type=\\"hidden\\" name=\\"pro[\'+key+\']\\" value=\\"\'+ price + \'_\' + val.quantity +\'\\">\';\\n            grandTotal += total;\\n//            list += \'<tr><td width=\\"55%\\"><div class=\\"product-img-name  cart-item-img\\"><figure class=\\"product-img-wrap\\"><img src=\\"\\" alt=\\"\\"></figure><div class=\\"product-name\\"><h5>\'+val.name+\'</h5><button class=\\"del\\" rel=\\"\'+ key +\'\\">Remove</button></div></div></td><td width=\\"15%\\">$\'+val.price+\'</td><td width=\\"15%\\"><input type=\\"number\\" id=\\"quantity\\" class=\\"plus\\" name=\\"quantity\\" rel=\\"\'+ key +\'\\" value=\\"\' + val.quantity + \'\\" min=\\"1\\"></td><td width=\\"15%\\">$\'+total+\'</td></tr>\';\\n            list += \'<tr><td width=\\"55%\\"><div class=\\"product-img-name  cart-item-img\\"><figure class=\\"product-img-wrap\\"><img src=\\"\\" alt=\\"\\"></figure><div class=\\"product-name\\"><h5>\'+val.name+\'</h5></div></div></td><td width=\\"15%\\">$\'+val.price+\'</td><td width=\\"15%\\"><input type=\\"number\\" id=\\"quantity\\" class=\\"plus\\" name=\\"quantity\\" rel=\\"\'+ key +\'\\" value=\\"\' + val.quantity + \'\\" min=\\"1\\"></td><td width=\\"15%\\">$\'+total+\'</td></tr>\';\\n        }\\n      }\\n    }\\n         runLoad();\\n      $(\'.hidden_list\').html(htmlPro);\\n      //if no item exists in the cart\\n      if (list != \\"\\" ) {\\n//         $(\'.grand-total\').html(\'$\'+grandTotal);\\n//         $(\'.checkout\').attr(\'id\',\'grandTotal\');\\n//         $(\'.checkout\').attr(\'rel\',grandTotal);\\n//         $(\'.checkout\').html(\'$\'+grandTotal);\\n//         $(\'#grandTotalValue\').val(grandTotal);\\n//         $(\'#stripe\').attr(\'data-amount\',grandTotal * 100);\\n//         $(\'#amount\').val(grandTotal * 100);\\n        \\n      } else {\\n        \\n//         list += \'<tr><td width=\\"55%\\"><div class=\\"product-img-name\\"><figure class=\\"product-img-wrap\\"><img src=\\"\\" alt=\\"\\"></figure><div class=\\"product-name\\"><h5>No Item</h5></div></div></td><td width=\\"15%\\"></td><td width=\\"15%\\"></td><td width=\\"15%\\"></td></tr>\';\\n          \\n//           $(\'.grand-total\').html(\'0\');\\n//           $(\'.checkout\').attr(\'id\',\'grandTotal\');\\n//           $(\'.checkout\').attr(\'rel\',\'0\');\\n//           $(\'.checkout\').html(\'Checkout 0\');\\n//           $(\'#grandTotalValue\').val(0);\\n//           $(\'#stripe\').attr(\'data-amount\',0);\\n//           $(\'#amount\').val(0);\\n      }\\n   \\n      //bind the data to html table\\n      //you can use jQuery too....\\n      //document.getElementById(\'list1\').innerHTML = list;\\n\\n      jQuery(function($)\\n      {\\n          var $cartHeader = $(\\".cart-header\\");\\n          var $cartContent = $(\\".cart-content\\");\\n  \\n          function onResize()\\n          {\\n              var wh = $(window).height();\\n              var ch = wh - $cartHeader.height();\\n  \\n              $cartContent.css({\\n                  \\"height\\": ch\\n              });\\n          }\\n  \\n          $(window).resize(onResize); onResize();\\n  \\n          $(\'.cart-tab-cell\').click(function(){\\n              console.log(\\"click\\");\\n              $(\'.cart-panel\').toggleClass(\\"current-section__favorites\\");\\n          });\\n  \\n      });\\n    } else {\\n        alert(\'Cannot save shopping list as your browser does not support HTML 5\');\\n    }\\n    \\n  }\\n    \\n  doShowAll();\\n  //below function may be redundant\\n  function getPortal(){\\n    $.ajax ({\\n      type: \\"GET\\",\\n      url: \'https://apps.webdew.com/hubspot/ecommerce/\'+$(\'#portal_id\').val(),\\n      dataType: \'jsonp\',\\n      success: function (result, status){\\n        if(result.status === true){\\n            $(\'#stripe\').attr(\'data-key\',result.data.stripe_pk_key);\\n            $(\'#paypalScrpt\').attr(\'src\',\'https://www.paypal.com/sdk/js?client-id=\'+result.data.paypal_key+\'&amp;currency=USD\');\\n            //runLoad();\\n        }\\n      },\\n      error: function(jqXhr, textStatus, errorMessage){\\n        console.log(textStatus);\\n        console.log(errorMessage);\\n      }\\n    });\\n  }\\n  getPortal();\\n  \\n \\nfunction hsOnReadyLoadTabber() { \\n  // Variables   \\n  var i;\\n  var tabPane = document.getElementsByClassName(\\"tab-pane\\"); \\n  var tabAnchor = document.querySelectorAll(\\".tabber-tabs a\\");\\n  \\n  // Loop through all of the tab anchors \\n  for (i = 0; i < tabAnchor.length; i++) {\\n\\n    // Click function for when tab is clicked on\\n    tabAnchor[i].addEventListener(\\"click\\", function(e){\\n      e.preventDefault(); \\n      var tabIsOpen = this.parentElement.classList.contains(\\"active\\");\\n      var tabPaneId = this.getAttribute(\\"href\\").substring(1);\\n      var activeTabPane = document.getElementById(tabPaneId); \\n      // If the tab clicked is not already opened\\n      if (tabIsOpen === false) { \\n        for (i = 0; i < tabAnchor.length; i++) {\\n          // Removes active class on all tab anchors\\n          tabAnchor[i].parentElement.classList.remove(\\"active\\");\\n        }\\n        for (i = 0; i < tabPane.length; i++) {\\n          // Removes active class on all tab panes\\n          tabPane[i].classList.remove(\\"active\\");\\n        }\\n        // Adds active class to the active tab pane and anchor\\n        this.parentElement.classList.add(\\"active\\");\\n        activeTabPane.classList.add(\\"active\\");\\n      }\\n    });   \\n  }\\n} \\n\\nif (document.readyState === \\"complete\\" ||\\n    (document.readyState !== \\"loading\\" && !document.documentElement.doScroll)\\n   ) {\\n  hsOnReadyLoadTabber();\\n} else {\\n  document.addEventListener(\\"DOMContentLoaded\\", hsOnReadyLoadTabber);\\n}\\n\\n\\n\\n\\n\\n// dropdown\\n// \\n// var x, i, j, l, ll, selElmnt, a, b, c;\\n/*look for any elements with the class \\"custom-select\\":*/\\nx = document.getElementsByClassName(\\"custom-select\\");\\nl = x.length;\\nfor (i = 0; i < l; i++) {\\n  selElmnt = x[i].getElementsByTagName(\\"select\\")[0];\\n  ll = selElmnt.length;\\n  /*for each element, create a new DIV that will act as the selected item:*/\\n  a = document.createElement(\\"DIV\\");\\n  a.setAttribute(\\"class\\", \\"select-selected\\");\\n  a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;\\n  x[i].appendChild(a);\\n  /*for each element, create a new DIV that will contain the option list:*/\\n  b = document.createElement(\\"DIV\\");\\n  b.setAttribute(\\"class\\", \\"select-items select-hide\\");\\n  for (j = 1; j < ll; j++) {\\n    /*for each option in the original select element,\\n    create a new DIV that will act as an option item:*/\\n    c = document.createElement(\\"DIV\\");\\n    c.innerHTML = selElmnt.options[j].innerHTML;\\n    c.addEventListener(\\"click\\", function(e) {\\n        /*when an item is clicked, update the original select box,\\n        and the selected item:*/\\n        var y, i, k, s, h, sl, yl;\\n        s = this.parentNode.parentNode.getElementsByTagName(\\"select\\")[0];\\n        sl = s.length;\\n        h = this.parentNode.previousSibling;\\n        for (i = 0; i < sl; i++) {\\n          if (s.options[i].innerHTML == this.innerHTML) {\\n            s.selectedIndex = i;\\n            h.innerHTML = this.innerHTML;\\n            y = this.parentNode.getElementsByClassName(\\"same-as-selected\\");\\n            yl = y.length;\\n            for (k = 0; k < yl; k++) {\\n              y[k].removeAttribute(\\"class\\");\\n            }\\n            this.setAttribute(\\"class\\", \\"same-as-selected\\");\\n            break;\\n          }\\n        }\\n        h.click();\\n    });\\n    b.appendChild(c);\\n  }\\n  x[i].appendChild(b);\\n  a.addEventListener(\\"click\\", function(e) {\\n      /*when the select box is clicked, close any other select boxes,\\n      and open/close the current select box:*/\\n      e.stopPropagation();\\n      closeAllSelect(this);\\n      this.nextSibling.classList.toggle(\\"select-hide\\");\\n      this.classList.toggle(\\"select-arrow-active\\");\\n    });\\n}\\nfunction closeAllSelect(elmnt) {\\n  /*a function that will close all select boxes in the document,\\n  except the current select box:*/\\n  var x, y, i, xl, yl, arrNo = [];\\n  x = document.getElementsByClassName(\\"select-items\\");\\n  y = document.getElementsByClassName(\\"select-selected\\");\\n  xl = x.length;\\n  yl = y.length;\\n  for (i = 0; i < yl; i++) {\\n    if (elmnt == y[i]) {\\n      arrNo.push(i)\\n    } else {\\n      y[i].classList.remove(\\"select-arrow-active\\");\\n    }\\n  }\\n  for (i = 0; i < xl; i++) {\\n    if (arrNo.indexOf(i)) {\\n      x[i].classList.add(\\"select-hide\\");\\n    }\\n  }\\n}\\n/*if the user clicks anywhere outside the select box,\\nthen close all select boxes:*/\\ndocument.addEventListener(\\"click\\", closeAllSelect);\\n\\n\\n\\n    </script>\\n    <!--   E-cart Module end  -->\\n\\n\\n\\n<script type=\\"text/javascript\\">\\n  \\n    $(document).ready(function() {\\n        $(\'.minus\').click(function() {\\n            var $input = $(this).parent().find(\'input\');\\n            var count = parseInt($input.val()) - 1;\\n            count = count < 1 ? 1 : count;\\n            $input.val(count);\\n            $input.change();\\n            return false;\\n        });\\n        $(\'.plus\').click(function() {\\n            var $input = $(this).parent().find(\'input\');\\n            $input.val(parseInt($input.val()) + 1);\\n            $input.change();\\n            return false;\\n        });\\n    });\\n\\n    $(document).ready(function() {\\n\\n        $(\\"#cart_item\\").click(function() {\\n            $(\'.ecm_header\').addClass(\'menu_fix\');\\n        });\\n\\n        $(\\"#close_cart\\").click(function() {\\n            $(\'.ecm_header\').removeClass(\'menu_fix\');\\n        });\\n    });\\n\\n\\n    // jQuery Plugin: http://flaviusmatis.github.io/simplePagination.js/\\n\\nvar items = $(\\".ecm_product_row .ecm_product_col-3\\");\\n    var numItems = items.length;\\n    var perPage = 5;\\n\\n    items.slice(perPage).hide();8\\n\\n    $(\'#pagination-container\').pagination({\\n        items: numItems,\\n        itemsOnPage: perPage,\\n        prevText: \\"&laquo;\\",\\n        nextText: \\"&raquo;\\",\\n        onPageClick: function (pageNumber) {\\n            var showFrom = perPage * (pageNumber - 1);\\n            var showTo = showFrom + perPage;\\n            items.hide().slice(showFrom, showTo).show();\\n        }\\n    });\\n\\n\\n\\n\\n    </script>\\n\\n\\n\\n    \\n    {{ standard_footer_includes }}\\n  </body>\\n</html>"
                    }',
                  CURLOPT_HTTPHEADER => array(
                    'Authorization: Bearer '.$token,
                    'Content-Type: application/json',
                    'Cookie: __cfduid=dbee73ed9d0d3ee23fabe7672fb9b848a1614580870'
                  ),
                 ));

                 $response = curl_exec($curl);

                 curl_close($curl);

                   $curl = curl_init();

                curl_setopt_array($curl, array(
                  CURLOPT_URL => 'https://api.hubapi.com/content/api/v2/templates',
                  CURLOPT_RETURNTRANSFER => true,
                  CURLOPT_ENCODING => '',
                  CURLOPT_MAXREDIRS => 10,
                  CURLOPT_TIMEOUT => 0,
                  CURLOPT_FOLLOWLOCATION => true,
                  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                  CURLOPT_CUSTOMREQUEST => 'POST',
                  CURLOPT_POSTFIELDS =>'{
                                    "category_id": 1,
                                    "folder": "example",
                                    "is_available_for_new_content": "False",
                                    "template_type": 4,        
                                    "path":"webdew/thank_you",
                                    "source":"<!--\\r\\n    templateType: page\\r\\n    isAvailableForNewContent: true\\r\\n-->\\r\\n<!doctype html>\\r\\n<html>\\r\\n  <head>\\r\\n    <meta charset=\\"utf-8\\">\\r\\n    <title>{{ content.html_title }}<\\/title>\\r\\n    <meta name=\\"viewport\\" content= \\"width=device-width, initial-scale=1.0\\">\\r\\n       <link rel=\\"stylesheet\\" href=\\"https:\\/\\/maxcdn.bootstrapcdn.com\\/bootstrap\\/3.4.1\\/css\\/bootstrap.min.css\\">\\r\\n        <link rel=\\"stylesheet\\" href=\\"https:\\/\\/cdnjs.cloudflare.com\\/ajax\\/libs\\/font-awesome\\/4.7.0\\/css\\/font-awesome.min.css\\">\\r\\n        <link href=\\"https:\\/\\/fonts.googleapis.com\\/css2?family=DM+Sans:wght@500&display=swap\\" rel=\\"stylesheet\\">\\r\\n        <link rel=\\"stylesheet\\" href=\\"https:\\/\\/pro.fontawesome.com\\/releases\\/v5.10.0\\/css\\/all.css\\">\\r\\n        <script src=\\"https:\\/\\/ajax.googleapis.com\\/ajax\\/libs\\/jquery\\/3.5.1\\/jquery.min.js\\"><\\/script>\\r\\n        <script src=\\"https:\\/\\/maxcdn.bootstrapcdn.com\\/bootstrap\\/3.4.1\\/js\\/bootstrap.min.js\\"><\\/script>\\r\\n    <meta name=\\"description\\" content=\\"{{ content.meta_description }}\\">\\r\\n    {{ standard_header_includes }}\\r\\n  <\\/head>\\r\\n  \\r\\n{% require_css %}\\r\\n<style> \\r\\n  @import url(\'https:\\/\\/fonts.googleapis.com\\/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap\');\\r\\n\\r\\nhtml {\\r\\n  height: 100%;\\r\\n  box-sizing: border-box;\\r\\n}\\r\\nbody {\\r\\n  position: relative;\\r\\n  padding-bottom: 6rem;\\r\\n  min-height: 100%;\\r\\n  \\r\\n}\\r\\n\\r\\n  \\/*------- header---------*\\/\\r\\n\\r\\n{% set brand_color = \\"#00a9fa\\" %}\\r\\n\\r\\nimg{ max-width: 100%; }\\r\\n.ecm_header{ width: 100%; display: inline-block; position: relative;  padding: 15px 0; background-color: #ffffff; box-shadow: 0 0px 10px #eee; }\\r\\n.cms_head{ display: flex; flex-wrap: wrap; justify-content: space-between; align-items: center; }\\r\\n.cms_head .logo{ max-width: 180px; }\\r\\n.cms_head .logo img {   width: 100%;}\\r\\n\\r\\n.ecm_product_warp{ padding: 50px 0; }\\r\\n.ecm_product_row{\\r\\n  display: -ms-flexbox;\\r\\n  display: flex;\\r\\n  -ms-flex-wrap: wrap;\\r\\n  flex-wrap: wrap;\\r\\n\\r\\n}\\r\\n\\r\\n.ecm_product_col-3 {\\r\\n -ms-flex: 0 0 25%;\\r\\n  flex: 0 0 25%;\\r\\n  max-width: 25%;\\r\\n  padding: 0 15px;\\r\\n  margin-bottom: 30px;\\r\\n}\\r\\n\\r\\n.ecm_product_img{ background-color: #efefef; padding: 15px; position: relative; }\\r\\na.ecm_view_detail {\\r\\n    display: none;\\r\\n    background-color: {{brand_color}} ;\\r\\n    color: #ffffff;\\r\\n    position: absolute;\\r\\n    bottom: 0;\\r\\n    left: 0;\\r\\n    width: 100%;\\r\\n    padding: 10px 0;\\r\\n    text-align: center;\\r\\n    text-decoration: none;\\r\\n}\\r\\n.ecm_product_box:hover a.ecm_view_detail { display: block; }\\r\\n\\r\\n.ecm_product_detail{ padding: 10px 0; text-align: center; }\\r\\n\\r\\nspan#cart_item {\\r\\n    cursor: pointer;\\r\\n}\\r\\n\\r\\n.ecm_product_list {\\r\\n    \\/*display: none;*\\/\\r\\n    position: fixed;\\r\\n    top: 0;\\r\\n    right: -320px;\\r\\n    width: 320px;\\r\\n    background-color: #fff;\\r\\n    z-index: 99;\\r\\n    height: 100%;\\r\\n    overflow: auto;\\r\\n    padding: 25px 0 0 0;\\r\\n    border-left: 1px solid #ccc;    \\r\\n      -webkit-transition: all 1s; \\r\\n      transition: all 1s; \\r\\n      opacity:0;\\r\\n      visibility: hidden;\\r\\n} \\r\\n\\r\\n.ecm_header.menu_fix  .ecm_product_list{\\r\\n      right: 0;\\r\\n      \\/*display: block;*\\/\\r\\n      -webkit-transition: all 1s; \\r\\n      transition: all 1s; \\r\\n      visibility: visible;\\r\\n      opacity: 1;\\r\\n}\\r\\n\\r\\ndiv#close_cart {\\r\\n    display: flex;\\r\\n    text-align: right;\\r\\n    justify-content: flex-end;\\r\\n    align-items: center;\\r\\n    margin: 0 25px 10px 0;\\r\\n    border-radius: 2px;\\r\\n       color: {{brand_color}};\\r\\n       cursor: pointer;\\r\\n}\\r\\ndiv#close_cart i.fas {\\r\\n    font-size: 20px;\\r\\n    color: {{brand_color}};\\r\\n    margin: 0 4px 0 0;\\r\\n}\\r\\n.ecm_product_list ul{ margin: 0; padding: 0; }\\r\\n.ecm_product_list ul li {\\r\\n    list-style: none;\\r\\n    padding: 12px 25px;\\r\\n    border-bottom: 1px solid #ccc;\\r\\n}\\r\\n\\r\\n.product_list_row{ \\r\\n   display: flex; \\r\\n    flex-wrap: wrap; \\r\\n\\r\\n    justify-content: space-between;\\r\\n}\\r\\n.ecm_description h4 {\\r\\n    margin: 0;\\r\\n    padding: 0;\\r\\n    font-size: 15px;\\r\\n}\\r\\n\\r\\n.product_thum {\\r\\n    width: 60px;\\r\\n}\\r\\n\\r\\n\\r\\n.eca_product_cart span {cursor:pointer; }\\r\\n.eca_product_cart .number{\\r\\n  margin:5px 0 0 0; text-align: center;\\r\\n }\\r\\n    .eca_product_cart .minus, \\r\\n    .eca_product_cart .plus{\\r\\n      width:20px;\\r\\n      \\/*height:20px;*\\/\\r\\n      background:#f2f2f2;\\r\\n      border-radius:4px;\\r\\n      padding:3px 5px 3px 5px;\\r\\n      border:1px solid #ddd;\\r\\n      display: inline-block;\\r\\n      vertical-align: middle;\\r\\n      text-align: center;\\r\\n    }\\r\\n    .eca_product_cart td input{\\r\\n      height:30px;\\r\\n      width: 40px;\\r\\n      text-align: center;\\r\\n      font-size: 15px;\\r\\n      border:1px solid #ddd;\\r\\n      border-radius:4px;\\r\\n      display: inline-block;\\r\\n      vertical-align: middle;\\r\\n      \\r\\n\\r\\n}\\r\\n.ecm_product_list > h4 {\\r\\n    font-size: 14px;\\r\\n    padding: 10px 15px 15px;\\r\\n    border-bottom: 1px solid #ccc;\\r\\n    font-weight: 700;\\r\\n}\\r\\n.ecm_close i.fas.fa-trash-alt{ color: {{brand_color}}; cursor: pointer; }\\r\\n.total_cart {\\r\\n    margin: 15px 0;\\r\\n    width: 100%;\\r\\n}\\r\\n.total {\\r\\n    display: flex;\\r\\n    justify-content: space-between;\\r\\n    padding: 0 15px;\\r\\n}\\r\\n.total span.label{ color: #333333; }\\r\\n.total span.label span {\\r\\n    font-size: 15px;\\r\\n}\\r\\n\\r\\n.total span.money{ color: {{brand_color}}; }\\r\\n.btn-checkout {\\r\\n    padding: 0 15px;\\r\\n    width: 100%;\\r\\n    display: inline-block;\\r\\n    text-align: center;\\r\\n}\\r\\n\\r\\n.btn-checkout a{ \\r\\n   background-color: {{brand_color}};\\r\\n   color: #ffffff;\\r\\n   padding: 12px 0;\\r\\n   width: 100%;\\r\\n   display: inline-block;\\r\\n   border-radius: 30px;\\r\\n   text-decoration: none;\\r\\n   border:2px solid transparent;\\r\\n  }\\r\\n\\r\\n  .btn-checkout a:hover{ \\r\\n   color: {{brand_color}};  \\r\\n   border:2px solid {{brand_color}};\\r\\n   background-color: #ffffff;\\r\\n  }\\r\\n\\r\\n\\r\\n\\r\\n.btn-view-cart {\\r\\n    padding: 0 15px;\\r\\n    width: 100%;\\r\\n    display: inline-block;\\r\\n    text-align: center;\\r\\n    margin-top: 15px;\\r\\n}\\r\\n\\r\\n.btn-view-cart a{ \\r\\n   color: {{brand_color}};\\r\\n   padding: 12px 0;\\r\\n   width: 100%;\\r\\n   display: inline-block;\\r\\n   border-radius: 30px;\\r\\n   border:2px solid {{brand_color}};\\r\\n    text-decoration: none;\\r\\n  }\\r\\n.btn-view-cart a:hover{ background-color: {{brand_color}}; color: #ffffff; }\\r\\n  \\/*------- header---------*\\/\\r\\ntable{\\r\\nfont-family: \'Montserrat\', sans-serif;\\r\\n}\\r\\nbody{\\r\\nmargin:0;\\r\\n}\\r\\nfigure.product-img-wrap {\\r\\n    width: 90px;\\r\\n    height: 90px;\\r\\n    margin: 0 40px 0 0;\\r\\n}\\r\\nfigure.product-img-wrap img{\\r\\n    width:100%;\\r\\n}\\r\\n.product-img-name{\\r\\ndisplay:flex;\\r\\n  justify-content: left;\\r\\n    align-items: center;\\r\\n}\\r\\n.product-name h5 {\\r\\n    font-size: 18px;\\r\\n    margin: 0 0 5px;\\r\\n    color:#333;\\r\\n}\\r\\n.products-cart-wrap table th {\\r\\n    font-size: 16px;\\r\\n    text-transform: uppercase;\\r\\n    color: #7a7a7a;\\r\\n    text-align: left;\\r\\n}\\r\\n.products-cart-wrap table td{\\r\\n     font-size: 16px;\\r\\n    color: #7a7a7a;\\r\\n    text-align: left;\\r\\n}\\r\\n input#quantity {\\r\\n    width: 40px;\\r\\n    height: 25px;\\r\\n    outline: none;\\r\\n    box-shadow: none;\\r\\n    border: none;\\r\\n    border: 1px solid #7a7a7a33;\\r\\n}\\r\\n.products-cart-wrap table tbody tr {\\r\\n    border: 1px solid #7a7a7a33;\\r\\n}\\r\\n\\r\\n.product-name a , .product-name button , .top-heading a{\\r\\n    color: #7a7a7a;\\r\\n    background:none;\\r\\n    padding:0;\\r\\n    border:none;\\r\\n}\\r\\n.product-name a:hover{\\r\\n   color:#333;\\r\\n}\\r\\n.products-cart-wrap table {\\r\\n    border-spacing: 0;\\r\\n}\\r\\n.top-heading h1 {\\r\\n    font-size: 30px;\\r\\n    margin: 0 0 10px 0;\\r\\n}\\r\\n.top-heading {\\r\\n    text-align: center;\\r\\n    padding: 30px 0 40px;\\r\\n}\\r\\n.total-checkout {\\r\\n    text-align: right;\\r\\n    padding: 30px 0;\\r\\n}\\r\\n.sub-total-checkout h3 {\\r\\n    font-size: 22px;\\r\\n    margin: 0;\\r\\n}\\r\\n.sub-total-checkout {\\r\\n    display: flex;\\r\\n    justify-content: space-between;\\r\\n    width: 24%;\\r\\n    margin: 0 0 0 auto;\\r\\n    align-items: center;\\r\\n    color:#7a7a7a;\\r\\n}\\r\\n.sub-total-checkout p {\\r\\n    margin: 0;\\r\\n    font-size: 16px;\\r\\n}\\r\\n.total-checkout h5 {\\r\\n    font-size: 16px;\\r\\n    margin: 0;\\r\\n    padding: 20px 0 40px;\\r\\n       color:#7a7a7a;\\r\\n}\\r\\n.checkout-button-custom a {\\r\\n    font-size: 15px;\\r\\n    text-transform: uppercase;\\r\\n    outline: none;\\r\\n    text-decoration: none !important;\\r\\n    display: inline-block;\\r\\n    background: #00A9FA;\\r\\n    padding: 12px 15px;\\r\\n    color: #fff;\\r\\n    transition:0.3s;\\r\\n}\\r\\n.checkout-button-custom a:hover {\\r\\n     color: #fff;\\r\\n    box-shadow: 0 0 2px 0 #7a7a7a;\\r\\n}\\r\\n.checkout-button-custom button {\\r\\n    font-size: 15px;\\r\\n    text-transform: uppercase;\\r\\n    outline: none;\\r\\n    text-decoration: none;\\r\\n    display: inline-block;\\r\\n    background: #00A9FA;\\r\\n    padding: 12px 15px;\\r\\n    color: #fff;\\r\\n    transition:0.3s;\\r\\n}\\r\\n.checkout-button-custom button:hover {\\r\\n    box-shadow: 0 0 2px 0 #7a7a7a;\\r\\n}\\r\\n.container {\\r\\n    max-width: 1200px;\\r\\n    \\/*min-width: 800px;*\\/\\r\\n    margin: 0 auto;\\r\\n     padding:0 15px;\\r\\n     width: 100%;\\r\\n}\\r\\n\\r\\n\\r\\nlabel{\\r\\n  margin-bottom: 5px!important;\\r\\n  margin-top: 5px!important;\\r\\n}\\r\\n\\r\\n\\r\\n\\/*cart*\\/\\r\\n.eca_cart_wrap h2{ text-align: center; font-weight: 700; margin-bottom: 45px; }\\r\\n\\r\\n.eca_container_wrap{ padding: 50px 0; width: 100%; display: inline-block; }\\r\\n.eca_product_cart{ float: left; width: 100%; overflow: auto; }\\r\\n\\r\\n.eca_product_cart table{ float: left; width: 100%; }\\r\\n.eca_product_cart table tr th{ text-transform: uppercase; font-weight: 700; font-size: 15px; background-color: #f8f8f8;  }\\r\\n.eca_product_cart table tr td{    }\\r\\n.eca_product_cart table tr th,\\r\\n.eca_product_cart table tr td{\\r\\n     border: 1px solid #ddd; \\r\\n     padding: 13px;\\r\\n\\r\\n}\\r\\n.eca_calculated {   width: 100%;  float: left;}\\r\\n.eca_checkout_wrap {  max-width: 330px;  float: right; width: 100%; }\\r\\n\\r\\n.eca_checkout_wrap h4{   display: block;  font-size: 16px;  font-weight: 600;   margin-bottom: 20px;}\\r\\n.eca_checkout_wrap a{ \\r\\n  background-color: {{brand_color}};\\r\\n    color: #ffffff;\\r\\n    padding: 8px 20px;\\r\\n    display: inline-block;\\r\\n    border-radius: 30px;\\r\\n    text-decoration: none;\\r\\n    border: 2px solid transparent;\\r\\n    text-align: center;\\r\\n}\\r\\n\\r\\n.eca_checkout_wrap a:hover{ \\r\\n   color: {{brand_color}};  \\r\\n   border:2px solid {{brand_color}};\\r\\n   background-color: #ffffff;\\r\\n}\\r\\n.back_arrow_btn {\\r\\n    padding: 20px 15px;\\r\\n    width: 100%;\\r\\n    display: inline-block;\\r\\n}\\r\\n\\r\\n.back_arrow_btn a{\\r\\n   color: {{brand_color}}; text-decoration: none;\\r\\n} \\r\\n.eca_product_thums { text-align: center; }\\r\\n.eca_product_thums img { max-width: 135px; }\\r\\nspan.eca_total_price {   font-size: 20px;}\\r\\n\\r\\n.close_eca i.fas.fa-times{ cursor: pointer; color: {{ brand_color }} }\\r\\n\\/*cart*\\/\\r\\n\\r\\n\\r\\n\\/*footer*\\/\\r\\n.ecm_footer{ \\r\\n  position: absolute;\\r\\n  right: 0;\\r\\n  bottom: 0;\\r\\n  left: 0;\\r\\n  background-color: {{ brand_color }};     padding: 15px 0; \\r\\n}\\r\\n.ecm_footer_copy_right {  text-align: center;}\\r\\n.ecm_footer_copy_right p { color: #ffffff;     margin: 0;}\\r\\n\\r\\n\\r\\n\\r\\n<\\/style>\\r\\n{% end_require_css %}\\r\\n  <body>\\r\\n\\r\\n\\r\\n    <div class=\\"ecm_header\\">\\r\\n    <div class=\\"container\\">\\r\\n        <div class=\\"cms_head\\">\\r\\n            <div class=\\"logo\\">\\r\\n                <a href=\\"#\\">\\r\\n                   <img id=\\"portal_id\\" portal_id=\\"{{ portal_id }}\\" src=\\"\\" alt=\\"Brand-logo\\">\\r\\n                <\\/a>\\r\\n            <\\/div>\\r\\n          \\r\\n        <\\/div>\\r\\n    <\\/div>\\r\\n<\\/div>\\r\\n\\r\\n\\r\\n   \\r\\n<!-- cart start -->\\r\\n<div class=\\"eca_container_wrap\\">\\r\\n    <div class=\\"container\\">\\r\\n      <div class=\\"back_arrow_btn\\">\\r\\n             <a href=\\"{{ get_asset_url(\'\\/webdew\\/products\') }}\\"><i class=\\"fas fa-long-arrow-alt-left\\"><\\/i> Back<\\/a>\\r\\n          <\\/div>\\r\\n       <div class=\\"eca_row_fluid\\">\\r\\n           <div class=\\"eca_cart_wrap\\">\\r\\n               <div class=\\"cart_head_t\\">\\r\\n                 <h2><\\/h2> \\r\\n             <h4><\\/h4>\\r\\n             <\\/div>\\r\\n               <div class=\\"container\\">\\r\\n  <div class=\\"jumbotron\\">\\r\\n    <h1>Thank you for Shopping with us!<\\/h1>      \\r\\n    <p>we hope to see you back soon!<\\/p>\\r\\n  <\\/div>    \\r\\n<\\/div>\\r\\n       \\r\\n           <\\/div>\\r\\n       <\\/div>\\r\\n    <\\/div>\\r\\n<\\/div>\\r\\n\\r\\n<!-- End cart -->\\r\\n\\r\\n   <div class=\\"ecm_footer\\">\\r\\n       <div class=\\"container\\">\\r\\n          <div class=\\"ecm_footer_copy_right\\">\\r\\n             <p>Copyright 2021 \\u2022 All Rights Reserved \\u2022 Terms of Service \\u2022 Privacy Policy<\\/p>\\r\\n          <\\/div>\\r\\n       <\\/div>\\r\\n    <\\/div>\\r\\n\\r\\n\\r\\n<script src=\\"https:\\/\\/static.hsappstatic.net\\/jquery-libs\\/static-1.1\\/jquery\\/jquery-1.7.1.js\\"><\\/script>\\r\\n<script src=\\"https:\\/\\/cdnjs.cloudflare.com\\/ajax\\/libs\\/jquery-validate\\/1.19.2\\/jquery.validate.min.js\\"><\\/script>\\r\\n    \\r\\n      <script type=\\"text\\/javascript\\">\\r\\n            function roundFloat(num,dec){\\r\\n            var d = 1;\\r\\n            for (var i=0; i<dec; i++){\\r\\n                d += \\"0\\";\\r\\n            }\\r\\n            return Math.round(num * d) \\/ d;\\r\\n        }\\r\\n\\r\\n        \\r\\n        \\r\\n    function logo_address(object){\\r\\n       $(\\"#portal_id\\").attr(\\"src\\",object.data);\\r\\n        }\\r\\n      \\r\\n   \\r\\n    $(document).ready(function() {\\r\\n      var portal_id = $(\\"#portal_id\\").attr(\\"portal_id\\");\\r\\n      var base_url = \\"https:\\/\\/apps.webdew.com\\/hubspot\\";\\r\\n    $.ajax({\\r\\n        url:\\"https:\\/\\/apps.webdew.com\\/hubspot\\/apps\\/ecommerce\\/logo?&portal_id=\\"+portal_id,\\r\\n        dataType: \'jsonp\',\\r\\n        async:true\\r\\n        });\\r\\n         \\r\\n  });\\r\\n     \\r\\n        \\r\\n        \\r\\n        \\r\\n    $(document).ready(function() {\\r\\n      \\r\\n      \\/\\/this function is used to subtract cart items\\r\\n      $(document).on(\'click\',\'.minus\', function()\\r\\n      {     \\r\\n            var $input = $(this).parent().find(\'input\');\\r\\n            var price  = $(this).attr(\'minus_price\');\\/\\/getting price from button\\r\\n            var  id    = $(this).attr(\'prod_minus\');\\/\\/getting product id from button\\r\\n            var items = localStorage.getItem(\'items\');\\r\\n        \\r\\n          if($input.val() > 1 ){\\r\\n          \\r\\n           \\/\\/converting price into number format\\r\\n            var str = String(price);\\r\\n            str = str.replace(\\",\\", \\"\\");\\r\\n            str = str.replace(\\"$\\", \\"\\");\\r\\n           \\/\\/converting price into number format\\r\\n           \\r\\n            var cart_total = localStorage.getItem(\'total\');\\/\\/getting total from local storage\\r\\n            var total = parseFloat(cart_total);\\r\\n            \\r\\n            total -= parseFloat(price);\\/\\/subtracting product price from total\\r\\n            total = roundFloat(total,4);\\r\\n            localStorage.setItem(\\"total\\",total);\\/\\/setting total in local storage\\r\\n            $(\\".money\\").text(\\" $ \\"+total);\\/\\/reflecting back into cart\\r\\n        \\r\\n            let product_detail = localStorage.getItem(id);\\r\\n            var product = JSON.parse(product_detail);\\r\\n            var product_id = product.product_id;\\r\\n            product.item -= 1;\\r\\n            \\r\\n            product.total -= parseFloat(str); \\r\\n            total = roundFloat(product.total,4);\\r\\n            localStorage.setItem(id,JSON.stringify(product));\\r\\n            $(\\".product_total\\"+product_id).text(\\"$\\"+product.total);\\r\\n        \\r\\n             var items = localStorage.getItem(\\"items\\");\\r\\n            var int_items = parseInt(items);\\r\\n            int_items -= 1;\\r\\n            localStorage.setItem(\\"items\\",int_items);\\r\\n            $(\\"#cart_count\\").text(int_items);\\r\\n        \\r\\n            var count = parseInt($input.val()) - 1;\\r\\n            count = count < 1 ? 1 : count;\\r\\n            $input.val(count);\\r\\n            $input.change();\\r\\n          }\\r\\n                    \\r\\n            return false;\\r\\n      });\\r\\n      \\r\\n         \\/\\/this function is used to add cart items into cart\\r\\n       $(document).on(\'click\', \'.plus\', function()\\r\\n      {     \\r\\n            var $input = $(this).parent().find(\'input\');\\r\\n            var price = $(this).attr(\'add_price\');\\/\\/ this price is in dollar format\\r\\n            var  id= $(this).attr(\'prod_add\');\\r\\n           \\r\\n            \\/\\/converting it to normal number\\r\\n            var str = String(price);\\r\\n            str = str.replace(\\",\\", \\"\\");\\r\\n            str = str.replace(\\"$\\", \\"\\");\\r\\n            \\/\\/converting it to normal number\\r\\n            \\r\\n            var cart_total = localStorage.getItem(\'total\');\\r\\n            var total = parseFloat(cart_total);\\r\\n            \\r\\n            total += parseFloat(price);\\r\\n             total = roundFloat(total,4);\\r\\n            localStorage.setItem(\\"total\\",total);\\r\\n            $(\\".money\\").text(\\" $ \\"+total);\\r\\n         \\r\\n             let product_detail = localStorage.getItem(id);\\r\\n             var product = JSON.parse(product_detail);\\r\\n             var product_id = product.product_id;\\r\\n             product.item += 1;\\r\\n             product.total += parseFloat(str);\\r\\n            total = roundFloat(product.total,4);\\r\\n            localStorage.setItem(id,JSON.stringify(product));\\r\\n             $(\\".product_total\\"+product_id).text(\\"$\\"+product.total);\\r\\n         \\r\\n            var items = localStorage.getItem(\\"items\\");\\r\\n            var int_items = parseInt(items);\\r\\n            int_items += 1;\\r\\n            localStorage.setItem(\\"items\\",int_items);\\r\\n            $(\\"#cart_count\\").text(int_items);\\r\\n         \\r\\n            $input.val(parseInt($input.val()) + 1);\\r\\n            $input.change();\\r\\n            return false;\\r\\n      });\\r\\n      \\r\\n             $(document).on(\'click\', \'#delete\', function()\\r\\n      {     \\r\\n            \\r\\n            var item_id = $(this).attr(\'item_id\');\\r\\n            $(\\".list_id\\"+item_id).remove();\\r\\n            \\r\\n             \\r\\n           \\r\\n            var product_detail = localStorage.getItem(item_id);\\r\\n            var product = JSON.parse(product_detail);\\r\\n            \\r\\n            var item_price = product.total;\\r\\n\\r\\n            var cart_total = localStorage.getItem(\'total\');\\r\\n            var total = parseFloat(cart_total);\\r\\n            \\r\\n            total -= parseFloat(item_price);\\r\\n            total = roundFloat(total,4);\\r\\n            localStorage.setItem(\\"total\\",total);\\r\\n            $(\\".money\\").text(\\" $ \\"+total);\\r\\n             \\r\\n           \\r\\n               \\r\\n               \\r\\n            \\/\\/storing item count in local storage\\r\\n            var items = localStorage.getItem(\\"items\\");\\r\\n            var int_items = parseInt(items);\\r\\n            \\r\\n            var items = localStorage.getItem(item_id);\\r\\n            var items_object = JSON.parse(items);\\r\\n            var product_items = items_object.item;\\r\\n\\r\\n               \\r\\n            int_items -= parseInt(product_items);\\r\\n            localStorage.setItem(\\"items\\",int_items);\\r\\n            \\/\\/storing item count in local storage\\r\\n        \\r\\n             $(\\"#cart_count\\").text(int_items);\\r\\n             \\r\\n             localStorage.removeItem(item_id);\\r\\n             var item_count= localStorage.getItem(\'items\');               \\r\\n         \\r\\n            if(item_count==0)\\r\\n              {\\r\\n                $(\\"#product_item\\").append(\\"<tr><td colspan=\'10\'><div class=\'eca_product_thums\'><h3> Your Cart is Empty.<\\/h3><p>please go <a href=\'{{ get_asset_url(\'\\/webdew\\/products\') }}\'>back<\\/a> and add items.<\\/p><\\/div><\\/td><\\/tr>\\");\\r\\n              }\\r\\n            return false;\\r\\n      });\\r\\n      \\r\\n    });\\r\\n      \\r\\n      $(document).on(\'click\', \'.product_details\', function(e)\\r\\n      {     e.preventDefault();\\r\\n            var product_id = $(this).attr(\'prod_id\');\\r\\n            var product_price = $(this).attr(\'prod_price\');\\r\\n            var product_name = $(this).attr(\'prod_name\');\\r\\n            var href = $(this).attr(\'href\');\\r\\n            var product_description =$(this).attr(\'prod_desc\');\\r\\n            var product_info={\'product_id\':product_id,\'product_name\':product_name,\'product_price\':product_price,\'product_description\':product_description};\\r\\n            localStorage.setItem(\'product_info\',JSON.stringify(product_info));\\r\\n            window.location.replace(href);\\r\\n          \\r\\n      });\\r\\n      \\r\\n      \\r\\n      \\r\\n      \\r\\n          \\/\\/add to cart button funcitonality\\r\\n          $(document).ready(function() {\\r\\n          \\/\\/setting cart total and items to 0\\r\\n          if(localStorage.getItem(\'items\'))\\r\\n          {  \\r\\n           var item_count= localStorage.getItem(\'items\');\\r\\n         \\r\\n            if(item_count==0)\\r\\n              {\\r\\n                $(\\"#product_item\\").append(\\"<tr><td colspan=\'10\'><div class=\'eca_product_thums\'><h3> Your Cart is Empty.<\\/h3><p>please go <a href=\'{{ get_asset_url(\'\\/webdew\\/products\') }}\'>back<\\/a> and add items.<\\/p><\\/div><\\/td><\\/tr>\\");\\r\\n              }\\r\\n                let nextValue;\\r\\n                    for (let i = 0; i < localStorage.length; i++)\\r\\n                    {\\r\\n                        nextValue = localStorage.key(i);\\r\\n                          if(nextValue != \\"total\\" && nextValue != \\"items\\" && nextValue !=\\"product_info\\" &&nextValue != \\"__paypal_storage__\\" )\\r\\n                          {\\r\\n                              var item = localStorage.getItem(nextValue);\\r\\n                                \\r\\n                                  var items_object = JSON.parse(item);\\r\\n                                  var product_id = nextValue; \\r\\n                                  var product_name = items_object.product_name;\\r\\n                                  var product_price = items_object.product_price;\\r\\n                                  var product_total = items_object.total;\\r\\n\\/\\/                                   var product_rate = items_object.total;\\r\\n                            \\r\\n                            \\r\\n                                var str = String(product_price);\\r\\n                                str = str.replace(\\",\\", \\"\\");        \\r\\n                                str = str.replace(\\"$\\", \\"\\");\\r\\n                                var product_rate = parseFloat(str);\\r\\n                                var image_path = items_object.image_path;\\r\\n\\/\\/                                $(\\"#product_item\\").append(\\"<li class=\'list_id\\"+product_id+\\"\'><div class=\'product_list_row\'><div class=\'product_thum\'><img src=\'https:\\/\\/f.hubspotusercontent40.net\\/hubfs\\/7323845\\/product.png\\\\\' alt=\'\'><\\/div><div class=\'ecm_description\'><h4>\\"+product_name+\\"<\\/h4><span class=\'price\'>\\"+product_price+\\"<\\/span><div class=\'number\'><span class=\'minus\' minus_price=\\"+product_rate+\\" prod_minus=\\"+product_id+\\">-<\\/span><input type=\'text\' value=\\"+items_object.item+\\" \\/><span class=\'plus\' add_price=\\"+product_rate+\\" prod_add=\\"+product_id+\\">+<\\/span><\\/div><\\/div><div class=\'ecm_close\'><i class=\'fas fa-trash-alt\' item_id=\\"+product_id+\\" id=\'delete\'><\\/i><\\/div><\\/div><\\/li>\\");\\r\\n                          \\r\\n                            \\r\\n                           $(\\"#product_item\\").append(\\"<tr class=\'list_id\\"+product_id+\\"\'><td><div class=\'eca_product_thums\'><img src=\'\\"+image_path+\\"\' alt=\'\'><\\/div><\\/td><td><div><h4>\\"+product_name+\\"<\\/h4><\\/div><\\/td><td class=\'price\'>\\"+product_price+\\"<\\/td><td><div class=\'number\'><span class=\'minus\'  minus_price=\\"+product_rate+\\" prod_minus=\\"+product_id+\\">-<\\/span><input type=\'text\' disabled value=\\"+items_object.item+\\" \\/><span class=\'plus\' add_price=\\"+product_rate+\\" prod_add=\\"+product_id+\\">+<\\/span><\\/div><\\/td><td class=\'product_total\\"+product_id+\\"\'>$\\"+product_total+\\"<\\/td><td align=\'center\'><div class=\'close_eca\'><i class=\'fas fa-times\' item_id=\\"+product_id+\\" id=\'delete\'><\\/i><\\/div><\\/td><\\/tr>\\");\\r\\n                            \\r\\n                            \\r\\n                            \\r\\n                            \\r\\n                                \\r\\n                          }\\r\\n                    } \\r\\n                        var total = localStorage.getItem(\'total\');\\r\\n                        $(\\".money\\").text(\\"$\\"+total);\\r\\n\\r\\n                        var items = localStorage.getItem(\'items\');\\r\\n                        $(\\"#cart_count\\").text(items);\\r\\n            \\r\\n            \\r\\n          }\\r\\n            else\\r\\n            {\\r\\n              localStorage.setItem(\\"total\\",0);\\r\\n              localStorage.setItem(\\"items\\",0);\\r\\n            }\\r\\n           \\r\\n           \\r\\n         \\r\\n         \\r\\n    });\\r\\n    \\r\\n    <\\/script>\\r\\n\\r\\n    <script type=\\"text\\/javascript\\">\\r\\n\\r\\n\\r\\n    $(document).ready(function() {\\r\\n\\r\\n        $(\\"#cart_item\\").click(function() {\\r\\n            $(\'.ecm_header\').addClass(\'menu_fix\');\\r\\n        });\\r\\n\\r\\n        $(\\"#close_cart\\").click(function() {\\r\\n            $(\'.ecm_header\').removeClass(\'menu_fix\');\\r\\n        });\\r\\n    });\\r\\n\\r\\n    <\\/script>\\r\\n\\r\\n\\r\\n\\r\\n    <!--   E-cart Module end  -->  \\r\\n    {{ standard_footer_includes }}\\r\\n  <\\/body>\\r\\n<\\/html>\\r\\n"
                                    }',
                  CURLOPT_HTTPHEADER => array(
                     'Authorization: Bearer '.$token,
                    'Content-Type: application/json',
                    'Cookie: __cfduid=d9d0152aaefdff0e8dd6c4e674ef840e91619001813'
                  ),
                ));

                $response = curl_exec($curl);

                curl_close($curl);



     }


    public function get_product_list() {

        $users = User::where('id', Auth::user()->id)->first();
        $hapi_key = '';
        if(!empty(DB::table('credentials')->where('type','ecommerce')->wherePortalId(Session::get('portal_id'))->value('data'))){
            $credential_all = unserialize(DB::table('credentials')->where('type','ecommerce')->wherePortalId(Session::get('portal_id'))->value('data'));
            $hapi_key = Crypt::decryptString($credential_all['hapi_key']);
            $razor_key = @$credential_all['razor_key'];
            $paypal_key = $credential_all['paypal_key'];
            $stripe_sk_key = $credential_all['stripe_sk_key'];
            $stripe_pk_key = $credential_all['stripe_pk_key'];
        }
            
        if(!empty($hapi_key))
        {
            // Create product property
            $this->create_product_properties($hapi_key);
            
            // Create product property

            //template creation
          //            	$this->curlShippingTemplate($hapi_key);
					// $this->curlCartTemplate($hapi_key);
                    $this->templates_generation($hapi_key);
						//template creation

            // $absolute_url = DB::table('ecommerce_templates')->where('page', 'cart')->where('portal_id', Session::get('portal_id') )->value('slug');
            // if(empty($absolute_url)){
            //     $args['url'] = "https://api.hubapi.com/content/api/v2/pages?hapikey=".$hapi_key;
            //     $milliseconds = time() * 1000;
            //     // $args['data'] = '{
            //     //     "name": "Cart Page",
            //     //     "template_path" : "test-theme/templates/cart-design.html",
            //     //     "publish_immediately" : 1,
            //     //     "publish_date" : "'.$milliseconds.'",
            //     //     "slug" : "cart_web_page"
            //     // }';
            //     $args['data'] = '{
            //         "name": "Cart Page",
            //         "template_path" : "custom/cartApi.html",
            //         "publish_immediately" : 1,
            //         "publish_date" : "'.$milliseconds.'",
            //         "slug" : "cart",
            //         "html_title": "The API Test Page"
                   
            //     }';
                
            //     $res = $this->curlAccessG('POST',$args);

            //     if(isset($res->status)){
            //         echo 'Template is not able to create.';
            //         die;
            //     } else {

            //         $absolute_url = $res->absolute_url;
            //         $page_id = $res->analytics_page_id;
            //         $args['url'] = "https://api.hubapi.com/content/api/v2/pages/".$page_id."/publish-action?hapikey=".$hapi_key;
            //         $args['data'] = '{"action":"schedule-publish"}';
            //         $res = $this->curlAccessG('POST',$args);
            //         DB::table('ecommerce_templates')->insert(['slug' => $absolute_url, 'page_id' => $page_id, 'user_id' => Auth::user()->id, 'page' => 'cart', 'portal_id' => Session::get('portal_id') ]);
            //     }
            // }

            // $absolute_url2 = DB::table('ecommerce_templates')->where('page', 'checkout')->where('portal_id', Session::get('portal_id') )->value('slug');
            // if(empty($absolute_url2)){
            //     $args['url'] = "https://api.hubapi.com/content/api/v2/pages?hapikey=".$hapi_key;
            //     $milliseconds = time() * 1000;
            //     // $args['data'] = '{
            //     //     "name": "Checkout Page",
            //     //     "template_path" : "test-theme/templates/shipping.html",
            //     //     "publish_immediately" : 1,
            //     //     "publish_date" : "'.$milliseconds.'",
            //     //     "slug" : "checkout_page"
            //     // }';
            //     $args['data'] = '{
            //         "name": "Checkout Page",
            //         "template_path" : "custom/checkoutApi.html",
            //         "publish_immediately" : 1,
            //         "publish_date" : "'.$milliseconds.'",
            //         "slug" : "checkout",
            //         "html_title": "The checkout  Page"
                    
            //     }';
            //     $res = $this->curlAccessG('POST',$args);

            //     if(isset($res->status)){
            //         echo 'Checkout page template is not able to create.';
            //         die;
            //     } else {

            //         $absolute_url2 = $res->absolute_url;
            //         $page_id = $res->analytics_page_id;
            //         $args['url'] = "https://api.hubapi.com/content/api/v2/pages/".$page_id."/publish-action?hapikey=".$hapi_key;
            //         $args['data'] = '{"action":"schedule-publish"}';
            //         $res = $this->curlAccessG('POST',$args);
            //         DB::table('ecommerce_templates')->insert(['slug' => $absolute_url2, 'page_id' => $page_id, 'user_id' => Auth::user()->id, 'page' => 'checkout', 'portal_id' => Session::get('portal_id') ]);
            //     }
            // }

            // $result = $this->productSync($hapi_key, $absolute_url, $credential_all);

            return ['status' => true, 'message' => 'Product Image Field has been pushed.'];
            //return view('ecommerce.product-list-code', compact('data','next'));
            //return view('ecommerce.product-list-code',compact('data','next'));
            //$next = url('apps/ecommerce/get-product-list') .'?offset='. $results->paging->next->after;
            //echo 'test';
        } else {
            return ['status' => false, 'message' => 'Hubspot key is required.'];
            
        }
    }
    private function create_product_properties($hapi_key){
        $argsp['data'] = '{
            "name": "image",
            "label": "Product Image",
            "description": "Product Image",
            "groupName": "productinformation",
            "type": "string",
            "fieldType": "text",
            "formField": true,
            "displayOrder": 6,
            "options": [
            ]
        }';
        $argsp['url'] = 'https://api.hubapi.com/properties/v1/products/properties?hapikey='.$hapi_key;
        $resp = $this->curlAccessG('POST',$argsp);

        	
        //this will add property if it's not there
        // $argsp['data'] = '
        //     {
        //         "name": "full_embed_code",
        //         "label": "Full Embed code",
        //         "description": "Full Embed code",
        //         "groupName": "productinformation",
        //         "type": "string",
        //         "fieldType": "text",
        //         "formField": true,
        //         "displayOrder": 6,
        //         "options": [
        //         ]
        //     }';
        // $resp = $this->curlAccessG('POST',$argsp);
        return 'true';
    }
    private function productSync($hapi_key, $absolute_url, $credential_all, $after = ''){

        $args['url'] = 'https://api.hubapi.com/crm/v3/objects/products?limit=10&hapikey='.$hapi_key;
        if(!empty($after)) {
            $args['url'] = 'https://api.hubapi.com/crm/v3/objects/products?limit=10&after=' . $after . '&hapikey='.$hapi_key;
        }
        
        $results = $this->curlAccessH('GET',$args);
        $data = [];
        if(!empty($results->results)){
            foreach($results->results as $key => $value) {
                $v = json_decode(json_encode($value), true);
                $data[$key] = $v ;
                $product_id = $v['id'];
                
                $embed_url = $absolute_url.'?product_id='.$product_id;
                $full_embed_url = $absolute_url.'?product_id='.$product_id.'&hapi_key='.$hapi_key;
            
                if(!empty($credential_all['paypal_key'])){
                    $full_embed_url .= '&paypal='.$credential_all['paypal_key'];
                }
                if(!empty($credential_all['stripe_pk_key']) && !empty($credential_all['stripe_sk_key'])){
                    $full_embed_url .= '&stripe_sk='.$credential_all['stripe_sk_key'];
                    $full_embed_url .= '&stripe_pk='.$credential_all['stripe_pk_key'];
                }
                if(!empty($credential_all['razor_key'])){
                    $full_embed_url .= '&razor='.$credential_all['razor_key'];   
                }
                
                $product_name = $v['properties']['name'];
                $product_price = $v['properties']['price'];

                $args['url'] = "https://api.hubapi.com/crm-objects/v1/objects/products/". $product_id ."?hapikey=".$hapi_key;
                // $args['data'] = '[
                //                     {
                //                         "name": "embed_code",
                //                         "value": "'.$embed_url.'"
                //                     }, 
                //                     {
                //                         "name": "full_embed_code",
                //                         "value": "'.$full_embed_url.'"
                //                     }
                //                 ]';
                $args['data'] = '[
                                    {
                                        "name": "product_image",
                                        "value": ""
                                    }
                                ]';
                $res = $this->curlAccessH('PUT',$args);
               

            }
        }

        if(isset($results->paging)){
            if(isset($results->paging->next->after)){
                $after = $results->paging->next->after;
                $results = $this->productSync($hapi_key, $absolute_url, $credential_all ,$after);
            }
        }
        return $results;
    }


    // public function get_product_list_hub($auth_id) {
      
    //     $users = User::where('id', $auth_id)->first();
    //     $hapi_key = '';
    //     if(!empty(DB::table('credentials')->where('type','ecommerce')->whereUserId($auth_id)->value('data'))){
  
    //         $credential_all = unserialize(DB::table('credentials')->where('type','ecommerce')->whereUserId($auth_id)->value('data'));
    //         $hapi_key = $credential_all['hapi_key'];
    //         $razor_key = @$credential_all['razor_key'];
    //         $paypal_key = $credential_all['paypal_key'];
    //         $stripe_sk_key = $credential_all['stripe_sk_key'];
    //         $stripe_pk_key = $credential_all['stripe_pk_key'];
    //     }
    //    if(!empty($hapi_key) && !empty($razor_key)){

    //         // Create product property
    //         $argsp['data'] = '{
    //                             "name": "embed_code",
    //                             "label": "Embed code",
    //                             "description": "Embed code",
    //                             "groupName": "productinformation",
    //                             "type": "string",
    //                             "fieldType": "text",
    //                             "formField": true,
    //                             "displayOrder": 6,
    //                             "options": [
    //                             ]
    //                         },
    //                         {
    //                             "name": "full_embed_code",
    //                             "label": "Full Embed code",
    //                             "description": "Full Embed code",
    //                             "groupName": "productinformation",
    //                             "type": "string",
    //                             "fieldType": "text",
    //                             "formField": true,
    //                             "displayOrder": 6,
    //                             "options": [
    //                             ]
    //                         }';
    //         $argsp['url'] = 'https://api.hubapi.com/properties/v1/products/properties?hapikey='.$hapi_key;
    //         $resp = $this->curlAccessG('POST',$argsp);
           
    //        //this will add property if it's not there
    //                     $argsp['data'] = '
    //                         {
    //                             "name": "full_embed_code",
    //                             "label": "Full Embed code",
    //                             "description": "Full Embed code",
    //                             "groupName": "productinformation",
    //                             "type": "string",
    //                             "fieldType": "text",
    //                             "formField": true,
    //                             "displayOrder": 6,
    //                             "options": [
    //                             ]
    //                         }';
    //             $argsp['url'] = 'https://api.hubapi.com/properties/v1/products/properties?hapikey='.$hapi_key;
    //            $resp = $this->curlAccessG('POST',$argsp);



    //         // Template start
    //         $absolute_url = DB::table('ecommerce_templates')->where('user_id', $auth_id )->value('slug');
    //         if(empty($absolute_url)){
    //             $args['url'] = "https://api.hubapi.com/content/api/v2/pages?hapikey=".$hapi_key;
    //             $milliseconds = time() * 1000;
    //             $args['data'] = '{
    //                 "name": "Checkout Page",
    //                 "template_path" : "test-theme/templates/cart-design.html",
    //                 "publish_immediately" : 1,
    //                 "publish_date" : "'.$milliseconds.'",
    //                 "slug" : "checkout_web_page"
    //             }';
    //             $res = $this->curlAccessG('POST',$args);
    //             if(isset($res->status)){
    //                 echo 'Template is not able to create.';
    //                 die;
    //             } else {
    //                 $absolute_url = $res->absolute_url;
    //                 $page_id = $res->analytics_page_id;
    //                 $args['url'] = "https://api.hubapi.com/content/api/v2/pages/".$page_id."/publish-action?hapikey=".$hapi_key;
    //                 $args['data'] = '{"action":"schedule-publish"}';
    //                 $res = $this->curlAccessG('POST',$args);
    //                 DB::table('ecommerce_templates')->insert(['slug' => $absolute_url, 'page_id' => $page_id, 'user_id' => $auth_id ]);
    //             }

    //         }
    //         // Template start
    //         $args['url'] = 'https://api.hubapi.com/crm/v3/objects/products?limit=10&properties=price&properties=name&properties=description&properties=currency_code&properties=full_embed_code&hapikey='.$hapi_key;
    //         if(isset($_GET['offset'])){
    //             $args['url'] = 'https://api.hubapi.com/crm/v3/objects/products?limit=10&properties=price&properties=name&properties=description&properties=currency_code&properties=full_embed_code&after=' . $_GET['offset'] . '&hapikey='.$hapi_key;
    //         }
    //         $results = $this->curlAccessH('GET',$args);
            
           
    //        try{
    //           $array_result=array($results);
    //           $url=$array_result[0]->paging->next->link;
    //           $pagingurl=$url."&hapikey=$hapi_key";  //here we are saving the next page url code with embeding api key if its not available then it will raise exception and the code will go into catch statement.     

    //         $data = [];
    //         $next = '';
    //         $prev = '';
    //         $i = 1;
    //         $count=$array_result[0]->results;
    //         $len = count($count);//keeping count of result items i.e. products
        
    //         if(!empty($results->results)){
    //             foreach($results->results as $key => $value) {
    //                 if($i!=$len){

    //                 $v = json_decode(json_encode($value), true);
    //                 $data[$key] = $v;
    //                 $product_id = $v['id'];
                    
    //                     $embed_url = $absolute_url.'?product_id='.$product_id;
    //                     $full_embed_url = $absolute_url.'?product_id='.$product_id.'&hapi_key='.$hapi_key;
           
    //                     if(!empty($paypal_key)){
    //                         $full_embed_url .= '&paypal='.$paypal_key;
    //                     }
    //                     if(!empty($stripe_pk_key) && !empty($stripe_sk_key)){
    //                         $full_embed_url .= '&stripe_sk='.$stripe_sk_key;
    //                         $full_embed_url .= '&stripe_pk='.$stripe_pk_key;
    //                     }
    //                     $product_name = $v['properties']['name'];
    //                     $product_price = $v['properties']['price'];
                
    //                     $args['url'] = "https://api.hubapi.com/crm-objects/v1/objects/products/". $product_id ."?hapikey=".$hapi_key;
    //                     $args['data'] = '[
    //                                         {
    //                                             "name": "embed_code",
    //                                             "value": "'.$embed_url.'"
    //                                         },
    //                                         {
    //                                             "name": "full_embed_code",
    //                                             "value": "'.$full_embed_url.'"
    //                                         }
    //                                     ]';
    //                     $results = $this->curlAccessH('PUT',$args);
    //                 }
    //                 elseif($i==$len){//when last item comes..it should call the next page so in the end we have appended the function call
   
    //                 $v = json_decode(json_encode($value), true);
    //                 $data[$key] = $v;
    //                 $product_id = $v['id'];
    //                     $embed_url = $absolute_url.'?product_id='.$product_id;
    //                     $full_embed_url = $absolute_url.'?product_id='.$product_id.'&hapi_key='.$hapi_key;
               
    //                     if(!empty($paypal_key)){
    //                         $full_embed_url .= '&paypal='.$paypal_key;
    //                     }
    //                     if(!empty($stripe_pk_key) && !empty($stripe_sk_key)){
    //                         $full_embed_url .= '&stripe_sk='.$stripe_sk_key;
    //                         $full_embed_url .= '&stripe_pk='.$stripe_pk_key;
    //                     }
    //                     $product_name = $v['properties']['name'];
    //                     $product_price = $v['properties']['price'];
    //                     $args['url'] = "https://api.hubapi.com/crm-objects/v1/objects/products/". $product_id ."?hapikey=".$hapi_key;
    //                     $args['data'] = '[
    //                                         {
    //                                             "name": "embed_code",
    //                                             "value": "'.$embed_url.'"
    //                                         },
    //                                         {
    //                                             "name": "full_embed_code",
    //                                             "value": "'.$full_embed_url.'"
    //                                         }
    //                                     ]';
    //                     $results = $this->curlAccessH('PUT',$args);
    //                     $this->calling_object($pagingurl,$absolute_url,$hapi_key);//this function calls the next page 
    //                 }//else if ends
    //               $i++;
    //             }//foreach ends
 
    //          }//outer if ends
    //       }//try ends
    //        catch(\throwable $err)//when there are less then 10 products then this catch code will get execute
    //         {
    //              if(!empty($results->results)){
    //             foreach($results->results as $key => $value) {
    //                 $v = json_decode(json_encode($value), true);
    //                 $data[$key] = $v;
    //                 $product_id = $v['id'];
               
    //                     $embed_url = $absolute_url.'?product_id='.$product_id;
    //                     $full_embed_url = $absolute_url.'?product_id='.$product_id.'&hapi_key='.$hapi_key;
                
    //                     if(!empty($paypal_key)){
    //                         $full_embed_url .= '&paypal='.$paypal_key;
    //                     }
    //                     if(!empty($stripe_pk_key) && !empty($stripe_sk_key)){
    //                         $full_embed_url .= '&stripe_sk='.$stripe_sk_key;
    //                         $full_embed_url .= '&stripe_pk='.$stripe_pk_key;
    //                     }
    //                     $product_name = $v['properties']['name'];
    //                     $product_price = $v['properties']['price'];
   
    //                     $args['url'] = "https://api.hubapi.com/crm-objects/v1/objects/products/". $product_id ."?hapikey=".$hapi_key;
    //                     $args['data'] = '[
    //                                         {
    //                                             "name": "embed_code",
    //                                             "value": "'.$embed_url.'"
    //                                         },
    //                                         {
    //                                             "name": "full_embed_code",
    //                                             "value": "'.$full_embed_url.'"
    //                                         }
    //                                     ]';
    //                     $results = $this->curlAccessH('PUT',$args);
                   
                 
    //              }//catch foreach ends

    //       }//catch if ends
    //   }//catch ends 

    //         return ['status'=> true, 'message' => 'Cart link has been set in product embed code property.'];
    //     } else {
    //         $message = 'Hubspot key is required.';
    //         return ['status'=> false, 'message' =>$message];
    //     }
    // }

    // public function home()
    // {
    //     return view('ecommerce.websites.home');
    // }
    
    // public function ctabutton()
    // {
    //     return view('render.ecommerce.cta');
    // }
    // public function calling_object($url,$absolute_url,$hapi_key)
    // {   
    //     //this function will simply recives thhe url and call the next product page 
    //     $args['url']=$url;
    //     $results = $this->curlAccessH('GET',$args);
    //     $this->process_results($results,$absolute_url,$hapi_key);//after getting prodcuts we will pass it on to another function to process that page and see if there is another page left or not.
        
    // }
    
    // public function process_results($results,$absolute_url,$hapi_key)
    // {     
    //       $paging_url="";
    //       $array_result=array($results);
          
    //       try{
    //       $url=$array_result[0]->paging->next->link;//if there is no other page left then this will raise exception and code will go into catch command
           
    //       $paging_url=$url."&hapikey=$hapi_key";//else it will store the next page url and we will make the call to calling_object function  
    //       $i = 1;
    //       $count=$array_result[0]->results;
    //       $len = count($count);//keeps count of products in the result
    //       if(!empty($results->results)){
    //             foreach($results->results as $key => $value) {
                   
    //                 if($i!=$len){
    //                     $v = json_decode(json_encode($value), true);
    //                 $data[$key] = $v;
    //                 $product_id = $v['id'];

    //                     $embed_url = $absolute_url.'?product_id='.$product_id;
    //                     $full_embed_url = $absolute_url.'?product_id='.$product_id.'&hapi_key='.$hapi_key;
             
    //                     if(!empty($paypal_key)){
    //                         $full_embed_url .= '&paypal='.$paypal_key;
    //                     }
    //                     if(!empty($stripe_pk_key) && !empty($stripe_sk_key)){
    //                         $full_embed_url .= '&stripe_sk='.$stripe_sk_key;
    //                         $full_embed_url .= '&stripe_pk='.$stripe_pk_key;
    //                     }
    //                     $product_name = $v['properties']['name'];
    //                     $product_price = $v['properties']['price'];
    //                     $args['url'] = "https://api.hubapi.com/crm-objects/v1/objects/products/". $product_id ."?hapikey=".$hapi_key;
    //                     $args['data'] = '[
    //                                         {
    //                                             "name": "embed_code",
    //                                             "value": "'.$embed_url.'"
    //                                         },
    //                                         {
    //                                             "name": "full_embed_code",
    //                                             "value": "'.$full_embed_url.'"
    //                                         }
    //                                     ]';
    //                     $results = $this->curlAccessH('PUT',$args);
                    
    //               }
    //        else{
    //          $v = json_decode(json_encode($value), true);
    //                 $data[$key] = $v;
    //                 $product_id = $v['id'];
            
    //                     $embed_url = $absolute_url.'?product_id='.$product_id;
    //                     $full_embed_url = $absolute_url.'?product_id='.$product_id.'&hapi_key='.$hapi_key;
                      
    //                     if(!empty($paypal_key)){
    //                         $full_embed_url .= '&paypal='.$paypal_key;
    //                     }
    //                     if(!empty($stripe_pk_key) && !empty($stripe_sk_key)){
    //                         $full_embed_url .= '&stripe_sk='.$stripe_sk_key;
    //                         $full_embed_url .= '&stripe_pk='.$stripe_pk_key;
    //                     }
    //                     $product_name = $v['properties']['name'];
    //                     $product_price = $v['properties']['price'];
                       
    //                     $args['url'] = "https://api.hubapi.com/crm-objects/v1/objects/products/". $product_id ."?hapikey=".$hapi_key;
    //                     $args['data'] = '[
    //                                         {
    //                                             "name": "embed_code",
    //                                             "value": "'.$embed_url.'"
    //                                         },
    //                                         {
    //                                             "name": "full_embed_code",
    //                                             "value": "'.$full_embed_url.'"
    //                                         }
    //                                     ]';
    //                     $results = $this->curlAccessH('PUT',$args);
                           
    //                         if($url!=""){   
    //                         $this->calling_object($paging_url,$absolute_url,$hapi_key);    
    //                         }
    //                         else{
    //                             return ['status'=> true, 'message' => 'Cart link has been set in product embed code property.'];
    //                         }
                        
    //                 }//else ends
    //                 $i++;
    //           }//foreach ends
    //        }//outer if ends
    //    }//try ends
    //     catch(\Throwable $e){
        
    //             if(!empty($results->results)){
    //             foreach($results->results as $key => $value) {
    //                 $v = json_decode(json_encode($value), true);
    //                 $data[$key] = $v;
    //                 $product_id = $v['id'];
                    

                 
    //                     $embed_url = $absolute_url.'?product_id='.$product_id;
    //                     $full_embed_url = $absolute_url.'?product_id='.$product_id.'&hapi_key='.$hapi_key;
                      
    //                     if(!empty($paypal_key)){
    //                         $full_embed_url .= '&paypal='.$paypal_key;
    //                     }
    //                     if(!empty($stripe_pk_key) && !empty($stripe_sk_key)){
    //                         $full_embed_url .= '&stripe_sk='.$stripe_sk_key;
    //                         $full_embed_url .= '&stripe_pk='.$stripe_pk_key;
    //                     }
    //                     $product_name = $v['properties']['name'];
    //                     $product_price = $v['properties']['price'];
                     
    //                     $args['url'] = "https://api.hubapi.com/crm-objects/v1/objects/products/". $product_id ."?hapikey=".$hapi_key;
    //                     $args['data'] = '[
    //                                         {
    //                                             "name": "embed_code",
    //                                             "value": "'.$embed_url.'"
    //                                         },
    //                                         {
    //                                             "name": "full_embed_code",
    //                                             "value": "'.$full_embed_url.'"
    //                                         }
    //                                     ]';
    //                     $results = $this->curlAccessH('PUT',$args);
    //                    } //foreach end     
    //               }//if end 
    //                return ['status'=> true, 'message' => 'Cart link has been set in product embed code property.'];

    //          }//catch end
    //   }//function end
}//class end
    
