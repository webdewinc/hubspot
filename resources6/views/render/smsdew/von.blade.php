<?php
$vendor = vendor_select('von');
?>
<p><a href="javascript:void(0);" title="Webdew Groups" ><strong id="select-vendor-append">{{$vendor['name']}}</strong></a></p>             
@csrf
<div class="kt-wizard-v2__form">
        
    <?php
        if(empty($data)){
            $button = 'Save';
        } else {
            $button = 'Update';
        }
    ?>
    <div class="form-row">
        <div class="form-group col-md-6 col-12">
            <label class="required">{{$vendor['label']['apikey_vo']}}</label>
            <div class="input-group">
                <input class="form-control" type="text" id="apikey_vo" name="apikey_vo" value="{{@$data['apikey_vo']}}" required="" autocomplete="off">
            </div>
        </div>
         <div class="form-group col-md-6 col-12">
            <label class="required">{{$vendor['label']['apisecret_vo']}}</label>
            <div class="input-group">
                <input class="form-control" type="text" id="apisecret_vo" name="apisecret_vo" value="{{@$data['apisecret_vo']}}" required="" autocomplete="off">
            </div>
        </div>
		
    </div>
  

  
	 <div class="form-group col-md-6 col-12">            
            <div class="input-group">
                
				<label class="kt-checkbox">
				   <input type="checkbox" id="default" name="default" value="von" @if(@$type == 'von'){{'checked'}}@endif> Set as default
					<span></span>
				</label>
            </div>
        </div>
   
   
    <div class="kt-form__actions">
        <button class="btn btn-label-brand btn-bold"  type="submit" data-ktwizard-type="action-submit">
            {{$button}}
        </button>
    </div>
</div>