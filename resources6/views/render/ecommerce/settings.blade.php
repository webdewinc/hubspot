
@csrf
<div class="kt-wizard-v2__form">
        
    <?php
        if(empty($data)){
            $button = 'Save';
        } else {
            $button = 'Update';
        }
    ?>
    <div class="form-row">
        <div class="form-group col-md-12 col-12">
            <label class="required">Stripe PK</label>
            <div class="input-group">
                <input class="form-control" type="text" id="stripe_pk_key" name="stripe_pk_key" value="{{@$data['stripe_pk_key']}}" minlength="25" maxlength="200" required="" autocomplete="off">
            </div>
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-12 col-12">
            <label class="required">Stripe SK</label>
            <div class="input-group">
                <input class="form-control" type="text" id="stripe_sk_key" name="stripe_sk_key" value="{{@$data['stripe_sk_key']}}" minlength="25" maxlength="200" required="" autocomplete="off">
            </div>
        </div>
    </div>

    <div class="form-row">
        <div class="form-group col-md-12 col-12">
            <label class="required">Paypal</label>
            <div class="input-group">
                <input class="form-control" type="text" id="paypal_key" name="paypal_key" value="{{@$data['paypal_key']}}" minlength="2" maxlength="200" required="" autocomplete="off">
            </div>
        </div>
    </div>

    <div class="form-row">
        <div class="form-group col-md-12 col-12">
            <label class="required">Razor Key</label>
            <div class="input-group">
                <input class="form-control" type="text" id="razor_key" name="razor_key" value="{{@$data['razor_key']}}" minlength="20" maxlength="200" required="" autocomplete="off">
            </div>
        </div>
    </div>
    
    <div class="form-row">
        <div class="form-group col-md-12 col-12">
            <label class="required">Hapi Key</label>
            <input class="form-control" type="text" id="hapi_key" name="hapi_key" value="{{@$data['hapi_key']}}" minlength="30" maxlength="200" required="" autocomplete="off">    
        </div>
    </div>
   
    <div class="kt-form__actions">
        <button class="btn btn-label-brand btn-bold"  type="submit" data-ktwizard-type="action-submit">
            {{$button}}
        </button>
    </div>
</div>