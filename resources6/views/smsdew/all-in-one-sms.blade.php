@extends('layout.app')
@php
$totalTitle = 'Home';
@endphp
@section('title', $totalTitle)
@section('title-description')

<meta name="description" content="Home">

<style type="text/css">
    
/* font kit start here */

@font-face {
    font-family: 'proxima_nova_blblack';
    src: url('../hubspot/public/assets/fonts/proxima_nova_black.eot');
    src: url('../hubspot/public/assets/fonts/proxima_nova_black.eot?#iefix') format('embedded-opentype'),
         url('../hubspot/public/assets/fonts/proxima_nova_black.woff2') format('woff2'),
         url('../hubspot/public/assets/fonts/proxima_nova_black.woff') format('woff'),
         url('../hubspot/public/assets/fonts/proxima_nova_black.ttf') format('truetype'),
         url('../hubspot/public/assets/fonts/proxima_nova_black.svg#proxima_nova_blblack') format('svg');
    font-weight: normal;
    font-style: normal;

}
@font-face {
    font-family: 'proxima_nova_rgbold';
    src: url('../hubspot/public/assets/fonts/proxima_nova_bold.eot');
    src: url('../hubspot/public/assets/fonts/proxima_nova_bold.eot?#iefix') format('embedded-opentype'),
         url('../hubspot/public/assets/fonts/proxima_nova_bold.woff2') format('woff2'),
         url('../hubspot/public/assets/fonts/proxima_nova_bold.woff') format('woff'),
         url('../hubspot/public/assets/fonts/proxima_nova_bold.ttf') format('truetype'),
         url('../hubspot/public/assets/fonts/proxima_nova_bold.svg#proxima_nova_rgbold') format('svg');
    font-weight: normal;
    font-style: normal;

}
@font-face {
    font-family: 'proxima_nova_thextrabold';
    src: url('../hubspot/public/assets/fonts/proxima_nova_extrabold.eot');
    src: url('../hubspot/public/assets/fonts/proxima_nova_extrabold.eot?#iefix') format('embedded-opentype'),
         url('../hubspot/public/assets/fonts/proxima_nova_extrabold.woff2') format('woff2'),
         url('../hubspot/public/assets/fonts/proxima_nova_extrabold.woff') format('woff'),
         url('../hubspot/public/assets/fonts/proxima_nova_extrabold.ttf') format('truetype'),
         url('../hubspot/public/assets/fonts/proxima_nova_extrabold.svg#proxima_nova_thextrabold') format('svg');
    font-weight: normal;
    font-style: normal;

}
@font-face {
    font-family: 'proxima_nova_ltlight';
    src: url('../hubspot/public/assets/fonts/proxima_nova_light.eot');
    src: url('../hubspot/public/assets/fonts/proxima_nova_light.eot?#iefix') format('embedded-opentype'),
         url('../hubspot/public/assets/fonts/proxima_nova_light.woff2') format('woff2'),
         url('../hubspot/public/assets/fonts/proxima_nova_light.woff') format('woff'),
         url('../hubspot/public/assets/fonts/proxima_nova_light.ttf') format('truetype'),
         url('../hubspot/public/assets/fonts/proxima_nova_light.svg#proxima_nova_ltlight') format('svg');
    font-weight: normal;
    font-style: normal;

}
@font-face {
    font-family: 'proxima_nova_rgregular';
    src: url('../hubspot/public/assets/fonts/proxima_nova_regular.eot');
    src: url('../hubspot/public/assets/fonts/proxima_nova_regular.eot?#iefix') format('embedded-opentype'),
         url('../hubspot/public/assets/fonts/proxima_nova_regular.woff2') format('woff2'),
         url('../hubspot/public/assets/fonts/proxima_nova_regular.woff') format('woff'),
         url('../hubspot/public/assets/fonts/proxima_nova_regular.ttf') format('truetype'),
         url('../hubspot/public/assets/fonts/proxima_nova_regular.svg#proxima_nova_rgregular') format('svg');
    font-weight: normal;
    font-style: normal;

}
@font-face {
    font-family: 'proxima_nova_ltsemibold';
    src: url('../hubspot/public/assets/fonts/proxima_nova_semibold.eot');
    src: url('../hubspot/public/assets/fonts/proxima_nova_semibold.eot?#iefix') format('embedded-opentype'),
         url('../hubspot/public/assets/fonts/proxima_nova_semibold.woff2') format('woff2'),
         url('../hubspot/public/assets/fonts/proxima_nova_semibold.woff') format('woff'),
         url('../hubspot/public/assets/fonts/proxima_nova_semibold.ttf') format('truetype'),
         url('../hubspot/public/assets/fonts/proxima_nova_semibold.svg#proxima_nova_ltsemibold') format('svg');
    font-weight: normal;
    font-style: normal;

}
@font-face {
    font-family: 'proxima_novathin_italic';
    src: url('../hubspot/public/assets/fonts/proxima_nova_thin_italic.eot');
    src: url('../hubspot/public/assets/fonts/proxima_nova_thin_italic.eot?#iefix') format('embedded-opentype'),
         url('../hubspot/public/assets/fonts/proxima_nova_thin_italic.woff2') format('woff2'),
         url('../hubspot/public/assets/fonts/proxima_nova_thin_italic.woff') format('woff'),
         url('../hubspot/public/assets/fonts/proxima_nova_thin_italic.ttf') format('truetype'),
         url('../hubspot/public/assets/fonts/proxima_nova_thin_italic.svg#proxima_novathin_italic') format('svg');
    font-weight: normal;
    font-style: normal;

}
@font-face {
    font-family: 'proxima_nova_ththin';
    src: url('../hubspot/public/assets/fonts/proxima_nova_thin.eot');
    src: url('../hubspot/public/assets/fonts/proxima_nova_thin.eot?#iefix') format('embedded-opentype'),
         url('../hubspot/public/assets/fonts/proxima_nova_thin.woff2') format('woff2'),
         url('../hubspot/public/assets/fonts/proxima_nova_thin.woff') format('woff'),
         url('../hubspot/public/assets/fonts/proxima_nova_thin.ttf') format('truetype'),
         url('../hubspot/public/assets/fonts/proxima_nova_thin.svg#proxima_nova_ththin') format('svg');
    font-weight: normal;
    font-style: normal;

}
/* font kit end here */


/* header css */

* {
    font-family: 'proxima_nova_rgregular';
    font-weight: normal;
}
h1, h2, h3, h4, h5, h6 {
    font-family: 'proxima_nova_rgbold';
    font-weight: normal;
}
.smshub_header {
    padding: 55px 0;
    left: 0;
    right: 0;
    top: 0;
    position: absolute;
    z-index: 999;
}
#smshub_menu a {
    font-size: 20px;
    color: #33475b;
}
#smshub_menu a:hover {
    color: #00a9fa;
}
#smshub_menu {
    display: flex;
    margin-left: 65px;
    margin-top: -9px;
}
#smshub_menu li {
    margin-right: 45px;
}

/* banner css */

/*.smshub_banner-content p {
    font-size: 22px;
}*/
.smshub_banner-actionBtn {
    margin-top: 43px;
}
.smshub_banner-actionBtn a.btn {
    margin-right: 20px;
}
.smshub_banner {
    padding-top: 345px;
    padding-bottom: 280px;
    position: relative;
}
.smshub_banner-img {
    padding: 415px 432px;
    position: absolute;
    right: 0;
    top: 0;
    background-image: url(../hubspot/public/assets/media/misc/banner-img.png);
    background-size: cover;
    background-position: center;
    background-repeat: no-repeat;
    bottom: 0;
    background-color: #00a9fa;
}
.smshub_banner div[class*="col-"] {
    position: inherit;
}
.smshub_banner-img:before {
    content: "";
    background-image: url(../hubspot/public/assets/media/misc/banner-shape.svg);
    background-position: center bottom;
    background-size: cover;
    background-repeat: no-repeat;
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
}
.primary-color {
    color: #00a9fa;
}
/* content box css */

.smshub-contentBox {
    padding-top: 145px;
    padding-bottom: 102px;
}
.smshub-contentBox-head h5 {
    color: #00a9fa;
    font-family: 'proxima_nova_rgregular';
    font-weight: normal;
}
.smshub-contentBox-head {
    margin-bottom: 72px;
}
.smshub-contentBox-iconInner {
    width: 135px;
    height: 135px;
    line-height: 135px;
    display: inline-block;
    vertical-align: middle;
    border: 2px dashed #00a9fa;
    border-radius: 100%;
    background: #eefaff;
}
.smshub-contentBox-icon {
    margin-bottom: 35px;
}
.smshub-contentBox-colInner {
    padding: 45px 25px 40px;
    border-radius: 10px;
    box-shadow: 0 0 8px rgba(0,0,0,.18);
}
.smshub-contentBox-col {
    margin-bottom: 40px;
}

/* ready drive css */

.smshub_ready-drive {
    padding-top: 115px;
    padding-bottom: 115px;
    background-image: url(../hubspot/public/assets/media/misc/ready-drive_bg.png);
    background-repeat: no-repeat;
    background-position: center top;
    background-size: cover;
}
.smshub_ready-drive-actionBtn a:last-child {
    margin-left: 20px;
}
.smshub_ready-drive-text h2 {
    text-transform: uppercase;
}
#smshub_mmenu {
    display: none;
}
div#kt_header {
    background: none;
    box-shadow: none;
    padding: 15px 0;
    height: auto;
}
div#kt_wrapper {
    padding: 0;
}
.kt-header__brand-logo img {
    height: 75px;
    width: auto !important;
}
.kt-scrolltop--on #kt_header {
    background: #fff;
}
.kt-scrolltop--on #kt_header img {
    height: 45px;
}

.kt-grid--root .btn.btn-label-brand {
    background-color: rgba(0, 155, 255, 0.1) !important;
    color: #009bff;
    cursor: text !important;
    border-color: rgba(0, 155, 255, 0) !important;
}
.kt-grid--root .btn.btn-label-brand:hover {
    background-color: rgba(0, 155, 255, 0.2) !important;
    color: #009bff;
}


/*@media screen and (min-width: 1200px) {

    .container {
        max-width: 1240px;
    }

    
}


@media screen and (min-width: 1600px) {

    .smshub_header .container, .smshub_banner .container {
        max-width: 1580px;
    }
    
}


@media screen and (min-width: 1500px) {

    .smshub_header .container, .smshub_banner .container {
        max-width: 1480px;
    }
    
}


@media screen and (min-width: 1400px) {

    .smshub_header .container, .smshub_banner .container {
        max-width: 1380px;
    }
    
}*/

@media screen and (max-width: 1899px) {

div.banner-animation-circle2 .circle-5 {
    margin-left: -9.7%;
}
section.smshub_banner {
    padding-top: 292px;
    padding-bottom: 222px;
}
div.smshub_banner-img {
    padding: 355px 479px;
}
div.smshub_banner-img:before {
    background-position: left bottom;
    width: 100%;
    height: 100%;
}


}

@media screen and (max-width: 1799px) {

div.banner-animation-circle2 .circle-5 {
    margin-left: -12.7%;
}


}

@media screen and (max-width: 1699px) {

div.banner-animation-circle2 .circle-5 {
    margin-left: -17.7%;
}
section.smshub-contentBox {
    padding-top: 85px;
    padding-bottom: 85px;
}
section.smshub_ready-drive {
    padding-top: 110px;
    padding-bottom: 110px;
}


}

@media screen and (max-width: 1599px) {

div.adg_banner-content h1 {
    font-size: 72px;
}
div.adg_banner-content p {
    font-size: 18px;
    max-width: 560px;
}
div.adg_banner-img {
    padding: 419px 355px;
}
div.banner-animation-circle2 .circle-6 {
    width: 290px;
    height: 290px;
    bottom: 0;
    margin-left: -0.5%;
}
section.adg_banner {
    padding-top: 195px;
    padding-bottom: 230px;
}
div.banner-animation-circle2 .circle-5 {
    margin-left: -11.7%;
}
section.smshub_banner {
    padding-top: 225px;
    padding-bottom: 197px;
}
div.smshub_banner-img {
    padding: 334px 431px;
}



}

@media screen and (max-width: 1499px) {

div.adg_banner-content h1 {
    font-size: 62px;
}
div.adg_banner-content p {
    font-size: 18px;
    max-width: 515px;
}
div.adg_banner-img {
    padding: 345px 340px;
}
section.adg_banner {
    padding-top: 160px;
    padding-bottom: 170px;
}
div.adg_our-stories-slider:before {
    width: 325px;
    height: 325px;
    top: auto;
    bottom: auto;
}
div.adg_our-stories-slider .swiper-container:before {
    width: 325px;
    height: 325px;
}
div.smshub_banner-img {
    padding: 295px 405px;
}
section.smshub_banner {
    padding-top: 245px;
    padding-bottom: 145px;
}
header.smshub_header {
    padding: 30px 0;
}
section.smshub_ready-drive {
    padding-top: 95px;
    padding-bottom: 80px;
}



}

@media screen and (max-width: 1399px) {

div.adg_banner-content h1 {
    font-size: 46px;
    line-height: 1.3;
}
div.adg_banner-content p {
    font-size: 16px;
    max-width: 468px;
}
div.adg_banner-img {
    padding: 315px 300px;
}
section.adg_banner {
    padding-top: 110px;
    padding-bottom: 185px;
}
div.hwh_banner-content {
    padding-left: 0;
}
div.adg_case-studyPost {
    padding: 275px 25px 51px;
}
div.adg_case-studyPost-content {
    padding: 28px 15px;
    min-height: 252px;
}
div.smshub_banner-img {
    padding: 260px 340px;
}
section.smshub_banner {
    padding-top: 210px;
    padding-bottom: 98px;
}
/*div.smshub_banner-content p {
    font-size: 20px;
}*/
div.smshub_brand-logo img {
    height: 55px;
}
ul#smshub_menu a {
    font-size: 18px;
}
ul#smshub_menu li {
    margin-right: 35px;
}
ul#smshub_menu {
    margin-left: 40px;
    margin-top: -6px;
}



}

@media screen and (max-width: 1199px) {

ul#adg_menu {
    display: none;
}
#menu {
    display: none;
}
.adg_mobile-menu {
    display: block !important;
}
.right_wap_memu.btn-nav {
    position: relative;
    background: transparent;
    border: none;
    -webkit-transition: all .5s ease;
    -moz-transition: all .5s ease;
    -ms-transition: all .5s ease;
    -o-transition: all .5s ease;
    transition: all .5s ease;
    z-index: 99999;
    display: flex;
    height: 46px;
    margin-left: 15px;
    align-items: center;
    border: 1px solid #194278;
    padding: 0 15px;
    border-radius: 3px;
}
.right_wap_memu.btn-nav span.icon-bar {
    display: block;
    margin: 6px 0;
    width: 30px;
    height: 3px;
    background-color: #194278;
    -webkit-transition: all .7s ease;
    -moz-transition: all .7s ease;
    -ms-transition: all .7s ease;
    -o-transition: all .7s ease;
    transition: all .7s ease;
    z-index: 999999;
}
div#mobile-menu .mm-panels {
    border-left: 2px solid #ddd;
}
div#mobile-menu {
    background: #fff;
}
div#mobile-menu .mm-listview>li>a, div#mobile-menu .mm-listview>li>span {
    padding: 15px 10px 15px 0px;
}
div.adg_banner-img {
    padding: 260px 245px;
}
div.banner-animation-circle2 .circle-6 {
    width: 250px;
    height: 250px;
    bottom: 0;
    margin-left: -0.5%;
    right: 12%;
}
div.adg_banner-content p {
    font-size: 16px;
    max-width: 425px;
}
section.adg_banner {
    padding-top: 95px;
    padding-bottom: 160px;
}
div.adg_banner-content h1 {
    font-size: 42px;
}
div.adg_banner-action-btn {
    margin-top: 30px;
}
section.adg_our-partner {
    padding-top: 45px;
    padding-bottom: 45px;
}
div.adg_section-head {
    margin-bottom: 35px;
}
section.adg_our-features {
    padding-top: 45px;
}
div.adg_about-usContent {
    padding-left: 0;
}
div.animation-circle3 .animation-circle31 {
    width: 210px;
    height: 210px;
}
div.adg_our-stories-slider:before {
    width: 245px;
    height: 245px;
    border-width: 25px;
    top: auto;
    bottom: auto;
}
div.adg_our-stories-slider .swiper-container:before {
    width: 245px;
    height: 245px;
    border-width: 25px;
}
div.adg_footer-top {
    padding-top: 85px;
    padding-bottom: 55px;
}
div.adg_footer-top:after {
    width: 175px;
    height: 175px;
    right: -110px;
    bottom: 10px;
}
section.adg_our-news {
    padding-top: 75px;
    padding-bottom: 55px;
}
section.adg_download {
    padding-top: 95px;
    padding-bottom: 95px;
}
.hwh_banner-content h1 br {
    display: none;
}
div.hwh_banner-content h1 {
    line-height: normal;
    margin-bottom: 15px;
    font-size: 60px;
}
div.hwh_banner-content {
    text-align: center;
    margin-bottom: 95px;
}
section.hwh_banner {
    padding-top: 60px;
}
div.hwh-animation-circle {
    display: none;
}
div.hwh_our-team-img img {
    width: 180px;
    height: 180px;
}
section.smshub_banner {
    padding-top: 150px;
    padding-bottom: 0;
    text-align: center;
}
div.smshub_banner-img:before {
    display: none;
}
div.smshub_banner-img {
    padding: 0;
    position: relative;
    border-radius: 950px;
    width: 885px;
    height: 885px;
    display: inline-block;
    vertical-align: middle;
    border: 40px solid #fff;
}
.smshub_header-inner {
    justify-content: space-between;
}
.smshub_banner-content {
    margin-bottom: 65px;
}
.smshub_banner-img:after {
    content: "";
    border: 2px dashed #00a9fa;
    position: absolute;
    left: -40px;
    right: -40px;
    top: -40px;
    bottom: -40px;
    border-radius: 100%;
}




}


@media screen and (max-width: 991px) {

div.adg_banner-img {
    padding: 250px 195px;
}
div.adg_banner-content h1 {
    font-size: 32px;
}
div.adg_banner-content p {
    font-size: 15px;
    max-width: 315px;
}
div.banner-animation-circle2 .circle-6 {
    width: 195px;
    height: 195px;
}
section.adg_banner {
    padding-top: 95px;
    padding-bottom: 107px;
}
div.banner-animation-circle2 .circle-5 {
    margin-left: 43.3%;
}
.adg_section-head p br {
    display: none;
}
div.adg_about-usContent h2:before {
    left: 0;
    bottom: 0;
    right: 0;
    margin: 0 auto;
}
.adg_about-usContent {
    margin-bottom: 145px;
}
section.adg_about-us {
    padding-top: 85px;
    padding-bottom: 135px;
}
div.animation-circle3 .animation-circle31 {
    width: 135px;
    height: 135px;
    right: -7%;
}
div.hwh_banner-imgInner img {
    width: auto;
    height: auto;
    max-width: 100%;
}
div.hwh_banner-imgInner {
    border: 2px dashed #fff;
    border-radius: 100%;
    padding: 30px;
    margin-bottom: -120px;
}
.hwh_banner-imgInner:before {
    display: none;
}
div.hwh_banner-content {
    margin-bottom: 55px;
}
.hwh_what-you-get .adg_about-usContent {
    margin-bottom: 55px;
}
.hwh_china-complex .adg_about-usImg {
    margin-bottom: 55px;
}
.hwh_china-complex .adg_about-usContent h2:before {
    margin-left: 0;
}
.hwh_china-complex .adg_about-usContent {
    margin: 0;
}
section.hwh_china-complex {
    padding-top: 55px;
    padding-bottom: 45px;
}
div.hwh_our-team_col {
    flex: 0 0 100%;
    max-width: 100%;
    margin-bottom: 45px;
}
.hwh_our-team .adg_download-action-btn {
    margin-top: 0;
}
div.smshub_banner-img {
    width: 675px;
    height: 675px;
    border: 20px solid #fff;
}
div.smshub_banner-img:after {
    left: -20px;
    right: -20px;
    top: -20px;
    bottom: -20px;
}
section.smshub_ready-drive {
    padding-top: 65px;
    padding-bottom: 65px;
    text-align: center;
}
.smshub_ready-drive-actionBtn {
    text-align: center !important;
    margin-top: 20px;
}
section.smshub-contentBox {
    padding-top: 85px;
    padding-bottom: 45px;
}


}


@media screen and (max-width: 767px) {


div.adg_banner-content {
    text-align: center;
    position: relative;
    z-index: 2;
}
div.adg_banner-content p {
    max-width: 100%;
}
header.adg_header {
    padding: 20px 0;
    box-shadow: 0 2px 3px rgba(0,0,0,.07);
}
section.adg_banner {
    overflow: hidden;
    padding: 0;
}
section.adg_banner .container {
    max-width: 100%;
    padding: 0px;
}
div.adg_banner-content {
    text-align: center;
    position: relative;
    z-index: 2;
    padding-top: 65px;
    padding-bottom: 65px;
}
div.adg_banner-img {
    padding: 250px 195px;
    margin-top: 0;
    position: relative;
}
div.adg_banner-img:before {
    background-repeat: no-repeat;
    background-size: cover;
    background-position: center bottom;
}
div.adg_our-stories-slider:before {
    width: 195px;
    height: 195px;
    border-width: 18px;
    top: auto;
    bottom: auto;
}
div.adg_our-stories-slider .swiper-container:before {
    width: 195px;
    height: 195px;
    border-width: 18px;
}
section.adg_our-stories {
    padding-top: 145px;
    padding-bottom: 145px;
}
div.right_wap_memu.btn-nav {
    height: 40px;
    padding: 0 13px;
}
div.right_wap_memu.btn-nav span.icon-bar {
    margin: 5px 0;
    width: 21px;
    height: 2px;
}
a#adg_contact-us {
    padding: 8px 25px;
    font-size: 15px;
}
.adg_brand-logo img {
    height: 40px;
}
div.body-container-wrapper {
    padding-top: 80px;
}
div.hwh_banner-content h1 {
    font-size: 42px;
}
div.hwh_banner-content p {
    font-size: 17px;
}
div.smshub_banner-img {
    width: 515px;
    height: 515px;
}
#smshub_menu {
    display: none;
}
div#smshub_mmenu {
    display: block;
    border: 2px solid #000;
    padding: 7px;
    width: 48px;
}
ul#smshub_menu {
    position: absolute;
    left: 0;
    top: 100%;
    right: 0;
    background: #e0f5ff;
    margin: 15px 0 0;
    padding: 22px 25px;
}
.smshub_header-inner {
    position: relative;
}
ul#smshub_menu li {
    margin: 0 0 20px;
}
ul#smshub_menu li:last-child {
    margin-bottom: 0;
}
form#payment-form {
    text-align: center;
}




}


@media screen and (max-width: 639px) {

div.adg_about-usImgBox:after {
    width: 290px;
    height: 290px;
    bottom: -29.9%;
    left: -30%;
    border: 40px solid rgb(255 255 255 / 40%);
}
.adg_about-usVideo img {
    width: 110px;
    height: 110px;
}
.animation-circle3 {
    display: none;
}
section.adg_about-us .container .row > div[class*="col-xl-"]:last-child {
    order: -1;
    text-align: center;
}
div.adg_our-stories-slider .swiper-container:before, .adg_our-stories-slider .swiper-container:after, div.adg_our-stories-slider:before, .adg_our-stories-slider:after {
    display: none;
}
.banner-animation-circle2 {
    display: none;
}
div.hwh_banner-content h1 {
    font-size: 36px;
}
.adg_scroll-down img {
    width: 45px;
}
div.hwh_banner-imgInner {
    margin-bottom: -35px;
}
div.smshub_banner-img {
    width: 420px;
    height: 420px;
}
section.smshub_ready-drive {
    padding-top: 45px;
    padding-bottom: 45px;
}


}


@media screen and (max-width: 479px) {

div.adg_about-usImgBox:after {
    width: 200px;
    height: 200px;
    bottom: -26.9%;
    left: -27%;
    border: 25px solid rgb(255 255 255 / 40%);
}
div.adg_about-usImg:before {
    width: 105px;
    height: 105px;
    left: -30px;
    top: -56px;
}
div.adg_about-usVideo img {
    width: 85px;
    height: 85px;
}
div.adg_about-usImg:after {
    width: 225px;
    height: 208px;
    left: -152px;
    bottom: -82px;
}
section.adg_about-us {
    padding-top: 45px;
    padding-bottom: 85px;
}
section.adg_our-stories {
    padding-top: 95px;
    padding-bottom: 80px;
}
section.adg_our-stories:after {
    bottom: -1px;
}
div.adg_our-stories-Quotes p {
    font-size: 15px;
    line-height: 1.6;
}
div.adg_our-stories-designationName h4 {
    font-size: 16px;
}
div.adg_our-stories-img img {
    width: 115px;
    height: 115px;
}
div.adg_our-stories-img:before {
    height: 180px;
}
.hwh_our-team_left {
    flex: 0 0 100%;
    max-width: 100%;
    margin-bottom: 30px;
}
.hwh_our-team_right {
    flex: 0 0 100%;
    max-width: 100%;
}
.hwh_our-team_colInner {
    flex-wrap: wrap;
}
div.hwh_our-team-heading {
    margin-bottom: 52px;
}
div.hwh_banner-content h1 {
    font-size: 30px;
}
div.hwh_banner-content p {
    font-size: 15px;
}
div.animation-circleCC1.circle {
    right: -52%;
    top: -15%;
    width: 295px;
    height: 295px;
}
section.hwh_what-you-get {
    padding-top: 35px;
    padding-bottom: 70px;
}
div.adg_section-head {
    margin-bottom: 15px;
}
section.hwh_what-you-get ul li {
    margin-bottom: 14px;
    font-size: 15px;
    background-position: 0 2px;
}
div.smshub_banner-img {
    width: 325px;
    height: 325px;
}
/*div.smshub_banner-content p {
    font-size: 18px;
}*/
.smshub_banner-actionBtn a:last-child, .smshub_ready-drive-actionBtn a:last-child {
    margin: 0 !important;
}
section.smshub-contentBox {
    padding-top: 60px;
    padding-bottom: 0;
}
.smshub-contentBox-head p br {
    display: none;
}

}


@media screen and (max-width: 359px) {

div.smshub_banner-img {
    width: 290px;
    height: 290px;
    border: 12px solid #fff;
}
div.smshub_banner-img:after {
    left: -12px;
    right: -12px;
    top: -12px;
    bottom: -12px;
}


}

</style>
    
@endsection
@section('content')
@section('head')
@parent    
@show
<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor bg-white pt-0" id="kt_content">

        <!-- begin:: Content -->
<!-- banner html start here -->
    
   

    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
       
          
             
    
    <section class="smshub_banner">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-xl-5 col-lg-5">
            <div class="smshub_banner-content">
              <h1 class="fa-3x">SMS Texting to enrich your online business</h1>
              <p class="fa-1x">Fast, simple, powerful business text messaging. Connecting your Twilio and HubSpot for sales, marketing, and support.</p>
              <div class="smshub_banner-actionBtn">
<!--                <a class="btn" href="https://app.hubspot.com/oauth/authorize?client_id=0f804048-64f7-4a3a-b7c6-dc08163f36a4&redirect_uri=https://phpstack-242348-1570241.cloudwaysapps.com/hubspot/smsdew/auth2-callback&scope=contacts%20timeline">Get Started</a>-->
                <!-- <form role="form" action="https://apps.webdew.com/hubspot/smsdew/stripe-payment" method="post"  data-cc-on-file="false"
                        data-stripe-publishable-key="pk_live_51HIGy1Et32QR8DEiqHLUJkoooDvREEx2pCmxY2N6D4IoBooH07VI7npE1tfrSObZjOXpG2trG3dmV8hrOIgFJHv600L2ZZRNuK"  id="payment-form">
                        <input type="hidden" name="_token" value="huQsL9rQuqXvAZNv7IXb13WKT7YJDMOBi0xRukKz">                                                    <input id="amount" value="" name="amount" type="hidden">
                        <input type="hidden" id="tot_price" value="19" />
                        <input type="hidden" name="stripe_promo_id" id="stripe_promo_id" value="0" />
                        <input type="hidden" id="stripe_wallet" name="wallet" value="19">


                        <noscript>You must <a href="http://www.enable-javascript.com" target="_blank">enable JavaScript</a> in your web browser in order to pay via Stripe.</noscript>
                            <input 
                                type="submit"
                                class="btn btn-brand btn-bold kt-mr-10" 
                                value="Pay Now"
                                data-key=pk_live_51HIGy1Et32QR8DEiqHLUJkoooDvREEx2pCmxY2N6D4IoBooH07VI7npE1tfrSObZjOXpG2trG3dmV8hrOIgFJHv600L2ZZRNuK
                                data-amount="1900"
                                data-currency="usd"
                                data-name="stripe Payment"
                                data-email=""
                                id="stripe"
                                
                            />
                             
                            
                            <script src="https://checkout.stripe.com/v2/checkout.js"></script>
                            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
                            
                            <script>
                            $(document).ready(function() {
                                $(document).on('click','#stripe' ,function(event) {
                                    event.preventDefault();


                                    var $button = $(this),
                                        $form = $button.parents('form');


                                    var opts = $.extend({}, $button.data(), {
                                        token: function(result) {
                                        $form.append($('<input>').attr({ type: 'hidden', name: 'stripeToken', value: result.id })).submit();
                                        }
                                    });


                                    StripeCheckout.open(opts);
                                });
                            });
                            </script>
                            
                             <a class="btn btn-label-brand btn-bold" href="{{url('signin')}}">Login</a>
                    </form> -->
                    @php
                    $amount = \DB::table('apps')->where('id',1)->value('amount');
                    @endphp

                    @if(Auth::check())
                        <?php
                        
                        $id = \DB::table('payments')->where('user_id',Auth::user()->id)->where('app_id',1)->value('id');
                        
                        ?>
                        @if($id < 1)
                          <form role="form" action="{{url('/stripe-payment')}}" method="post"  data-cc-on-file="false" data-stripe-publishable-key="{{env('STRIPE_KEY')}}" id="payment-form">
                            @csrf
                            <input type="hidden" name="email" value="{{Auth::user()->email}}">
                            <input type="hidden" name="type" value="smsdew">
                            <input type="hidden" name="app_id" value="1">
                            <noscript>You must <a href="http://www.enable-javascript.com" target="_blank">enable JavaScript</a> in your web browser in order to pay via Stripe.</noscript>
                                <?php
                                    $s_a = $amount * 100
                                ?>
                                <input 
                                    type="submit"
                                    class="btn btn-brand btn-bold kt-mr-10" 
                                    value="Pay Now"
                                    data-key="{{env('STRIPE_KEY')}}"
                                    data-amount="{{$s_a}}"
                                    data-currency="usd"
                                    data-name="stripe Payment"
                                    data-email="{{Auth::user()->email}}"
                                    id="stripe"
                                />
                                <script src="https://checkout.stripe.com/v2/checkout.js"></script>
                                <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
                                <script>
                                    $(document).ready(function() {
                                        $(document).on('click','#stripe' ,function(event) {
                                            event.preventDefault();


                                            var $button = $(this),
                                                $form = $button.parents('form');


                                            var opts = $.extend({}, $button.data(), {
                                                token: function(result) {
                                                $form.append($('<input>').attr({ type: 'hidden', name: 'stripeToken', value: result.id })).submit();
                                                }
                                            });


                                            StripeCheckout.open(opts);
                                        });
                                    });
                                </script>
                          </form>
                        @else
                            <a href="{{url('/smsdew/auth2-callback')}}" class="btn btn-brand btn-bold kt-mr-10">Install App</a>
                        @endif
                    @else
                        <a class="btn btn-label-brand btn-bold" href="{{url('signin')}}">Login</a>
                    @endif
              </div>
            </div>
          </div>
          <div class="col-xl-7 col-lg-7">
            <div class="smshub_banner-img">
              
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- banner html end here -->


<section class="smshub-contentBox">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="smshub-contentBox-head text-center">
              <h5>TWILIO CONNECT FROM WITHIN YOUR HUBSPOT</h5>
              <h2>A better way to communicate with your contacts</h2>
              <p>From right within your HubSpot you can send and receive text messages on the go. Communicate <br> with your customers faster with two-way texting.</p>
            </div>
          </div>
        </div>
        <div class="row smshub-contentBox-row">
          <div class="col-xl-4 col-lg-6">
            <div class="card card-custom smshub-contentBox-col kt-portlet kt-iconbox kt-icon-size kt-iconbox--primary kt-iconbox--animate-slow">
              <div class="smshub-contentBox-colInner text-center">
                <div class="smshub-contentBox-icon">
                  <div class="smshub-contentBox-iconInner">
                    <img src="/hubspot/public/assets/media/misc/icon1.svg" alt="icon1">
                  </div>
                </div>
                <div class="smshub-contentBox-content">
                  <h3>Hubspot Workflows</h3>
                  <p>Send and receive messages from any phone number in the US or Canada. Even if the person using the phone isn't!</p>
                </div>
              </div>
            </div>
          </div>
          <div class="col-xl-4 col-lg-6">
            <div class="card card-custom smshub-contentBox-col kt-portlet kt-iconbox kt-icon-size kt-iconbox--primary kt-iconbox--animate-slow">
              <div class="smshub-contentBox-colInner text-center">
                <div class="smshub-contentBox-icon">
                  <div class="smshub-contentBox-iconInner">
                    <img src="/hubspot/public/assets/media/misc/icon1.svg" alt="icon1">
                  </div>
                </div>
                <div class="smshub-contentBox-content">
                  <h3>Instant Delivery</h3>
                  <p>Send and receive messages from any phone number in the US or Canada. Even if the person using the phone isn't!</p>
                </div>
              </div>
            </div>
          </div>
          <div class="col-xl-4 col-lg-6">
            <div class="card card-custom smshub-contentBox-col kt-portlet kt-iconbox kt-icon-size kt-iconbox--primary kt-iconbox--animate-slow">
              <div class="smshub-contentBox-colInner text-center">
                <div class="smshub-contentBox-icon">
                  <div class="smshub-contentBox-iconInner">
                    <img src="/hubspot/public/assets/media/misc/icon1.svg" alt="icon1">
                  </div>
                </div>
                <div class="smshub-contentBox-content">
                  <h3>Twilio Connect</h3>
                  <p>Send and receive messages from any phone number in the US or Canada. Even if the person using the phone isn't!</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>


    <section class="smshub_ready-drive">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-xl-6 col-lg-6 col-md-6">
            <div class="smshub_ready-drive-text">
              <h2>Ready to dive in? <br> <span class="primary-color">Start for ${{$amount}} today!</span></h2>
            </div>
          </div>
          <div class="col-xl-6 col-lg-6 col-md-6">
            <div class="smshub_ready-drive-actionBtn text-right">

                    @if(Auth::check())
                        <?php
                        
                        $id = \DB::table('payments')->where('user_id',Auth::user()->id)->where('app_id',1)->value('id');
                        ?>
                        @if($id < 1)
                          <form role="form" action="{{url('/stripe-payment')}}" method="post"  data-cc-on-file="false" data-stripe-publishable-key="{{env('STRIPE_KEY')}}" id="payment-form">
                            @csrf
                            <input type="hidden" name="email" value="{{Auth::user()->email}}">
                            <input type="hidden" name="type" value="smsdew">
                            <input type="hidden" name="app_id" value="1">
                            <noscript>You must <a href="http://www.enable-javascript.com" target="_blank">enable JavaScript</a> in your web browser in order to pay via Stripe.</noscript>
                                <input 
                                    type="submit"
                                    class="btn btn-brand btn-bold kt-mr-10" 
                                    value="Pay Now"
                                    data-key="{{env('STRIPE_KEY')}}"
                                    data-amount="{{$s_a}}"
                                    data-currency="usd"
                                    data-name="stripe Payment"
                                    data-email="{{Auth::user()->email}}"
                                    id="stripe"
                                />
                                <script src="https://checkout.stripe.com/v2/checkout.js"></script>
                                <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
                                <script>
                                    $(document).ready(function() {
                                        $(document).on('click','#stripe' ,function(event) {
                                            event.preventDefault();


                                            var $button = $(this),
                                                $form = $button.parents('form');


                                            var opts = $.extend({}, $button.data(), {
                                                token: function(result) {
                                                $form.append($('<input>').attr({ type: 'hidden', name: 'stripeToken', value: result.id })).submit();
                                                }
                                            });


                                            StripeCheckout.open(opts);
                                        });
                                    });
                                </script>
                          </form>
                        @else
                            <a href="{{url('/smsdew/auth2-callback')}}" class="btn btn-brand btn-bold kt-mr-10">Install App</a>
                        @endif
                    @else
                        <a class="btn btn-label-brand btn-bold" href="{{url('signin')}}">Login</a>
                    @endif

            </div>
          </div>
        </div>
      </div>
    </section>


    </div>                  
</div>

@section('scripts')
@parent
@show
@endsection