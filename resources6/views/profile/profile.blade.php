@extends('layout.app')

@php
if(Auth::check()):
    
        $switched = @Auth::user()->switched;
        $switched_to = 'Publisher';
    
else:
    
        $switched = 'publisher';
        $switched_to = 'Publisher';
    
endif;


$metatitle = "Profile section for " . $switched_to . " - FLY.BIZ";
$metadescription = "Under profile section, an " . $switched . " will be able to view his/her details which can be further used to contact him/her.";
@endphp

@section('title',$metatitle)

@section('title-description')

<meta name="description" content="{{$metadescription}}">
    
@endsection

@section('mainhead')
    @parent
@endsection

@section('content')
<link href="{{ asset('') . config('app.public_url') . '/assets/css/pages/wizard/wizard-2.css' }}" rel="stylesheet" type="text/css" />
<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
        <!-- begin:: Subheader -->
        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
            </div>
        </div>
        <!-- end:: Subheader -->
        <!-- begin:: Content -->
        <div class="kt-container  kt-grid__item kt-grid__item--fluid">
            <div class="kt-portlet">
                <div class="kt-portlet__body kt-portlet__body--fit">
                    <div class="kt-grid  kt-wizard-v2 kt-wizard-v2--white" id="kt_wizard_v2" data-ktwizard-state="step-first">
                        <div class="kt-grid__item kt-wizard-v2__aside">
                            <!--begin: Form Wizard Nav -->
                            <div class="kt-wizard-v2__nav">
                                <!--doc: Remove "kt-wizard-v2__nav-items--clickable" class and also set 'clickableSteps: false' in the JS init to disable manually clicking step titles -->
                                <div class="kt-widget kt-widget--user-profile-1 w-100">
                                    <div class="kt-widget__head">
                                        <div class="kt-widget__media">
                                            @php
                                            $image = Auth::user()->image;
                                            @endphp
                                            @if(!empty($image))
                                            @php
                                                $image = url('storage/app/') . '/' . $image;
                                            @endphp
                                            @else
                                            @php
                                                $image = asset('') . config('app.public_url') .'/assets/media/users/300_21.jpg';
                                            @endphp
                                            @endif
                                            <img src="{{$image}}" alt="image">
                                        </div>
                                        <div class="kt-widget__content">
                                            <div class="kt-widget__section">
                                                <a href="#" class="kt-widget__username">
                                                    {{ Auth::user()->user_fname }}{{ Auth::user()->user_lname }}
                                                </a>
                                            </div>   
                                        </div>
                                    </div>
                                    <div class="kt-widget__body">
                                        <div class="kt-widget__content">
                                            <div class="kt-widget__info">
                                                <span class="kt-widget__label">Email:</span>
                                                <a href="#" class="kt-widget__data">{{ Auth::user()->email }}</a>
                                            </div>
                                            <div class="kt-widget__info">
                                                <span class="kt-widget__label">Phone:</span>
                                                <a href="#" class="kt-widget__data">{{ Auth::user()->user_mob_no }}</a>
                                            </div>
                                            <div class="kt-widget__info">
                                                <span class="kt-widget__label">Reviews:</span>
                                                <span class="kt-widget__data">
                                                    <div class="d-flex">         
                                                        <div class="kt-demo-icon__preview">
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="kt-demo-icon__preview">
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="kt-demo-icon__preview">
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="kt-demo-icon__preview">
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="kt-demo-icon__preview">
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                    </div>
                                                </span>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                                <div class="kt-wizard-v2__nav-items kt-wizard-v2__nav-items--clickable">
                                    <div class="kt-wizard-v2__nav-item" data-ktwizard-type="step" data-ktwizard-state="current">
                                        <div class="kt-wizard-v2__nav-body">
                                            <div class="kt-wizard-v2__nav-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <polygon points="0 0 24 0 24 24 0 24"></polygon>
                                                        <path d="M12.9336061,16.072447 L19.36,10.9564761 L19.5181585,10.8312381 C20.1676248,10.3169571 20.2772143,9.3735535 19.7629333,8.72408713 C19.6917232,8.63415859 19.6104327,8.55269514 19.5206557,8.48129411 L12.9336854,3.24257445 C12.3871201,2.80788259 11.6128799,2.80788259 11.0663146,3.24257445 L4.47482784,8.48488609 C3.82645598,9.00054628 3.71887192,9.94418071 4.23453211,10.5925526 C4.30500305,10.6811601 4.38527899,10.7615046 4.47382636,10.8320511 L4.63,10.9564761 L11.0659024,16.0730648 C11.6126744,16.5077525 12.3871218,16.5074963 12.9336061,16.072447 Z" fill="#000000" fill-rule="nonzero"></path>
                                                        <path d="M11.0563554,18.6706981 L5.33593024,14.122919 C4.94553994,13.8125559 4.37746707,13.8774308 4.06710397,14.2678211 C4.06471678,14.2708238 4.06234874,14.2738418 4.06,14.2768747 L4.06,14.2768747 C3.75257288,14.6738539 3.82516916,15.244888 4.22214834,15.5523151 C4.22358765,15.5534297 4.2250303,15.55454 4.22647627,15.555646 L11.0872776,20.8031356 C11.6250734,21.2144692 12.371757,21.2145375 12.909628,20.8033023 L19.7677785,15.559828 C20.1693192,15.2528257 20.2459576,14.6784381 19.9389553,14.2768974 C19.9376429,14.2751809 19.9363245,14.2734691 19.935,14.2717619 L19.935,14.2717619 C19.6266937,13.8743807 19.0546209,13.8021712 18.6572397,14.1104775 C18.654352,14.112718 18.6514778,14.1149757 18.6486172,14.1172508 L12.9235044,18.6705218 C12.377022,19.1051477 11.6029199,19.1052208 11.0563554,18.6706981 Z" fill="#000000" opacity="0.3"></path>
                                                    </g>
                                                </svg>
                                            </div>
                                            <div class="kt-wizard-v2__nav-label">
                                                <div class="kt-wizard-v2__nav-label-title">
                                                    Profile
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--end: Form Wizard Nav -->
                        </div>
                        <div class="kt-grid__item kt-grid__item--fluid kt-wizard-v2__wrapper">
                            <!--begin: Form Wizard Step 1-->
                            <div class="kt-wizard-v2__content" data-ktwizard-type="step-content" data-ktwizard-state="current">
                                <div class="kt-form__section kt-form__section--first">
                                    <!--begin: Form Wizard Form-->
                                    <form class="kt-form" method="POST" action="{{ url('/update_singleProfile') }}">
                                        @csrf
                                        <div class="kt-wizard-v2__form">
                                            <div class="form-group row">
                                                <div class="col-md-6 col-12">
                                                    <div class="kt-avatar kt-avatar--outline kt-avatar--circle-" id="kt_user_edit_avatar">
                                                        
                                                        <!-- <label class="kt-avatar__upload" data-toggle="kt-tooltip" title="" data-original-title="Change avatar">
                                                            <i class="fa fa-pen"></i>
                                                            <input type="file" name="profile_avatar" accept=".png, .jpg, .jpeg" />
                                                        </label>
                                                        <span class="kt-avatar__cancel" data-toggle="kt-tooltip" title="" data-original-title="Cancel avatar">
                                                            <i class="fa fa-times"></i>
                                                        </span> -->



                                                         @php
                                                            $image = @$user_data->image;
                                                            $name  = '';
                                                            $lname = '';
                                                        @endphp
                                                        @if(!empty($image))
                                                            @php
                                                                $image = url('storage/app/') . '/' . $image;
                                                            @endphp
                                                        @else
                                                            @php
                                                                $name  = strtoupper($rest = substr(@$user_data->user_fname, 0, 1));

                                                                $lname = strtoupper($rest = substr(@$user_data->user_lname, 0, 1));
                                                            @endphp
                                                        @endif
                                                        @if(!empty($image))
                                                            <div class="kt-avatar__holder" style="background-image: url('{{$image}}');"></div>
                                                        @else
                                                            @php
                                                                $image = asset('') . config('app.public_url') .'/assets/media/users/default.jpg';
                                                            @endphp
                                                            @if(!empty($name))
                                                                <div class="kt-widget4__pic kt-widget__pic--primary kt-font-primary kt-font-boldest kt-font-light">
                                                                <span class="kt-badge kt-badge--username kt-badge--unified-primary kt-badge--lg kt-badge--rounded kt-badge--bold">
                                                                {{$name}}{{$lname}}</span>
                                                                    
                                                                </div>
                                                            @else
                                                                <div class="kt-widget4__pic kt-widget4__pic--pic">
                                                                    <img src="{{$image}}" alt="">           
                                                                </div>

                                                            @endif
                                                        @endif

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-md-6 col-12">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend"><span class="input-group-text"><i class="la la-user"></i></span></div>
                                                        <input type="text" class="form-control" name="first_name" placeholder="First Name" value="{{ @$user_data->user_fname }}"  maxlength="20" disabled="" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-md-6 col-12">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend"><span class="input-group-text"><i class="la la-user"></i></span></div>
                                                        <input type="text" class="form-control" name="last_name" placeholder="Last Name" value="{{ @$user_data->user_lname }}"  maxlength="20" disabled=""  />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-md-6 col-12">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend"><span class="input-group-text"><i class="la la-user"></i></span></div>
                                                        <input type="text" class="form-control" name="user_code" placeholder="User Code" value="{{ @$user_data->user_code }}" disabled="" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-md-6 col-12">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend"><span class="input-group-text"><i class="la la-globe"></i></span></div>
                                                        <input type="text" class="form-control" name="user_email" disabled=""   value="{{ @$user_data->email}}" disabled="disabled" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-md-6 col-12">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend"><span class="input-group-text"><i class="la la-phone"></i></span></div>
                                                        <input type="text" class="form-control" name="contact_number" placeholder="Phone" aria-describedby="basic-addon1" value="{{ @$user_data->user_mob_no }}"  maxlength="10" disabled=""  />
                                                    </div>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <!--end: Form Wizard Step 1-->
                            <!--begin: Form Wizard Step 2-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end:: Content -->
    </div>
</div>
<script src="{{ asset('') . config('app.public_url') . '/assets/js/pages/custom/wizard/wizard-2.js' }}" type="text/javascript"></script>
@section('mainscripts')
@parent
@show
@endsection