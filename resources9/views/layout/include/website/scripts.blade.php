    <!-- begin::Global Config(global config for global JS sciprts) -->

    <script>
        var KTAppOptions = {
            "colors": {
                "state": {
                    "brand": "#009bff",
                    "light": "#d5efff",
                    "dark": "#282a3c",
                    "primary": "#009bff",
                    "success": "#34bfa3",
                    "info": "#36a3f7",
                    "warning": "#ffb822",
                    "danger": "#fd3995"
                },
                "base": {
                    "label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
                    "shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
                }
            }
        };
    </script>

    <!-- end::Global Config -->

    <!--begin::Global Theme Bundle(used by all pages) -->   

    <script src="{{ asset('') . config('app.public_url') . '/assets/plugins/global/plugins.bundle.js' }}" type="text/javascript"></script>
    <script src="{{ asset('') . config('app.public_url') . '/assets/js/scripts.bundle.js' }}" type="text/javascript"></script>
    <script src="{{ asset('') . config('app.public_url') . '/js/script.js'  }}"></script>
    <script src="{{ asset('') . config('app.public_url') . '/js/jquery.min.js'  }}"></script>
    <script src="{{ asset('') . config('app.public_url') . '/js/bootstrap.min.js'  }}"></script>

    <!--end::Global Theme Bundle(used by all pages) -->   
