<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>

    <title>@yield('title')</title>
     
    <!-- begin::Head -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!--begin::Page Vendors Styles(used by this page) -->
    
    @yield('title-description')

    @section('head')
        @include('layout.include.head')
    @show
    <!-- end::Head -->
    

    @yield('customCSS')
    

</head>
    <?php 
        if(Auth::check()){
            $class =  'publisher-wrapper ';
        } else {
            $class = '';
        }
    ?>
    <body class="{{$class}}kt-page--loading-enabled kt-page--loading kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header--minimize-menu kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-aside--enabled kt-aside--left kt-aside--fixed kt-page--loading">
    
    <!-- begin::Page loader -->

        @include('layout.include.partials.loader')

        <!-- end::Page Loader -->

        <!-- begin:: Page -->

        <!-- begin:: Header Mobile -->
        <div id="kt_header_mobile" class="kt-header-mobile  kt-header-mobile--fixed ">
            <div class="kt-header-mobile__logo">
                @include('layout.include.partials.brand')
            </div>
            <div class="kt-header-mobile__toolbar">                
                <button class="kt-header-mobile__toolbar-topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more-1"></i></button>
            </div>
        </div>

        <!-- end:: Header Mobile -->
        <div class="kt-grid kt-grid--hor kt-grid--root">
            <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
                <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

                    @include('layout.include.header')

                    @yield('content')

            <!-- end::Page Loader -->
                    @include('layout.include.footer')
            <!-- end:: Footer -->
                </div>
            </div>
        </div>

    <!-- end:: Page -->
   

        <!-- begin::Scrolltop -->
        <div id="kt_scrolltop" class="kt-scrolltop">
            <i class="fa fa-arrow-up"></i>
        </div>

        <!-- end::Scrolltop -->


        @section('scripts')
            @include('layout.include.scripts')
        @show

        @yield('customScript')

    </body>
    <!-- end::Body -->
</html>