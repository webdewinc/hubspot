@extends('layout.website')
@php
$totalTitle = 'Policy';
@endphp
@section('title', $totalTitle)
@section('title-description')

<meta name="description" content="Settings">
    
@endsection
@section('content')
@section('head')
@parent    
@show

<?php /* ?>
<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor bg-white pb-0" id="kt_content">

        <!-- begin:: Content -->
        <div class="kt-container  kt-grid__item kt-grid__item--fluid home-container">
           
            <div class="row">
                <!-- body content start -->
                <div class="col-12">
                    <div class="kt_blog-post">
                        
                        <div class="row justify-content-center">
                            <div class="col-xl-8 col-lg-8 col-md-12">
                                <div class="row">
                                    <div class="col">
                                        <div class="card">
                                            Policy
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>                                  
                    </div>
                </div>
                <!-- body content end -->
            </div>
        </div>
    </div>                  
</div>
<?php */ ?>
<section class="smshub_banner">
      <div class="container">
        <div class="row align-items-center">
             <p>
                <h2>Privacy Policy</h2>
            </p>
            <p>
                <strong>Terms</strong><br/>
                By accessing this web site, you are agreeing to be bound by these web site Terms and Conditions of Use, applicable laws and regulations and their compliance. If you disagree with any of the stated terms and conditions, you are prohibited from using or accessing this site. The materials contained in this site are secured by relevant copyright and trade mark law.</p>
                <p><strong>Use License</strong><br/>
                <p>1. Permission is allowed to temporarily download one duplicate of the materials (data or programming) on SMS DEW's site for individual and non-business use only. This is the just a permit of license and not an exchange of title, and under this permit you may not:<br/>
                    1. Modify or copy the materials;<br/>
                    2. Use the materials for any commercial use , or for any public presentation (business or non-business);<br/>
                    3. Attempt to decompile or rebuild any product or material contained on SMS DEW's site;<br/>
                    4. Remove any copyright or other restrictive documentations from the materials; or<br/>
                    5. Transfer the materials to someone else or even "mirror" the materials on another server.<br/>
                
                2. This permit might consequently be terminated if you disregard any of these confinements and may be ended by SMS DEW whenever deemed. After permit termination or when your viewing permit is terminated, you must destroy any downloaded materials in your ownership whether in electronic or printed form.</p>
            <p>
            <p>
                <strong>Disclaimer</strong><br/>
                1. The materials on SMS DEW's site are given "as is". SMS DEW makes no guarantees, communicated or suggested, and thus renounces and nullifies every single other warranties, including without impediment, inferred guarantees or states of merchantability, fitness for a specific reason, or non-encroachment of licensed property or other infringement of rights. Further, SMS DEW does not warrant or make any representations concerning the precision, likely results, or unwavering quality of the utilization of the materials on its Internet site or generally identifying with such materials or on any destinations connected to this website.
            </p>
            <p>
                <strong>Constraints</strong><br/>
                In no occasion should SMS DEW or its suppliers subject for any harms (counting, without constraint, harms for loss of information or benefit, or because of business interference,) emerging out of the utilization or powerlessness to utilize the materials on SMS DEW's Internet webpage, regardless of the possibility that SMS DEW or a SMS DEW approved agent has been told orally or in written of the likelihood of such harm. Since a few purviews don't permit constraints on inferred guarantees, or impediments of obligation for weighty or coincidental harms, these confinements may not make a difference to you.

            </p>
            <p>
                <strong>Amendments and Errata</strong><br/>
                The materials showing up on SMS DEW's site could incorporate typographical, or photographic mistakes. SMS DEW does not warrant that any of the materials on its site are exact, finished, or current. SMS DEW may roll out improvements to the materials contained on its site whenever without notification. SMS DEW does not, then again, make any dedication to update the materials.
            </p>
            <p>
                <strong>Links</strong><br/>
                SMS DEW has not checked on the majority of the websites or links connected to its website and is not in charge of the substance of any such connected webpage. The incorporation of any connection does not infer support by SMS DEW of the site. Utilization of any such connected site is at the user's own risk.
                Site Terms of Use Modifications
                SMS DEW may update these terms of utilization for its website whenever without notification. By utilizing this site you are consenting to be bound by the then current form of these Terms and Conditions of Use.
            </p>
            <p>
                <strong>Governing Law</strong><br/>
                Any case identifying with SMS DEW's site should be administered by the laws of the country of India SMS DEW State without respect to its contention of law provisions.
            </p>
            <p>
                <strong>General Terms and Conditions applicable to Use of a Web Site.</strong>
                <strong>Privacy Policy</strong>
            </p>
            <p>
                Your privacy is critical to us. Likewise, we have built up this Policy with the end goal you should see how we gather, utilize, impart and reveal and make utilization of individual data. The following blueprints our privacy policy.
            </p>
            <p>
                1) Before or at the time of collecting personal information, we will identify the purposes for which information is being collected.<br/>
                2) We will gather and utilization of individual data singularly with the target of satisfying those reasons indicated by us and for other good purposes, unless we get the  assent of the individual concerned or as required by law.<br/>
                3) We will just hold individual data the length of essential for the satisfaction of those reasons.<br/>
                4) We will gather individual data by legal and reasonable means and, where fitting, with the information or assent of the individual concerned.<br/>
                5) Personal information ought to be important to the reasons for which it is to be utilized, and, to the degree essential for those reasons, ought to be exact, finished, and updated.<br/>
                6) We will protect individual data by security shields against misfortune or burglary, and also unapproved access, divulgence, duplicating, use or alteration.<br/>
                7) We will promptly provide customers with access to our policies and procedures for the administration of individual data.<br/>
            </p>
            <p>We are focused on leading our business as per these standards with a specific end goal to guarantee that the privacy of individual data is secure and maintained.</p>
        </div>
    </div>
</section>

@section('scripts')
@parent

@show
@endsection