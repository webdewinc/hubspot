<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class AuthToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
        if( empty(Auth::user()->auth_token) ){
            return redirect()->to('auth');
        }
        return $next($request);
        //return redirect()->back();
    }
}
