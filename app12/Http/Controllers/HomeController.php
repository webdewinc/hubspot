<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Support\Facades\Validator;
use Auth;
use Crypt;
use Redirect;
use Hash;
use DB;
use App\User;
use Session;
use Twilio\Rest\Client;
use App\Http\Controllers\Ecommerce\EcommerceController;

class HomeController extends EcommerceController
{

    public function __construct()
    {
        //$this->middleware('auth');
    }
    public function web()
    {
        abort(401);
        //$user = User::where('email','tim@nimbushosting.co.uk')->first();
        //$user = User::where('id',61)->first();        
        //Auth::login($user);
        //Session::put('portal_id', 7392775);

        // client
        // $user = User::where('id',48)->first();        
        // Auth::login($user);
        //Session::put('portal_id', 5256721);
        // client ryan
        
        return Redirect::back();
    }
    public function index() {   
        return redirect('signin');
    }
    public function home() {
        return view('home');
    }
    public function all_app(){
        return view('all-app');
    }
    
    public function auth2_callback_ecommerce(Request $request) {

        if(env('APP_ENV') == 'live'){
            $provider = new \Flipbox\OAuth2\Client\Provider\HubSpot([
                'clientId'          => config('services.live_ecommerceCartButton.clientId'),
                'clientSecret'      => config('services.live_ecommerceCartButton.clientSecret'),
                'redirectUri'       => url('/ecommerce/auth2-callback')
            ]);
        } else {
            $provider = new \Flipbox\OAuth2\Client\Provider\HubSpot([
                'clientId'          => config('services.stage_ecommerceCartButton.clientId'),
                'clientSecret'      => config('services.stage_ecommerceCartButton.clientSecret'),
                'redirectUri'       => url('/ecommerce/auth2-callback')
            ]);
        }
        
        if ( !isset($_GET['code']) ) {
            
            $options = [
                'scope' => ['contacts','timeline','content','oauth'] // array or string
            ];

            // If we don't have an authorization code then get one
            $authUrl = $provider->getAuthorizationUrl($options);
            header('Location: '.$authUrl);
            exit;
            // Check given state against previously stored one to mitigate CSRF attack

        } 
        // elseif (empty($_GET['state']) || ($_GET['state'] !== Auth::user()->oauth2state)) {

        //     exit('Invalid state');

        // }
        else {

            $result = [];
            // Try to get an access token (using the authorization code grant)
            $token = $provider->getAccessToken('authorization_code', [
                'code' => $_GET['code']
            ]);
            // Optional: Now you have a token you can look up a users profile data
            try {
                // We got an access token, let's now get the user's details
                $user = $provider->getResourceOwner($token);
                
                $result['portal_id'] = $user->getHubId();
                $result['email'] = $user->getEmail();
                $result['app_id'] = @$user->getAppId();
                $result['refresh_token'] = @$token->getRefreshToken();
                $result['expires_at'] = @$token->getExpires();
                $result['user_expires_at'] = @$user->getExpires();
                $result['code'] = $_GET['code'];
                $result['expires_at'] = time() + $result['expires_at'] * 0.95;
                $result['auth_token'] = $token->getToken();
                $token1 = $token->getToken();

                $this->templates_generation($token1);    

                $u = User::whereEmail($result['email'])->first();
                if(empty($u->id)) {
                    $validatedData['user_type']       = 'users';
                    $validatedData['activation_code'] = str_random(30).time();
                    $validatedData['user_email_verified'] = 1;
                    $validatedData['user_fname'] = 'New';
                    $validatedData['user_lname'] = 'User';
                    $validatedData['email'] = $result['email'];
                    $validatedData['user_status'] = 1;
                    $validatedData['vid'] = 0;
                    app(User::class)->create($validatedData);
                    $u = User::whereEmail($result['email'])->first();
                } 
                Auth::login($u);

                $mapped_app_id = DB::table('mapped_apps')->where('user_id',Auth::user()->id)->where('fk_app_id',3)->value('mapped_app_id');
                if(empty($mapped_app_id)){
                    DB::table('mapped_apps')->insert(['fk_app_id'=> 3,'user_id'=> Auth::user()->id]);
                }

                $scopes = @serialize($user->getScopes());
                  // update
                    $users = User::find(Auth::user()->id);
                    $users->portal_id = $result['portal_id'];
                    $users->hub_email = $user->getEmail();
                    $users->scopes =  $scopes;
                    $users->app_id = @$user->getAppId();
                    $users->refresh_token =$result['refresh_token'];
                    $users->expires_at =$result['expires_at'];
                    $users->code = $_GET['code'];
                    $users->auth_token = $token1;
                    $users->save();
                  // update
                
                //$user_id = $u->id;
                $user_id = Auth::user()->id;
                $portal_id = $result['portal_id'];
                $refresh_token = $result['refresh_token'];
                $expiry_time = $result['expires_at'];

                /*===============
                    |implementing payment check
                    |================
                    */
                    $email = $user->getEmail();



                    // $paymentCheck = DB::table('payments')->where('user_id',Auth::user()->id)->where('app_id',3)->value('id');

                    
                    // if ($paymentCheck > 0) { //payment is done

                        /*===============
                        |Select query to check if application was already intalled in the same poratl or not
                        |================
                        */

                        $record_check = DB::table('application_records')->where('app_id',3)->where('portal_id',$portal_id)->value('id');

                        if ($record_check > 0)
                        {
                            DB::table('application_records')->where('app_id',3)->where('portal_id',$portal_id)->update([
                                        'refresh_token' => $refresh_token,
                                        'expiry_time' => $expiry_time,
                                        'email' => $user->getEmail()
                                    ]);
                        } else {

                            /*===============
                            |insert code in new table for storing refresh token for different portals
                            |================
                            */

                            $insert = DB::table('application_records')->insertGetId([
                                        'user_id' => $user_id,
                                        'portal_id' => $portal_id,
                                        'client_id' => '',
                                        'secret_id' => '',
                                        'refresh_token' => $refresh_token,
                                        'expiry_time' => $expiry_time,
                                        'app_id' => 3,
                                        'email' => $user->getEmail()
                                    ]);

                        }

                        // redirect to hubspot
                        // $hubspotUrl = 'https://app.hubspot.com/';
                        // if (!empty($result['portal_id']) && isset($result['portal_id']))
                        // {
                            
                        //     $hubspotUrl = 'https://app.hubspot.com/integrations-settings/'.$result['portal_id'].'/installed';
                        //     //$hubspotUrl = 'https://app.hubspot.com/home?portalID=' . $result['portal_id'];
                        // }
                        // header('Location: ' . $hubspotUrl);
                        // exit;
                        // redirect to hubspot
                        //return redirect('/apps/whmcs/import');


                        // $webdewUrl = 'https://apps.webdew.com/hubspot/';
                        // header('Location: ' . $webdewUrl);
                        // exit;
                
                    $scopes = @serialize($user->getScopes());
                      if(!empty($result['email'])){
                            $user_result = User::where('email',$result['email'])->first();
                            $count = User::where('email',$result['email'])->count();
                            if($count > 0){
                                Auth::login($user_result);
                                // update
                                $users = User::find(Auth::user()->id);
                                $users->portal_id = $result['portal_id'];
                                $users->hub_email = $user->getEmail();
                                $users->scopes =  $scopes;
                                $users->app_id = @$user->getAppId();
                                $users->refresh_token = @$token->getRefreshToken();
                                $users->expires_at = @$token->getExpires();
                                $users->code = $_GET['code'];
                                $users->auth_token = $token->getToken();
                                $users->save();
                                // update

                                Session::put('portal_id', $result['portal_id']);
                                
                                // Auth::logout();
                                // Auth::login($user_result);
                            } else {
                                unset($result['user_expires_at']);
                                User::create($result);
                                $user_result = User::where('email',$result['email'])->first();
                                Auth::login($user_result);
                            }
                      }        
                    //} 
                    //else { 

                    //     $paymentCheck = DB::table('payments')->insertGetId([
                    //                                             'app_id' => 3,
                    //                                             'user_id' => Auth::user()->id,
                    //                                             'stripe_id' => 'manual',
                    //                                             'amount' => 0,
                    //                                             'currency_code' => 'usd',
                    //                                             'status' => 'succeeded',
                    //                                             'app_type' => 'ecommerce',
                    //                                             'subscription_id' => 0
                    //                                         ]);

                    //     /*===============
                    //     |Select query to check if application was already intalled in the same poratl or not
                    //     |================
                    //     */

                    //     $record_check = DB::table('application_records')->where('portal_id',$portal_id)->where('app_id',3)->value('id');

                    //     if ($record_check > 0)
                    //     {
                    //         DB::table('application_records')->where('app_id',3)->where('portal_id',$portal_id)->update([
                    //                     'refresh_token' => $refresh_token,
                    //                     'expiry_time' => $expiry_time,
                    //                     'email' => $user->getEmail()
                    //                 ]);
                    //     } else {

                    //         ===============
                    //         |insert code in new table for storing refresh token for different portals
                    //         |================
                            
                    //         $insert = DB::table('application_records')->insertGetId([
                    //                     'user_id' => $user_id,
                    //                     'portal_id' => $portal_id,
                    //                     'client_id' => '',
                    //                     'secret_id' => '',
                    //                     'refresh_token' => $refresh_token,
                    //                     'expiry_time' => $expiry_time,
                    //                     'app_id' => 3,
                    //                     'email' => $user->getEmail()
                    //                 ]);

                    //     }

                    //     // redirect to hubspot
                    //     // $hubspotUrl = 'https://app.hubspot.com/';
                    //     // if (!empty($result['portal_id']) && isset($result['portal_id']))
                    //     // {
                            
                    //     //     $hubspotUrl = 'https://app.hubspot.com/integrations-settings/'.$result['portal_id'].'/installed';
                    //     //     //$hubspotUrl = 'https://app.hubspot.com/home?portalID=' . $result['portal_id'];
                    //     // }
                    //     // header('Location: ' . $hubspotUrl);
                    //     // exit;
                    //     // redirect to hubspot
                    //     //$webdewUrl = 'https://www.webdew.com/hubspot/apps/ecommerce';
                    //     $webdewUrl = 'https://www.webdew.com/hubspot/apps/whmcs';
                    //     header('Location: ' . $webdewUrl);
                    //     exit;

                    //     //exit;
                    // }

                // redirect to hubspot 
                // $hubspotUrl = 'https://app.hubspot.com/';
                // if(!empty($result['portal_id']) && isset($result['portal_id'])){
                //     $hubspotUrl = 'https://app.hubspot.com/home?portalID='.$result['portal_id'];
                // }
                // header('Location: '.$hubspotUrl);
                // exit;
                // redirect to hubspot 

                // $args['data'] = '{
                //       "properties": [
                //         {
                //           "property": "portal_id",
                //           "value": "'.$result['portal_id'].'"
                //         },
                //         {
                //           "property": "hub_email",
                //           "value": "'.$result['email'].'"
                //         },
                //         {
                //           "property": "refresh_token",
                //           "value": "'.$result['refresh_token'].'"
                //         },
                //         {
                //           "property": "token",
                //           "value": "'.$result['auth_token'].'"
                //         },
                //         {
                //           "property": "code",
                //           "value": "'.$result['code'].'"
                //         },
                //         {
                //           "property": "app_id",
                //           "value": "'.$result['app_id'].'"
                //         }
                //       ]
                //     }';
                // ;
                // $args['url'] = 'https://api.hubapi.com/contacts/v1/contact/email/'.$result['email'].'/profile?hapikey='.env('HUBSPOT_API_KEY');
                // $store = $this->curlAccess( 'POST', $args );                
                // Use these details to create a new profile

            } catch (Exception $e) {
                // Failed to get user details
                exit('Oh dear...');
            }
            // Use this to interact with an API on the users behalf
            return redirect('/');
        }
    }
    public function auth2_callback(Request $request)
    {  
        $provider = new \Flipbox\OAuth2\Client\Provider\HubSpot([
            'clientId'          => config('services.login.clientId'),
            'clientSecret'      => config('services.login.clientSecret'),
            'redirectUri'       => url('auth2-callback')
        ]);

        if ( !isset($_GET['code']) ) {
            
            $options = [
                'scope' => ['contacts','timeline'] // array or string
            ];

            // If we don't have an authorization code then get one
            $authUrl = $provider->getAuthorizationUrl($options);
            
            header('Location: '.$authUrl);
            exit;
            // Check given state against previously stored one to mitigate CSRF attack

        } 
        //elseif (empty($_GET['state'])) {
            //exit('Access code');
        //} 
        else {
            $result = [];
            // Try to get an access token (using the authorization code grant)
            $token = $provider->getAccessToken('authorization_code', [
                'code' => $_GET['code']
            ]);
            // Optional: Now you have a token you can look up a users profile data
            try {

                // We got an access token, let's now get the user's details
                $user = $provider->getResourceOwner($token);
                
                
                $result['portal_id'] = $user->getHubId();
                $result['email'] = $user->getEmail();
                $result['app_id'] = @$user->getAppId();
                $result['refresh_token'] = @$token->getRefreshToken();
                $result['expires_at'] = @$token->getExpires();
                $result['user_expires_at'] = @$user->getExpires();
                $result['code'] = $_GET['code'];
                $result['expires_at'] = time() + $result['expires_at'] * 0.95;
                $result['auth_token'] = $token->getToken();
                $result['user_fname'] = 'User';
                $result['oauth2state'] = @$_GET['state'];
                $result['user_email_verified'] = 1;
                $result['user_status'] = 1;
                $result['sms_credentials_default_type'] = '';

                
                //inserting the user credentials with portal in appcredentials table to authorize the user and give acces to client to use the app
                
                        
//                 $insert = DB::table('application_records')->insertGetId([
//                                                                    'portal_id' => $result['portal_id'],
//                                                                    'email' => $result['email'],
//                                                                    'client_id' => '',
//                                                                    'secret_id' => '',
//                                                                    'refresh_token' => $result['refresh_token'],
//                                                                    'expiry_time' => $result['expires_at'],
//                                                                    'app_id' => 1
//                                                                ]);
                
                  $scopes = @serialize($user->getScopes());
                  if(!empty($result['email'])){
                        $user_result = User::where('email',$result['email'])->first();
                        $count = User::where('email',$result['email'])->count();
                        if($count > 0){
                            Auth::login($user_result);
                            // update
                            $users = User::find(Auth::user()->id);
                            $users->portal_id = $result['portal_id'];
                            $users->hub_email = $user->getEmail();
                            $users->scopes =  $scopes;
                            $users->app_id = @$user->getAppId();
                            $users->refresh_token = @$token->getRefreshToken();
                            $users->expires_at = @$token->getExpires();
                            $users->code = $_GET['code'];
                            $users->auth_token = $token->getToken();
                            $users->save();
                            // update

                            Session::put('portal_id', $result['portal_id']);
                            
                            

                            // Auth::logout();
                            // Auth::login($user_result);
                        } else {
                            unset($result['user_expires_at']);
                            User::create($result);
                            $user_result = User::where('email',$result['email'])->first();
                            Auth::login($user_result);
                        }
                  }

                // $args['data'] = '{
                //       "properties": [
                //         {
                //           "property": "portal_id",
                //           "value": "'.$result['portal_id'].'"
                //         },
                //         {
                //           "property": "hub_email",
                //           "value": "'.$result['email'].'"
                //         },
                //         {
                //           "property": "refresh_token",
                //           "value": "'.$result['refresh_token'].'"
                //         },
                //         {
                //           "property": "token",
                //           "value": "'.$result['auth_token'].'"
                //         },
                //         {
                //           "property": "code",
                //           "value": "'.$result['code'].'"
                //         },
                //         {
                //           "property": "app_id",
                //           "value": "'.$result['app_id'].'"
                //         }
                //       ]
                //     }';
                // ;



                // $args['data'] = '{
                //       "properties": [
                //         {
                //           "property": "email",
                //           "value": "'.$result['email'].'"
                //         },
                //         {
                //           "property": "firstname",
                //           "value": "'.$result['email'].'"
                //         },
                //         {
                //           "property": "lastname",
                //           "value": "'.$result['email'].'"
                //         }
                //       ]
                //     }';
                // ;
                // $args['url'] = 'https://api.hubapi.com/contacts/v1/contact/email/'.$result['email'].'/profile?hapikey='.env('HUBSPOT_API_KEY');
                // $store = $this->curlAccess( 'POST', $args );
                // Use these details to create a new profile

            } catch (Exception $e) {
                // Failed to get user details
                exit('Oh dear...');
            }
            // Use this to interact with an API on the users behalf
            return redirect('/');
        }
    }

    function curlAccess($method, $array, $content_type = 'array' ) 
    {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $array['url']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        switch ($method){
          case "POST":
            curl_setopt($ch, CURLOPT_POST, 1);        
             if ($array['data'])
                curl_setopt($ch, CURLOPT_POSTFIELDS, $array['data']);
             break;
          case "PUT":
             curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
             if ($array['data'])
                curl_setopt($ch, CURLOPT_POSTFIELDS, $array['data']);
             break;
          default:
             curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        }    

        $headers = array();
        $headers[] = 'Content-Type: application/json';
        if(!empty($array['token'])){
            $headers[] = 'authorization: Bearer '. $array['token'];  
        }
        

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $result = curl_exec($ch);
        
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);

        if($content_type == 'array'){
            $result = json_decode($result); 
        }  
        return $result;

    }
    public function logout(){
        Auth::logout();
        Session::flush();
        return redirect('/signin');   
    }
}