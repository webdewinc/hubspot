<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Http\Controllers\API\APIController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;
use DB;
use App\Notifications\UserRegisteredSuccessfully;



class RegisterController extends APIController
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/signin';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('guest');
        //$this->middleware('guest')->except('logout');
    }

    protected function register(Request $request)
    {
        $data = $_POST;

        /** @var User $user */
            $validatedData = $request->validate([
                'email'         => 'required|string|email|max:255|unique:users',
                'password'      => 'required|string',
                'user_fname'    => 'required|string',
                'user_lname'    => 'required|string'
            ]);
        
        if(isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response']))
        {
            $secret = '6Lf0ycYaAAAAAJVIGpVpfFVpi1Rpc75SLeF8RkA6';
            $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
            $responseData = json_decode($verifyResponse);
            if($responseData->success)
            {
                
                try {
                    /* hubspot */
                    // $response = $this->checkContact($validatedData['email'], $args = ['hapi_key' => env('HUBSPOT_API_KEY')]);
                    // if($response['status'] < 1) {
                    //     // contact create
                    //     $args['data'] = '{
                    //         "properties": [
                    //           {
                    //             "property": "firstname",
                    //             "value": "'.$validatedData['user_fname'].'"
                    //           },
                    //           {
                    //             "property": "lastname",
                    //             "value": "'.$validatedData['user_lname'].'"
                    //           },
                    //           {
                    //             "property": "email",
                    //             "value": "'.$validatedData['email'].'"
                    //           },
                    //           {
                    //             "property": "password",
                    //             "value": "'.$validatedData['password'].'"
                    //           }
                    //         ]
                    //       }';
                    //     // contact create
                    //     $args['url'] = "https://api.hubapi.com/contacts/v1/contact/?hapikey=" . env('HUBSPOT_API_KEY');
                    //     $result = curlAccess( 'POST', $args );                
                    //     if($result->vid){
                            $validatedData['password']        = bcrypt(array_get($validatedData, 'password'));
                            $validatedData['user_type']       = 'users';
                            $validatedData['activation_code'] = str_random(30).time();
                            $validatedData['user_email_verified'] = 1;
                            $validatedData['user_status'] = 1;
                            //$validatedData['vid'] = $result->vid;
                            $validatedData['vid'] = 0;
                            $user                             = app(User::class)->create($validatedData);
                            //$user->notify(new UserRegisteredSuccessfully($user));
                            //$link = url('/verify-user') . '/' . $user->activation_code;

                            // $args['email'] = $user->email;                
                            // $args['subject'] = 'FlyBiz';                
                            // $args['template_id'] = 'd-85e8c6fc0df84faf85319b2ca578e46c';                
                            // $args['data'] = '{                        
                            //     "F_name" : "'.@$user->user_fname.'",                       
                            //     "L_name" : "'.@$user->user_lname.'",
                            //     "link"   : "'.$link.'"
                            // }';
                            
                            //email send to register user
                                //sendgridEmail($args);
                            //email send to Admin users  
                            

                    //     } else {
                    //         return redirect()->to('/signup')->with(['message'=> 'Email not able to send.', 'alert' => 'danger','form' => 'signup']);
                    //     }

                    // } else {
                    //     return redirect()->to('/signup')->with(['message'=> 'Email Already Exist.', 'alert' => 'danger','form' => 'signup']);
                    // }
                    /* hubspot */                
                    
                } catch (\Exception $exception) {
                    //logger()->error($exception);
                    return redirect()->to('/signup')->with(['message'=> 'Unable to create new user.', 'alert' => 'danger','form' => 'signup']);                
                }
                //return redirect()->to('/signup')->with(['message'=> 'Thank You for Registration. Please check your email.', 'alert' => 'success','form' => 'signup']);
                return redirect()->to('/signup')->with(['message'=> 'Thank You for Registration.', 'alert' => 'success','form' => 'signup']);
                //return redirect()->to('/signup')->with(['message'=> 'Thank You for Registration.', 'alert' => 'success','form' => 'signup']);
            } else {
                $errMsg = 'Robot verification failed, please try again.';
                return redirect()->to('/signup')->with(['message'=> $errMsg, 'alert' => 'danger','form' => 'signup']); 
            }
        } else {
            $errMsg = 'Robot verification failed, please try again.';
            return redirect()->to('/signup')->with(['message'=> $errMsg, 'alert' => 'danger','form' => 'signup']);
        }
    }
    
    /**
     * Activate the user with given activation code.
     * @param string $activationCode

    * @return string
    */
    public function activateUser(string $activationCode)
    {
        try {
            $user = app(User::class)->where('activation_code', $activationCode)->first();
            if (!$user) {
                return redirect()->to('/signin')->with(['message'=> 'The code does not exist for any user in our system.', 'alert' => 'danger', 'form' => 'signup']);
                
            }
            $user->user_status         = 1;
            $user->user_email_verified = 1;
            $user->activation_code = null;
            $user->save();
            return redirect()->to('/signin')->with(['message'=> 'Successfully activate your account. you can login now.', 'alert' => 'success', 'form' => 'signin']);
        } catch (\Exception $exception) {
            logger()->error($exception);
            return "Whoops! something went wrong.";
        }
        return redirect()->to('/');
    }

    public function varifyemail(Request $request)
    {
        $email = User::where('email', $request->email)->get()->toArray();

        if(count($email) > 0)
        {
            echo json_encode(false);
        } else {
            echo json_encode(true);
        }
    }
    public function varifyemailForgot(Request $request)
    {
        $email = User::where('email', $request->email)->get()->toArray();
        if(count($email) > 0)
        {
            echo json_encode(true);
        } 
        else {
            echo json_encode(false);
        }
    }
   
}
