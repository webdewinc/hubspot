<?php
namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\DB;
use App\SearchRecord;
use App\RestClient;
use App\MstDomainUrl;
use App\MstSeoData;
use App\CnfDomainCountry;
use App\MstCountry;
use App\MstOrganicSearch;
use App\MstOrgExtraField;
use App\MstSimilarSite;
use App\MstDaPa;
use App\User;
use App\Chat;

class APIController extends Controller
{
    

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }
      
    public function checkContact($email, $args = [])
    {
        $args['url'] = "https://api.hubapi.com/contacts/v1/contact/email/".$email."/profile?hapikey=" . $args['hapi_key'];
        $result = curlAccess( 'GET', $args );
        $res = ['status' => 1,'result' => $result];
        if(isset($result->status)) {
            if($result->status == 'error'){
                $res = ['status' => 0,'result' => $result];
            }
        } else {
            $res = ['status' => 1,'result' => $result];
        }
        return $res;
    }
    // public function contactSync() {
    //   //die;
    //     // live webdew account - 8bbdfd78-a950-4838-b686-cc0a0c07e0ac
    //   $live_webdew_hapikey = '8bbdfd78-a950-4838-b686-cc0a0c07e0ac';

    //   // $args['url'] = 'https://api.hubapi.com/crm-objects/v1/objects/products/paged?hapikey=8bbdfd78-a950-4838-b686-cc0a0c07e0ac&properties=name';
    //   //   $product = curlAccess('GET',$args);

    //     // $args['url'] = 'https://api.hubapi.com/owners/v2/owners?hapikey=8bbdfd78-a950-4838-b686-cc0a0c07e0ac';
    //     // $owner = curlAccess('GET',$args);

    //     // echo '<pre>';
    //     // print_r($owner);
    //     // die;
      
    //     $users = User::whereNotIn('email',['meghna.b@webdew.com','guriqbal.d@webdew.com','randeep.s@webdew.com','randeep.s@webdew.com1','danish@webdew.com', 'danish_cloudnanny@webdew.com','hubspot@webdew.com','sourav.v@webdew.com','deepti.s@webdew.com','divya.v@webdew.com'])->get()->toArray();

            
    //     die;

    //     // $args['url'] = 'https://api.hubapi.com/deals/v1/pipelines?hapikey=8bbdfd78-a950-4838-b686-cc0a0c07e0ac';
    //     // $deals = curlAccess('GET',$args);

    //     // echo '<pre>';
    //     // print_r($deals);
    //     // die;

    //     // $username= 'r2JpXguEVDViFGsl5VuHnaXN6U6VSqmf';
    //     // $password= 'Tr74VFTyji94g6d73BvjC6zWL7HdyuaF';
         
    //     // $username= 'd6inKPQKOJbY5vyXLB8UI6N3r2mlStUt';
    //     // $password= '7Ou6xFTEt9R1H9haCtbOU3lZFrSXPqmo';

    //     // $username= 'RHjcycMRiCBe7JXKGWtREPIhkWCKtZzW';
    //     // $password= 'h7DmPs8Cx10eLgHYS2YEIUtMk60VOT4Z';


    //     //http://whmcs.webdew.com/includes/api.php
    //     // $ch = curl_init();
    //     // curl_setopt($ch, CURLOPT_URL, 'https://phpstack-242348-1101472.cloudwaysapps.com/includes/api.php');
    //     // curl_setopt($ch, CURLOPT_POST, 1);
    //     // curl_setopt($ch, CURLOPT_POSTFIELDS,
    //     //     http_build_query(
    //     //         array(
    //     //             'action' => 'GetProducts',
    //     //             // See https://developers.whmcs.com/api/authentication
    //     //             'username' => $username,
    //     //             'password' => $password,
    //     //             'responsetype' => 'json',
    //     //         )
    //     //     )
    //     // );
    //     // curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    //     // $response = curl_exec($ch);
    //     // curl_close($ch);
    //     // echo '<pre>';
    //     // print_r($response);
    //     // die;

        
    //     //$args['url'] = 'https://api.hubapi.com/contacts/v1/contact/?hapikey='.env('HUBSPOT_API_KEY');
                             
    //     foreach ($users as $key => $value) {
          
    //         $user_id = $value['id'];
    //         $applicationRecords = DB::table('application_records')->where('user_id',$user_id)->where('app_id' ,'!=' , 3)->get()->toArray();
    //         if(count($applicationRecords) > 0){
    //             foreach ($applicationRecords as $k => $v) { 
    //                 $app_id = $v->app_id;
    //                 $value['app_type'] = $app_id;
    //                 $vid = $this->contactSearch($value);
    //                 if($app_id == 1) {
    //                     $get_payment = DB::table('payments')->where('user_id',$user_id)->where('amount','>',0)->where('status','succeeded')->where('deal_id',0)->get();
    //                     $dealname  = 'All in one SMS';
    //                     foreach ($get_payment as $kp => $vp) {
    //                         $payment_id = $vp->id;
    //                         $amount = $vp->amount;
    //                         $time = time() * 1000;
    //                         $args['url'] = 'https://api.hubapi.com/deals/v1/deal?hapikey='.$live_webdew_hapikey;
    //                         $args['data'] = '{
    //                                       "associations": {
    //                                         "associatedCompanyIds": [],
    //                                         "associatedVids": [
    //                                           "'.$vid.'"
    //                                         ]
    //                                       },
    //                                       "properties": [
    //                                         {
    //                                           "value": "9597429",
    //                                           "name": "dealstage"
    //                                         },
    //                                         {
    //                                           "value": "'.$dealname.'",
    //                                           "name": "dealname"
    //                                         },
    //                                         {
    //                                           "value": "9597423",
    //                                           "name": "pipeline"
    //                                         },
    //                                         {
    //                                           "value": "'. $time .'",
    //                                           "name": "closedate"
    //                                         },
    //                                         {
    //                                           "value": "'. $amount .'",
    //                                           "name": "amount"
    //                                         },
    //                                         {
    //                                           "value": "newbusiness",
    //                                           "name": "dealtype"
    //                                         }
    //                                       ]
    //                                     }';
    //                         $deals = curlAccess('POST',$args);
                            
    //                         if(isset($deals->dealId)){
    //                             DB::table('payments')->where('id',$payment_id)->update([ 'deal_id' => $deals->dealId ]);
    //                         }

    //                     }
    //                 } else if ($app_id == 2){
    //                     $get_payment = DB::table('whmcs_payments')->where('user_id',$user_id)->where('amount','>',0)->where('status','succeeded')->where('deal_id',0)->get();
    //                     $dealname = 'WHMCS';
    //                     foreach ($get_payment as $kp => $vp) {
    //                         $payment_id = $vp->id;
    //                         $time = time() * 1000;
    //                         $amount = $vp->amount;
    //                         $args['url'] = 'https://api.hubapi.com/deals/v1/deal?hapikey='.$live_webdew_hapikey;
    //                         $args['data'] = '{
    //                                           "associations": {
    //                                             "associatedCompanyIds": [],
    //                                             "associatedVids": [
    //                                               "'.$vid.'"
    //                                             ]
    //                                           },
    //                                           "properties": [
    //                                             {
    //                                                 "value": "9597429",
    //                                                 "name": "dealstage"
    //                                             },
    //                                             {
    //                                               "value": "9597423",
    //                                               "name": "pipeline"
    //                                             },
    //                                             {
    //                                               "value": "'.$dealname.'",
    //                                               "name": "dealname"
    //                                             },
    //                                             {
    //                                                 "value": "'. $time .'",
    //                                                 "name": "closedate"
    //                                             },
    //                                             {
    //                                                 "value": "'. $amount .'",
    //                                                 "name": "amount"
    //                                             },
    //                                             {
    //                                                 "value": "newbusiness",
    //                                                 "name": "dealtype"
    //                                             }
    //                                           ]
    //                                         }';
    //                         $deals = curlAccess('POST',$args);
    //                         if(isset($deals->dealId)){
    //                             DB::table('whmcs_payments')->where('id',$payment_id)->update([ 'deal_id' => $deals->dealId ]);

    //                             // line items
    //                             $args['url'] = 'https://api.hubapi.com/crm-objects/v1/objects/line_items?hapikey='.$live_webdew_hapikey;
    //                             $args['data'] = '[
    //                                           {
    //                                             "name": "hs_product_id",
    //                                             "value": ""
    //                                           },
    //                                           {
    //                                             "name": "quantity",
    //                                             "value": "1"
    //                                           },
    //                                           {
    //                                             "name": "price",
    //                                             "value": "'.$amt.'"
    //                                           }
    //                                         ]';
    //                             $line = curlAccess('POST',$args);
    //                             // line items

    //                             //deal to line items associates
    //                             $args['url'] = 'https://api.hubapi.com/crm-associations/v1/associations?hapikey='.$live_webdew_hapikey;
    //                             $args['data'] = '{
    //                                             "fromObjectId": '.$deals->dealId.',
    //                                             "toObjectId": '.$line->objectId.',
    //                                             "category": "HUBSPOT_DEFINED",
    //                                             "definitionId": 19
    //                                           }';
    //                             $result = curlAccess('PUT',$args);
    //                             //deal to line items associates
    //                         }
    //                     }
    //                 } else if ($app_id == 3){
    //                     // ecommerce
    //                 }
    //             }

    //             // if(isset($deals)){
    //             //   echo '<pre>';
    //             //   print_r($deals);
    //             //   die;  
    //             // }
    //         }
    //     }
    //     die;
    // }

    // private function contactSearch($data){
    //     $live_webdew_hapikey = '8bbdfd78-a950-4838-b686-cc0a0c07e0ac';

    //     $args['url'] = 'https://api.hubapi.com/crm/v3/objects/contacts/search?hapikey='.$live_webdew_hapikey;
    //     $args['data'] =  json_encode([
    //             "filterGroups"=>[
    //             [
    //                 "filters"=>[
    //                     [
    //                         "propertyName" => "email",
    //                         "operator" => "EQ",
    //                         "value" => $data['email']
    //                     ]
    //                 ]
    //             ]
    //         ]
    //     ]);
    //     $result = curlAccess('POST',$args);

    //     // search contact
    //     $vid = 0;
    //     if(!isset($result->status)){
    //           if($result->total > 0){
    //               $vid = $result->results[0]->id;
                   
    //               // update contacts
    //               $args['url'] = 'https://api.hubapi.com/contacts/v1/contact/vid/'.$vid.'/profile?hapikey='.$live_webdew_hapikey;
                  
    //               // $args['data']= json_encode([
    //               //       "properties" => [
    //               //         [
    //               //           "property" => "email",
    //               //           "value" => $data['email']
    //               //         ],
    //               //         [
    //               //           "property" => "firstname",
    //               //           "value" => $data['user_fname']
    //               //         ],
    //               //         [
    //               //           "property" => "lastname",
    //               //           "value" => $data['user_lname']
    //               //         ]
    //               //       ]
    //               // ]);
    //               $args['data']= json_encode([
    //                     "properties" => [
    //                       [
    //                         "property" => "email",
    //                         "value" => $data['email']
    //                       ]
    //                     ]
    //               ]);
             
    //               curlAccess('PUT',$args);
    //               // update contact

    //           } else {
    //               // insert contacts
                 
    //               $args['url'] = 'https://api.hubapi.com/contacts/v1/contact/?hapikey='.$live_webdew_hapikey;
                  
    //               $args['data']= json_encode([
    //                     "properties" => [
    //                       [
    //                         "property" => "email",
    //                         "value" => $data['email']
    //                       ],
    //                       [
    //                         "property" => "firstname",
    //                         "value" => $data['user_fname']
    //                       ],
    //                       [
    //                         "property" => "lastname",
    //                         "value" => $data['user_lname']
    //                       ]
    //                     ]
    //               ]);
    //               $result  = curlAccess('POST',$args);
    //               // insert contact

    //                 if(!isset($result->status)){
    //                     $vid = $result->vid;
    //                 }
    //           }
    //     } 
    //     return $vid;
    // }


    public function contactSync() {
      //die;
     //live webdew account - 8bbdfd78-a950-4838-b686-cc0a0c07e0ac
      //$test_webdew_app_testing_hapikey = '58723188-d3ff-473b-b9b0-24c927e10c10';
        $live_webdew_hapikey = '8bbdfd78-a950-4838-b686-cc0a0c07e0ac';

      // $args['url'] = 'https://api.hubapi.com/crm-objects/v1/objects/products/paged?hapikey=8bbdfd78-a950-4838-b686-cc0a0c07e0ac&properties=name';
      //   $product = curlAccess('GET',$args);

        // $args['url'] = 'https://api.hubapi.com/owners/v2/owners?hapikey=8bbdfd78-a950-4838-b686-cc0a0c07e0ac';
        // $owner = curlAccess('GET',$args);

        // echo '<pre>';
        // print_r($owner);
        // die;
      
        $users = User::whereNotIn('email',['meghna.b@webdew.com','guriqbal.d@webdew.com','randeep.s@webdew.com','randeep.s@webdew.com1','danish@webdew.com', 'danish_cloudnanny@webdew.com','hubspot@webdew.com','sourav.v@webdew.com','deepti.s@webdew.com','divya.v@webdew.com'])->get()->toArray();
                             
        foreach ($users as $key => $value) {
          
            $user_id = $value['id'];
            $applicationRecords = DB::table('application_records')->where('user_id',$user_id)->where('app_id' ,'!=' , 3)->get()->toArray();
            if(count($applicationRecords) > 0) {
                foreach ($applicationRecords as $k => $v) { 
                  
                    $app_id = $v->app_id;
                    $value['app_type'] = $app_id;
                    $vid = $this->contactSearch($value);
                    if($app_id == 1) {
                        $get_payment = DB::table('payments')->where('user_id',$user_id)->where('amount','>',0)->where('status','succeeded')->get()->toArray();
                        $dealname  = 'All in one SMS';
                        $time = time() * 1000;
                        $amount = 0;
                        if(count($get_payment) > 0){
                            foreach ($get_payment as $kp => $vp) {
                                $exist_deal_id = $vp->deal_id;
                                $payment_id = $vp->id;
                                $amount = $vp->amount;
                                $args['data'] = '{
                                          "associations": {
                                            "associatedCompanyIds": [],
                                            "associatedVids": [
                                              "'.$vid.'"
                                            ]
                                          },
                                          "properties": [
                                            {
                                              "value": "9597429",
                                              "name": "dealstage"
                                            },
                                            {
                                              "value": "'.$dealname.'",
                                              "name": "dealname"
                                            },
                                            {
                                              "value": "9597423",
                                              "name": "pipeline"
                                            },
                                            {
                                              "value": "44937937",
                                              "name": "hubspot_owner_id"
                                            },
                                            {
                                              "value": "'. $time .'",
                                              "name": "closedate"
                                            },
                                            {
                                              "value": "'. $amount .'",
                                              "name": "amount"
                                            },
                                            {
                                              "value": "newbusiness",
                                              "name": "dealtype"
                                            }
                                          ]
                                        }';

                                if($exist_deal_id == 0) {
                                    $args['url'] = 'https://api.hubapi.com/deals/v1/deal?hapikey='.$live_webdew_hapikey;
                                    $deals = curlAccess('POST',$args);
                                    $exist_deal_id = $deals->dealId;

                                    // line items
                                    $args['url'] = 'https://api.hubapi.com/crm-objects/v1/objects/line_items?hapikey='.$live_webdew_hapikey;
                                    $args['data'] = '[
                                                  {
                                                    "name": "hs_product_id",
                                                    "value": "535611447"
                                                  },
                                                  {
                                                    "name": "quantity",
                                                    "value": "1"
                                                  },
                                                  {
                                                    "name": "price",
                                                    "value": "'.$amount.'"
                                                  }
                                                ]';
                                    $line = curlAccess('POST',$args);
                                    // // line items

                                    // //deal to line items associates
                                    $args['url'] = 'https://api.hubapi.com/crm-associations/v1/associations?hapikey='.$live_webdew_hapikey;
                                    $args['data'] = '{
                                                    "fromObjectId": '.$deals->dealId.',
                                                    "toObjectId": '.$line->objectId.',
                                                    "category": "HUBSPOT_DEFINED",
                                                    "definitionId": 19
                                                  }';
                                    curlAccess('PUT',$args);
                                    //deal to line items associates

                                    DB::table('payments')->where('id',$payment_id)->update([ 'deal_id' => $exist_deal_id, 'line_item_id' => $line->objectId  ]);


                                } else {
                                    //$args['url'] = 'https://api.hubapi.com/deals/v1/deal/'.$exist_deal_id.'?hapikey='.$live_webdew_hapikey;
                                    //curlAccess('PUT',$args);
                                }
                            }  
                        } else {
                              $args['url'] = 'https://api.hubapi.com/deals/v1/deal?hapikey='.$live_webdew_hapikey;
                              $args['data'] = '{
                                              "associations": {
                                                "associatedCompanyIds": [],
                                                "associatedVids": [
                                                  "'.$vid.'"
                                                ]
                                              },
                                              "properties": [
                                                {
                                                  "value": "9597429",
                                                  "name": "dealstage"
                                                },
                                                {
                                                  "value": "'.$dealname.'",
                                                  "name": "dealname"
                                                },
                                                {
                                                  "value": "9597423",
                                                  "name": "pipeline"
                                                },
                                                {
                                                  "value": "44937937",
                                                  "name": "hubspot_owner_id"
                                                },
                                                {
                                                  "value": "'. $time .'",
                                                  "name": "closedate"
                                                },
                                                {
                                                  "value": "'.$amount.'",
                                                  "name": "amount"
                                                },
                                                {
                                                  "value": "newbusiness",
                                                  "name": "dealtype"
                                                }
                                              ]
                                            }';
                                if($value['deal_id'] == 0){
                                    $deals = curlAccess('POST',$args);
                                    if(isset($deals->dealId)){
                                        User::where('id',$user_id)->update(['deal_id' => $deals->dealId]);    
                                    }
                                }
                                
                        }
                        
                    } else if ($app_id == 2){
                        $get_payment = DB::table('whmcs_payments')->where('user_id',$user_id)->where('amount','>',0)->where('status','succeeded')->get()->toArray();
                        $dealname = 'WHMCS';
                        $time = time() * 1000;
                        $amount = 0;
                        if(count($get_payment) > 0){
                            foreach ($get_payment as $kp => $vp) {
                                $exist_deal_id = $vp->deal_id;
                                $payment_id = $vp->id;
                                $amount = $vp->amount;
                                $args['url'] = 'https://api.hubapi.com/deals/v1/deal?hapikey='.$live_webdew_hapikey;
                                $args['data'] = '{
                                                  "associations": {
                                                    "associatedCompanyIds": [],
                                                    "associatedVids": [
                                                      "'.$vid.'"
                                                    ]
                                                  },
                                                  "properties": [
                                                    {
                                                        "value": "9597429",
                                                        "name": "dealstage"
                                                    },
                                                    {
                                                      "value": "9597423",
                                                      "name": "pipeline"
                                                    },
                                                    {
                                                      "value": "'.$dealname.'",
                                                      "name": "dealname"
                                                    },
                                                    {
                                                        "value": "44937937",
                                                        "name": "hubspot_owner_id"
                                                    },
                                                    {
                                                        "value": "'. $time .'",
                                                        "name": "closedate"
                                                    },
                                                    {
                                                        "value": "'. $amount .'",
                                                        "name": "amount"
                                                    },
                                                    {
                                                        "value": "newbusiness",
                                                        "name": "dealtype"
                                                    }
                                                  ]
                                                }';
                                if($exist_deal_id == 0) {
                                    $args['url'] = 'https://api.hubapi.com/deals/v1/deal?hapikey='.$live_webdew_hapikey;
                                    $deals = curlAccess('POST',$args);
                                    $exist_deal_id = $deals->dealId;
                                        
                                        if($vp->app_type == 'daily_month'){
                                            $p_id = '535611452';
                                        } else if($vp->app_type == 'daily_year'){
                                            $p_id = '535611456';
                                        } else if($vp->app_type == 'hour_month'){
                                            $p_id = '535611460';
                                        } else if($vp->app_type == 'hour_year'){
                                            $p_id = '535611461';
                                        }
                                        // line items
                                        $args['url'] = 'https://api.hubapi.com/crm-objects/v1/objects/line_items?hapikey='.$live_webdew_hapikey;
                                        $args['data'] = '[
                                                      {
                                                        "name": "hs_product_id",
                                                        "value": "'.$p_id.'"
                                                      },
                                                      {
                                                        "name": "quantity",
                                                        "value": "1"
                                                      },
                                                      {
                                                        "name": "price",
                                                        "value": "'.$amount.'"
                                                      }
                                                    ]';
                                        $line = curlAccess('POST',$args);
                                        // // line items

                                        if(isset($line->objectId)){
                                            // //deal to line items associates
                                            $args['url'] = 'https://api.hubapi.com/crm-associations/v1/associations?hapikey='.$live_webdew_hapikey;
                                            $args['data'] = '{
                                                            "fromObjectId": '.$deals->dealId.',
                                                            "toObjectId": '.$line->objectId.',
                                                            "category": "HUBSPOT_DEFINED",
                                                            "definitionId": 19
                                                          }';
                                            $result = curlAccess('PUT',$args);
                                            //deal to line items associates    
                                            DB::table('whmcs_payments')->where('id',$payment_id)->update(['line_item_id' => @$line->objectId ]);
                                        }
                                        DB::table('whmcs_payments')->where('id',$payment_id)->update([ 'deal_id' => $exist_deal_id]);

                                } else {
                                    //$args['url'] = 'https://api.hubapi.com/deals/v1/deal/'.$exist_deal_id.'?hapikey='.$live_webdew_hapikey;
                                    //curlAccess('PUT',$args);
                                }
                            }
                        } else {
                            $args['url'] = 'https://api.hubapi.com/deals/v1/deal?hapikey='.$live_webdew_hapikey;
                            $args['data'] = '{
                                              "associations": {
                                                "associatedCompanyIds": [],
                                                "associatedVids": [
                                                  "'.$vid.'"
                                                ]
                                              },
                                              "properties": [
                                                {
                                                    "value": "9597429",
                                                    "name": "dealstage"
                                                },
                                                {
                                                  "value": "9597423",
                                                  "name": "pipeline"
                                                },
                                                {
                                                  "value": "44937937",
                                                  "name": "hubspot_owner_id"
                                                },
                                                {
                                                  "value": "'.$dealname.'",
                                                  "name": "dealname"
                                                },
                                                {
                                                    "value": "'. $time .'",
                                                    "name": "closedate"
                                                },
                                                {
                                                    "value": "'.$amount.'",
                                                    "name": "amount"
                                                },
                                                {
                                                    "value": "newbusiness",
                                                    "name": "dealtype"
                                                }
                                              ]
                                            }';
                            if($value['deal_id'] == 0){
                                $deals = curlAccess('POST',$args);
                                if(isset($deals->dealId)){

                                    // line items
                                    $args['url'] = 'https://api.hubapi.com/crm-objects/v1/objects/line_items?hapikey='.$live_webdew_hapikey;
                                    $args['data'] = '[
                                                  {
                                                    "name": "hs_product_id",
                                                    "value": "585992229"
                                                  },
                                                  {
                                                    "name": "quantity",
                                                    "value": "1"
                                                  },
                                                  {
                                                    "name": "price",
                                                    "value": "'.$amount.'"
                                                  }
                                                ]';
                                    $line = curlAccess('POST',$args);
                                    // line items

                                    if(isset($line->objectId)){
                                        //deal to line items associates
                                        $args['url'] = 'https://api.hubapi.com/crm-associations/v1/associations?hapikey='.$live_webdew_hapikey;
                                        $args['data'] = '{
                                                        "fromObjectId": '.$deals->dealId.',
                                                        "toObjectId": '.$line->objectId.',
                                                        "category": "HUBSPOT_DEFINED",
                                                        "definitionId": 19
                                                      }';
                                        curlAccess('PUT',$args);
                                        //deal to line items associates    
                                        User::where('id',$user_id)->update(['line_item_id' => $line->objectId]);
                                    }
                                    User::where('id',$user_id)->update(['deal_id' => $deals->dealId]);
                                }    
                            }
                            
                        }
                    } else if ($app_id == 3){
                        // ecommerce
                    }
                }
            }
        }
        echo 'All done';
        die;
    }
    
    private function contactSearch($data){
        //$live_webdew_hapikey = '58723188-d3ff-473b-b9b0-24c927e10c10';
        $live_webdew_hapikey = '8bbdfd78-a950-4838-b686-cc0a0c07e0ac';

        $args['url'] = 'https://api.hubapi.com/crm/v3/objects/contacts/search?hapikey='.$live_webdew_hapikey;
        $args['data'] =  json_encode([
                "filterGroups"=>[
                [
                    "filters"=>[
                        [
                            "propertyName" => "email",
                            "operator" => "EQ",
                            "value" => $data['email']
                        ]
                    ]
                ]
            ]
        ]);
        $result = curlAccess('POST',$args);

        // search contact
        $vid = 0;
        if(!isset($result->status)){
              if($result->total > 0){
                  $vid = $result->results[0]->id;
                   
                  // update contacts
                  $args['url'] = 'https://api.hubapi.com/contacts/v1/contact/vid/'.$vid.'/profile?hapikey='.$live_webdew_hapikey;
                  
                  // $args['data']= json_encode([
                  //       "properties" => [
                  //         [
                  //           "property" => "email",
                  //           "value" => $data['email']
                  //         ],
                  //         [
                  //           "property" => "firstname",
                  //           "value" => $data['user_fname']
                  //         ],
                  //         [
                  //           "property" => "lastname",
                  //           "value" => $data['user_lname']
                  //         ]
                  //       ]
                  // ]);
                  $args['data']= json_encode([
                        "properties" => [
                          [
                            "property" => "email",
                            "value" => $data['email']
                          ]
                        ]
                  ]);
             
                  curlAccess('PUT',$args);
                  // update contact

              } else {
                  // insert contacts
                 
                  $args['url'] = 'https://api.hubapi.com/contacts/v1/contact/?hapikey='.$live_webdew_hapikey;
                  
                  $args['data']= json_encode([
                        "properties" => [
                          [
                            "property" => "email",
                            "value" => $data['email']
                          ],
                          [
                            "property" => "firstname",
                            "value" => $data['user_fname']
                          ],
                          [
                            "property" => "lastname",
                            "value" => $data['user_lname']
                          ]
                        ]
                  ]);
                  $result  = curlAccess('POST',$args);
                  // insert contact

                    if(!isset($result->status)){
                        $vid = $result->vid;
                    }
              }
        } 
        return $vid;
    }

}
