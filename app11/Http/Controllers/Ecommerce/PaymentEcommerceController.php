<?php

namespace App\Http\Controllers\Ecommerce;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Support\Facades\Validator;
use Auth;
use Crypt;
use Redirect;
use Hash;
use DB;
use App\User;
use App\Credential;
use Stripe\Error\Card;
use Stripe;
use Session;

class PaymentEcommerceController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }
    /**
     * handling payment with POST
     */
    public function EcommereceStripeHandlePost(Request $request)
    {

        $data = $request->all();
        if(!empty(DB::table('credentials')->where('type','ecommerce')->wherePortalId($data['portal_id'])->value('data'))) {
            $credential_all = unserialize(DB::table('credentials')->where('type','ecommerce')->wherePortalId($data['portal_id'])->value('data'));
            try {
                $amount = $data['amount'];
                if($amount > 0) {
                    Stripe\Stripe::setApiKey($credential_all['stripe_sk_key']);
                    $response= Stripe\Charge::create ([
                        "amount" => $amount,
                        "currency" => "usd",
                        "source" => $data['stripeToken'],
                        "description" => "Plugin Charges" 
                    ]);
                    
                    $status = $response->status;

                    if($status == 'succeeded') {
                 
                        $id = $response->id;
                        $amount = $response->amount / 100;
                        $currency = $response->currency;
                        $customer_name = $request->request->get('cardholder');
                        $status = $response->status;
                        $email  = $response->billing_details->name;
                        $res = json_encode($response);
                        $payment_status = $response->paid;
                        $receipt_url = @$response->receipt_url;

                        $res = $this->dataSave($data, $credential_all);
                        echo $_GET['callback']."(".json_encode($res).");";
                        exit;
                    } else {
                        $res = ['status' => false, 'data' => $data, 'message' => 'Payment failed.'];
                        echo $_GET['callback']."(".json_encode($res).");";
                        exit;
                    }
                } 
            } catch(\throwable $e) {

                $res = ['status' => false, 'data' => $data,'message' => $e->getMessage()];
                echo $_GET['callback']."(".json_encode($res).");";
                exit;
            }
            return $res;
        } 
        
    }

    public function dataSave($data, $credential_all){


        $hapi_key = $credential_all['hapi_key'];
        // search contact
        $args['url'] = 'https://api.hubapi.com/crm/v3/objects/contacts/search?hapikey='.$hapi_key;
        $args['data'] =  json_encode([
                          "filterGroups"=>[
                            [
                              "filters"=>[
                                [
                                  "propertyName" => "email",
                                  "operator" => "EQ",
                                  "value" => $data['email_mobile']
                                ]
                              ]
                            ]
                          ]
                        ]);
        $result = curlAccess('POST',$args);
        
        // search contact
        $vid = 0;
        if(!isset($result->status)){
            if($result->total > 0){
                $vid = $result->results[0]->id;
                // update contacts
                $args['url'] = 'https://api.hubapi.com/contacts/v1/contact/vid/'.$vid.'/profile?hapikey='.$hapi_key;
                
                $args['data']= json_encode([
                      "properties" => [
                        [
                          "property" => "email",
                          "value" => $data['email_mobile']
                        ],
                        [
                          "property" => "firstname",
                          "value" => $data['fname']
                        ],
                        [
                          "property" => "lastname",
                          "value" => $data['lname']
                        ]
                      ]
                ]);
                curlAccess('POST',$args);
                // update contact

            } else {
                // insert contacts
                $args['url'] = 'https://api.hubapi.com/contacts/v1/contact/?hapikey='.$hapi_key;
                $args['data']= json_encode([
                      "properties" => [
                        [
                          "property" => "email",
                          "value" => $data['email_mobile']
                        ],
                        [
                          "property" => "firstname",
                          "value" => $data['fname']
                        ],
                        [
                          "property" => "lastname",
                          "value" => $data['lname']
                        ]
                      ]
                ]);
           
                $result  = curlAccess('POST',$args);
                if(!isset($result->status)){
                    $vid = $result->vid;
                }
                // insert contact
            }

            // create deals
              $a = $data['amount'] / 100;
              $timestamp = time() * 1000;
              $args['url'] = 'https://api.hubapi.com/deals/v1/deal?hapikey='.$hapi_key;
              $args['data']= '{
                 "associations": {
                   "associatedCompanyIds": [],
                   "associatedVids": [
                     '.$vid.'
                   ]
                 },
                 "properties": [
                   {
                     "value": "Deal",
                     "name": "dealname"
                   },
                   {
                     "value": "appointmentscheduled",
                     "name": "dealstage"
                   },
                   {
                     "value": "default",
                     "name": "pipeline"
                   },
                   {
                     "value": '.$timestamp.',
                     "name": "closedate"
                   },
                   {
                     "value": "'.$a.'",
                     "name": "amount"
                   },
                   {
                     "value": "newbusiness",
                     "name": "dealtype"
                   }
                 ]
              }';
         
              $r = curlAccess('POST', $args);
            // create deals
           
         
            if(isset($r->dealId)) {
                $deal_id = $r->dealId;
 
                foreach ($data['pro'] as $key => $value) {
                    $pro_id = $key;
                    $d = explode('_', $value);
                    $qty = 0;
                    $amt = 0;
                    if(isset($d[1])){
                        $qty = $d[1];
                    }
                    if(isset($d[0])){
                        $amt = $d[0];
                    }

            
                    // line items
                    $args3['url'] = 'https://api.hubapi.com/crm-objects/v1/objects/line_items?hapikey='.$hapi_key;
                    $args3['data'] = '[
                                  {
                                    "name": "hs_product_id",
                                    "value": "'.$pro_id.'"
                                  },
                                  {
                                    "name": "quantity",
                                    "value": "'.$qty.'"
                                  },
                                  {
                                    "name": "price",
                                    "value": "'.$amt.'"
                                  }
                                ]';
                    $line = curlAccess('POST',$args3);

                    // line items

                    //deal to line items associates
                    $args['url'] = 'https://api.hubapi.com/crm-associations/v1/associations?hapikey='.$hapi_key;
                    $args['data'] = '{
                                    "fromObjectId": '.$deal_id.',
                                    "toObjectId": '.$pro_id.',
                                    "category": "HUBSPOT_DEFINED",
                                    "definitionId": 19
                                  }';
                    $lineasso = curlAccess('PUT',$args);
                 
                    //deal to line items associates
                }  
                $res = ['status' => true, 'message' => 'Payment has been done.'];
            } else {
                $res = ['status' => false, 'message' => $r->message];
            }
        
       //  if(isset($r->dealId)){
       //   $res = ['status' => true, 'message' => 'Payment has been done.'];
       // }
       // else{
       //  $res = ['status' => false, 'message' => $r->message];
       // }
        }
        return $res;
    }

    public function EcommerecePayPalHandlePost(Request $request)
    {

        $data = $request->all();
        if(!empty(DB::table('credentials')->where('type','ecommerce')->wherePortalId($data['portal_id'])->value('data'))) {
            $credential_all = unserialize(DB::table('credentials')->where('type','ecommerce')->wherePortalId($data['portal_id'])->value('data'));
            try {
                $amount = $data['amount'] / 100;

                $res = $this->dataPaypalSave($data, $credential_all);
                echo $_GET['callback']."(".json_encode($res).");";
                exit;
            } catch(\throwable $e) {
                
                $res = ['status' => false, 'data' => $data,'message' => $e->getMessage()];
                echo $_GET['callback']."(".json_encode($res).");";
                exit;
            }
            return $res;
        } 
        
    }
    
    public function dataPaypalSave($data, $credential_all){
        $hapi_key = $credential_all['hapi_key'];
        // search contact
        $args['url'] = 'https://api.hubapi.com/crm/v3/objects/contacts/search?hapikey='.$hapi_key;
        $args['data'] =  json_encode([
                          "filterGroups"=>[
                            [
                              "filters"=>[
                                [
                                  "propertyName" => "email",
                                  "operator" => "EQ",
                                  "value" => $data['email_mobile']
                                ]
                              ]
                            ]
                          ]
                        ]);
        $result = curlAccess('POST',$args);
        
        // search contact
        $vid = 0;
        if(!isset($result->status)){
            if($result->total > 0){
                $vid = $result->results[0]->id;
                // update contacts
                $args['url'] = 'https://api.hubapi.com/contacts/v1/contact/vid/'.$vid.'/profile?hapikey='.$hapi_key;
                
                $args['data']= json_encode([
                      "properties" => [
                        [
                          "property" => "email",
                          "value" => $data['email_mobile']
                        ],
                        [
                          "property" => "firstname",
                          "value" => $data['fname']
                        ],
                        [
                          "property" => "lastname",
                          "value" => $data['lname']
                        ]
                      ]
                ]);
                curlAccess('POST',$args);
                // update contact

            } else {
                // insert contacts
                $args['url'] = 'https://api.hubapi.com/contacts/v1/contact/?hapikey='.$hapi_key;
                $args['data']= json_encode([
                      "properties" => [
                        [
                          "property" => "email",
                          "value" => $data['email_mobile']
                        ],
                        [
                          "property" => "firstname",
                          "value" => $data['fname']
                        ],
                        [
                          "property" => "lastname",
                          "value" => $data['lname']
                        ]
                      ]
                ]);
           
                $result  = curlAccess('POST',$args);
                if(!isset($result->status)){
                    $vid = $result->vid;
                }
                // insert contact
            }

            // create deals
              $a = $data['amount'] / 100;
              $timestamp = time() * 1000;
              $args['url'] = 'https://api.hubapi.com/deals/v1/deal?hapikey='.$hapi_key;
              $args['data']= '{
                 "associations": {
                   "associatedCompanyIds": [],
                   "associatedVids": [
                     '.$vid.'
                   ]
                 },
                 "properties": [
                   {
                     "value": "Deal",
                     "name": "dealname"
                   },
                   {
                     "value": "appointmentscheduled",
                     "name": "dealstage"
                   },
                   {
                     "value": "default",
                     "name": "pipeline"
                   },
                   {
                     "value": '.$timestamp.',
                     "name": "closedate"
                   },
                   {
                     "value": "'.$a.'",
                     "name": "amount"
                   },
                   {
                     "value": "newbusiness",
                     "name": "dealtype"
                   }
                 ]
              }';
         
              $r = curlAccess('POST', $args);
            // create deals
            
            if(isset($r->dealId)) {
                $deal_id = $r->dealId;
                foreach ($data['pro'] as $key => $value) {
                    $pro_id = $key;
                    $d = explode('_', $value);
                    $qty = 0;
                    $amt = 0;
                    if(isset($d[1])){
                        $qty = $d[1];
                    }
                    if(isset($d[0])){
                        $amt = $d[0];
                    }

                    // line items
                    $args['url'] = 'https://api.hubapi.com/crm-objects/v1/objects/line_items?hapikey='.$hapi_key;
                    $args['data'] = '[
                                  {
                                    "name": "hs_product_id",
                                    "value": "'.$pro_id.'"
                                  },
                                  {
                                    "name": "quantity",
                                    "value": "'.$qty.'"
                                  },
                                  {
                                    "name": "price",
                                    "value": "'.$amt.'"
                                  }
                                ]';
                    $line = curlAccess('POST',$args);
                    // line items


                    // deal to line items associates
                    $args['url'] = 'https://api.hubapi.com/crm-associations/v1/associations?hapikey='.$hapi_key;
                    $args['data'] = '{
                                    "fromObjectId": '.$deal_id.',
                                    "toObjectId": '.$line->objectId.',
                                    "category": "HUBSPOT_DEFINED",
                                    "definitionId": 19
                                  }';
                    $lineasso = curlAccess('PUT',$args);
                    // deal to line items associates

                }  
                $res = ['status' => true, 'message' => 'Payment has been done.'];
            } else {

                $res = ['status' => false, 'message' => $r->message];
              }
            
        }
        return $res;
    }

}
