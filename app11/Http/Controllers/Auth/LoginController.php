<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Auth;
use App\User;
use Session;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/signin';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    public function login(Request $request)
    {   
        
        if(Auth::attempt(['email' => $request->user_email, 'password' => $request->password_login, 'user_type' => 'users'],($request->remember == 'on') ? true : false)) {
            
            if(Auth::user()->user_email_verified == 1) {
                return redirect('home');
                
            } else {
                Auth::logout();
                return redirect()->to('/signin')->with(['message'=> 'Please activate your account first.', 'alert' => 'danger','form' => 'signin']);
            }
            

        } else {
            $user = User::where('email', $request->user_email)
                  ->where('password',md5($request->password_login))
                  ->where('user_type','users')
                  ->first();
            $user_exist = json_decode(json_encode($user), true);
            if($user_exist){
                Auth::login($user);
                if($user_exist['user_type'] == 'users'){
                   
                    if($user_exist['user_email_verified'] == 1) {
                        return redirect('home');
                    } else {
                        
                        Auth::logout();
                        return redirect()->to('/signin')->with(['message'=> 'Please activate your account first.', 'alert' => 'danger','form' => 'signin']);    
                    }
                } else {
                    return redirect()->to('/signin')->with(['message'=> 'Something went wrong in user_type and switched user.', 'alert' => 'danger','form' => 'signin']);
                }
            } else {
                return redirect()->to('/signin')->with(['message'=> 'Please enter a valid email address and password.', 'alert' => 'danger','form' => 'signin']);

            }            
        }
    }
    
    /**
     * @param Request $request
     * @throws ValidationException
     */
    protected function sendFailedLoginResponse(Request $request)
    {
        $request->session()->put('login_error', trans('auth.failed'));
        throw ValidationException::withMessages(
            [
                'error' => [trans('auth.failed')],
            ]
        );
    }

    /**
     * @param Request $request
     * @throws ValidationException
     */
    protected function logout(Request $request)
    {
        Auth::logout();
        return redirect()->to('signin');
    }
}
