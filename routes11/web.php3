<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes(['verify' => true,'login' => true, 'register'=>true]);

Route::match(['get','post'],'signin',[ 'as' => 'signin', 'uses' => 'Auth\LoginController@showLoginForm']);
Route::match(['get','post'],'signup','Auth\RegisterController@showRegistrationForm');
Route::match(['get','post'],'signout','Auth\LoginController@logout');

Route::get('/verify-user/{code}', 'Auth\RegisterController@activateUser')->name('activate.user');
Route::match(['get','post'],'varifyemail','Auth\RegisterController@varifyemail');
Route::match(['get','post'],'varifyemailForgot','Auth\RegisterController@varifyemailForgot');
Route::match(['get','post'],'varifyusercode','Auth\RegisterController@varifyUsercode');
Route::match(['get','post'],'login-ajax','Auth\LoginController@login_ajax')->name('login-ajax');

Route::match(['get','post'],'definations','HubController@definations');
Route::match(['get','post'],'workflowSent', 'HubController@workflowSent');



Route::get('fetch-view', 'HubController@fetch_view');
// Route::get('/noauth-settings', 'HubController@settings');
// Route::post('/noauth-settings', 'HubController@POST_settings');

Route::get('send-sms', 'HubController@GET_send_sms');

// Route::get('fetch-view', 'HomeController@fetch_view');
// Route::get('send-sms', 'HomeController@GET_send_sms');
// Route::post('send-sms', 'HomeController@POST_send_sms');


Route::get('terms-service', 'HubController@terms_service');
Route::get('policy', 'HubController@policy');
Route::get('documentation', 'HubController@documentation');
Route::get('tagline', 'HubController@tagline');

// website pages

Route::get('/patch', 'HomeController@patch');
Route::get('/vendor1','HubController@vendor1');
// website pages

Route::group(['middleware' => 'auth'], function() {
	
	

	Route::get('/auth', 'HomeController@auth');
	Route::get('/auth2-callback', 'HomeController@auth2_callback');

	Route::group(['middleware' => 'auth_token'], function() {

	/** routes for admin panel **/
		Route::get('/', 'HomeController@index');
		Route::get('edit-profile','ProfileController@edit_profile');
		Route::post('update_profile','ProfileController@store_profile');
		Route::post('/change_password','ProfileController@store_change_password');

		// Route::get('/settings', 'ProfileController@settings');
		// Route::post('/settings', 'ProfileController@POST_settings');
		Route::post('send-sms', 'HubController@POST_send_sms');
		Route::get('/settings', 'HubController@settings');
		Route::post('/settings', 'HubController@POST_settings');
		Route::post('/render-vendor', 'HubController@render_vendor');

		Route::get('/settings-hub', 'HubController@settings_hub');
		Route::post('/settings-hub', 'HubController@POST_settings_hub');

	    Route::get('/home', 'HomeController@home');
		Route::post('/custom', 'HomeController@sendCustomMessage');

		Route::get('/contacts/{portal_id}/contacts/list/view/all/', 'HomeController@contacts');
		Route::get('/contacts/{portal_id}/contact/{vid}', 'HomeController@contact_details');
	});

});
