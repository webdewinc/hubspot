<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// common Auth
Auth::routes(['verify' => true,'login' => true, 'register'=>true]);

Route::match(['get','post'],'signin',[ 'as' => 'signin', 'uses' => 'Auth\LoginController@showLoginForm']);
Route::match(['get','post'],'signup','Auth\RegisterController@showRegistrationForm');

Route::get('/{app}/login', 'LoginController@login');
//Route::match(['get','post'],'/{app}/signout','Auth\LoginController@logout');
Route::match(['get','post'],'/signout','Auth\LoginController@logout');
Route::get('auth2-callback', 'HomeController@auth2_callback');
// common Auth



// smsdew
Route::get('/smsdew', 'SMS\SMSController@home');
Route::get('/all-in-one-sms', 'SMS\SMSController@all_in_sms');
Route::match(['get','post'],'smsdew/definations','SMS\SMSController@definations');
Route::match(['get','post'],'smsdew/workflowSent','SMS\SMSController@workflowSent');

//Route::post('smsdew/workflowsent','SMS\SMSController@workflowsent');
//Route::match(['get','post'],'smsdew/check', 'SMS\SMSController@check');

Route::post('smsdew/check','SMS\SMSController@check');
Route::get('smsdew/checking','SMS\SMSController@checking');

Route::get('smsdew/fetch-view', 'SMS\SMSController@fetch_view');
Route::get('smsdew/send-sms/{id}', 'SMS\SMSController@GET_send_sms');
Route::get('smsdew/send-sms-app/{id}', 'SMS\SMSController@GET_send_sms_app');
Route::post('smsdew/send-sms', 'SMS\SMSController@POST_send_sms');
Route::post('smsdew/send-sms-app', 'SMS\SMSController@POST_send_sms_app');
Route::get('smsdew/settings-hub/{id}', 'SMS\SMSController@settings_hub');
Route::post('smsdew/settings-hub', 'SMS\SMSController@POST_settings_hub');
Route::get('smsdew/render-vendor/{id}', 'SMS\SMSController@render_vendor');
Route::get('smsdew/terms-service', 'SMS\SMSController@terms_service');
Route::get('smsdew/policy', 'SMS\SMSController@policy');
Route::get('smsdew/documentation', 'SMS\SMSController@documentation');
Route::get('smsdew/tagline', 'SMS\SMSController@tagline');
Route::get('smsdew/auth2-callback', 'SMS\SMSController@auth2_callback');
// smsdew

//payment sms-dew

Route::get('smsdew/payment','PaymentController@payment');

Route::get('smsdew/stripe-payment', 'SMS\SMSController@handleGet');
Route::post('smsdew/stripe-payment', 'SMS\SMSController@handlePost')->name('stripe.payment');
Route::get('smsdew/payment','SMS\SMSController@stripepay');

//payment sms-dew


//WHMCS
Route::get('/whmcs', 'WHMCS\WHMCSController@home');
Route::match(['get','post'],'whmcs/definations','HubController@definations');
//Route::match(['get','post'],'whmcs/workflowSent', 'HubController@workflowSent');
Route::match(['get','post'],'whmcs/workflowSent', 'WHMCS\WHMCSController@workflowSent');
Route::get('whmcs/fetch-view', 'WHMCS\WHMCSController@fetch_view');
Route::get('whmcs/settings-hub/{id}', 'WHMCS\WHMCSController@GET_settings_hub');
Route::post('whmcs/settings-hub', 'WHMCS\WHMCSController@POST_settings_hub');
Route::get('sync-data-hub/{id}', 'WHMCS\WHMCSController@sync_data_hub');
Route::get('whmcs-products-hub', 'WHMCS\WHMCSController@whmcs_products_hub');
Route::get('whmcs-contacts-hub', 'WHMCS\WHMCSController@whmcs_contacts_hub');
Route::get('whmcs-orders-hub', 'WHMCS\WHMCSController@whmcs_orders_hub');
Route::get('whmcs/terms-service', 'WHMCS\WHMCSController@terms_service');
Route::get('whmcs/policy', 'WHMCS\WHMCSController@policy');
Route::get('whmcs/documentation', 'WHMCS\WHMCSController@documentation');
Route::get('whmcs/tagline', 'WHMCS\WHMCSController@tagline');
Route::get('whmcs/auth2-callback', 'WHMCS\WHMCSController@auth2_callback');
Route::match(['get','post'],'whmcs/definations','WHMCS\WHMCSController@definations');
//WHMCS

// ecommerce
Route::get('/ecommerce', 'Ecommerce\EcommerceController@home');
Route::get('ecommerce/settings-hub', 'Ecommerce\EcommerceController@GET_settings_hub');
Route::post('ecommerce/settings-hub', 'Ecommerce\EcommerceController@POST_settings_hub');
Route::get('ecommerce/fetch-view', 'Ecommerce\EcommerceController@fetch_view');
Route::match(['get','post'],'get-product-list', 'Ecommerce\EcommerceController@get_product_list');
Route::match(['get','post'],'get-product-list-hub/{auth_id}', 'Ecommerce\EcommerceController@get_product_list_hub');
Route::match(['get','post'],'get-product-list-workflow/{auth_id}', 'Ecommerce\EcommerceController@get_product_list_workflow');
Route::get('ecommerce/sync-data/{auth_id}', 'Ecommerce\EcommerceController@sync_data');
Route::get('ecommerce/terms-service', 'Ecommerce\EcommerceController@terms_service');
Route::get('ecommerce/policy', 'Ecommerce\EcommerceController@policy');
Route::get('ecommerce/documentation', 'Ecommerce\EcommerceController@documentation');
Route::get('ecommerce/tagline', 'Ecommerce\EcommerceController@tagline');
Route::get('ecommerce/auth2-callback', 'Ecommerce\EcommerceController@auth2_callback');

Route::get('ecommerce/ctabutton','Ecommerce\EcommerceController@ctabutton');
// ecommerce

// flock
Route::get('/flock', 'Flock\FlockController@home');
Route::match(['get','post'],'flock/workflowSent', 'Flock\FlockController@workflowSent');
Route::match(['get','post'],'flock/definations','Flock\FlockController@definations');
Route::get('flock/fetch-view', 'Flock\FlockController@fetch_view');
Route::get('flock/settings-hub/{id}', 'Flock\FlockController@settings_hub');
Route::post('flock/settings-hub', 'Flock\FlockController@POST_settings_hub');
Route::get('flock/terms-service', 'Flock\FlockController@terms_service');
Route::get('flock/policy', 'Flock\FlockController@policy');
Route::get('flock/documentation', 'Flock\FlockController@documentation');
Route::get('flock/tagline', 'Flock\FlockController@tagline');
Route::get('flock/auth2-callback', 'Flock\FlockController@auth2_callback');
// flock
Route::get('whmcs-ecommerce-admin', 'WHMCS\WHMCSController@whmcs_ecommerce_admin');
Route::post('whmcs-ecommerce', 'WHMCS\WHMCSController@whmcs_ecommerce');
Route::get('whmcs-ecommerce', 'WHMCS\WHMCSController@whmcs_ecommerces');

Route::group(['middleware' => 'auth'], function() {
	
	// common
	Route::get('/auth', 'HomeController@auth');
	// common
	
	// whmcs
	Route::get('sync-data', 'WHMCS\WHMCSController@sync_data');
	Route::get('whmcs-products', 'WHMCS\WHMCSController@whmcs_products');
	Route::get('whmcs-contacts', 'WHMCS\WHMCSController@whmcs_contacts');
	Route::get('whmcs-orders', 'WHMCS\WHMCSController@whmcs_orders');
	//Route::get('whmcs-ecommerce', 'WHMCS\WHMCSController@whmcs_ecommerce');
	// whmcs
	
	Route::group(['middleware' => 'auth_token'], function() {
		// common
		Route::get('/', 'SMS\SMSController@index');
		Route::get('edit-profile','ProfileController@edit_profile');
		Route::post('update_profile','ProfileController@store_profile');
		Route::get('/home', 'HomeController@home');
		Route::get('/all-app', 'HomeController@all_app');

		Route::post('stripe-payment', 'PaymentController@handlePost');
		// common

		// smsdew
		Route::get('/smsdew/settings', 'SMS\SMSController@settings');
		Route::post('/smsdew/settings', 'SMS\SMSController@POST_settings');
		// smsdew

		// whmcs
		Route::get('/whmcs/settings', 'WHMCS\WHMCSController@settings');
		Route::post('/whmcs/settings', 'WHMCS\WHMCSController@POST_settings');
		// whmcs

		// ecommerce
		Route::get('/ecommerce/settings', 'Ecommerce\EcommerceController@settings');
		Route::post('/ecommerce/settings', 'Ecommerce\EcommerceController@POST_settings');
		//ecommerce
	});
});

