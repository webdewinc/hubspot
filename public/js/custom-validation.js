$(document).ready(function(){
    var form_id = $('form').attr('id');
    var url = $('.url').val();   
		$('#'+form_id).validate({
		  ignore: [],
        debug: false,
        rules:{   	
            "email":{
                required:true,
                email:true,
                remote: url+ "/" +"checkEmail",
            },            
            "email_edit":{
                required:true,
                email:true,
            },            
            password : {
                minlength : 6
            },
				
            confirm_password : {
                minlength : 6,
                equalTo : "#password"
            }
        },
        messages:{
        		
            "email":{                
                remote:"This email has been already entered."
            },
            content:{
                        required:"This field is required.",
                        minlength:"Please enter minimum 10 characters"
            },
            password :{
                minlength:"Password should be minimum 6 character",
            },
            confirm_password : {
                minlength:"Password should be minimum 6 character",
                equalTo:"Confirm Password should be same with new password",
            }
        },
        errorPlacement: function(error, element) {            
            if( $( ".input-group" ).hasClass( element.attr("name") ) )
                error.insertAfter("."+element.attr("name"));
            else if( $( ".input-group1" ).hasClass( element.attr("name") ) )
                error.insertAfter("."+element.attr("name"));            
            else if( $( ".input-group1" ).hasClass( element.attr("id") ) )
                error.insertAfter("#"+element.attr("id"));            
            else if( $( ".form-group" ).hasClass( element.attr("name") ) )
                error.insertAfter("."+element.attr("name"));
            else if( $( ".form-group" ).hasClass( element.attr("id") ) )
                error.insertAfter("."+element.attr("id"));
            else if( $( ".form-control" ).hasClass( element.attr("name") ) )
                error.insertAfter("."+element.attr("name"));  
            else if( $( ".file-btn" ).hasClass( element.attr("name") ) )
                error.insertAfter("."+element.attr("name"));  
                        
            else
                error.insertAfter();
        },
    });
});
