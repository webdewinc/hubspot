@extends('layout.chat')
@section('title', 'Chat Section - FLY.BIZ')
@section('title-description')

<meta name="description" content="In a chat section, you can directly chat with any of the team members of FLY.BIZ regarding your instant doubts about how to add websites in FLY.BIZ, what's the price of the website and so on.">
    
@endsection
@section('content')
@section('customCSS')
@endsection

<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

        <!-- begin:: Subheader -->
        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
                
            </div>
        </div>
        <!-- end:: Subheader -->

        <!-- begin:: Content -->
        <div class="kt-container  kt-grid__item kt-grid__item--fluid">

            <!--Begin::App-->
            <div class="kt-grid kt-grid--desktop kt-grid--ver kt-grid--ver-desktop kt-app">

                <!--Begin:: App Aside Mobile Toggle-->
                <button class="kt-app__aside-close" id="kt_chat_aside_close">
                    <i class="la la-close"></i>
                </button>

                <!--End:: App Aside Mobile Toggle-->

                <!--Begin:: App Aside-->
                <div class="kt-grid__item kt-app__toggle kt-app__aside kt-app__aside--lg kt-app__aside--fit" id="kt_chat_aside">

                    <!--begin::Portlet-->
                    <div class="kt-portlet kt-portlet--last">
                        <div class="kt-portlet__body">
                            {{--
						<div class="kt-input-icon kt-input-icon--right kt-subheader__search">
                                <input type="text" class="form-control" placeholder="Search Website" id="chatroom_search">
                                <span class="kt-input-icon__icon kt-input-icon__icon--right">
                                    <span>
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                <rect x="0" y="0" width="24" height="24"></rect>
                                                <path d="M14.2928932,16.7071068 C13.9023689,16.3165825 13.9023689,15.6834175 14.2928932,15.2928932 C14.6834175,14.9023689 15.3165825,14.9023689 15.7071068,15.2928932 L19.7071068,19.2928932 C20.0976311,19.6834175 20.0976311,20.3165825 19.7071068,20.7071068 C19.3165825,21.0976311 18.6834175,21.0976311 18.2928932,20.7071068 L14.2928932,16.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"></path>
                                                <path d="M11,16 C13.7614237,16 16,13.7614237 16,11 C16,8.23857625 13.7614237,6 11,6 C8.23857625,6 6,8.23857625 6,11 C6,13.7614237 8.23857625,16 11,16 Z M11,18 C7.13400675,18 4,14.8659932 4,11 C4,7.13400675 7.13400675,4 11,4 C14.8659932,4 18,7.13400675 18,11 C18,14.8659932 14.8659932,18 11,18 Z" fill="#000000" fill-rule="nonzero"></path>
                                            </g>
                                        </svg>
                                        <!--<i class="flaticon2-search-1"></i>-->
                                    </span>
                                </span>
						</div>--}}
                            <div class="kt-widget kt-widget--users kt-mt-20">

                                <div class="kt-scroll kt-scroll--pull" style="height: 595px;">
                                    <div class="kt-widget__items" id="chat-append-search">
                                        @include('chat.chat-render-search-channel-ajax')
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--end::Portlet-->
                </div>

                <!--End:: App Aside-->

                <!--Begin:: App Content-->
                <div class="kt-grid__item kt-grid__item--fluid kt-app__content" id="kt_chat_content">
                    <div class="kt-chat">
                        <div class="kt-portlet kt-portlet--head-lg kt-portlet--last" id="chat-append">
                            <!-- render function -->
                            <div class="kt-portlet__body">
                                <div class="kt-scroll kt-scroll--pull d-flex justify-content-center align-items-center" id="input-div" data-mobile-height="300" style="height: 615px;">
                                    <div class="kt-chat__messages">
                                        {{-- @include('chat.chat-render') --}}
                                        <div class="alert alert-primary fade show" role="alert">                                                                 <h5 class="alert-text kt-margin-0">Please select chat from sidebar.</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- render function -->
                            
                        </div>
                    </div>
                </div>
                <!--End:: App Content-->
            </div>

        <!--End::App-->
        </div>        

        <!-- end:: Content -->

    @section('customScript')

    <script type="text/javascript">            
            //$(document).ready(function(){
                //$('.chatroom').click(function(e){
                $(document).on('click','.chatroom',function(e){
                    $(".loader").fadeIn("slow");
                    $('div.kt-widget__item').removeClass('active-bg');
                    //$(this).child().addClass('active-bg');
                    $(this).find('div.kt-widget__item').toggleClass('active-bg');
                    e.preventDefault();
                    var chatroom    = $(this).attr('data-chatroom');
                    var authUser    = $(this).attr('data-auth-user');
                    var otherUser    = $(this).attr('data-other-user');
                    var domain_id    = $(this).attr('data-domain-id');

                    
                    $.ajax({
                         headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                          },
                           beforeSend: function() {
                                // setting a timeout
                                $(".loader").fadeIn("slow");
                            },
                          url  : "{{ url('/chat-render') }}",
                          type : 'POST',
                          data: {
                                'chatroom' : chatroom,
                                'authUser' : authUser,
                                'otherUser' : otherUser,
                                'domain_id' : domain_id
                             },
                          async: true,
                          //dataType: 'json',
                          enctype: 'multipart/form-data',
                          cache: false,                      
                          success: function(response){
                            $('#chat-append-search').html(response.channel);
                            $('#chat-append').html(response.chat);
                            $('#partials-profile').html(response.profileFull);
                            //$('#chat-append').html(response);
                            run();
                          },
                          error: function(){}
                        });
                        e.stopImmediatePropagation();
                        return false; 

                });
            //});
        </script>
        <script type="text/javascript">
            
            $(document).on('keyup','#chatroom_search', function(e){
                //$(".loader").fadeIn("slow");
                //e.preventDefault();
                var val_search    = $(this).val();
                
                $.ajax({
                     headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                      },
                       beforeSend: function() {
                            // setting a timeout
                            //$(".loader").fadeIn("slow");
                        },
                      url  : "{{ url('/chat-render-search-ajax') }}",
                      type : 'POST',
                      data: {
                            'val_search' : val_search,
                         },
                      //async: false,
                      //dataType: 'json',
                      //enctype: 'multipart/form-data',
                      cache: false,                      
                      success: function(response){
                        $('#chat-append-search').html(response);
                        
                      },
                      error: function(){}
                    });
                    e.stopImmediatePropagation();
                    return false; 
            });

        </script>


<!--    <script href="{{ asset('') . config('app.public_url') . '/assets/js/pages/custom/chat/chat.js'  }}" type="text/javascript"></script> -->

    <script type="text/html" id="channel-template">
      <div class="col-md-12">
        <p class="channel-element" data-content="channelName"></p>
      </div>
    </script>
    <script type="text/html" id="member-notification-template">
      <p class="member-status" data-content="status"></p>
    </script>    
    
    <!-- JavaScript -->
    <script src="//code.jquery.com/jquery-2.2.0.min.js"></script>
    
    <script src="{{ asset('') . config('app.public_url') . '/js/vendor/jquery-throttle.min.js' }}"></script>
    <script src="{{ asset('') . config('app.public_url') . '/js/vendor/jquery.loadTemplate-1.4.4.min.js' }}"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.1/moment.min.js"></script>
    <!-- Twilio Common helpers and Twilio Chat JavaScript libs from CDN. -->
    <script src="//media.twiliocdn.com/sdk/js/common/v0.1/twilio-common.min.js"></script>
    <script src="//media.twiliocdn.com/sdk/js/chat/v3.0/twilio-chat.min.js"></script>
    <script type="text/javascript">
    function run(){


        var twiliochat = (function() {
        var tc = {};

        var GENERAL_CHANNEL_UNIQUE_NAME = $('#roomname').val();

        var GENERAL_CHANNEL_NAME = $('#roomnameview').val();
        var MESSAGES_HISTORY_LIMIT = 50;

        var $channelList;
        var $inputText;
        var $usernameInput;
        var $statusRow;
        var $connectPanel;
        var $newChannelInputRow;
        var $newChannelInput;
        var $typingRow;
        var $typingPlaceholder;

        $(document).ready(function() {
            tc.$messageList = $('#message-list');
            $channelList = $('#channel-list');
            $inputText = $('#input-text');
            $usernameInput = $('#username-input');
            $statusRow = $('#status-row');
            
            $connectPanel = $('#connect-panel');
            $newChannelInputRow = $('#new-channel-input-row');
            $newChannelInput = $('#new-channel-input');
            $typingRow = $('#typing-row');
            $typingPlaceholder = $('#typing-placeholder');
            $usernameInput.focus();
            $usernameInput.on('keypress', handleUsernameInputKeypress);
            
            $inputText.on('keypress', handleInputTextKeypress);
            $('#input-button').on('click', handleInputTextKeypress);

            $newChannelInput.on('keypress', tc.handleNewChannelInputKeypress);
            $('#connect-image').on('click', connectClientWithUsername);
            $('#add-channel-image').on('click', showAddChannelInput);
            $('#leave-span').on('click', disconnectClient);
            $('#delete-channel-span').on('click', deleteCurrentChannel);
        });
        connectClientWithUsername();

        function getUsername(author) {
            var str = author;
            //var res = str.split("_");
            //return res[0];
            return str;
        }

        function handleUsernameInputKeypress(event) {
            if (event.keyCode === 13){
                connectClientWithUsername();
            }
        }

        function handleInputTextKeypress(event) {
            var sender = $('#auth').val();
            var receiver = $('#user_id').val();
            var channelid = tc.currentChannel.sid;            
            var msg = $.trim($('#input-text').val());
            if(msg == '') {
                $('label.error').text('Please enter a valid details.');
                return;
            } 
            $('label.error').text('');
            if ((event.keyCode === 13 || event.type == 'click') && msg != '') {
                tc.currentChannel.sendMessage($('#input-text').val());
                event.preventDefault();
                $('#input-text').val('');
                var chatChannelId = $('#chatChannelId').val();
                var roomname=$('#roomname').val();
                var date= dateFormatter.getTodayDate();
                $.post("{{url('/storemsg')}}", {chatChannelId:chatChannelId,sender:sender,receiver:receiver,channelid:channelid,msg:msg,roomname:roomname,date:date}, null, 'json')
                    .done(function(response) {
                    })
                    .fail(function(error) {
                        alert('error saving data');
                        console.log('Failed to fetch the Access Token with error: ' + error);
                    });
            }
            else {
                notifyTyping();
            }
        }

        var notifyTyping = $.throttle(function() {
            tc.currentChannel.typing();
        }, 1000);

        tc.handleNewChannelInputKeypress = function(event) {
            if (event.keyCode === 13) {
            tc.messagingClient.createChannel({
                friendlyName: $newChannelInput.val()
            }).then(hideAddChannelInput);
            $(this).val('');
            event.preventDefault();
            }
        };

        function connectClientWithUsername() {
            var usernameText = $usernameInput.val();
            $usernameInput.val('');
            if (usernameText == '') {
                alert('Username cannot be empty');
                return;
            }
            tc.username = usernameText;
            fetchAccessToken(tc.username, connectMessagingClient);
        }

        function fetchAccessToken(username, handler) {
            
            $.post("{{url('/token')}}", {identity: username, device: 'browser'}, null, 'json')
            .done(function(response) {
                handler(response.token);
            })
            .fail(function(error) {
                console.log('Failed to fetch the Access Token with error: ' + error);
            });
        }

        function connectMessagingClient(token) {
            // Initialize the Chat messaging client
            
            tc.accessManager = new Twilio.AccessManager(token);
            Twilio.Chat.Client.create(token).then(function(client) {
            tc.messagingClient = client;
            updateConnectedUI();
            
            tc.loadChannelList(tc.joinGeneralChannel);
            tc.messagingClient.on('channelAdded', $.throttle(tc.loadChannelList));
            tc.messagingClient.on('channelRemoved', $.throttle(tc.loadChannelList));
            tc.messagingClient.on('tokenExpired', refreshToken);
            });
        }

        function refreshToken() {
            fetchAccessToken(tc.username, setNewToken);
        }

        function setNewToken(tokenResponse) {
            tc.accessManager.updateToken(tokenResponse.token);
        }

        function updateConnectedUI() {
            $('#username-span').text(tc.username);
            $statusRow.addClass('connected').removeClass('disconnected');
            tc.$messageList.addClass('connected').removeClass('disconnected');
            $connectPanel.addClass('connected').removeClass('disconnected');
            $inputText.addClass('with-shadow');
            $typingRow.addClass('connected').removeClass('disconnected');
        }

        tc.loadChannelList = function(handler) {
            if (tc.messagingClient === undefined) {
            console.log('Client is not initialized');
            return;
            }

            tc.messagingClient.getPublicChannelDescriptors().then(function(channels) {
            tc.channelArray = tc.sortChannelsByName(channels.items);
            $channelList.text('');
            tc.channelArray.forEach(addChannel);
            if (typeof handler === 'function') {
                handler();
            }
            });
        };

        tc.joinGeneralChannel = function() {
            console.log('Attempting to join "general" chat channel...');
            if (!tc.generalChannel) {
                //If it doesn't exist, let's create it
                tc.messagingClient.createChannel({
                    uniqueName: GENERAL_CHANNEL_UNIQUE_NAME,
                    friendlyName: GENERAL_CHANNEL_NAME
                }).then(function(channel) {

                    console.log('Created general channel');
                    tc.generalChannel = channel;
                    tc.loadChannelList(tc.joinGeneralChannel);
                });

                
            }
            else {
            console.log('Found general channel:');
            setupChannel(tc.generalChannel);
            }
        };

        function initChannel(channel) {
            console.log('Initialized channel ' + channel.friendlyName);
            return tc.messagingClient.getChannelBySid(channel.sid);
        }

        function joinChannel(_channel) {
            
                return _channel.join()
                .then(function(joinedChannel) {
                    console.log('Joined channel ' + joinedChannel.friendlyName);
                    updateChannelUI(_channel);
                    tc.currentChannel = _channel;
                    tc.loadMessages();
                    return joinedChannel;
                });
            
        }

        function initChannelEvents() {
            console.log(tc.currentChannel.friendlyName + ' ready.');
            tc.currentChannel.on('messageAdded', tc.addMessageToList);
            tc.currentChannel.on('typingStarted', showTypingStarted);
            tc.currentChannel.on('typingEnded', hideTypingStarted);
            tc.currentChannel.on('memberJoined', notifyMemberJoined);
            tc.currentChannel.on('memberLeft', notifyMemberLeft);
            $inputText.prop('disabled', false).focus();

            $(".loader").fadeOut("slow");
        }

        function setupChannel(channel) {
            return leaveCurrentChannel()
            .then(function() {
                
                return initChannel(channel);
            })
            .then(function(_channel) {

                return joinChannel(_channel);
            })
            .then(initChannelEvents);
        }

        tc.loadMessages = function() {
            //tc.currentChannel.getMessages(MESSAGES_HISTORY_LIMIT).then(function (messages) {
           // messages.items.forEach(tc.addMessageToList);
            //});
			scrollToMessageListBottom();
        };

        function leaveCurrentChannel() {

            if (tc.currentChannel) {
                
            return tc.currentChannel.leave().then(function(leftChannel) {
                console.log('left ' + leftChannel.friendlyName);
                leftChannel.removeListener('messageAdded', tc.addMessageToList);
                leftChannel.removeListener('typingStarted', showTypingStarted);
                leftChannel.removeListener('typingEnded', hideTypingStarted);
                leftChannel.removeListener('memberJoined', notifyMemberJoined);
                leftChannel.removeListener('memberLeft', notifyMemberLeft);
            });
            } else {
                
                return Promise.resolve();
            }
        }

        tc.addMessageToList = function(message) {
            
            var username = getUsername(message.author);
            
            if (message.author === tc.username) {

                var rowDiv = $('<div>').addClass('kt-chat__message kt-chat__message--right');
                rowDiv.loadTemplate($('#message-template-right'), {
                username: message.author,
                date: dateFormatter.getTodayDate(message.timestamp),
                body: message.body
                });
                rowDiv.addClass('own-message');
                
            } else {

                var rowDiv = $('<div>').addClass('kt-chat__message');
                rowDiv.loadTemplate($('#message-template'), {
                username: username,
                date: dateFormatter.getTodayDate(message.timestamp),
                body: message.body
                });
                rowDiv.addClass('own-message');
                
            }

            tc.$messageList.append(rowDiv);
            scrollToMessageListBottom();
        };

        function notifyMemberJoined(member) {
            notify(member.identity + ' joined the channel')
        }

        function notifyMemberLeft(member) {
            notify(member.identity + ' left the channel');
        }

        function notify(message) {
            var row = $('<div>').addClass('col-md-12');
            row.loadTemplate('#member-notification-template', {
            status: message
            });
            tc.$messageList.append(row);
            scrollToMessageListBottom();
        }

        function showTypingStarted(member) {
            $typingPlaceholder.text(member.identity + ' is typing...');
        }

        function hideTypingStarted(member) {
            $typingPlaceholder.text('');
        }

        function scrollToMessageListBottom() {
			var height = $('#message-list').height();
			$('.kt-scroll').scrollTop(height);
            //tc.$messageList.scrollTop(tc.$messageList[0].scrollHeight);
        }

        function updateChannelUI(selectedChannel) {
            var channelElements = $('.channel-element').toArray();
            var channelElement = channelElements.filter(function(element) {
            return $(element).data().sid === selectedChannel.sid;
            });
            channelElement = $(channelElement);
            if (tc.currentChannelContainer === undefined && selectedChannel.uniqueName === GENERAL_CHANNEL_UNIQUE_NAME) {
            tc.currentChannelContainer = channelElement;
            }
            tc.currentChannelContainer.removeClass('selected-channel').addClass('unselected-channel');
            channelElement.removeClass('unselected-channel').addClass('selected-channel');
            tc.currentChannelContainer = channelElement;
        }

        function showAddChannelInput() {
            if (tc.messagingClient) {
            $newChannelInputRow.addClass('showing').removeClass('not-showing');
            $channelList.addClass('showing').removeClass('not-showing');
            $newChannelInput.focus();
            }
        }

        function hideAddChannelInput() {
            $newChannelInputRow.addClass('not-showing').removeClass('showing');
            $channelList.addClass('not-showing').removeClass('showing');
            $newChannelInput.val('');
        }

        function addChannel(channel) {
           
            if (channel.uniqueName === GENERAL_CHANNEL_UNIQUE_NAME) {
                tc.generalChannel = channel;
            }
           
            var rowDiv = $('<div>').addClass('row channel-row');
            rowDiv.loadTemplate('#channel-template', {
            channelName: channel.friendlyName
            });

            var channelP = rowDiv.children().children().first();
            
            rowDiv.on('click', selectChannel);
            channelP.data('sid', channel.sid);
            if (tc.currentChannel && channel.sid === tc.currentChannel.sid) {
            tc.currentChannelContainer = channelP;
            channelP.addClass('selected-channel');

            }
            else {
            channelP.addClass('unselected-channel')
            }

            $channelList.append(rowDiv);
        }

        function deleteCurrentChannel() {
            if (!tc.currentChannel) {
            return;
            }
            if (tc.currentChannel.sid === tc.generalChannel.sid) {
            alert('You cannot delete the general channel');
            return;
            }
            tc.currentChannel.delete().then(function(channel) {
            console.log('channel: '+ channel.friendlyName + ' deleted');
            setupChannel(tc.generalChannel);
            });
        }

        function selectChannel(event) {
            var target = $(event.target);
            var channelSid = target.data().sid;
            var selectedChannel = tc.channelArray.filter(function(channel) {
            return channel.sid === channelSid;
            })[0];
            if (selectedChannel === tc.currentChannel) {
            return;
            }
            setupChannel(selectedChannel);
        };

        function disconnectClient() {
            leaveCurrentChannel();
            $channelList.text('');
            tc.$messageList.text('');
            channels = undefined;
            $statusRow.addClass('disconnected').removeClass('connected');
            tc.$messageList.addClass('disconnected').removeClass('connected');
            $connectPanel.addClass('disconnected').removeClass('connected');
            $inputText.removeClass('with-shadow');
            $typingRow.addClass('disconnected').removeClass('connected');
        }

        tc.sortChannelsByName = function(channels) {
            return channels.sort(function(a, b) {
            if (a.friendlyName === GENERAL_CHANNEL_NAME) {
                return -1;
            }
            if (b.friendlyName === GENERAL_CHANNEL_NAME) {
                return 1;
            }
            return a.friendlyName.localeCompare(b.friendlyName);
            });
        };

        return tc;
        })();


		// Initialize the plugin
        const demo = document.querySelector('#input-div');
        const ps = new PerfectScrollbar(demo);

       
       }
    </script>
    <script src="{{ asset('') . config('app.public_url') . '/js/dateformatter.js' }}"></script>
	
	<script type="text/javascript">
        $(window).load(function(){
            $('.loader').fadeOut('slow');
        });

    </script>

    @endsection
    </div>
</div>
@endsection
