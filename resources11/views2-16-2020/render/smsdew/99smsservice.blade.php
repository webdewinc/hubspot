<?php
$vendor = vendor_select('99smsservice');
?>
<div><a href="javascript:void(0);" title="Webdew Groups" ><strong id="select-vendor-append">{{$vendor['name']}}</strong></a> <a href="https://docs.msg91.com/collection/msg91-api-integration/5/pages/139" target="_blank" hidden style="float:right;">Api Docs</a>    </div>     
     
@csrf
<div class="kt-wizard-v2__form">
        
    <?php
        if(empty($data)){
            $button = 'Save';
        } else {
            $button = 'Update';
        }
    ?>
    <div class="form-row">
        <div class="form-group col-md-6 col-12">
            <label class="required">{{$vendor['label']['apikey_ct']}}</label>
            <div class="input-group">
                <input class="form-control" type="text" id="apikey_ct" name="apikey_ct" value="{{@$data['apikey_ct']}}" minlength="15" maxlength="50" required="" autocomplete="off">
            </div>
        </div>
			<div class="form-group col-md-6 col-12">
				<label class="required">{{$vendor['label']['from']}}</label>
				<div class="input-group">
					<input class="form-control" type="text" id="from" name="from" value="{{@$data['from']}}" required="" minlength="3" maxlength="10" autocomplete="off">
				</div>
			</div>
    </div>     
    
        <div class="form-row">
          <div class="form-group col-md-6 col-12">
            <label class="required">{{$vendor['label']['t_hapi_key']}}</label>
            <div class="input-group">
                <input class="form-control" type="text" id="t_hapi_key" name="t_hapi_key" value="{{@$data['t_hapi_key']}}" required="" minlength="10"  autocomplete="off">
            </div>
        </div> 

        </div>
   

   
    <div class="form-group row">
        <div class="col-md-6 col-12">            
            <div class="input-group">
                <!--<input type="checkbox" id="default" name="default" value="msg91" @if(@$type == 'msg91'){{'checked'}}@endif> <label>Set as default</label>-->
				<label class="kt-checkbox">
				   <input type="checkbox" id="default" name="default" value="99smsservice" @if(@$type == '99smsservice'){{'checked'}}@endif> Set as default
					<span></span>
				</label>
            </div>
        </div>
    </div>
    <div class="kt-form__actions">
        <button class="btn btn-label-brand btn-bold"  type="submit" data-ktwizard-type="action-submit">
            {{$button}}
        </button>
    </div>
</div>