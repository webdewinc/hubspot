<?php
$vendor = vendor_select('d7sms');
?>
<p><a href="javascript:void(0);" title="Webdew Groups" ><strong id="select-vendor-append">{{$vendor['name']}}</strong></a>
<a href="https://d7networks.com/docs/apis/http/index.html#sending-sms"  hidden target="_blank" style="float:right;">Api Docs</a>
</p>             
@csrf
<div class="kt-wizard-v2__form">
        
    <?php
        if(empty($data)){
            $button = 'Save';
        } else {
            $button = 'Update';
        }
    ?>
    <div class="form-row">
        <div class="form-group col-md-6 col-12">
            <label class="required">{{$vendor['label']['username']}}</label>
            <div class="input-group">
                <input class="form-control" type="text" id="username" name="username" value="{{@$data['username']}}" required="" autocomplete="off">
            </div>
        </div>
       
		
    <div class="form-group col-md-6 col-12">
            <label class="required">{{$vendor['label']['password']}}</label>
            <div class="input-group">
                <input class="form-control" type="password" id="password" name="password" value="{{@$data['password']}}" required=""   autocomplete="off">
            </div>
        </div>
    </div>
   
   
       <div class="form-row">
	
    <div class="form-group col-md-6 col-12">
            <label class="required">{{$vendor['label']['t_hapi_key']}}</label>
            <div class="input-group">
                <input class="form-control" type="text" id="t_hapi_key" name="t_hapi_key" value="{{@$data['t_hapi_key']}}" required="" minlength="10"  autocomplete="off">
            </div>
        </div>
    </div>
   
   
  
		 <div class="form-group col-md-6 col-12">            
            <div class="input-group">
                
				<label class="kt-checkbox">
				   <input type="checkbox" id="default" name="default" value="d7sms" @if(@$type == 'd7sms'){{'checked'}}@endif> Set as default
					<span></span>
				</label>
            </div>
        </div>
   
   
    <div class="kt-form__actions">
        <button class="btn btn-label-brand btn-bold"  type="submit" data-ktwizard-type="action-submit">
            {{$button}}
        </button>
    </div>
</div>