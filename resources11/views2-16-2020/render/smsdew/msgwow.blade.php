<?php
$vendor = vendor_select('msgwow');
?>
<div><a href="javascript:void(0);" title="Webdew Groups" ><strong id="select-vendor-append">{{$vendor['name']}}</strong></a>
 <a href="http://my.msgwow.com/index.php" target="_blank" hidden style="float:right;">Api Docs</a></div>             
@csrf
<div class="kt-wizard-v2__form">
        
    <?php
        if(empty($data)){
            $button = 'Save';
        } else {
            $button = 'Update';
        }
    ?>
    <div class="form-row">
        <div class="vform-group col-md-6 col-12">
            <label class="required">{{$vendor['label']['msgwow']}}</label>
            <div class="input-group">
                <input class="form-control" type="text" id="msgwow" name="msgwow" value="{{@$data['msgwow']}}" minlength="15" maxlength="50" required="" autocomplete="off">
            </div>
        </div>
		<div class="form-group col-md-6 col-12">
            <label class="required">{{$vendor['label']['routes']}}</label>
            <div class="input-group">
                <select class="form-control" id="routes" name="routes" required="">
                    <option value=""></option>
                    <option value="1" @if(@$data['routes'] == 1) {{'selected'}} @endif>{{$vendor['routes'][1]}}</option>
                    <option value="4" @if(@$data['routes'] == 4) {{'selected'}} @endif>{{$vendor['routes'][4]}}</option>
                </select>
            </div>
        </div>
    </div>     
   
    <div class="form-row">
        <div class="form-group col-md-6 col-12">
            <label class="required">{{$vendor['label']['country_code']}}</label>
            <div class="input-group">
                <select class="form-control" type="text" id="country_code" name="country_code" required="">
                    <option value=""></option>
                    <option value="0" @if(@$data['country_code'] == 0) {{'selected'}} @endif>{{$vendor['country_code']['0']}}</option>
                    <option value="1" @if(@$data['country_code'] == 1) {{'selected'}} @endif>{{$vendor['country_code']['1']}}</option>
                    <option value="91" @if(@$data['country_code'] == 91) {{'selected'}} @endif>{{$vendor['country_code']['91']}}</option>
                </select>
            </div>
        </div>
		 <div class="form-group col-md-6 col-12">
            <label class="required">{{$vendor['label']['sender']}}</label>
            <div class="input-group">
                <input class="form-control" type="text" id="sender" name="sender" value="{{@$data['sender']}}" required="" autocomplete="off" minlength="3" maxlength="10">
            </div>
        </div>
    </div>
    
       <div class="form-row">
          <div class="form-group col-md-6 col-12">
            <label class="required">{{$vendor['label']['t_hapi_key']}}</label>
            <div class="input-group">
                <input class="form-control" type="text" id="t_hapi_key" name="t_hapi_key" value="{{@$data['t_hapi_key']}}" required="" minlength="10"  autocomplete="off">
            </div>
        </div>
	 </div>
   
    <div class="form-group row">
        <div class="col-md-6 col-12">            
            <div class="input-group">
                
				<label class="kt-checkbox">
				   <input type="checkbox" id="default" name="default" value="msgwow" @if(@$type == 'msgwow'){{'checked'}} @endif> Set as default
					<span></span>
				</label>
            </div>
        </div>
    </div>
    <div class="kt-form__actions">
        <button class="btn btn-label-brand btn-bold"  type="submit" data-ktwizard-type="action-submit">
            {{$button}}
        </button>
    </div>
</div>