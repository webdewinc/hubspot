<link href="{{url('/public/assets/css/webdew-product.css')}}" rel="stylesheet">
{% set query = request.query_dict %}
{% set $product_id = query.product_id %}
{% set contact = crm_object("product", product_id) %}
{% set $contact_name = contact.name %}
{% set $contact_price = contact.price %}
<div class="card">
<p><button style="display: none;" data-obid="{{@$product_id}}" data-name="{{ @$contact_name }}" data-price="{{ @$contact_price }}" class="add_cart">Add to Cart Rs.{{ @$contact_price }}</button></p>
<input id="grandTotalValue" type="hidden">
</div>
<div class="cart-wrapper">
<div class="cart-panel"><header class="cart-header">
<div class="cart-tabs">
<ul class="cart-tab-table">
<li class="cart-tab-cell selected">
  <span class="cart-tab">
    <span class="tab-ic tab-cart-ic"></span>
    <span class="tab-label grand-total"></span>
  </span>
  </li>
</ul>
<span class="tab-selector"></span></div>
</header>
<div class="cart-content">
<div class="cart-sections">
<section class="cart-section nano">
<ul id="list" class="cart-list overthrow nano-content"></ul>
</section>
<section class="cart-section nano"></section>
</div>
</div>
<footer class="cart-footer">
<div class="cart-checkout-btn"><span class="checkout-cart-ic"></span> <span class="checkout-label checkout">Checkout</span></div>
<span class="cart-copyright">Powered by <a>Webdew</a></span></footer></div>
</div>
<script src="https://static.hsappstatic.net/jquery-libs/static-1.1/jquery/jquery-1.7.1.js"></script>
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script src="{{url('/public/assets/js/webdew-product.js')}}"></script>