@extends('layout.app')
@php
$totalTitle = 'Settings';
@endphp
@section('title', $totalTitle)
@section('title-description')
<meta name="description" content="Settings">
@endsection
@section('content')
@section('head')
@parent    
@show
<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor bg-white mt-5" id="kt_content">

        <!-- begin:: Content -->
        <div class="kt-container  kt-grid__item kt-grid__item--fluid home-container">
           
            <div class="row">
                <!-- body content start -->
                <div class="col-12">
                    <div class="kt_blog-post">
                        
                        <div class="row justify-content-center">
                            <div class="col-xl-8 col-lg-8 col-md-12">
                                
                                @if (!empty($alert))
                                <div class="alert alert-{{$alert}}">
                                    {{ $message }}
                                </div>
                                @endif
                                @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif

                                <div class="row">
                                    <div class="col">
                                        <div class="card">
                                            <form id="kt_form" name="kt_form" action="{{url('apps/ecommerce/settings')}}" method="POST">
                                                <div class="card-header">
                                                    <label>Settings</label>
                                                    <?php
                                                        if(!empty(DB::table('credentials')->where('type','ecommerce')->wherePortalId(Session::get('portal_id'))->value('data'))) {
                                                    ?>
                                                        <div class="float-right">
                                                            <a href="{{url('apps/ecommerce/sync-data')}}">Sync Data</a>
                                                        </div>
                                                    <?php
                                                        }
                                                    ?>
                                                </div>
                                                <div class="card-body" id="vendor-append">
                                                    @php
                                                        echo $html;
                                                    @endphp
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>                                  
                    </div>
                </div>
                <!-- body content end -->
            </div>
        </div>
    </div>                  
</div>

@section('scripts')
@parent

  <!--begin::Page Scripts(used by this page) -->
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
        <script type="text/javascript">
            "use strict";

            // Class definition
            var KTWizard2 = function () {
                // Base elements
                var wizardEl;
                var formEl;
                var validator;
                var wizard;

                var initValidation = function() {
                    validator = formEl.validate({
                        // Validate only visible fields
                        ignore: ":hidden",

                        // Validation rules
                        rules: {
                           
                            // routes :{
                            //     required: true,
                            //     maxlength: 3,
                            //     digits:true
                            // },
                            // country_code :{
                            //     required: true,
                            //     maxlength: 3,
                            //     digits:true
                            // },
                            // sender :{
                            //     required: true,
                            //     maxlength: 10,
                            // },
                            // MSG91 :{
                            //     required: true,
                            // }
                        },

                        // Display error
                        invalidHandler: function(event, validator) {
                            KTUtil.scrollTop();
                            swal.fire({
                                "title": "",
                                "text": "There are some errors in your submission. Please correct them.",
                                "type": "error",
                                "confirmButtonClass": "btn btn-secondary"
                            });
                        },

                        // Submit valid form
                        // submitHandler: function (form) {

                        // }
                    });
                }

                var initSubmit = function() {
                    var btn = formEl.find('[data-ktwizard-type="action-submit"]');

                    btn.on('click', function(e) {
                        e.preventDefault();

                        if (validator.form()) {
                            $('#kt_form').submit();     
                        }
                    });
                }

                return {
                    // public functions
                    init: function() {
                        wizardEl = KTUtil.get('kt_wizard_v2');
                        formEl = $('#kt_form');
                        initValidation();
                        initSubmit();
                    }
                };
            }();

            jQuery(document).ready(function() {
                KTWizard2.init();
                /** validation for guest example not to match with domain url **/
                jQuery.validator.addMethod("notEqual", function(value, element, param) {
                  return this.optional(element) || value != param;
                }, "Please specify a different value");
                /** end here code **/
                $.validator.addMethod('lessThanEqual', function(value, element, param) {
                    return this.optional(element) || parseInt(value) <= parseInt($(param).val());
                }, "The content cost must be less than number words");
            });

            jQuery(document).on('change','#type',function(e){
                    var vendor = $(this).val();
                    e.preventDefault();
                    $.ajax({
                         headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                          },
                           beforeSend: function() {
                                // setting a timeout
                                $(".loader").fadeIn("slow");
                            },
                          url  : "{{ url('/render-vendor') }}",
                          type : 'POST',
                          data: {
                                'vendor' : vendor,
                             },
                          async: true,
                          //dataType: 'json',
                          enctype: 'multipart/form-data',
                          cache: false,                      
                          success: function(response){
                            $('#vendor-append').html(response);
                            $(".loader").fadeOut("slow");
                          },
                          error: function(){}
                        });
                        e.stopImmediatePropagation();
                        return false; 

            });

        </script>     
@show
@endsection