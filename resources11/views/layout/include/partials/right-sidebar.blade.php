@if(Auth::user()->user_type != 'admin')
{{--	
	<a href="{{URL::asset('/home')}}" class="kt-notification__item">
		<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-mr-10">
			<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
				<rect x="0" y="0" width="24" height="24"></rect>
				<path d="M3,4 L20,4 C20.5522847,4 21,4.44771525 21,5 L21,7 C21,7.55228475 20.5522847,8 20,8 L3,8 C2.44771525,8 2,7.55228475 2,7 L2,5 C2,4.44771525 2.44771525,4 3,4 Z M10,10 L20,10 C20.5522847,10 21,10.4477153 21,11 L21,13 C21,13.5522847 20.5522847,14 20,14 L10,14 C9.44771525,14 9,13.5522847 9,13 L9,11 C9,10.4477153 9.44771525,10 10,10 Z M10,16 L20,16 C20.5522847,16 21,16.4477153 21,17 L21,19 C21,19.5522847 20.5522847,20 20,20 L10,20 C9.44771525,20 9,19.5522847 9,19 L9,17 C9,16.4477153 9.44771525,16 10,16 Z" fill="#000000"></path>
				<rect fill="#000000" opacity="0.3" x="2" y="10" width="5" height="10" rx="1"></rect>
			</g>
		</svg>
		<div class="kt-notification__item-details">
			<div class="kt-notification__item-title kt-font-bold">
				Home
			</div>
			<div class="kt-notification__item-time">
				Account settings and more
			</div>
		</div>
	</a>
	<a href="{{url('edit-profile')}}" class="kt-notification__item">
		<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-mr-10">
			<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
				<polygon points="0 0 24 0 24 24 0 24"/>
				<path d="M12,11 C9.790861,11 8,9.209139 8,7 C8,4.790861 9.790861,3 12,3 C14.209139,3 16,4.790861 16,7 C16,9.209139 14.209139,11 12,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
				<path d="M3.00065168,20.1992055 C3.38825852,15.4265159 7.26191235,13 11.9833413,13 C16.7712164,13 20.7048837,15.2931929 20.9979143,20.2 C21.0095879,20.3954741 20.9979143,21 20.2466999,21 C16.541124,21 11.0347247,21 3.72750223,21 C3.47671215,21 2.97953825,20.45918 3.00065168,20.1992055 Z" fill="#000000" fill-rule="nonzero"/>
			</g>
		</svg>
		<div class="kt-notification__item-details">
			<div class="kt-notification__item-title kt-font-bold">
				My Profile
			</div>
		</div>
	</a>
	--}}
	<?php
            $portal_id = Session::get('portal_id');
            
                       
//            $portal_id =\DB::table('users')->where('id',Auth::user()->id)->value('portal_id');
            $id = DB::table('payments')->where('user_id',Auth::user()->id)->where('app_id',1)->where('portal_id',$portal_id)->value('id');
	    	$exist = DB::table('mapped_apps')->where('user_id',Auth::user()->id)->where('fk_app_id',1)->value('mapped_app_id');

	    	$current_date=date("Y-m-d");
            $expiry_date=DB::table('payments')->where('app_type',"smsdew")->where('portal_id',$portal_id)->value('end_date');
            
            if($current_date<$expiry_date){
            	$status="Notexpired";
            }else{
            	$status="expired";
            }
		if(!empty($exist) && !empty($id) && $status=="Notexpired"){
	?>
	<a href="{{url('apps/all-in-one-sms/settings')}}" class="kt-notification__item">
		<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-mr-10">
			<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
				<rect x="0" y="0" width="24" height="24"></rect>
				<path d="M3,4 L20,4 C20.5522847,4 21,4.44771525 21,5 L21,7 C21,7.55228475 20.5522847,8 20,8 L3,8 C2.44771525,8 2,7.55228475 2,7 L2,5 C2,4.44771525 2.44771525,4 3,4 Z M10,10 L20,10 C20.5522847,10 21,10.4477153 21,11 L21,13 C21,13.5522847 20.5522847,14 20,14 L10,14 C9.44771525,14 9,13.5522847 9,13 L9,11 C9,10.4477153 9.44771525,10 10,10 Z M10,16 L20,16 C20.5522847,16 21,16.4477153 21,17 L21,19 C21,19.5522847 20.5522847,20 20,20 L10,20 C9.44771525,20 9,19.5522847 9,19 L9,17 C9,16.4477153 9.44771525,16 10,16 Z" fill="#000000"></path>
				<rect fill="#000000" opacity="0.3" x="2" y="10" width="5" height="10" rx="1"></rect>
			</g>
		</svg>
		<div class="kt-notification__item-details">
			<div class="kt-notification__item-title kt-font-bold">
				SMS Hub Settings
			</div>
		</div>
	</a>

	<a href="{{url('apps/all-in-one-sms/sms-logs')}}" class="kt-notification__item">
		<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-mr-10">
			<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
				<rect x="0" y="0" width="24" height="24"></rect>
				<path d="M3,4 L20,4 C20.5522847,4 21,4.44771525 21,5 L21,7 C21,7.55228475 20.5522847,8 20,8 L3,8 C2.44771525,8 2,7.55228475 2,7 L2,5 C2,4.44771525 2.44771525,4 3,4 Z M10,10 L20,10 C20.5522847,10 21,10.4477153 21,11 L21,13 C21,13.5522847 20.5522847,14 20,14 L10,14 C9.44771525,14 9,13.5522847 9,13 L9,11 C9,10.4477153 9.44771525,10 10,10 Z M10,16 L20,16 C20.5522847,16 21,16.4477153 21,17 L21,19 C21,19.5522847 20.5522847,20 20,20 L10,20 C9.44771525,20 9,19.5522847 9,19 L9,17 C9,16.4477153 9.44771525,16 10,16 Z" fill="#000000"></path>
				<rect fill="#000000" opacity="0.3" x="2" y="10" width="5" height="10" rx="1"></rect>
			</g>
		</svg>
		<div class="kt-notification__item-details">
			<div class="kt-notification__item-title kt-font-bold">
				SMS Logs
			</div>
		</div>
	</a>

	<?php
		}
	?>
	<?php
	// $exist = \DB::table('mapped_apps')->where('user_id',Auth::user()->id)->where('fk_app_id',2)->value('mapped_app_id');
	$exist = \DB::table('application_records')->where('portal_id',\Session::get('portal_id'))->where('app_id',2)->value('id');
	if(!empty($exist)) {

		$app_user_id = \App\Credential::where('type','whmcs')
                            ->where('portal_id', \Session::get('portal_id'))
                            ->value('user_id');

        ?>
        @if(!empty($app_user_id))
        	@if($app_user_id == Auth::user()->id) 		
				<a href="{{url('apps/whmcs/settings')}}" class="kt-notification__item">
					<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-mr-10">
						<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
							<rect x="0" y="0" width="24" height="24"></rect>
							<path d="M3,4 L20,4 C20.5522847,4 21,4.44771525 21,5 L21,7 C21,7.55228475 20.5522847,8 20,8 L3,8 C2.44771525,8 2,7.55228475 2,7 L2,5 C2,4.44771525 2.44771525,4 3,4 Z M10,10 L20,10 C20.5522847,10 21,10.4477153 21,11 L21,13 C21,13.5522847 20.5522847,14 20,14 L10,14 C9.44771525,14 9,13.5522847 9,13 L9,11 C9,10.4477153 9.44771525,10 10,10 Z M10,16 L20,16 C20.5522847,16 21,16.4477153 21,17 L21,19 C21,19.5522847 20.5522847,20 20,20 L10,20 C9.44771525,20 9,19.5522847 9,19 L9,17 C9,16.4477153 9.44771525,16 10,16 Z" fill="#000000"></path>
							<rect fill="#000000" opacity="0.3" x="2" y="10" width="5" height="10" rx="1"></rect>
						</g>
					</svg>


					<div class="kt-notification__item-details">
						<div class="kt-notification__item-title kt-font-bold">
							WHMCS Credentials
						</div>
					</div>
				</a>
			@endif
		@else 
			<a href="{{url('apps/whmcs/settings')}}" class="kt-notification__item">
					<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-mr-10">
						<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
							<rect x="0" y="0" width="24" height="24"></rect>
							<path d="M3,4 L20,4 C20.5522847,4 21,4.44771525 21,5 L21,7 C21,7.55228475 20.5522847,8 20,8 L3,8 C2.44771525,8 2,7.55228475 2,7 L2,5 C2,4.44771525 2.44771525,4 3,4 Z M10,10 L20,10 C20.5522847,10 21,10.4477153 21,11 L21,13 C21,13.5522847 20.5522847,14 20,14 L10,14 C9.44771525,14 9,13.5522847 9,13 L9,11 C9,10.4477153 9.44771525,10 10,10 Z M10,16 L20,16 C20.5522847,16 21,16.4477153 21,17 L21,19 C21,19.5522847 20.5522847,20 20,20 L10,20 C9.44771525,20 9,19.5522847 9,19 L9,17 C9,16.4477153 9.44771525,16 10,16 Z" fill="#000000"></path>
							<rect fill="#000000" opacity="0.3" x="2" y="10" width="5" height="10" rx="1"></rect>
						</g>
					</svg>


					<div class="kt-notification__item-details">
						<div class="kt-notification__item-title kt-font-bold">
							WHMCS Credentials
						</div>
					</div>
				</a>
		@endif
		<?php
			$data = \App\Credential::where('portal_id' , \Session::get('portal_id'))
	                                ->where('type','whmcs')
	                                ->value('data');
	        $data = @unserialize($data);
	        if(!empty($data)){
	        	if($app_user_id == Auth::user()->id) {	
		?>
		<a href="{{url('apps/whmcs/whmcs-pipeline-hubspot')}}" class="kt-notification__item">
			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-mr-10">
				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
					<rect x="0" y="0" width="24" height="24"></rect>
					<path d="M8,3 L8,3.5 C8,4.32842712 8.67157288,5 9.5,5 L14.5,5 C15.3284271,5 16,4.32842712 16,3.5 L16,3 L18,3 C19.1045695,3 20,3.8954305 20,5 L20,21 C20,22.1045695 19.1045695,23 18,23 L6,23 C4.8954305,23 4,22.1045695 4,21 L4,5 C4,3.8954305 4.8954305,3 6,3 L8,3 Z" fill="#000000" opacity="0.3"></path>
					<path d="M11,2 C11,1.44771525 11.4477153,1 12,1 C12.5522847,1 13,1.44771525 13,2 L14.5,2 C14.7761424,2 15,2.22385763 15,2.5 L15,3.5 C15,3.77614237 14.7761424,4 14.5,4 L9.5,4 C9.22385763,4 9,3.77614237 9,3.5 L9,2.5 C9,2.22385763 9.22385763,2 9.5,2 L11,2 Z" fill="#000000"></path>
					<rect fill="#000000" opacity="0.3" x="10" y="9" width="7" height="2" rx="1"></rect>
					<rect fill="#000000" opacity="0.3" x="7" y="9" width="2" height="2" rx="1"></rect>
					<rect fill="#000000" opacity="0.3" x="7" y="13" width="2" height="2" rx="1"></rect>
					<rect fill="#000000" opacity="0.3" x="10" y="13" width="7" height="2" rx="1"></rect>
					<rect fill="#000000" opacity="0.3" x="7" y="17" width="2" height="2" rx="1"></rect>
					<rect fill="#000000" opacity="0.3" x="10" y="17" width="7" height="2" rx="1"></rect>
				</g>
			</svg>
			<div class="kt-notification__item-details">
				<div class="kt-notification__item-title kt-font-bold">
					Pipeline
				</div>
			</div>
		</a>
		<?php
					}
			$ids = DB::table('whmcs_sync_pipelines')->where('portal_id' , \Session::get('portal_id'))
	                                ->pluck('id');
	        if(count($ids) > 0){
	        	
		?>
		<a href="{{url('apps/whmcs/import')}}" class="kt-notification__item">
			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-mr-10">
				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
					<rect x="0" y="0" width="24" height="24"></rect>
					<polygon fill="#000000" opacity="0.3" points="6 7 6 15 18 15 18 7"></polygon>
					<path d="M11,19 L11,16 C11,15.4477153 11.4477153,15 12,15 C12.5522847,15 13,15.4477153 13,16 L13,19 L14.5,19 C14.7761424,19 15,19.2238576 15,19.5 C15,19.7761424 14.7761424,20 14.5,20 L9.5,20 C9.22385763,20 9,19.7761424 9,19.5 C9,19.2238576 9.22385763,19 9.5,19 L11,19 Z" fill="#000000" opacity="0.3"></path>
					<path d="M6,7 L6,15 L18,15 L18,7 L6,7 Z M6,5 L18,5 C19.1045695,5 20,5.8954305 20,7 L20,15 C20,16.1045695 19.1045695,17 18,17 L6,17 C4.8954305,17 4,16.1045695 4,15 L4,7 C4,5.8954305 4.8954305,5 6,5 Z" fill="#000000" fill-rule="nonzero"></path>
				</g>
			</svg>
			<div class="kt-notification__item-details">
				<div class="kt-notification__item-title kt-font-bold">
					Import
				</div>
			</div>
		</a>

				<a href="{{url('apps/whmcs/whmcs-logs')}}" class="kt-notification__item">
			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-mr-10">
				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
					<rect x="0" y="0" width="24" height="24"></rect>
					<polygon fill="#000000" opacity="0.3" points="6 7 6 15 18 15 18 7"></polygon>
					<path d="M11,19 L11,16 C11,15.4477153 11.4477153,15 12,15 C12.5522847,15 13,15.4477153 13,16 L13,19 L14.5,19 C14.7761424,19 15,19.2238576 15,19.5 C15,19.7761424 14.7761424,20 14.5,20 L9.5,20 C9.22385763,20 9,19.7761424 9,19.5 C9,19.2238576 9.22385763,19 9.5,19 L11,19 Z" fill="#000000" opacity="0.3"></path>
					<path d="M6,7 L6,15 L18,15 L18,7 L6,7 Z M6,5 L18,5 C19.1045695,5 20,5.8954305 20,7 L20,15 C20,16.1045695 19.1045695,17 18,17 L6,17 C4.8954305,17 4,16.1045695 4,15 L4,7 C4,5.8954305 4.8954305,5 6,5 Z" fill="#000000" fill-rule="nonzero"></path>
				</g>
			</svg>
			<div class="kt-notification__item-details">
				<div class="kt-notification__item-title kt-font-bold">
					WHMCS Logs
				</div>
			</div>
		</a>
	<?php

			}
		}
	?>
	{{--
		<a href="{{url('apps/whmcs/automation-pricing')}}" class="kt-notification__item">
			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-mr-10">
				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
					<rect x="0" y="0" width="24" height="24"></rect>
					<path d="M8,3 L8,3.5 C8,4.32842712 8.67157288,5 9.5,5 L14.5,5 C15.3284271,5 16,4.32842712 16,3.5 L16,3 L18,3 C19.1045695,3 20,3.8954305 20,5 L20,21 C20,22.1045695 19.1045695,23 18,23 L6,23 C4.8954305,23 4,22.1045695 4,21 L4,5 C4,3.8954305 4.8954305,3 6,3 L8,3 Z" fill="#000000" opacity="0.3"></path>
					<path d="M11,2 C11,1.44771525 11.4477153,1 12,1 C12.5522847,1 13,1.44771525 13,2 L14.5,2 C14.7761424,2 15,2.22385763 15,2.5 L15,3.5 C15,3.77614237 14.7761424,4 14.5,4 L9.5,4 C9.22385763,4 9,3.77614237 9,3.5 L9,2.5 C9,2.22385763 9.22385763,2 9.5,2 L11,2 Z" fill="#000000"></path>
					<rect fill="#000000" opacity="0.3" x="10" y="9" width="7" height="2" rx="1"></rect>
					<rect fill="#000000" opacity="0.3" x="7" y="9" width="2" height="2" rx="1"></rect>
					<rect fill="#000000" opacity="0.3" x="7" y="13" width="2" height="2" rx="1"></rect>
					<rect fill="#000000" opacity="0.3" x="10" y="13" width="7" height="2" rx="1"></rect>
					<rect fill="#000000" opacity="0.3" x="7" y="17" width="2" height="2" rx="1"></rect>
					<rect fill="#000000" opacity="0.3" x="10" y="17" width="7" height="2" rx="1"></rect>
				</g>
			</svg>
			<div class="kt-notification__item-details">
				<div class="kt-notification__item-title kt-font-bold">
					Automation Pricing
				</div>
			</div>
		</a>
	--}}
		<?php
		
	}
	?>
	<?php

	$exist = \DB::table('application_records')->where('portal_id',\Session::get('portal_id'))->where('app_id',3)->value('id');
	if(!empty($exist)) {
		 $app_user_id = \App\Credential::where('type','ecommerce')
                            ->where('portal_id', \Session::get('portal_id'))
                            ->value('user_id');
	?>
		@if(!empty($app_user_id))
	        @if($app_user_id == Auth::user()->id) 
			 	<a href="{{url('apps/ecommerce/settings')}}" class="kt-notification__item">
					<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-mr-10">
						<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
							<rect x="0" y="0" width="24" height="24"></rect>
							<path d="M3,4 L20,4 C20.5522847,4 21,4.44771525 21,5 L21,7 C21,7.55228475 20.5522847,8 20,8 L3,8 C2.44771525,8 2,7.55228475 2,7 L2,5 C2,4.44771525 2.44771525,4 3,4 Z M10,10 L20,10 C20.5522847,10 21,10.4477153 21,11 L21,13 C21,13.5522847 20.5522847,14 20,14 L10,14 C9.44771525,14 9,13.5522847 9,13 L9,11 C9,10.4477153 9.44771525,10 10,10 Z M10,16 L20,16 C20.5522847,16 21,16.4477153 21,17 L21,19 C21,19.5522847 20.5522847,20 20,20 L10,20 C9.44771525,20 9,19.5522847 9,19 L9,17 C9,16.4477153 9.44771525,16 10,16 Z" fill="#000000"></path>
							<rect fill="#000000" opacity="0.3" x="2" y="10" width="5" height="10" rx="1"></rect>
						</g>
					</svg>
					<div class="kt-notification__item-details">
						<div class="kt-notification__item-title kt-font-bold">
							Ecommerce Settings
						</div>
					</div>
				</a>
			@endif
		@else
			<a href="{{url('apps/ecommerce/settings')}}" class="kt-notification__item">
				<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-mr-10">
					<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
						<rect x="0" y="0" width="24" height="24"></rect>
						<path d="M3,4 L20,4 C20.5522847,4 21,4.44771525 21,5 L21,7 C21,7.55228475 20.5522847,8 20,8 L3,8 C2.44771525,8 2,7.55228475 2,7 L2,5 C2,4.44771525 2.44771525,4 3,4 Z M10,10 L20,10 C20.5522847,10 21,10.4477153 21,11 L21,13 C21,13.5522847 20.5522847,14 20,14 L10,14 C9.44771525,14 9,13.5522847 9,13 L9,11 C9,10.4477153 9.44771525,10 10,10 Z M10,16 L20,16 C20.5522847,16 21,16.4477153 21,17 L21,19 C21,19.5522847 20.5522847,20 20,20 L10,20 C9.44771525,20 9,19.5522847 9,19 L9,17 C9,16.4477153 9.44771525,16 10,16 Z" fill="#000000"></path>
						<rect fill="#000000" opacity="0.3" x="2" y="10" width="5" height="10" rx="1"></rect>
					</g>
				</svg>
				<div class="kt-notification__item-details">
					<div class="kt-notification__item-title kt-font-bold">
						Ecommerce Settings
					</div>
				</div>
			</a>
		@endif
	<?php
	}
	?>
@endif