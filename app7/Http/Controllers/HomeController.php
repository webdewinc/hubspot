<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Support\Facades\Validator;
use Auth;
use Crypt;
use Redirect;
use Hash;
use DB;
use App\User;
use Session;
use Twilio\Rest\Client;

class HomeController extends Controller
{

    public function __construct()
    {
        //$this->middleware('auth');
    }
    public function index() {   
        return redirect('signin');
    }
    public function home() {
        return view('home');
    }
    public function all_app(){
        return view('all-app');
    }
    
    public function auth2_callback(Request $request)
    {  
        $provider = new \Flipbox\OAuth2\Client\Provider\HubSpot([
            'clientId'          => config('services.login.clientId'),
            'clientSecret'      => config('services.login.clientSecret'),
            'redirectUri'       => url('auth2-callback')
        ]);

        if ( !isset($_GET['code']) ) {
            
            $options = [
                'scope' => ['contacts','timeline'] // array or string
            ];

            // If we don't have an authorization code then get one
            $authUrl = $provider->getAuthorizationUrl($options);
            
            header('Location: '.$authUrl);
            exit;
            // Check given state against previously stored one to mitigate CSRF attack

        } 
        //elseif (empty($_GET['state'])) {
            //exit('Access code');
        //} 
        else {
            $result = [];
            // Try to get an access token (using the authorization code grant)
            $token = $provider->getAccessToken('authorization_code', [
                'code' => $_GET['code']
            ]);
            // Optional: Now you have a token you can look up a users profile data
            try {

                // We got an access token, let's now get the user's details
                $user = $provider->getResourceOwner($token);
                
                
                $result['portal_id'] = $user->getHubId();
                $result['email'] = $user->getEmail();
                $result['app_id'] = @$user->getAppId();
                $result['refresh_token'] = @$token->getRefreshToken();
                $result['expires_at'] = @$token->getExpires();
                $result['user_expires_at'] = @$user->getExpires();
                $result['code'] = $_GET['code'];
                $result['expires_at'] = time() + $result['expires_at'] * 0.95;
                $result['auth_token'] = $token->getToken();
                $result['user_fname'] = 'User';
                $result['oauth2state'] = @$_GET['state'];
                $result['user_email_verified'] = 1;
                $result['user_status'] = 1;
                $result['sms_credentials_default_type'] = '';

                  $scopes = @serialize($user->getScopes());
                  if(!empty($result['email'])){
                        $user_result = User::where('email',$result['email'])->first();
                        $count = User::where('email',$result['email'])->count();
                        if($count > 0){
                            Auth::login($user_result);
                            // update
                            $users = User::find(Auth::user()->id);
                            $users->portal_id = $result['portal_id'];
                            $users->hub_email = $user->getEmail();
                            $users->scopes =  $scopes;
                            $users->app_id = @$user->getAppId();
                            $users->refresh_token = @$token->getRefreshToken();
                            $users->expires_at = @$token->getExpires();
                            $users->code = $_GET['code'];
                            $users->auth_token = $token->getToken();
                            $users->save();
                            // update
                        } else {
                            unset($result['user_expires_at']);
                            User::create($result);
                            $user_result = User::where('email',$result['email'])->first();
                            Auth::login($user_result);
                        }
                  }

                // $args['data'] = '{
                //       "properties": [
                //         {
                //           "property": "portal_id",
                //           "value": "'.$result['portal_id'].'"
                //         },
                //         {
                //           "property": "hub_email",
                //           "value": "'.$result['email'].'"
                //         },
                //         {
                //           "property": "refresh_token",
                //           "value": "'.$result['refresh_token'].'"
                //         },
                //         {
                //           "property": "token",
                //           "value": "'.$result['auth_token'].'"
                //         },
                //         {
                //           "property": "code",
                //           "value": "'.$result['code'].'"
                //         },
                //         {
                //           "property": "app_id",
                //           "value": "'.$result['app_id'].'"
                //         }
                //       ]
                //     }';
                // ;



                // $args['data'] = '{
                //       "properties": [
                //         {
                //           "property": "email",
                //           "value": "'.$result['email'].'"
                //         },
                //         {
                //           "property": "firstname",
                //           "value": "'.$result['email'].'"
                //         },
                //         {
                //           "property": "lastname",
                //           "value": "'.$result['email'].'"
                //         }
                //       ]
                //     }';
                // ;
                // $args['url'] = 'https://api.hubapi.com/contacts/v1/contact/email/'.$result['email'].'/profile?hapikey='.env('HUBSPOT_API_KEY');
                // $store = $this->curlAccess( 'POST', $args );
                // Use these details to create a new profile

            } catch (Exception $e) {
                // Failed to get user details
                exit('Oh dear...');
            }
            // Use this to interact with an API on the users behalf
            return redirect('/');
        }
    }

    function curlAccess($method, $array, $content_type = 'array' ) 
    {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $array['url']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        switch ($method){
          case "POST":
            curl_setopt($ch, CURLOPT_POST, 1);        
             if ($array['data'])
                curl_setopt($ch, CURLOPT_POSTFIELDS, $array['data']);
             break;
          case "PUT":
             curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
             if ($array['data'])
                curl_setopt($ch, CURLOPT_POSTFIELDS, $array['data']);
             break;
          default:
             curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        }    

        $headers = array();
        $headers[] = 'Content-Type: application/json';
        if(!empty($array['token'])){
            $headers[] = 'authorization: Bearer '. $array['token'];  
        }
        

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $result = curl_exec($ch);
        
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);

        if($content_type == 'array'){
            $result = json_decode($result); 
        }  
        return $result;

    }
    public function logout(){
        Auth::logout();
        return redirect('/signin');   
    }
}